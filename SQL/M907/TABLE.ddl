--MW oopCREATE  TABLE M907.BTFV10                                                       
--MW oop        ( NSOCGL CHAR ( 5 ) NOT NULL                                            
--MW oop        , NETAB CHAR ( 3 ) NOT NULL                                             
--MW oop        , NJRN CHAR ( 10 ) NOT NULL                                             
--MW oop        , NPIECE CHAR ( 10 ) NOT NULL                                           
--MW oop        , CORIG CHAR ( 1 ) NOT NULL                                             
--MW oop        , NMVT CHAR ( 7 ) NOT NULL                                              
--MW oop        , NEXERCICE CHAR ( 4 ) NOT NULL                                         
--MW oop        , NPERIODE CHAR ( 2 ) NOT NULL                                          
--MW oop        , DMVT CHAR ( 8 ) NOT NULL                                              
--MW oop        , DCOMPT CHAR ( 8 ) NOT NULL                                            
--MW oop        , NAUX CHAR ( 3 ) NOT NULL                                              
--MW oop        , LMVT CHAR ( 30 ) NOT NULL                                             
--MW oop        , NCOMPTE CHAR ( 6 ) NOT NULL                                           
--MW oop        , NSSCOMPTE CHAR ( 6 ) NOT NULL                                         
--MW oop        , NSECTION CHAR ( 6 ) NOT NULL                                          
--MW oop        , NRUBR CHAR ( 6 ) NOT NULL                                             
--MW oop        , NSTEAPP CHAR ( 5 ) NOT NULL                                           
--MW oop        , NETABCLEF CHAR ( 3 ) NOT NULL                                         
--MW oop        , NTIERSCV CHAR ( 6 ) NOT NULL                                          
--MW oop        , NLETTRAGE CHAR ( 6 ) NOT NULL                                         
--MW oop        , NOECS CHAR ( 5 ) NOT NULL                                             
--MW oop        , NENTITE CHAR ( 5 ) NOT NULL                                           
--MW oop        , PMONTMVT DECIMAL ( 11 , 2 ) NOT NULL                                  
--MW oop        , TMONTMVT CHAR ( 1 ) NOT NULL                                          
--MW oop        , WCTRP CHAR ( 1 ) NOT NULL                                             
--MW oop        , WNCENT CHAR ( 1 ) NOT NULL                                            
--MW oop        , DCREAT CHAR ( 8 ) NOT NULL                                            
--MW oop        , DANNUL CHAR ( 8 ) NOT NULL                                            
--MW oop        , DDCENT CHAR ( 8 ) NOT NULL                                            
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        , WNSOLDE CHAR ( 1 ) NOT NULL                                           
--MW oop        )                                                                       
--MW oop      IN M907_BTFV10_TAB INDEX IN M907_BTFV10_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE M907.BTFV12                                                       
--MW oop        ( NSOCGL CHAR ( 5 ) NOT NULL                                            
--MW oop        , NETAB CHAR ( 3 ) NOT NULL                                             
--MW oop        , NEXERCICE CHAR ( 4 ) NOT NULL                                         
--MW oop        , NPERIODE CHAR ( 2 ) NOT NULL                                          
--MW oop        , NCOMPTE CHAR ( 6 ) NOT NULL                                           
--MW oop        , NTIERSCV CHAR ( 6 ) NOT NULL                                          
--MW oop        , NLETTRAGE CHAR ( 6 ) NOT NULL                                         
--MW oop        , NAUX CHAR ( 3 ) NOT NULL                                              
--MW oop        , WNSOLDE CHAR ( 1 ) NOT NULL                                           
--MW oop        , DDMAJ CHAR ( 8 ) NOT NULL                                             
--MW oop        , PDEBIT DECIMAL ( 13 , 2 ) NOT NULL                                    
--MW oop        , PCREDIT DECIMAL ( 13 , 2 ) NOT NULL                                   
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_BTFV12_TAB INDEX IN M907_BTFV12_IDX
--MW oop   ;                                                                            
CREATE  TABLE M907.RTAC01                                                       
        ( DVENTE CHAR ( 8 ) NOT NULL                                            
        , NSOCIETE CHAR ( 3 ) NOT NULL                                          
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , QTLM DECIMAL ( 4 , 0 ) NOT NULL                                       
        , QELA DECIMAL ( 4 , 0 ) NOT NULL                                       
        , QDACEM DECIMAL ( 4 , 0 ) NOT NULL                                     
        , CATLM DECIMAL ( 7 , 0 ) NOT NULL                                      
        , CAELA DECIMAL ( 7 , 0 ) NOT NULL                                      
        , CADACEM DECIMAL ( 7 , 0 ) NOT NULL                                    
        , QTRDARTY DECIMAL ( 4 , 0 ) NOT NULL                                   
        , QTRDACEM DECIMAL ( 4 , 0 ) NOT NULL                                   
        , QENTRE DECIMAL ( 5 , 0 ) NOT NULL                                     
        , QVENDEUR DECIMAL ( 3 , 1 ) NOT NULL                                   
        , COUVERTN1 CHAR ( 1 ) NOT NULL                                         
        , COUVERTJ1 CHAR ( 1 ) NOT NULL                                         
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTAC01_TAB INDEX IN M907_RTAC01_IDX
   ;                                                                            
CREATE  TABLE M907.RTAC02                                                       
        ( NSOCIETE CHAR ( 3 ) NOT NULL                                          
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , DVENTE CHAR ( 8 ) NOT NULL                                            
        , HVENTE CHAR ( 6 ) NOT NULL                                            
        , QTLM DECIMAL ( 4 , 0 ) NOT NULL                                       
        , QELA DECIMAL ( 4 , 0 ) NOT NULL                                       
        , QDACEM DECIMAL ( 4 , 0 ) NOT NULL                                     
        , CATLM DECIMAL ( 9 , 2 ) NOT NULL                                      
        , CAELA DECIMAL ( 9 , 2 ) NOT NULL                                      
        , CADACEM DECIMAL ( 9 , 2 ) NOT NULL                                    
        , QTRDARTY DECIMAL ( 4 , 0 ) NOT NULL                                   
        , QENTRE DECIMAL ( 5 , 0 ) NOT NULL                                     
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTAC02_TAB INDEX IN M907_RTAC02_IDX
   ;                                                                            
CREATE  TABLE M907.RTAC03                                                       
        ( NSOCIETE CHAR ( 3 ) NOT NULL                                          
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , DVENTE CHAR ( 8 ) NOT NULL                                            
        , HVENTE CHAR ( 6 ) NOT NULL                                            
        , QENTRE DECIMAL ( 5 , 0 ) NOT NULL                                     
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , QSTATUT CHAR ( 2 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , QBOUCHON CHAR ( 2 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , WPRESENCE CHAR ( 1 ) NOT NULL                                         
          WITH DEFAULT                                                          
        , WMODIF CHAR ( 1 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CATOTAL DECIMAL ( 9 , 2 ) NOT NULL                                    
          WITH DEFAULT                                                          
        )                                                                       
      IN M907_RTAC03_TAB INDEX IN M907_RTAC03_IDX
   ;                                                                            
CREATE  TABLE M907.RTAN00                                                       
        ( CNOMPGRM CHAR ( 6 ) NOT NULL                                          
        , NSEQERR CHAR ( 4 ) NOT NULL                                           
        , LIBERR CHAR ( 100 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTAN00_TAB INDEX IN M907_RTAN00_IDX
   ;                                                                            
--MW oopCREATE  TABLE M907.RTAP00                                                       
--MW oop        ( MATRICULE CHAR ( 9 ) NOT NULL                                         
--MW oop        , MONTANT DECIMAL ( 13 , 2 ) NOT NULL                                   
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_RTAP00_TAB INDEX IN M907_RTAP00_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE M907.RTAP05                                                       
--MW oop        ( MATRICULE CHAR ( 9 ) NOT NULL                                         
--MW oop        , NPIECE CHAR ( 6 ) NOT NULL                                            
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_RTAP05_TAB INDEX IN M907_RTAP05_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE M907.RTBTROW                                                      
--MW oop        ( NOMTABLE CHAR ( 18 ) NOT NULL                                         
--MW oop        , SEM01 INTEGER NOT NULL                                                
--MW oop        , SEM02 INTEGER NOT NULL                                                
--MW oop        , SEM03 INTEGER NOT NULL                                                
--MW oop        , SEM04 INTEGER NOT NULL                                                
--MW oop        , SEM05 INTEGER NOT NULL                                                
--MW oop        , SEM06 INTEGER NOT NULL                                                
--MW oop        , SEM07 INTEGER NOT NULL                                                
--MW oop        , SEM08 INTEGER NOT NULL                                                
--MW oop        , SEM09 INTEGER NOT NULL                                                
--MW oop        , SEM10 INTEGER NOT NULL                                                
--MW oop        , SEM11 INTEGER NOT NULL                                                
--MW oop        , SEM12 INTEGER NOT NULL                                                
--MW oop        , SEM13 INTEGER NOT NULL                                                
--MW oop        , SEM14 INTEGER NOT NULL                                                
--MW oop        , SEM15 INTEGER NOT NULL                                                
--MW oop        , SEM16 INTEGER NOT NULL                                                
--MW oop        , SEM17 INTEGER NOT NULL                                                
--MW oop        , SEM18 INTEGER NOT NULL                                                
--MW oop        , SEM19 INTEGER NOT NULL                                                
--MW oop        , SEM20 INTEGER NOT NULL                                                
--MW oop        , SEM21 INTEGER NOT NULL                                                
--MW oop        , SEM22 INTEGER NOT NULL                                                
--MW oop        , SEM23 INTEGER NOT NULL                                                
--MW oop        , SEM24 INTEGER NOT NULL                                                
--MW oop        , SEM25 INTEGER NOT NULL                                                
--MW oop        , SEM26 INTEGER NOT NULL                                                
--MW oop        , SEM27 INTEGER NOT NULL                                                
--MW oop        , SEM28 INTEGER NOT NULL                                                
--MW oop        , SEM29 INTEGER NOT NULL                                                
--MW oop        , SEM30 INTEGER NOT NULL                                                
--MW oop        , SEM31 INTEGER NOT NULL                                                
--MW oop        , SEM32 INTEGER NOT NULL                                                
--MW oop        , SEM33 INTEGER NOT NULL                                                
--MW oop        , SEM34 INTEGER NOT NULL                                                
--MW oop        , SEM35 INTEGER NOT NULL                                                
--MW oop        , SEM36 INTEGER NOT NULL                                                
--MW oop        , SEM37 INTEGER NOT NULL                                                
--MW oop        , SEM38 INTEGER NOT NULL                                                
--MW oop        , SEM39 INTEGER NOT NULL                                                
--MW oop        , SEM40 INTEGER NOT NULL                                                
--MW oop        , SEM41 INTEGER NOT NULL                                                
--MW oop        , SEM42 INTEGER NOT NULL                                                
--MW oop        , SEM43 INTEGER NOT NULL                                                
--MW oop        , SEM44 INTEGER NOT NULL                                                
--MW oop        , SEM45 INTEGER NOT NULL                                                
--MW oop        , SEM46 INTEGER NOT NULL                                                
--MW oop        , SEM47 INTEGER NOT NULL                                                
--MW oop        , SEM48 INTEGER NOT NULL                                                
--MW oop        , SEM49 INTEGER NOT NULL                                                
--MW oop        , SEM50 INTEGER NOT NULL                                                
--MW oop        , SEM51 INTEGER NOT NULL                                                
--MW oop        , SEM52 INTEGER NOT NULL                                                
--MW oop        , SEM53 INTEGER NOT NULL                                                
--MW oop        , SEM54 INTEGER NOT NULL                                                
--MW oop        , SEM55 INTEGER NOT NULL                                                
--MW oop        )                                                                       
--MW oop      IN M907_RTBTROW_TAB INDEX IN M907_RTBTROW_IDX
--MW oop   ;                                                                            
CREATE  TABLE M907.RTEG00                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , RUPTCHTPAGE SMALLINT NOT NULL                                         
        , RUPTRAZPAGE SMALLINT NOT NULL                                         
        , LARGEUR SMALLINT NOT NULL                                             
        , LONGUEUR SMALLINT NOT NULL                                            
        , DSECTCREE CHAR ( 1 ) NOT NULL                                         
        , TRANSFERE CHAR ( 1 ) NOT NULL                                         
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , LIBETAT VARCHAR ( 64 ) NOT NULL                                       
        )                                                                       
      IN M907_RTEG00_TAB INDEX IN M907_RTEG00_IDX
   ;                                                                            
CREATE  TABLE M907.RTEG05                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , SKIPBEFORE SMALLINT NOT NULL                                          
        , SKIPAFTER SMALLINT NOT NULL                                           
        , RUPTIMPR SMALLINT NOT NULL                                            
        , NOMCHAMPC CHAR ( 18 ) NOT NULL                                        
        , NOMCHAMPS CHAR ( 8 ) NOT NULL                                         
        , SOUSTABLE CHAR ( 5 ) NOT NULL                                         
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTEG05_TAB INDEX IN M907_RTEG05_IDX
   ;                                                                            
CREATE  TABLE M907.RTEG10                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , POSCHAMP SMALLINT NOT NULL                                            
        , NOMCHAMP CHAR ( 18 ) NOT NULL                                         
        , RUPTIMPR SMALLINT NOT NULL                                            
        , CHCOND1 CHAR ( 18 ) NOT NULL                                          
        , "CONDITION" CHAR ( 2 ) NOT NULL                                       
        , CHCOND2 CHAR ( 18 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , MASKCHAMP VARCHAR ( 30 ) NOT NULL                                     
        )                                                                       
      IN M907_RTEG10_TAB INDEX IN M907_RTEG10_IDX
   ;                                                                            
CREATE  TABLE M907.RTEG11                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , POSCHAMP SMALLINT NOT NULL                                            
        , RUPTIMPR SMALLINT NOT NULL                                            
        , SOUSTABLE CHAR ( 5 ) NOT NULL                                         
        , NOMCHAMPS CHAR ( 8 ) NOT NULL                                         
        , NOMCHAMPC CHAR ( 18 ) NOT NULL                                        
        , CHCOND1 CHAR ( 18 ) NOT NULL                                          
        , "CONDITION" CHAR ( 2 ) NOT NULL                                       
        , CHCOND2 CHAR ( 18 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , LIBCHAMP VARCHAR ( 66 ) NOT NULL                                      
        )                                                                       
      IN M907_RTEG11_TAB INDEX IN M907_RTEG11_IDX
   ;                                                                            
CREATE  TABLE M907.RTEG15                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , POSCHAMP SMALLINT NOT NULL                                            
        , NOMCHAMPT CHAR ( 18 ) NOT NULL                                        
        , RUPTRAZ SMALLINT NOT NULL                                             
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTEG15_TAB INDEX IN M907_RTEG15_IDX
   ;                                                                            
CREATE  TABLE M907.RTEG20                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , POSCHAMP SMALLINT NOT NULL                                            
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , LIBCHAMP VARCHAR ( 66 ) NOT NULL                                      
        )                                                                       
      IN M907_RTEG20_TAB INDEX IN M907_RTEG20_IDX
   ;                                                                            
CREATE  TABLE M907.RTEG25                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , NOMCHAMP CHAR ( 18 ) NOT NULL                                         
        , LIGNECALC SMALLINT NOT NULL                                           
        , CHCALC1 CHAR ( 18 ) NOT NULL                                          
        , TYPCALCUL CHAR ( 2 ) NOT NULL                                         
        , CHCALC2 CHAR ( 18 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTEG25_TAB INDEX IN M907_RTEG25_IDX
   ;                                                                            
CREATE  TABLE M907.RTEG30                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , NOMCHAMP CHAR ( 18 ) NOT NULL                                         
        , TYPCHAMP CHAR ( 1 ) NOT NULL                                          
        , LGCHAMP SMALLINT NOT NULL                                             
        , DECCHAMP SMALLINT NOT NULL                                            
        , POSDSECT SMALLINT NOT NULL                                            
        , OCCURENCES SMALLINT NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTEG30_TAB INDEX IN M907_RTEG30_IDX
   ;                                                                            
CREATE  TABLE M907.RTEG40                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , NOMCHAMP CHAR ( 18 ) NOT NULL                                         
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTEG40_TAB INDEX IN M907_RTEG40_IDX
   ;                                                                            
CREATE  TABLE M907.RTEG90                                                       
        ( IDENTTS CHAR ( 8 ) NOT NULL                                           
        , NOMETAT CHAR ( 8 ) NOT NULL                                           
        , CHAMPTRI CHAR ( 254 ) NOT NULL                                        
        , RANGTS SMALLINT NOT NULL                                              
        )                                                                       
      IN M907_RTEG90_TAB INDEX IN M907_RTEG90_IDX
   ;                                                                            
CREATE  TABLE M907.RTFG01                                                       
        ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
        , CCHRONO CHAR ( 2 ) NOT NULL                                           
        , NUMFACT CHAR ( 7 ) NOT NULL                                           
        , NLIGNE CHAR ( 3 ) NOT NULL                                            
        , NSOCORIG CHAR ( 3 ) NOT NULL                                          
        , NLIEUORIG CHAR ( 3 ) NOT NULL                                         
        , NSOCDEST CHAR ( 3 ) NOT NULL                                          
        , NLIEUDEST CHAR ( 3 ) NOT NULL                                         
        , CTYPFACT CHAR ( 2 ) NOT NULL                                          
        , NATFACT CHAR ( 5 ) NOT NULL                                           
        , CVENT CHAR ( 5 ) NOT NULL                                             
        , NUMORIG CHAR ( 7 ) NOT NULL                                           
        , DMOUV CHAR ( 8 ) NOT NULL                                             
        , MONTHT DECIMAL ( 13 , 2 ) NOT NULL                                    
        , CTAUXTVA CHAR ( 5 ) NOT NULL                                          
        , TXESCPT DECIMAL ( 5 , 2 ) NOT NULL                                    
        , CFRAIS CHAR ( 6 ) NOT NULL                                            
        , MPCF DECIMAL ( 13 , 2 ) NOT NULL                                      
        , MFRMG DECIMAL ( 13 , 2 ) NOT NULL                                     
        , QNBPIECES DECIMAL ( 5 , 0 ) NOT NULL                                  
        , DCREAT CHAR ( 8 ) NOT NULL                                            
        , DPIECE CHAR ( 8 ) NOT NULL                                            
        , DCOMPTA CHAR ( 8 ) NOT NULL                                           
        , DMVT CHAR ( 8 ) NOT NULL                                              
        , DANNUL CHAR ( 8 ) NOT NULL                                            
        , DECHEANC CHAR ( 8 ) NOT NULL                                          
        , DREGL CHAR ( 8 ) NOT NULL                                             
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , SERVORIG CHAR ( 5 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , SERVDEST CHAR ( 5 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , DVALEUR CHAR ( 8 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , CACID CHAR ( 8 ) NOT NULL                                             
          WITH DEFAULT                                                          
        , DEXCPT CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , MPRMP DECIMAL ( 13 , 2 ) NOT NULL                                     
          WITH DEFAULT                                                          
        , SECORIG CHAR ( 6 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , SECDEST CHAR ( 6 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , NTIERSE CHAR ( 8 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , NLETTRE CHAR ( 10 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , NTIERSR CHAR ( 8 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , NLETTRR CHAR ( 10 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , TYPECLI CHAR ( 3 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , MFRTP DECIMAL ( 13 , 2 ) NOT NULL                                     
          WITH DEFAULT                                                          
        )                                                                       
      IN M907_RTFG01_TAB INDEX IN M907_RTFG01_IDX
   ;                                                                            
CREATE  TABLE M907.RTFG02                                                       
        ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
        , NJRN CHAR ( 10 ) NOT NULL                                             
        , NPIECE CHAR ( 10 ) NOT NULL                                           
        , NLIGNE CHAR ( 3 ) NOT NULL                                            
        , NEXERCICE CHAR ( 4 ) NOT NULL                                         
        , NPERIODE CHAR ( 3 ) NOT NULL                                          
        , NOECS CHAR ( 5 ) NOT NULL                                             
        , DCREAT CHAR ( 8 ) NOT NULL                                            
        , COMPTE CHAR ( 6 ) NOT NULL                                            
        , SSCOMPTE CHAR ( 6 ) NOT NULL                                          
        , SECTION CHAR ( 6 ) NOT NULL                                           
        , RUBRIQUE CHAR ( 6 ) NOT NULL                                          
        , TCOMPTE CHAR ( 1 ) NOT NULL                                           
        , LIBELLE CHAR ( 20 ) NOT NULL                                          
        , MONTANT DECIMAL ( 13 , 2 ) NOT NULL                                   
        , SENS CHAR ( 1 ) NOT NULL                                              
        , DCOMPTA CHAR ( 8 ) NOT NULL                                           
        , NETAB CHAR ( 3 ) NOT NULL                                             
        , NATFACT CHAR ( 5 ) NOT NULL                                           
        , CVENT CHAR ( 5 ) NOT NULL                                             
        , NUMFACT CHAR ( 7 ) NOT NULL                                           
        , CTYPPIECE CHAR ( 2 ) NOT NULL                                         
        , CTYPLIGNE CHAR ( 3 ) NOT NULL                                         
        , NLIGNEFACT CHAR ( 3 ) NOT NULL                                        
        , TSOCFACT CHAR ( 1 ) NOT NULL                                          
        , NSOC CHAR ( 3 ) NOT NULL                                              
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , NSOCCOR CHAR ( 3 ) NOT NULL                                           
        , DATEFIC CHAR ( 8 ) NOT NULL                                           
        , WRECUP CHAR ( 1 ) NOT NULL                                            
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , NTIERS CHAR ( 6 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , DVALEUR CHAR ( 8 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , CCHRONO CHAR ( 2 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , DRECUPCC CHAR ( 8 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , TYPECPTE CHAR ( 1 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , TIERSGCT CHAR ( 8 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , NLETTRAGE CHAR ( 10 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , TYPMT CHAR ( 3 ) NOT NULL                                             
          WITH DEFAULT                                                          
        , TAUXTVA DECIMAL ( 5 , 2 ) NOT NULL                                    
          WITH DEFAULT                                                          
        )                                                                       
      IN M907_RTFG02_TAB INDEX IN M907_RTFG02_IDX
   ;                                                                            
CREATE  TABLE M907.RTFG03                                                       
        ( NSOCCRE CHAR ( 3 ) NOT NULL                                           
        , NSOCORIG CHAR ( 3 ) NOT NULL                                          
        , NLIEUORIG CHAR ( 3 ) NOT NULL                                         
        , NSOCDEST CHAR ( 3 ) NOT NULL                                          
        , NLIEUDEST CHAR ( 3 ) NOT NULL                                         
        , CTYPFACT CHAR ( 2 ) NOT NULL                                          
        , NATFACT CHAR ( 5 ) NOT NULL                                           
        , CVENT CHAR ( 5 ) NOT NULL                                             
        , DEFFET CHAR ( 8 ) NOT NULL                                            
        , MONTHT DECIMAL ( 13 , 2 ) NOT NULL                                    
        , CTAUXTVA CHAR ( 5 ) NOT NULL                                          
        , TXESCPT DECIMAL ( 5 , 2 ) NOT NULL                                    
        , DFIN CHAR ( 8 ) NOT NULL                                              
        , FREQ CHAR ( 2 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , SECORIG CHAR ( 6 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , SECDEST CHAR ( 6 ) NOT NULL                                           
          WITH DEFAULT                                                          
        )                                                                       
      IN M907_RTFG03_TAB INDEX IN M907_RTFG03_IDX
   ;                                                                            
CREATE  TABLE M907.RTFG05                                                       
        ( NSOC CHAR ( 3 ) NOT NULL                                              
        , DSOLDE CHAR ( 8 ) NOT NULL                                            
        , MTSOLDE DECIMAL ( 13 , 2 ) NOT NULL                                   
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTFG05_TAB INDEX IN M907_RTFG05_IDX
   ;                                                                            
CREATE  TABLE M907.RTFG06                                                       
        ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
        , CCHRONO CHAR ( 2 ) NOT NULL                                           
        , NUMFACT CHAR ( 7 ) NOT NULL                                           
        , NLIGNE CHAR ( 3 ) NOT NULL                                            
        , NSEQ CHAR ( 2 ) NOT NULL                                              
        , DANNUL CHAR ( 8 ) NOT NULL                                            
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , LCOMMENT CHAR ( 78 ) NOT NULL                                         
        )                                                                       
      IN M907_RTFG06_TAB INDEX IN M907_RTFG06_IDX
   ;                                                                            
CREATE  TABLE M907.RTFG07                                                       
        ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
        , CCHRONO CHAR ( 2 ) NOT NULL                                           
        , NUMFACT CHAR ( 7 ) NOT NULL                                           
        , NLIGNE CHAR ( 3 ) NOT NULL                                            
        , QTEFACT DECIMAL ( 7 , 3 ) NOT NULL                                    
        , PUFACT DECIMAL ( 13 , 2 ) NOT NULL                                    
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , LCOMMENT CHAR ( 40 ) NOT NULL                                         
        )                                                                       
      IN M907_RTFG07_TAB INDEX IN M907_RTFG07_IDX
   ;                                                                            
CREATE  TABLE M907.RTFG10                                                       
        ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
        , CFRAIS CHAR ( 6 ) NOT NULL                                            
        , NATFACT CHAR ( 5 ) NOT NULL                                           
        , TAUX DECIMAL ( 7 , 4 ) NOT NULL                                       
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTFG10_TAB INDEX IN M907_RTFG10_IDX
   ;                                                                            
CREATE  TABLE M907.RTFG11                                                       
        ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
        , EXERCICE CHAR ( 4 ) NOT NULL                                          
        , PERIODE CHAR ( 3 ) NOT NULL                                           
        , EXERSOC CHAR ( 4 ) NOT NULL                                           
        , PERSOC CHAR ( 3 ) NOT NULL                                            
        , DDMAJ CHAR ( 8 ) NOT NULL                                             
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTFG11_TAB INDEX IN M907_RTFG11_IDX
   ;                                                                            
CREATE  TABLE M907.RTFG12                                                       
        ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
        , NJRNG CHAR ( 10 ) NOT NULL                                            
        , NJRN CHAR ( 10 ) NOT NULL                                             
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTFG12_TAB INDEX IN M907_RTFG12_IDX
   ;                                                                            
CREATE  TABLE M907.RTFG13                                                       
        ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
        , NSOC CHAR ( 3 ) NOT NULL                                              
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , COMPTECG CHAR ( 6 ) NOT NULL                                          
        , NATFACT CHAR ( 5 ) NOT NULL                                           
        , CVENT CHAR ( 5 ) NOT NULL                                             
        , NSOCCOR CHAR ( 3 ) NOT NULL                                           
        , COMPTESOC CHAR ( 6 ) NOT NULL                                         
        , NAUXSOC CHAR ( 1 ) NOT NULL                                           
        , DATEFF CHAR ( 8 ) NOT NULL                                            
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTFG13_TAB INDEX IN M907_RTFG13_IDX
   ;                                                                            
CREATE  TABLE M907.RTFG16                                                       
        ( NSOC CHAR ( 3 ) NOT NULL                                              
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , SECORIG CHAR ( 6 ) NOT NULL                                           
        , SECTION CHAR ( 6 ) NOT NULL                                           
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTFG16_TAB INDEX IN M907_RTFG16_IDX
   ;                                                                            
CREATE  TABLE M907.RTFM93                                                       
        ( CRITLIEU1 CHAR ( 3 ) NOT NULL                                         
        , CRITLIEU2 CHAR ( 3 ) NOT NULL                                         
        , LIBLIEU CHAR ( 30 ) NOT NULL                                          
        , LIEUPCA CHAR ( 3 ) NOT NULL                                           
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTFM93_TAB INDEX IN M907_RTFM93_IDX
   ;                                                                            
--MW oop CREATE  TABLE M907.RTFP01                                                       
--MW oop         ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
--MW oop         , NSOC CHAR ( 3 ) NOT NULL                                              
--MW oop         , NLIEU CHAR ( 3 ) NOT NULL                                             
--MW oop         , CASSUR CHAR ( 5 ) NOT NULL                                            
--MW oop         , NFACTURE CHAR ( 10 ) NOT NULL                                         
--MW oop         , DFACTURE CHAR ( 8 ) NOT NULL                                          
--MW oop         , NSINISTRE CHAR ( 20 ) NOT NULL                                        
--MW oop         , DSINISTRE CHAR ( 8 ) NOT NULL                                         
--MW oop         , NOM CHAR ( 25 ) NOT NULL                                              
--MW oop         , PRENOM CHAR ( 25 ) NOT NULL                                           
--MW oop         , CPOSTAL CHAR ( 5 ) NOT NULL                                           
--MW oop         , REFDOC CHAR ( 10 ) NOT NULL                                           
--MW oop         , CTYPINTER CHAR ( 3 ) NOT NULL                                         
--MW oop         , CLIEUINTER CHAR ( 3 ) NOT NULL                                        
--MW oop         , MTFRANCHISE DECIMAL ( 11 , 2 ) NOT NULL                               
--MW oop         , WFRANCHISE CHAR ( 1 ) NOT NULL                                        
--MW oop         , TXREMISE DECIMAL ( 5 , 2 ) NOT NULL                                   
--MW oop         , MTLIVR DECIMAL ( 11 , 2 ) NOT NULL                                    
--MW oop         , MTFORFREP DECIMAL ( 11 , 2 ) NOT NULL                                 
--MW oop         , NDOSSAV CHAR ( 10 ) NOT NULL                                          
--MW oop         , DANNUL CHAR ( 8 ) NOT NULL                                            
--MW oop         , DSELECT CHAR ( 8 ) NOT NULL                                           
--MW oop         , NPRESENT CHAR ( 2 ) NOT NULL                                          
--MW oop         , NLOT CHAR ( 6 ) NOT NULL                                              
--MW oop         , DVALID CHAR ( 8 ) NOT NULL                                            
--MW oop         , WREJET CHAR ( 1 ) NOT NULL                                            
--MW oop         , DREJET CHAR ( 8 ) NOT NULL                                            
--MW oop         , CREJET CHAR ( 3 ) NOT NULL                                            
--MW oop         , LREJET CHAR ( 25 ) NOT NULL                                           
--MW oop         , DCREAT CHAR ( 8 ) NOT NULL                                            
--MW oop         , DNETTING CHAR ( 8 ) NOT NULL                                          
--MW oop         , ORIGINE CHAR ( 8 ) NOT NULL                                           
--MW oop         , CDEVISE CHAR ( 3 ) NOT NULL                                           
--MW oop         , DMAJ CHAR ( 8 ) NOT NULL                                              
--MW oop         , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop         , MTFORFEXP DECIMAL ( 11 , 2 ) NOT NULL                                 
--MW oop           WITH DEFAULT                                                          
--MW oop         , WDERFACT CHAR ( 1 ) NOT NULL                                          
--MW oop           WITH DEFAULT                                                          
--MW oop         , WCARTE CHAR ( 1 ) NOT NULL                                            
--MW oop           WITH DEFAULT                                                          
--MW oop         )                                                                       
--MW oop       IN M907_RTFP01_TAB INDEX IN M907_RTFP01_IDX
--MW oop    ;                                                                            
--MW oop CREATE  TABLE M907.RTFP02                                                       
--MW oop         ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
--MW oop         , CASSUR CHAR ( 5 ) NOT NULL                                            
--MW oop         , NFACTURE CHAR ( 10 ) NOT NULL                                         
--MW oop         , NLIGNE CHAR ( 3 ) NOT NULL                                            
--MW oop         , NATURE CHAR ( 3 ) NOT NULL                                            
--MW oop         , MARQUE CHAR ( 5 ) NOT NULL                                            
--MW oop         , NCODIC CHAR ( 7 ) NOT NULL                                            
--MW oop         , MTHT DECIMAL ( 11 , 2 ) NOT NULL                                      
--MW oop         , CTAUXTVA CHAR ( 5 ) NOT NULL                                          
--MW oop         , MTTTC DECIMAL ( 11 , 2 ) NOT NULL                                     
--MW oop         , MTCLIENT DECIMAL ( 11 , 2 ) NOT NULL                                  
--MW oop         , MTREMISE DECIMAL ( 11 , 2 ) NOT NULL                                  
--MW oop         , MTPSE DECIMAL ( 11 , 2 ) NOT NULL                                     
--MW oop         , CDIAGNO CHAR ( 3 ) NOT NULL                                           
--MW oop         , NCRI CHAR ( 10 ) NOT NULL                                             
--MW oop         , DANNUL CHAR ( 8 ) NOT NULL                                            
--MW oop         , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop         )                                                                       
--MW oop       IN M907_RTFP02_TAB INDEX IN M907_RTFP02_IDX
--MW oop    ;                                                                            
--MW oop CREATE  TABLE M907.RTFP03                                                       
--MW oop         ( NSINISTRE CHAR ( 20 ) NOT NULL                                        
--MW oop         , CTYPFAC2 CHAR ( 1 ) NOT NULL                                          
--MW oop         , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop         )                                                                       
--MW oop       IN M907_RTFP03_TAB INDEX IN M907_RTFP03_IDX
--MW oop    ;                                                                            
--MW oopCREATE  TABLE M907.RTFX01                                                       
--MW oop        ( NCOMPTE CHAR ( 6 ) NOT NULL                                           
--MW oop        , CTYPCOMPTE CHAR ( 3 ) NOT NULL                                        
--MW oop        , CAGREG CHAR ( 2 ) NOT NULL                                            
--MW oop        , PMONTCA DECIMAL ( 15 , 2 ) NOT NULL                                   
--MW oop        , CTAUXTVA CHAR ( 5 ) NOT NULL                                          
--MW oop        , NOECS CHAR ( 5 ) NOT NULL                                             
--MW oop        , NCOMPTETVA CHAR ( 6 ) NOT NULL                                        
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_RTFX01_TAB INDEX IN M907_RTFX01_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE M907.RTFX45                                                       
--MW oop        ( CODEAPPLI CHAR ( 1 ) NOT NULL                                         
--MW oop        , NEXERCICE CHAR ( 4 ) NOT NULL                                         
--MW oop        , NPERIODE CHAR ( 2 ) NOT NULL                                          
--MW oop        , DCREAT CHAR ( 8 ) NOT NULL                                            
--MW oop        , PMONTANT DECIMAL ( 15 , 2 ) NOT NULL                                  
--MW oop        , WNORMAL CHAR ( 1 ) NOT NULL                                           
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_RTFX45_TAB INDEX IN M907_RTFX45_IDX
--MW oop   ;                                                                            
CREATE  TABLE M907.RTFX90                                                       
        ( NSOC CHAR ( 3 ) NOT NULL                                              
        , NOECS CHAR ( 5 ) NOT NULL                                             
        , CRITERE1 CHAR ( 5 ) NOT NULL                                          
        , CRITERE2 CHAR ( 5 ) NOT NULL                                          
        , CRITERE3 CHAR ( 5 ) NOT NULL                                          
        , COMPTECG CHAR ( 6 ) NOT NULL                                          
        , SECTION CHAR ( 6 ) NOT NULL                                           
        , RUBRIQUE CHAR ( 6 ) NOT NULL                                          
        , DATEFF CHAR ( 8 ) NOT NULL                                            
        , WSEQFAM DECIMAL ( 5 , 0 ) NOT NULL                                    
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , WFX00 CHAR ( 6 ) NOT NULL                                             
          WITH DEFAULT                                                          
        )                                                                       
      IN M907_RTFX90_TAB INDEX IN M907_RTFX90_IDX
   ;                                                                            
CREATE  TABLE M907.RTGA01                                                       
        ( CTABLEG1 CHAR ( 5 ) NOT NULL                                          
        , CTABLEG2 CHAR ( 15 ) NOT NULL                                         
        , WTABLEG CHAR ( 80 ) NOT NULL                                          
        )                                                                       
      IN M907_RTGA01_TAB INDEX IN M907_RTGA01_IDX
   ;                                                                            
CREATE  TABLE M907.RTGA10FP                                                     
        ( NSOCIETE CHAR ( 3 ) NOT NULL                                          
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , CTYPLIEU CHAR ( 1 ) NOT NULL                                          
        , LLIEU CHAR ( 20 ) NOT NULL                                            
        , DOUV CHAR ( 8 ) NOT NULL                                              
        , DFERM CHAR ( 8 ) NOT NULL                                             
        , NZONPRIX CHAR ( 2 ) NOT NULL                                          
        , CGRPMAG CHAR ( 2 ) NOT NULL                                           
        , CSTATEXPO CHAR ( 5 ) NOT NULL                                         
        , CACID CHAR ( 4 ) NOT NULL                                             
          WITH DEFAULT                                                          
        , WDACEM CHAR ( 1 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CTYPFACT CHAR ( 5 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , CTYPCONTR CHAR ( 5 ) NOT NULL                                         
          WITH DEFAULT                                                          
        , QTAUXESCPT DECIMAL ( 5 , 2 ) NOT NULL                                 
          WITH DEFAULT                                                          
        , LCONDRGLT CHAR ( 20 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
          WITH DEFAULT                                                          
        , LADRLIEU1 CHAR ( 32 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , LADRLIEU2 CHAR ( 32 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , LADRLIEU3 CHAR ( 32 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , CPOSTAL CHAR ( 5 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , LCOMMUNE CHAR ( 32 ) NOT NULL                                         
          WITH DEFAULT                                                          
        , CTYPSOC CHAR ( 3 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
          WITH DEFAULT                                                          
        , NLIEUCOMPT CHAR ( 3 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , NSOCFACT CHAR ( 3 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , NLIEUFACT CHAR ( 3 ) NOT NULL                                         
          WITH DEFAULT                                                          
        , CGRPMUT CHAR ( 2 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , NSOCVALO CHAR ( 3 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , NLIEUVALO CHAR ( 3 ) NOT NULL                                         
          WITH DEFAULT                                                          
        , CTYPLIEUVALO CHAR ( 1 ) NOT NULL                                      
          WITH DEFAULT                                                          
        , NSOCGRP CHAR ( 3 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , NORDRE CHAR ( 3 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID1 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID2 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID3 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID4 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID5 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID6 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CFRAIS CHAR ( 6 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , SECTANA CHAR ( 6 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , CCUMCA CHAR ( 1 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , WRETRO CHAR ( 1 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , NZPPREST CHAR ( 2 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , TIERSGCT CHAR ( 6 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , CODEBDF CHAR ( 2 ) NOT NULL                                           
          WITH DEFAULT                                                          
        )                                                                       
      IN M907_RTGA10FP_TAB INDEX IN M907_RTGA10FP_IDX
   ;                                                                            
CREATE  TABLE M907.RTGA71                                                       
        ( CTABLE CHAR ( 6 ) NOT NULL                                            
        , CSTABLE CHAR ( 5 ) NOT NULL                                           
        , LTABLE CHAR ( 30 ) NOT NULL                                           
        , CRESP CHAR ( 3 ) NOT NULL                                             
        , DDEBUT CHAR ( 8 ) NOT NULL                                            
        , DFIN CHAR ( 8 ) NOT NULL                                              
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , CHAMP1 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP1 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP1 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP1 CHAR ( 1 ) NOT NULL                                           
        , QDEC1 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR1 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND1 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND11 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND12 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND13 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND14 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND15 CHAR ( 3 ) NOT NULL                                         
        , CTABASS1 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS1 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE1 DECIMAL ( 1 , 0 ) NOT NULL                                   
        , CHAMP2 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP2 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP2 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP2 CHAR ( 1 ) NOT NULL                                           
        , QDEC2 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR2 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND2 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND21 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND22 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND23 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND24 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND25 CHAR ( 3 ) NOT NULL                                         
        , CTABASS2 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS2 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE2 DECIMAL ( 1 , 0 ) NOT NULL                                   
        , CHAMP3 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP3 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP3 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP3 CHAR ( 1 ) NOT NULL                                           
        , QDEC3 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR3 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND3 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND31 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND32 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND33 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND34 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND35 CHAR ( 3 ) NOT NULL                                         
        , CTABASS3 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS3 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE3 DECIMAL ( 1 , 0 ) NOT NULL                                   
        , CHAMP4 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP4 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP4 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP4 CHAR ( 1 ) NOT NULL                                           
        , QDEC4 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR4 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND4 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND41 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND42 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND43 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND44 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND45 CHAR ( 3 ) NOT NULL                                         
        , CTABASS4 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS4 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE4 DECIMAL ( 1 , 0 ) NOT NULL                                   
        , CHAMP5 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP5 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP5 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP5 CHAR ( 1 ) NOT NULL                                           
        , QDEC5 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR5 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND5 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND51 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND52 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND53 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND54 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND55 CHAR ( 3 ) NOT NULL                                         
        , CTABASS5 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS5 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE5 DECIMAL ( 1 , 0 ) NOT NULL                                   
        , CHAMP6 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP6 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP6 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP6 CHAR ( 1 ) NOT NULL                                           
        , QDEC6 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR6 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND6 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND61 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND62 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND63 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND64 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND65 CHAR ( 3 ) NOT NULL                                         
        , CTABASS6 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS6 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE6 DECIMAL ( 1 , 0 ) NOT NULL                                   
        )                                                                       
      IN M907_RTGA71_TAB INDEX IN M907_RTGA71_IDX
   ;                                                                            
CREATE  TABLE M907.RTGA99                                                       
        ( CNOMPGRM CHAR ( 5 ) NOT NULL                                          
        , NSEQERR CHAR ( 4 ) NOT NULL                                           
        , CERRUT CHAR ( 4 ) NOT NULL                                            
        , LIBERR CHAR ( 53 ) NOT NULL                                           
        , CLANG CHAR ( 2 ) NOT NULL                                             
          WITH DEFAULT                                                          
        )                                                                       
      IN M907_RTGA99_TAB INDEX IN M907_RTGA99_IDX
   ;                                                                            
CREATE  TABLE M907.RTGQ01                                                       
        ( CINSEE CHAR ( 5 ) NOT NULL                                            
        , NSOC CHAR ( 3 ) NOT NULL                                              
        , CURBAIN CHAR ( 3 ) NOT NULL                                           
        , CDOUBLE DECIMAL ( 9 , 0 ) NOT NULL                                    
        , QPRINCI DECIMAL ( 9 , 0 ) NOT NULL                                    
        , QSECOND DECIMAL ( 9 , 0 ) NOT NULL                                    
        , QVACANT DECIMAL ( 9 , 0 ) NOT NULL                                    
        , QEVOLUT DECIMAL ( 5 , 2 ) NOT NULL                                    
        , QRESACT DECIMAL ( 7 , 0 ) NOT NULL                                    
        , QCOEFF DECIMAL ( 3 , 0 ) NOT NULL                                     
        , WCONTRA CHAR ( 1 ) NOT NULL                                           
        , CELEMEN CHAR ( 5 ) NOT NULL                                           
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTGQ01_TAB INDEX IN M907_RTGQ01_IDX
   ;                                                                            
CREATE  TABLE M907.RTGQ06                                                       
        ( CTYPE CHAR ( 1 ) NOT NULL                                             
        , CINSEE CHAR ( 5 ) NOT NULL                                            
        , WPARCOM CHAR ( 1 ) NOT NULL                                           
        , LCOMUNE CHAR ( 32 ) NOT NULL                                          
        , CPOSTAL CHAR ( 5 ) NOT NULL                                           
        , WPARBUR CHAR ( 1 ) NOT NULL                                           
        , LBUREAU CHAR ( 26 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTGQ06_TAB INDEX IN M907_RTGQ06_IDX
   ;                                                                            
--MW oopCREATE  TABLE M907.RTGQ91                                                       
--MW oop        ( CINSEE CHAR ( 5 ) NOT NULL                                            
--MW oop        , NSOC CHAR ( 3 ) NOT NULL                                              
--MW oop        , CURBAIN CHAR ( 3 ) NOT NULL                                           
--MW oop        , CDOUBLE DECIMAL ( 9 , 0 ) NOT NULL                                    
--MW oop        , QPRINCI DECIMAL ( 9 , 0 ) NOT NULL                                    
--MW oop        , QSECOND DECIMAL ( 9 , 0 ) NOT NULL                                    
--MW oop        , QVACANT DECIMAL ( 9 , 0 ) NOT NULL                                    
--MW oop        , QEVOLUT DECIMAL ( 5 , 2 ) NOT NULL                                    
--MW oop        , QRESACT DECIMAL ( 7 , 0 ) NOT NULL                                    
--MW oop        , QCOEFF DECIMAL ( 3 , 0 ) NOT NULL                                     
--MW oop        , WCONTRA CHAR ( 1 ) NOT NULL                                           
--MW oop        , CELEMEN CHAR ( 5 ) NOT NULL                                           
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_RTGQ91_TAB INDEX IN M907_RTGQ91_IDX
--MW oop   ;                                                                            
CREATE  TABLE M907.RTGQ96                                                       
        ( CTYPE CHAR ( 1 ) NOT NULL                                             
        , CINSEE CHAR ( 5 ) NOT NULL                                            
        , WPARCOM CHAR ( 1 ) NOT NULL                                           
        , LCOMUNE CHAR ( 32 ) NOT NULL                                          
        , CPOSTAL CHAR ( 5 ) NOT NULL                                           
        , WPARBUR CHAR ( 1 ) NOT NULL                                           
        , LBUREAU CHAR ( 26 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTGQ96_TAB INDEX IN M907_RTGQ96_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF00                                                       
        ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
        , CTYPCTA CHAR ( 3 ) NOT NULL                                           
        , CMODIMP CHAR ( 1 ) NOT NULL                                           
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , NETAB CHAR ( 3 ) NOT NULL                                             
          WITH DEFAULT                                                          
        )                                                                       
      IN M907_RTIF00_TAB INDEX IN M907_RTIF00_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF01                                                       
        ( NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
        , CORGANISME CHAR ( 3 ) NOT NULL                                        
        , WACTIF CHAR ( 1 ) NOT NULL                                            
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , NETAB CHAR ( 3 ) NOT NULL                                             
          WITH DEFAULT                                                          
        )                                                                       
      IN M907_RTIF01_TAB INDEX IN M907_RTIF01_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF02                                                       
        ( CORGANISME CHAR ( 3 ) NOT NULL                                        
        , NIDENT CHAR ( 20 ) NOT NULL                                           
        , NSOCIETE CHAR ( 3 ) NOT NULL                                          
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , CBANQUE CHAR ( 2 ) NOT NULL                                           
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTIF02_TAB INDEX IN M907_RTIF02_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF03                                                       
        ( CMODPAI CHAR ( 3 ) NOT NULL                                           
        , CTYPCTA CHAR ( 3 ) NOT NULL                                           
        , CTYPMONT CHAR ( 5 ) NOT NULL                                          
        , CMODCTA CHAR ( 1 ) NOT NULL                                           
        , NAUX CHAR ( 3 ) NOT NULL                                              
        , WLETTRAGE CHAR ( 1 ) NOT NULL                                         
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTIF03_TAB INDEX IN M907_RTIF03_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF04                                                       
        ( CTYPCTA CHAR ( 3 ) NOT NULL                                           
        , CTYPMONT CHAR ( 5 ) NOT NULL                                          
        , NOECS CHAR ( 5 ) NOT NULL                                             
        , CMOTCLE1 CHAR ( 10 ) NOT NULL                                         
        , CMOTCLE2 CHAR ( 10 ) NOT NULL                                         
        , CMOTCLE3 CHAR ( 10 ) NOT NULL                                         
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTIF04_TAB INDEX IN M907_RTIF04_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF05                                                       
        ( CMODPAI CHAR ( 3 ) NOT NULL                                           
        , CTYPMONT CHAR ( 5 ) NOT NULL                                          
        , NORDRE CHAR ( 2 ) NOT NULL                                            
        , CLIBFIXE CHAR ( 30 ) NOT NULL                                         
        , CMOTCLE CHAR ( 10 ) NOT NULL                                          
        , CMASQUE CHAR ( 5 ) NOT NULL                                           
        , WSEPFIN CHAR ( 1 ) NOT NULL                                           
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTIF05_TAB INDEX IN M907_RTIF05_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF06                                                       
        ( NEXER CHAR ( 4 ) NOT NULL                                             
        , NPER CHAR ( 3 ) NOT NULL                                              
        , DDEBUT CHAR ( 8 ) NOT NULL                                            
        , DFIN CHAR ( 8 ) NOT NULL                                              
        , DLIMITE CHAR ( 8 ) NOT NULL                                           
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTIF06_TAB INDEX IN M907_RTIF06_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF07                                                       
        ( CMODPAI CHAR ( 3 ) NOT NULL                                           
        , CTYPCTA CHAR ( 3 ) NOT NULL                                           
        , CTYPMONT CHAR ( 5 ) NOT NULL                                          
        , NORDRE CHAR ( 2 ) NOT NULL                                            
        , CMOTCLE CHAR ( 10 ) NOT NULL                                          
        , CMASQUE CHAR ( 5 ) NOT NULL                                           
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTIF07_TAB INDEX IN M907_RTIF07_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF08                                                       
        ( CMODPAI CHAR ( 3 ) NOT NULL                                           
        , CTYPCTA CHAR ( 3 ) NOT NULL                                           
        , CTYPMONT CHAR ( 5 ) NOT NULL                                          
        , NORDRE CHAR ( 2 ) NOT NULL                                            
        , CLIBFIXE CHAR ( 6 ) NOT NULL                                          
        , CMOTCLE CHAR ( 10 ) NOT NULL                                          
        , CMASQUE CHAR ( 5 ) NOT NULL                                           
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTIF08_TAB INDEX IN M907_RTIF08_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF10                                                       
        ( CORGANISME CHAR ( 3 ) NOT NULL                                        
        , DRECEPT CHAR ( 8 ) NOT NULL                                           
        , HRECEPT CHAR ( 4 ) NOT NULL                                           
        , DDTRAIT CHAR ( 8 ) NOT NULL                                           
        , NBRECUS DECIMAL ( 5 , 0 ) NOT NULL                                    
        , NBREJETS DECIMAL ( 5 , 0 ) NOT NULL                                   
        , NBANOS DECIMAL ( 5 , 0 ) NOT NULL                                     
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , DFICORG CHAR ( 8 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , NFICORG DECIMAL ( 7 , 0 ) NOT NULL                                    
          WITH DEFAULT                                                          
        )                                                                       
      IN M907_RTIF10_TAB INDEX IN M907_RTIF10_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF11                                                       
        ( CORGANISME CHAR ( 3 ) NOT NULL                                        
        , DRECEPT CHAR ( 8 ) NOT NULL                                           
        , HRECEPT CHAR ( 4 ) NOT NULL                                           
        , NIDENT CHAR ( 20 ) NOT NULL                                           
        , CMODPAI CHAR ( 5 ) NOT NULL                                           
        , DFINANCE CHAR ( 8 ) NOT NULL                                          
        , DOPER CHAR ( 8 ) NOT NULL                                             
        , DREMISE CHAR ( 8 ) NOT NULL                                           
        , NREMISE CHAR ( 6 ) NOT NULL                                           
        , CODANO CHAR ( 2 ) NOT NULL                                            
        , MTBRUT DECIMAL ( 9 , 2 ) NOT NULL                                     
        , MTCOM DECIMAL ( 7 , 2 ) NOT NULL                                      
        , MTNET DECIMAL ( 9 , 2 ) NOT NULL                                      
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTIF11_TAB INDEX IN M907_RTIF11_IDX
   ;                                                                            
CREATE  TABLE M907.RTIF20                                                       
        ( CORGANISME CHAR ( 3 ) NOT NULL                                        
        , CMODPAI CHAR ( 5 ) NOT NULL                                           
        , NIDENT CHAR ( 20 ) NOT NULL                                           
        , DFINANCE CHAR ( 8 ) NOT NULL                                          
        , DTRAIT CHAR ( 8 ) NOT NULL                                            
        , DREMISE CHAR ( 8 ) NOT NULL                                           
        , NREMISE CHAR ( 6 ) NOT NULL                                           
        , NBMVTS DECIMAL ( 5 , 0 ) NOT NULL                                     
        , NJRN CHAR ( 3 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTIF20_TAB INDEX IN M907_RTIF20_IDX
   ;                                                                            
CREATE  TABLE M907.RTIGSA                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , COL01001 CHAR ( 92 ) NOT NULL                                         
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     )                                                                          
        )                                                                       
      IN M907_RTIGSA_TAB INDEX IN M907_RTIGSA_IDX
   ;                                                                            
CREATE  TABLE M907.RTIGSB                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , NT_DIGSB DECIMAL ( 1 , 0 ) NOT NULL                                   
        , COL05001 CHAR ( 90 ) NOT NULL                                         
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     ,NT_DIGSB                                                                  
     )                                                                          
        )                                                                       
      IN M907_RTIGSB_TAB INDEX IN M907_RTIGSB_IDX
   ;                                                                            
CREATE  TABLE M907.RTIGSC                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , DIGFC0 CHAR ( 12 ) NOT NULL                                           
        , SM_DIGSC TIMESTAMP NOT NULL                                           
        , COL06001 CHAR ( 2 ) NOT NULL                                          
        , COL06002 VARCHAR ( 144 ) NOT NULL                                     
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     ,DIGFC0                                                                    
     ,SM_DIGSC                                                                  
     )                                                                          
        )                                                                       
      IN M907_RTIGSC_TAB INDEX IN M907_RTIGSC_IDX
   ;                                                                            
CREATE  TABLE M907.RTIGSD                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , DIGFD0 CHAR ( 24 ) NOT NULL                                           
        , COL02001 CHAR ( 40 ) NOT NULL                                         
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     ,DIGFD0                                                                    
     )                                                                          
        )                                                                       
      IN M907_RTIGSD_TAB INDEX IN M907_RTIGSD_IDX
   ;                                                                            
CREATE  TABLE M907.RTIGSE                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , DIGFE0 CHAR ( 9 ) NOT NULL                                            
        , COL03001 CHAR ( 55 ) NOT NULL                                         
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     ,DIGFE0                                                                    
     )                                                                          
        )                                                                       
      IN M907_RTIGSE_TAB INDEX IN M907_RTIGSE_IDX
   ;                                                                            
CREATE  TABLE M907.RTIGSF                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , DIGFF0 CHAR ( 4 ) NOT NULL                                            
        , COL04001 CHAR ( 60 ) NOT NULL                                         
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     ,DIGFF0                                                                    
     )                                                                          
        )                                                                       
      IN M907_RTIGSF_TAB INDEX IN M907_RTIGSF_IDX
   ;                                                                            
CREATE  TABLE M907.RTLI00                                                       
        ( NSOCIETE CHAR ( 3 ) NOT NULL                                          
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , CPARAM CHAR ( 5 ) NOT NULL                                            
        , LVPARAM CHAR ( 20 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTLI00_TAB INDEX IN M907_RTLI00_IDX
   ;                                                                            
--MW oopCREATE  TABLE M907.RTPE01                                                       
--MW oop        ( NSOCIETE CHAR ( 5 ) NOT NULL                                          
--MW oop        , NRUBPAIE CHAR ( 3 ) NOT NULL                                          
--MW oop        , CCONTRAT CHAR ( 2 ) NOT NULL                                          
--MW oop        , CRUBRIQUE CHAR ( 6 ) NOT NULL                                         
--MW oop        , WSIGNE CHAR ( 1 ) NOT NULL                                            
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_RTPE01_TAB INDEX IN M907_RTPE01_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE M907.RTPE02                                                       
--MW oop        ( NSOCIETE CHAR ( 5 ) NOT NULL                                          
--MW oop        , NRUBPAIE CHAR ( 3 ) NOT NULL                                          
--MW oop        , NCPTGESTION CHAR ( 6 ) NOT NULL                                       
--MW oop        , NCPTBILAN CHAR ( 6 ) NOT NULL                                         
--MW oop        , WSSCOMPTE CHAR ( 1 ) NOT NULL                                         
--MW oop        , WSIGNE CHAR ( 1 ) NOT NULL                                            
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_RTPE02_TAB INDEX IN M907_RTPE02_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE M907.RTPE03                                                       
--MW oop        ( NSOCIETE CHAR ( 5 ) NOT NULL                                          
--MW oop        , NSECTPAIE CHAR ( 3 ) NOT NULL                                         
--MW oop        , NSSCOMPTE CHAR ( 6 ) NOT NULL                                         
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_RTPE03_TAB INDEX IN M907_RTPE03_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE M907.RTPE04                                                       
--MW oop        ( NSOCIETE CHAR ( 5 ) NOT NULL                                          
--MW oop        , NRUBPAIE CHAR ( 3 ) NOT NULL                                          
--MW oop        , CCONTRAT CHAR ( 2 ) NOT NULL                                          
--MW oop        , CHORAIRE CHAR ( 3 ) NOT NULL                                          
--MW oop        , CRUBRIQUE CHAR ( 6 ) NOT NULL                                         
--MW oop        , WSIGNE CHAR ( 1 ) NOT NULL                                            
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        , CABSENCE CHAR ( 3 ) NOT NULL                                          
--MW oop          WITH DEFAULT                                                          
--MW oop        )                                                                       
--MW oop      IN M907_RTPE04_TAB INDEX IN M907_RTPE04_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE M907.RTPE05                                                       
--MW oop        ( CENREG CHAR ( 2 ) NOT NULL                                            
--MW oop        , NSOCIETE CHAR ( 5 ) NOT NULL                                          
--MW oop        , DMOIS CHAR ( 2 ) NOT NULL                                             
--MW oop        , DANNEE CHAR ( 2 ) NOT NULL                                            
--MW oop        , NSECTION CHAR ( 3 ) NOT NULL                                          
--MW oop        , NSSSECTION CHAR ( 3 ) NOT NULL                                        
--MW oop        , CHORAIRE CHAR ( 3 ) NOT NULL                                          
--MW oop        , CCONTRAT CHAR ( 2 ) NOT NULL                                          
--MW oop        , NRUBPAIE CHAR ( 3 ) NOT NULL                                          
--MW oop        , CORGABS CHAR ( 3 ) NOT NULL                                           
--MW oop        , PMONTANT DECIMAL ( 11 , 2 ) NOT NULL                                  
--MW oop        , CANOMALIE CHAR ( 1 ) NOT NULL                                         
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_RTPE05_TAB INDEX IN M907_RTPE05_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE M907.RTPE06                                                       
--MW oop        ( NSOCIETE CHAR ( 5 ) NOT NULL                                          
--MW oop        , TYPECPT CHAR ( 1 ) NOT NULL                                           
--MW oop        , NCPT CHAR ( 6 ) NOT NULL                                              
--MW oop        , CRUBRIQUE CHAR ( 6 ) NOT NULL                                         
--MW oop        , NSECTION CHAR ( 3 ) NOT NULL                                          
--MW oop        , NSSSECTION CHAR ( 3 ) NOT NULL                                        
--MW oop        , LIBCPTPE CHAR ( 17 ) NOT NULL                                         
--MW oop        , WSOLDE CHAR ( 1 ) NOT NULL                                            
--MW oop        , NJRN CHAR ( 10 ) NOT NULL                                             
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_RTPE06_TAB INDEX IN M907_RTPE06_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE M907.RTPE07                                                       
--MW oop        ( NSOCIETE CHAR ( 5 ) NOT NULL                                          
--MW oop        , NSECTIONPAIE CHAR ( 3 ) NOT NULL                                      
--MW oop        , NSSSECTIONPAIE CHAR ( 3 ) NOT NULL                                    
--MW oop        , NSECTIONCONSO CHAR ( 3 ) NOT NULL                                     
--MW oop        , NSSSECTIONCONSO CHAR ( 3 ) NOT NULL                                   
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN M907_RTPE07_TAB INDEX IN M907_RTPE07_IDX
--MW oop   ;                                                                            
CREATE  TABLE M907.RTPT01                                                       
        ( CODLANG CHAR ( 2 ) NOT NULL                                           
        , CODPIC CHAR ( 2 ) NOT NULL                                            
        )                                                                       
      IN M907_RTPT01_TAB INDEX IN M907_RTPT01_IDX
   ;                                                                            
CREATE  TABLE M907.RTRC10                                                       
        ( NSOCIETE CHAR ( 3 ) NOT NULL                                          
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , NOMFIC CHAR ( 8 ) NOT NULL                                            
        , DATEFIC CHAR ( 8 ) NOT NULL                                           
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTRC10_TAB INDEX IN M907_RTRC10_IDX
   ;                                                                            
CREATE  TABLE M907.RTRC20                                                       
        ( NSOCIETE CHAR ( 3 ) NOT NULL                                          
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , NOMFIC CHAR ( 8 ) NOT NULL                                            
        , DATEFIC CHAR ( 8 ) NOT NULL                                           
        , NSEQ DECIMAL ( 13 , 0 ) NOT NULL                                      
        , ENRFIC1 CHAR ( 128 ) NOT NULL                                         
        , ENRFIC2 CHAR ( 128 ) NOT NULL                                         
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN M907_RTRC20_TAB INDEX IN M907_RTRC20_IDX
   ;                                                                            
--MW oopCREATE  TABLE M907.RTZZZZ                                                       
--MW oop        ( NSOCCRE CHAR ( 3 ) NOT NULL                                           
--MW oop        , NSOCORIG CHAR ( 3 ) NOT NULL                                          
--MW oop        , NLIEUORIG CHAR ( 3 ) NOT NULL                                         
--MW oop        , NSOCDEST CHAR ( 3 ) NOT NULL                                          
--MW oop        , NLIEUDEST CHAR ( 3 ) NOT NULL                                         
--MW oop        , CTYPFACT CHAR ( 2 ) NOT NULL                                          
--MW oop        , NATFACT CHAR ( 5 ) NOT NULL                                           
--MW oop        , CVENT CHAR ( 5 ) NOT NULL                                             
--MW oop        , DEFFET CHAR ( 8 ) NOT NULL                                            
--MW oop        , MONTHT DECIMAL ( 13 , 2 ) NOT NULL                                    
--MW oop        , CTAUXTVA CHAR ( 5 ) NOT NULL                                          
--MW oop        , TXESCPT DECIMAL ( 5 , 2 ) NOT NULL                                    
--MW oop        , DFIN CHAR ( 8 ) NOT NULL                                              
--MW oop        , FREQ CHAR ( 2 ) NOT NULL                                              
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        , SECORIG CHAR ( 6 ) NOT NULL                                           
--MW oop          WITH DEFAULT                                                          
--MW oop        , SECDEST CHAR ( 6 ) NOT NULL                                           
--MW oop          WITH DEFAULT                                                          
--MW oop        )                                                                       
--MW oop      IN M907_RTZZZZ_TAB INDEX IN M907_RTZZZZ_IDX
--MW oop   ;                                                                            
