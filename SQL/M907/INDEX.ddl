CREATE  INDEX M907.RXRC201 ON M907.RTRC20                                       
        ( NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
        , NSEQ ASC                                                              
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
--MW DAR-1119           PARTITION BY RANGE                                                   
--MW DAR-1119          (PARTITION 1                                                          
--MW DAR-1119           ENDING AT ( 'CBCOSYS' )                                              
--MW DAR-1119          ,PARTITION 2                                                          
--MW DAR-1119           ENDING AT ( 'CETCOSYS' )                                             
--MW DAR-1119          ,PARTITION 3                                                          
--MW DAR-1119           ENDING AT ( 'FFGCOSYS' )                                             
--MW DAR-1119          ,PARTITION 4                                                          
--MW DAR-1119           ENDING AT ( 'SOFCOSYS' )                                             
--MW DAR-1119          )                                                                     
    ;                                                                           
--MW CREATE  UNIQUE INDEX M907.RXIGSA0R ON M907.RTIGSA                               
--MW         ( DIGFA0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX M907.RXIGSB0R ON M907.RTIGSB                               
--MW         ( DIGFA0 ASC                                                            
--MW         , NT_DIGSB ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX M907.RXIGSC0R ON M907.RTIGSC                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFC0 ASC                                                            
--MW         , SM_DIGSC ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX M907.RXIGSD0R ON M907.RTIGSD                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFD0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX M907.RXIGSE0R ON M907.RTIGSE                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFE0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX M907.RXIGSF0R ON M907.RTIGSF                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFF0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
CREATE  INDEX M907.RXFG021 ON M907.RTFG02                                       
        ( NUMFACT ASC                                                           
        , NATFACT ASC                                                           
        , NSOCCOMPT ASC                                                         
        , CTYPPIECE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW oop CREATE  INDEX M907.RXFP011 ON M907.RTFP01                                       
--MW oop         ( NSINISTRE ASC                                                         
--MW oop           )                                                                     
--MW oop            PCTFREE 10                                                           
--MW oop     ;                                                                           
CREATE  INDEX M907.RXGQ01 ON M907.RTGQ01                                        
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX M907.RXGQ061 ON M907.RTGQ06                                       
        ( CTYPE ASC                                                             
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX M907.RXGQ063 ON M907.RTGQ06                                       
        ( CPOSTAL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX M907.RXGQ910 ON M907.RTGQ91                                       
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX M907.RXGQ960 ON M907.RTGQ96                                       
        ( CTYPE ASC                                                             
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX M907.RXGQ963 ON M907.RTGQ96                                       
        ( CPOSTAL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX M907.RXLI001 ON M907.RTLI00                                       
        ( CPARAM ASC                                                            
        , LVPARAM ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXAC011P ON M907.RTAC01                               
        ( DVENTE DESC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXAC020 ON M907.RTAC02                                
        ( DVENTE DESC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , HVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXAC030 ON M907.RTAC03                                
        ( DVENTE DESC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , HVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXAP000 ON M907.RTAP00                                
        ( MATRICULE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXAP050 ON M907.RTAP05                                
        ( MATRICULE ASC                                                         
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXEG250F ON M907.RTEG25                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , LIGNECALC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXEG300F ON M907.RTEG30                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXEG400 ON M907.RTEG40                                
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFG010 ON M907.RTFG01                                
        ( NSOCCOMPT ASC                                                         
        , CCHRONO ASC                                                           
        , NUMFACT ASC                                                           
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFG020 ON M907.RTFG02                                
        ( NSOCCOMPT ASC                                                         
        , NJRN ASC                                                              
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFG030 ON M907.RTFG03                                
        ( SECORIG ASC                                                           
        , NSOCORIG ASC                                                          
        , NLIEUORIG ASC                                                         
        , SECDEST ASC                                                           
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , NATFACT ASC                                                           
        , CVENT ASC                                                             
        , CTYPFACT ASC                                                          
        , NSOCCRE ASC                                                           
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFG050 ON M907.RTFG05                                
        ( DSOLDE ASC                                                            
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFG060 ON M907.RTFG06                                
        ( NSOCCOMPT ASC                                                         
        , CCHRONO ASC                                                           
        , NUMFACT ASC                                                           
        , NLIGNE ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFG070 ON M907.RTFG07                                
        ( NSOCCOMPT ASC                                                         
        , CCHRONO ASC                                                           
        , NUMFACT ASC                                                           
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFG100 ON M907.RTFG10                                
        ( NSOCCOMPT ASC                                                         
        , CFRAIS ASC                                                            
        , NATFACT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFG110 ON M907.RTFG11                                
        ( NSOCCOMPT ASC                                                         
        , EXERCICE ASC                                                          
        , PERIODE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFG120 ON M907.RTFG12                                
        ( NSOCCOMPT ASC                                                         
        , NJRNG ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFG130 ON M907.RTFG13                                
        ( NSOCCOMPT ASC                                                         
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
        , COMPTECG ASC                                                          
        , NATFACT ASC                                                           
        , CVENT ASC                                                             
        , NSOCCOR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFG160 ON M907.RTFG16                                
        ( SECORIG ASC                                                           
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFM930 ON M907.RTFM93                                
        ( CRITLIEU1 ASC                                                         
        , CRITLIEU2 ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
--MW oop CREATE  UNIQUE INDEX M907.RXFP010 ON M907.RTFP01                                
--MW oop         ( NFACTURE ASC                                                          
--MW oop         , CASSUR ASC                                                            
--MW oop         , NSOCCOMPT ASC                                                         
--MW oop           )                                                                     
--MW oop            PCTFREE 10                                                           
--MW oop            CLUSTER                                                              
--MW oop     ;                                                                           
--MW oop CREATE  UNIQUE INDEX M907.RXFP020 ON M907.RTFP02                                
--MW oop         ( NLIGNE ASC                                                            
--MW oop         , NFACTURE ASC                                                          
--MW oop         , CASSUR ASC                                                            
--MW oop         , NSOCCOMPT ASC                                                         
--MW oop           )                                                                     
--MW oop            PCTFREE 10                                                           
--MW oop            CLUSTER                                                              
--MW oop     ;                                                                           
--MW oop CREATE  UNIQUE INDEX M907.RXFP030 ON M907.RTFP03                                
--MW oop         ( NSINISTRE ASC                                                         
--MW oop           )                                                                     
--MW oop            PCTFREE 10                                                           
--MW oop            CLUSTER                                                              
--MW oop     ;                                                                           
CREATE  UNIQUE INDEX M907.RXFX010P ON M907.RTFX01                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFX450P ON M907.RTFX45                               
        ( CODEAPPLI ASC                                                         
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
        , DCREAT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXFX900 ON M907.RTFX90                                
        ( NOECS ASC                                                             
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , DATEFF DESC                                                           
        , NSOC ASC                                                              
        , WFX00 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXGA01FP ON M907.RTGA01                               
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
--MW CREATE  UNIQUE INDEX M907.RXGA100 ON M907.RTGA10FP                              
--MW        ( NSOCIETE ASC                                                          
--MW        , NLIEU ASC                                                             
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  UNIQUE INDEX M907.RXGA101 ON M907.RTGA10FP                              
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXGA71FP ON M907.RTGA71                               
        ( CTABLE ASC                                                            
        , CSTABLE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXGA99FP ON M907.RTGA99                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXIF000 ON M907.RTIF00                                
        ( NSOCCOMPT ASC                                                         
        , NETAB ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXIF010 ON M907.RTIF01                                
        ( NSOCCOMPT ASC                                                         
        , CORGANISME ASC                                                        
        , NETAB ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXIF020 ON M907.RTIF02                                
        ( CORGANISME ASC                                                        
        , NIDENT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXIF030 ON M907.RTIF03                                
        ( CMODPAI ASC                                                           
        , CTYPCTA ASC                                                           
        , CTYPMONT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXIF040 ON M907.RTIF04                                
        ( CTYPCTA ASC                                                           
        , CTYPMONT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXIF050 ON M907.RTIF05                                
        ( CMODPAI ASC                                                           
        , CTYPMONT ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXIF060 ON M907.RTIF06                                
        ( NEXER ASC                                                             
        , NPER ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXIF070 ON M907.RTIF07                                
        ( CMODPAI ASC                                                           
        , CTYPCTA ASC                                                           
        , CTYPMONT ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXIF080 ON M907.RTIF08                                
        ( CMODPAI ASC                                                           
        , CTYPCTA ASC                                                           
        , CTYPMONT ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXIF100 ON M907.RTIF10                                
        ( DRECEPT ASC                                                           
        , HRECEPT ASC                                                           
        , CORGANISME ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXIF200 ON M907.RTIF20                                
        ( CORGANISME ASC                                                        
        , NIDENT ASC                                                            
        , CMODPAI ASC                                                           
        , DFINANCE ASC                                                          
        , DTRAIT ASC                                                            
        , DREMISE ASC                                                           
        , NREMISE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXLI000 ON M907.RTLI00                                
        ( CPARAM ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXPE010 ON M907.RTPE01                                
        ( NRUBPAIE ASC                                                          
        , CCONTRAT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXPE020 ON M907.RTPE02                                
        ( NRUBPAIE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXPE030 ON M907.RTPE03                                
        ( NSOCIETE ASC                                                          
        , NSECTPAIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXPE040 ON M907.RTPE04                                
        ( NRUBPAIE ASC                                                          
        , CCONTRAT ASC                                                          
        , CHORAIRE ASC                                                          
        , CABSENCE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXPE050 ON M907.RTPE05                                
        ( NSOCIETE ASC                                                          
        , CENREG ASC                                                            
        , NSECTION ASC                                                          
        , NSSSECTION ASC                                                        
        , NRUBPAIE ASC                                                          
        , CHORAIRE ASC                                                          
        , CCONTRAT ASC                                                          
        , CORGABS ASC                                                           
        , CANOMALIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXPE060 ON M907.RTPE06                                
        ( NSOCIETE ASC                                                          
        , NCPT ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXPE070 ON M907.RTPE07                                
        ( NSOCIETE ASC                                                          
        , NSECTIONPAIE ASC                                                      
        , NSSSECTIONPAIE ASC                                                    
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXRC10FP ON M907.RTRC10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXRC200 ON M907.RTRC20                                
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  UNIQUE INDEX M907.RXZZZ30 ON M907.RTZZZZ                                
        ( SECORIG ASC                                                           
        , NSOCORIG ASC                                                          
        , NLIEUORIG ASC                                                         
        , SECDEST ASC                                                           
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , NATFACT ASC                                                           
        , CVENT ASC                                                             
        , CTYPFACT ASC                                                          
        , NSOCCRE ASC                                                           
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
