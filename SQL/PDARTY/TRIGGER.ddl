CREATE  TRIGGER PDARTY.RJGA59S                                                  
           AFTER DELETE                                                         
        ON PDARTY.RTGA59                                                        
        REFERENCING OLD AS "OLD"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( OLD.DEFFET = SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO         
        ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6        
        , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PX' || OLD.NCODIC || '907' || 'XXX' ||                 
        OLD.NZONPRIX , 'M' , ' ' , '            ' , CURRENT TIMESTAMP )         
    ;                                                                           
CREATE  TRIGGER PDARTY.RJGG40M                                                  
           AFTER UPDATE                                                         
        OF PEXPCOMMART                                                          
        ON PDARTY.RTGG40                                                        
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.PEXPCOMMART <> OLD.PEXPCOMMART                               
    AND NEW.DEFFET = OLD.DEFFET                                                 
    AND ( OLD.DEFFET <= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) ,         
        1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 , 2        
        ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )          
    AND ( OLD.DFINEFFET >= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PM' || NEW.NCODIC || NEW.NSOCIETE || NEW.NLIEU         
        || 'XX' , 'M' , ' ' , ' ' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER PDARTY.RJGG40S                                                  
           AFTER DELETE                                                         
        ON PDARTY.RTGG40                                                        
        REFERENCING OLD AS "OLD"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( ( OLD.DEFFET <= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) ,          
        ISO ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 6 , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 ,        
        2 ) )                                                                   
    AND ( OLD.DFINEFFET > SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )         
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PM' || OLD.NCODIC || OLD.NSOCIETE || OLD.NLIEU         
        || 'XX' , 'M' , ' ' , ' ' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER PDARTY.RJGG40U                                                  
           AFTER UPDATE                                                         
        OF DFINEFFET                                                            
        ON PDARTY.RTGG40                                                        
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.NCODIC = OLD.NCODIC                                          
    AND NEW.NSOCIETE = OLD.NSOCIETE                                             
    AND NEW.NLIEU = OLD.NLIEU                                                   
    AND NEW.DEFFET = OLD.DEFFET                                                 
    AND ( NEW.DFINEFFET < SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )         
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
    AND ( OLD.DFINEFFET >= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PM' || NEW.NCODIC || NEW.NSOCIETE || NEW.NLIEU         
        || 'XX' , 'M' , ' ' , ' ' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER PDARTY.RJGF551                                                  
           AFTER INSERT                                                         
        ON PDARTY.RTGF55                                                        
        REFERENCING NEW AS "NEW"                                                
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTEC00 ( NCODIC , NSOCIETE , NLIEU )                             
 VALUES ( NEW.NCODIC , NEW.NSOCDEPOT , NEW.NDEPOT )                             
    ;                                                                           
CREATE  TRIGGER PDARTY.RJGF553                                                  
           AFTER DELETE                                                         
        ON PDARTY.RTGF55                                                        
        REFERENCING OLD AS "OLD"                                                
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTEC00 ( NCODIC , NSOCIETE , NLIEU )                             
 VALUES ( OLD.NCODIC , OLD.NSOCDEPOT , OLD.NDEPOT )                             
    ;                                                                           
CREATE  TRIGGER PDARTY.RJGF552                                                  
           AFTER UPDATE                                                         
        OF DCDE                                                                 
        , QCDE                                                                  
        , QCDERES                                                               
        ON PDARTY.RTGF55                                                        
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTEC00 ( NCODIC , NSOCIETE , NLIEU )                             
 VALUES ( OLD.NCODIC , OLD.NSOCDEPOT , OLD.NDEPOT )                             
    ;                                                                           
CREATE  TRIGGER PDARTY.RJSL111                                                  
           AFTER INSERT                                                         
        ON PDARTY.RTSL11                                                        
        REFERENCING NEW AS "NEW"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.CRSL = 'DSP'                                                 
    AND NEW.QUANTITE > 0 )                                                      
INSERT                                                                          
   INTO PDARTY.RTEC00 ( NCODIC , NSOCIETE , NLIEU )                             
 VALUES ( NEW.NCODIC , NEW.NSOCIETE , NEW.NLIEU )                               
    ;                                                                           
CREATE  TRIGGER PDARTY.RJSL112                                                  
           AFTER UPDATE                                                         
        OF QUANTITE                                                             
        ON PDARTY.RTSL11                                                        
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( OLD.CRSL = 'DSP' )                                               
INSERT                                                                          
   INTO PDARTY.RTEC00 ( NCODIC , NSOCIETE , NLIEU )                             
 VALUES ( OLD.NCODIC , OLD.NSOCIETE , OLD.NLIEU )                               
    ;                                                                           
CREATE  TRIGGER PDARTY.RJSL113                                                  
           AFTER DELETE                                                         
        ON PDARTY.RTSL11                                                        
        REFERENCING OLD AS "OLD"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( OLD.CRSL = 'DSP'                                                 
    AND OLD.QUANTITE > 0 )                                                      
INSERT                                                                          
   INTO PDARTY.RTEC00 ( NCODIC , NSOCIETE , NLIEU )                             
 VALUES ( OLD.NCODIC , OLD.NSOCIETE , OLD.NLIEU )                               
    ;                                                                           
CREATE  TRIGGER PDARTY.RJSL20I                                                  
           AFTER DELETE                                                         
        ON PDARTY.RTSL20                                                        
        REFERENCING OLD AS "O"                                                  
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTSL25                                                                  
 VALUES ( O.NSOCIETE , O.NLIEU , O.NVENTE , O.STATUT , O.INCOMPLETE ,           
        O.NBLIGNENS , O.DUPLIQUE , O.DATERESERVATION , O.PGMMAJ ,               
        O.DSYST , CURRENT TIMESTAMP )                                           
    ;                                                                           
CREATE  TRIGGER PDARTY.RJSL20U                                                  
           AFTER UPDATE                                                         
        ON PDARTY.RTSL20                                                        
        REFERENCING OLD AS "O"                                                  
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTSL25                                                                  
 VALUES ( O.NSOCIETE , O.NLIEU , O.NVENTE , O.STATUT , O.INCOMPLETE ,           
        O.NBLIGNENS , O.DUPLIQUE , O.DATERESERVATION , O.PGMMAJ ,               
        O.DSYST , CURRENT TIMESTAMP )                                           
    ;                                                                           
CREATE  TRIGGER PDARTY.RJSL21I                                                  
           AFTER DELETE                                                         
        ON PDARTY.RTSL21                                                        
        REFERENCING OLD AS "O"                                                  
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTSL26                                                                  
 VALUES ( O.NSOCIETE , O.NLIEU , O.NVENTE , O.NCODIC , O.NSEQNQ ,               
        O.WEMPORTE , O.QTE , O.QTEMANQUANTE , O.STATUT , O.DDELIV ,             
        O.PGMMAJ , O.DSYST , CURRENT TIMESTAMP )                                
    ;                                                                           
CREATE  TRIGGER PDARTY.RJSL21U                                                  
           AFTER UPDATE                                                         
        ON PDARTY.RTSL21                                                        
        REFERENCING OLD AS "O"                                                  
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTSL26                                                                  
 VALUES ( O.NSOCIETE , O.NLIEU , O.NVENTE , O.NCODIC , O.NSEQNQ ,               
        O.WEMPORTE , O.QTE , O.QTEMANQUANTE , O.STATUT , O.DDELIV ,             
        O.PGMMAJ , O.DSYST , CURRENT TIMESTAMP )                                
    ;                                                                           
CREATE  TRIGGER PDARTY.RJGS101                                                  
           AFTER INSERT                                                         
        ON PDARTY.RTGS10                                                        
        REFERENCING NEW AS "NEW"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.WSTOCKMASQ = 'N'                                             
    AND NEW.NSSLIEU = 'V' )                                                     
INSERT                                                                          
   INTO PDARTY.RTEC00 ( NCODIC , NSOCIETE , NLIEU )                             
 VALUES ( NEW.NCODIC , NEW.NSOCDEPOT , NEW.NDEPOT )                             
    ;                                                                           
CREATE  TRIGGER PDARTY.RJGS102                                                  
           AFTER UPDATE                                                         
        OF QSTOCK                                                               
        , QSTOCKRES                                                             
        ON PDARTY.RTGS10                                                        
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( OLD.WSTOCKMASQ = 'N'                                             
    AND OLD.NSSLIEU = 'V' )                                                     
INSERT                                                                          
   INTO PDARTY.RTEC00 ( NCODIC , NSOCIETE , NLIEU )                             
 VALUES ( OLD.NCODIC , OLD.NSOCDEPOT , OLD.NDEPOT )                             
    ;                                                                           
CREATE  TRIGGER PDARTY.RJGS103                                                  
           AFTER DELETE                                                         
        ON PDARTY.RTGS10                                                        
        REFERENCING OLD AS "OLD"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( OLD.WSTOCKMASQ = 'N'                                             
    AND OLD.NSSLIEU = 'V' )                                                     
INSERT                                                                          
   INTO PDARTY.RTEC00 ( NCODIC , NSOCIETE , NLIEU )                             
 VALUES ( OLD.NCODIC , OLD.NSOCDEPOT , OLD.NDEPOT )                             
    ;                                                                           
CREATE  TRIGGER PDARTY.RJFL40M                                                  
           AFTER UPDATE                                                         
        OF CASSORT                                                              
        , LSTATCOMP                                                             
        ON PDARTY.RTFL40                                                        
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( ( OLD.CASSORT <> NEW.CASSORT                                     
     OR OLD.LSTATCOMP <> NEW.LSTATCOMP )                                        
    AND OLD.NCODIC = NEW.NCODIC                                                 
    AND OLD.NSOCIETE = NEW.NSOCIETE )                                           
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'entreeStatut' , 'C' || NEW.NCODIC , 'M' , ' ' , '

 ' ,              
        CURRENT TIMESTAMP )                                                     
    ;                                                                           
CREATE  TRIGGER PDARTY.RJFL50M                                                  
           AFTER UPDATE                                                         
        OF CAPPRO                                                               
        , WSENSAPPRO                                                            
        ON PDARTY.RTFL50                                                        
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( ( OLD.CAPPRO <> NEW.CAPPRO                                       
     OR OLD.WSENSAPPRO <> NEW.WSENSAPPRO )                                      
    AND ( OLD.NSOCDEPOT = NEW.NSOCDEPOT )                                       
    AND ( OLD.NDEPOT = NEW.NDEPOT ) )                                           
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'entreeStatut' , 'C' || NEW.NCODIC , 'M' , ' ' , '

 ' ,              
        CURRENT TIMESTAMP )                                                     
    ;                                                                           
CREATE  TRIGGER PDARTY.GV11PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTGV11                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV11HP                                                         
 VALUES ( 'GV11D' || O.NSOCIETE || O.NLIEU || O.NVENTE || O.CTYPENREG ||        
        O.NCODICGRP || O.NCODIC || O.NSEQ || O.CMODDEL || O.DDELIV ||           
        O.NORDRE || O.CENREG || LEFT ( CHAR ( O.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( O.PVUNIT ) , 8 ) || RIGHT ( CHAR ( O.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( O.PVUNITF ) , 8 ) || RIGHT ( CHAR ( O.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( O.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        O.PVTOTAL ) , 2 ) || O.CEQUIPE || O.NLIGNE || LEFT ( CHAR (             
        O.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( O.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( O.QCONDT ) , 6 ) || LEFT ( CHAR ( O.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( O.PRMP ) , 2 ) || O.WEMPORTE || O.CPLAGE || O.CPROTOUR         
        || O.CADRTOUR || O.CPOSTAL || O.LCOMMENT || O.CVENDEUR ||               
        O.NSOCLIVR || O.NDEPOT || O.NAUTORM || O.WARTINEX || O.WEDITBL          
        || O.WACOMMUTER || O.WCQERESF || O.NMUTATION || O.CTOURNEE ||           
        O.WTOPELIVRE || O.DCOMPTA || O.DCREATION || O.HCREATION ||              
        O.DANNULATION || O.NLIEUORIG || O.WINTMAJ || LEFT ( CHAR (              
        O.DSYST ) , 14 ) || O.DSTAT || O.CPRESTGRP || O.NSOCMODIF ||            
        O.NLIEUMODIF || O.DATENC || O.CDEV || O.WUNITR || O.LRCMMT ||           
        O.NSOCP || O.NLIEUP || LEFT ( CHAR ( O.NTRANS ) , 9 ) ||                
        O.NSEQFV || LEFT ( CHAR ( O.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        O.NSEQREF ) , 6 ) || LEFT ( CHAR ( O.NMODIF ) , 6 ) ||                  
        O.NSOCORIG || O.DTOPE || O.NSOCLIVR1 || O.NDEPOT1 || O.NSOCGEST         
        || O.NLIEUGEST || O.NSOCDEPLIV || O.NLIEUDEPLIV || O.TYPVTE ||          
        O.CTYPENT || LEFT ( CHAR ( O.NLIEN ) , 6 ) || LEFT ( CHAR (             
        O.NACTVTE ) , 6 ) || LEFT ( CHAR ( O.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( O.PVCODIG ) , 2 ) || LEFT ( CHAR ( O.QTCODIG ) , 6 ) ||          
        O.DPRLG || O.CALERTE || O.CREPRISE || LEFT ( CHAR ( O.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( O.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        O.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER PDARTY.GV11PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTGV11                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV11HP                                                         
 VALUES ( 'GV11I' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.CTYPENREG ||        
        N.NCODICGRP || N.NCODIC || N.NSEQ || N.CMODDEL || N.DDELIV ||           
        N.NORDRE || N.CENREG || LEFT ( CHAR ( N.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( N.PVUNIT ) , 8 ) || RIGHT ( CHAR ( N.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( N.PVUNITF ) , 8 ) || RIGHT ( CHAR ( N.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( N.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        N.PVTOTAL ) , 2 ) || N.CEQUIPE || N.NLIGNE || LEFT ( CHAR (             
        N.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( N.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( N.QCONDT ) , 6 ) || LEFT ( CHAR ( N.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( N.PRMP ) , 2 ) || N.WEMPORTE || N.CPLAGE || N.CPROTOUR         
        || N.CADRTOUR || N.CPOSTAL || N.LCOMMENT || N.CVENDEUR ||               
        N.NSOCLIVR || N.NDEPOT || N.NAUTORM || N.WARTINEX || N.WEDITBL          
        || N.WACOMMUTER || N.WCQERESF || N.NMUTATION || N.CTOURNEE ||           
        N.WTOPELIVRE || N.DCOMPTA || N.DCREATION || N.HCREATION ||              
        N.DANNULATION || N.NLIEUORIG || N.WINTMAJ || LEFT ( CHAR (              
        N.DSYST ) , 14 ) || N.DSTAT || N.CPRESTGRP || N.NSOCMODIF ||            
        N.NLIEUMODIF || N.DATENC || N.CDEV || N.WUNITR || N.LRCMMT ||           
        N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 ) ||                
        N.NSEQFV || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        N.NSEQREF ) , 6 ) || LEFT ( CHAR ( N.NMODIF ) , 6 ) ||                  
        N.NSOCORIG || N.DTOPE || N.NSOCLIVR1 || N.NDEPOT1 || N.NSOCGEST         
        || N.NLIEUGEST || N.NSOCDEPLIV || N.NLIEUDEPLIV || N.TYPVTE ||          
        N.CTYPENT || LEFT ( CHAR ( N.NLIEN ) , 6 ) || LEFT ( CHAR (             
        N.NACTVTE ) , 6 ) || LEFT ( CHAR ( N.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( N.PVCODIG ) , 2 ) || LEFT ( CHAR ( N.QTCODIG ) , 6 ) ||          
        N.DPRLG || N.CALERTE || N.CREPRISE || LEFT ( CHAR ( N.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( N.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        N.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER PDARTY.GV11PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTGV11                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV11HP                                                         
 VALUES ( 'GV11U' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.CTYPENREG ||        
        N.NCODICGRP || N.NCODIC || N.NSEQ || N.CMODDEL || N.DDELIV ||           
        N.NORDRE || N.CENREG || LEFT ( CHAR ( N.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( N.PVUNIT ) , 8 ) || RIGHT ( CHAR ( N.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( N.PVUNITF ) , 8 ) || RIGHT ( CHAR ( N.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( N.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        N.PVTOTAL ) , 2 ) || N.CEQUIPE || N.NLIGNE || LEFT ( CHAR (             
        N.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( N.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( N.QCONDT ) , 6 ) || LEFT ( CHAR ( N.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( N.PRMP ) , 2 ) || N.WEMPORTE || N.CPLAGE || N.CPROTOUR         
        || N.CADRTOUR || N.CPOSTAL || N.LCOMMENT || N.CVENDEUR ||               
        N.NSOCLIVR || N.NDEPOT || N.NAUTORM || N.WARTINEX || N.WEDITBL          
        || N.WACOMMUTER || N.WCQERESF || N.NMUTATION || N.CTOURNEE ||           
        N.WTOPELIVRE || N.DCOMPTA || N.DCREATION || N.HCREATION ||              
        N.DANNULATION || N.NLIEUORIG || N.WINTMAJ || LEFT ( CHAR (              
        N.DSYST ) , 14 ) || N.DSTAT || N.CPRESTGRP || N.NSOCMODIF ||            
        N.NLIEUMODIF || N.DATENC || N.CDEV || N.WUNITR || N.LRCMMT ||           
        N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 ) ||                
        N.NSEQFV || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        N.NSEQREF ) , 6 ) || LEFT ( CHAR ( N.NMODIF ) , 6 ) ||                  
        N.NSOCORIG || N.DTOPE || N.NSOCLIVR1 || N.NDEPOT1 || N.NSOCGEST         
        || N.NLIEUGEST || N.NSOCDEPLIV || N.NLIEUDEPLIV || N.TYPVTE ||          
        N.CTYPENT || LEFT ( CHAR ( N.NLIEN ) , 6 ) || LEFT ( CHAR (             
        N.NACTVTE ) , 6 ) || LEFT ( CHAR ( N.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( N.PVCODIG ) , 2 ) || LEFT ( CHAR ( N.QTCODIG ) , 6 ) ||          
        N.DPRLG || N.CALERTE || N.CREPRISE || LEFT ( CHAR ( N.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( N.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        N.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER PDARTY.VE11PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTVE11                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE11HP                                                         
 VALUES ( 'VE11D' || O.NSOCIETE || O.NLIEU || O.NVENTE || O.CTYPENREG ||        
        O.NCODICGRP || O.NCODIC || O.NSEQ || O.CMODDEL || O.DDELIV ||           
        O.NORDRE || O.CENREG || LEFT ( CHAR ( O.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( O.PVUNIT ) , 8 ) || RIGHT ( CHAR ( O.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( O.PVUNITF ) , 8 ) || RIGHT ( CHAR ( O.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( O.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        O.PVTOTAL ) , 2 ) || O.CEQUIPE || O.NLIGNE || LEFT ( CHAR (             
        O.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( O.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( O.QCONDT ) , 6 ) || LEFT ( CHAR ( O.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( O.PRMP ) , 2 ) || O.WEMPORTE || O.CPLAGE || O.CPROTOUR         
        || O.CADRTOUR || O.CPOSTAL || O.LCOMMENT || O.CVENDEUR ||               
        O.NSOCLIVR || O.NDEPOT || O.NAUTORM || O.WARTINEX || O.WEDITBL          
        || O.WACOMMUTER || O.WCQERESF || O.NMUTATION || O.CTOURNEE ||           
        O.WTOPELIVRE || O.DCOMPTA || O.DCREATION || O.HCREATION ||              
        O.DANNULATION || O.NLIEUORIG || O.WINTMAJ || LEFT ( CHAR (              
        O.DSYST ) , 14 ) || O.DSTAT || O.CPRESTGRP || O.NSOCMODIF ||            
        O.NLIEUMODIF || O.DATENC || O.CDEV || O.WUNITR || O.LRCMMT ||           
        O.NSOCP || O.NLIEUP || LEFT ( CHAR ( O.NTRANS ) , 9 ) ||                
        O.NSEQFV || LEFT ( CHAR ( O.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        O.NSEQREF ) , 6 ) || LEFT ( CHAR ( O.NMODIF ) , 6 ) ||                  
        O.NSOCORIG || O.DTOPE || O.NSOCLIVR1 || O.NDEPOT1 || O.NSOCGEST         
        || O.NLIEUGEST || O.NSOCDEPLIV || O.NLIEUDEPLIV || O.TYPVTE ||          
        O.CTYPENT || LEFT ( CHAR ( O.NLIEN ) , 6 ) || LEFT ( CHAR (             
        O.NACTVTE ) , 6 ) || LEFT ( CHAR ( O.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( O.PVCODIG ) , 2 ) || LEFT ( CHAR ( O.QTCODIG ) , 6 ) ||          
        O.DPRLG || O.CALERTE || O.CREPRISE || LEFT ( CHAR ( O.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( O.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        O.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER PDARTY.VE11PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTVE11                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE11HP                                                         
 VALUES ( 'VE11I' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.CTYPENREG ||        
        N.NCODICGRP || N.NCODIC || N.NSEQ || N.CMODDEL || N.DDELIV ||           
        N.NORDRE || N.CENREG || LEFT ( CHAR ( N.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( N.PVUNIT ) , 8 ) || RIGHT ( CHAR ( N.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( N.PVUNITF ) , 8 ) || RIGHT ( CHAR ( N.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( N.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        N.PVTOTAL ) , 2 ) || N.CEQUIPE || N.NLIGNE || LEFT ( CHAR (             
        N.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( N.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( N.QCONDT ) , 6 ) || LEFT ( CHAR ( N.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( N.PRMP ) , 2 ) || N.WEMPORTE || N.CPLAGE || N.CPROTOUR         
        || N.CADRTOUR || N.CPOSTAL || N.LCOMMENT || N.CVENDEUR ||               
        N.NSOCLIVR || N.NDEPOT || N.NAUTORM || N.WARTINEX || N.WEDITBL          
        || N.WACOMMUTER || N.WCQERESF || N.NMUTATION || N.CTOURNEE ||           
        N.WTOPELIVRE || N.DCOMPTA || N.DCREATION || N.HCREATION ||              
        N.DANNULATION || N.NLIEUORIG || N.WINTMAJ || LEFT ( CHAR (              
        N.DSYST ) , 14 ) || N.DSTAT || N.CPRESTGRP || N.NSOCMODIF ||            
        N.NLIEUMODIF || N.DATENC || N.CDEV || N.WUNITR || N.LRCMMT ||           
        N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 ) ||                
        N.NSEQFV || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        N.NSEQREF ) , 6 ) || LEFT ( CHAR ( N.NMODIF ) , 6 ) ||                  
        N.NSOCORIG || N.DTOPE || N.NSOCLIVR1 || N.NDEPOT1 || N.NSOCGEST         
        || N.NLIEUGEST || N.NSOCDEPLIV || N.NLIEUDEPLIV || N.TYPVTE ||          
        N.CTYPENT || LEFT ( CHAR ( N.NLIEN ) , 6 ) || LEFT ( CHAR (             
        N.NACTVTE ) , 6 ) || LEFT ( CHAR ( N.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( N.PVCODIG ) , 2 ) || LEFT ( CHAR ( N.QTCODIG ) , 6 ) ||          
        N.DPRLG || N.CALERTE || N.CREPRISE || LEFT ( CHAR ( N.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( N.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        N.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER PDARTY.VE11PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTVE11                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE11HP                                                         
 VALUES ( 'VE11U' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.CTYPENREG ||        
        N.NCODICGRP || N.NCODIC || N.NSEQ || N.CMODDEL || N.DDELIV ||           
        N.NORDRE || N.CENREG || LEFT ( CHAR ( N.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( N.PVUNIT ) , 8 ) || RIGHT ( CHAR ( N.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( N.PVUNITF ) , 8 ) || RIGHT ( CHAR ( N.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( N.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        N.PVTOTAL ) , 2 ) || N.CEQUIPE || N.NLIGNE || LEFT ( CHAR (             
        N.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( N.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( N.QCONDT ) , 6 ) || LEFT ( CHAR ( N.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( N.PRMP ) , 2 ) || N.WEMPORTE || N.CPLAGE || N.CPROTOUR         
        || N.CADRTOUR || N.CPOSTAL || N.LCOMMENT || N.CVENDEUR ||               
        N.NSOCLIVR || N.NDEPOT || N.NAUTORM || N.WARTINEX || N.WEDITBL          
        || N.WACOMMUTER || N.WCQERESF || N.NMUTATION || N.CTOURNEE ||           
        N.WTOPELIVRE || N.DCOMPTA || N.DCREATION || N.HCREATION ||              
        N.DANNULATION || N.NLIEUORIG || N.WINTMAJ || LEFT ( CHAR (              
        N.DSYST ) , 14 ) || N.DSTAT || N.CPRESTGRP || N.NSOCMODIF ||            
        N.NLIEUMODIF || N.DATENC || N.CDEV || N.WUNITR || N.LRCMMT ||           
        N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 ) ||                
        N.NSEQFV || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        N.NSEQREF ) , 6 ) || LEFT ( CHAR ( N.NMODIF ) , 6 ) ||                  
        N.NSOCORIG || N.DTOPE || N.NSOCLIVR1 || N.NDEPOT1 || N.NSOCGEST         
        || N.NLIEUGEST || N.NSOCDEPLIV || N.NLIEUDEPLIV || N.TYPVTE ||          
        N.CTYPENT || LEFT ( CHAR ( N.NLIEN ) , 6 ) || LEFT ( CHAR (             
        N.NACTVTE ) , 6 ) || LEFT ( CHAR ( N.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( N.PVCODIG ) , 2 ) || LEFT ( CHAR ( N.QTCODIG ) , 6 ) ||          
        N.DPRLG || N.CALERTE || N.CREPRISE || LEFT ( CHAR ( N.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( N.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        N.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER PDARTY.GV10PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTGV10                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV10HP                                                         
 VALUES ( 'GV10D' || O.NSOCIETE || O.NLIEU || O.NVENTE || O.NORDRE ||           
        O.NCLIENT || O.DVENTE || O.DHVENTE || O.DMVENTE || LEFT ( CHAR (        
        O.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( O.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( O.PVERSE ) , 8 ) || RIGHT ( CHAR ( O.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( O.PCOMPT ) , 8 ) || RIGHT ( CHAR ( O.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( O.PLIVR ) , 8 ) || RIGHT ( CHAR ( O.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( O.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        O.PDIFFERE ) , 2 ) || LEFT ( CHAR ( O.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( O.PRFACT ) , 2 ) || O.CREMVTE || LEFT ( CHAR ( O.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( O.PREMVTE ) , 2 ) || O.LCOMVTE1 ||            
        O.LCOMVTE2 || O.LCOMVTE3 || O.LCOMVTE4 || O.DMODIFVTE ||                
        O.WFACTURE || O.WEXPORT || O.WDETAXEC || O.WDETAXEHC ||                 
        O.CORGORED || O.CMODPAIMTI || O.LDESCRIPTIF1 || O.LDESCRIPTIF2          
        || O.DLIVRBL || O.NFOLIOBL || O.LAUTORM || O.NAUTORD || LEFT (          
        CHAR ( O.DSYST ) , 14 ) || O.DSTAT || O.DFACTURE || O.CACID ||          
        O.NSOCMODIF || O.NLIEUMODIF || O.NSOCP || O.NLIEUP || O.CDEV ||         
        O.CFCRED || O.NCREDI || LEFT ( CHAR ( O.NSEQNQ ) , 6 ) ||               
        O.TYPVTE || O.VTEGPE || O.CTRMRQ || O.DATENC || O.WDGRAD ||             
        O.NAUTO || LEFT ( CHAR ( O.NSEQENS ) , 6 ) || O.NSOCO ||                
        O.NLIEUO || O.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER PDARTY.GV10PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTGV10                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV10HP                                                         
 VALUES ( 'GV10I' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.NORDRE ||           
        N.NCLIENT || N.DVENTE || N.DHVENTE || N.DMVENTE || LEFT ( CHAR (        
        N.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( N.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( N.PVERSE ) , 8 ) || RIGHT ( CHAR ( N.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( N.PCOMPT ) , 8 ) || RIGHT ( CHAR ( N.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( N.PLIVR ) , 8 ) || RIGHT ( CHAR ( N.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( N.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        N.PDIFFERE ) , 2 ) || LEFT ( CHAR ( N.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( N.PRFACT ) , 2 ) || N.CREMVTE || LEFT ( CHAR ( N.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREMVTE ) , 2 ) || N.LCOMVTE1 ||            
        N.LCOMVTE2 || N.LCOMVTE3 || N.LCOMVTE4 || N.DMODIFVTE ||                
        N.WFACTURE || N.WEXPORT || N.WDETAXEC || N.WDETAXEHC ||                 
        N.CORGORED || N.CMODPAIMTI || N.LDESCRIPTIF1 || N.LDESCRIPTIF2          
        || N.DLIVRBL || N.NFOLIOBL || N.LAUTORM || N.NAUTORD || LEFT (          
        CHAR ( N.DSYST ) , 14 ) || N.DSTAT || N.DFACTURE || N.CACID ||          
        N.NSOCMODIF || N.NLIEUMODIF || N.NSOCP || N.NLIEUP || N.CDEV ||         
        N.CFCRED || N.NCREDI || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) ||               
        N.TYPVTE || N.VTEGPE || N.CTRMRQ || N.DATENC || N.WDGRAD ||             
        N.NAUTO || LEFT ( CHAR ( N.NSEQENS ) , 6 ) || N.NSOCO ||                
        N.NLIEUO || N.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER PDARTY.GV10PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTGV10                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV10HP                                                         
 VALUES ( 'GV10U' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.NORDRE ||           
        N.NCLIENT || N.DVENTE || N.DHVENTE || N.DMVENTE || LEFT ( CHAR (        
        N.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( N.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( N.PVERSE ) , 8 ) || RIGHT ( CHAR ( N.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( N.PCOMPT ) , 8 ) || RIGHT ( CHAR ( N.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( N.PLIVR ) , 8 ) || RIGHT ( CHAR ( N.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( N.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        N.PDIFFERE ) , 2 ) || LEFT ( CHAR ( N.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( N.PRFACT ) , 2 ) || N.CREMVTE || LEFT ( CHAR ( N.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREMVTE ) , 2 ) || N.LCOMVTE1 ||            
        N.LCOMVTE2 || N.LCOMVTE3 || N.LCOMVTE4 || N.DMODIFVTE ||                
        N.WFACTURE || N.WEXPORT || N.WDETAXEC || N.WDETAXEHC ||                 
        N.CORGORED || N.CMODPAIMTI || N.LDESCRIPTIF1 || N.LDESCRIPTIF2          
        || N.DLIVRBL || N.NFOLIOBL || N.LAUTORM || N.NAUTORD || LEFT (          
        CHAR ( N.DSYST ) , 14 ) || N.DSTAT || N.DFACTURE || N.CACID ||          
        N.NSOCMODIF || N.NLIEUMODIF || N.NSOCP || N.NLIEUP || N.CDEV ||         
        N.CFCRED || N.NCREDI || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) ||               
        N.TYPVTE || N.VTEGPE || N.CTRMRQ || N.DATENC || N.WDGRAD ||             
        N.NAUTO || LEFT ( CHAR ( N.NSEQENS ) , 6 ) || N.NSOCO ||                
        N.NLIEUO || N.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER PDARTY.VE10PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTVE10                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE10HP                                                         
 VALUES ( 'VE10D' || O.NSOCIETE || O.NLIEU || O.NVENTE || O.NORDRE ||           
        O.NCLIENT || O.DVENTE || O.DHVENTE || O.DMVENTE || LEFT ( CHAR (        
        O.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( O.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( O.PVERSE ) , 8 ) || RIGHT ( CHAR ( O.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( O.PCOMPT ) , 8 ) || RIGHT ( CHAR ( O.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( O.PLIVR ) , 8 ) || RIGHT ( CHAR ( O.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( O.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        O.PDIFFERE ) , 2 ) || LEFT ( CHAR ( O.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( O.PRFACT ) , 2 ) || O.CREMVTE || LEFT ( CHAR ( O.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( O.PREMVTE ) , 2 ) || O.LCOMVTE1 ||            
        O.LCOMVTE2 || O.LCOMVTE3 || O.LCOMVTE4 || O.DMODIFVTE ||                
        O.WFACTURE || O.WEXPORT || O.WDETAXEC || O.WDETAXEHC ||                 
        O.CORGORED || O.CMODPAIMTI || O.LDESCRIPTIF1 || O.LDESCRIPTIF2          
        || O.DLIVRBL || O.NFOLIOBL || O.LAUTORM || O.NAUTORD || LEFT (          
        CHAR ( O.DSYST ) , 14 ) || O.DSTAT || O.DFACTURE || O.CACID ||          
        O.NSOCMODIF || O.NLIEUMODIF || O.NSOCP || O.NLIEUP || O.CDEV ||         
        O.CFCRED || O.NCREDI || LEFT ( CHAR ( O.NSEQNQ ) , 6 ) ||               
        O.TYPVTE || O.VTEGPE || O.CTRMRQ || O.DATENC || O.WDGRAD ||             
        O.NAUTO || LEFT ( CHAR ( O.NSEQENS ) , 6 ) || O.NSOCO ||                
        O.NLIEUO || O.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER PDARTY.VE10PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTVE10                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE10HP                                                         
 VALUES ( 'VE10I' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.NORDRE ||           
        N.NCLIENT || N.DVENTE || N.DHVENTE || N.DMVENTE || LEFT ( CHAR (        
        N.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( N.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( N.PVERSE ) , 8 ) || RIGHT ( CHAR ( N.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( N.PCOMPT ) , 8 ) || RIGHT ( CHAR ( N.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( N.PLIVR ) , 8 ) || RIGHT ( CHAR ( N.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( N.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        N.PDIFFERE ) , 2 ) || LEFT ( CHAR ( N.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( N.PRFACT ) , 2 ) || N.CREMVTE || LEFT ( CHAR ( N.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREMVTE ) , 2 ) || N.LCOMVTE1 ||            
        N.LCOMVTE2 || N.LCOMVTE3 || N.LCOMVTE4 || N.DMODIFVTE ||                
        N.WFACTURE || N.WEXPORT || N.WDETAXEC || N.WDETAXEHC ||                 
        N.CORGORED || N.CMODPAIMTI || N.LDESCRIPTIF1 || N.LDESCRIPTIF2          
        || N.DLIVRBL || N.NFOLIOBL || N.LAUTORM || N.NAUTORD || LEFT (          
        CHAR ( N.DSYST ) , 14 ) || N.DSTAT || N.DFACTURE || N.CACID ||          
        N.NSOCMODIF || N.NLIEUMODIF || N.NSOCP || N.NLIEUP || N.CDEV ||         
        N.CFCRED || N.NCREDI || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) ||               
        N.TYPVTE || N.VTEGPE || N.CTRMRQ || N.DATENC || N.WDGRAD ||             
        N.NAUTO || LEFT ( CHAR ( N.NSEQENS ) , 6 ) || N.NSOCO ||                
        N.NLIEUO || N.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER PDARTY.VE10PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTVE10                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE10HP                                                         
 VALUES ( 'VE10U' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.NORDRE ||           
        N.NCLIENT || N.DVENTE || N.DHVENTE || N.DMVENTE || LEFT ( CHAR (        
        N.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( N.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( N.PVERSE ) , 8 ) || RIGHT ( CHAR ( N.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( N.PCOMPT ) , 8 ) || RIGHT ( CHAR ( N.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( N.PLIVR ) , 8 ) || RIGHT ( CHAR ( N.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( N.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        N.PDIFFERE ) , 2 ) || LEFT ( CHAR ( N.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( N.PRFACT ) , 2 ) || N.CREMVTE || LEFT ( CHAR ( N.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREMVTE ) , 2 ) || N.LCOMVTE1 ||            
        N.LCOMVTE2 || N.LCOMVTE3 || N.LCOMVTE4 || N.DMODIFVTE ||                
        N.WFACTURE || N.WEXPORT || N.WDETAXEC || N.WDETAXEHC ||                 
        N.CORGORED || N.CMODPAIMTI || N.LDESCRIPTIF1 || N.LDESCRIPTIF2          
        || N.DLIVRBL || N.NFOLIOBL || N.LAUTORM || N.NAUTORD || LEFT (          
        CHAR ( N.DSYST ) , 14 ) || N.DSTAT || N.DFACTURE || N.CACID ||          
        N.NSOCMODIF || N.NLIEUMODIF || N.NSOCP || N.NLIEUP || N.CDEV ||         
        N.CFCRED || N.NCREDI || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) ||               
        N.TYPVTE || N.VTEGPE || N.CTRMRQ || N.DATENC || N.WDGRAD ||             
        N.NAUTO || LEFT ( CHAR ( N.NSEQENS ) , 6 ) || N.NSOCO ||                
        N.NLIEUO || N.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER PDARTY.GV02PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTGV02                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV02HP                                                         
 VALUES ( 'GV02D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.WTYPEADR || O.CTITRENOM || O.LNOM || O.LPRENOM || O.LBATIMENT         
        || O.LESCALIER || O.LETAGE || O.LPORTE || O.LCMPAD1 || O.LCMPAD2        
        || O.CVOIE || O.CTVOIE || O.LNOMVOIE || O.LCOMMUNE || O.CPOSTAL         
        || O.LBUREAU || O.TELDOM || O.TELBUR || O.LPOSTEBUR ||                  
        O.LCOMLIV1 || O.LCOMLIV2 || O.WETRANGER || O.CZONLIV || O.CINSEE        
        || O.WCONTRA || LEFT ( CHAR ( O.DSYST ) , 14 ) || O.CILOT ||            
        O.IDCLIENT || O.CPAYS || O.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER PDARTY.GV02PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTGV02                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV02HP                                                         
 VALUES ( 'GV02I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.CTITRENOM || N.LNOM || N.LPRENOM || N.LBATIMENT         
        || N.LESCALIER || N.LETAGE || N.LPORTE || N.LCMPAD1 || N.LCMPAD2        
        || N.CVOIE || N.CTVOIE || N.LNOMVOIE || N.LCOMMUNE || N.CPOSTAL         
        || N.LBUREAU || N.TELDOM || N.TELBUR || N.LPOSTEBUR ||                  
        N.LCOMLIV1 || N.LCOMLIV2 || N.WETRANGER || N.CZONLIV || N.CINSEE        
        || N.WCONTRA || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.CILOT ||            
        N.IDCLIENT || N.CPAYS || N.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER PDARTY.GV02PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTGV02                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV02HP                                                         
 VALUES ( 'GV02U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.CTITRENOM || N.LNOM || N.LPRENOM || N.LBATIMENT         
        || N.LESCALIER || N.LETAGE || N.LPORTE || N.LCMPAD1 || N.LCMPAD2        
        || N.CVOIE || N.CTVOIE || N.LNOMVOIE || N.LCOMMUNE || N.CPOSTAL         
        || N.LBUREAU || N.TELDOM || N.TELBUR || N.LPOSTEBUR ||                  
        N.LCOMLIV1 || N.LCOMLIV2 || N.WETRANGER || N.CZONLIV || N.CINSEE        
        || N.WCONTRA || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.CILOT ||            
        N.IDCLIENT || N.CPAYS || N.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER PDARTY.GV03PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTGV03                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV03HP                                                         
 VALUES ( 'GV03D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.WTYPEADR || O.EMAIL || LEFT ( CHAR ( O.DSYST ) , 14 ) ||              
        O.CPORTE || O.WFLAG || O.WASC || O.WADPRP , 'N' , CURRENT               
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER PDARTY.GV03PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTGV03                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV03HP                                                         
 VALUES ( 'GV03I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.EMAIL || LEFT ( CHAR ( N.DSYST ) , 14 ) ||              
        N.CPORTE || N.WFLAG || N.WASC || N.WADPRP , 'N' , CURRENT               
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER PDARTY.GV03PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTGV03                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV03HP                                                         
 VALUES ( 'GV03U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.EMAIL || LEFT ( CHAR ( N.DSYST ) , 14 ) ||              
        N.CPORTE || N.WFLAG || N.WASC || N.WADPRP , 'N' , CURRENT               
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER PDARTY.GV04PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTGV04                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV04HP                                                         
 VALUES ( 'GV04D' || O.NSOCIETE || O.NLIEU || O.NVENTE || O.DCREATION ||        
        O.DMAJ || O.WACCES || O.WREPRISE || LEFT ( CHAR ( O.NBPRDREP ) ,        
        3 ) || O.DATENAISS || O.CPNAISS || O.LIEUNAISS || O.WBTOB ||            
        O.WARF || LEFT ( CHAR ( O.DSYST ) , 14 ) || O.WASCUTIL ||               
        O.WMARCHES || O.WCOL || O.WSDEP || O.WFLAG , 'N' , CURRENT              
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER PDARTY.GV04PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTGV04                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV04HP                                                         
 VALUES ( 'GV04I' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.DCREATION ||        
        N.DMAJ || N.WACCES || N.WREPRISE || LEFT ( CHAR ( N.NBPRDREP ) ,        
        3 ) || N.DATENAISS || N.CPNAISS || N.LIEUNAISS || N.WBTOB ||            
        N.WARF || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.WASCUTIL ||               
        N.WMARCHES || N.WCOL || N.WSDEP || N.WFLAG , 'N' , CURRENT              
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER PDARTY.GV04PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTGV04                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV04HP                                                         
 VALUES ( 'GV04U' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.DCREATION ||        
        N.DMAJ || N.WACCES || N.WREPRISE || LEFT ( CHAR ( N.NBPRDREP ) ,        
        3 ) || N.DATENAISS || N.CPNAISS || N.LIEUNAISS || N.WBTOB ||            
        N.WARF || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.WASCUTIL ||               
        N.WMARCHES || N.WCOL || N.WSDEP || N.WFLAG , 'N' , CURRENT              
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER PDARTY.GV14PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTGV14                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV14HP                                                         
 VALUES ( 'GV14D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.DSAISIE || O.NSEQ || O.CMODPAIMT || LEFT ( CHAR ( O.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( O.PREGLTVTE ) , 2 ) || O.NREGLTVTE ||         
        O.DREGLTVTE || O.WREGLTVTE || O.DCOMPTA || LEFT ( CHAR ( O.DSYST        
        ) , 14 ) || O.CPROTOUR || O.CTOURNEE || O.CDEVISE || LEFT ( CHAR        
        ( O.MDEVISE ) , 8 ) || RIGHT ( CHAR ( O.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( O.MECART ) , 2 ) || RIGHT ( CHAR ( O.MECART ) , 2 ) ||         
        O.CDEV || O.NSOCP || O.NLIEUP || LEFT ( CHAR ( O.NTRANS ) , 9 )         
        || LEFT ( CHAR ( O.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        O.PMODPAIMT ) , 2 ) || O.CVENDEUR || O.CFCRED || O.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
CREATE  TRIGGER PDARTY.GV14PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTGV14                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV14HP                                                         
 VALUES ( 'GV14I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.DSAISIE || N.NSEQ || N.CMODPAIMT || LEFT ( CHAR ( N.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREGLTVTE ) , 2 ) || N.NREGLTVTE ||         
        N.DREGLTVTE || N.WREGLTVTE || N.DCOMPTA || LEFT ( CHAR ( N.DSYST        
        ) , 14 ) || N.CPROTOUR || N.CTOURNEE || N.CDEVISE || LEFT ( CHAR        
        ( N.MDEVISE ) , 8 ) || RIGHT ( CHAR ( N.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( N.MECART ) , 2 ) || RIGHT ( CHAR ( N.MECART ) , 2 ) ||         
        N.CDEV || N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 )         
        || LEFT ( CHAR ( N.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        N.PMODPAIMT ) , 2 ) || N.CVENDEUR || N.CFCRED || N.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
CREATE  TRIGGER PDARTY.GV14PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTGV14                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV14HP                                                         
 VALUES ( 'GV14U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.DSAISIE || N.NSEQ || N.CMODPAIMT || LEFT ( CHAR ( N.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREGLTVTE ) , 2 ) || N.NREGLTVTE ||         
        N.DREGLTVTE || N.WREGLTVTE || N.DCOMPTA || LEFT ( CHAR ( N.DSYST        
        ) , 14 ) || N.CPROTOUR || N.CTOURNEE || N.CDEVISE || LEFT ( CHAR        
        ( N.MDEVISE ) , 8 ) || RIGHT ( CHAR ( N.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( N.MECART ) , 2 ) || RIGHT ( CHAR ( N.MECART ) , 2 ) ||         
        N.CDEV || N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 )         
        || LEFT ( CHAR ( N.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        N.PMODPAIMT ) , 2 ) || N.CVENDEUR || N.CFCRED || N.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
CREATE  TRIGGER PDARTY.GV15PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTGV15                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV15HP                                                         
 VALUES ( 'GV15D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.NCODIC || O.NCODICGRP || O.NSEQ || O.CARACTSPE1 ||                    
        O.CVCARACTSPE1 || O.CARACTSPE2 || O.CVCARACTSPE2 || O.CARACTSPE3        
        || O.CVCARACTSPE3 || O.CARACTSPE4 || O.CVCARACTSPE4 ||                  
        O.CARACTSPE5 || O.CVCARACTSPE5 || O.WANNULCAR || LEFT ( CHAR (          
        O.DSYST ) , 14 ) , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER PDARTY.GV15PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTGV15                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV15HP                                                         
 VALUES ( 'GV15I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.NCODIC || N.NCODICGRP || N.NSEQ || N.CARACTSPE1 ||                    
        N.CVCARACTSPE1 || N.CARACTSPE2 || N.CVCARACTSPE2 || N.CARACTSPE3        
        || N.CVCARACTSPE3 || N.CARACTSPE4 || N.CVCARACTSPE4 ||                  
        N.CARACTSPE5 || N.CVCARACTSPE5 || N.WANNULCAR || LEFT ( CHAR (          
        N.DSYST ) , 14 ) , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER PDARTY.GV15PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTGV15                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV15HP                                                         
 VALUES ( 'GV15U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.NCODIC || N.NCODICGRP || N.NSEQ || N.CARACTSPE1 ||                    
        N.CVCARACTSPE1 || N.CARACTSPE2 || N.CVCARACTSPE2 || N.CARACTSPE3        
        || N.CVCARACTSPE3 || N.CARACTSPE4 || N.CVCARACTSPE4 ||                  
        N.CARACTSPE5 || N.CVCARACTSPE5 || N.WANNULCAR || LEFT ( CHAR (          
        N.DSYST ) , 14 ) , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER PDARTY.TL04PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTTL04                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTTL04HP                                                         
 VALUES ( 'TL04D' || O.NSOC || O.NMAG || O.NVENTE || O.NCODICGRP ||             
        O.NCODIC || O.NSEQ || O.DDELIV || O.CPROTOUR || O.CADRTOUR ||           
        O.CTOURNEE || O.WRECYCL || O.CRETOUR || LEFT ( CHAR ( O.QTE ) ,         
        6 ) || LEFT ( CHAR ( O.QCDEE ) , 6 ) || LEFT ( CHAR ( O.DSYST )         
        , 14 ) || O.DMVT , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER PDARTY.TL04PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTTL04                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTTL04HP                                                         
 VALUES ( 'TL04I' || N.NSOC || N.NMAG || N.NVENTE || N.NCODICGRP ||             
        N.NCODIC || N.NSEQ || N.DDELIV || N.CPROTOUR || N.CADRTOUR ||           
        N.CTOURNEE || N.WRECYCL || N.CRETOUR || LEFT ( CHAR ( N.QTE ) ,         
        6 ) || LEFT ( CHAR ( N.QCDEE ) , 6 ) || LEFT ( CHAR ( N.DSYST )         
        , 14 ) || N.DMVT , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER PDARTY.TL04PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTTL04                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTTL04HP                                                         
 VALUES ( 'TL04U' || N.NSOC || N.NMAG || N.NVENTE || N.NCODICGRP ||             
        N.NCODIC || N.NSEQ || N.DDELIV || N.CPROTOUR || N.CADRTOUR ||           
        N.CTOURNEE || N.WRECYCL || N.CRETOUR || LEFT ( CHAR ( N.QTE ) ,         
        6 ) || LEFT ( CHAR ( N.QCDEE ) , 6 ) || LEFT ( CHAR ( N.DSYST )         
        , 14 ) || N.DMVT , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER PDARTY.VE02PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTVE02                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE02HP                                                         
 VALUES ( 'VE02D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.WTYPEADR || O.CTITRENOM || O.LNOM || O.LPRENOM || O.LBATIMENT         
        || O.LESCALIER || O.LETAGE || O.LPORTE || O.LCMPAD1 || O.LCMPAD2        
        || O.CVOIE || O.CTVOIE || O.LNOMVOIE || O.LCOMMUNE || O.CPOSTAL         
        || O.LBUREAU || O.TELDOM || O.TELBUR || O.LPOSTEBUR ||                  
        O.LCOMLIV1 || O.LCOMLIV2 || O.WETRANGER || O.CZONLIV || O.CINSEE        
        || O.WCONTRA || LEFT ( CHAR ( O.DSYST ) , 14 ) || O.CILOT ||            
        O.IDCLIENT || O.CPAYS || O.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER PDARTY.VE02PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTVE02                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE02HP                                                         
 VALUES ( 'VE02I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.CTITRENOM || N.LNOM || N.LPRENOM || N.LBATIMENT         
        || N.LESCALIER || N.LETAGE || N.LPORTE || N.LCMPAD1 || N.LCMPAD2        
        || N.CVOIE || N.CTVOIE || N.LNOMVOIE || N.LCOMMUNE || N.CPOSTAL         
        || N.LBUREAU || N.TELDOM || N.TELBUR || N.LPOSTEBUR ||                  
        N.LCOMLIV1 || N.LCOMLIV2 || N.WETRANGER || N.CZONLIV || N.CINSEE        
        || N.WCONTRA || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.CILOT ||            
        N.IDCLIENT || N.CPAYS || N.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER PDARTY.VE02PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTVE02                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE02HP                                                         
 VALUES ( 'VE02U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.CTITRENOM || N.LNOM || N.LPRENOM || N.LBATIMENT         
        || N.LESCALIER || N.LETAGE || N.LPORTE || N.LCMPAD1 || N.LCMPAD2        
        || N.CVOIE || N.CTVOIE || N.LNOMVOIE || N.LCOMMUNE || N.CPOSTAL         
        || N.LBUREAU || N.TELDOM || N.TELBUR || N.LPOSTEBUR ||                  
        N.LCOMLIV1 || N.LCOMLIV2 || N.WETRANGER || N.CZONLIV || N.CINSEE        
        || N.WCONTRA || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.CILOT ||            
        N.IDCLIENT || N.CPAYS || N.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER PDARTY.VE14PD_H                                                 
           AFTER DELETE                                                         
        ON PDARTY.RTVE14                                                        
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE14HP                                                         
 VALUES ( 'VE14D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.DSAISIE || O.NSEQ || O.CMODPAIMT || LEFT ( CHAR ( O.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( O.PREGLTVTE ) , 2 ) || O.NREGLTVTE ||         
        O.DREGLTVTE || O.WREGLTVTE || O.DCOMPTA || LEFT ( CHAR ( O.DSYST        
        ) , 14 ) || O.CPROTOUR || O.CTOURNEE || O.CDEVISE || LEFT ( CHAR        
        ( O.MDEVISE ) , 8 ) || RIGHT ( CHAR ( O.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( O.MECART ) , 2 ) || RIGHT ( CHAR ( O.MECART ) , 2 ) ||         
        O.CDEV || O.NSOCP || O.NLIEUP || LEFT ( CHAR ( O.NTRANS ) , 9 )         
        || LEFT ( CHAR ( O.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        O.PMODPAIMT ) , 2 ) || O.CVENDEUR || O.CFCRED || O.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
CREATE  TRIGGER PDARTY.VE14PI_H                                                 
           AFTER INSERT                                                         
        ON PDARTY.RTVE14                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE14HP                                                         
 VALUES ( 'VE14I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.DSAISIE || N.NSEQ || N.CMODPAIMT || LEFT ( CHAR ( N.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREGLTVTE ) , 2 ) || N.NREGLTVTE ||         
        N.DREGLTVTE || N.WREGLTVTE || N.DCOMPTA || LEFT ( CHAR ( N.DSYST        
        ) , 14 ) || N.CPROTOUR || N.CTOURNEE || N.CDEVISE || LEFT ( CHAR        
        ( N.MDEVISE ) , 8 ) || RIGHT ( CHAR ( N.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( N.MECART ) , 2 ) || RIGHT ( CHAR ( N.MECART ) , 2 ) ||         
        N.CDEV || N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 )         
        || LEFT ( CHAR ( N.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        N.PMODPAIMT ) , 2 ) || N.CVENDEUR || N.CFCRED || N.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
CREATE  TRIGGER PDARTY.VE14PU_H                                                 
           AFTER UPDATE                                                         
        ON PDARTY.RTVE14                                                        
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE14HP                                                         
 VALUES ( 'VE14U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.DSAISIE || N.NSEQ || N.CMODPAIMT || LEFT ( CHAR ( N.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREGLTVTE ) , 2 ) || N.NREGLTVTE ||         
        N.DREGLTVTE || N.WREGLTVTE || N.DCOMPTA || LEFT ( CHAR ( N.DSYST        
        ) , 14 ) || N.CPROTOUR || N.CTOURNEE || N.CDEVISE || LEFT ( CHAR        
        ( N.MDEVISE ) , 8 ) || RIGHT ( CHAR ( N.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( N.MECART ) , 2 ) || RIGHT ( CHAR ( N.MECART ) , 2 ) ||         
        N.CDEV || N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 )         
        || LEFT ( CHAR ( N.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        N.PMODPAIMT ) , 2 ) || N.CVENDEUR || N.CFCRED || N.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
