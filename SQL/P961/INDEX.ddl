--MW CREATE  UNIQUE INDEX P961.RXIGSA0R ON P961.RTIGSA                               
--MW         ( DIGFA0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P961.RXIGSB0R ON P961.RTIGSB                               
--MW         ( DIGFA0 ASC                                                            
--MW         , NT_DIGSB ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P961.RXIGSC0R ON P961.RTIGSC                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFC0 ASC                                                            
--MW         , SM_DIGSC ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P961.RXIGSD0R ON P961.RTIGSD                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFD0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P961.RXIGSE0R ON P961.RTIGSE                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFE0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P961.RXIGSF0R ON P961.RTIGSF                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFF0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
CREATE  INDEX P961.RXAI241L ON P961.RTAI24                                      
        ( FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXAI531L ON P961.RTAI53                                      
        ( FSSKU ASC                                                             
        , FSFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXAN000L ON P961.RTAN00                                      
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXAN001L ON P961.RTAN00                                      
        ( ENRID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXBA001L ON P961.RTBA00                                      
        ( NFACTREM ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXBA011L ON P961.RTBA01                                      
        ( CTIERSBA ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXBA012L ON P961.RTBA01                                      
        ( NCHRONO ASC                                                           
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXBA013L ON P961.RTBA01                                      
        ( DEMIS DESC                                                            
        , WANNUL ASC                                                            
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXBA014L ON P961.RTBA01                                      
        ( DEMIS ASC                                                             
        , WANNUL ASC                                                            
        , NBA DESC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXBA015L ON P961.RTBA01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXBA101L ON P961.RTBA10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NECART ASC                                                            
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXBA201L ON P961.RTBA20                                      
        ( NTIERSCV ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXCC031L ON P961.RTCC03                                      
        ( MOPAI ASC                                                             
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , TYPTRANS ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXCC041L ON P961.RTCC04                                      
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXCD301L ON P961.RTCD30                                      
        ( NCDE ASC                                                              
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXCD302L ON P961.RTCD30                                      
        ( NMAG ASC                                                              
        , NCODIC ASC                                                            
        , DREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXCD303L ON P961.RTCD30                                      
        ( NMAG ASC                                                              
        , NREC ASC                                                              
        , DVALIDATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXCE101L ON P961.RTCE10                                      
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , WETIQUETTE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXCE102L ON P961.RTCE10                                      
        ( NZONPRIX ASC                                                          
        , WETIQUETTE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXCE103L ON P961.RTCE10                                      
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXCE104L ON P961.RTCE10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXDC001L ON P961.RTDC00                                      
        ( NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXDC101L ON P961.RTDC10                                      
        ( NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXEG051L ON P961.RTEG05                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMPC ASC                                                         
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXEG101L ON P961.RTEG10                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXEG102L ON P961.RTEG10                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXEG103L ON P961.RTEG10                                      
        ( NOMETAT ASC                                                           
        , CHCOND1 ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXEG104L ON P961.RTEG10                                      
        ( NOMETAT ASC                                                           
        , CHCOND2 ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXEG900L ON P961.RTEG90                                      
        ( IDENTTS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXEXFA0L ON P961.RTEXFA                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE3 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXEXFD0L ON P961.RTEXFD                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXEX000L ON P961.RTEX00                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXEX100L ON P961.RTEX10                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXEX200L ON P961.RTEX20                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXEX300L ON P961.RTEX30                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXEX400L ON P961.RTEX40                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXEX500L ON P961.RTEX50                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXEX600L ON P961.RTEX60                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXEX700L ON P961.RTEX70                                      
        ( NTRI ASC                                                              
        , LRUPTURE2 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXEX900L ON P961.RTEX90                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXFF001L ON P961.RTFF00                                      
        ( NTIERS ASC                                                            
        , WANN ASC                                                              
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFF002L ON P961.RTFF00                                      
        ( DEVN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFF051L ON P961.RTFF05                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFF101L ON P961.RTFF10                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFF102L ON P961.RTFF10                                      
        ( NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFF111L ON P961.RTFF11                                      
        ( NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFF121L ON P961.RTFF12                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFF131L ON P961.RTFF13                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFF151L ON P961.RTFF15                                      
        ( NPIECEFAC ASC                                                         
        , DEXCPTFAC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFF860L ON P961.RTFF86                                      
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXFF870L ON P961.RTFF87                                      
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
--MW CREATE  INDEX P961.RXFF880L ON P961.RTFF88                                      
--MW        ( PREFT1 ASC                                                            
--MW        , DEXCPT ASC                                                            
--MW        , NPIECE ASC                                                            
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  INDEX P961.RXFF881L ON P961.RTFF88                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXFI101L ON P961.RTFI10                                      
        ( NMUTATION ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXFI102L ON P961.RTFI10                                      
        ( DCOMPTA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFM101L ON P961.RTFM10                                      
        ( WCOLLECTIF ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT121L ON P961.RTFT12                                      
        ( NJRN ASC                                                              
        , NEXER ASC                                                             
        , NSOC ASC                                                              
        , NETAB ASC                                                             
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT122L ON P961.RTFT12                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT131L ON P961.RTFT13                                      
        ( NREG ASC                                                              
        , NORDRE ASC                                                            
        , NPROPO ASC                                                            
        , NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT133L ON P961.RTFT13                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT134L ON P961.RTFT13                                      
        ( NREG ASC                                                              
        , NORDRE ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT141L ON P961.RTFT14                                      
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXFT211L ON P961.RTFT21                                      
        ( LETTRAGE DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT212L ON P961.RTFT21                                      
        ( COMPTE ASC                                                            
        , NSOC ASC                                                              
        , LETTRAGE ASC                                                          
        , CDEVISE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT214L ON P961.RTFT21                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT221L ON P961.RTFT22                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , SECTION ASC                                                           
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
        , MONTANT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT222L ON P961.RTFT22                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , RUBRIQUE ASC                                                          
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
        , MONTANT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT261L ON P961.RTFT26                                      
        ( BAN ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT271L ON P961.RTFT27                                      
        ( NTBENEF ASC                                                           
        , NFOUR ASC                                                             
        , CTRESO ASC                                                            
        , CDEVISE ASC                                                           
        , METHREG ASC                                                           
        , NAUX ASC                                                              
        , NAUXR ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT272L ON P961.RTFT27                                      
        ( NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXFT310L ON P961.RTFT31                                      
        ( SECTION ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXFT311 ON P961.RTFT31                                       
        ( RUBRIQUE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT312L ON P961.RTFT31                                      
        ( SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT313L ON P961.RTFT31                                      
        ( RUBRIQUE ASC                                                          
        , SECTION ASC                                                           
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXFT760L ON P961.RTFT76                                      
        ( DFACTURE ASC                                                          
        , NFACTURE ASC                                                          
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXFT761L ON P961.RTFT76                                      
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA001L ON P961.RTGA00                                      
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA002L ON P961.RTGA00                                      
        ( CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA003L ON P961.RTGA00                                      
        ( CMARQ ASC                                                             
        , LEMBALLAGE ASC                                                        
        , NCODIC ASC                                                            
        , CASSORT ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA004L ON P961.RTGA00                                      
        ( NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA005L ON P961.RTGA00                                      
        ( NCODIC ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA006L ON P961.RTGA00                                      
        ( CASSORT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA007L ON P961.RTGA00                                      
        ( CFAM ASC                                                              
        , CASSORT ASC                                                           
        , CAPPRO ASC                                                            
        , NCODIC ASC                                                            
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA008L ON P961.RTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA009L ON P961.RTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CASSORT ASC                                                           
        , D1RECEPT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA011A ON P961.RTGA01LA                                    
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA011J ON P961.RTGA01LJ                                    
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA011L ON P961.RTGA01                                      
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA031L ON P961.RTGA03                                      
        ( NCODICK ASC                                                           
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA101L ON P961.RTGA10                                      
        ( NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA102L ON P961.RTGA10                                      
        ( CTYPLIEU ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA103L ON P961.RTGA10                                      
        ( CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA120L ON P961.RTGA12                                      
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , CMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA141L ON P961.RTGA14                                      
        ( WSEQFAM ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA213L ON P961.RTGA21                                      
        ( WRAYONFAM ASC                                                         
        , CRAYONFAM ASC                                                         
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA251L ON P961.RTGA25                                      
        ( CMARKETING ASC                                                        
        , CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA302L ON P961.RTGA30                                      
        ( CPARAM ASC                                                            
        , LVPARAM ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA431L ON P961.RTGA43                                      
        ( CINSEE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXGA490L ON P961.RTGA49                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXGA561L ON P961.RTGA56                                      
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA581L ON P961.RTGA58                                      
        ( NCODICLIE ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA591L ON P961.RTGA59                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA592L ON P961.RTGA59                                      
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA621L ON P961.RTGA62                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA651L ON P961.RTGA65                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA652L ON P961.RTGA65                                      
        ( CSTATUT ASC                                                           
        , NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA661L ON P961.RTGA66                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA681L ON P961.RTGA68                                      
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA751L ON P961.RTGA75                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGA752L ON P961.RTGA75                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGB301L ON P961.RTGB30                                      
        ( NCODIC ASC                                                            
        , NMUTATION ASC                                                         
        , NLIEUENT ASC                                                          
        , NHS ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGB801L ON P961.RTGB80                                      
        ( NCODIC ASC                                                            
        , NMUTATION ASC                                                         
        , NLIEUENT ASC                                                          
        , NHS ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGG151L ON P961.RTGG15                                      
        ( NCODIC ASC                                                            
        , DDECISION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGG170L ON P961.RTGG17                                      
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DRELEVE ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXGG201L ON P961.RTGG20                                      
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGG202L ON P961.RTGG20                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DFINEFFET ASC                                                         
        , DEFFET DESC                                                           
        , PEXPTTC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGG203L ON P961.RTGG20                                      
        ( DFINEFFET ASC                                                         
        , DEFFET ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGG204L ON P961.RTGG20                                      
        ( NLIEU ASC                                                             
        , DFINEFFET ASC                                                         
        , DEFFET ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGG210L ON P961.RTGG21                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGG402L ON P961.RTGG40                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGG403L ON P961.RTGG40                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
        , DFINEFFET ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGG601L ON P961.RTGG60                                      
        ( WTRAITE ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXGG701L ON P961.RTGG70                                      
        ( NCODIC ASC                                                            
        , DREC DESC                                                             
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXGG702L ON P961.RTGG70                                      
        ( DREC DESC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXGP040L ON P961.RTGP04                                      
        ( WTYPMVT ASC                                                           
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGQ014L ON P961.RTGQ01                                      
        ( CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGQ015L ON P961.RTGQ01                                      
        ( CINSEE ASC                                                            
        , WCONTRA DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGQ031L ON P961.RTGQ03                                      
        ( CZONLIV ASC                                                           
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGQ061L ON P961.RTGQ06                                      
        ( CTYPE ASC                                                             
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGQ062L ON P961.RTGQ06                                      
        ( CTYPE ASC                                                             
        , CINSEE ASC                                                            
        , LCOMUNE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGQ063L ON P961.RTGQ06                                      
        ( CPOSTAL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGR301L ON P961.RTGR30                                      
        ( DMVTREC ASC                                                           
        , WTLMELA ASC                                                           
        , CTYPMVT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGR302L ON P961.RTGR30                                      
        ( NENTCDE ASC                                                           
        , DJRECQUAI ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGR303L ON P961.RTGR30                                      
        ( NBL ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGR304L ON P961.RTGR30                                      
        ( NREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGR351L ON P961.RTGR35                                      
        ( NCDE ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGR353L ON P961.RTGR35                                      
        ( NBL ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGR354L ON P961.RTGR35                                      
        ( NREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGR355L ON P961.RTGR35                                      
        ( NENTCDEPT ASC                                                         
        , QREC ASC                                                              
        , QPRISE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGS301L ON P961.RTGS30                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGS311 ON P961.RTGS31                                       
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGS420L ON P961.RTGS42                                      
        ( NSOCORIG ASC                                                          
        , NLIEUORIG ASC                                                         
        , NSSLIEUORIG ASC                                                       
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , NSSLIEUDEST ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXGS422L ON P961.RTGS42                                      
        ( DMVT ASC                                                              
        , DHMVT ASC                                                             
        , DMMVT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGS601L ON P961.RTGS60                                      
        ( NSOCTRT ASC                                                           
        , NLIEUTRT ASC                                                          
        , NSSLIEUTRT ASC                                                        
        , CLIEUTRT ASC                                                          
        , NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGS602L ON P961.RTGS60                                      
        ( NCODIC ASC                                                            
        , NSOCDEST ASC                                                          
        , NSOCTRT ASC                                                           
        , NLIEUTRT ASC                                                          
        , NSSLIEUTRT ASC                                                        
        , CLIEUTRT ASC                                                          
        , CSTATUTTRT ASC                                                        
        , LEMPLACT ASC                                                          
        , NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGS700L ON P961.RTGS70                                      
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXGV021L ON P961.RTGV02                                      
        ( LNOM ASC                                                              
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV022L ON P961.RTGV02                                      
        ( TELDOM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV023L ON P961.RTGV02                                      
        ( IDCLIENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV024L ON P961.RTGV02                                      
        ( TELBUR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV031L ON P961.RTGV03                                      
        ( EMAIL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV050L ON P961.RTGV05                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXGV081L ON P961.RTGV08                                      
        ( VALCTRL ASC                                                           
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV104L ON P961.RTGV10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV105L ON P961.RTGV10                                      
        ( NVENTO ASC                                                            
        , NLIEU ASC                                                             
        , TYPVTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV111L ON P961.RTGV11                                      
        ( CPROTOUR ASC                                                          
        , DDELIV ASC                                                            
        , CTYPENREG ASC                                                         
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV113L ON P961.RTGV11                                      
        ( NCODIC ASC                                                            
        , WTOPELIVRE ASC                                                        
        , CTYPENREG ASC                                                         
        , CENREG ASC                                                            
        , WCQERESF ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV114L ON P961.RTGV11                                      
        ( DCOMPTA ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV115L ON P961.RTGV11                                      
        ( NSOCIETE ASC                                                          
        , DDELIV ASC                                                            
        , WTOPELIVRE ASC                                                        
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV116L ON P961.RTGV11                                      
        ( DCREATION ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV141L ON P961.RTGV14                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NTRANS DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV142L ON P961.RTGV14                                      
        ( DSAISIE ASC                                                           
        , NSOCP ASC                                                             
        , NLIEUP ASC                                                            
        , NVENTE ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV200L ON P961.RTGV20                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXGV231L ON P961.RTGV23                                      
        ( DCREATION1 ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV271L ON P961.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , CAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXGV272L ON P961.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DAUTORD ASC                                                           
        , NORDRE ASC                                                            
        , CAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXGV273L ON P961.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , DAUTORD ASC                                                           
        , NAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXGV300L ON P961.RTGV30                                      
        ( LNOM ASC                                                              
        , DVENTE ASC                                                            
        , DNAISSANCE ASC                                                        
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXGV351L ON P961.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV352L ON P961.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NDOCENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV353L ON P961.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV355L ON P961.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
        , NDOCENLV DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV356L ON P961.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
        , NBONENLV DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXGV990L ON P961.RTGV99                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE001L ON P961.RTHE00                                      
        ( CTIERSELA ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE002L ON P961.RTHE00                                      
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE011L ON P961.RTHE01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE012L ON P961.RTHE01                                      
        ( CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE013L ON P961.RTHE01                                      
        ( NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE014L ON P961.RTHE01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE101L ON P961.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE102L ON P961.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE103L ON P961.RTHE10                                      
        ( NCODIC ASC                                                            
        , NSERIE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE104L ON P961.RTHE10                                      
        ( PDOSNASC ASC                                                          
        , DOSNASC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE105L ON P961.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NENVOI ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE107L ON P961.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WSOLDE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE108L ON P961.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CREFUS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  INDEX P961.RXHE110L ON P961.RTHE11                                      
--MW        ( NLIEUHED ASC                                                          
--MW        , NGHE ASC                                                              
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  INDEX P961.RXHE111L ON P961.RTHE11                                      
        ( NLIEUHED ASC                                                          
        , NGHE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXHE202L ON P961.RTHE20                                      
        ( NUMPAL ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHE221L ON P961.RTHE22                                      
        ( CLIEUHET ASC                                                          
        , CTIERS ASC                                                            
        , DENVOI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHN001L ON P961.RTHN00                                      
        ( CTIERSELA ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHN002L ON P961.RTHN00                                      
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHN003L ON P961.RTHN00                                      
        ( DTRAIT ASC                                                            
        , WSUPP ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHN011L ON P961.RTHN01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHN012L ON P961.RTHN01                                      
        ( CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHV021L ON P961.RTHV02                                      
        ( DVENTECIALE DESC                                                      
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXHV071L ON P961.RTHV07                                      
        ( CETAT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXHV081L ON P961.RTHV08                                      
        ( DVENTECIALE DESC                                                      
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P961.RXHV600L ON P961.RTHV60                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXHV960L ON P961.RTHV96                                      
        ( DTSTAT ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
        , CVENDEUR ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXIA001L ON P961.RTIA00                                      
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , WTLMELA ASC                                                           
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXIE001L ON P961.RTIE00                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , CMODSTOCK ASC                                                         
        , CEMP13 ASC                                                            
        , CEMP23 ASC                                                            
        , CEMP33 ASC                                                            
        , CTYPEMPL3 ASC                                                         
        , NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  INDEX P961.RXIE002L ON P961.RTIE00                                      
--MW        ( NSOCDEPOT ASC                                                         
--MW        , NDEPOT ASC                                                            
--MW        , NCODIC3 ASC                                                           
--MW          )                                                                     
--MW           PCTFREE 5                                                            
--MW    ;                                                                           
CREATE  INDEX P961.RXIE003L ON P961.RTIE00                                      
        ( NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXIE004L ON P961.RTIE00                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXIE052L ON P961.RTIE05                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXIE301L ON P961.RTIE30                                      
        ( "ZONE" ASC                                                            
        , SECTEUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXIN001L ON P961.RTIN00                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXIN050L ON P961.RTIN05                                      
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , DINVENTAIRE ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXIN901L ON P961.RTIN90                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NSSLIEU ASC                                                           
        , LIEUTRT ASC                                                           
        , DINVENTAIRE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXKIS01L ON P961.RTKIS0                                      
        ( FILIALE ASC                                                           
        , NPLAN ASC                                                             
        , NCOMPTE ASC                                                           
        , NUMSS ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXKIS11L ON P961.RTKIS1                                      
        ( FILIALE ASC                                                           
        , NPLAN ASC                                                             
        , NCOMPTE ASC                                                           
        , NUMSS ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXKIS21L ON P961.RTKIS2                                      
        ( CACIDOPER ASC                                                         
        , CODEOPER ASC                                                          
        , DATEOPER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXKIS31L ON P961.RTKIS3                                      
        ( CACIDOPER ASC                                                         
        , CODEOPER ASC                                                          
        , DATEOPER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXLG001 ON P961.RTLG00                                       
        ( CINSEE ASC                                                            
        , LNOMPVOIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXLG002 ON P961.RTLG00                                       
        ( CTVOIE ASC                                                            
        , LNOMVOIE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXLM000L ON P961.RTLM00                                      
        ( NLISTE ASC                                                            
        , NAPPORT ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DATTRANS ASC                                                          
        , NCAISSE ASC                                                           
        , NTRANS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXPR001L ON P961.RTPR00                                      
        ( CTYPPREST ASC                                                         
        , NENTCDE ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXPR002L ON P961.RTPR00                                      
        ( CTYPPREST ASC                                                         
        , NENTCDE ASC                                                           
        , CASSORT ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXPR011L ON P961.RTPR01                                      
        ( CFAM ASC                                                              
        , WAFFICH ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXPR061L ON P961.RTPR06                                      
        ( CSTATUT ASC                                                           
        , LSTATUT ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXPR062L ON P961.RTPR06                                      
        ( CSTATUT ASC                                                           
        , DEFFET ASC                                                            
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXPR063L ON P961.RTPR06                                      
        ( CSTATUT ASC                                                           
        , CPRESTATION ASC                                                       
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXPS001L ON P961.RTPS00                                      
        ( NOMCLI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXQC011L ON P961.RTQC01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXQC111L ON P961.RTQC11                                      
        ( DTESSI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXQC112L ON P961.RTQC11                                      
        ( NCLIENT ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P961.RXQC113L ON P961.RTQC11                                      
        ( EMAIL ASC                                                             
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P961.RXQC121L ON P961.RTQC12                                      
        ( DENVOI_QV ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXRA001L ON P961.RTRA00                                      
        ( CTYPDEMANDE ASC                                                       
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXRA002L ON P961.RTRA00                                      
        ( NAVOIRCLIENT ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXRD000L ON P961.RTRD00                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXRE600L ON P961.RTRE60                                      
        ( CTYPENREG ASC                                                         
        , WTYPE1 ASC                                                            
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
        , CPREST ASC                                                            
        , CENREG ASC                                                            
        , WTYPE2 ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXRE610L ON P961.RTRE61                                      
        ( CTYPENREG ASC                                                         
        , WTYPE1 ASC                                                            
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
        , CPREST ASC                                                            
        , CENREG ASC                                                            
        , WTYPE2 ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXRX251L ON P961.RTRX25                                      
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXRX300L ON P961.RTRX30                                      
        ( NRELEVE ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXRX551L ON P961.RTRX55                                      
        ( NCONC ASC                                                             
        , DRELEVE DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXSP101L ON P961.RTSP10                                      
        ( NCODIC ASC                                                            
        , DJOUR DESC                                                            
        , SRP ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXSP200L ON P961.RTSP20                                      
        ( DTRAITEMENT ASC                                                       
        , CPROG ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXSP201L ON P961.RTSP20                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXTL021L ON P961.RTTL02                                      
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXTL022L ON P961.RTTL02                                      
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , CPROTOUR ASC                                                          
        , CTOURNEE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXTL040L ON P961.RTTL04                                      
        ( NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXTL111L ON P961.RTTL11                                      
        ( NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXVA000L ON P961.RTVA00                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXVA001L ON P961.RTVA00                                      
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXVA010L ON P961.RTVA01                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXVA011L ON P961.RTVA01                                      
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXVE021L ON P961.RTVE02                                      
        ( LNOM ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXVE022L ON P961.RTVE02                                      
        ( TELDOM ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXVE102L ON P961.RTVE10                                      
        ( NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXVE103L ON P961.RTVE10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.RXVE111L ON P961.RTVE11                                      
        ( NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXVE113L ON P961.RTVE11                                      
        ( NCODIC ASC                                                            
        , WTOPELIVRE ASC                                                        
        , CTYPENREG ASC                                                         
        , CENREG ASC                                                            
        , WCQERESF ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXVE114L ON P961.RTVE11                                      
        ( DCOMPTA ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXVE115L ON P961.RTVE11                                      
        ( NSOCIETE ASC                                                          
        , DDELIV ASC                                                            
        , WTOPELIVRE ASC                                                        
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXVE116L ON P961.RTVE11                                      
        ( DCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXVE141L ON P961.RTVE14                                      
        ( DSAISIE ASC                                                           
        , NSOCP ASC                                                             
        , NLIEUP ASC                                                            
        , NVENTE ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P961.RXVI010L ON P961.RTVI01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.SXVA000L ON P961.STVA00                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.SXVA010L ON P961.STVA01                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD060L ON P961.RTAD06                               
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD070L ON P961.RTAD07                               
        ( CSOC ASC                                                              
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD071L ON P961.RTAD07                               
        ( NENTCDEK ASC                                                          
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD100L ON P961.RTAD10                               
        ( CSOC ASC                                                              
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD101L ON P961.RTAD10                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD102L ON P961.RTAD10                               
        ( NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD110L ON P961.RTAD11                               
        ( CSOC ASC                                                              
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD111L ON P961.RTAD11                               
        ( CFAMK ASC                                                             
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD140L ON P961.RTAD14                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD510L ON P961.RTAD51                               
        ( CSOC ASC                                                              
        , CCOULEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD511L ON P961.RTAD51                               
        ( CCOULEURK ASC                                                         
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD550L ON P961.RTAD55                               
        ( CUSINE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD610L ON P961.RTAD61                               
        ( CSOC ASC                                                              
        , CPAYS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAD611L ON P961.RTAD61                               
        ( CPAYSK ASC                                                            
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAI240L ON P961.RTAI24                               
        ( FDCLAS ASC                                                            
        , FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAI270L ON P961.RTAI27                               
        ( CDESCRIPTIF ASC                                                       
        , FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAI530L ON P961.RTAI53                               
        ( FSSKU ASC                                                             
        , FSCTL ASC                                                             
        , FSCLAS ASC                                                            
        , FSFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAN010L ON P961.RTAN01                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
        , CDEST ASC                                                             
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAV010L ON P961.RTAV01                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
        , CTYPAVOIR ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAV020L ON P961.RTAV02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
        , CTYPAVOIR ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXAV030L ON P961.RTAV03                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXBA000L ON P961.RTBA00                               
        ( NFACTBA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXBA010L ON P961.RTBA01                               
        ( NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXBA100L ON P961.RTBA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NECART ASC                                                            
        , CMODRGLT ASC                                                          
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXBA200L ON P961.RTBA20                               
        ( CTIERSBA ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXBA210L ON P961.RTBA21                               
        ( CTIERSBA ASC                                                          
        , DANNEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXBA300L ON P961.RTBA30                               
        ( CTIERSBA ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXBC310 ON P961.RTBC31                                
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXBC320L ON P961.RTBC32                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXBC330L ON P961.RTBC33                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXBC340L ON P961.RTBC34                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXBC350L ON P961.RTBC35                               
        ( CPROGRAMME ASC                                                        
        , DDEBUT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCC000L ON P961.RTCC00                               
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCC010L ON P961.RTCC01                               
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCC020L ON P961.RTCC02                               
        ( DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCC030L ON P961.RTCC03                               
        ( TYPTRANS ASC                                                          
        , MOPAI ASC                                                             
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NOPERATEUR ASC                                                        
        , HTRAIT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCC040L ON P961.RTCC04                               
        ( CPAICPT ASC                                                           
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCC050L ON P961.RTCC05                               
        ( NSOC ASC                                                              
        , DJOUR ASC                                                             
        , NSEQ ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCD000L ON P961.RTCD00                               
        ( NCODIC ASC                                                            
        , NMAGGRP ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCD050L ON P961.RTCD05                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCD100L ON P961.RTCD10                               
        ( DSEMAINE ASC                                                          
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCD150L ON P961.RTCD15                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCD200L ON P961.RTCD20                               
        ( NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCD250L ON P961.RTCD25                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCD300L ON P961.RTCD30                               
        ( DCDE ASC                                                              
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCD350L ON P961.RTCD35                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCD500L ON P961.RTCD50                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCE100L ON P961.RTCE10                               
        ( NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCE200L ON P961.RTCE20                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCT000L ON P961.RTCT00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DDELIV ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXCX000L ON P961.RTCX00                               
        ( NOECS ASC                                                             
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , COMPTECG ASC                                                          
        , DATEFF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXDC000L ON P961.RTDC00                               
        ( NDOSSIER ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXDC100L ON P961.RTDC10                               
        ( NDOSSIER ASC                                                          
        , NECHEANCE ASC                                                         
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXDC200L ON P961.RTDC20                               
        ( NDOSSIER ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXDS000L ON P961.RTDS00                               
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CAPPRO ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXDS050L ON P961.RTDS05                               
        ( NCODIC ASC                                                            
        , DSEMESTRE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXDS100L ON P961.RTDS10                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXEF100L ON P961.RTEF10                               
        ( CTRAIT ASC                                                            
        , CTIERS ASC                                                            
        , NRENDU ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXEG000L ON P961.RTEG00                               
        ( NOMETAT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXEG050L ON P961.RTEG05                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXEG100L ON P961.RTEG10                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXEG110L ON P961.RTEG11                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXEG150L ON P961.RTEG15                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXEG200L ON P961.RTEG20                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXEG250L ON P961.RTEG25                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , LIGNECALC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXEG300L ON P961.RTEG30                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXEG400L ON P961.RTEG40                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFA010L ON P961.RTFA01                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFA020L ON P961.RTFA02                               
        ( NSOCV ASC                                                             
        , NLIEUV ASC                                                            
        , NORDV ASC                                                             
        , NSOCF ASC                                                             
        , NLIEUF ASC                                                            
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF000L ON P961.RTFF00                               
        ( NPIECE ASC                                                            
        , DEXCPT DESC                                                           
        , PREFT1 ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF050L ON P961.RTFF05                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF060L ON P961.RTFF06                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF100L ON P961.RTFF10                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF110L ON P961.RTFF11                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF120L ON P961.RTFF12                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF130L ON P961.RTFF13                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF150L ON P961.RTFF15                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF200L ON P961.RTFF20                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNEFAC ASC                                                         
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF210L ON P961.RTFF21                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF250L ON P961.RTFF25                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , WTYPESEQ ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF300L ON P961.RTFF30                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF350L ON P961.RTFF35                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF360L ON P961.RTFF36                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF370L ON P961.RTFF37                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF400L ON P961.RTFF40                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF410L ON P961.RTFF41                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF420L ON P961.RTFF42                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF430L ON P961.RTFF43                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF440L ON P961.RTFF44                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF450L ON P961.RTFF45                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF500L ON P961.RTFF50                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
        , DEXCPTLIT ASC                                                         
        , NPIECELIT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF510L ON P961.RTFF51                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , DEXCPTLIT ASC                                                         
        , NPIECELIT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF600L ON P961.RTFF60                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF650L ON P961.RTFF65                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF660L ON P961.RTFF66                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF670L ON P961.RTFF67                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF700L ON P961.RTFF70                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF750L ON P961.RTFF75                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF800L ON P961.RTFF80                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF810L ON P961.RTFF81                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFF850L ON P961.RTFF85                               
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFI000L ON P961.RTFI00                               
        ( CPROF ASC                                                             
        , QTAUX ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFI050L ON P961.RTFI05                               
        ( NENTCDE ASC                                                           
        , CFAM ASC                                                              
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM010L ON P961.RTFM01                               
        ( NENTITE ASC                                                           
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM020L ON P961.RTFM02                               
        ( CDEVISE ASC                                                           
        , WSENS ASC                                                             
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM030L ON P961.RTFM03                               
        ( NENTITE ASC                                                           
        , CMETHODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM040L ON P961.RTFM04                               
        ( CDEVISE ASC                                                           
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM050L ON P961.RTFM05                               
        ( CDEVORIG ASC                                                          
        , CDEVDEST ASC                                                          
        , DEFFET ASC                                                            
        , COPERAT ASC                                                           
        , PTAUX ASC                                                             
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM060L ON P961.RTFM06                               
        ( CETAT ASC                                                             
        , NDEMANDE ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM100L ON P961.RTFM10                               
        ( NENTITE ASC                                                           
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM200L ON P961.RTFM20                               
        ( NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM230L ON P961.RTFM23                               
        ( NENTITE ASC                                                           
        , NEXER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM540L ON P961.RTFM54                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM550L ON P961.RTFM55                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM560L ON P961.RTFM56                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM570L ON P961.RTFM57                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM580L ON P961.RTFM58                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM590L ON P961.RTFM59                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM641L ON P961.RTFM64                               
        ( STEAPP ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM691 ON P961.RTFM69                                
        ( COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM730L ON P961.RTFM73                               
        ( CNOMPROG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM801L ON P961.RTFM80                               
        ( CNATOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM811L ON P961.RTFM81                               
        ( CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM821L ON P961.RTFM82                               
        ( CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM831L ON P961.RTFM83                               
        ( CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM841L ON P961.RTFM84                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CTVA ASC                                                              
        , CGEO ASC                                                              
        , WGROUPE ASC                                                           
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM871L ON P961.RTFM87                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM881L ON P961.RTFM88                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM891L ON P961.RTFM89                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM901L ON P961.RTFM90                               
        ( CINTERFACE ASC                                                        
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM910L ON P961.RTFM91                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM921L ON P961.RTFM92                               
        ( CZONE ASC                                                             
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFM930L ON P961.RTFM93                               
        ( CRITLIEU1 ASC                                                         
        , CRITLIEU2 ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT010L ON P961.RTFT01                               
        ( NAUX ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT020L ON P961.RTFT02                               
        ( CACID ASC                                                             
        , NAUX ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT030L ON P961.RTFT03                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT040L ON P961.RTFT04                               
        ( CBANQUE ASC                                                           
        , METHREG ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT050L ON P961.RTFT05                               
        ( CBANQUE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT060L ON P961.RTFT06                               
        ( NSOC ASC                                                              
        , CPTR ASC                                                              
        , NAUX ASC                                                              
        , CREGTVA ASC                                                           
        , NATURE ASC                                                            
        , NPER ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT070L ON P961.RTFT07                               
        ( CETAT ASC                                                             
        , CSERVICE ASC                                                          
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT080L ON P961.RTFT08                               
        ( CETAT ASC                                                             
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT090L ON P961.RTFT09                               
        ( CETAT ASC                                                             
        , CSERVICE ASC                                                          
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT100L ON P961.RTFT10                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT110L ON P961.RTFT11                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , WTYPEADR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT120L ON P961.RTFT12                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT130L ON P961.RTFT13                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , DECH ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT140L ON P961.RTFT14                               
        ( CBANQUE ASC                                                           
        , METHREG ASC                                                           
        , NREG ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT150L ON P961.RTFT15                               
        ( NORDRE ASC                                                            
        , NPROPO ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT160L ON P961.RTFT16                               
        ( NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT170L ON P961.RTFT17                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NAUX ASC                                                              
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT180L ON P961.RTFT18                               
        ( METHREG ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT190L ON P961.RTFT19                               
        ( CTRAIT ASC                                                            
        , CACID ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT200L ON P961.RTFT20                               
        ( NAUX ASC                                                              
        , CDEVISE ASC                                                           
        , METHREG ASC                                                           
        , CTRESO ASC                                                            
        , WACCES ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT210L ON P961.RTFT21                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NLIGNE ASC                                                            
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT220L ON P961.RTFT22                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , COMPTE ASC                                                            
        , SSCOMPTE ASC                                                          
        , SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
        , STEAPP ASC                                                            
        , CDEVISE ASC                                                           
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , WSENS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT230L ON P961.RTFT23                               
        ( NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT250L ON P961.RTFT25                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT260L ON P961.RTFT26                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT270L ON P961.RTFT27                               
        ( NLIGNE ASC                                                            
        , NPIECE ASC                                                            
        , DECH ASC                                                              
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NAUXR ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT280L ON P961.RTFT28                               
        ( METHREG ASC                                                           
        , NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT290L ON P961.RTFT29                               
        ( NCODE ASC                                                             
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT300L ON P961.RTFT30                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NATURE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT320L ON P961.RTFT32                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFERENCE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT321L ON P961.RTFT32                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , COMPTE ASC                                                            
        , SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT330L ON P961.RTFT33                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFERENCE ASC                                                         
        , NVENTIL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT340L ON P961.RTFT34                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT350 ON P961.RTFT35                                
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , REFDOC ASC                                                            
        , CDEVISE ASC                                                           
        , WTRT ASC                                                              
        , NEXER ASC                                                             
        , NPER ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT360 ON P961.RTFT36                                
        ( REFDOC ASC                                                            
        , NJRN ASC                                                              
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT370 ON P961.RTFT37                                
        ( NJRN ASC                                                              
        , REFDOC ASC                                                            
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT380 ON P961.RTFT38                                
        ( REFDOC ASC                                                            
        , NJRN ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT390 ON P961.RTFT39                                
        ( NLIGNE ASC                                                            
        , REFDOC ASC                                                            
        , NJRN ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT400 ON P961.RTFT40                                
        ( CATCH ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT410 ON P961.RTFT41                                
        ( NATCH ASC                                                             
        , CATCH ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT430L ON P961.RTFT43                               
        ( NPIECE DESC                                                           
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , CDEVISE ASC                                                           
        , NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT440L ON P961.RTFT44                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT450 ON P961.RTFT45                                
        ( CPOND ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT460 ON P961.RTFT46                                
        ( NPER ASC                                                              
        , CPOND ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT490L ON P961.RTFT49                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NATURE ASC                                                            
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT500L ON P961.RTFT50                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT510L ON P961.RTFT51                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT520L ON P961.RTFT52                               
        ( CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT530L ON P961.RTFT53                               
        ( CACID ASC                                                             
        , NSOC ASC                                                              
        , NETAB ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT540L ON P961.RTFT54                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , DECH ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT570L ON P961.RTFT57                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NAUX ASC                                                              
        , NTIERSCV ASC                                                          
        , WTYPEADR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT601L ON P961.RTFT60                               
        ( COMPTE ASC                                                            
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT611L ON P961.RTFT61                               
        ( SECTION ASC                                                           
        , NSOC ASC                                                              
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT621L ON P961.RTFT62                               
        ( RUBRIQUE ASC                                                          
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT631L ON P961.RTFT63                               
        ( NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT651L ON P961.RTFT65                               
        ( CMASQUEC ASC                                                          
        , CMASQUES ASC                                                          
        , CMASQUER ASC                                                          
        , CMASQUEE ASC                                                          
        , CMASQUEA ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT660L ON P961.RTFT66                               
        ( COMPTE ASC                                                            
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT670L ON P961.RTFT67                               
        ( SECTION ASC                                                           
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT680L ON P961.RTFT68                               
        ( RUBRIQUE ASC                                                          
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT700L ON P961.RTFT70                               
        ( COMPTE ASC                                                            
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT710L ON P961.RTFT71                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFDOC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT720L ON P961.RTFT72                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFDOC ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT730L ON P961.RTFT73                               
        ( NSOC ASC                                                              
        , CNOMPROG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT740L ON P961.RTFT74                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , CNOMPROG ASC                                                          
        , DDATE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT750L ON P961.RTFT75                               
        ( DDEMANDE DESC                                                         
        , NTIERS ASC                                                            
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT851L ON P961.RTFT85                               
        ( CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT861L ON P961.RTFT86                               
        ( DEFFET ASC                                                            
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT871L ON P961.RTFT87                               
        ( SECTIONO ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT881L ON P961.RTFT88                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT891L ON P961.RTFT89                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT900L ON P961.RTFT90                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT930L ON P961.RTFT93                               
        ( NBC ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT940L ON P961.RTFT94                               
        ( NBC ASC                                                               
        , NLBC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT950L ON P961.RTFT95                               
        ( NBC ASC                                                               
        , "TYPE" ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFT960L ON P961.RTFT96                               
        ( NBC ASC                                                               
        , NSOC ASC                                                              
        , NETAB ASC                                                             
        , NEXER ASC                                                             
        , NJRN ASC                                                              
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX000L ON P961.RTFX00                               
        ( NOECS ASC                                                             
        , DATEFF DESC                                                           
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , WFX00 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX001L ON P961.RTFX00                               
        ( NOECS ASC                                                             
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , WFX00 ASC                                                             
        , DATEFF DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX010L ON P961.RTFX01                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX050L ON P961.RTFX05                               
        ( NJRN ASC                                                              
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX100L ON P961.RTFX10                               
        ( COMPTEGL ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX110L ON P961.RTFX11                               
        ( CZONE ASC                                                             
        , ZONEGL ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX150L ON P961.RTFX15                               
        ( WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX200L ON P961.RTFX20                               
        ( CTYPSTAT ASC                                                          
        , NDEPTLMELA ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX300L ON P961.RTFX30                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX350L ON P961.RTFX35                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX400L ON P961.RTFX40                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX450L ON P961.RTFX45                               
        ( CODEAPPLI ASC                                                         
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
        , DCREAT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXFX500L ON P961.RTFX50                               
        ( DATTRANS ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CTYPTRANS ASC                                                         
        , CAPPRET ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA000L ON P961.RTGA00                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA010A ON P961.RTGA01LA                             
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA010J ON P961.RTGA01LJ                             
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA010L ON P961.RTGA01                               
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA030L ON P961.RTGA03                               
        ( CSOC ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA040L ON P961.RTGA04                               
        ( NCONC ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA060L ON P961.RTGA06                               
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA070L ON P961.RTGA07                               
        ( CGRPFOURN ASC                                                         
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA080L ON P961.RTGA08                               
        ( NENTCDE ASC                                                           
        , CINTERLOCUT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA090L ON P961.RTGA09                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , WSEQED ASC                                                            
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CMARKETIN3 ASC                                                        
        , CVMARKETIN3 ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA100L ON P961.RTGA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA110L ON P961.RTGA11                               
        ( CETAT ASC                                                             
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA130L ON P961.RTGA13                               
        ( CFAM ASC                                                              
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA140L ON P961.RTGA14                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA150L ON P961.RTGA15                               
        ( CFAM ASC                                                              
        , CTYPDCL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA160L ON P961.RTGA16                               
        ( CFAM ASC                                                              
        , CGARANTCPLT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA170L ON P961.RTGA17                               
        ( CFAM ASC                                                              
        , CARACTSPE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA180L ON P961.RTGA18                               
        ( CFAM ASC                                                              
        , CDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA200L ON P961.RTGA20                               
        ( CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA210L ON P961.RTGA21                               
        ( CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA211L ON P961.RTGA21                               
        ( WRAYONFAM ASC                                                         
        , CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA220L ON P961.RTGA22                               
        ( CMARQ ASC                                                             
        , LMARQ ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA230L ON P961.RTGA23                               
        ( CMARKETING ASC                                                        
        , LMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA240L ON P961.RTGA24                               
        ( CDESCRIPTIF ASC                                                       
        , LDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA250L ON P961.RTGA25                               
        ( CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
        , CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA260L ON P961.RTGA26                               
        ( CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA270L ON P961.RTGA27                               
        ( CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA290L ON P961.RTGA29                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , WSEQED ASC                                                            
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  UNIQUE INDEX P961.RXGA300L ON P961.RTGA30                               
--MW        ( CPARAM ASC                                                            
--MW        , CFAM ASC                                                              
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA301L ON P961.RTGA30                               
        ( CPARAM ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA310L ON P961.RTGA31                               
        ( NCODIC ASC                                                            
        , NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA320L ON P961.RTGA32                               
        ( NCODIC ASC                                                            
        , NSEQUENCE ASC                                                         
        , NEANCOMPO ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA330L ON P961.RTGA33                               
        ( NCODIC ASC                                                            
        , CDEST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA390L ON P961.RTGA39                               
        ( NENTSAP ASC                                                           
        , NENTANC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA400L ON P961.RTGA40                               
        ( CGARANTIE ASC                                                         
        , CFAM ASC                                                              
        , CTARIF ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA410L ON P961.RTGA41                               
        ( CGCPLT ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA420L ON P961.RTGA42                               
        ( CGCPLT ASC                                                            
        , NMOIS ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA430L ON P961.RTGA43                               
        ( CSECTEUR ASC                                                          
        , CINSEE ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA440L ON P961.RTGA44                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA450L ON P961.RTGA45                               
        ( CFAM ASC                                                              
        , CGRP ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA460L ON P961.RTGA46                               
        ( CGRP ASC                                                              
        , CSECT1 ASC                                                            
        , ZONE1 ASC                                                             
        , CSECT2 ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA480L ON P961.RTGA48                               
        ( NCODIC ASC                                                            
        , SPPRTP ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA510L ON P961.RTGA51                               
        ( NCODIC ASC                                                            
        , CTYPDCL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA520L ON P961.RTGA52                               
        ( NCODIC ASC                                                            
        , CGARANTCPLT ASC                                                       
        , CVGARANCPLT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA530L ON P961.RTGA53                               
        ( NCODIC ASC                                                            
        , CDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA550L ON P961.RTGA55                               
        ( NCODIC ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA560L ON P961.RTGA56                               
        ( NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA580L ON P961.RTGA58                               
        ( NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , CTYPLIEN ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA590L ON P961.RTGA59                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA600L ON P961.RTGA60                               
        ( NCODIC ASC                                                            
        , NMESSDEPOT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA620L ON P961.RTGA62                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA630L ON P961.RTGA63                               
        ( NCODIC ASC                                                            
        , CARACTSPE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA640L ON P961.RTGA64                               
        ( NCODIC ASC                                                            
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA650L ON P961.RTGA65                               
        ( NCODIC ASC                                                            
        , CSTATUT ASC                                                           
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA660L ON P961.RTGA66                               
        ( NCODIC ASC                                                            
        , CSENS ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA680L ON P961.RTGA68                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA710L ON P961.RTGA71                               
        ( CTABLE ASC                                                            
        , CSTABLE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA720L ON P961.RTGA72                               
        ( CNUMCONT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA730L ON P961.RTGA73                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA740L ON P961.RTGA74                               
        ( NCODIC ASC                                                            
        , NTITRE ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA750L ON P961.RTGA75                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA760L ON P961.RTGA76                               
        ( CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA770L ON P961.RTGA77                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA790L ON P961.RTGA79                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA810L ON P961.RTGA81                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA830L ON P961.RTGA83                               
        ( NCODIC ASC                                                            
        , CMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA910L ON P961.RTGA91                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA920L ON P961.RTGA92                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA930L ON P961.RTGA93                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA940L ON P961.RTGA94                               
        ( CPROG ASC                                                             
        , CCHAMP ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGA990L ON P961.RTGA99                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGB300L ON P961.RTGB30                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGB800L ON P961.RTGB80                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGD990L ON P961.RTGD99                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGF350L ON P961.RTGF35                               
        ( DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
        , NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGF400L ON P961.RTGF40                               
        ( NENTCDE ASC                                                           
        , CINTERLOCUT ASC                                                       
        , WMARQFAM ASC                                                          
        , CMARQFAM ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGF500L ON P961.RTGF50                               
        ( NCDE ASC                                                              
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGGXX0L ON P961.RTGGXX                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG050L ON P961.RTGG05                               
        ( NCONC ASC                                                             
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG100L ON P961.RTGG10                               
        ( NDEMALI ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG150L ON P961.RTGG15                               
        ( NDEMALI ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG200L ON P961.RTGG20                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG250L ON P961.RTGG25                               
        ( CHEFPROD ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG301L ON P961.RTGG30                               
        ( DANNEE ASC                                                            
        , DMOIS ASC                                                             
        , CFAM ASC                                                              
        , CHEFPROD ASC                                                          
        , NCONC ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG400L ON P961.RTGG40                               
        ( NCODIC ASC                                                            
        , DEFFET ASC                                                            
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG500L ON P961.RTGG50                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG510L ON P961.RTGG51                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG550L ON P961.RTGG55                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG600L ON P961.RTGG60                               
        ( NCODIC ASC                                                            
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , DDEMANDE ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGG700L ON P961.RTGG70                               
        ( NCODIC ASC                                                            
        , NREC ASC                                                              
        , NCDE ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGI1000L ON P961.RTGI10                              
        ( NSOCIETE ASC                                                          
        , CTABINT ASC                                                           
        , TTABINT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGI1200L ON P961.RTGI12                              
        ( NSOCIETE ASC                                                          
        , CTABINTMB ASC                                                         
        , MB ASC                                                                
        , TMB ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGI1400L ON P961.RTGI14                              
        ( NSOCIETE ASC                                                          
        , CTABINTCA ASC                                                         
        , CA ASC                                                                
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGI1600L ON P961.RTGI16                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGI2000L ON P961.RTGI20                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CTYPEAFF ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGI2100L ON P961.RTGI21                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CTYPEAFF ASC                                                          
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGI2200L ON P961.RTGI22                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGI2300L ON P961.RTGI23                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGI2400L ON P961.RTGI24                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGJ100L ON P961.RTGJ10                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGJ400L ON P961.RTGJ40                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CSIMU ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGM010L ON P961.RTGM01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGM060L ON P961.RTGM06                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , CZPRIX ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGM700L ON P961.RTGM70                               
        ( NSOCIETE ASC                                                          
        , NZONE ASC                                                             
        , CHEFPROD ASC                                                          
        , WSEQFAM ASC                                                           
        , LVMARKET1 ASC                                                         
        , LVMARKET2 ASC                                                         
        , LVMARKET3 ASC                                                         
        , NCODIC2 ASC                                                           
        , NCODIC ASC                                                            
        , "SEQUENCE" ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGP000L ON P961.RTGP00                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGP010L ON P961.RTGP01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGP020L ON P961.RTGP02                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , DLIVRAISON ASC                                                        
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGP030L ON P961.RTGP03                               
        ( DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGP041L ON P961.RTGP04                               
        ( NCODIC ASC                                                            
        , WTYPMVT ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGP050L ON P961.RTGP05                               
        ( CRAYON ASC                                                            
        , WRAYONFAM ASC                                                         
        , DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGQ000L ON P961.RTGQ00                               
        ( CMODDEL ASC                                                           
        , CEQUIP ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGQ011L ON P961.RTGQ01                               
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGQ020L ON P961.RTGQ02                               
        ( CMODDEL ASC                                                           
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGQ030L ON P961.RTGQ03                               
        ( CMODDEL ASC                                                           
        , CEQUIP ASC                                                            
        , DJOUR ASC                                                             
        , CZONLIV ASC                                                           
        , CPLAGE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGQ040L ON P961.RTGQ04                               
        ( CMODDEL ASC                                                           
        , CPERIMETRE ASC                                                        
        , DEFFET ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGQ041L ON P961.RTGQ04                               
        ( CMODDEL ASC                                                           
        , CPERIMETRE ASC                                                        
        , CFAM ASC                                                              
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGQ120L ON P961.RTGQ12                               
        ( CINSEE ASC                                                            
        , CGRP ASC                                                              
        , CTYPGRP ASC                                                           
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGQ200L ON P961.RTGQ20                               
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
        , CCADRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGR300L ON P961.RTGR30                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NAVENANT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGR350L ON P961.RTGR35                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , WTLMELA ASC                                                           
        , NCODIC ASC                                                            
        , NAVENANT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGR352L ON P961.RTGR35                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , WTLMELA ASC                                                           
        , NCODIC ASC                                                            
        , NAVENANT ASC                                                          
        , QREC ASC                                                              
        , QPRISE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGS300L ON P961.RTGS30                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , WSTOCKMASQ ASC                                                        
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGS310L ON P961.RTGS31                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , WSTOCKMASQ ASC                                                        
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGS450L ON P961.RTGS45                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGS500L ON P961.RTGS50                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGS550L ON P961.RTGS55                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGS600L ON P961.RTGS60                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV020L ON P961.RTGV02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV030L ON P961.RTGV03                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV040L ON P961.RTGV04                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV060L ON P961.RTGV06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV080L ON P961.RTGV08                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQ ASC                                                              
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV100L ON P961.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV101L ON P961.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV102L ON P961.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV110L ON P961.RTGV11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV119L ON P961.RTGV11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV130L ON P961.RTGV13                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTATION ASC                                                       
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV140L ON P961.RTGV14                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV150L ON P961.RTGV15                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV230L ON P961.RTGV23                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
        , DCREATION ASC                                                         
        , HCREATION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV250L ON P961.RTGV25                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTATION ASC                                                       
        , NSEQ ASC                                                              
        , DCREATION ASC                                                         
        , HCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV260L ON P961.RTGV26                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV270L ON P961.RTGV27                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DAUTORD ASC                                                           
        , NAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV310L ON P961.RTGV31                               
        ( CVENDEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV350L ON P961.RTGV35                               
        ( NSOCIETE ASC                                                          
        , NBONENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV400L ON P961.RTGV40                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV410L ON P961.RTGV41                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , CTYPADR ASC                                                           
        , CMODDEL ASC                                                           
        , DDELIV ASC                                                            
        , CPLAGE ASC                                                            
        , NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXGV810L ON P961.RTGV81                               
        ( CVENDEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHA300L ON P961.RTHA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHC000L ON P961.RTHC00                               
        ( NCHS ASC                                                              
        , NLIEUHC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHC030L ON P961.RTHC03                               
        ( NCHS ASC                                                              
        , NSEQ ASC                                                              
        , NLIEUHC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHE000L ON P961.RTHE00                               
        ( CTIERS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHE010L ON P961.RTHE01                               
        ( CTIERS ASC                                                            
        , CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHE100L ON P961.RTHE10                               
        ( NLIEUHED ASC                                                          
        , NGHE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHE150L ON P961.RTHE15                               
        ( CLIEUHET ASC                                                          
        , NLIEUHED ASC                                                          
        , NGHE ASC                                                              
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHE200L ON P961.RTHE20                               
        ( NLIEUHED ASC                                                          
        , CLIEUHET ASC                                                          
        , NRETOUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHE210L ON P961.RTHE21                               
        ( CLIEUHET ASC                                                          
        , CLIEUHEI ASC                                                          
        , NLOTDIAG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHE220L ON P961.RTHE22                               
        ( CLIEUHET ASC                                                          
        , NENVOI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHE230L ON P961.RTHE23                               
        ( CLIEUHET ASC                                                          
        , NDEMANDE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHL000L ON P961.RTHL00                               
        ( NOMMAP ASC                                                            
        , POSIT ASC                                                             
        , ZECRAN ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHL050L ON P961.RTHL05                               
        ( CTABLE ASC                                                            
        , CCHAMPR ASC                                                           
        , CCHAMPL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHL100L ON P961.RTHL10                               
        ( NOMMAP ASC                                                            
        , ZECRAN ASC                                                            
        , CTABLE ASC                                                            
        , CCHAMPR ASC                                                           
        , CCHAMPL ASC                                                           
        , NREQUETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHL150 ON P961.RTHL15                                
        ( NOMMAP ASC                                                            
        , ZECRAN ASC                                                            
        , NOCCUR ASC                                                            
        , NSEQCRIT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHN000L ON P961.RTHN00                               
        ( CTIERS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHN010L ON P961.RTHN01                               
        ( CTIERS ASC                                                            
        , CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHN110L ON P961.RTHN11                               
        ( CTIERS ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV000L ON P961.RTHV00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CMODDEL ASC                                                           
        , DOPER ASC                                                             
        , WSEQFAM ASC                                                           
        , CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV020L ON P961.RTHV02                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV030 ON P961.RTHV03                                
        ( DVENTECIALE ASC                                                       
        , CPRESTATION ASC                                                       
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV060L ON P961.RTHV06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV070L ON P961.RTHV07                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV080L ON P961.RTHV08                               
        ( NSOCIETE ASC                                                          
        , CFAM ASC                                                              
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV090L ON P961.RTHV09                               
        ( NSOCIETE ASC                                                          
        , CFAM ASC                                                              
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV100L ON P961.RTHV10                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV110L ON P961.RTHV11                               
        ( NSOCIETE ASC                                                          
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV150L ON P961.RTHV15                               
        ( CPROGRAMME ASC                                                        
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTECIALE ASC                                                       
        , CTERMID ASC                                                           
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV160L ON P961.RTHV16                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV170L ON P961.RTHV17                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DSVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV180L ON P961.RTHV18                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DSVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV200L ON P961.RTHV20                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CRAYON ASC                                                            
        , CINSEE ASC                                                            
        , DOPER ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV250L ON P961.RTHV25                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CRAYON ASC                                                            
        , CINSEE ASC                                                            
        , DGRPMOIS ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV260L ON P961.RTHV26                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV270L ON P961.RTHV27                               
        ( NSOCIETE ASC                                                          
        , WSEQPRO ASC                                                           
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV280L ON P961.RTHV28                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , WSEQPRO ASC                                                           
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV290L ON P961.RTHV29                               
        ( NSOCIETE ASC                                                          
        , WSEQPRO ASC                                                           
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV300L ON P961.RTHV30                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , NAGREGATED ASC                                                        
        , DSVENTECIALE ASC                                                      
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV320L ON P961.RTHV32                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV330L ON P961.RTHV33                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV340L ON P961.RTHV34                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DMVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV350L ON P961.RTHV35                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DMVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV700L ON P961.RTHV70                               
        ( NSOCIETE ASC                                                          
        , DMOIS ASC                                                             
        , NCODIC ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV820L ON P961.RTHV82                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV830L ON P961.RTHV83                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV860L ON P961.RTHV86                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXHV920L ON P961.RTHV92                               
        ( NSOCIETE ASC                                                          
        , MAGASIN ASC                                                           
        , NCODIC ASC                                                            
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIA000L ON P961.RTIA00                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIA050L ON P961.RTIA05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIA051L ON P961.RTIA05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIA600L ON P961.RTIA60                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE000L ON P961.RTIE00                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NFICHE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE050L ON P961.RTIE05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE051L ON P961.RTIE05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE100L ON P961.RTIE10                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE200L ON P961.RTIE20                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE250L ON P961.RTIE25                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CAIRE ASC                                                             
        , CCOTE ASC                                                             
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE251L ON P961.RTIE25                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , CAIRE ASC                                                             
        , CCOTE ASC                                                             
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE300L ON P961.RTIE30                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
        , "ZONE" ASC                                                            
        , SECTEUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE350L ON P961.RTIE35                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , CALLEE ASC                                                            
        , CNIVEAU ASC                                                           
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE360L ON P961.RTIE36                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CALLEE ASC                                                            
        , CNIVEAU ASC                                                           
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE450L ON P961.RTIE45                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CZONE ASC                                                             
        , CSECTEUR ASC                                                          
        , NSOUSSECT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE600L ON P961.RTIE60                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE700L ON P961.RTIE70                               
        ( NHS ASC                                                               
        , NLIEUHS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE800L ON P961.RTIE80                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIE900L ON P961.RTIE90                               
        ( NDEPOT ASC                                                            
        , NSOCDEPOT ASC                                                         
        , CREGROUP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIJ000L ON P961.RTIJ00                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIN000L ON P961.RTIN00                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , DINVENTAIRE ASC                                                       
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , WSTOCKMASQ ASC                                                        
        , CSTATUT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIN100L ON P961.RTIN01                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , SSLIEU ASC                                                            
        , CSTATUT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIN900L ON P961.RTIN90                               
        ( DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIT000L ON P961.RTIT00                               
        ( NINVENTAIRE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIT050L ON P961.RTIT05                               
        ( NINVENTAIRE ASC                                                       
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIT100L ON P961.RTIT10                               
        ( NINVENTAIRE ASC                                                       
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIT150L ON P961.RTIT15                               
        ( NINVENTAIRE ASC                                                       
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NSSLIEU ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXIT900L ON P961.RTIT90                               
        ( NINVENTAIRE ASC                                                       
        , NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXLG000 ON P961.RTLG00                                
        ( CINSEE ASC                                                            
        , CTVOIE ASC                                                            
        , LNOMVOIE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXLG010L ON P961.RTLG01                               
        ( CINSEE ASC                                                            
        , NVOIE ASC                                                             
        , CPARITE ASC                                                           
        , NDEBTR ASC                                                            
        , NFINTR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXLM100L ON P961.RTLM10                               
        ( NLISTE ASC                                                            
        , NAPPORT ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXLT000L ON P961.RTLT00                               
        ( NLETTRE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXLT010L ON P961.RTLT01                               
        ( NLETTRE ASC                                                           
        , NPAGE ASC                                                             
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXLT020L ON P961.RTLT02                               
        ( NLETTRE ASC                                                           
        , NPAGE ASC                                                             
        , NLIGNE ASC                                                            
        , INDVAR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMG010L ON P961.RTMG01                               
        ( CFAMMGI ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMO010L ON P961.RTMO01                               
        ( TYPOFFRE ASC                                                          
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMQ150L ON P961.RTMQ15                               
        ( CPROGRAMME ASC                                                        
        , CFONCTION ASC                                                         
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , DOPERATION ASC                                                        
        , "COMMENT" ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMQ160L ON P961.RTMQ16                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMU050L ON P961.RTMU05                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSSLIEU ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMU210L ON P961.RTMU21                               
        ( DSEMAINE ASC                                                          
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMU250L ON P961.RTMU25                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMU260L ON P961.RTMU26                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMU300L ON P961.RTMU30                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NROTATION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMU310L ON P961.RTMU31                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NROTATION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMU350L ON P961.RTMU35                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXMU500L ON P961.RTMU50                               
        ( CTYPCDE ASC                                                           
        , NSEQED ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPA700L ON P961.RTPA70                               
        ( NSOCIETE ASC                                                          
        , CPTSAV ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPC080L ON P961.RTPC08                               
        ( RANDOMNUM ASC                                                         
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPC090L ON P961.RTPC09                               
        ( RANDOMNUM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPM000L ON P961.RTPM00                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPM060L ON P961.RTPM06                               
        ( CMOPAI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPP000L ON P961.RTPP00                               
        ( NLIEU ASC                                                             
        , DMOISPAYE ASC                                                         
        , CPRIME ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR000 ON P961.RTPR00                                
        ( CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR010 ON P961.RTPR01                                
        ( CFAM ASC                                                              
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR020 ON P961.RTPR02                                
        ( NCODIC ASC                                                            
        , CPRESTATION ASC                                                       
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR030 ON P961.RTPR03                                
        ( CPRESTATION ASC                                                       
        , CGESTION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR031L ON P961.RTPR03                               
        ( CGESTION ASC                                                          
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR040 ON P961.RTPR04                                
        ( CPRESTATION ASC                                                       
        , CPRESTELT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR050 ON P961.RTPR05                                
        ( CPRESTATION ASC                                                       
        , CGRPETAT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR060 ON P961.RTPR06                                
        ( CPRESTATION ASC                                                       
        , CSTATUT ASC                                                           
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR100 ON P961.RTPR10                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR110 ON P961.RTPR11                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR120 ON P961.RTPR12                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR130 ON P961.RTPR13                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR140 ON P961.RTPR14                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR150 ON P961.RTPR15                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR160 ON P961.RTPR16                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR170 ON P961.RTPR17                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPR200 ON P961.RTPR20                                
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , CAGREPRE ASC                                                          
        , WSEQED ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPS000L ON P961.RTPS00                               
        ( CGCPLT ASC                                                            
        , NGCPLT ASC                                                            
        , NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPS010L ON P961.RTPS01                               
        ( CGCPLT ASC                                                            
        , NOSAV ASC                                                             
        , CSECTEUR ASC                                                          
        , DEFFET ASC                                                            
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPS020L ON P961.RTPS02                               
        ( CGCPLT ASC                                                            
        , NOSAV ASC                                                             
        , CSECTEUR ASC                                                          
        , DEFFET ASC                                                            
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPS030L ON P961.RTPS03                               
        ( CGCPLT ASC                                                            
        , NGCPLT ASC                                                            
        , NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPS040L ON P961.RTPS04                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , "TYPE" ASC                                                            
        , DINIT ASC                                                             
        , MONTTOT ASC                                                           
        , DUREE ASC                                                             
        , INFO1 ASC                                                             
        , INFO2 ASC                                                             
        , INFO3 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPT030L ON P961.RTPT03                               
        ( CODLANG ASC                                                           
        , CNOMPGRM ASC                                                          
        , NSEQLIB ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPV000L ON P961.RTPV00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXPV800L ON P961.RTPV80                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXQC010L ON P961.RTQC01                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXQC020L ON P961.RTQC02                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXQC030 ON P961.RTQC03                                
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXQC050L ON P961.RTQC05                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXQC060L ON P961.RTQC06                               
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXQC110L ON P961.RTQC11                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTQUEST ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXQC120L ON P961.RTQC12                               
        ( NCARTE ASC                                                            
        , CTQUEST ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXQC130L ON P961.RTQC13                               
        ( NCARTE ASC                                                            
        , CTQUEST ASC                                                           
        , CQUESTION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRA000L ON P961.RTRA00                               
        ( NDEMANDE ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRA010L ON P961.RTRA01                               
        ( NDEMANDE ASC                                                          
        , NSEQCODIC ASC                                                         
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRA020L ON P961.RTRA02                               
        ( CTYPREGL ASC                                                          
        , NREGL ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRC000L ON P961.RTRC00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRC100L ON P961.RTRC10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRE000L ON P961.RTRE00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRE100L ON P961.RTRE10                               
        ( NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
        , CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
        , WTLMELA ASC                                                           
        , NAGREGATED ASC                                                        
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRX000L ON P961.RTRX00                               
        ( NCONC ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRX050L ON P961.RTRX05                               
        ( NCONC ASC                                                             
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRX100L ON P961.RTRX10                               
        ( NCONC ASC                                                             
        , NMAG ASC                                                              
        , CRAYON ASC                                                            
        , NFREQUENCE ASC                                                        
        , NSEQUENCE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRX150L ON P961.RTRX15                               
        ( NCONC ASC                                                             
        , NMAG ASC                                                              
        , CRAYON ASC                                                            
        , DSUPPORT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRX200L ON P961.RTRX20                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRX210L ON P961.RTRX21                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRX220L ON P961.RTRX22                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRX250L ON P961.RTRX25                               
        ( NRELEVE ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRX450L ON P961.RTRX45                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CPROFIL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRX500L ON P961.RTRX50                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CPROFIL ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXRX550L ON P961.RTRX55                               
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DRELEVE DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSB110L ON P961.RTSB11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSG020L ON P961.RTSG02                               
        ( SECPAIE ASC                                                           
        , CSECTION ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSG030L ON P961.RTSG03                               
        ( COMPTE ASC                                                            
        , NETABADM ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSL400L ON P961.RTSL40                               
        ( NSOCIETE ASC                                                          
        , CLIEUINV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSL410L ON P961.RTSL41                               
        ( CLIEUINV ASC                                                          
        , NREGL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSL500L ON P961.RTSL50                               
        ( DINV ASC                                                              
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CLIEUINV ASC                                                          
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSL510L ON P961.RTSL51                               
        ( DINV ASC                                                              
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CEMPL ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSM010L ON P961.RTSM01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSM020L ON P961.RTSM02                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSP000L ON P961.RTSP00                               
        ( NENTCDE ASC                                                           
        , CFAM ASC                                                              
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSP050L ON P961.RTSP05                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSP100L ON P961.RTSP10                               
        ( NCODIC ASC                                                            
        , DJOUR DESC                                                            
        , NREC ASC                                                              
        , CMAJ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSP150L ON P961.RTSP15                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSP250L ON P961.RTSP25                               
        ( CMARQ ASC                                                             
        , NSOCDEST ASC                                                          
        , DPROV ASC                                                             
        , WTEBRB ASC                                                            
        , CTAUXTVA ASC                                                          
        , NLIEUDEST ASC                                                         
        , NLIEUORIG ASC                                                         
        , NSOCORIG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXSV010L ON P961.RTSV01                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CTPSAV ASC                                                            
        , DEFFET ASC                                                            
        , NSOCSAV ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXTF300L ON P961.RTTF30                               
        ( LFICHIER ASC                                                          
        , CRITERE ASC                                                           
        , DCREATION ASC                                                         
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXTF350L ON P961.RTTF35                               
        ( LFICHIER ASC                                                          
        , CRITERE ASC                                                           
        , DCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXTLZ20L ON P961.RTTLZZ                               
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , CTOURNEE ASC                                                          
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXTL020L ON P961.RTTL02                               
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , CTOURNEE ASC                                                          
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXTL060L ON P961.RTTL06                               
        ( NSOC ASC                                                              
        , CPROTOUR ASC                                                          
        , DDELIV ASC                                                            
        , CTOURNEE ASC                                                          
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXTL080L ON P961.RTTL08                               
        ( DDELIV ASC                                                            
        , CPROTOUR ASC                                                          
        , NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
        , CTOURNEE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXTL090L ON P961.RTTL09                               
        ( CLIVR ASC                                                             
        , DMMLIVR ASC                                                           
        , CRETOUR ASC                                                           
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXTL110L ON P961.RTTL11                               
        ( DDELIV ASC                                                            
        , CPROTOUR ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA050L ON P961.RTVA05                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA051L ON P961.RTVA05                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA060L ON P961.RTVA06                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA061L ON P961.RTVA06                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA100L ON P961.RTVA10                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA150L ON P961.RTVA15                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA160L ON P961.RTVA16                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA200L ON P961.RTVA20                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA250L ON P961.RTVA25                               
        ( CFAM ASC                                                              
        , CSEQRAYON ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA300L ON P961.RTVA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA310L ON P961.RTVA31                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVA500L ON P961.RTVA50                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , DOPER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVE020L ON P961.RTVE02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVE050L ON P961.RTVE05                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVE060L ON P961.RTVE06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVE080L ON P961.RTVE08                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQ ASC                                                              
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVE100L ON P961.RTVE10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE DESC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVE101L ON P961.RTVE10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVE110L ON P961.RTVE11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.RXVE140L ON P961.RTVE14                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.SXVA050L ON P961.STVA05                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.SXVA060L ON P961.STVA06                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.SXVA100L ON P961.STVA10                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.SXVA150L ON P961.STVA15                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.SXVA160L ON P961.STVA16                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.SXVA200L ON P961.STVA20                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.SXVA250L ON P961.STVA25                               
        ( CFAM ASC                                                              
        , CSEQRAYON ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.SXVA300L ON P961.STVA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P961.SXVA310L ON P961.STVA31                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P961.RXQTPSEPX ON P961.QTPSEPX                                    
        ( CFAM ASC                                                              
        , CGCPLT ASC                                                            
        , CTARIF ASC                                                            
        , WACTIF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.CCDIX_TTGV02 ON P961.TTGV02                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.CCDIX_TTGV11 ON P961.TTGV11                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.CCDIX_TTVE10 ON P961.TTVE10                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.TXGV020 ON P961.TTGV02                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
        , IBMSNAP_LOGMARKER ASC                                                 
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.TXGV021 ON P961.TTGV02                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.TXGV022 ON P961.TTGV02                                       
        ( IBMSNAP_LOGMARKER ASC                                                 
        , NVENTE ASC                                                            
        , DSYSTUPD ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.TXGV110 ON P961.TTGV11                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQNQ ASC                                                            
        , CTYPENREG ASC                                                         
        , IBMSNAP_LOGMARKER ASC                                                 
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.TXGV111 ON P961.TTGV11                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.TXGV112 ON P961.TTGV11                                       
        ( IBMSNAP_LOGMARKER ASC                                                 
        , NVENTE ASC                                                            
        , DSYSTUPD ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.TXVE101 ON P961.TTVE10                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P961.TXVE102 ON P961.TTVE10                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.CXGV020L ON P961.CTGV02                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.CXGV110L ON P961.CTGV11                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P961.CXVE100L ON P961.CTVE10                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
