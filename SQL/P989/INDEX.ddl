--MW CREATE  UNIQUE INDEX P989.RXIGSA0R ON P989.RTIGSA                               
--MW         ( DIGFA0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P989.RXIGSB0R ON P989.RTIGSB                               
--MW         ( DIGFA0 ASC                                                            
--MW         , NT_DIGSB ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P989.RXIGSC0R ON P989.RTIGSC                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFC0 ASC                                                            
--MW         , SM_DIGSC ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P989.RXIGSD0R ON P989.RTIGSD                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFD0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P989.RXIGSE0R ON P989.RTIGSE                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFE0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P989.RXIGSF0R ON P989.RTIGSF                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFF0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
CREATE  INDEX P989.RXAI241M ON P989.RTAI24                                      
        ( FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXAI531M ON P989.RTAI53                                      
        ( FSSKU ASC                                                             
        , FSFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXAN000M ON P989.RTAN00                                      
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXAN001M ON P989.RTAN00                                      
        ( ENRID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXBA001M ON P989.RTBA00                                      
        ( NFACTREM ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXBA011M ON P989.RTBA01                                      
        ( CTIERSBA ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXBA012M ON P989.RTBA01                                      
        ( NCHRONO ASC                                                           
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXBA013M ON P989.RTBA01                                      
        ( DEMIS DESC                                                            
        , WANNUL ASC                                                            
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXBA014M ON P989.RTBA01                                      
        ( DEMIS ASC                                                             
        , WANNUL ASC                                                            
        , NBA DESC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXBA015M ON P989.RTBA01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXBA101M ON P989.RTBA10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NECART ASC                                                            
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXBA201M ON P989.RTBA20                                      
        ( NTIERSCV ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXCC031M ON P989.RTCC03                                      
        ( MOPAI ASC                                                             
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , TYPTRANS ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXCC041M ON P989.RTCC04                                      
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXCD301M ON P989.RTCD30                                      
        ( NCDE ASC                                                              
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXCD302M ON P989.RTCD30                                      
        ( NMAG ASC                                                              
        , NCODIC ASC                                                            
        , DREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXCD303M ON P989.RTCD30                                      
        ( NMAG ASC                                                              
        , NREC ASC                                                              
        , DVALIDATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXCE101M ON P989.RTCE10                                      
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , WETIQUETTE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXCE102M ON P989.RTCE10                                      
        ( NZONPRIX ASC                                                          
        , WETIQUETTE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXCE103M ON P989.RTCE10                                      
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXCE104M ON P989.RTCE10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXDC001M ON P989.RTDC00                                      
        ( NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXDC101M ON P989.RTDC10                                      
        ( NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXEG051M ON P989.RTEG05                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMPC ASC                                                         
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXEG101M ON P989.RTEG10                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXEG102M ON P989.RTEG10                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXEG103M ON P989.RTEG10                                      
        ( NOMETAT ASC                                                           
        , CHCOND1 ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXEG104M ON P989.RTEG10                                      
        ( NOMETAT ASC                                                           
        , CHCOND2 ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXEG900M ON P989.RTEG90                                      
        ( IDENTTS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXEXFA0M ON P989.RTEXFA                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE3 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXEXFD0M ON P989.RTEXFD                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXEX000M ON P989.RTEX00                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXEX100M ON P989.RTEX10                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXEX200M ON P989.RTEX20                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXEX300M ON P989.RTEX30                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXEX400M ON P989.RTEX40                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXEX500M ON P989.RTEX50                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXEX600M ON P989.RTEX60                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXEX700M ON P989.RTEX70                                      
        ( NTRI ASC                                                              
        , LRUPTURE2 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXEX900M ON P989.RTEX90                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXFF001M ON P989.RTFF00                                      
        ( NTIERS ASC                                                            
        , WANN ASC                                                              
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFF002M ON P989.RTFF00                                      
        ( DEVN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFF051M ON P989.RTFF05                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFF101M ON P989.RTFF10                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFF102M ON P989.RTFF10                                      
        ( NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFF111M ON P989.RTFF11                                      
        ( NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFF121M ON P989.RTFF12                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFF131M ON P989.RTFF13                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFF151M ON P989.RTFF15                                      
        ( NPIECEFAC ASC                                                         
        , DEXCPTFAC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFF860M ON P989.RTFF86                                      
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXFF870M ON P989.RTFF87                                      
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
--MW CREATE  INDEX P989.RXFF880M ON P989.RTFF88                                      
--MW        ( PREFT1 ASC                                                            
--MW        , DEXCPT ASC                                                            
--MW        , NPIECE ASC                                                            
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  INDEX P989.RXFF881M ON P989.RTFF88                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXFI101M ON P989.RTFI10                                      
        ( NMUTATION ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXFI102M ON P989.RTFI10                                      
        ( DCOMPTA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFM101M ON P989.RTFM10                                      
        ( WCOLLECTIF ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT121M ON P989.RTFT12                                      
        ( NJRN ASC                                                              
        , NEXER ASC                                                             
        , NSOC ASC                                                              
        , NETAB ASC                                                             
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT122M ON P989.RTFT12                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT131M ON P989.RTFT13                                      
        ( NREG ASC                                                              
        , NORDRE ASC                                                            
        , NPROPO ASC                                                            
        , NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT133M ON P989.RTFT13                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT134M ON P989.RTFT13                                      
        ( NREG ASC                                                              
        , NORDRE ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT141M ON P989.RTFT14                                      
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXFT211M ON P989.RTFT21                                      
        ( LETTRAGE DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT212M ON P989.RTFT21                                      
        ( COMPTE ASC                                                            
        , NSOC ASC                                                              
        , LETTRAGE ASC                                                          
        , CDEVISE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT214M ON P989.RTFT21                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT221M ON P989.RTFT22                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , SECTION ASC                                                           
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
        , MONTANT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT222M ON P989.RTFT22                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , RUBRIQUE ASC                                                          
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
        , MONTANT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT261M ON P989.RTFT26                                      
        ( BAN ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT271M ON P989.RTFT27                                      
        ( NTBENEF ASC                                                           
        , NFOUR ASC                                                             
        , CTRESO ASC                                                            
        , CDEVISE ASC                                                           
        , METHREG ASC                                                           
        , NAUX ASC                                                              
        , NAUXR ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT272M ON P989.RTFT27                                      
        ( NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXFT310M ON P989.RTFT31                                      
        ( SECTION ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXFT311 ON P989.RTFT31                                       
        ( RUBRIQUE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT312M ON P989.RTFT31                                      
        ( SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT313M ON P989.RTFT31                                      
        ( RUBRIQUE ASC                                                          
        , SECTION ASC                                                           
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXFT760M ON P989.RTFT76                                      
        ( DFACTURE ASC                                                          
        , NFACTURE ASC                                                          
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXFT761M ON P989.RTFT76                                      
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA001M ON P989.RTGA00                                      
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA002M ON P989.RTGA00                                      
        ( CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA003M ON P989.RTGA00                                      
        ( CMARQ ASC                                                             
        , LEMBALLAGE ASC                                                        
        , NCODIC ASC                                                            
        , CASSORT ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA004M ON P989.RTGA00                                      
        ( NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA005M ON P989.RTGA00                                      
        ( NCODIC ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA006M ON P989.RTGA00                                      
        ( CASSORT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA007M ON P989.RTGA00                                      
        ( CFAM ASC                                                              
        , CASSORT ASC                                                           
        , CAPPRO ASC                                                            
        , NCODIC ASC                                                            
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA008M ON P989.RTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA009M ON P989.RTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CASSORT ASC                                                           
        , D1RECEPT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA011A ON P989.RTGA01MA                                    
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA011J ON P989.RTGA01MJ                                    
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA011M ON P989.RTGA01                                      
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA031M ON P989.RTGA03                                      
        ( NCODICK ASC                                                           
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA101M ON P989.RTGA10                                      
        ( NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA102M ON P989.RTGA10                                      
        ( CTYPLIEU ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA103M ON P989.RTGA10                                      
        ( CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA120M ON P989.RTGA12                                      
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , CMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA141M ON P989.RTGA14                                      
        ( WSEQFAM ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA213M ON P989.RTGA21                                      
        ( WRAYONFAM ASC                                                         
        , CRAYONFAM ASC                                                         
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA251M ON P989.RTGA25                                      
        ( CMARKETING ASC                                                        
        , CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA302M ON P989.RTGA30                                      
        ( CPARAM ASC                                                            
        , LVPARAM ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA431M ON P989.RTGA43                                      
        ( CINSEE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXGA490M ON P989.RTGA49                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXGA561M ON P989.RTGA56                                      
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA581M ON P989.RTGA58                                      
        ( NCODICLIE ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA591M ON P989.RTGA59                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA592M ON P989.RTGA59                                      
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA621M ON P989.RTGA62                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA651M ON P989.RTGA65                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA652M ON P989.RTGA65                                      
        ( CSTATUT ASC                                                           
        , NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA661M ON P989.RTGA66                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA681M ON P989.RTGA68                                      
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA751M ON P989.RTGA75                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXGA752M ON P989.RTGA75                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXGB301M ON P989.RTGB30                                      
        ( NCODIC ASC                                                            
        , NMUTATION ASC                                                         
        , NLIEUENT ASC                                                          
        , NHS ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGB801M ON P989.RTGB80                                      
        ( NCODIC ASC                                                            
        , NMUTATION ASC                                                         
        , NLIEUENT ASC                                                          
        , NHS ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGCCC1M ON P989.RTGCCC                                      
        ( NLIEUMAJ ASC                                                          
        , WTOPE ASC                                                             
        , WMUTATION ASC                                                         
        , NSOCMAJ ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGCCC2M ON P989.RTGCCC                                      
        ( NCODIC ASC                                                            
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , NORIGINE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGG151M ON P989.RTGG15                                      
        ( NCODIC ASC                                                            
        , DDECISION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGG170M ON P989.RTGG17                                      
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DRELEVE ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXGG201M ON P989.RTGG20                                      
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGG202M ON P989.RTGG20                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DFINEFFET ASC                                                         
        , DEFFET DESC                                                           
        , PEXPTTC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGG203M ON P989.RTGG20                                      
        ( DFINEFFET ASC                                                         
        , DEFFET ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGG204M ON P989.RTGG20                                      
        ( NLIEU ASC                                                             
        , DFINEFFET ASC                                                         
        , DEFFET ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGG210M ON P989.RTGG21                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGG402M ON P989.RTGG40                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGG403M ON P989.RTGG40                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
        , DFINEFFET ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGG601M ON P989.RTGG60                                      
        ( WTRAITE ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXGG701M ON P989.RTGG70                                      
        ( NCODIC ASC                                                            
        , DREC DESC                                                             
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXGG702M ON P989.RTGG70                                      
        ( DREC DESC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXGP040M ON P989.RTGP04                                      
        ( WTYPMVT ASC                                                           
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGQ014M ON P989.RTGQ01                                      
        ( CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGQ015M ON P989.RTGQ01                                      
        ( CINSEE ASC                                                            
        , WCONTRA DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGQ031M ON P989.RTGQ03                                      
        ( CZONLIV ASC                                                           
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGQ061M ON P989.RTGQ06                                      
        ( CTYPE ASC                                                             
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGQ062M ON P989.RTGQ06                                      
        ( CINSEE ASC                                                            
        , CTYPE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGQ063M ON P989.RTGQ06                                      
        ( CPOSTAL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGR301M ON P989.RTGR30                                      
        ( DMVTREC ASC                                                           
        , WTLMELA ASC                                                           
        , CTYPMVT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGR302M ON P989.RTGR30                                      
        ( NENTCDE ASC                                                           
        , DJRECQUAI ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGR303M ON P989.RTGR30                                      
        ( NBL ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGR304M ON P989.RTGR30                                      
        ( NREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGR351M ON P989.RTGR35                                      
        ( NCDE ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGR353M ON P989.RTGR35                                      
        ( NBL ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGR354M ON P989.RTGR35                                      
        ( NREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGR355M ON P989.RTGR35                                      
        ( NENTCDEPT ASC                                                         
        , QREC ASC                                                              
        , QPRISE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGS301M ON P989.RTGS30                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGS311M ON P989.RTGS31                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGS420M ON P989.RTGS42                                      
        ( NSOCORIG ASC                                                          
        , NLIEUORIG ASC                                                         
        , NSSLIEUORIG ASC                                                       
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , NSSLIEUDEST ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXGS422M ON P989.RTGS42                                      
        ( DMVT ASC                                                              
        , DHMVT ASC                                                             
        , DMMVT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGS601M ON P989.RTGS60                                      
        ( NSOCTRT ASC                                                           
        , NLIEUTRT ASC                                                          
        , NSSLIEUTRT ASC                                                        
        , CLIEUTRT ASC                                                          
        , NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGS602M ON P989.RTGS60                                      
        ( NCODIC ASC                                                            
        , NSOCDEST ASC                                                          
        , NSOCTRT ASC                                                           
        , NLIEUTRT ASC                                                          
        , NSSLIEUTRT ASC                                                        
        , CLIEUTRT ASC                                                          
        , CSTATUTTRT ASC                                                        
        , LEMPLACT ASC                                                          
        , NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGS700M ON P989.RTGS70                                      
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXGV021M ON P989.RTGV02                                      
        ( LNOM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV022M ON P989.RTGV02                                      
        ( TELDOM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV023M ON P989.RTGV02                                      
        ( IDCLIENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV024M ON P989.RTGV02                                      
        ( TELBUR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV031M ON P989.RTGV03                                      
        ( EMAIL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV050M ON P989.RTGV05                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXGV081M ON P989.RTGV08                                      
        ( VALCTRL ASC                                                           
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV104M ON P989.RTGV10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV105M ON P989.RTGV10                                      
        ( NVENTO ASC                                                            
        , NLIEU ASC                                                             
        , TYPVTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV111M ON P989.RTGV11                                      
        ( CPROTOUR ASC                                                          
        , DDELIV ASC                                                            
        , CTYPENREG ASC                                                         
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV112M ON P989.RTGV11                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV113M ON P989.RTGV11                                      
        ( NCODIC ASC                                                            
        , WTOPELIVRE ASC                                                        
        , CTYPENREG ASC                                                         
        , CENREG ASC                                                            
        , WCQERESF ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV114M ON P989.RTGV11                                      
        ( DCOMPTA ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV115M ON P989.RTGV11                                      
        ( NSOCIETE ASC                                                          
        , DDELIV ASC                                                            
        , WTOPELIVRE ASC                                                        
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV116M ON P989.RTGV11                                      
        ( DCREATION ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV141M ON P989.RTGV14                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NTRANS DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV142M ON P989.RTGV14                                      
        ( DSAISIE ASC                                                           
        , NSOCP ASC                                                             
        , NLIEUP ASC                                                            
        , NVENTE ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV200M ON P989.RTGV20                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXGV231M ON P989.RTGV23                                      
        ( DCREATION1 ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV271M ON P989.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , CAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXGV272M ON P989.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DAUTORD ASC                                                           
        , NORDRE ASC                                                            
        , CAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXGV273M ON P989.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , DAUTORD ASC                                                           
        , NAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXGV300M ON P989.RTGV30                                      
        ( LNOM ASC                                                              
        , DVENTE ASC                                                            
        , DNAISSANCE ASC                                                        
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXGV352M ON P989.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NDOCENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV353M ON P989.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV355M ON P989.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
        , NDOCENLV DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV356M ON P989.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
        , NBONENLV DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXGV990M ON P989.RTGV99                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE001M ON P989.RTHE00                                      
        ( CTIERSELA ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE002M ON P989.RTHE00                                      
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE011M ON P989.RTHE01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE012M ON P989.RTHE01                                      
        ( CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE013M ON P989.RTHE01                                      
        ( NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE014M ON P989.RTHE01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE101M ON P989.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE102M ON P989.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE103M ON P989.RTHE10                                      
        ( NCODIC ASC                                                            
        , NSERIE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE104M ON P989.RTHE10                                      
        ( PDOSNASC ASC                                                          
        , DOSNASC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE105M ON P989.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NENVOI ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE107M ON P989.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WSOLDE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE108M ON P989.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CREFUS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  INDEX P989.RXHE110M ON P989.RTHE11                                      
--MW        ( NLIEUHED ASC                                                          
--MW        , NGHE ASC                                                              
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  INDEX P989.RXHE111M ON P989.RTHE11                                      
        ( NLIEUHED ASC                                                          
        , NGHE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXHE202M ON P989.RTHE20                                      
        ( NUMPAL ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHE221M ON P989.RTHE22                                      
        ( CLIEUHET ASC                                                          
        , CTIERS ASC                                                            
        , DENVOI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHN001M ON P989.RTHN00                                      
        ( CTIERSELA ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHN002M ON P989.RTHN00                                      
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHN003M ON P989.RTHN00                                      
        ( DTRAIT ASC                                                            
        , WSUPP ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHN011M ON P989.RTHN01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHN012M ON P989.RTHN01                                      
        ( CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHV021M ON P989.RTHV02                                      
        ( DVENTECIALE ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXHV071M ON P989.RTHV07                                      
        ( CETAT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXHV081M ON P989.RTHV08                                      
        ( DVENTECIALE ASC                                                       
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P989.RXHV600M ON P989.RTHV60                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXIE001M ON P989.RTIE00                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , CMODSTOCK ASC                                                         
        , CEMP13 ASC                                                            
        , CEMP23 ASC                                                            
        , CEMP33 ASC                                                            
        , CTYPEMPL3 ASC                                                         
        , NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXIE003M ON P989.RTIE00                                      
        ( NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXIE004M ON P989.RTIE00                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXIE052M ON P989.RTIE05                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXIE301M ON P989.RTIE30                                      
        ( "ZONE" ASC                                                            
        , SECTEUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXIN001M ON P989.RTIN00                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXIN050M ON P989.RTIN05                                      
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , DINVENTAIRE ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXIN901M ON P989.RTIN90                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NSSLIEU ASC                                                           
        , LIEUTRT ASC                                                           
        , DINVENTAIRE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXKIS01M ON P989.RTKIS0                                      
        ( FILIALE ASC                                                           
        , NPLAN ASC                                                             
        , NCOMPTE ASC                                                           
        , NUMSS ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXKIS11M ON P989.RTKIS1                                      
        ( FILIALE ASC                                                           
        , NPLAN ASC                                                             
        , NCOMPTE ASC                                                           
        , NUMSS ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXKIS21M ON P989.RTKIS2                                      
        ( CACIDOPER ASC                                                         
        , CODEOPER ASC                                                          
        , DATEOPER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXKIS31M ON P989.RTKIS3                                      
        ( CACIDOPER ASC                                                         
        , CODEOPER ASC                                                          
        , DATEOPER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXLG001 ON P989.RTLG00                                       
        ( CINSEE ASC                                                            
        , LNOMPVOIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXLG002 ON P989.RTLG00                                       
        ( CTVOIE ASC                                                            
        , LNOMVOIE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXLM000M ON P989.RTLM00                                      
        ( NLISTE ASC                                                            
        , NAPPORT ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DATTRANS ASC                                                          
        , NCAISSE ASC                                                           
        , NTRANS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXPR001M ON P989.RTPR00                                      
        ( CTYPPREST ASC                                                         
        , NENTCDE ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXPR002M ON P989.RTPR00                                      
        ( CTYPPREST ASC                                                         
        , NENTCDE ASC                                                           
        , CASSORT ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXPR011M ON P989.RTPR01                                      
        ( CFAM ASC                                                              
        , WAFFICH ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXPR061M ON P989.RTPR06                                      
        ( CSTATUT ASC                                                           
        , LSTATUT ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXPR062M ON P989.RTPR06                                      
        ( CSTATUT ASC                                                           
        , DEFFET ASC                                                            
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXPR063M ON P989.RTPR06                                      
        ( CSTATUT ASC                                                           
        , CPRESTATION ASC                                                       
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXPS001M ON P989.RTPS00                                      
        ( NOMCLI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXQC011M ON P989.RTQC01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXQC111M ON P989.RTQC11                                      
        ( DTESSI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXQC112M ON P989.RTQC11                                      
        ( NCLIENT ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P989.RXQC113M ON P989.RTQC11                                      
        ( EMAIL ASC                                                             
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P989.RXQC121M ON P989.RTQC12                                      
        ( DENVOI_QV ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXRA001M ON P989.RTRA00                                      
        ( CTYPDEMANDE ASC                                                       
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXRA002M ON P989.RTRA00                                      
        ( NAVOIRCLIENT ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXRD000M ON P989.RTRD00                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXRE600M ON P989.RTRE60                                      
        ( CTYPENREG ASC                                                         
        , WTYPE1 ASC                                                            
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
        , CPREST ASC                                                            
        , CENREG ASC                                                            
        , WTYPE2 ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXRE610M ON P989.RTRE61                                      
        ( CTYPENREG ASC                                                         
        , WTYPE1 ASC                                                            
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
        , CPREST ASC                                                            
        , CENREG ASC                                                            
        , WTYPE2 ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXRX251M ON P989.RTRX25                                      
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXRX300M ON P989.RTRX30                                      
        ( NRELEVE ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXRX551M ON P989.RTRX55                                      
        ( NCONC ASC                                                             
        , DRELEVE DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXSP101M ON P989.RTSP10                                      
        ( NCODIC ASC                                                            
        , DJOUR DESC                                                            
        , SRP ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXSP200M ON P989.RTSP20                                      
        ( DTRAITEMENT ASC                                                       
        , CPROG ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXSP201M ON P989.RTSP20                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXTL021M ON P989.RTTL02                                      
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXTL022M ON P989.RTTL02                                      
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , CPROTOUR ASC                                                          
        , CTOURNEE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXTL040M ON P989.RTTL04                                      
        ( NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXTL111M ON P989.RTTL11                                      
        ( NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXVA000M ON P989.RTVA00                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXVA001M ON P989.RTVA00                                      
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXVE021M ON P989.RTVE02                                      
        ( LNOM ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXVE022M ON P989.RTVE02                                      
        ( TELDOM ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXVE102M ON P989.RTVE10                                      
        ( NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXVE103M ON P989.RTVE10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.RXVE111M ON P989.RTVE11                                      
        ( NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXVE113M ON P989.RTVE11                                      
        ( NCODIC ASC                                                            
        , WTOPELIVRE ASC                                                        
        , CTYPENREG ASC                                                         
        , CENREG ASC                                                            
        , WCQERESF ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXVE114M ON P989.RTVE11                                      
        ( DCOMPTA ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXVE115M ON P989.RTVE11                                      
        ( NSOCIETE ASC                                                          
        , DDELIV ASC                                                            
        , WTOPELIVRE ASC                                                        
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXVE116M ON P989.RTVE11                                      
        ( DCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXVE141M ON P989.RTVE14                                      
        ( DSAISIE ASC                                                           
        , NSOCP ASC                                                             
        , NLIEUP ASC                                                            
        , NVENTE ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P989.RXVI010M ON P989.RTVI01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.SXVA000M ON P989.STVA00                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD060M ON P989.RTAD06                               
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD070M ON P989.RTAD07                               
        ( CSOC ASC                                                              
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD071M ON P989.RTAD07                               
        ( NENTCDEK ASC                                                          
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD100M ON P989.RTAD10                               
        ( CSOC ASC                                                              
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD101M ON P989.RTAD10                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD102M ON P989.RTAD10                               
        ( NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD110M ON P989.RTAD11                               
        ( CSOC ASC                                                              
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD111M ON P989.RTAD11                               
        ( CFAMK ASC                                                             
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD140M ON P989.RTAD14                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD510M ON P989.RTAD51                               
        ( CSOC ASC                                                              
        , CCOULEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD511M ON P989.RTAD51                               
        ( CCOULEURK ASC                                                         
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD550M ON P989.RTAD55                               
        ( CUSINE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD610M ON P989.RTAD61                               
        ( CSOC ASC                                                              
        , CPAYS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAD611M ON P989.RTAD61                               
        ( CPAYSK ASC                                                            
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAI240M ON P989.RTAI24                               
        ( FDCLAS ASC                                                            
        , FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAI270M ON P989.RTAI27                               
        ( CDESCRIPTIF ASC                                                       
        , FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAI530M ON P989.RTAI53                               
        ( FSSKU ASC                                                             
        , FSCTL ASC                                                             
        , FSCLAS ASC                                                            
        , FSFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAN010M ON P989.RTAN01                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
        , CDEST ASC                                                             
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAV010M ON P989.RTAV01                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
        , CTYPAVOIR ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAV020M ON P989.RTAV02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
        , CTYPAVOIR ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXAV030M ON P989.RTAV03                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXBA000M ON P989.RTBA00                               
        ( NFACTBA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXBA010M ON P989.RTBA01                               
        ( NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXBA100M ON P989.RTBA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NECART ASC                                                            
        , CMODRGLT ASC                                                          
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXBA200M ON P989.RTBA20                               
        ( CTIERSBA ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXBA210M ON P989.RTBA21                               
        ( CTIERSBA ASC                                                          
        , DANNEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXBA300M ON P989.RTBA30                               
        ( CTIERSBA ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXBC310 ON P989.RTBC31                                
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXBC320M ON P989.RTBC32                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXBC330M ON P989.RTBC33                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXBC340M ON P989.RTBC34                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXBC350M ON P989.RTBC35                               
        ( CPROGRAMME ASC                                                        
        , DDEBUT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCC000M ON P989.RTCC00                               
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCC010M ON P989.RTCC01                               
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCC020M ON P989.RTCC02                               
        ( DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCC030M ON P989.RTCC03                               
        ( TYPTRANS ASC                                                          
        , MOPAI ASC                                                             
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NOPERATEUR ASC                                                        
        , HTRAIT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCC040M ON P989.RTCC04                               
        ( CPAICPT ASC                                                           
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCC050M ON P989.RTCC05                               
        ( NSOC ASC                                                              
        , DJOUR ASC                                                             
        , NSEQ ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCD000M ON P989.RTCD00                               
        ( NCODIC ASC                                                            
        , NMAGGRP ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCD050M ON P989.RTCD05                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCD100M ON P989.RTCD10                               
        ( DSEMAINE ASC                                                          
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCD150M ON P989.RTCD15                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCD200M ON P989.RTCD20                               
        ( NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCD250M ON P989.RTCD25                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCD300M ON P989.RTCD30                               
        ( DCDE ASC                                                              
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCD350M ON P989.RTCD35                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCD500M ON P989.RTCD50                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCE100M ON P989.RTCE10                               
        ( NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCE200M ON P989.RTCE20                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCT000M ON P989.RTCT00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DDELIV ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXCX000M ON P989.RTCX00                               
        ( NOECS ASC                                                             
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , COMPTECG ASC                                                          
        , DATEFF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXDC000M ON P989.RTDC00                               
        ( NDOSSIER ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXDC100M ON P989.RTDC10                               
        ( NDOSSIER ASC                                                          
        , NECHEANCE ASC                                                         
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXDC200M ON P989.RTDC20                               
        ( NDOSSIER ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXDS000M ON P989.RTDS00                               
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CAPPRO ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXDS050M ON P989.RTDS05                               
        ( NCODIC ASC                                                            
        , DSEMESTRE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXDS100M ON P989.RTDS10                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXEF100M ON P989.RTEF10                               
        ( CTRAIT ASC                                                            
        , CTIERS ASC                                                            
        , NRENDU ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXEG000M ON P989.RTEG00                               
        ( NOMETAT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXEG050M ON P989.RTEG05                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXEG100M ON P989.RTEG10                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXEG110M ON P989.RTEG11                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXEG150M ON P989.RTEG15                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXEG200M ON P989.RTEG20                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXEG250M ON P989.RTEG25                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , LIGNECALC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXEG300M ON P989.RTEG30                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXEG400M ON P989.RTEG40                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFA010M ON P989.RTFA01                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFA020M ON P989.RTFA02                               
        ( NSOCV ASC                                                             
        , NLIEUV ASC                                                            
        , NORDV ASC                                                             
        , NSOCF ASC                                                             
        , NLIEUF ASC                                                            
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF000M ON P989.RTFF00                               
        ( NPIECE ASC                                                            
        , DEXCPT DESC                                                           
        , PREFT1 ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF050M ON P989.RTFF05                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF060M ON P989.RTFF06                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF100M ON P989.RTFF10                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF110M ON P989.RTFF11                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF120M ON P989.RTFF12                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF130M ON P989.RTFF13                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF150M ON P989.RTFF15                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF200M ON P989.RTFF20                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNEFAC ASC                                                         
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF210M ON P989.RTFF21                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF250M ON P989.RTFF25                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , WTYPESEQ ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF300M ON P989.RTFF30                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF350M ON P989.RTFF35                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF360M ON P989.RTFF36                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF370M ON P989.RTFF37                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF400M ON P989.RTFF40                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF410M ON P989.RTFF41                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF420M ON P989.RTFF42                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF430M ON P989.RTFF43                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF440M ON P989.RTFF44                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF450M ON P989.RTFF45                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF500M ON P989.RTFF50                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
        , DEXCPTLIT ASC                                                         
        , NPIECELIT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF510M ON P989.RTFF51                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , DEXCPTLIT ASC                                                         
        , NPIECELIT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF600M ON P989.RTFF60                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF650M ON P989.RTFF65                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF660M ON P989.RTFF66                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF670M ON P989.RTFF67                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF700M ON P989.RTFF70                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF750M ON P989.RTFF75                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF800M ON P989.RTFF80                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF810M ON P989.RTFF81                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFF850M ON P989.RTFF85                               
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFG000 ON P989.RTFG00                                
        ( CODEAPPLI ASC                                                         
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
        , DCREAT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFI000M ON P989.RTFI00                               
        ( CPROF ASC                                                             
        , QTAUX ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFI050M ON P989.RTFI05                               
        ( NENTCDE ASC                                                           
        , CFAM ASC                                                              
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM010M ON P989.RTFM01                               
        ( NENTITE ASC                                                           
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM020M ON P989.RTFM02                               
        ( CDEVISE ASC                                                           
        , WSENS ASC                                                             
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM030M ON P989.RTFM03                               
        ( NENTITE ASC                                                           
        , CMETHODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM040M ON P989.RTFM04                               
        ( CDEVISE ASC                                                           
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM050M ON P989.RTFM05                               
        ( CDEVORIG ASC                                                          
        , CDEVDEST ASC                                                          
        , DEFFET ASC                                                            
        , COPERAT ASC                                                           
        , PTAUX ASC                                                             
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM060M ON P989.RTFM06                               
        ( CETAT ASC                                                             
        , NDEMANDE ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM100M ON P989.RTFM10                               
        ( NENTITE ASC                                                           
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM200M ON P989.RTFM20                               
        ( NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM230M ON P989.RTFM23                               
        ( NENTITE ASC                                                           
        , NEXER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM540M ON P989.RTFM54                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM550M ON P989.RTFM55                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM560M ON P989.RTFM56                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM570M ON P989.RTFM57                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM580M ON P989.RTFM58                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM641M ON P989.RTFM64                               
        ( STEAPP ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM691 ON P989.RTFM69                                
        ( COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM730M ON P989.RTFM73                               
        ( CNOMPROG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM801M ON P989.RTFM80                               
        ( CNATOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM811M ON P989.RTFM81                               
        ( CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM821M ON P989.RTFM82                               
        ( CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM831M ON P989.RTFM83                               
        ( CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM841M ON P989.RTFM84                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CTVA ASC                                                              
        , CGEO ASC                                                              
        , WGROUPE ASC                                                           
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM871M ON P989.RTFM87                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM881M ON P989.RTFM88                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM891M ON P989.RTFM89                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM901M ON P989.RTFM90                               
        ( CINTERFACE ASC                                                        
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM910M ON P989.RTFM91                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM921M ON P989.RTFM92                               
        ( CZONE ASC                                                             
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFM930M ON P989.RTFM93                               
        ( CRITLIEU1 ASC                                                         
        , CRITLIEU2 ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT010M ON P989.RTFT01                               
        ( NAUX ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT020M ON P989.RTFT02                               
        ( CACID ASC                                                             
        , NAUX ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT030M ON P989.RTFT03                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT040M ON P989.RTFT04                               
        ( CBANQUE ASC                                                           
        , METHREG ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT050M ON P989.RTFT05                               
        ( CBANQUE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT060M ON P989.RTFT06                               
        ( NSOC ASC                                                              
        , CPTR ASC                                                              
        , NAUX ASC                                                              
        , CREGTVA ASC                                                           
        , NATURE ASC                                                            
        , NPER ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT070M ON P989.RTFT07                               
        ( CETAT ASC                                                             
        , CSERVICE ASC                                                          
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT080M ON P989.RTFT08                               
        ( CETAT ASC                                                             
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT090M ON P989.RTFT09                               
        ( CETAT ASC                                                             
        , CSERVICE ASC                                                          
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT100M ON P989.RTFT10                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT110M ON P989.RTFT11                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , WTYPEADR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT120M ON P989.RTFT12                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT130M ON P989.RTFT13                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , DECH ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT140M ON P989.RTFT14                               
        ( CBANQUE ASC                                                           
        , METHREG ASC                                                           
        , NREG ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT150M ON P989.RTFT15                               
        ( NORDRE ASC                                                            
        , NPROPO ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT160M ON P989.RTFT16                               
        ( NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT170M ON P989.RTFT17                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NAUX ASC                                                              
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT180M ON P989.RTFT18                               
        ( METHREG ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT190M ON P989.RTFT19                               
        ( CTRAIT ASC                                                            
        , CACID ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT200M ON P989.RTFT20                               
        ( NAUX ASC                                                              
        , CDEVISE ASC                                                           
        , METHREG ASC                                                           
        , CTRESO ASC                                                            
        , WACCES ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT210M ON P989.RTFT21                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NLIGNE ASC                                                            
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT220M ON P989.RTFT22                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , COMPTE ASC                                                            
        , SSCOMPTE ASC                                                          
        , SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
        , STEAPP ASC                                                            
        , CDEVISE ASC                                                           
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , WSENS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT230M ON P989.RTFT23                               
        ( NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT250M ON P989.RTFT25                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT260M ON P989.RTFT26                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT270M ON P989.RTFT27                               
        ( NLIGNE ASC                                                            
        , NPIECE ASC                                                            
        , DECH ASC                                                              
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NAUXR ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT280M ON P989.RTFT28                               
        ( METHREG ASC                                                           
        , NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT290M ON P989.RTFT29                               
        ( NCODE ASC                                                             
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT300M ON P989.RTFT30                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NATURE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT320M ON P989.RTFT32                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFERENCE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT321M ON P989.RTFT32                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , COMPTE ASC                                                            
        , SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT330M ON P989.RTFT33                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFERENCE ASC                                                         
        , NVENTIL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT340M ON P989.RTFT34                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT350 ON P989.RTFT35                                
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , REFDOC ASC                                                            
        , CDEVISE ASC                                                           
        , WTRT ASC                                                              
        , NEXER ASC                                                             
        , NPER ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT360 ON P989.RTFT36                                
        ( REFDOC ASC                                                            
        , NJRN ASC                                                              
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT370 ON P989.RTFT37                                
        ( NJRN ASC                                                              
        , REFDOC ASC                                                            
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT380 ON P989.RTFT38                                
        ( REFDOC ASC                                                            
        , NJRN ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT390 ON P989.RTFT39                                
        ( NLIGNE ASC                                                            
        , REFDOC ASC                                                            
        , NJRN ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT400 ON P989.RTFT40                                
        ( CATCH ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT410 ON P989.RTFT41                                
        ( NATCH ASC                                                             
        , CATCH ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT430M ON P989.RTFT43                               
        ( NPIECE DESC                                                           
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , CDEVISE ASC                                                           
        , NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT440M ON P989.RTFT44                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT450 ON P989.RTFT45                                
        ( CPOND ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT460 ON P989.RTFT46                                
        ( NPER ASC                                                              
        , CPOND ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT490M ON P989.RTFT49                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NATURE ASC                                                            
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT500M ON P989.RTFT50                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT510M ON P989.RTFT51                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT520M ON P989.RTFT52                               
        ( CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT530M ON P989.RTFT53                               
        ( CACID ASC                                                             
        , NSOC ASC                                                              
        , NETAB ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT540M ON P989.RTFT54                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , DECH ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT570M ON P989.RTFT57                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NAUX ASC                                                              
        , NTIERSCV ASC                                                          
        , WTYPEADR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT601M ON P989.RTFT60                               
        ( COMPTE ASC                                                            
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT611M ON P989.RTFT61                               
        ( SECTION ASC                                                           
        , NSOC ASC                                                              
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT621M ON P989.RTFT62                               
        ( RUBRIQUE ASC                                                          
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT631M ON P989.RTFT63                               
        ( NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT651M ON P989.RTFT65                               
        ( CMASQUEC ASC                                                          
        , CMASQUES ASC                                                          
        , CMASQUER ASC                                                          
        , CMASQUEE ASC                                                          
        , CMASQUEA ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT660M ON P989.RTFT66                               
        ( COMPTE ASC                                                            
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT670M ON P989.RTFT67                               
        ( SECTION ASC                                                           
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT680M ON P989.RTFT68                               
        ( RUBRIQUE ASC                                                          
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT700M ON P989.RTFT70                               
        ( COMPTE ASC                                                            
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT710M ON P989.RTFT71                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFDOC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT720M ON P989.RTFT72                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFDOC ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT730M ON P989.RTFT73                               
        ( NSOC ASC                                                              
        , CNOMPROG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT740M ON P989.RTFT74                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , CNOMPROG ASC                                                          
        , DDATE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT750M ON P989.RTFT75                               
        ( DDEMANDE DESC                                                         
        , NTIERS ASC                                                            
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT851M ON P989.RTFT85                               
        ( CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT861M ON P989.RTFT86                               
        ( DEFFET ASC                                                            
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT871M ON P989.RTFT87                               
        ( SECTIONO ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT881M ON P989.RTFT88                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT891M ON P989.RTFT89                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFT900M ON P989.RTFT90                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX000M ON P989.RTFX00                               
        ( NOECS ASC                                                             
        , DATEFF DESC                                                           
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , WFX00 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX001M ON P989.RTFX00                               
        ( NOECS ASC                                                             
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , WFX00 ASC                                                             
        , DATEFF DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX010M ON P989.RTFX01                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX050M ON P989.RTFX05                               
        ( NJRN ASC                                                              
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX100M ON P989.RTFX10                               
        ( COMPTEGL ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX110M ON P989.RTFX11                               
        ( CZONE ASC                                                             
        , ZONEGL ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX150M ON P989.RTFX15                               
        ( WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX200M ON P989.RTFX20                               
        ( CTYPSTAT ASC                                                          
        , NDEPTLMELA ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX300M ON P989.RTFX30                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX350M ON P989.RTFX35                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX400M ON P989.RTFX40                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX450M ON P989.RTFX45                               
        ( CODEAPPLI ASC                                                         
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
        , DCREAT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXFX500M ON P989.RTFX50                               
        ( DATTRANS ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CTYPTRANS ASC                                                         
        , CAPPRET ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA000M ON P989.RTGA00                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA010A ON P989.RTGA01MA                             
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA010J ON P989.RTGA01MJ                             
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA010M ON P989.RTGA01                               
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA030M ON P989.RTGA03                               
        ( CSOC ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA040M ON P989.RTGA04                               
        ( NCONC ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA060M ON P989.RTGA06                               
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA070M ON P989.RTGA07                               
        ( CGRPFOURN ASC                                                         
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA080M ON P989.RTGA08                               
        ( NENTCDE ASC                                                           
        , CINTERLOCUT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA090M ON P989.RTGA09                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , WSEQED ASC                                                            
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CMARKETIN3 ASC                                                        
        , CVMARKETIN3 ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA100M ON P989.RTGA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA110M ON P989.RTGA11                               
        ( CETAT ASC                                                             
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA130M ON P989.RTGA13                               
        ( CFAM ASC                                                              
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA140M ON P989.RTGA14                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA150M ON P989.RTGA15                               
        ( CFAM ASC                                                              
        , CTYPDCL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA160M ON P989.RTGA16                               
        ( CFAM ASC                                                              
        , CGARANTCPLT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA170M ON P989.RTGA17                               
        ( CFAM ASC                                                              
        , CARACTSPE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA180M ON P989.RTGA18                               
        ( CFAM ASC                                                              
        , CDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA200M ON P989.RTGA20                               
        ( CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA210M ON P989.RTGA21                               
        ( CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA211M ON P989.RTGA21                               
        ( WRAYONFAM ASC                                                         
        , CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA220M ON P989.RTGA22                               
        ( CMARQ ASC                                                             
        , LMARQ ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA230M ON P989.RTGA23                               
        ( CMARKETING ASC                                                        
        , LMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA240M ON P989.RTGA24                               
        ( CDESCRIPTIF ASC                                                       
        , LDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA250M ON P989.RTGA25                               
        ( CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
        , CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA260M ON P989.RTGA26                               
        ( CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA270M ON P989.RTGA27                               
        ( CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA290M ON P989.RTGA29                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , WSEQED ASC                                                            
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  UNIQUE INDEX P989.RXGA300M ON P989.RTGA30                               
--MW        ( CPARAM ASC                                                            
--MW        , CFAM ASC                                                              
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA301M ON P989.RTGA30                               
        ( CPARAM ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA310M ON P989.RTGA31                               
        ( NCODIC ASC                                                            
        , NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA320M ON P989.RTGA32                               
        ( NCODIC ASC                                                            
        , NSEQUENCE ASC                                                         
        , NEANCOMPO ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA330M ON P989.RTGA33                               
        ( NCODIC ASC                                                            
        , CDEST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA390M ON P989.RTGA39                               
        ( NENTSAP ASC                                                           
        , NENTANC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA400M ON P989.RTGA40                               
        ( CGARANTIE ASC                                                         
        , CFAM ASC                                                              
        , CTARIF ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA410M ON P989.RTGA41                               
        ( CGCPLT ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA420M ON P989.RTGA42                               
        ( CGCPLT ASC                                                            
        , NMOIS ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA430M ON P989.RTGA43                               
        ( CSECTEUR ASC                                                          
        , CINSEE ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA440M ON P989.RTGA44                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA450M ON P989.RTGA45                               
        ( CFAM ASC                                                              
        , CGRP ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA460M ON P989.RTGA46                               
        ( CGRP ASC                                                              
        , CSECT1 ASC                                                            
        , ZONE1 ASC                                                             
        , CSECT2 ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA480M ON P989.RTGA48                               
        ( NCODIC ASC                                                            
        , SPPRTP ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA510M ON P989.RTGA51                               
        ( NCODIC ASC                                                            
        , CTYPDCL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA520M ON P989.RTGA52                               
        ( NCODIC ASC                                                            
        , CGARANTCPLT ASC                                                       
        , CVGARANCPLT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA530M ON P989.RTGA53                               
        ( NCODIC ASC                                                            
        , CDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA550M ON P989.RTGA55                               
        ( NCODIC ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA560M ON P989.RTGA56                               
        ( NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA580M ON P989.RTGA58                               
        ( NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , CTYPLIEN ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA590M ON P989.RTGA59                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA600M ON P989.RTGA60                               
        ( NCODIC ASC                                                            
        , NMESSDEPOT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA620M ON P989.RTGA62                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA630M ON P989.RTGA63                               
        ( NCODIC ASC                                                            
        , CARACTSPE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA640M ON P989.RTGA64                               
        ( NCODIC ASC                                                            
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA650M ON P989.RTGA65                               
        ( NCODIC ASC                                                            
        , CSTATUT ASC                                                           
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA660M ON P989.RTGA66                               
        ( NCODIC ASC                                                            
        , CSENS ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA680M ON P989.RTGA68                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA710M ON P989.RTGA71                               
        ( CTABLE ASC                                                            
        , CSTABLE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA720M ON P989.RTGA72                               
        ( CNUMCONT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA730M ON P989.RTGA73                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA740M ON P989.RTGA74                               
        ( NCODIC ASC                                                            
        , NTITRE ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA750M ON P989.RTGA75                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA760M ON P989.RTGA76                               
        ( CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA770M ON P989.RTGA77                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA790M ON P989.RTGA79                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA810M ON P989.RTGA81                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA830M ON P989.RTGA83                               
        ( NCODIC ASC                                                            
        , CMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA910M ON P989.RTGA91                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA920M ON P989.RTGA92                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA930M ON P989.RTGA93                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA940M ON P989.RTGA94                               
        ( CPROG ASC                                                             
        , CCHAMP ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGA990M ON P989.RTGA99                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGB300M ON P989.RTGB30                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGB800M ON P989.RTGB80                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGCCC0M ON P989.RTGCCC                               
        ( NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGD990M ON P989.RTGD99                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGF350M ON P989.RTGF35                               
        ( DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
        , NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGF400M ON P989.RTGF40                               
        ( NENTCDE ASC                                                           
        , CINTERLOCUT ASC                                                       
        , WMARQFAM ASC                                                          
        , CMARQFAM ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGF500M ON P989.RTGF50                               
        ( NCDE ASC                                                              
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGGXX0M ON P989.RTGGXX                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGG050M ON P989.RTGG05                               
        ( NCONC ASC                                                             
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGG100M ON P989.RTGG10                               
        ( NDEMALI ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGG150M ON P989.RTGG15                               
        ( NDEMALI ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGG200M ON P989.RTGG20                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGG250M ON P989.RTGG25                               
        ( CHEFPROD ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGG301M ON P989.RTGG30                               
        ( DANNEE ASC                                                            
        , DMOIS ASC                                                             
        , CFAM ASC                                                              
        , CHEFPROD ASC                                                          
        , NCONC ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGG400M ON P989.RTGG40                               
        ( NCODIC ASC                                                            
        , DEFFET ASC                                                            
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGG500M ON P989.RTGG50                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGG550M ON P989.RTGG55                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGG600M ON P989.RTGG60                               
        ( NCODIC ASC                                                            
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , DDEMANDE ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGG700M ON P989.RTGG70                               
        ( NCODIC ASC                                                            
        , NREC ASC                                                              
        , NCDE ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGI1000M ON P989.RTGI10                              
        ( NSOCIETE ASC                                                          
        , CTABINT ASC                                                           
        , TTABINT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGI1200M ON P989.RTGI12                              
        ( NSOCIETE ASC                                                          
        , CTABINTMB ASC                                                         
        , MB ASC                                                                
        , TMB ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGI1400M ON P989.RTGI14                              
        ( NSOCIETE ASC                                                          
        , CTABINTCA ASC                                                         
        , CA ASC                                                                
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGI1600M ON P989.RTGI16                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGI2000M ON P989.RTGI20                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CTYPEAFF ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGI2100M ON P989.RTGI21                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CTYPEAFF ASC                                                          
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGI2200M ON P989.RTGI22                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGI2300M ON P989.RTGI23                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGI2400M ON P989.RTGI24                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGJ100M ON P989.RTGJ10                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGJ400M ON P989.RTGJ40                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CSIMU ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGM010M ON P989.RTGM01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGM060M ON P989.RTGM06                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , CZPRIX ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGM700M ON P989.RTGM70                               
        ( NSOCIETE ASC                                                          
        , NZONE ASC                                                             
        , CHEFPROD ASC                                                          
        , WSEQFAM ASC                                                           
        , LVMARKET1 ASC                                                         
        , LVMARKET2 ASC                                                         
        , LVMARKET3 ASC                                                         
        , NCODIC2 ASC                                                           
        , NCODIC ASC                                                            
        , "SEQUENCE" ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGP000M ON P989.RTGP00                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGP010M ON P989.RTGP01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGP020M ON P989.RTGP02                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , DLIVRAISON ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGP030M ON P989.RTGP03                               
        ( DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGP041M ON P989.RTGP04                               
        ( NCODIC ASC                                                            
        , WTYPMVT ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGP050M ON P989.RTGP05                               
        ( CRAYON ASC                                                            
        , WRAYONFAM ASC                                                         
        , DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGQ000M ON P989.RTGQ00                               
        ( CMODDEL ASC                                                           
        , CEQUIP ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGQ011M ON P989.RTGQ01                               
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGQ020M ON P989.RTGQ02                               
        ( CMODDEL ASC                                                           
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGQ030M ON P989.RTGQ03                               
        ( CMODDEL ASC                                                           
        , CEQUIP ASC                                                            
        , DJOUR ASC                                                             
        , CZONLIV ASC                                                           
        , CPLAGE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGQ040M ON P989.RTGQ04                               
        ( CMODDEL ASC                                                           
        , CPERIMETRE ASC                                                        
        , DEFFET ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGQ041M ON P989.RTGQ04                               
        ( CMODDEL ASC                                                           
        , CPERIMETRE ASC                                                        
        , CFAM ASC                                                              
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGQ120M ON P989.RTGQ12                               
        ( CINSEE ASC                                                            
        , CGRP ASC                                                              
        , CTYPGRP ASC                                                           
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGQ200M ON P989.RTGQ20                               
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
        , CCADRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGR300M ON P989.RTGR30                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NAVENANT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGR350M ON P989.RTGR35                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , WTLMELA ASC                                                           
        , NCODIC ASC                                                            
        , NAVENANT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGR352M ON P989.RTGR35                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , WTLMELA ASC                                                           
        , NCODIC ASC                                                            
        , NAVENANT ASC                                                          
        , QREC ASC                                                              
        , QPRISE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGS300M ON P989.RTGS30                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , WSTOCKMASQ ASC                                                        
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGS310M ON P989.RTGS31                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , WSTOCKMASQ ASC                                                        
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGS450M ON P989.RTGS45                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGS500M ON P989.RTGS50                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGS550M ON P989.RTGS55                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGS600M ON P989.RTGS60                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV020M ON P989.RTGV02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV030M ON P989.RTGV03                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV040M ON P989.RTGV04                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV060M ON P989.RTGV06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV080M ON P989.RTGV08                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQ ASC                                                              
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV100M ON P989.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV101M ON P989.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV102M ON P989.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV110M ON P989.RTGV11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV119M ON P989.RTGV11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV130M ON P989.RTGV13                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTATION ASC                                                       
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV140M ON P989.RTGV14                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV150M ON P989.RTGV15                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV230M ON P989.RTGV23                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
        , DCREATION ASC                                                         
        , HCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV250M ON P989.RTGV25                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTATION ASC                                                       
        , NSEQ ASC                                                              
        , DCREATION ASC                                                         
        , HCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV260M ON P989.RTGV26                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV270M ON P989.RTGV27                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DAUTORD ASC                                                           
        , NAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV310M ON P989.RTGV31                               
        ( CVENDEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV350M ON P989.RTGV35                               
        ( NSOCIETE ASC                                                          
        , NBONENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV400M ON P989.RTGV40                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXGV410M ON P989.RTGV41                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , CTYPADR ASC                                                           
        , CMODDEL ASC                                                           
        , DDELIV ASC                                                            
        , CPLAGE ASC                                                            
        , NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHA300M ON P989.RTHA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHC000M ON P989.RTHC00                               
        ( NCHS ASC                                                              
        , NLIEUHC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHC030M ON P989.RTHC03                               
        ( NCHS ASC                                                              
        , NSEQ ASC                                                              
        , NLIEUHC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHE000M ON P989.RTHE00                               
        ( CTIERS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHE010M ON P989.RTHE01                               
        ( CTIERS ASC                                                            
        , CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHE100M ON P989.RTHE10                               
        ( NLIEUHED ASC                                                          
        , NGHE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHE150M ON P989.RTHE15                               
        ( CLIEUHET ASC                                                          
        , NLIEUHED ASC                                                          
        , NGHE ASC                                                              
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHE200M ON P989.RTHE20                               
        ( NLIEUHED ASC                                                          
        , CLIEUHET ASC                                                          
        , NRETOUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHE210M ON P989.RTHE21                               
        ( CLIEUHET ASC                                                          
        , CLIEUHEI ASC                                                          
        , NLOTDIAG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHE220M ON P989.RTHE22                               
        ( CLIEUHET ASC                                                          
        , NENVOI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHE230M ON P989.RTHE23                               
        ( CLIEUHET ASC                                                          
        , NDEMANDE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHL000M ON P989.RTHL00                               
        ( NOMMAP ASC                                                            
        , POSIT ASC                                                             
        , ZECRAN ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHL050M ON P989.RTHL05                               
        ( CTABLE ASC                                                            
        , CCHAMPR ASC                                                           
        , CCHAMPL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHL100M ON P989.RTHL10                               
        ( NOMMAP ASC                                                            
        , ZECRAN ASC                                                            
        , CTABLE ASC                                                            
        , CCHAMPR ASC                                                           
        , CCHAMPL ASC                                                           
        , NREQUETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHL150 ON P989.RTHL15                                
        ( NOMMAP ASC                                                            
        , ZECRAN ASC                                                            
        , NOCCUR ASC                                                            
        , NSEQCRIT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHN000M ON P989.RTHN00                               
        ( CTIERS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHN010M ON P989.RTHN01                               
        ( CTIERS ASC                                                            
        , CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHN110M ON P989.RTHN11                               
        ( CTIERS ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV000M ON P989.RTHV00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CMODDEL ASC                                                           
        , DOPER ASC                                                             
        , WSEQFAM ASC                                                           
        , CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV020M ON P989.RTHV02                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV030 ON P989.RTHV03                                
        ( DVENTECIALE ASC                                                       
        , CPRESTATION ASC                                                       
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV060M ON P989.RTHV06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV070M ON P989.RTHV07                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV080M ON P989.RTHV08                               
        ( NSOCIETE ASC                                                          
        , CFAM ASC                                                              
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV090M ON P989.RTHV09                               
        ( NSOCIETE ASC                                                          
        , CFAM ASC                                                              
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV100M ON P989.RTHV10                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV110M ON P989.RTHV11                               
        ( NSOCIETE ASC                                                          
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV150M ON P989.RTHV15                               
        ( CPROGRAMME ASC                                                        
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTECIALE ASC                                                       
        , CTERMID ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV160M ON P989.RTHV16                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV170M ON P989.RTHV17                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DSVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV180M ON P989.RTHV18                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DSVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV200M ON P989.RTHV20                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CRAYON ASC                                                            
        , CINSEE ASC                                                            
        , DOPER ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV250M ON P989.RTHV25                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CRAYON ASC                                                            
        , CINSEE ASC                                                            
        , DGRPMOIS ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV260M ON P989.RTHV26                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV270M ON P989.RTHV27                               
        ( NSOCIETE ASC                                                          
        , WSEQPRO ASC                                                           
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV280M ON P989.RTHV28                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , WSEQPRO ASC                                                           
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV290M ON P989.RTHV29                               
        ( NSOCIETE ASC                                                          
        , WSEQPRO ASC                                                           
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV300M ON P989.RTHV30                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , NAGREGATED ASC                                                        
        , DSVENTECIALE ASC                                                      
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV320M ON P989.RTHV32                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV330M ON P989.RTHV33                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV340M ON P989.RTHV34                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DMVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV350M ON P989.RTHV35                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DMVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXHV700M ON P989.RTHV70                               
        ( NSOCIETE ASC                                                          
        , DMOIS ASC                                                             
        , NCODIC ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIA000M ON P989.RTIA00                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIA050M ON P989.RTIA05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIA051M ON P989.RTIA05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIA600M ON P989.RTIA60                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE000M ON P989.RTIE00                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NFICHE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE050M ON P989.RTIE05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE051M ON P989.RTIE05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE100M ON P989.RTIE10                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE200M ON P989.RTIE20                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE250M ON P989.RTIE25                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CAIRE ASC                                                             
        , CCOTE ASC                                                             
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE251M ON P989.RTIE25                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , CAIRE ASC                                                             
        , CCOTE ASC                                                             
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE300M ON P989.RTIE30                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
        , "ZONE" ASC                                                            
        , SECTEUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE350M ON P989.RTIE35                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , CALLEE ASC                                                            
        , CNIVEAU ASC                                                           
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE360M ON P989.RTIE36                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CALLEE ASC                                                            
        , CNIVEAU ASC                                                           
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE450M ON P989.RTIE45                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CZONE ASC                                                             
        , CSECTEUR ASC                                                          
        , NSOUSSECT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE600M ON P989.RTIE60                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE700M ON P989.RTIE70                               
        ( NHS ASC                                                               
        , NLIEUHS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE800M ON P989.RTIE80                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIE900M ON P989.RTIE90                               
        ( NDEPOT ASC                                                            
        , NSOCDEPOT ASC                                                         
        , CREGROUP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIJ000M ON P989.RTIJ00                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIN000M ON P989.RTIN00                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , DINVENTAIRE ASC                                                       
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , WSTOCKMASQ ASC                                                        
        , CSTATUT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIN100M ON P989.RTIN01                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , SSLIEU ASC                                                            
        , CSTATUT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIN900M ON P989.RTIN90                               
        ( DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIT000M ON P989.RTIT00                               
        ( NINVENTAIRE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIT050M ON P989.RTIT05                               
        ( NINVENTAIRE ASC                                                       
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIT100M ON P989.RTIT10                               
        ( NINVENTAIRE ASC                                                       
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIT150M ON P989.RTIT15                               
        ( NINVENTAIRE ASC                                                       
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NSSLIEU ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXIT900M ON P989.RTIT90                               
        ( NINVENTAIRE ASC                                                       
        , NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXLG000 ON P989.RTLG00                                
        ( CINSEE ASC                                                            
        , CTVOIE ASC                                                            
        , LNOMVOIE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXLG010M ON P989.RTLG01                               
        ( CINSEE ASC                                                            
        , NVOIE ASC                                                             
        , CPARITE ASC                                                           
        , NDEBTR ASC                                                            
        , NFINTR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXLM100M ON P989.RTLM10                               
        ( NLISTE ASC                                                            
        , NAPPORT ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXLT000M ON P989.RTLT00                               
        ( NLETTRE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXLT010M ON P989.RTLT01                               
        ( NLETTRE ASC                                                           
        , NPAGE ASC                                                             
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXLT020M ON P989.RTLT02                               
        ( NLETTRE ASC                                                           
        , NPAGE ASC                                                             
        , NLIGNE ASC                                                            
        , INDVAR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMG010M ON P989.RTMG01                               
        ( CFAMMGI ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMO010M ON P989.RTMO01                               
        ( TYPOFFRE ASC                                                          
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMQ150M ON P989.RTMQ15                               
        ( CPROGRAMME ASC                                                        
        , CFONCTION ASC                                                         
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , DOPERATION ASC                                                        
        , "COMMENT" ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMQ160M ON P989.RTMQ16                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMU050M ON P989.RTMU05                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSSLIEU ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMU210M ON P989.RTMU21                               
        ( DSEMAINE ASC                                                          
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMU250M ON P989.RTMU25                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMU260M ON P989.RTMU26                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMU300M ON P989.RTMU30                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NROTATION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMU310M ON P989.RTMU31                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NROTATION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMU350M ON P989.RTMU35                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXMU500M ON P989.RTMU50                               
        ( CTYPCDE ASC                                                           
        , NSEQED ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPA700M ON P989.RTPA70                               
        ( NSOCIETE ASC                                                          
        , CPTSAV ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPC080M ON P989.RTPC08                               
        ( RANDOMNUM ASC                                                         
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPC090M ON P989.RTPC09                               
        ( RANDOMNUM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPM000M ON P989.RTPM00                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPM060M ON P989.RTPM06                               
        ( CMOPAI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPP000M ON P989.RTPP00                               
        ( NLIEU ASC                                                             
        , DMOISPAYE ASC                                                         
        , CPRIME ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPP001M ON P989.RTPP01                               
        ( NLIEU ASC                                                             
        , DMOISPAYE ASC                                                         
        , CPRIME ASC                                                            
        , NRUB ASC                                                              
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPP010M ON P989.RTPP10                               
        ( DMOISPAYE ASC                                                         
        , CPRIME ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR000 ON P989.RTPR00                                
        ( CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR010 ON P989.RTPR01                                
        ( CFAM ASC                                                              
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR020 ON P989.RTPR02                                
        ( NCODIC ASC                                                            
        , CPRESTATION ASC                                                       
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR030 ON P989.RTPR03                                
        ( CPRESTATION ASC                                                       
        , CGESTION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR031M ON P989.RTPR03                               
        ( CGESTION ASC                                                          
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR040 ON P989.RTPR04                                
        ( CPRESTATION ASC                                                       
        , CPRESTELT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR050 ON P989.RTPR05                                
        ( CPRESTATION ASC                                                       
        , CGRPETAT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR060 ON P989.RTPR06                                
        ( CPRESTATION ASC                                                       
        , CSTATUT ASC                                                           
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR100 ON P989.RTPR10                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR110 ON P989.RTPR11                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR120 ON P989.RTPR12                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR130 ON P989.RTPR13                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR140 ON P989.RTPR14                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR150 ON P989.RTPR15                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR160 ON P989.RTPR16                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR170 ON P989.RTPR17                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPR200 ON P989.RTPR20                                
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , CAGREPRE ASC                                                          
        , WSEQED ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPS000M ON P989.RTPS00                               
        ( CGCPLT ASC                                                            
        , NGCPLT ASC                                                            
        , NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPS010M ON P989.RTPS01                               
        ( CGCPLT ASC                                                            
        , NOSAV ASC                                                             
        , CSECTEUR ASC                                                          
        , DEFFET ASC                                                            
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPS020M ON P989.RTPS02                               
        ( CGCPLT ASC                                                            
        , NOSAV ASC                                                             
        , CSECTEUR ASC                                                          
        , DEFFET ASC                                                            
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPS030M ON P989.RTPS03                               
        ( CGCPLT ASC                                                            
        , NGCPLT ASC                                                            
        , NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPT030M ON P989.RTPT03                               
        ( CODLANG ASC                                                           
        , CNOMPGRM ASC                                                          
        , NSEQLIB ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPV000M ON P989.RTPV00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXQC010M ON P989.RTQC01                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXQC020M ON P989.RTQC02                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXQC030 ON P989.RTQC03                                
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXQC050M ON P989.RTQC05                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXQC060M ON P989.RTQC06                               
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXQC110M ON P989.RTQC11                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTQUEST ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXQC120M ON P989.RTQC12                               
        ( NCARTE ASC                                                            
        , CTQUEST ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXQC130M ON P989.RTQC13                               
        ( NCARTE ASC                                                            
        , CTQUEST ASC                                                           
        , CQUESTION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRA000M ON P989.RTRA00                               
        ( NDEMANDE ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRA010M ON P989.RTRA01                               
        ( NDEMANDE ASC                                                          
        , NSEQCODIC ASC                                                         
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRA020M ON P989.RTRA02                               
        ( CTYPREGL ASC                                                          
        , NREGL ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRC000M ON P989.RTRC00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRC100M ON P989.RTRC10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRE000M ON P989.RTRE00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRE100M ON P989.RTRE10                               
        ( NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
        , CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
        , WTLMELA ASC                                                           
        , NAGREGATED ASC                                                        
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRX000M ON P989.RTRX00                               
        ( NCONC ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRX050M ON P989.RTRX05                               
        ( NCONC ASC                                                             
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRX100M ON P989.RTRX10                               
        ( NCONC ASC                                                             
        , NMAG ASC                                                              
        , CRAYON ASC                                                            
        , NFREQUENCE ASC                                                        
        , NSEQUENCE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRX150M ON P989.RTRX15                               
        ( NCONC ASC                                                             
        , NMAG ASC                                                              
        , CRAYON ASC                                                            
        , DSUPPORT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRX200M ON P989.RTRX20                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRX210M ON P989.RTRX21                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRX220M ON P989.RTRX22                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRX250M ON P989.RTRX25                               
        ( NRELEVE ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRX450M ON P989.RTRX45                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CPROFIL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRX500M ON P989.RTRX50                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CPROFIL ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXRX550M ON P989.RTRX55                               
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DRELEVE DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSB110M ON P989.RTSB11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSG020M ON P989.RTSG02                               
        ( SECPAIE ASC                                                           
        , CSECTION ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSG030M ON P989.RTSG03                               
        ( COMPTE ASC                                                            
        , NETABADM ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSL400M ON P989.RTSL40                               
        ( NSOCIETE ASC                                                          
        , CLIEUINV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSL410M ON P989.RTSL41                               
        ( CLIEUINV ASC                                                          
        , NREGL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSL500M ON P989.RTSL50                               
        ( DINV ASC                                                              
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CLIEUINV ASC                                                          
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSL510M ON P989.RTSL51                               
        ( DINV ASC                                                              
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CEMPL ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSM010M ON P989.RTSM01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSM020M ON P989.RTSM02                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSP000M ON P989.RTSP00                               
        ( NENTCDE ASC                                                           
        , CFAM ASC                                                              
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSP050M ON P989.RTSP05                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSP100M ON P989.RTSP10                               
        ( NCODIC ASC                                                            
        , DJOUR DESC                                                            
        , NREC ASC                                                              
        , CMAJ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSP150M ON P989.RTSP15                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSP250M ON P989.RTSP25                               
        ( CMARQ ASC                                                             
        , NSOCDEST ASC                                                          
        , DPROV ASC                                                             
        , WTEBRB ASC                                                            
        , CTAUXTVA ASC                                                          
        , NLIEUDEST ASC                                                         
        , NLIEUORIG ASC                                                         
        , NSOCORIG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXSV010M ON P989.RTSV01                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CTPSAV ASC                                                            
        , DEFFET ASC                                                            
        , NSOCSAV ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXTF300M ON P989.RTTF30                               
        ( LFICHIER ASC                                                          
        , CRITERE ASC                                                           
        , DCREATION ASC                                                         
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXTF350M ON P989.RTTF35                               
        ( LFICHIER ASC                                                          
        , CRITERE ASC                                                           
        , DCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXTL020M ON P989.RTTL02                               
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , CTOURNEE ASC                                                          
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXTL060M ON P989.RTTL06                               
        ( NSOC ASC                                                              
        , CPROTOUR ASC                                                          
        , DDELIV ASC                                                            
        , CTOURNEE ASC                                                          
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXTL080M ON P989.RTTL08                               
        ( DDELIV ASC                                                            
        , CPROTOUR ASC                                                          
        , NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
        , CTOURNEE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXTL090M ON P989.RTTL09                               
        ( CLIVR ASC                                                             
        , DMMLIVR ASC                                                           
        , CRETOUR ASC                                                           
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXTL110M ON P989.RTTL11                               
        ( DDELIV ASC                                                            
        , CPROTOUR ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVA050M ON P989.RTVA05                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVA051M ON P989.RTVA05                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVA100M ON P989.RTVA10                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVA150M ON P989.RTVA15                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVA200M ON P989.RTVA20                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVA250M ON P989.RTVA25                               
        ( CFAM ASC                                                              
        , CSEQRAYON ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVA300M ON P989.RTVA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVA500M ON P989.RTVA50                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , DOPER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVE020M ON P989.RTVE02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVE050M ON P989.RTVE05                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVE060M ON P989.RTVE06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVE080M ON P989.RTVE08                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQ ASC                                                              
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVE100M ON P989.RTVE10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE DESC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVE101M ON P989.RTVE10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVE110M ON P989.RTVE11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXVE140M ON P989.RTVE14                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.SXVA050M ON P989.STVA05                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.SXVA100M ON P989.STVA10                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.SXVA150M ON P989.STVA15                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.SXVA200M ON P989.STVA20                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.SXVA250M ON P989.STVA25                               
        ( CFAM ASC                                                              
        , CSEQRAYON ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P989.SXVA300M ON P989.STVA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.RXQTPSEPX ON P989.QTPSEPX                                    
        ( CFAM ASC                                                              
        , CGCPLT ASC                                                            
        , CTARIF ASC                                                            
        , WACTIF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.RXPS040D ON P989.RTPS04                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , "TYPE" ASC                                                            
        , DINIT ASC                                                             
        , MONTTOT ASC                                                           
        , DUREE ASC                                                             
        , INFO1 ASC                                                             
        , INFO2 ASC                                                             
        , INFO3 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P989.CCDIX_TTGV02 ON P989.TTGV02                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.CCDIX_TTGV11 ON P989.TTGV11                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.CCDIX_TTVE10 ON P989.TTVE10                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.TXGV020 ON P989.TTGV02                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
        , IBMSNAP_LOGMARKER ASC                                                 
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.TXGV021 ON P989.TTGV02                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.TXGV022 ON P989.TTGV02                                       
        ( IBMSNAP_LOGMARKER ASC                                                 
        , NVENTE ASC                                                            
        , DSYSTUPD ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.TXGV110 ON P989.TTGV11                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQNQ ASC                                                            
        , CTYPENREG ASC                                                         
        , IBMSNAP_LOGMARKER ASC                                                 
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.TXGV111 ON P989.TTGV11                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.TXGV112 ON P989.TTGV11                                       
        ( NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.TXGV113 ON P989.TTGV11                                       
        ( IBMSNAP_LOGMARKER ASC                                                 
        , NVENTE ASC                                                            
        , DSYSTUPD ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.TXVE101 ON P989.TTVE10                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P989.TXVE102 ON P989.TTVE10                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.CXGV020M ON P989.CTGV02                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.CXGV110M ON P989.CTGV11                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P989.CXVE100M ON P989.CTVE10                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
