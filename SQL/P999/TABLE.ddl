CREATE  TABLE P999.RTEG00                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , RUPTCHTPAGE SMALLINT NOT NULL                                         
        , RUPTRAZPAGE SMALLINT NOT NULL                                         
        , LARGEUR SMALLINT NOT NULL                                             
        , LONGUEUR SMALLINT NOT NULL                                            
        , DSECTCREE CHAR ( 1 ) NOT NULL                                         
        , TRANSFERE CHAR ( 1 ) NOT NULL                                         
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , LIBETAT VARCHAR ( 64 ) NOT NULL                                       
        )                                                                       
      IN P999_RTEG00_TAB INDEX IN P999_RTEG00_IDX
   ;                                                                            
CREATE  TABLE P999.RTEG05                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , SKIPBEFORE SMALLINT NOT NULL                                          
        , SKIPAFTER SMALLINT NOT NULL                                           
        , RUPTIMPR SMALLINT NOT NULL                                            
        , NOMCHAMPC CHAR ( 18 ) NOT NULL                                        
        , NOMCHAMPS CHAR ( 8 ) NOT NULL                                         
        , SOUSTABLE CHAR ( 5 ) NOT NULL                                         
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTEG05_TAB INDEX IN P999_RTEG05_IDX
   ;                                                                            
CREATE  TABLE P999.RTEG10                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , POSCHAMP SMALLINT NOT NULL                                            
        , NOMCHAMP CHAR ( 18 ) NOT NULL                                         
        , RUPTIMPR SMALLINT NOT NULL                                            
        , CHCOND1 CHAR ( 18 ) NOT NULL                                          
        , "CONDITION" CHAR ( 2 ) NOT NULL                                       
        , CHCOND2 CHAR ( 18 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , MASKCHAMP VARCHAR ( 30 ) NOT NULL                                     
        )                                                                       
      IN P999_RTEG10_TAB INDEX IN P999_RTEG10_IDX
   ;                                                                            
CREATE  TABLE P999.RTEG11                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , POSCHAMP SMALLINT NOT NULL                                            
        , RUPTIMPR SMALLINT NOT NULL                                            
        , SOUSTABLE CHAR ( 5 ) NOT NULL                                         
        , NOMCHAMPS CHAR ( 8 ) NOT NULL                                         
        , NOMCHAMPC CHAR ( 18 ) NOT NULL                                        
        , CHCOND1 CHAR ( 18 ) NOT NULL                                          
        , "CONDITION" CHAR ( 2 ) NOT NULL                                       
        , CHCOND2 CHAR ( 18 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , LIBCHAMP VARCHAR ( 66 ) NOT NULL                                      
        )                                                                       
      IN P999_RTEG11_TAB INDEX IN P999_RTEG11_IDX
   ;                                                                            
CREATE  TABLE P999.RTEG15                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , POSCHAMP SMALLINT NOT NULL                                            
        , NOMCHAMPT CHAR ( 18 ) NOT NULL                                        
        , RUPTRAZ SMALLINT NOT NULL                                             
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTEG15_TAB INDEX IN P999_RTEG15_IDX
   ;                                                                            
CREATE  TABLE P999.RTEG20                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , POSCHAMP SMALLINT NOT NULL                                            
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , LIBCHAMP VARCHAR ( 66 ) NOT NULL                                      
        )                                                                       
      IN P999_RTEG20_TAB INDEX IN P999_RTEG20_IDX
   ;                                                                            
CREATE  TABLE P999.RTEG25                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , NOMCHAMP CHAR ( 18 ) NOT NULL                                         
        , LIGNECALC SMALLINT NOT NULL                                           
        , CHCALC1 CHAR ( 18 ) NOT NULL                                          
        , TYPCALCUL CHAR ( 2 ) NOT NULL                                         
        , CHCALC2 CHAR ( 18 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTEG25_TAB INDEX IN P999_RTEG25_IDX
   ;                                                                            
CREATE  TABLE P999.RTEG30                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , NOMCHAMP CHAR ( 18 ) NOT NULL                                         
        , TYPCHAMP CHAR ( 1 ) NOT NULL                                          
        , LGCHAMP SMALLINT NOT NULL                                             
        , DECCHAMP SMALLINT NOT NULL                                            
        , POSDSECT SMALLINT NOT NULL                                            
        , OCCURENCES SMALLINT NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTEG30_TAB INDEX IN P999_RTEG30_IDX
   ;                                                                            
CREATE  TABLE P999.RTEG40                                                       
        ( NOMETAT CHAR ( 8 ) NOT NULL                                           
        , TYPLIGNE CHAR ( 1 ) NOT NULL                                          
        , NOLIGNE SMALLINT NOT NULL                                             
        , CONTINUER CHAR ( 1 ) NOT NULL                                         
        , NOMCHAMP CHAR ( 18 ) NOT NULL                                         
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTEG40_TAB INDEX IN P999_RTEG40_IDX
   ;                                                                            
CREATE  TABLE P999.RTEG90                                                       
        ( IDENTTS CHAR ( 8 ) NOT NULL                                           
        , NOMETAT CHAR ( 8 ) NOT NULL                                           
        , CHAMPTRI CHAR ( 254 ) NOT NULL                                        
        , RANGTS SMALLINT NOT NULL                                              
        )                                                                       
      IN P999_RTEG90_TAB INDEX IN P999_RTEG90_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM01                                                       
        ( NENTITE CHAR ( 5 ) NOT NULL                                           
        , NAUX CHAR ( 3 ) NOT NULL                                              
        , LAUX CHAR ( 30 ) NOT NULL                                             
        , WTYPAUX CHAR ( 1 ) NOT NULL                                           
        , WSAISIE CHAR ( 1 ) NOT NULL                                           
        , WAUXNCG CHAR ( 1 ) NOT NULL                                           
        , WLETTRE CHAR ( 1 ) NOT NULL                                           
        , WRELANCE CHAR ( 1 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , WAUXARF CHAR ( 1 ) NOT NULL                                           
          WITH DEFAULT                                                          
        'N'                                                                     
        )                                                                       
      IN P999_RTFM01_TAB INDEX IN P999_RTFM01_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM02                                                       
        ( NENTITE CHAR ( 5 ) NOT NULL                                           
        , CDEVISE CHAR ( 3 ) NOT NULL                                           
        , WSENS CHAR ( 1 ) NOT NULL                                             
        , COMPTE CHAR ( 6 ) NOT NULL                                            
        , SECTION CHAR ( 6 ) NOT NULL                                           
        , RUBRIQUE CHAR ( 6 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM02_TAB INDEX IN P999_RTFM02_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM03                                                       
        ( NENTITE CHAR ( 5 ) NOT NULL                                           
        , CMETHODE CHAR ( 3 ) NOT NULL                                          
        , LMETHODE CHAR ( 30 ) NOT NULL                                         
        , CDOC CHAR ( 6 ) NOT NULL                                              
        , WRIB CHAR ( 1 ) NOT NULL                                              
        , WNUMAUTO CHAR ( 1 ) NOT NULL                                          
        , WEAP CHAR ( 1 ) NOT NULL                                              
        , DELAI DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WACTIF CHAR ( 1 ) NOT NULL                                            
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM03_TAB INDEX IN P999_RTFM03_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM04                                                       
        ( NENTITE CHAR ( 5 ) NOT NULL                                           
        , CDEVISE CHAR ( 3 ) NOT NULL                                           
        , LDEVISE CHAR ( 25 ) NOT NULL                                          
        , NBDECIM DECIMAL ( 1 , 0 ) NOT NULL                                    
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , LDEV CHAR ( 5 ) NOT NULL                                              
          WITH DEFAULT                                                          
        , LSUB CHAR ( 5 ) NOT NULL                                              
          WITH DEFAULT                                                          
        , LSUBDIVIS CHAR ( 25 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , CCAISSE CHAR ( 1 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , WEURO CHAR ( 1 ) NOT NULL                                             
          WITH DEFAULT                                                          
        , CDEVISENUM DECIMAL ( 3 , 0 ) NOT NULL                                 
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTFM04_TAB INDEX IN P999_RTFM04_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM05                                                       
        ( NENTITE CHAR ( 5 ) NOT NULL                                           
        , CDEVORIG CHAR ( 3 ) NOT NULL                                          
        , CDEVDEST CHAR ( 3 ) NOT NULL                                          
        , DEFFET CHAR ( 8 ) NOT NULL                                            
        , COPERAT CHAR ( 1 ) NOT NULL                                           
        , PTAUX DECIMAL ( 9 , 5 ) NOT NULL                                      
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM05_TAB INDEX IN P999_RTFM05_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM06                                                       
        ( NENTITE CHAR ( 5 ) NOT NULL                                           
        , CETAT CHAR ( 10 ) NOT NULL                                            
        , NDEMANDE CHAR ( 2 ) NOT NULL                                          
        , WMFICHE CHAR ( 1 ) NOT NULL                                           
        , WPAPIER CHAR ( 1 ) NOT NULL                                           
        , NETAB CHAR ( 3 ) NOT NULL                                             
        , WTRIETAB CHAR ( 1 ) NOT NULL                                          
        , WNTRIETAB CHAR ( 1 ) NOT NULL                                         
        , NAUXMIN CHAR ( 3 ) NOT NULL                                           
        , NAUXMAX CHAR ( 3 ) NOT NULL                                           
        , WTYPAUX CHAR ( 1 ) NOT NULL                                           
        , WTRIAUX CHAR ( 1 ) NOT NULL                                           
        , WNTRIAUX CHAR ( 1 ) NOT NULL                                          
        , NTIERSMIN CHAR ( 8 ) NOT NULL                                         
        , NTIERSMAX CHAR ( 8 ) NOT NULL                                         
        , WTRITIERS CHAR ( 1 ) NOT NULL                                         
        , WNTRITIERS CHAR ( 1 ) NOT NULL                                        
        , CMETHODE CHAR ( 3 ) NOT NULL                                          
        , WTRIMETHODE CHAR ( 1 ) NOT NULL                                       
        , WNTRIMETHODE CHAR ( 1 ) NOT NULL                                      
        , NCOMPTEMIN CHAR ( 6 ) NOT NULL                                        
        , NCOMPTEMAX CHAR ( 6 ) NOT NULL                                        
        , WTRICOMPTE CHAR ( 1 ) NOT NULL                                        
        , WNTRICOMPTE CHAR ( 1 ) NOT NULL                                       
        , NCPTEAUXMIN CHAR ( 6 ) NOT NULL                                       
        , NCPTEAUXMAX CHAR ( 6 ) NOT NULL                                       
        , WTRICPTEAUX CHAR ( 1 ) NOT NULL                                       
        , WNTRICPTEAUX CHAR ( 1 ) NOT NULL                                      
        , NSECTIONMIN CHAR ( 6 ) NOT NULL                                       
        , NSECTIONMAX CHAR ( 6 ) NOT NULL                                       
        , WTRISECTION CHAR ( 1 ) NOT NULL                                       
        , WNTRISECTION CHAR ( 1 ) NOT NULL                                      
        , NRUBRIQUEMIN CHAR ( 6 ) NOT NULL                                      
        , NRUBRIQUEMAX CHAR ( 6 ) NOT NULL                                      
        , WTRIRUBRIQUE CHAR ( 1 ) NOT NULL                                      
        , WNTRIRUBRIQUE CHAR ( 1 ) NOT NULL                                     
        , NBANQUEMIN CHAR ( 5 ) NOT NULL                                        
        , NBANQUEMAX CHAR ( 5 ) NOT NULL                                        
        , WTRIBANQUE CHAR ( 1 ) NOT NULL                                        
        , WNTRIBANQUE CHAR ( 1 ) NOT NULL                                       
        , DECHEANCEMIN CHAR ( 8 ) NOT NULL                                      
        , DECHEANCEMAX CHAR ( 8 ) NOT NULL                                      
        , WTRIECHEANCE CHAR ( 1 ) NOT NULL                                      
        , WNTRIECHEANCE CHAR ( 1 ) NOT NULL                                     
        , CDEVISE CHAR ( 3 ) NOT NULL                                           
        , WPTOTDEVISE CHAR ( 1 ) NOT NULL                                       
        , WTTRIECRIT CHAR ( 1 ) NOT NULL                                        
        , WTTRITIERS CHAR ( 1 ) NOT NULL                                        
        , WEDTTIERS CHAR ( 1 ) NOT NULL                                         
        , WTSELTIERS CHAR ( 1 ) NOT NULL                                        
        , WCOMPTESOLDE CHAR ( 1 ) NOT NULL                                      
        , WPIECELETTRE CHAR ( 1 ) NOT NULL                                      
        , CNAT1 CHAR ( 3 ) NOT NULL                                             
        , CNAT2 CHAR ( 3 ) NOT NULL                                             
        , CNAT3 CHAR ( 3 ) NOT NULL                                             
        , CNAT4 CHAR ( 3 ) NOT NULL                                             
        , CNAT5 CHAR ( 3 ) NOT NULL                                             
        , CNAT6 CHAR ( 3 ) NOT NULL                                             
        , CNAT7 CHAR ( 3 ) NOT NULL                                             
        , CNAT8 CHAR ( 3 ) NOT NULL                                             
        , CNAT9 CHAR ( 3 ) NOT NULL                                             
        , CNAT10 CHAR ( 3 ) NOT NULL                                            
        , CNATE1 CHAR ( 3 ) NOT NULL                                            
        , CNATE2 CHAR ( 3 ) NOT NULL                                            
        , CNATE3 CHAR ( 3 ) NOT NULL                                            
        , CNATE4 CHAR ( 3 ) NOT NULL                                            
        , CNATE5 CHAR ( 3 ) NOT NULL                                            
        , CNATE6 CHAR ( 3 ) NOT NULL                                            
        , CNATE7 CHAR ( 3 ) NOT NULL                                            
        , CNATE8 CHAR ( 3 ) NOT NULL                                            
        , CNATE9 CHAR ( 3 ) NOT NULL                                            
        , CNATE10 CHAR ( 3 ) NOT NULL                                           
        , NJOURNALMIN CHAR ( 3 ) NOT NULL                                       
        , NJOURNALMAX CHAR ( 3 ) NOT NULL                                       
        , WJOURNALGAL CHAR ( 1 ) NOT NULL                                       
        , DCREATION CHAR ( 8 ) NOT NULL                                         
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , CACID CHAR ( 8 ) NOT NULL                                             
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM06_TAB INDEX IN P999_RTFM06_IDX
   ;                                                                            
--MW oopCREATE  TABLE P999.RTFM10                                                       
--MW oop        ( NENTITE CHAR ( 5 ) NOT NULL                                           
--MW oop        , COMPTE CHAR ( 6 ) NOT NULL                                            
--MW oop        , NAUX CHAR ( 3 ) NOT NULL                                              
--MW oop        , WCOLLECTIF CHAR ( 1 ) NOT NULL                                        
--MW oop        , WAUXILIARISE CHAR ( 1 ) NOT NULL                                      
--MW oop        , WSLIDSLAC CHAR ( 1 ) NOT NULL                                         
--MW oop        , WLETTRAGE CHAR ( 1 ) NOT NULL                                         
--MW oop        , WPOINTAGE CHAR ( 1 ) NOT NULL                                         
--MW oop        , WTYPCOMPTE CHAR ( 1 ) NOT NULL                                        
--MW oop        , WEDITION CHAR ( 1 ) NOT NULL                                          
--MW oop        , WGL CHAR ( 1 ) NOT NULL                                               
--MW oop        , WCLOTURE CHAR ( 1 ) NOT NULL                                          
--MW oop        , WREGAUTO CHAR ( 1 ) NOT NULL                                          
--MW oop        , CEPURATION CHAR ( 1 ) NOT NULL                                        
--MW oop        )                                                                       
--MW oop      IN P999_RTFM10_TAB INDEX IN P999_RTFM10_IDX
--MW oop   ;                                                                            
CREATE  TABLE P999.RTFM11                                                       
        ( CPCG CHAR ( 4 ) NOT NULL                                              
        , LPCG CHAR ( 20 ) NOT NULL                                             
        )                                                                       
      IN P999_RTFM11_TAB INDEX IN P999_RTFM11_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM13                                                       
        ( CPCG CHAR ( 4 ) NOT NULL                                              
        , COMPTE CHAR ( 6 ) NOT NULL                                            
        , NSEQ CHAR ( 2 ) NOT NULL                                              
        , CCATEGORIE CHAR ( 6 ) NOT NULL                                        
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM13_TAB INDEX IN P999_RTFM13_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM14                                                       
        ( CPDS CHAR ( 4 ) NOT NULL                                              
        , LPDS CHAR ( 20 ) NOT NULL                                             
        )                                                                       
      IN P999_RTFM14_TAB INDEX IN P999_RTFM14_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM16                                                       
        ( CPDS CHAR ( 4 ) NOT NULL                                              
        , SECTION CHAR ( 6 ) NOT NULL                                           
        , NSEQ CHAR ( 2 ) NOT NULL                                              
        , CCATEGORIE CHAR ( 6 ) NOT NULL                                        
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM16_TAB INDEX IN P999_RTFM16_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM17                                                       
        ( CPDR CHAR ( 4 ) NOT NULL                                              
        , LPDR CHAR ( 20 ) NOT NULL                                             
        )                                                                       
      IN P999_RTFM17_TAB INDEX IN P999_RTFM17_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM19                                                       
        ( CPDR CHAR ( 4 ) NOT NULL                                              
        , RUBRIQUE CHAR ( 6 ) NOT NULL                                          
        , NSEQ CHAR ( 2 ) NOT NULL                                              
        , CCATEGORIE CHAR ( 6 ) NOT NULL                                        
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM19_TAB INDEX IN P999_RTFM19_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM20                                                       
        ( NENTITE CHAR ( 5 ) NOT NULL                                           
        , LENTITE CHAR ( 20 ) NOT NULL                                          
        , CPCG CHAR ( 4 ) NOT NULL                                              
        , CPDS CHAR ( 4 ) NOT NULL                                              
        , CPDR CHAR ( 4 ) NOT NULL                                              
        , CDEVISE CHAR ( 3 ) NOT NULL                                           
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , ANCCPDS CHAR ( 4 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , ANCCPDR CHAR ( 4 ) NOT NULL                                           
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTFM20_TAB INDEX IN P999_RTFM20_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM23                                                       
        ( NENTITE CHAR ( 4 ) NOT NULL                                           
        , NEXER CHAR ( 4 ) NOT NULL                                             
        , NANCIV CHAR ( 4 ) NOT NULL                                            
        , NMOISDEB CHAR ( 2 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM23_TAB INDEX IN P999_RTFM23_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM25                                                       
        ( NJRN CHAR ( 3 ) NOT NULL                                              
        , LJRN CHAR ( 25 ) NOT NULL                                             
        , WCTRPASS CHAR ( 1 ) NOT NULL                                          
        , WCTRPART CHAR ( 1 ) NOT NULL                                          
        , WNUMAUTO CHAR ( 1 ) NOT NULL                                          
        , WJRNREG CHAR ( 1 ) NOT NULL                                           
        , WEDTCRS CHAR ( 1 ) NOT NULL                                           
        , COMPTE CHAR ( 6 ) NOT NULL                                            
        , SECTION CHAR ( 6 ) NOT NULL                                           
        , RUBRIQUE CHAR ( 6 ) NOT NULL                                          
        , STEAPP CHAR ( 5 ) NOT NULL                                            
        , CDEVISE CHAR ( 3 ) NOT NULL                                           
        , NAUX CHAR ( 3 ) NOT NULL                                              
        , WBATCHTP CHAR ( 1 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM25_TAB INDEX IN P999_RTFM25_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM30                                                       
        ( NATURE CHAR ( 3 ) NOT NULL                                            
        , LNATURE CHAR ( 20 ) NOT NULL                                          
        , WSENS CHAR ( 1 ) NOT NULL                                             
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM30_TAB INDEX IN P999_RTFM30_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM54                                                       
        ( CODE CHAR ( 1 ) NOT NULL                                              
        , LIBELLE CHAR ( 30 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM54_TAB INDEX IN P999_RTFM54_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM55                                                       
        ( CODE CHAR ( 3 ) NOT NULL                                              
        , LIBELLE CHAR ( 30 ) NOT NULL                                          
        , CODEGEO CHAR ( 1 ) NOT NULL                                           
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , CODEBDF CHAR ( 2 ) NOT NULL                                           
          WITH DEFAULT                                                          
        ''                                                                      
        )                                                                       
      IN P999_RTFM55_TAB INDEX IN P999_RTFM55_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM56                                                       
        ( CODE CHAR ( 2 ) NOT NULL                                              
        , LIBELLE CHAR ( 30 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM56_TAB INDEX IN P999_RTFM56_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM57                                                       
        ( CODE CHAR ( 2 ) NOT NULL                                              
        , LIBELLE CHAR ( 30 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM57_TAB INDEX IN P999_RTFM57_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM58                                                       
        ( CODE CHAR ( 1 ) NOT NULL                                              
        , LIBELLE CHAR ( 30 ) NOT NULL                                          
        , NBMOIS DECIMAL ( 2 , 0 ) NOT NULL                                     
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM58_TAB INDEX IN P999_RTFM58_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM59                                                       
        ( CODE CHAR ( 6 ) NOT NULL                                              
        , LIBELLE CHAR ( 30 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM59_TAB INDEX IN P999_RTFM59_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM60                                                       
        ( CPCG CHAR ( 4 ) NOT NULL                                              
        , COMPTE CHAR ( 6 ) NOT NULL                                            
        , LCOMPTEC CHAR ( 15 ) NOT NULL                                         
        , LCOMPTEL CHAR ( 30 ) NOT NULL                                         
        , CMASQUE CHAR ( 6 ) NOT NULL                                           
        , CCONSOD CHAR ( 6 ) NOT NULL                                           
        , CCONSOC CHAR ( 6 ) NOT NULL                                           
        , NAUX CHAR ( 3 ) NOT NULL                                              
        , WTYPCOMPTE CHAR ( 1 ) NOT NULL                                        
        , WAUXILIARISE CHAR ( 1 ) NOT NULL                                      
        , WCOLLECTIF CHAR ( 1 ) NOT NULL                                        
        , WABONNE CHAR ( 1 ) NOT NULL                                           
        , WREGAUTO CHAR ( 1 ) NOT NULL                                          
        , WCATNATCH CHAR ( 1 ) NOT NULL                                         
        , WLETTRAGE CHAR ( 1 ) NOT NULL                                         
        , WPOINTAGE CHAR ( 1 ) NOT NULL                                         
        , WEDITION CHAR ( 1 ) NOT NULL                                          
        , WCLOTURE CHAR ( 1 ) NOT NULL                                          
        , WINTERFACE CHAR ( 1 ) NOT NULL                                        
        , CEPURATION CHAR ( 1 ) NOT NULL                                        
        , DCLOTURE CHAR ( 8 ) NOT NULL                                          
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , WREGAUTOC CHAR ( 1 ) NOT NULL                                         
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTFM60_TAB INDEX IN P999_RTFM60_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM61                                                       
        ( CPDS CHAR ( 4 ) NOT NULL                                              
        , SECTION CHAR ( 6 ) NOT NULL                                           
        , LSECTIONC CHAR ( 15 ) NOT NULL                                        
        , LSECTIONL CHAR ( 30 ) NOT NULL                                        
        , CMASQUE CHAR ( 6 ) NOT NULL                                           
        , WINTERFACE CHAR ( 1 ) NOT NULL                                        
        , DCLOTURE CHAR ( 8 ) NOT NULL                                          
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , WFIR CHAR ( 1 ) NOT NULL                                              
          WITH DEFAULT                                                          
        ''                                                                      
        , NSOC CHAR ( 5 ) NOT NULL                                              
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTFM61_TAB INDEX IN P999_RTFM61_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM62                                                       
        ( CPDR CHAR ( 4 ) NOT NULL                                              
        , RUBRIQUE CHAR ( 6 ) NOT NULL                                          
        , LRUBRIQUEC CHAR ( 15 ) NOT NULL                                       
        , LRUBRIQUEL CHAR ( 30 ) NOT NULL                                       
        , CMASQUE CHAR ( 6 ) NOT NULL                                           
        , WINTERFACE CHAR ( 1 ) NOT NULL                                        
        , DCLOTURE CHAR ( 8 ) NOT NULL                                          
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM62_TAB INDEX IN P999_RTFM62_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM64                                                       
        ( STEAPP CHAR ( 5 ) NOT NULL                                            
        , LSTEAPPC CHAR ( 15 ) NOT NULL                                         
        , LSTEAPPL CHAR ( 30 ) NOT NULL                                         
        , CMASQUE CHAR ( 5 ) NOT NULL                                           
        , DCLOTURE CHAR ( 8 ) NOT NULL                                          
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , NSIRET CHAR ( 14 ) NOT NULL                                           
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTFM64_TAB INDEX IN P999_RTFM64_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM65                                                       
        ( NENTITE CHAR ( 5 ) NOT NULL                                           
        , CMASQUEC CHAR ( 6 ) NOT NULL                                          
        , CMASQUES CHAR ( 6 ) NOT NULL                                          
        , CMASQUER CHAR ( 6 ) NOT NULL                                          
        , CMASQUEE CHAR ( 3 ) NOT NULL                                          
        , CMASQUEA CHAR ( 5 ) NOT NULL                                          
        , DCLOTURE CHAR ( 8 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM65_TAB INDEX IN P999_RTFM65_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM69                                                       
        ( COMPTE CHAR ( 6 ) NOT NULL                                            
        , LCOMPTEC CHAR ( 15 ) NOT NULL                                         
        , LCOMPTEL CHAR ( 30 ) NOT NULL                                         
        , DCLOTURE CHAR ( 8 ) NOT NULL                                          
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM69_TAB INDEX IN P999_RTFM69_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM73                                                       
        ( CNOMPROG CHAR ( 8 ) NOT NULL                                          
        , LNOMPROG CHAR ( 30 ) NOT NULL                                         
        , CTYPE CHAR ( 1 ) NOT NULL                                             
        , WTRT1 CHAR ( 1 ) NOT NULL                                             
        , WTRT2 CHAR ( 1 ) NOT NULL                                             
        , WSEMAINE CHAR ( 7 ) NOT NULL                                          
        , NJRN CHAR ( 3 ) NOT NULL                                              
        , NATURE CHAR ( 3 ) NOT NULL                                            
        , CETAT CHAR ( 8 ) NOT NULL                                             
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM73_TAB INDEX IN P999_RTFM73_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM80                                                       
        ( CNATOPER CHAR ( 5 ) NOT NULL                                          
        , LNATOPER CHAR ( 25 ) NOT NULL                                         
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM80_TAB INDEX IN P999_RTFM80_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM81                                                       
        ( CTYPOPER CHAR ( 5 ) NOT NULL                                          
        , LTYPOPER CHAR ( 25 ) NOT NULL                                         
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM81_TAB INDEX IN P999_RTFM81_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM82                                                       
        ( CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , LINTERFACE CHAR ( 25 ) NOT NULL                                       
        , CPCG CHAR ( 4 ) NOT NULL                                              
        , NJRN CHAR ( 3 ) NOT NULL                                              
        , CNATURE CHAR ( 3 ) NOT NULL                                           
        , WNATSEC CHAR ( 1 ) NOT NULL                                           
        , WTYPSEC CHAR ( 1 ) NOT NULL                                           
        , WNATRUB CHAR ( 1 ) NOT NULL                                           
        , WTYPRUB CHAR ( 1 ) NOT NULL                                           
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM82_TAB INDEX IN P999_RTFM82_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM83                                                       
        ( CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CTYPOPER CHAR ( 5 ) NOT NULL                                          
        , NJRN CHAR ( 3 ) NOT NULL                                              
        , CNATURE CHAR ( 3 ) NOT NULL                                           
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM83_TAB INDEX IN P999_RTFM83_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM84                                                       
        ( CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CNATOPER CHAR ( 5 ) NOT NULL                                          
        , CTYPOPER CHAR ( 5 ) NOT NULL                                          
        , CTVA CHAR ( 5 ) NOT NULL                                              
        , CGEO CHAR ( 1 ) NOT NULL                                              
        , WGROUPE CHAR ( 1 ) NOT NULL                                           
        , COMPTE CHAR ( 6 ) NOT NULL                                            
        , CSENS CHAR ( 1 ) NOT NULL                                             
        , WCUMUL CHAR ( 1 ) NOT NULL                                            
        , NLETTRAGE CHAR ( 1 ) NOT NULL                                         
        , NTIERS CHAR ( 1 ) NOT NULL                                            
        , WANAL CHAR ( 1 ) NOT NULL                                             
        , CONTREP CHAR ( 6 ) NOT NULL                                           
        , WCUMULC CHAR ( 1 ) NOT NULL                                           
        , NLETTRAGEC CHAR ( 1 ) NOT NULL                                        
        , NTIERSC CHAR ( 1 ) NOT NULL                                           
        , WANALC CHAR ( 1 ) NOT NULL                                            
        , DEFFET CHAR ( 8 ) NOT NULL                                            
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM84_TAB INDEX IN P999_RTFM84_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM85                                                       
        ( SOCREF CHAR ( 5 ) NOT NULL                                            
        , CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CNATOPER CHAR ( 5 ) NOT NULL                                          
        , CTYPOPER CHAR ( 5 ) NOT NULL                                          
        , CRITERE1 CHAR ( 5 ) NOT NULL                                          
        , CRITERE2 CHAR ( 5 ) NOT NULL                                          
        , CRITERE3 CHAR ( 5 ) NOT NULL                                          
        , NSOCIETE CHAR ( 3 ) NOT NULL                                          
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , SECTION CHAR ( 6 ) NOT NULL                                           
        , SECTIONC CHAR ( 6 ) NOT NULL                                          
        , DEFFET CHAR ( 8 ) NOT NULL                                            
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM85_TAB INDEX IN P999_RTFM85_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM86                                                       
        ( SOCREF CHAR ( 5 ) NOT NULL                                            
        , CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CNATOPER CHAR ( 5 ) NOT NULL                                          
        , CTYPOPER CHAR ( 5 ) NOT NULL                                          
        , CRITERE1 CHAR ( 5 ) NOT NULL                                          
        , CRITERE2 CHAR ( 5 ) NOT NULL                                          
        , CRITERE3 CHAR ( 5 ) NOT NULL                                          
        , RUBRIQUE CHAR ( 6 ) NOT NULL                                          
        , RUBRIQUEC CHAR ( 6 ) NOT NULL                                         
        , DEFFET CHAR ( 8 ) NOT NULL                                            
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM86_TAB INDEX IN P999_RTFM86_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM87                                                       
        ( CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CZONE CHAR ( 2 ) NOT NULL                                             
        , LETTRAGE CHAR ( 15 ) NOT NULL                                         
        , LIBELLE CHAR ( 30 ) NOT NULL                                          
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM87_TAB INDEX IN P999_RTFM87_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM88                                                       
        ( CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CZONE CHAR ( 2 ) NOT NULL                                             
        , TIERS CHAR ( 15 ) NOT NULL                                            
        , LIBELLE CHAR ( 30 ) NOT NULL                                          
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , TYPTIERS CHAR ( 1 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , WSOC CHAR ( 1 ) NOT NULL                                              
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTFM88_TAB INDEX IN P999_RTFM88_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM89                                                       
        ( CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CZONE CHAR ( 2 ) NOT NULL                                             
        , LIBELLE CHAR ( 30 ) NOT NULL                                          
        , NPOS DECIMAL ( 3 , 0 ) NOT NULL                                       
        , NLONG DECIMAL ( 3 , 0 ) NOT NULL                                      
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM89_TAB INDEX IN P999_RTFM89_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM90                                                       
        ( CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CNATOPER CHAR ( 5 ) NOT NULL                                          
        , CTYPOPER CHAR ( 5 ) NOT NULL                                          
        , NZ1 DECIMAL ( 2 , 0 ) NOT NULL                                        
        , NZ2 DECIMAL ( 2 , 0 ) NOT NULL                                        
        , NZ3 DECIMAL ( 2 , 0 ) NOT NULL                                        
        , NZ4 DECIMAL ( 2 , 0 ) NOT NULL                                        
        , NZ5 DECIMAL ( 2 , 0 ) NOT NULL                                        
        , NZ6 DECIMAL ( 2 , 0 ) NOT NULL                                        
        , NZ7 DECIMAL ( 2 , 0 ) NOT NULL                                        
        , NZ8 DECIMAL ( 2 , 0 ) NOT NULL                                        
        , NZ9 DECIMAL ( 2 , 0 ) NOT NULL                                        
        , NZ10 DECIMAL ( 2 , 0 ) NOT NULL                                       
        , LIBELLE1 CHAR ( 30 ) NOT NULL                                         
        , LIBELLE2 CHAR ( 30 ) NOT NULL                                         
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM90_TAB INDEX IN P999_RTFM90_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM91                                                       
        ( CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CNATOPER CHAR ( 5 ) NOT NULL                                          
        , CTYPOPER CHAR ( 5 ) NOT NULL                                          
        , CRUB1 CHAR ( 1 ) NOT NULL                                             
        , CRUB2 CHAR ( 1 ) NOT NULL                                             
        , CRUB3 CHAR ( 1 ) NOT NULL                                             
        , CSEC1 CHAR ( 1 ) NOT NULL                                             
        , CSEC2 CHAR ( 1 ) NOT NULL                                             
        , CSEC3 CHAR ( 1 ) NOT NULL                                             
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM91_TAB INDEX IN P999_RTFM91_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM92                                                       
        ( CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CZONE CHAR ( 2 ) NOT NULL                                             
        , CRITERE CHAR ( 8 ) NOT NULL                                           
        , LCRITERE CHAR ( 25 ) NOT NULL                                         
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , SEQS CHAR ( 1 ) NOT NULL                                              
          WITH DEFAULT                                                          
        , SEQR CHAR ( 1 ) NOT NULL                                              
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTFM92_TAB INDEX IN P999_RTFM92_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM93                                                       
        ( CRITLIEU1 CHAR ( 3 ) NOT NULL                                         
        , CRITLIEU2 CHAR ( 3 ) NOT NULL                                         
        , LIBLIEU CHAR ( 30 ) NOT NULL                                          
        , LIEUPCA CHAR ( 3 ) NOT NULL                                           
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM93_TAB INDEX IN P999_RTFM93_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM94                                                       
        ( CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CNATOPER CHAR ( 5 ) NOT NULL                                          
        , CTYPOPER CHAR ( 5 ) NOT NULL                                          
        , MONTANT DECIMAL ( 3 , 0 ) NOT NULL                                    
        , DUREE DECIMAL ( 2 , 0 ) NOT NULL                                      
        , DELAI DECIMAL ( 2 , 0 ) NOT NULL                                      
        , WPRO CHAR ( 1 ) NOT NULL                                              
        , WPOIDS CHAR ( 1 ) NOT NULL                                            
        , INFO1 CHAR ( 20 ) NOT NULL                                            
        , INFO2 CHAR ( 20 ) NOT NULL                                            
        , INFO3 CHAR ( 20 ) NOT NULL                                            
        , TXANN1 DECIMAL ( 3 , 0 ) NOT NULL                                     
        , TXANN2 DECIMAL ( 3 , 0 ) NOT NULL                                     
        , TXANN3 DECIMAL ( 3 , 0 ) NOT NULL                                     
        , TXANN4 DECIMAL ( 3 , 0 ) NOT NULL                                     
        , TXANN5 DECIMAL ( 3 , 0 ) NOT NULL                                     
        , DEFFET CHAR ( 8 ) NOT NULL                                            
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM94_TAB INDEX IN P999_RTFM94_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM97                                                       
        ( CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CNATOPER CHAR ( 5 ) NOT NULL                                          
        , CTYPOPER CHAR ( 5 ) NOT NULL                                          
        , CTVA CHAR ( 5 ) NOT NULL                                              
        , CGEO CHAR ( 1 ) NOT NULL                                              
        , WGROUPE CHAR ( 1 ) NOT NULL                                           
        , CPTSAP CHAR ( 8 ) NOT NULL                                            
        , CSENS CHAR ( 1 ) NOT NULL                                             
        , WCUMUL CHAR ( 1 ) NOT NULL                                            
        , NLETTRAGE CHAR ( 1 ) NOT NULL                                         
        , NTIERSSAP CHAR ( 1 ) NOT NULL                                         
        , CCGS CHAR ( 1 ) NOT NULL                                              
        , WANAL CHAR ( 1 ) NOT NULL                                             
        , TYPMT CHAR ( 2 ) NOT NULL                                             
        , CONTREPSAP CHAR ( 8 ) NOT NULL                                        
        , WCUMULC CHAR ( 1 ) NOT NULL                                           
        , NLETTRAGEC CHAR ( 1 ) NOT NULL                                        
        , NTIERSSAPC CHAR ( 1 ) NOT NULL                                        
        , CCGSC CHAR ( 1 ) NOT NULL                                             
        , WANALC CHAR ( 1 ) NOT NULL                                            
        , TYPMTC CHAR ( 2 ) NOT NULL                                            
        , DEFFET CHAR ( 8 ) NOT NULL                                            
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , WECHEANC CHAR ( 1 ) NOT NULL                                          
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTFM97_TAB INDEX IN P999_RTFM97_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM98                                                       
        ( SOCREF CHAR ( 5 ) NOT NULL                                            
        , CINTERFACE CHAR ( 5 ) NOT NULL                                        
        , CNATOPER CHAR ( 5 ) NOT NULL                                          
        , CTYPOPER CHAR ( 5 ) NOT NULL                                          
        , CRITERE1 CHAR ( 5 ) NOT NULL                                          
        , CRITERE2 CHAR ( 5 ) NOT NULL                                          
        , CRITERE3 CHAR ( 5 ) NOT NULL                                          
        , CPTSAP CHAR ( 8 ) NOT NULL                                            
        , CONTREPSAP CHAR ( 8 ) NOT NULL                                        
        , DEFFET CHAR ( 8 ) NOT NULL                                            
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFM98_TAB INDEX IN P999_RTFM98_IDX
   ;                                                                            
CREATE  TABLE P999.RTFM99                                                       
        ( CPTSAP CHAR ( 8 ) NOT NULL                                            
        , WSTEAPP CHAR ( 1 ) NOT NULL                                           
        , CPTGCT CHAR ( 6 ) NOT NULL                                            
        , RUBRGCT CHAR ( 6 ) NOT NULL                                           
        , TYPCPT CHAR ( 1 ) NOT NULL                                            
        , CSODEBI CHAR ( 6 ) NOT NULL                                           
        , CSOCRED CHAR ( 6 ) NOT NULL                                           
        )                                                                       
      IN P999_RTFM99_TAB INDEX IN P999_RTFM99_IDX
   ;                                                                            
CREATE  TABLE P999.RTFX00                                                       
        ( NOECS CHAR ( 5 ) NOT NULL                                             
        , CRITERE1 CHAR ( 5 ) NOT NULL                                          
        , CRITERE2 CHAR ( 5 ) NOT NULL                                          
        , CRITERE3 CHAR ( 5 ) NOT NULL                                          
        , COMPTECG CHAR ( 6 ) NOT NULL                                          
        , DATEFF CHAR ( 8 ) NOT NULL                                            
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , WSEQFAM DECIMAL ( 5 , 0 ) NOT NULL                                    
          WITH DEFAULT                                                          
        , SECTION CHAR ( 6 ) NOT NULL                                           
        , RUBRIQUE CHAR ( 6 ) NOT NULL                                          
        , WFX00 CHAR ( 6 ) NOT NULL                                             
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTFX00_TAB INDEX IN P999_RTFX00_IDX
   ;                                                                            
--MW oopCREATE  TABLE P999.RTFX01                                                       
--MW oop        ( NCOMPTE CHAR ( 6 ) NOT NULL                                           
--MW oop        , CTYPCOMPTE CHAR ( 3 ) NOT NULL                                        
--MW oop        , CAGREG CHAR ( 2 ) NOT NULL                                            
--MW oop        , PMONTCA DECIMAL ( 15 , 2 ) NOT NULL                                   
--MW oop        , CTAUXTVA CHAR ( 5 ) NOT NULL                                          
--MW oop        , NOECS CHAR ( 5 ) NOT NULL                                             
--MW oop        , NCOMPTETVA CHAR ( 6 ) NOT NULL                                        
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN P999_RTFX01_TAB INDEX IN P999_RTFX01_IDX
--MW oop   ;                                                                            
CREATE  TABLE P999.RTFX05                                                       
        ( NJRN CHAR ( 10 ) NOT NULL                                             
        , NEXERCICE CHAR ( 4 ) NOT NULL                                         
        , NPERIODE CHAR ( 2 ) NOT NULL                                          
        , NPIECE CHAR ( 10 ) NOT NULL                                           
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFX05_TAB INDEX IN P999_RTFX05_IDX
   ;                                                                            
CREATE  TABLE P999.RTFX10                                                       
        ( COMPTEGL CHAR ( 6 ) NOT NULL                                          
        , COMPTECOSYS1 CHAR ( 6 ) NOT NULL                                      
        , COMPTECOSYS2 CHAR ( 6 ) NOT NULL                                      
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTFX10_TAB INDEX IN P999_RTFX10_IDX
   ;                                                                            
CREATE  TABLE P999.RTFX11                                                       
        ( CZONE CHAR ( 8 ) NOT NULL                                             
        , ZONEGL CHAR ( 12 ) NOT NULL                                           
        , ZONECOSYS CHAR ( 16 ) NOT NULL                                        
        )                                                                       
      IN P999_RTFX11_TAB INDEX IN P999_RTFX11_IDX
   ;                                                                            
CREATE  TABLE P999.RTFX15                                                       
        ( WSEQPRO DECIMAL ( 7 , 0 ) NOT NULL                                    
        , CFAMGL CHAR ( 3 ) NOT NULL                                            
        , NDEPTLMELA CHAR ( 1 ) NOT NULL                                        
        )                                                                       
      IN P999_RTFX15_TAB INDEX IN P999_RTFX15_IDX
   ;                                                                            
CREATE  TABLE P999.RTFX20                                                       
        ( CTYPSTAT CHAR ( 1 ) NOT NULL                                          
        , NDEPTLMELA CHAR ( 1 ) NOT NULL                                        
        , CFINSECT CHAR ( 3 ) NOT NULL                                          
        )                                                                       
      IN P999_RTFX20_TAB INDEX IN P999_RTFX20_IDX
   ;                                                                            
CREATE  TABLE P999.RTFX30                                                       
        ( NSOC CHAR ( 3 ) NOT NULL                                              
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , NUMCAIS CHAR ( 3 ) NOT NULL                                           
        , MTFOND DECIMAL ( 9 , 2 ) NOT NULL                                     
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        , DATTRANS CHAR ( 8 ) NOT NULL                                          
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTFX30_TAB INDEX IN P999_RTFX30_IDX
   ;                                                                            
--MW oopCREATE  TABLE P999.RTFX35                                                       
--MW oop        ( NSOC CHAR ( 3 ) NOT NULL                                              
--MW oop        , NLIEU CHAR ( 3 ) NOT NULL                                             
--MW oop        , NUMCAIS CHAR ( 3 ) NOT NULL                                           
--MW oop        , MTFOND DECIMAL ( 9 , 2 ) NOT NULL                                     
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN P999_RTFX35_TAB INDEX IN P999_RTFX35_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE P999.RTFX40                                                       
--MW oop        ( NSOC CHAR ( 3 ) NOT NULL                                              
--MW oop        , NLIEU CHAR ( 3 ) NOT NULL                                             
--MW oop        , NUMCAIS CHAR ( 3 ) NOT NULL                                           
--MW oop        , MTFOND DECIMAL ( 9 , 2 ) NOT NULL                                     
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN P999_RTFX40_TAB INDEX IN P999_RTFX40_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE P999.RTFX45                                                       
--MW oop        ( CODEAPPLI CHAR ( 1 ) NOT NULL                                         
--MW oop        , NEXERCICE CHAR ( 4 ) NOT NULL                                         
--MW oop        , NPERIODE CHAR ( 2 ) NOT NULL                                          
--MW oop        , DCREAT CHAR ( 8 ) NOT NULL                                            
--MW oop        , PMONTANT DECIMAL ( 15 , 2 ) NOT NULL                                  
--MW oop        , WNORMAL CHAR ( 1 ) NOT NULL                                           
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN P999_RTFX45_TAB INDEX IN P999_RTFX45_IDX
--MW oop   ;                                                                            
--MW oopCREATE  TABLE P999.RTFX50                                                       
--MW oop        ( DATTRANS CHAR ( 8 ) NOT NULL                                          
--MW oop        , NSOCIETE CHAR ( 3 ) NOT NULL                                          
--MW oop        , NLIEU CHAR ( 3 ) NOT NULL                                             
--MW oop        , CTYPTRANS CHAR ( 1 ) NOT NULL                                         
--MW oop        , CAPPRET CHAR ( 2 ) NOT NULL                                           
--MW oop        , MTCHE DECIMAL ( 9 , 2 ) NOT NULL                                      
--MW oop        , DCOMPTA CHAR ( 8 ) NOT NULL                                           
--MW oop        , DTRAIT CHAR ( 8 ) NOT NULL                                            
--MW oop        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
--MW oop        )                                                                       
--MW oop      IN P999_RTFX50_TAB INDEX IN P999_RTFX50_IDX
--MW oop   ;                                                                            
CREATE  TABLE P999.RTGA01                                                       
        ( CTABLEG1 CHAR ( 5 ) NOT NULL                                          
        , CTABLEG2 CHAR ( 15 ) NOT NULL                                         
        , WTABLEG CHAR ( 80 ) NOT NULL                                          
        )                                                                       
      IN P999_RTGA01_TAB INDEX IN P999_RTGA01_IDX
   ;                                                                            
CREATE  TABLE P999.RTGA10                                                       
        ( NSOCIETE CHAR ( 3 ) NOT NULL                                          
        , NLIEU CHAR ( 3 ) NOT NULL                                             
        , CTYPLIEU CHAR ( 1 ) NOT NULL                                          
        , LLIEU CHAR ( 20 ) NOT NULL                                            
        , DOUV CHAR ( 8 ) NOT NULL                                              
        , DFERM CHAR ( 8 ) NOT NULL                                             
        , NZONPRIX CHAR ( 2 ) NOT NULL                                          
        , CGRPMAG CHAR ( 2 ) NOT NULL                                           
        , CSTATEXPO CHAR ( 5 ) NOT NULL                                         
        , CACID CHAR ( 4 ) NOT NULL                                             
          WITH DEFAULT                                                          
        , WDACEM CHAR ( 1 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CTYPFACT CHAR ( 5 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , CTYPCONTR CHAR ( 5 ) NOT NULL                                         
          WITH DEFAULT                                                          
        , QTAUXESCPT DECIMAL ( 5 , 2 ) NOT NULL                                 
          WITH DEFAULT                                                          
        , LCONDRGLT CHAR ( 20 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
          WITH DEFAULT                                                          
        , LADRLIEU1 CHAR ( 32 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , LADRLIEU2 CHAR ( 32 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , LADRLIEU3 CHAR ( 32 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , CPOSTAL CHAR ( 5 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , LCOMMUNE CHAR ( 32 ) NOT NULL                                         
          WITH DEFAULT                                                          
        , CTYPSOC CHAR ( 3 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , NSOCCOMPT CHAR ( 3 ) NOT NULL                                         
          WITH DEFAULT                                                          
        , NLIEUCOMPT CHAR ( 3 ) NOT NULL                                        
          WITH DEFAULT                                                          
        , NSOCFACT CHAR ( 3 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , NLIEUFACT CHAR ( 3 ) NOT NULL                                         
          WITH DEFAULT                                                          
        , CGRPMUT CHAR ( 2 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , NSOCVALO CHAR ( 3 ) NOT NULL                                          
          WITH DEFAULT                                                          
        , NLIEUVALO CHAR ( 3 ) NOT NULL                                         
          WITH DEFAULT                                                          
        , CTYPLIEUVALO CHAR ( 1 ) NOT NULL                                      
          WITH DEFAULT                                                          
        , NSOCGRP CHAR ( 3 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , NORDRE CHAR ( 3 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID1 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID2 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID3 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID4 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID5 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CACID6 CHAR ( 4 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , CFRAIS CHAR ( 6 ) NOT NULL                                            
          WITH DEFAULT                                                          
        , SECTANA CHAR ( 6 ) NOT NULL                                           
          WITH DEFAULT                                                          
        , CCUMCA CHAR ( 1 ) NOT NULL                                            
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTGA10_TAB INDEX IN P999_RTGA10_IDX
   ;                                                                            
CREATE  TABLE P999.RTGA71                                                       
        ( CTABLE CHAR ( 6 ) NOT NULL                                            
        , CSTABLE CHAR ( 5 ) NOT NULL                                           
        , LTABLE CHAR ( 30 ) NOT NULL                                           
        , CRESP CHAR ( 3 ) NOT NULL                                             
        , DDEBUT CHAR ( 8 ) NOT NULL                                            
        , DFIN CHAR ( 8 ) NOT NULL                                              
        , DMAJ CHAR ( 8 ) NOT NULL                                              
        , CHAMP1 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP1 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP1 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP1 CHAR ( 1 ) NOT NULL                                           
        , QDEC1 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR1 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND1 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND11 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND12 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND13 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND14 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND15 CHAR ( 3 ) NOT NULL                                         
        , CTABASS1 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS1 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE1 DECIMAL ( 1 , 0 ) NOT NULL                                   
        , CHAMP2 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP2 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP2 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP2 CHAR ( 1 ) NOT NULL                                           
        , QDEC2 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR2 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND2 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND21 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND22 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND23 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND24 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND25 CHAR ( 3 ) NOT NULL                                         
        , CTABASS2 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS2 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE2 DECIMAL ( 1 , 0 ) NOT NULL                                   
        , CHAMP3 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP3 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP3 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP3 CHAR ( 1 ) NOT NULL                                           
        , QDEC3 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR3 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND3 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND31 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND32 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND33 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND34 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND35 CHAR ( 3 ) NOT NULL                                         
        , CTABASS3 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS3 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE3 DECIMAL ( 1 , 0 ) NOT NULL                                   
        , CHAMP4 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP4 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP4 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP4 CHAR ( 1 ) NOT NULL                                           
        , QDEC4 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR4 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND4 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND41 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND42 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND43 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND44 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND45 CHAR ( 3 ) NOT NULL                                         
        , CTABASS4 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS4 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE4 DECIMAL ( 1 , 0 ) NOT NULL                                   
        , CHAMP5 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP5 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP5 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP5 CHAR ( 1 ) NOT NULL                                           
        , QDEC5 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR5 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND5 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND51 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND52 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND53 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND54 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND55 CHAR ( 3 ) NOT NULL                                         
        , CTABASS5 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS5 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE5 DECIMAL ( 1 , 0 ) NOT NULL                                   
        , CHAMP6 CHAR ( 8 ) NOT NULL                                            
        , LCHAMP6 CHAR ( 30 ) NOT NULL                                          
        , QCHAMP6 DECIMAL ( 3 , 0 ) NOT NULL                                    
        , WCHAMP6 CHAR ( 1 ) NOT NULL                                           
        , QDEC6 DECIMAL ( 3 , 0 ) NOT NULL                                      
        , WCTLDIR6 CHAR ( 1 ) NOT NULL                                          
        , WCTLIND6 CHAR ( 1 ) NOT NULL                                          
        , CCTLIND61 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND62 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND63 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND64 CHAR ( 3 ) NOT NULL                                         
        , CCTLIND65 CHAR ( 3 ) NOT NULL                                         
        , CTABASS6 CHAR ( 6 ) NOT NULL                                          
        , CSTABASS6 CHAR ( 5 ) NOT NULL                                         
        , WPOSCLE6 DECIMAL ( 1 , 0 ) NOT NULL                                   
        , NOMVUE CHAR ( 8 ) NOT NULL                                            
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTGA71_TAB INDEX IN P999_RTGA71_IDX
   ;                                                                            
CREATE  TABLE P999.RTGA99                                                       
        ( CNOMPGRM CHAR ( 5 ) NOT NULL                                          
        , NSEQERR CHAR ( 4 ) NOT NULL                                           
        , CERRUT CHAR ( 4 ) NOT NULL                                            
        , LIBERR CHAR ( 53 ) NOT NULL                                           
        , CLANG CHAR ( 2 ) NOT NULL                                             
          WITH DEFAULT                                                          
        )                                                                       
      IN P999_RTGA99_TAB INDEX IN P999_RTGA99_IDX
   ;                                                                            
CREATE  TABLE P999.RTIGSA                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , COL01001 CHAR ( 92 ) NOT NULL                                         
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     )                                                                          
        )                                                                       
      IN P999_RTIGSA_TAB INDEX IN P999_RTIGSA_IDX
   ;                                                                            
CREATE  TABLE P999.RTIGSB                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , NT_DIGSB DECIMAL ( 1 , 0 ) NOT NULL                                   
        , COL05001 CHAR ( 90 ) NOT NULL                                         
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     ,NT_DIGSB                                                                  
     )                                                                          
        )                                                                       
      IN P999_RTIGSB_TAB INDEX IN P999_RTIGSB_IDX
   ;                                                                            
CREATE  TABLE P999.RTIGSC                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , DIGFC0 CHAR ( 12 ) NOT NULL                                           
        , SM_DIGSC TIMESTAMP NOT NULL                                           
        , COL06001 CHAR ( 2 ) NOT NULL                                          
        , COL06002 VARCHAR ( 144 ) NOT NULL                                     
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     ,DIGFC0                                                                    
     ,SM_DIGSC                                                                  
     )                                                                          
        )                                                                       
      IN P999_RTIGSC_TAB INDEX IN P999_RTIGSC_IDX
   ;                                                                            
CREATE  TABLE P999.RTIGSD                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , DIGFD0 CHAR ( 24 ) NOT NULL                                           
        , COL02001 CHAR ( 40 ) NOT NULL                                         
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     ,DIGFD0                                                                    
     )                                                                          
        )                                                                       
      IN P999_RTIGSD_TAB INDEX IN P999_RTIGSD_IDX
   ;                                                                            
CREATE  TABLE P999.RTIGSE                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , DIGFE0 CHAR ( 9 ) NOT NULL                                            
        , COL03001 CHAR ( 55 ) NOT NULL                                         
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     ,DIGFE0                                                                    
     )                                                                          
        )                                                                       
      IN P999_RTIGSE_TAB INDEX IN P999_RTIGSE_IDX
   ;                                                                            
CREATE  TABLE P999.RTIGSF                                                       
        ( DIGFA0 CHAR ( 36 ) NOT NULL                                           
        , DIGFF0 CHAR ( 4 ) NOT NULL                                            
        , COL04001 CHAR ( 60 ) NOT NULL                                         
   ,PRIMARY KEY                                                                 
     (DIGFA0                                                                    
     ,DIGFF0                                                                    
     )                                                                          
        )                                                                       
      IN P999_RTIGSF_TAB INDEX IN P999_RTIGSF_IDX
   ;                                                                            
CREATE  TABLE P999.RTSG01                                                       
        ( NSOC CHAR ( 3 ) NOT NULL                                              
        , NETAB CHAR ( 3 ) NOT NULL                                             
        , RUBPAIE CHAR ( 3 ) NOT NULL                                           
        , CONTRAT CHAR ( 2 ) NOT NULL                                           
        , CPTEBIL CHAR ( 6 ) NOT NULL                                           
        , SENSBIL CHAR ( 1 ) NOT NULL                                           
        , CPTECHG CHAR ( 6 ) NOT NULL                                           
        , SENSCHG CHAR ( 1 ) NOT NULL                                           
        , CSECTION CHAR ( 3 ) NOT NULL                                          
        , RUBRIQUE CHAR ( 6 ) NOT NULL                                          
        , CPTECONT CHAR ( 6 ) NOT NULL                                          
        , SENSCONT CHAR ( 1 ) NOT NULL                                          
        , SECCONT CHAR ( 3 ) NOT NULL                                           
        , RUBRCONT CHAR ( 6 ) NOT NULL                                          
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTSG01_TAB INDEX IN P999_RTSG01_IDX
   ;                                                                            
CREATE  TABLE P999.RTSG02                                                       
        ( NSOC CHAR ( 3 ) NOT NULL                                              
        , NETAB CHAR ( 3 ) NOT NULL                                             
        , SECPAIE CHAR ( 6 ) NOT NULL                                           
        , CSECTION CHAR ( 3 ) NOT NULL                                          
        , SECGRP CHAR ( 6 ) NOT NULL                                            
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTSG02_TAB INDEX IN P999_RTSG02_IDX
   ;                                                                            
CREATE  TABLE P999.RTSG03                                                       
        ( NSOC CHAR ( 3 ) NOT NULL                                              
        , NETAB CHAR ( 3 ) NOT NULL                                             
        , NETABADM CHAR ( 3 ) NOT NULL                                          
        , COMPTE CHAR ( 6 ) NOT NULL                                            
        , NTIERS CHAR ( 8 ) NOT NULL                                            
        , DSYST DECIMAL ( 13 , 0 ) NOT NULL                                     
        )                                                                       
      IN P999_RTSG03_TAB INDEX IN P999_RTSG03_IDX
   ;                                                                            
