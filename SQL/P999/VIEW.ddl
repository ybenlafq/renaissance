CREATE  VIEW P999.RVBDB2B                                                       
        ( CTABLEG2 , SOC , LIEU , WTABLEG , ACTIF , VALDEF , COMMENT )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 40 )                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BDB2B'                                                      
;                                                                               
CREATE  VIEW P999.RVBSDEF                                                       
        ( CTABLEG2 , ORIG , USER , WTABLEG , MDEF , TVDEF , CLDF ,              
        RMDACC ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 2 ) , SUBSTR ( WTABLEG , 6 , 4 ) , SUBSTR ( WTABLEG , 10 , 1        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BSDEF'                                                      
;                                                                               
CREATE  VIEW P999.RVCASIM                                                       
        ( CTABLEG2 , NCASE , WTABLEG , IMPR , TAILLE , ALLEE ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 1 ) , SUBSTR (             
        WTABLEG , 12 , 2 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CASIM'                                                      
;                                                                               
CREATE  VIEW P999.RVCAXIS                                                       
        ( CTABLEG2 , CMOPAI , WTABLEG , CARDT , COPERD , COPERC , CTRLC         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 1 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CAXIS'                                                      
;                                                                               
CREATE  VIEW P999.RVCCSTA                                                       
        ( CTABLEG2 , CSTATUT , TYPE , WTABLEG , LSTATUT ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCSTA'                                                      
;                                                                               
CREATE  VIEW P999.RVCREAS                                                       
        ( CTABLEG2 , CREASON , WTABLEG , LREASON , WACTIF , WNSEQ ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 57 ) , SUBSTR ( WTABLEG , 58 , 1 ) , SUBSTR (             
        WTABLEG , 59 , 3 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CREAS'                                                      
;                                                                               
CREATE  VIEW P999.RVCRFIL                                                       
        ( CTABLEG2 , CNSOC , NLIEU , WTABLEG , SAVREF , PRFDOS ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG ,         
        7 , 3 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CRFIL'                                                      
;                                                                               
CREATE  VIEW P999.RVCTSAP                                                       
        ( CTABLEG2 , CENT , TYPC , WTABLEG , CSAP , LCODE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG        
        , 21 , 30 )                                                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CTSAP'                                                      
;                                                                               
CREATE  VIEW P999.RVCTTVA                                                       
        ( CTABLEG2 , NATTVA , TXTVA , WTABLEG , WACTIF , CTVASAP ,              
        TYPTVA , CPTSAP ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        4 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 2 ) , SUBSTR ( WTABLEG , 4 , 4 ) , SUBSTR ( WTABLEG , 8 , 8         
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CTTVA'                                                      
;                                                                               
CREATE  VIEW P999.RVDTENV                                                       
        ( CTABLEG2 , RACPRF , WTABLEG , NSOCIETE , QMAS400 , QMHOST ,           
        BIBFIC , BIBTAB ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 8 ) , SUBSTR (               
        WTABLEG , 12 , 4 ) , SUBSTR ( WTABLEG , 16 , 10 ) , SUBSTR (            
        WTABLEG , 26 , 10 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DTENV'                                                      
;                                                                               
CREATE  VIEW P999.RVEDTST                                                       
        ( CTABLEG2 , SOCIETE , LIEU , WTABLEG , PARAM ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EDTST'                                                      
;                                                                               
CREATE  VIEW P999.RVEG0000                                                      
        AS                                                                      
SELECT  NOMETAT , RUPTCHTPAGE , RUPTRAZPAGE , LARGEUR , LONGUEUR ,              
        DSECTCREE , TRANSFERE , DSYST , LIBETAT                                 
    FROM  P999.RTEG00                                                           
;                                                                               
CREATE  VIEW P999.RVEG0500                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , SKIPBEFORE ,                 
        SKIPAFTER , RUPTIMPR , NOMCHAMPC , NOMCHAMPS , SOUSTABLE , DSYST        
    FROM  P999.RTEG05                                                           
;                                                                               
CREATE  VIEW P999.RVEG1000                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , POSCHAMP , NOMCHAMP ,        
        RUPTIMPR , CHCOND1 , CONDITION , CHCOND2 , DSYST , MASKCHAMP            
    FROM  P999.RTEG10                                                           
;                                                                               
CREATE  VIEW P999.RVEG1100                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , POSCHAMP , RUPTIMPR ,        
        SOUSTABLE , NOMCHAMPS , NOMCHAMPC , CHCOND1 , CONDITION ,               
        CHCOND2 , DSYST , LIBCHAMP                                              
    FROM  P999.RTEG11                                                           
;                                                                               
CREATE  VIEW P999.RVEG1500                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , POSCHAMP , NOMCHAMPT         
        , RUPTRAZ , DSYST                                                       
    FROM  P999.RTEG15                                                           
;                                                                               
CREATE  VIEW P999.RVEG2000                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , POSCHAMP , DSYST ,           
        LIBCHAMP                                                                
    FROM  P999.RTEG20                                                           
;                                                                               
CREATE  VIEW P999.RVEG2500                                                      
        AS                                                                      
SELECT  NOMETAT , NOMCHAMP , LIGNECALC , CHCALC1 , TYPCALCUL , CHCALC2 ,        
        DSYST                                                                   
    FROM  P999.RTEG25                                                           
;                                                                               
CREATE  VIEW P999.RVEG3000                                                      
        AS                                                                      
SELECT  NOMETAT , NOMCHAMP , TYPCHAMP , LGCHAMP , DECCHAMP , POSDSECT ,         
        OCCURENCES , DSYST                                                      
    FROM  P999.RTEG30                                                           
;                                                                               
CREATE  VIEW P999.RVEG4000                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , NOMCHAMP , DSYST             
    FROM  P999.RTEG40                                                           
;                                                                               
CREATE  VIEW P999.RVEG9000                                                      
        AS                                                                      
SELECT  IDENTTS , NOMETAT , CHAMPTRI , RANGTS                                   
    FROM  P999.RTEG90                                                           
;                                                                               
CREATE  VIEW P999.RVEGPRT                                                       
        ( CTABLEG2 , CETAT , CTERM , WTABLEG , CIMPRIM , LIBELLE ,              
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 4 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG        
        , 11 , 30 ) , SUBSTR ( WTABLEG , 41 , 1 )                               
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EGPRT'                                                      
;                                                                               
CREATE  VIEW P999.RVFCCLI                                                       
        ( CTABLEG2 , NCLIB2B , WTABLEG , WACTIF , CFAC , PARAMF ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 7 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FCCLI'                                                      
;                                                                               
CREATE  VIEW P999.RVFIFAC                                                       
        ( CTABLEG2 , NCODIC , WTABLEG , WACTIF , CVENT ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FIFAC'                                                      
;                                                                               
CREATE  VIEW P999.RVFM0100                                                      
        AS                                                                      
SELECT  NENTITE , NAUX , LAUX , WTYPAUX , WSAISIE , WAUXNCG , WLETTRE ,         
        WRELANCE , DSYST                                                        
    FROM  P999.RTFM01                                                           
;                                                                               
CREATE  VIEW P999.RVFM0101                                                      
        AS                                                                      
SELECT  NENTITE , NAUX , LAUX , WTYPAUX , WSAISIE , WAUXNCG , WLETTRE ,         
        WRELANCE , DSYST , WAUXARF                                              
    FROM  P999.RTFM01                                                           
;                                                                               
CREATE  VIEW P999.RVFM0200                                                      
        AS                                                                      
SELECT  NENTITE , CDEVISE , WSENS , COMPTE , SECTION , RUBRIQUE , DSYST         
    FROM  P999.RTFM02                                                           
;                                                                               
CREATE  VIEW P999.RVFM0300                                                      
        AS                                                                      
SELECT  NENTITE , CMETHODE , LMETHODE , CDOC , WRIB , WNUMAUTO , WEAP ,         
        DELAI , WACTIF , DSYST                                                  
    FROM  P999.RTFM03                                                           
;                                                                               
CREATE  VIEW P999.RVFM0400                                                      
        AS                                                                      
SELECT  NENTITE , CDEVISE , LDEVISE , NBDECIM , DSYST                           
    FROM  P999.RTFM04                                                           
;                                                                               
CREATE  VIEW P999.RVFM0401                                                      
        AS                                                                      
SELECT  ALL NENTITE , CDEVISE , LDEVISE , NBDECIM , DSYST , LDEV , LSUB         
        , LSUBDIVIS , CCAISSE , WEURO                                           
    FROM  P999.RTFM04                                                           
;                                                                               
CREATE  VIEW P999.RVFM0402                                                      
        AS                                                                      
SELECT  NENTITE , CDEVISE , LDEVISE , NBDECIM , DSYST , LDEV , LSUB ,           
        LSUBDIVIS , CCAISSE , WEURO , CDEVISENUM                                
    FROM  P999.RTFM04                                                           
;                                                                               
CREATE  VIEW P999.RVFM0500                                                      
        AS                                                                      
SELECT  NENTITE , CDEVORIG , CDEVDEST , DEFFET , COPERAT , PTAUX , DSYST        
    FROM  P999.RTFM05                                                           
;                                                                               
CREATE  VIEW P999.RVFM0600                                                      
        AS                                                                      
SELECT  NENTITE , CETAT , NDEMANDE , WMFICHE , WPAPIER , NETAB ,                
        WTRIETAB , WNTRIETAB , NAUXMIN , NAUXMAX , WTYPAUX , WTRIAUX ,          
        WNTRIAUX , NTIERSMIN , NTIERSMAX , WTRITIERS , WNTRITIERS ,             
        CMETHODE , WTRIMETHODE , WNTRIMETHODE , NCOMPTEMIN , NCOMPTEMAX         
        , WTRICOMPTE , WNTRICOMPTE , NCPTEAUXMIN , NCPTEAUXMAX ,                
        WTRICPTEAUX , WNTRICPTEAUX , NSECTIONMIN , NSECTIONMAX ,                
        WTRISECTION , WNTRISECTION , NRUBRIQUEMIN , NRUBRIQUEMAX ,              
        WTRIRUBRIQUE , WNTRIRUBRIQUE , NBANQUEMIN , NBANQUEMAX ,                
        WTRIBANQUE , WNTRIBANQUE , DECHEANCEMIN , DECHEANCEMAX ,                
        WTRIECHEANCE , WNTRIECHEANCE , CDEVISE , WPTOTDEVISE ,                  
        WTTRIECRIT , WTTRITIERS , WEDTTIERS , WTSELTIERS , WCOMPTESOLDE         
        , WPIECELETTRE , CNAT1 , CNAT2 , CNAT3 , CNAT4 , CNAT5 , CNAT6 ,        
        CNAT7 , CNAT8 , CNAT9 , CNAT10 , CNATE1 , CNATE2 , CNATE3 ,             
        CNATE4 , CNATE5 , CNATE6 , CNATE7 , CNATE8 , CNATE9 , CNATE10 ,         
        NJOURNALMIN , NJOURNALMAX , WJOURNALGAL , DCREATION , DMAJ ,            
        CACID , DSYST                                                           
    FROM  P999.RTFM06                                                           
;                                                                               
--MW DZ-400 CREATE  VIEW P999.RVFM1000                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  NENTITE , COMPTE , NAUX , WCOLLECTIF , WAUXILIARISE , WSLIDSLAC         
--MW DZ-400         , WLETTRAGE , WPOINTAGE , WTYPCOMPTE , WEDITION , WGL , WCLOTURE        
--MW DZ-400         , WREGAUTO , CEPURATION                                                 
--MW DZ-400     FROM  P999.RTFM10                                                           
--MW DZ-400 ;                                                                               
CREATE  VIEW P999.RVFM1100                                                      
        AS                                                                      
SELECT  CPCG , LPCG                                                             
    FROM  p999.RTFM11                                                           
;                                                                               
CREATE  VIEW P999.RVFM1300                                                      
        AS                                                                      
SELECT  CPCG , COMPTE , NSEQ , CCATEGORIE , DSYST                               
    FROM  p999.RTFM13                                                           
;                                                                               
CREATE  VIEW P999.RVFM1400                                                      
        AS                                                                      
SELECT  CPDS , LPDS                                                             
    FROM  p999.RTFM14                                                           
;                                                                               
CREATE  VIEW P999.RVFM1600                                                      
        AS                                                                      
SELECT  CPDS , SECTION , NSEQ , CCATEGORIE , DSYST                              
    FROM  p999.RTFM16                                                           
;                                                                               
CREATE  VIEW P999.RVFM1700                                                      
        AS                                                                      
SELECT  CPDR , LPDR                                                             
    FROM  p999.RTFM17                                                           
;                                                                               
CREATE  VIEW P999.RVFM1900                                                      
        AS                                                                      
SELECT  CPDR , RUBRIQUE , NSEQ , CCATEGORIE , DSYST                             
    FROM  p999.RTFM19                                                           
;                                                                               
CREATE  VIEW P999.RVFM2000                                                      
        AS                                                                      
SELECT  NENTITE , LENTITE , CPCG , CPDS , CPDR , CDEVISE , DMAJ , DSYST         
    FROM  P999.RTFM20                                                           
;                                                                               
CREATE  VIEW P999.RVFM2001                                                      
        AS                                                                      
SELECT  NENTITE , LENTITE , CPCG , CPDS , CPDR , CDEVISE , DMAJ , DSYST         
        , ANCCPDS , ANCCPDR                                                     
    FROM  P999.RTFM20                                                           
;                                                                               
CREATE  VIEW P999.RVFM2300                                                      
        AS                                                                      
SELECT  NENTITE , NEXER , NANCIV , NMOISDEB , DSYST                             
    FROM  p999.RTFM23                                                           
;                                                                               
CREATE  VIEW P999.RVFM2500                                                      
        AS                                                                      
SELECT  NJRN , LJRN , WCTRPASS , WCTRPART , WNUMAUTO , WJRNREG , WEDTCRS        
        , COMPTE , SECTION , RUBRIQUE , STEAPP , CDEVISE , NAUX ,               
        WBATCHTP , DMAJ , DSYST                                                 
    FROM  P999.RTFM25                                                           
;                                                                               
CREATE  VIEW P999.RVFM3000                                                      
        AS                                                                      
SELECT  NATURE , LNATURE , WSENS , DMAJ , DSYST                                 
    FROM  P999.RTFM30                                                           
;                                                                               
CREATE  VIEW P999.RVFM5400                                                      
        AS                                                                      
SELECT  CODE , LIBELLE , DSYST                                                  
    FROM  P999.RTFM54                                                           
;                                                                               
CREATE  VIEW P999.RVFM5500                                                      
        AS                                                                      
SELECT  CODE , LIBELLE , CODEGEO , DSYST                                        
    FROM  P999.RTFM55                                                           
;                                                                               
CREATE  VIEW P999.RVFM5501                                                      
        AS                                                                      
SELECT  CODE , LIBELLE , CODEGEO , DSYST , CODEBDF                              
    FROM  P999.RTFM55                                                           
;                                                                               
CREATE  VIEW P999.RVFM5600                                                      
        AS                                                                      
SELECT  CODE , LIBELLE , DSYST                                                  
    FROM  P999.RTFM56                                                           
;                                                                               
CREATE  VIEW P999.RVFM5700                                                      
        AS                                                                      
SELECT  CODE , LIBELLE , DSYST                                                  
    FROM  P999.RTFM57                                                           
;                                                                               
CREATE  VIEW P999.RVFM5800                                                      
        AS                                                                      
SELECT  CODE , LIBELLE , NBMOIS , DSYST                                         
    FROM  P999.RTFM58                                                           
;                                                                               
CREATE  VIEW P999.RVFM5900                                                      
        AS                                                                      
SELECT  CODE , LIBELLE , DSYST                                                  
    FROM  P999.RTFM59                                                           
;                                                                               
CREATE  VIEW P999.RVFM6000                                                      
        AS                                                                      
SELECT  CPCG , COMPTE , LCOMPTEC , LCOMPTEL , CMASQUE , CCONSOD ,               
        CCONSOC , NAUX , WTYPCOMPTE , WAUXILIARISE , WCOLLECTIF ,               
        WABONNE , WREGAUTO , WCATNATCH , WLETTRAGE , WPOINTAGE ,                
        WEDITION , WCLOTURE , WINTERFACE , CEPURATION , DCLOTURE , DMAJ         
        , DSYST                                                                 
    FROM  p999.RTFM60                                                           
;                                                                               
CREATE  VIEW P999.RVFM6001                                                      
        AS                                                                      
SELECT  CPCG , COMPTE , LCOMPTEC , LCOMPTEL , CMASQUE , CCONSOD ,               
        CCONSOC , NAUX , WTYPCOMPTE , WAUXILIARISE , WCOLLECTIF ,               
        WABONNE , WREGAUTO , WCATNATCH , WLETTRAGE , WPOINTAGE ,                
        WEDITION , WCLOTURE , WINTERFACE , CEPURATION , DCLOTURE , DMAJ         
        , DSYST , WREGAUTOC                                                     
    FROM  P999.RTFM60                                                           
;                                                                               
CREATE  VIEW P999.RVFM6100                                                      
        AS                                                                      
SELECT  CPDS , SECTION , LSECTIONC , LSECTIONL , CMASQUE , WINTERFACE ,         
        DCLOTURE , DMAJ , DSYST                                                 
    FROM  p999.RTFM61                                                           
;                                                                               
CREATE  VIEW P999.RVFM6102                                                      
        AS                                                                      
SELECT  CPDS , SECTION , LSECTIONC , LSECTIONL , CMASQUE , WINTERFACE ,         
        DCLOTURE , DMAJ , DSYST , WFIR                                          
    FROM  P999.RTFM61                                                           
;                                                                               
CREATE  VIEW P999.RVFM6103                                                      
        AS                                                                      
SELECT  CPDS , SECTION , LSECTIONC , LSECTIONL , CMASQUE , WINTERFACE ,         
        DCLOTURE , DMAJ , DSYST , WFIR , NSOC                                   
    FROM  P999.RTFM61                                                           
;                                                                               
CREATE  VIEW P999.RVFM6200                                                      
        AS                                                                      
SELECT  CPDR , RUBRIQUE , LRUBRIQUEC , LRUBRIQUEL , CMASQUE , WINTERFACE        
        , DCLOTURE , DMAJ , DSYST                                               
    FROM  p999.RTFM62                                                           
;                                                                               
CREATE  VIEW P999.RVFM6400                                                      
        AS                                                                      
SELECT  STEAPP , LSTEAPPC , LSTEAPPL , CMASQUE , DCLOTURE , DMAJ , DSYST        
    FROM  P999.RTFM64                                                           
;                                                                               
CREATE  VIEW P999.RVFM6401                                                      
        AS                                                                      
SELECT  STEAPP , LSTEAPPC , LSTEAPPL , CMASQUE , DCLOTURE , DMAJ , DSYST        
        , NSIRET                                                                
    FROM  P999.RTFM64                                                           
;                                                                               
CREATE  VIEW P999.RVFM6500                                                      
        AS                                                                      
SELECT  NENTITE , CMASQUEC , CMASQUES , CMASQUER , CMASQUEE , CMASQUEA ,        
        DCLOTURE , DSYST                                                        
    FROM  P999.RTFM65                                                           
;                                                                               
CREATE  VIEW P999.RVFM6900                                                      
        AS                                                                      
SELECT  COMPTE , LCOMPTEC , LCOMPTEL , DCLOTURE , DMAJ , DSYST                  
    FROM  P999.RTFM69                                                           
;                                                                               
CREATE  VIEW P999.RVFM7300                                                      
        AS                                                                      
SELECT  CNOMPROG , LNOMPROG , CTYPE , WTRT1 , WTRT2 , WSEMAINE , NJRN ,         
        NATURE , CETAT , DSYST                                                  
    FROM  P999.RTFM73                                                           
;                                                                               
CREATE  VIEW P999.RVFM8000                                                      
        AS                                                                      
SELECT  CNATOPER , LNATOPER , DMAJ , DSYST                                      
    FROM  P999.RTFM80                                                           
;                                                                               
CREATE  VIEW P999.RVFM8100                                                      
        AS                                                                      
SELECT  CTYPOPER , LTYPOPER , DMAJ , DSYST                                      
    FROM  P999.RTFM81                                                           
;                                                                               
CREATE  VIEW P999.RVFM8200                                                      
        AS                                                                      
SELECT  CINTERFACE , LINTERFACE , CPCG , NJRN , CNATURE , WNATSEC ,             
        WTYPSEC , WNATRUB , WTYPRUB , DMAJ , DSYST                              
    FROM  P999.RTFM82                                                           
;                                                                               
CREATE  VIEW P999.RVFM8300                                                      
        AS                                                                      
SELECT  CINTERFACE , CTYPOPER , NJRN , CNATURE , DMAJ , DSYST                   
    FROM  P999.RTFM83                                                           
;                                                                               
CREATE  VIEW P999.RVFM8400                                                      
        AS                                                                      
SELECT  CINTERFACE , CNATOPER , CTYPOPER , CTVA , CGEO , WGROUPE ,              
        COMPTE , CSENS , WCUMUL , NLETTRAGE , NTIERS , WANAL , CONTREP ,        
        WCUMULC , NLETTRAGEC , NTIERSC , WANALC , DEFFET , DMAJ , DSYST         
    FROM  P999.RTFM84                                                           
;                                                                               
CREATE  VIEW P999.RVFM8500                                                      
        AS                                                                      
SELECT  SOCREF , CINTERFACE , CNATOPER , CTYPOPER , CRITERE1 , CRITERE2         
        , CRITERE3 , NSOCIETE , NLIEU , SECTION , SECTIONC , DEFFET ,           
        DMAJ , DSYST                                                            
    FROM  P999.RTFM85                                                           
;                                                                               
CREATE  VIEW P999.RVFM8600                                                      
        AS                                                                      
SELECT  SOCREF , CINTERFACE , CNATOPER , CTYPOPER , CRITERE1 , CRITERE2         
        , CRITERE3 , RUBRIQUE , RUBRIQUEC , DEFFET , DMAJ , DSYST               
    FROM  P999.RTFM86                                                           
;                                                                               
CREATE  VIEW P999.RVFM8700                                                      
        AS                                                                      
SELECT  CINTERFACE , CZONE , LETTRAGE , LIBELLE , DMAJ , DSYST                  
    FROM  P999.RTFM87                                                           
;                                                                               
--MW DZ-400 CREATE  VIEW P999.RVFM8800                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  CINTERFACE , CZONE , TIERS , LIBELLE , DMAJ , DSYST                     
--MW DZ-400     FROM  P999.RTFM88                                                           
--MW DZ-400 ;                                                                               
CREATE  VIEW P999.RVFM8801                                                      
        AS                                                                      
SELECT  CINTERFACE , CZONE , TIERS , LIBELLE , DMAJ , DSYST , TYPTIERS ,        
        WSOC                                                                    
    FROM  P999.RTFM88                                                           
;                                                                               
CREATE  VIEW P999.RVFM8900                                                      
        AS                                                                      
SELECT  CINTERFACE , CZONE , LIBELLE , NPOS , NLONG , DMAJ , DSYST              
    FROM  P999.RTFM89                                                           
;                                                                               
CREATE  VIEW P999.RVFM9000                                                      
        AS                                                                      
SELECT  CINTERFACE , CNATOPER , CTYPOPER , NZ1 , NZ2 , NZ3 , NZ4 , NZ5 ,        
        NZ6 , NZ7 , NZ8 , NZ9 , NZ10 , LIBELLE1 , LIBELLE2 , DMAJ ,             
        DSYST                                                                   
    FROM  P999.RTFM90                                                           
;                                                                               
CREATE  VIEW P999.RVFM9100                                                      
        AS                                                                      
SELECT  CINTERFACE , CNATOPER , CTYPOPER , CRUB1 , CRUB2 , CRUB3 , CSEC1        
        , CSEC2 , CSEC3 , DMAJ , DSYST                                          
    FROM  P999.RTFM91                                                           
;                                                                               
CREATE  VIEW P999.RVFM9200                                                      
        AS                                                                      
SELECT  CINTERFACE , CZONE , CRITERE , LCRITERE , DMAJ , DSYST                  
    FROM  P999.RTFM92                                                           
;                                                                               
CREATE  VIEW P999.RVFM9201                                                      
        AS                                                                      
SELECT  CINTERFACE , CZONE , CRITERE , LCRITERE , DMAJ , DSYST , SEQS ,         
        SEQR                                                                    
    FROM  P999.RTFM92                                                           
;                                                                               
CREATE  VIEW P999.RVFM9300                                                      
        AS                                                                      
SELECT  CRITLIEU1 , CRITLIEU2 , LIBLIEU , LIEUPCA , DMAJ , DSYST                
    FROM  P999.RTFM93                                                           
;                                                                               
CREATE  VIEW P999.RVFM9400                                                      
        ( "CINTERFACE" , "CNATOPER" , "CTYPOPER" , "MONTANT" , "DUREE" ,        
        "DELAI" , "WPRO" , "WPOIDS" , "INFO1" , "INFO2" , "INFO3" ,             
        "TXANN1" , "TXANN2" , "TXANN3" , "TXANN4" , "TXANN5" , "DEFFET"         
        , "DMAJ" , "DSYST" ) AS                                                 
SELECT  CINTERFACE , CNATOPER , CTYPOPER , MONTANT , DUREE , DELAI ,            
        WPRO , WPOIDS , INFO1 , INFO2 , INFO3 , TXANN1 , TXANN2 , TXANN3        
        , TXANN4 , TXANN5 , DEFFET , DMAJ , DSYST                               
    FROM  P999.RTFM94                                                           
;                                                                               
CREATE  VIEW P999.RVFM9700                                                      
        AS                                                                      
SELECT  CINTERFACE , CNATOPER , CTYPOPER , CTVA , CGEO , WGROUPE ,              
        CPTSAP , CSENS , WCUMUL , NLETTRAGE , NTIERSSAP , CCGS , WANAL ,        
        TYPMT , CONTREPSAP , WCUMULC , NLETTRAGEC , NTIERSSAPC , CCGSC ,        
        WANALC , TYPMTC , DEFFET , DMAJ , DSYST                                 
    FROM  P999.RTFM97                                                           
;                                                                               
CREATE  VIEW P999.RVFM9701                                                      
        AS                                                                      
SELECT  CINTERFACE , CNATOPER , CTYPOPER , CTVA , CGEO , WGROUPE ,              
        CPTSAP , CSENS , WCUMUL , NLETTRAGE , NTIERSSAP , CCGS , WANAL ,        
        TYPMT , CONTREPSAP , WCUMULC , NLETTRAGEC , NTIERSSAPC , CCGSC ,        
        WANALC , TYPMTC , DEFFET , DMAJ , DSYST , WECHEANC                      
    FROM  P999.RTFM97                                                           
;                                                                               
CREATE  VIEW P999.RVFM9800                                                      
        AS                                                                      
SELECT  SOCREF , CINTERFACE , CNATOPER , CTYPOPER , CRITERE1 , CRITERE2         
        , CRITERE3 , CPTSAP , CONTREPSAP , DEFFET , DMAJ , DSYST                
    FROM  P999.RTFM98                                                           
;                                                                               
CREATE  VIEW P999.RVFM9900                                                      
        AS                                                                      
SELECT  CPTSAP , WSTEAPP , CPTGCT , RUBRGCT , TYPCPT , CSODEBI , CSOCRED        
    FROM  P999.RTFM99                                                           
;                                                                               
CREATE  VIEW P999.RVFTCEG                                                       
        ( CTABLEG2 , CEGA , WTABLEG , LIBCEGA ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTCEG'                                                      
;                                                                               
CREATE  VIEW P999.RVFTDIV                                                       
        ( CTABLEG2 , ELTCTL , TYPCTL , WTABLEG , WACTIF , LIBELLE ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 12 ) , SUBSTR ( CTABLEG2 , 13        
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 50 )                                                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTDIV'                                                      
;                                                                               
CREATE  VIEW P999.RVFTEFG                                                       
        ( CTABLEG2 , CODPARAM , WTABLEG , VALPARAM , LIBPARAM ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 30 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTEFG'                                                      
;                                                                               
CREATE  VIEW P999.RVFTPCA                                                       
        ( CTABLEG2 , CODLIEU , WTABLEG , LIBLIEU ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTPCA'                                                      
;                                                                               
CREATE  VIEW P999.RVFTTYP                                                       
        ( CTABLEG2 , CTYPE , WTABLEG , WACTIF , LTYPE ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTTYP'                                                      
;                                                                               
CREATE  VIEW P999.RVFX0000                                                      
        AS                                                                      
SELECT  NOECS , CRITERE1 , CRITERE2 , CRITERE3 , COMPTECG , DATEFF ,            
        DSYST , WSEQFAM , SECTION , RUBRIQUE                                    
    FROM  P999.RTFX00                                                           
;                                                                               
--MW DZ-400 CREATE  VIEW P999.RVFX0100                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL NCOMPTE , CTYPCOMPTE , CAGREG , PMONTCA , CTAUXTVA , NOECS ,        
--MW DZ-400         NCOMPTETVA , DSYST                                                      
--MW DZ-400     FROM  P999.RTFX01                                                           
--MW DZ-400 ;                                                                               
CREATE  VIEW P999.RVFX0500                                                      
        AS                                                                      
SELECT  NJRN , NEXERCICE , NPERIODE , NPIECE , DSYST                            
    FROM  P999.RTFX05                                                           
;                                                                               
CREATE  VIEW P999.RVFX1000                                                      
        AS                                                                      
SELECT  COMPTEGL , COMPTECOSYS1 , COMPTECOSYS2 , DSYST                          
    FROM  P999.RTFX10                                                           
;                                                                               
CREATE  VIEW P999.RVFX1100                                                      
        AS                                                                      
SELECT  CZONE , ZONEGL , ZONECOSYS                                              
    FROM  P999.RTFX11                                                           
;                                                                               
CREATE  VIEW P999.RVFX1500                                                      
        AS                                                                      
SELECT  WSEQPRO , CFAMGL , NDEPTLMELA                                           
    FROM  P999.RTFX15                                                           
;                                                                               
CREATE  VIEW P999.RVFX2000                                                      
        AS                                                                      
SELECT  CTYPSTAT , NDEPTLMELA , CFINSECT                                        
    FROM  P999.RTFX20                                                           
;                                                                               
--MW DZ-400 CREATE  VIEW P999.RVFX3000                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL NSOC , NLIEU , NUMCAIS , MTFOND , DSYST                             
--MW DZ-400     FROM  P999.RTFX30                                                           
--MW DZ-400 ;                                                                               
CREATE  VIEW P999.RVFX3001                                                      
        AS                                                                      
SELECT  ALL NSOC , NLIEU , NUMCAIS , MTFOND , DSYST , DATTRANS                  
    FROM  P999.RTFX30                                                           
;                                                                               
--MW DZ-400 CREATE  VIEW P999.RVFX3500                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL NSOC , NLIEU , NUMCAIS , MTFOND , DSYST                             
--MW DZ-400     FROM  P999.RTFX35                                                           
--MW DZ-400 ;                                                                               
--MW DZ-400 CREATE  VIEW P999.RVFX4000                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL NSOC , NLIEU , NUMCAIS , MTFOND , DSYST                             
--MW DZ-400     FROM  P999.RTFX40                                                           
--MW DZ-400 ;                                                                               
--MW DZ-400 CREATE  VIEW P999.RVFX4500                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  CODEAPPLI , NEXERCICE , NPERIODE , DCREAT , PMONTANT , WNORMAL ,        
--MW DZ-400         DSYST                                                                   
--MW DZ-400     FROM  P999.RTFX45                                                           
--MW DZ-400 ;                                                                               
--MW DZ-400 CREATE  VIEW P999.RVFX5000                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL DATTRANS , NSOCIETE , NLIEU , CTYPTRANS , CAPPRET , MTCHE ,         
--MW DZ-400         DCOMPTA , DTRAIT , DSYST                                                
--MW DZ-400     FROM  P999.RTFX50                                                           
--MW DZ-400 ;                                                                               
CREATE  VIEW P999.RVGA0100                                                      
        AS                                                                      
SELECT  CTABLEG1 , CTABLEG2 , WTABLEG                                           
    FROM  P999.RTGA01                                                           
;                                                                               
CREATE  VIEW P999.RVGA01A0                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , CCDAC ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCDAC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01A1                                                      
        ( CTABLEG2 , CSEQ , WTABLEG , CTAB ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE00'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01A2                                                      
        ( CTABLEG2 , CFAMCCE , WTABLEG , LFAMCCE ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE01'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01A3                                                      
        ( CTABLEG2 , CSFAMCCE , WTABLEG , LSFAMCCE , CCRICCE , COPTCCE ,        
        CLIVR ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 18 ) , SUBSTR (            
        WTABLEG , 39 , 6 ) , SUBSTR ( WTABLEG , 45 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE02'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01A4                                                      
        ( CTABLEG2 , CCRICCE , WTABLEG , LCRICCE ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE03'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01A5                                                      
        ( CTABLEG2 , CVALCRI , WTABLEG , LVALCRI ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE04'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01A6                                                      
        ( CTABLEG2 , CSFAMCCE , CTRANCHE , WTABLEG , PRIXMINI , PRIXMAXI        
        , LTRANCHE ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 8 ) , SUBSTR ( WTABLEG , 17 , 20 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE05'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01A7                                                      
        ( CTABLEG2 , COPTCCE , WTABLEG , LOPTCCE ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE06'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01A8                                                      
        ( CTABLEG2 , CVOPTCCE , WTABLEG , LVOPTCCE ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE07'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01A9                                                      
        ( CTABLEG2 , CTXTVA , WTABLEG , VTXTVA ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE08'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AA                                                      
        ( CTABLEG2 , CACCES , WTABLEG , LACCES ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ACCES'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AB                                                      
        ( CTABLEG2 , CZONE , NSEQ , WTABLEG , CNIVEAUX , CPOSIT , CAGR1         
        , CAGR2 ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG ,         
        5 , 6 ) , SUBSTR ( WTABLEG , 11 , 2 ) , SUBSTR ( WTABLEG , 13 ,         
        2 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'AGRAL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AC                                                      
        ( CTABLEG2 , NAGREG , WTABLEG , LAGREG ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'AGRCL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AD                                                      
        ( CTABLEG2 , NAGREG , WTABLEG , LAGREG ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 26 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'AGRPR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AE                                                      
        ( CTABLEG2 , CAPPOR , WTABLEG , LIBAPPOR ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'APPOR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AF                                                      
        ( CTABLEG2 , CAPPRO , WTABLEG , LAPPRO ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'APPRO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AG                                                      
        ( CTABLEG2 , CPARAM , TAFF , CAFF , WTABLEG , W , LIB , T ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        1 ) , SUBSTR ( CTABLEG2 , 4 , 1 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR ( WTABLEG , 22 ,         
        1 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'APRET'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AH                                                      
        ( CTABLEG2 , ASSOR , WTABLEG , LASSOR ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ASSOR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AI                                                      
        ( CTABLEG2 , CTABLE , WTABLEG , MODE ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTOC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AJ                                                      
        ( CTABLEG2 , CODTABLE , WTABLEG , TYPEAUTO , W ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 1 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTOF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AK                                                      
        ( CTABLEG2 , CPSWD , WTABLEG , SOCIETE , LIEU , NOM ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 ) , SUBSTR (               
        WTABLEG , 7 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTOM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AL                                                      
        ( CTABLEG2 , SOCIETE , NLIEU , WTABLEG , FLAG ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BBTE '                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AM                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG ) AS                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BCDEX'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AN                                                      
        ( CTABLEG2 , CSEQ , WTABLEG , CTAB ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BOR00'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AO                                                      
        ( CTABLEG2 , CRAYON , WTABLEG , WACTIF , LRAYON , NSEQ ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 2 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BOR01'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AP                                                      
        ( CTABLEG2 , CFAMBOR , WTABLEG , WACTIF , LFAMBOR ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BOR02'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AQ                                                      
        ( CTABLEG2 , CFAMCCL , WTABLEG , WACTIF , CRITERE , COPTION ,           
        CDESCRIP , ADDRESSE ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 5 ) , SUBSTR ( WTABLEG , 37 , 5 ) , SUBSTR (             
        WTABLEG , 42 , 3 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BOR03'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AR                                                      
        ( CTABLEG2 , CRITERE , WTABLEG , WACTIF , LCRITBOR ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BOR04'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AS                                                      
        ( CTABLEG2 , CRAYON , NSEQ , WTABLEG , WACTIF , CRAYFAM ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BOR07'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AT                                                      
        ( CTABLEG2 , CFAMBOR , NSEQFAM , WTABLEG , WACTIF , CFAMCCL ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6         
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 15 )                                                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BOR08'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AU                                                      
        ( CTABLEG2 , TYPSTAT , WTABLEG , CPTGL ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BUCPT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AV                                                      
        ( CTABLEG2 , MOIS , WTABLEG , CDJRN ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 7 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BUJRN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AW                                                      
        ( CTABLEG2 , CZONVENT , CSENVENT , DEFFET , WTABLEG , FLAGACT ,         
        CTYPREGL , VALCOMM ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 8 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR ( WTABLEG , 3 , 5         
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CALCO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AX                                                      
        ( CTABLEG2 , CODE_CAR , WTABLEG , LIBELLE , CVDCARAC , WCARACTS         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 ) , SUBSTR (             
        WTABLEG , 26 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CARSP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AY                                                      
        ( CTABLEG2 , CARACTSP , CVCARACT , WTABLEG , LVCARSP ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CARSV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01AZ                                                      
        ( CTABLEG2 , CODETAB , CODEGUI , WTABLEG , FLAG , NOEMET ,              
        REFREM , NOCOMPTE ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 6 ) , SUBSTR ( WTABLEG , 8 , 25 ) , SUBSTR ( WTABLEG , 33 ,         
        11 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CBANQ'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01B0                                                      
        ( CTABLEG2 , SOCENTRE , DEPOT , SOCLIEU , LIEU , NUMERO ,               
        WTABLEG , CSELART ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , SUBSTR ( CTABLEG2 , 10 , 3 )        
        , SUBSTR ( CTABLEG2 , 13 , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1         
        , 5 )                                                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CSELA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01B1                                                      
        ( CTABLEG2 , TYPE , CTRAN , TYPTRAN , WTABLEG , O_N_LIB ,               
        RAY_FLAG ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        1 ) , SUBSTR ( CTABLEG2 , 4 , 1 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 15 ) , SUBSTR ( WTABLEG , 16 , 45 )                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CTRAN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01B2                                                      
        ( CTABLEG2 , ECS , WTABLEG , CTR1 , CTR2 , CTR3 , SSC , SECT )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 ) , SUBSTR (             
        WTABLEG , 21 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CTREC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01B3                                                      
        ( CTABLEG2 , CUTIL , WTABLEG , LCUTIL , CPROG , WACTIVE ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 6 ) , SUBSTR (             
        WTABLEG , 27 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CUTIL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01B4                                                      
        ( CTABLEG2 , NSEQ , WTABLEG , CCLASSE , CCRITERE , CVALEUR ,            
        QPMINI , QPMAXI ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 , 10 ) , SUBSTR (              
        WTABLEG , 15 , 10 ) , SUBSTR ( WTABLEG , 25 , 8 ) , SUBSTR (            
        WTABLEG , 33 , 8 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CVEHI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01B5                                                      
        ( CTABLEG2 , PROG , WTABLEG , DATE1 , DATE2 , DATE3 , DATE4 ,           
        COMMENT ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 8 ) , SUBSTR (               
        WTABLEG , 17 , 8 ) , SUBSTR ( WTABLEG , 25 , 8 ) , SUBSTR (             
        WTABLEG , 33 , 20 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DATGL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01B6                                                      
        ( CTABLEG2 , NOMAUTO , WTABLEG , FLAG ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 15 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DCAUT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01B7                                                      
        ( CTABLEG2 , SOCIETE , MAGASIN , WTABLEG , COEF ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DCCOF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01B8                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , ENCMIN , FIXE , SCOREACC ,             
        MINACOMP ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 7 ) , SUBSTR ( WTABLEG , 8 , 3 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DCDIF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01B9                                                      
        ( CTABLEG2 , MINBORN , WTABLEG , MAXBORN , JPREMECH , NBMOIS ,          
        DDEBEFF , DFINEFF ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 2 ) , SUBSTR (               
        WTABLEG , 5 , 1 ) , SUBSTR ( WTABLEG , 6 , 4 ) , SUBSTR (               
        WTABLEG , 10 , 4 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DCECH'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BA                                                      
        ( CTABLEG2 , CFAMNCP , CDESMKG , CVDESMKG , WTABLEG , TYPCDSMK ,        
        CSFAMCCE ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE09'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BB                                                      
        ( CTABLEG2 , CFAMNCP , CDESMKG , CVDESMKG , WTABLEG , TYPCDSMK ,        
        CCRICCE , CVALCRI ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR ( WTABLEG , 8 ,         
        6 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE10'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BC                                                      
        ( CTABLEG2 , CDES , CVDES , WTABLEG , COPTCCE , CVOPTCCE ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG ,         
        7 , 6 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE11'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BD                                                      
        ( CTABLEG2 , CFAMCCE , WTABLEG , RSFAMCCE ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 18 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE12'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BE                                                      
        ( CTABLEG2 , CSFAMCCE , CCRICCE , CVCRICCE , WTABLEG , PRIXPSE )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , SUBSTR ( CTABLEG2 , 10 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 8 )                                                               
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCE13'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BF                                                      
        ( CTABLEG2 , CSELART , WTABLEG , CDEPOT , CTYPE ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 1 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CDEPO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BG                                                      
        ( CTABLEG2 , SOCMAG , WTABLEG , CODIF ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CDPRI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BH                                                      
        ( CTABLEG2 , NCOMPTE , WTABLEG , STATUT ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CEGLG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BI                                                      
        ( CTABLEG2 , CETAT , WTABLEG , LIBELLE , CTYPE , WFLAGMGI ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 54 ) , SUBSTR ( WTABLEG , 55 , 1 ) , SUBSTR (             
        WTABLEG , 56 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CETAT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BJ                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE , RANG , WACTIF ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 2 ) , SUBSTR (             
        WTABLEG , 23 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CEXPO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BK                                                      
        ( CTABLEG2 , TYPE , TYPELIEU , WTABLEG , WSOLIEU , COMMENT ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 7 ) , SUBSTR ( WTABLEG ,         
        8 , 20 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CHSDE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BL                                                      
        ( CTABLEG2 , CINSEE , WTABLEG , FLAG , COMMUNE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 32 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CILOT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BM                                                      
        ( CTABLEG2 , CCICS , WTABLEG , NSOC , LCICS , GCA ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 20 ) , SUBSTR (              
        WTABLEG , 24 , 2 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CISOC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BN                                                      
        ( CTABLEG2 , CODE , WTABLEG , JTJA , FLAG ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 24 ) , SUBSTR ( WTABLEG , 25 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CJTJA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BO                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , LMMJVSD ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 7 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CMUTD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BP                                                      
        ( CTABLEG2 , TRANCHE , WTABLEG , MONTANT ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'COLIS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BQ                                                      
        ( CTABLEG2 , NCONSO , NSOCIETE , NLIEU , WTABLEG , FLAG ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'COMCO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BR                                                      
        ( CTABLEG2 , CONSO , NSOC , NLIEU , WTABLEG , CAGR , NSEQ ,             
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , SUBSTR ( CTABLEG2 , 9 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 15 ) , SUBSTR ( WTABLEG , 16 , 4 ) , SUBSTR ( WTABLEG , 20 ,        
        1 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CONSL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BS                                                      
        ( CTABLEG2 , NCONSO , WTABLEG , LCONSO ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CONSO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BT                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , MODE1ER , MODE2EM , MODE3EM )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 ) , SUBSTR (               
        WTABLEG , 7 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'COPAI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BU                                                      
        ( CTABLEG2 , VALEUR , WTABLEG , LIBELLE , TYPES , TYPE , WGROUPE        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'COPAR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BV                                                      
        ( CTABLEG2 , CODESOC , MONTANT , WTABLEG , LETTRE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'COPRI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BW                                                      
        ( CTABLEG2 , CODESOC , MONTANT , WTABLEG , LETTRE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'COPR1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BX                                                      
        ( CTABLEG2 , CPARAM , CRAY , WTABLEG , WT , LIB_RAY , W , LLXTYP        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG ,         
        3 , 30 ) , SUBSTR ( WTABLEG , 33 , 1 ) , SUBSTR ( WTABLEG , 34 ,        
        20 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CRAY1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BY                                                      
        ( CTABLEG2 , CPARAM , CRAY , WTABLEG , WT , LIB_RAY , W , LLXTYP        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG ,         
        3 , 30 ) , SUBSTR ( WTABLEG , 33 , 1 ) , SUBSTR ( WTABLEG , 34 ,        
        20 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CRAY2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01BZ                                                      
        ( CTABLEG2 , SOCIETE , MAGASIN , WTABLEG , NUMCOM ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CREDE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01C0                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , ECTIVATI ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EXMAG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01C1                                                      
        ( CTABLEG2 , CTRAIT , WTABLEG , CETAT , CONSOL , CTABLE1 ,              
        CTABLE2 , CTABLE3 ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 5 ) , SUBSTR (             
        WTABLEG , 16 , 6 ) , SUBSTR ( WTABLEG , 22 , 6 ) , SUBSTR (             
        WTABLEG , 28 , 6 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EXTR1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01C2                                                      
        ( CTABLEG2 , CTRAIT , WTABLEG , CRUPT1 , CRUPT2 , CRUPT3 ,              
        CRUPT4 , CRUPT5 ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 ) , SUBSTR (               
        WTABLEG , 7 , 3 ) , SUBSTR ( WTABLEG , 10 , 3 ) , SUBSTR (              
        WTABLEG , 13 , 3 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EXTR2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01C3                                                      
        ( CTABLEG2 , CTRAIT , NCOL , WTABLEG , CDONNEES ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EXTR3'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01C4                                                      
        ( CTABLEG2 , ETAT , WTABLEG , FLAG ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EXT99'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01C5                                                      
        ( CTABLEG2 , CCAIRE , CCOTE , NPOSITIO , WTABLEG , QFONDEMP ,           
        NCODIC ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , SUBSTR ( CTABLEG2 , 5 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 3 ) , SUBSTR ( WTABLEG , 4 , 7 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FDFIX'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01C6                                                      
        ( CTABLEG2 , CFORMAT , WTABLEG , WACTIF , NOMETAT , LFORMAT ,           
        ZSWITCH1 ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR (               
        WTABLEG , 10 , 30 ) , SUBSTR ( WTABLEG , 40 , 24 )                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FORMA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01C7                                                      
        ( CTABLEG2 , CFORME , WTABLEG , LFORME ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FORME'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01C8                                                      
        ( CTABLEG2 , CFORMI , WTABLEG , LFORMI ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FORMI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01C9                                                      
        ( CTABLEG2 , CFORMP , WTABLEG , LFORMP ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FORMP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CA                                                      
        ( CTABLEG2 , CLEF , WTABLEG , POURMIN , POURMAX , ECHMIN ,              
        ECHMAX , NBPOINTS ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 ) , SUBSTR (               
        WTABLEG , 7 , 7 ) , SUBSTR ( WTABLEG , 14 , 7 ) , SUBSTR (              
        WTABLEG , 21 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DCEND'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CB                                                      
        ( CTABLEG2 , CHABIT , WTABLEG , LIBHABIT , FLAG ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DCHAB'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CC                                                      
        ( CTABLEG2 , DATEEFFE , WTABLEG , NOMBECH ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DCNEC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CD                                                      
        ( CTABLEG2 , CODPROF , WTABLEG , LIBPROF , FLAGEC , MPLAFOND ,          
        FLAGACT ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 35 ) , SUBSTR ( WTABLEG , 36 , 1 ) , SUBSTR (             
        WTABLEG , 37 , 7 ) , SUBSTR ( WTABLEG , 44 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DCPRO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CE                                                      
        ( CTABLEG2 , CODEREJ , WTABLEG , LIBREJ ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 25 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DCREJ'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CF                                                      
        ( CTABLEG2 , CODESIT , WTABLEG , LIBSIT , FLAG ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG , 16 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DCSIT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CG                                                      
        ( CTABLEG2 , SOCIETE , NOMPROG , WTABLEG , DELAI , LIBRE ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 15 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DELAI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CH                                                      
        ( CTABLEG2 , CMODDEL , WTABLEG , LMODDEL , QDECMIN , QDECMAX )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 4 ) , SUBSTR (             
        WTABLEG , 25 , 2 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DELIG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CI                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , DELAI ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DEPER'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CJ                                                      
        ( CTABLEG2 , CDEPART , WTABLEG ) AS                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DEPRG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CK                                                      
        ( CTABLEG2 , ZONEDEST , WTABLEG , LIBZONE ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DESTO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CL                                                      
        ( CTABLEG2 , LIEN , CODE , WTABLEG , LIBELLE ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DGLIB'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CM                                                      
        ( CTABLEG2 , CODSOC , WTABLEG , POURCENT ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ECPRA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CN                                                      
        ( CTABLEG2 , CZONELE , WTABLEG , LZONELE ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ELEMG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CO                                                      
        ( CTABLEG2 , CAIRE , CCOTE , NPOSITIO , WTABLEG ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , SUBSTR ( CTABLEG2 , 5 , 3 ) , WTABLEG                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EMPLC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CP                                                      
        ( CTABLEG2 , NSOC , NMAG , CMODDEL , WTABLEG , CPROAFF ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 5 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ENAFA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CQ                                                      
        ( CTABLEG2 , CPROAFF , WTABLEG , LPROAFF ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ENAFG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CR                                                      
        ( CTABLEG2 , NSOCGL , WTABLEG , NENTITE ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ENTST'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CS                                                      
        ( CTABLEG2 , CEQUIQ , WTABLEG , LEQUIP , WEQPOU , WMIXTE ,              
        WTYPEQUI , WSM ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 5 ) , SUBSTR (             
        WTABLEG , 28 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EQUIG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CU                                                      
        ( CTABLEG2 , CVERSION , WTABLEG , SFAM , STATUT , FLAG ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 4 ) , SUBSTR (               
        WTABLEG , 10 , 6 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ESTKM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CV                                                      
        ( CTABLEG2 , CETAT , NCONSO , WTABLEG , FLAG ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                            
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ETACO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CW                                                      
        ( CTABLEG2 , TYPLIGNE , WTABLEG , TYPEDIT ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ETATS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CX                                                      
        ( CTABLEG2 , CTITRE , WTABLEG , LTITRE , SDEST , LDEST , PERIODE        
        , CPARAM ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 3 ) , SUBSTR (             
        WTABLEG , 24 , 3 ) , SUBSTR ( WTABLEG , 27 , 1 ) , SUBSTR (             
        WTABLEG , 28 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ETMVT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CY                                                      
        ( CTABLEG2 , CTITRE , WTABLEG , ZONE1 , ZONE2 , ZONE3 , ZONE4 ,         
        ZONE5 ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG , 7 , 6 ) , SUBSTR (               
        WTABLEG , 13 , 6 ) , SUBSTR ( WTABLEG , 19 , 6 ) , SUBSTR (             
        WTABLEG , 25 , 6 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ETTRI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01CZ                                                      
        ( CTABLEG2 , SOC , MARQUE , FAMILLE , WTABLEG , REGLE , FLAG )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , SUBSTR ( CTABLEG2 , 9 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 4 ) , SUBSTR ( WTABLEG , 5 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EXFAC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01D0                                                      
        ( CTABLEG2 , CONDUITE , WTABLEG , LIBELLE ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVMC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01D1                                                      
        ( CTABLEG2 , CONDUITE , WTABLEG , LIBELLE ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVMD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01D2                                                      
        ( CTABLEG2 , RAYON , LIEU , WTABLEG , TYVARGLO , ETIQAUTO ,             
        ETATPRE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG ,         
        5 , 1 ) , SUBSTR ( WTABLEG , 6 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVPC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01D3                                                      
        ( CTABLEG2 , RAYON , LIEU , WTABLEG , TYPVARGO , ETIQAUTO ,             
        ETATPRE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG ,         
        5 , 1 ) , SUBSTR ( WTABLEG , 6 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVPD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01D4                                                      
        ( CTABLEG2 , RAYON , LIEU , WTABLEG , TYVARGLO , ETIQAUTO ,             
        ETATPRE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG ,         
        5 , 1 ) , SUBSTR ( WTABLEG , 6 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVPI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01D5                                                      
        ( CTABLEG2 , RAYON , LIEU , WTABLEG , TYVARGLO , ETIQAUTO ,             
        ETATPRE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG ,         
        5 , 1 ) , SUBSTR ( WTABLEG , 6 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVPS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01D6                                                      
        ( CTABLEG2 , STATUT , WTABLEG , FLAG , COEFF , LIBELLE ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR (               
        WTABLEG , 7 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVSC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01D7                                                      
        ( CTABLEG2 , TYPE , STATUT , WTABLEG , FLAG , COEFF , LIBELLE ,         
        CONTROLE ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 ) , SUBSTR ( WTABLEG , 7 , 20 ) , SUBSTR ( WTABLEG , 27 ,         
        8 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVST'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01D8                                                      
        ( CTABLEG2 , LIEU , SLIEU , LTRT , WTABLEG , FLAG ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ITLIE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01D9                                                      
        ( CTABLEG2 , NJRN , WTABLEG , NPIECE , LIBELLE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 20 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'JEPUR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DA                                                      
        ( CTABLEG2 , CFAM , WTABLEG ) AS                                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FRISQ'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DB                                                      
        ( CTABLEG2 , CGARAN , WTABLEG , LGARAN , NBMOIS ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 2 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GARAN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DC                                                      
        ( CTABLEG2 , PREFT1 , WTABLEG , PBF , TOTLI , TOTFAC , TOTLIT )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 4 ) , SUBSTR (               
        WTABLEG , 8 , 5 ) , SUBSTR ( WTABLEG , 13 , 5 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GCAEP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DD                                                      
        ( CTABLEG2 , PREFT , WTABLEG , LPREFT ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GCAP1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DE                                                      
        ( CTABLEG2 , PREFT , WTABLEG , LPREFT ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GCAP2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DF                                                      
        ( CTABLEG2 , WTYPE , WTABLEG , LTYPE ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 35 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GCASQ'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DG                                                      
        ( CTABLEG2 , CTYPE , WTABLEG , LTYPE , SENS , NATURE ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 1 ) , SUBSTR (             
        WTABLEG , 32 , 3 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GCATP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DH                                                      
        ( CTABLEG2 , CACID , WTABLEG , PREFT1 , PREFT2 ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GCAUT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DI                                                      
        ( CTABLEG2 , CGCPLT , WTABLEG , LGCPLT , CTVA , QDC , QDS ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 ) , SUBSTR (             
        WTABLEG , 26 , 3 ) , SUBSTR ( WTABLEG , 29 , 3 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GCPLT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DJ                                                      
        ( CTABLEG2 , NSEQ , WTABLEG , CALLEE1 , CALLEE2 , WACTIF ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 2 ) , SUBSTR (               
        WTABLEG , 5 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GD300'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DK                                                      
        ( CTABLEG2 , CALLEE , CNIVEAU , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GD305'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DL                                                      
        ( CTABLEG2 , CODGARAN , WTABLEG , LIBELLE , DUREE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 2 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GECDE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DM                                                      
        ( CTABLEG2 , FLAG , WTABLEG , NCDE ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 7 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GENCE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DN                                                      
        ( CTABLEG2 , CETAT , LIEU , WTABLEG , W ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                            
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GS400'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DO                                                      
        ( CTABLEG2 , COPER , WTABLEG , TOTALON ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GS401'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DP                                                      
        ( CTABLEG2 , DDD , WTABLEG , BBB , CCC , AAA , EEE , FFF ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 12 ) , SUBSTR (              
        WTABLEG , 16 , 10 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HERV '                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DQ                                                      
        ( CTABLEG2 , ACIDNAME , WTABLEG ) AS                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'IEANN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DR                                                      
        ( CTABLEG2 , NDEPOT , NSSLIEU , CLIEUTRT , WTABLEG , WREMONT )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'IE60I'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DS                                                      
        ( CTABLEG2 , NSSLIEU , CLIEUTRT , WTABLEG ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG                                                           
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'IE60X'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DT                                                      
        ( CTABLEG2 , NSOCIETE , LIEU , SLIEUTRT , WTABLEG , FLAG ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 2 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INDEP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DU                                                      
        ( CTABLEG2 , CPOSTAL , WTABLEG , CINSANC , CINSNOU , WCONTRA ,          
        ACTIF , CREATION ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 6 ) , SUBSTR ( WTABLEG , 17 , 1 ) , SUBSTR (             
        WTABLEG , 18 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INSEE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DV                                                      
        ( CTABLEG2 , INVDATE , WTABLEG , FLAGACT ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVDA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DW                                                      
        ( CTABLEG2 , SOCIETE , LIEU , WTABLEG , TICKET , SLIEU , LTRT ,         
        FLAGS ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 5 ) , SUBSTR ( WTABLEG , 10 , 5        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVLC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DX                                                      
        ( CTABLEG2 , SOCIETE , LIEU , WTABLEG , TICKET , SLIEU , TRT ,          
        FLAG ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 5 ) , SUBSTR ( WTABLEG , 10 , 1        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVLD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DY                                                      
        ( CTABLEG2 , SOCIETE , LIEU , WTABLEG , TICKET , SLIEU , LTRT ,         
        FLAG ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 5 ) , SUBSTR ( WTABLEG , 10 , 1        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVLI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01DZ                                                      
        ( CTABLEG2 , SOCIETE , LIEU , WTABLEG , TICKET , SLIEU , LTRT ,         
        FLAGS ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 5 ) , SUBSTR ( WTABLEG , 10 , 5        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVLS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01E0                                                      
        ( CTABLEG2 , MAGASINE , MAGASINR , WTABLEG , ACTIF ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MAGMA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01E1                                                      
        ( CTABLEG2 , NMAG , WTABLEG , V , J , G , E , LIBELLE ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 1 ) , SUBSTR (               
        WTABLEG , 5 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MAGPV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01E2                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG                                                           
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MAGSP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01E3                                                      
        ( CTABLEG2 , MAG , WTABLEG , TYPE , LIBELLE , WVE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 20 ) , SUBSTR (              
        WTABLEG , 24 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MAGTY'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01E4                                                      
        ( CTABLEG2 , LIEU , WTABLEG , FLAG , FLAG_EDT ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MAGVE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01E5                                                      
        ( CTABLEG2 , CMODDEL , WTABLEG , LMODDEL , WPLASSO , QDECMINI ,         
        QDECMAXI , WDONNEES ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 2 ) , SUBSTR ( WTABLEG , 24 , 3 ) , SUBSTR (             
        WTABLEG , 27 , 33 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MDLIV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01E6                                                      
        ( CTABLEG2 , SOCIETE , OPTION , WTABLEG , ETAT , FORMAT , TITRE         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 1 ) , SUBSTR ( WTABLEG , 10 , 47 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MEG36'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01E7                                                      
        ( CTABLEG2 , SOC , SEQ , WTABLEG , MGI , MAG ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 3 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGISM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01E8                                                      
        ( CTABLEG2 , CMODDEL , COULEUR , WTABLEG , LIB ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 15 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MODEC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01E9                                                      
        ( CTABLEG2 , CMODRG , WTABLEG , LMODRG , WFLAGS ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 10 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MODRG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EA                                                      
        ( CTABLEG2 , NJOUR , WTABLEG , QJOUR ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'JINVT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EB                                                      
        ( CTABLEG2 , NJRN , WTABLEG , CRITERES ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'JNTRI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ED                                                      
        ( CTABLEG2 , CETAT , CAGREG , WTABLEG , LAGREG , COMMENT ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 30 )                                                               
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LAGRE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EE                                                      
        ( CTABLEG2 , SOCIETE , LIEU , WTABLEG , ACTIF , CTYPLIEU , LLIEU        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 20 )                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LAUTR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EF                                                      
        ( CTABLEG2 , CTITRE , NSEQ , WTABLEG , ORIG , DEST ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 9 ) , SUBSTR ( WTABLEG ,         
        10 , 9 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LGMVT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EG                                                      
        ( CTABLEG2 , LIBELLE , WTABLEG , WACTIF , QTEMPS ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LGTPS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EH                                                      
        ( CTABLEG2 , ECS , WTABLEG , LIB1 , LIB2 , LIB3 ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 20 ) , SUBSTR (            
        WTABLEG , 41 , 20 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LIBCR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EI                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE , M_P , LIB ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LIENS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EJ                                                      
        ( CTABLEG2 , LIEN , DEGRE , WTABLEG , TOP , LIBELLE ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LIENT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EK                                                      
        ( CTABLEG2 , LIEUVA , NSEQ , WTABLEG , NLIEUNCG , PHYSIQUE , MGI        
        , LLIEUVAL ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 11 ) , SUBSTR ( WTABLEG ,        
        12 , 3 ) , SUBSTR ( WTABLEG , 15 , 3 ) , SUBSTR ( WTABLEG , 18 ,        
        20 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LIEUA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EL                                                      
        ( CTABLEG2 , LIEU , WTABLEG ) AS                                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LINV '                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EM                                                      
        ( CTABLEG2 , NLIEU , SSLIEU , LTRT , WTABLEG , ACTIF , COMMENT )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        4 ) , SUBSTR ( CTABLEG2 , 8 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LINVT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EN                                                      
        ( CTABLEG2 , CODSOC , CODLIEU , WTABLEG , TICOPT , SLIEU ,              
        FLAGAC ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LINV1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EO                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , NSLIEU , WTABLEG , WACTIVE ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LISLI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EP                                                      
        ( CTABLEG2 , CLITRT , WTABLEG , LLITRT , ACTIF , ENTMAG ,               
        ASSOCIAT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 3 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LITRT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EQ                                                      
        ( CTABLEG2 , CLIVR , WTABLEG , LLIVR ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LIVR '                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ER                                                      
        ( CTABLEG2 , CZONLIV , CZONELE , WTABLEG ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG                                                           
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LIVRD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ES                                                      
        ( CTABLEG2 , ZLIV , CELEM , PERIM , WTABLEG ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG                            
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LIVRE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ET                                                      
        ( CTABLEG2 , CZONLIV , WTABLEG , LZONLIV , LIEUGEST , LDEPLIV )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 6 ) , SUBSTR (             
        WTABLEG , 27 , 6 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LIVRG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EU                                                      
        ( CTABLEG2 , CPLAGE , WTABLEG , LPLAGE , HEURE , CPLOGI ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 4 ) , SUBSTR (               
        WTABLEG , 13 , 2 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LPLGE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EV                                                      
        ( CTABLEG2 , CSELART , CFAM , WTABLEG ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG                                                           
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LSELA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EW                                                      
        ( CTABLEG2 , NSOCMAG , NMAG , WTABLEG , WLIBRE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LSERV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EX                                                      
        ( CTABLEG2 , NSOCIETE , CTYPLIEU , LIEUINV , WTABLEG , CSSLIEU ,        
        EDITTO , ACTIV ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        1 ) , SUBSTR ( CTABLEG2 , 5 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 3 ) , SUBSTR ( WTABLEG , 4 , 1 ) , SUBSTR ( WTABLEG , 5 , 1         
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LSINV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EY                                                      
        ( CTABLEG2 , TYPE , LIEU , WTABLEG , ACTIF , COMMENTA ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MACHS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01EZ                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , CAUTOR ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MAGCL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01F0                                                      
        ( CTABLEG2 , DATEFFET , WTABLEG , NOMBECH ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PDNEC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01F1                                                      
        ( CTABLEG2 , CODPROF , WTABLEG , LIBPROF , FLAGEC , MPLAFOND ,          
        FLAGACT ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 35 ) , SUBSTR ( WTABLEG , 36 , 1 ) , SUBSTR (             
        WTABLEG , 37 , 7 ) , SUBSTR ( WTABLEG , 44 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PDPRO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01F2                                                      
        ( CTABLEG2 , CODEREJ , WTABLEG , LIBELREJ ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 25 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PDREJ'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01F3                                                      
        ( CTABLEG2 , CODE_SIT , WTABLEG , LIBSIT , FLAG ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG , 16 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PDSIT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01F4                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PERRC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01F5                                                      
        ( CTABLEG2 , ALLEE , WTABLEG ) AS                                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PICKS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01F6                                                      
        ( CTABLEG2 , RINVC , LIEUINV , WTABLEG , TYPCOMPT , VARQTE ,            
        COMPTGLB ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG ,         
        3 , 1 ) , SUBSTR ( WTABLEG , 4 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PINV1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01F7                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , QTEPLAFD ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PLAFD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01F8                                                      
        ( CTABLEG2 , NSOC , CDEPART , WTABLEG , WACTIF , LDEPT ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'POMAD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01F9                                                      
        ( CTABLEG2 , PRCT , WTABLEG , PRC1 ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRCTE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FA                                                      
        ( CTABLEG2 , CMODRGLT , WTABLEG , LMODRGLT , WMTVTE , C36SEQV ,         
        RENDU , IWERC ) AS                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 ) , SUBSTR (            
        WTABLEG , 31 , 4 ) , SUBSTR ( WTABLEG , 35 , 5 ) , SUBSTR (             
        WTABLEG , 40 , 9 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MOPAI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FB                                                      
        ( CTABLEG2 , CMODRGLT , WTABLEG , LMOD , WMTVTE , C36S , RENDU ,        
        IWERC ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 ) , SUBSTR (            
        WTABLEG , 31 , 4 ) , SUBSTR ( WTABLEG , 35 , 5 ) , SUBSTR (             
        WTABLEG , 40 , 9 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MOPA2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FC                                                      
        ( CTABLEG2 , CMOTIF , WTABLEG , LMOTIF , WACTIVE ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MOTHS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FD                                                      
        ( CTABLEG2 , CRETOUR , WTABLEG , LRETOUR ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MRETL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FE                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MSTCK'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FF                                                      
        ( CTABLEG2 , CTYPE , WTABLEG , LTYPE ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MUMGI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FG                                                      
        ( CTABLEG2 , SOCE , MAGE , SOCR , MAGR , WTABLEG , ACTIF ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , SUBSTR ( CTABLEG2 , 10 , 3 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MUTNO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FH                                                      
        ( CTABLEG2 , CPROG , CDEPOT , WTABLEG , WFLAG , CEDIT ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NCGDP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FI                                                      
        ( CTABLEG2 , CPROG , WTABLEG , WFLAG , QPVUNIT , COMMENT ,              
        WFLAG2 ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 15 ) , SUBSTR ( WTABLEG , 20 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NCGFC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FJ                                                      
        ( CTABLEG2 , NECS , WTABLEG , CJOURN1 , CJOURN2 , WSENSECS ,            
        LECS ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 2 ) , SUBSTR (               
        WTABLEG , 5 , 1 ) , SUBSTR ( WTABLEG , 6 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NOECS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FK                                                      
        ( CTABLEG2 , NUMECS , WTABLEG , JC , JP , JF , SENS , LECS ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 10 ) , SUBSTR (            
        WTABLEG , 21 , 10 ) , SUBSTR ( WTABLEG , 31 , 1 ) , SUBSTR (            
        WTABLEG , 32 , 25 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NUECS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FL                                                      
        ( CTABLEG2 , CCRED , WTABLEG , LIBCRED , DARTY , CODE36 , RG , W        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 2 ) , SUBSTR (             
        WTABLEG , 25 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'OCRED'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FM                                                      
        ( CTABLEG2 , COPER , WTABLEG , LOPER , WACTIVE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'OPER '                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FN                                                      
        ( CTABLEG2 , CORIG , WTABLEG , LORIG ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ORIGP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FO                                                      
        ( CTABLEG2 , CPROG , WTABLEG , QVOLUME , NBMAXETI ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 2 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PAMUT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FP                                                      
        ( CTABLEG2 , IDENTIF , NSEQ , WTABLEG , LIBELLE ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 60 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PARGD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FQ                                                      
        ( CTABLEG2 , IDENT , NSEQ , WTABLEG , LIBELLE ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 60 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PARGG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FR                                                      
        ( CTABLEG2 , CPARITE , WTABLEG , WACTIF , WASSOCIE ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PARIT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FS                                                      
        ( CTABLEG2 , COL1 , WTABLEG , POURC1 , POURC2 , LIBELLE , PERSO         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 , 4 ) , SUBSTR (               
        WTABLEG , 9 , 7 ) , SUBSTR ( WTABLEG , 16 , 1 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PCREM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FT                                                      
        ( CTABLEG2 , NOMAUTO , WTABLEG , FLAG ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 15 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PDAUT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FU                                                      
        ( CTABLEG2 , SOCIETE , MAGASIN , WTABLEG , COEF ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PDCOF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FV                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , ENCMIN , FIXE , SCOREACC ,             
        MINACOMP ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 7 ) , SUBSTR ( WTABLEG , 8 , 3 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PDDIF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FW                                                      
        ( CTABLEG2 , MINBORN , WTABLEG , MAXBORN , JPREMECH , NBMOIS ,          
        DDEBEFF , DFINEFF ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 2 ) , SUBSTR (               
        WTABLEG , 5 , 1 ) , SUBSTR ( WTABLEG , 6 , 4 ) , SUBSTR (               
        WTABLEG , 10 , 4 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PDECH'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FX                                                      
        ( CTABLEG2 , NCOMPTE , WTABLEG , PRINCIPE ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PDEGL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FY                                                      
        ( CTABLEG2 , CLEF , WTABLEG , POURMIN , POURMAX , ECHEMIN ,             
        ECHEMAX , NBPOINT ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 ) , SUBSTR (               
        WTABLEG , 7 , 7 ) , SUBSTR ( WTABLEG , 14 , 7 ) , SUBSTR (              
        WTABLEG , 21 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PDEND'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01FZ                                                      
        ( CTABLEG2 , CHABIT , WTABLEG , LIBHABIT , FLAG ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PDHAB'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01G0                                                      
        ( CTABLEG2 , CAT , COMPTE , WTABLEG , DEB , LG ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SITIS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01G1                                                      
        ( CTABLEG2 , CSSLIEU , CTYPLIEU , WTABLEG , LIBEL , WACT , WHS )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 1 ) , SUBSTR ( WTABLEG , 22 , 1 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLIEU'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01G2                                                      
        ( CTABLEG2 , NOM , PRENOM , WTABLEG , ENFANT ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SOURI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01G3                                                      
        ( CTABLEG2 , CLE , WTABLEG , QSTOCK ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 7 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SPLAF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01G4                                                      
        ( CTABLEG2 , CSRACK , WTABLEG , LSRACK ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SRACK'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01G5                                                      
        ( CTABLEG2 , CSTATUT , WTABLEG , LSTATUT , ACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'STATU'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01G6                                                      
        ( CTABLEG2 , CAUTO , WTABLEG , LAUTO ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TAUTO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01G7                                                      
        ( CTABLEG2 , WCODE , WTABLEG , WEXPRESS ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 50 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TCCAP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01G8                                                      
        ( CTABLEG2 , COMMENT , WTABLEG ) AS                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TCMFA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01G9                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE , DEFFET , CTAUXTVA ,             
        PGARANT , QGARANT ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 8 ) , SUBSTR (             
        WTABLEG , 29 , 5 ) , SUBSTR ( WTABLEG , 34 , 5 ) , SUBSTR (             
        WTABLEG , 39 , 2 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TCPLT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GA                                                      
        ( CTABLEG2 , CPRODEL , CZONLIV , CPLAGE , WTABLEG , WZONACT ,           
        QHLIM , QPS ) AS                                                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR ( WTABLEG , 8 ,         
        3 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRDED'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GB                                                      
        ( CTABLEG2 , CPRODEL , WTABLEG , LPRODEL ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRDEG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GC                                                      
        ( CTABLEG2 , CPRELE , WTABLEG , LIBPRELE ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRELE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GD                                                      
        ( CTABLEG2 , CODPRO , NTITRE , WTABLEG , NSEQ , LGMAX , POSDEB ,        
        FLAG ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG         
        , 4 , 2 ) , SUBSTR ( WTABLEG , 6 , 12 ) , SUBSTR ( WTABLEG , 18         
        , 1 )                                                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRODE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GE                                                      
        ( CTABLEG2 , CFORMAT , NSEQ , WTABLEG , DATAS , WFLAG ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 60 ) , SUBSTR ( WTABLEG ,        
        61 , 1 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROFE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GF                                                      
        ( CTABLEG2 , CODPRO , WTABLEG , FLAG , FAMCLT , SEPARTXT ,              
        DATEFFET , WCOUL ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 1 ) , SUBSTR ( WTABLEG , 33 , 8 ) , SUBSTR (             
        WTABLEG , 41 , 7 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROGE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GG                                                      
        ( CTABLEG2 , PROG , WTABLEG , PROBIBL ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 17 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRO36'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GH                                                      
        ( CTABLEG2 , TAUX , MEFFET , WTABLEG , COEF ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PVPSE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GI                                                      
        ( CTABLEG2 , RINVC , LIEU , WTABLEG , TYPE , VARIANTE , COMPTAGE        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG ,         
        3 , 1 ) , SUBSTR ( WTABLEG , 4 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'P1NVC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GJ                                                      
        ( CTABLEG2 , RINVC , LIEU , WTABLEG , NBRETIQ , PREREMPL ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'P2NVC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GK                                                      
        ( CTABLEG2 , CFAM , WTABLEG , CODDEC ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RABRD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GL                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , AFFICHE ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RECEP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GM                                                      
        ( CTABLEG2 , COPER , CACID , WTABLEG , TOP ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'REGUT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GN                                                      
        ( CTABLEG2 , STE , WTABLEG , MONTANT ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RELAN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GO                                                      
        ( CTABLEG2 , NMAG , WTABLEG , MONNAIE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RENDU'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GP                                                      
        ( CTABLEG2 , CPARAM , WTABLEG , CVALEUR ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RENTA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GQ                                                      
        ( CTABLEG2 , NSOCITE , LIEU , WTABLEG , FLAG ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RESPE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GR                                                      
        ( CTABLEG2 , SATCAS , CASE , WTABLEG ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        3 ) , WTABLEG                                                           
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SATCA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GS                                                      
        ( CTABLEG2 , SECTEUR , CZONE , WTABLEG , LZONE , NSOCIETE ,             
        POUBELLE ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 3 ) , SUBSTR ( WTABLEG , 24 , 1 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SAVIN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GT                                                      
        ( CTABLEG2 , RUBRIQUE , WTABLEG , CHAMP1 , CHAMP2 , CHAMP3 ,            
        COMMENT , POINTS ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 10 ) , SUBSTR (            
        WTABLEG , 21 , 10 ) , SUBSTR ( WTABLEG , 31 , 19 ) , SUBSTR (           
        WTABLEG , 50 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SCORE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GU                                                      
        ( CTABLEG2 , NDEPOT , CSELART , WTABLEG ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG                                                           
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SELMU'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GV                                                      
        ( CTABLEG2 , SOCIETE , DEPOT , WTABLEG , WSEQ ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SEQIV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GW                                                      
        ( CTABLEG2 , SOC , WTABLEG , SEUIL ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SEUIL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GX                                                      
        ( CTABLEG2 , CATAUX , CPTSIGA , WTABLEG , CPTGCV ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 6 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SICPT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GY                                                      
        ( CTABLEG2 , CATSIGA , WTABLEG , CATGCV , TIERS , LETTRAGE ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 15 ) , SUBSTR (              
        WTABLEG , 19 , 15 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SIGAT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01GZ                                                      
        ( CTABLEG2 , CATSIGA , TIERSIGA , WTABLEG , TIERSGCV ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 6 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SITIE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01H0                                                      
        ( CTABLEG2 , C , WTABLEG , L ) AS                                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TYPCO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01H1                                                      
        ( CTABLEG2 , TYPMAT , WTABLEG , LTYPMAT ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TYPMA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01H2                                                      
        ( CTABLEG2 , TYPE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TYPSO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01H3                                                      
        ( CTABLEG2 , SOCLIE , WTABLEG , LIBELLIE , TYPELIEU ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VALLI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01H4                                                      
        ( CTABLEG2 , SOCLIE , NSEQ , WTABLEG , NSOCLI , NSSLIEU ,               
        CLIEUTRT , DEFAUT ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG ,         
        7 , 3 ) , SUBSTR ( WTABLEG , 10 , 5 ) , SUBSTR ( WTABLEG , 15 ,         
        1 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VANLI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01H5                                                      
        ( CTABLEG2 , CVESPE , WTABLEG , LVESPE ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VESPE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01H6                                                      
        ( CTABLEG2 , TYPE , WTABLEG , LIBEL ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VTESP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01H7                                                      
        ( CTABLEG2 , CPREST , WTABLEG , LPREST , PMONT , DEFFET ,               
        CTAUXTVA , WP01 ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 ) , SUBSTR (             
        WTABLEG , 26 , 8 ) , SUBSTR ( WTABLEG , 34 , 5 ) , SUBSTR (             
        WTABLEG , 39 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VTPRE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01H8                                                      
        ( CTABLEG2 , CREMVTE , WTABLEG , LREMVTE , DEFFET , WTYPCOND ,          
        WREMPERS , QPOURCEN ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 8 ) , SUBSTR (             
        WTABLEG , 29 , 2 ) , SUBSTR ( WTABLEG , 31 , 1 ) , SUBSTR (             
        WTABLEG , 32 , 3 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VTREM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01H9                                                      
        ( CTABLEG2 , ZONE , WTABLEG , COULEUR , LIB ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 15 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ZONEC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HA                                                      
        ( CTABLEG2 , COEF , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TDURE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HB                                                      
        ( CTABLEG2 , CEQUIP , WTABLEG , LEQUIP , CAGR1 , CAGR2 , WEDIT )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 2 ) , SUBSTR (             
        WTABLEG , 23 , 2 ) , SUBSTR ( WTABLEG , 25 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TEQUI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HC                                                      
        ( CTABLEG2 , NOTIERS , WTABLEG , CATTIERS ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 9 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TIERS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HD                                                      
        ( CTABLEG2 , DATEDEM , SOCIETE , TYPEMAG , WTABLEG , DEMPRE ,           
        EDIT , DATEINV ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        3 ) , SUBSTR ( CTABLEG2 , 12 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 ,         
        8 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TINV1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HE                                                      
        ( CTABLEG2 , NTITRE , WTABLEG , TYPE , LIBELLE , CHAMP4 ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 35 ) , SUBSTR (              
        WTABLEG , 37 , 6 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TITRE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HF                                                      
        ( CTABLEG2 , CTYPLIEU , WTABLEG , LIBELLE ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TLIEU'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HG                                                      
        ( CTABLEG2 , CPLAN , CPROFIL , WTABLEG , WTOUR , WRECUP ,               
        WCOMMUT , LADRTOUR ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 18        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TLIPP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HH                                                      
        ( CTABLEG2 , CTYPE , WTABLEG , WDARTY ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TLIVR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HI                                                      
        ( CTABLEG2 , CLOGI , WTABLEG , WACTIF , CPROTOUR ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TLOGI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HJ                                                      
        ( CTABLEG2 , TNOM , WTABLEG , LNOM , CAPPEL ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TNOM '                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HK                                                      
        ( CTABLEG2 , CPLAN , WTABLEG , LPLAN , A ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TPLAN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HL                                                      
        ( CTABLEG2 , CPLAN , CPROFIL , WTABLEG , WTOUR , WRECUP ,               
        LADRTOUR , WCOMMUT ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 18 ) , SUBSTR ( WTABLEG , 21 ,         
        1 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TPLPR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HM                                                      
        ( CTABLEG2 , NSOCIETE , TYPPRET , WTABLEG , LTYPPRET ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TPRET'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HN                                                      
        ( CTABLEG2 , CPROF , CMODDEL , CPLAGE , CEQUIP , WTABLEG ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , SUBSTR ( CTABLEG2 , 9 , 2 ) , SUBSTR ( CTABLEG2 , 11 , 5 )        
        , WTABLEG                                                               
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TPROD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HO                                                      
        ( CTABLEG2 , PROFIL , WTABLEG , LIBELLE , FLAG , WECART , IMPRIM        
        , SAT ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 19 ) , SUBSTR (            
        WTABLEG , 42 , 2 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TPROG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HP                                                      
        ( CTABLEG2 , CFAM , WTABLEG , TPSCFAM ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TPSFA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HQ                                                      
        ( CTABLEG2 , CTRACK , WTABLEG , LTRACK , QCAPCONT , QEQPAL ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 3 ) , SUBSTR (             
        WTABLEG , 24 , 3 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TRACK'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HR                                                      
        ( CTABLEG2 , CTRANS , WTABLEG , LTRANS ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TRANS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HS                                                      
        ( CTABLEG2 , CRETOUR , WTABLEG , LRETOUR , WRETOUR , WINPUTAB ,         
        AGREGAT ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 10 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TRETO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HT                                                      
        ( CTABLEG2 , CTYPE , WTABLEG ) AS                                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TTYPA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HU                                                      
        ( CTABLEG2 , TVOIE , WTABLEG , LVOIE ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TVOIE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HV                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , POURCENT , JOUR ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 3 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TXESC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HW                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , POURCENT , NBJOUR ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 3 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TXFIN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HX                                                      
        ( CTABLEG2 , TAUX , WTABLEG , LIBELLE , TVA , DATE , ANCTVA , CW        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 3 ) , SUBSTR (             
        WTABLEG , 24 , 8 ) , SUBSTR ( WTABLEG , 32 , 3 ) , SUBSTR (             
        WTABLEG , 35 , 2 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TXTVA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HY                                                      
        ( CTABLEG2 , TYPE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TYCON'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01HZ                                                      
        ( CTABLEG2 , CTYPDCL , WTABLEG , LTYPDCL ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TYDCL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01I6                                                      
        ( CTABLEG2 , COPTION , WTABLEG , WACTIF , LOPTION ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BOR05'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01I7                                                      
        ( CTABLEG2 , CVOPT , WTABLEG , WACTIF , LVOPT ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BOR06'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01I8                                                      
        ( CTABLEG2 , CDESCR , CVDESCR , WTABLEG , WACTIF , COPTION ,            
        CVOPTION ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 ) , SUBSTR ( WTABLEG , 7 , 5 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BOR09'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01I9                                                      
        ( CTABLEG2 , FAMILLE , WTABLEG , VALEUR ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CFACT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01IA                                                      
        ( CTABLEG2 , SOCIETE , ZONE , WTABLEG , LIBELLE , WTYP , WMGI ,         
        EDITDACE ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 1 ) , SUBSTR ( WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 ,        
        2 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ZPRIX'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01IB                                                      
        ( CTABLEG2 , SOC , DEPOT , ZONE , SECTEUR , WTABLEG , SSLIEU ,          
        LIBELLE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 2 ) , SUBSTR ( CTABLEG2 , 9 , 2 )         
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 ,         
        20 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ZVBID'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01IC                                                      
        ( CTABLEG2 , CETAT , NSEQ , WTABLEG , CTYPLIEU , NSOCLIEU ,             
        CCONSO , WACTIF ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 6 ) , SUBSTR ( WTABLEG , 8 , 6 ) , SUBSTR ( WTABLEG , 14 , 1        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VAGLI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ID                                                      
        ( CTABLEG2 , CETAT , CCONSO , NSEQ , WTABLEG , NSOCLIEU , WACTIF        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , SUBSTR ( CTABLEG2 , 13 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 6 ) , SUBSTR ( WTABLEG , 7 , 1 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VAELI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01IE                                                      
        ( CTABLEG2 , CETAT , NSEQEDIT , WTABLEG , CRAYON , TYPTRI ,             
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 2 ) , SUBSTR ( WTABLEG , 8 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VARAY'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01IF                                                      
        ( CTABLEG2 , CETAT , CCONSO , WTABLEG , LIBELLE ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VALCO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01IG                                                      
        ( CTABLEG2 , CETAT , DEB_FIN , WTABLEG , NBLIGNES , COMMANDE )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 56 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EGIMP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01J0                                                      
        ( CTABLEG2 , TYPTRAN , CAPRET , WTABLEG , CTRAN , TYPAP ,               
        LIBTRAN , LIBCTRAN ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 30 ) , SUBSTR ( WTABLEG , 33 ,         
        25 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CTINT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01J2                                                      
        ( CTABLEG2 , NENTCDE , WTABLEG , FLAG ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GGENT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01J3                                                      
        ( CTABLEG2 , NSOCIETE , CBANQ , WTABLEG , LBANQUE , FLAG ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 1 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RABQE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01J4                                                      
        ( CTABLEG2 , NSOCIETE , CLETTRE , WTABLEG , WACTIF , CTYPREGL ,         
        LIBELLE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 20 )                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RALET'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01J5                                                      
        ( CTABLEG2 , NSOCIETE , CMOTIFAV , WTABLEG , LMOTIFAV , CBC ,           
        NUECS , CRITECS ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 1 ) , SUBSTR ( WTABLEG , 22 , 5 ) , SUBSTR ( WTABLEG , 27 ,        
        2 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RAMOT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01J6                                                      
        ( CTABLEG2 , NSOCIETE , CTYPREG , WTABLEG , LCTYPREG ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RAREG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01J7                                                      
        ( CTABLEG2 , CETAT , WDARDAC , WTABLEG , LIBETAT , EDIT ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 40 ) , SUBSTR ( WTABLEG ,        
        41 , 1 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FETAT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01J8                                                      
        ( CTABLEG2 , CETAT , NUMCOL , NSEQ , WTABLEG , MODP , LIBMODP ,         
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 9 ) , SUBSTR ( CTABLEG2 , 10         
        , 2 ) , SUBSTR ( CTABLEG2 , 12 , 2 ) , WTABLEG , SUBSTR (               
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'COLMP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01J9                                                      
        ( CTABLEG2 , CMTMAXI , WTABLEG , PMTMAXI ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BAMAX'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JA                                                      
        ( CTABLEG2 , CFAM , WTABLEG , POURCENT , DEFFET ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 8 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CFRGE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JB                                                      
        ( CTABLEG2 , CPTCG , WTABLEG , LIBCPTCG ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CGEN '                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JC                                                      
        ( CTABLEG2 , DELAI , WTABLEG ) AS                                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CREDI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JD                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , WDOMIC ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 60 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DOMIC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JE                                                      
        ( CTABLEG2 , NSEQ , WTABLEG , CFAM ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FRISK'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JF                                                      
        ( CTABLEG2 , RUBRIQUE , WTABLEG , LRUBRIQ ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LDSTK'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JG                                                      
        ( CTABLEG2 , NLETTRE , NVAR , WTABLEG , LGVAR , DESCRIPT ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        4 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG ,         
        3 , 50 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LEDIC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JH                                                      
        ( CTABLEG2 , NOM_LETT , WTABLEG , LIBELLE , LIG ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 2 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LETTR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JI                                                      
        ( CTABLEG2 , COMPTE , WTABLEG , NCOMPTE , NSSCPTE , SECTION ,           
        RUBRIQUE , STEAPP ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 1 ) , SUBSTR (               
        WTABLEG , 5 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PCEGL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JJ                                                      
        ( CTABLEG2 , RINVC , LIEUINV , WTABLEG , EDITETIQ , EDITETAT )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PINV2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JK                                                      
        ( CTABLEG2 , NOMIMP , WTABLEG ) AS                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'POOLI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JL                                                      
        ( CTABLEG2 , CPROF , WTABLEG , LPROF ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROF '                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JM                                                      
        ( CTABLEG2 , NSEQ , WTABLEG , PVTMINC , PVTMINHC ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PVTMM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JN                                                      
        ( CTABLEG2 , CICS , WTABLEG , SOC ) AS                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SESSI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JO                                                      
        ( CTABLEG2 , A1 , WTABLEG , A2 , A3 , A4 , A5 ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 5 ) , SUBSTR (               
        WTABLEG , 14 , 9 ) , SUBSTR ( WTABLEG , 23 , 2 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TCPSE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JP                                                      
        ( CTABLEG2 , WTYPE , WTABLEG , TPSSUPPL ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TPSCO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JQ                                                      
        ( CTABLEG2 , TFIXE , TVARIABL , WTABLEG ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG                                                           
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TPSGE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JR                                                      
        ( CTABLEG2 , WCODE , WTABLEG , TPSSUPPL ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TPSSU'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JS                                                      
        ( CTABLEG2 , CODE , TYPE , WTABLEG , LIBELLE , RAYON ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG ,        
        31 , 30 )                                                               
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TRA36'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JT                                                      
        ( CTABLEG2 , JOURNAL , WTABLEG , PPAC , PCAC , PFAC , PFAF ,            
        ACTIF ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 , 4 ) , SUBSTR (               
        WTABLEG , 9 , 4 ) , SUBSTR ( WTABLEG , 13 , 4 ) , SUBSTR (              
        WTABLEG , 17 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TYPJN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JU                                                      
        ( CTABLEG2 , CLE , WTABLEG , D2222 ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VRESP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JV                                                      
        ( CTABLEG2 , NOMAG , WDARDAC , MODP , WTABLEG , CBANQ , LIBANQ )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 3 ) , SUBSTR ( WTABLEG , 4 , 25 )                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BQMAG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JW                                                      
        ( CTABLEG2 , CODETAT , WDARDAC , NSEQ , WTABLEG , MODP , LIBMODP        
        , WACTIF ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , SUBSTR ( CTABLEG2 , 10 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR ( WTABLEG , 22         
        , 1 )                                                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PMODP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01JX                                                      
        ( CTABLEG2 , TYPV , WTABLEG , LIBTYPV , W_ACTIF ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TYPVE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01K0                                                      
        ( CTABLEG2 , CFAM , CPANNE , WTABLEG , WACTIF ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HEPAF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01K1                                                      
        ( CTABLEG2 , CREFUS , WTABLEG , WACTIF , LREFUS , WCHCODIC ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 35 ) , SUBSTR (              
        WTABLEG , 37 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HEREF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01K4                                                      
        ( CTABLEG2 , CTAUX , WTABLEG , QTAUX ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DSTAU'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01K5                                                      
        ( CTABLEG2 , CPROG , NSEQ , WTABLEG , WACTIF , WCLEDATA ,               
        ENVORIG , ENVDEST ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 16 ) , SUBSTR ( WTABLEG , 18 , 14 ) , SUBSTR ( WTABLEG , 32         
        , 14 )                                                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HEGSB'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01K6                                                      
        ( CTABLEG2 , NSOCVALO , NLIEUVAL , WTABLEG , WACTIF ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DSLIE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01K9                                                      
        ( CTABLEG2 , NECART , WTABLEG , WACTIF , LECART , WMODRGLT ,            
        WANCBA , WECS ) AS                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 ) , SUBSTR (             
        WTABLEG , 24 , 2 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BAERR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KA                                                      
        ( CTABLEG2 , NSIGNAT , WTABLEG , WACTIF , LSIGNAT ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 15 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BASIG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KB                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , WACTIF , CGRP , NSEQ ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 2 ) , SUBSTR ( WTABLEG , 4 , 2 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VTSTK'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KC                                                      
        ( CTABLEG2 , CGARANTI , WTABLEG , WACTIF , LGARANTI , CMOTIF ,          
        WQTEMUL , WNHSMUL ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 10 ) , SUBSTR ( WTABLEG , 32 , 1 ) , SUBSTR (            
        WTABLEG , 33 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HEGAR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KF                                                      
        ( CTABLEG2 , CTYPTRAN , WTABLEG , WACTIF , LTYPTRAN ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HETRA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KH                                                      
        ( CTABLEG2 , NLIEUHED , WTABLEG , WACTIF , LLIEUHED ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HELID'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KI                                                      
        ( CTABLEG2 , CLIEUHET , WTABLEG , WACTIF , LLIEUHET , WSAISIES ,        
        CLIEUNAT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 5 ) , SUBSTR ( WTABLEG , 27 , 8 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HELIT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KJ                                                      
        ( CTABLEG2 , CLIEUHEI , WTABLEG , WACTIF , LLIEUHEI ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HELII'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KK                                                      
        ( CTABLEG2 , NSOC , NLIEU , CTRAIT , WTABLEG , WACTIF , CTYPOPT         
        , LIBELLE ) AS                                                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 , 20        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFINT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KL                                                      
        ( CTABLEG2 , CTYPOPT , NOPTION , WTABLEG , WACTIF , COPTION ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6         
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 2 )                                                               
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFOPT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KM                                                      
        ( CTABLEG2 , COPTION , WTABLEG , WACTIF , LOPTION ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFCOP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KN                                                      
        ( CTABLEG2 , CTRAIT , WTABLEG , WACTIF , LTRAIT , TIERSHE ,             
        TOLERANC , FLAGS ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 16 ) , SUBSTR ( WTABLEG , 38 , 3 ) , SUBSTR (            
        WTABLEG , 41 , 3 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFTRT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KO                                                      
        ( CTABLEG2 , CACID , CLIEUHET , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HEAUT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KP                                                      
        ( CTABLEG2 , CRENDU , WTABLEG , WACTIF , LRENDU , WRENDU ,              
        CTYPRDU , TYPREP ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 ) , SUBSTR (             
        WTABLEG , 24 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFRDU'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KR                                                      
        ( CTABLEG2 , NTYOPE , WTABLEG , WACTIF , LTYOPE ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HETYO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KT                                                      
        ( CTABLEG2 , CREC , WTABLEG , LREC , WACTIF ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GRREC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KU                                                      
        ( CTABLEG2 , NSOCDEPO , NDEPOT , CQUOTA , WTABLEG , LQUOTA ,            
        WACTIF , DATA ) AS                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR ( WTABLEG , 22 ,        
        30 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GRQUO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KV                                                      
        ( CTABLEG2 , NSEQ , WTABLEG , CQUOTA , CREC , WACTIF ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GRQOR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KW                                                      
        ( CTABLEG2 , CTRAIT , CRENDU , WTABLEG , WACTIF , CMOTIF ,              
        DESTRET , FLAGIR ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 10 ) , SUBSTR ( WTABLEG , 12 , 14 ) , SUBSTR ( WTABLEG , 26         
        , 1 )                                                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFRDF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KX                                                      
        ( CTABLEG2 , CPROG , NSEQ , WTABLEG , JOUR , WACTIF ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG ,         
        3 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DTECH'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KY                                                      
        ( CTABLEG2 , CTRAIT , CRENDU , WTABLEG , WACTIF , ENVORIG ,             
        ENVDEST ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 14 ) , SUBSTR ( WTABLEG , 16 , 14 )                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFENV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01KZ                                                      
        ( CTABLEG2 , CTRAIT , WTABLEG , WACTIF , NTEL , NFAX , NBCOP )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 11 ) , SUBSTR (              
        WTABLEG , 13 , 11 ) , SUBSTR ( WTABLEG , 24 , 1 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFBEF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01L0                                                      
        ( CTABLEG2 , CTYPE , WTABLEG , WACTIF , LTYPE , WAVOIR , SENSDC         
        , FLAGS ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 25 ) , SUBSTR (              
        WTABLEG , 27 , 1 ) , SUBSTR ( WTABLEG , 28 , 1 ) , SUBSTR (             
        WTABLEG , 29 , 10 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTYP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01L1                                                      
        ( CTABLEG2 , NATURE , WTABLEG , WACTIF , LIBELLE , VENT ,               
        WAUTEMET , WAUTREC ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 1 ) , SUBSTR ( WTABLEG , 33 , 1 ) , SUBSTR (             
        WTABLEG , 34 , 10 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGNAT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01L2                                                      
        ( CTABLEG2 , NATURE , WTABLEG , WACTIF , ALIM , FREQ , ECHEANCE         
        , JOUR ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 2 ) , SUBSTR ( WTABLEG , 5 , 1 ) , SUBSTR (               
        WTABLEG , 6 , 2 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGFRQ'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01L3                                                      
        ( CTABLEG2 , NATURE , TYPDOC , TYPEER , NOECS , WTABLEG , WACTIF        
        , WGL ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , SUBSTR ( CTABLEG2 , 8 , 1 ) , SUBSTR ( CTABLEG2 , 9 , 5 )         
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        1 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGFAC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01L6                                                      
        ( CTABLEG2 , NSOCGL , NAUX , WTABLEG , NETAB , WACTIF ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BAAUX'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01L8                                                      
        ( CTABLEG2 , CTRAIT , WTABLEG , WACTIF , LTRAIT , CMOTIF ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 10 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HETRT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01L9                                                      
        ( CTABLEG2 , CTYPEHS , WTABLEG , WACTIF , LTYPEHS ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HETHS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LA                                                      
        ( CTABLEG2 , CFAMMGI , CMARKETI , WTABLEG , WACTIF , NSEQ ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 2 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGCDK'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LB                                                      
        ( CTABLEG2 , CRAYMGI , CRAYMGIP , WTABLEG , WACTIF , CTRAYMGI )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        4 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGRY2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LC                                                      
        ( CTABLEG2 , CLE , WTABLEG , COEFS ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'COEFS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LD                                                      
        ( CTABLEG2 , CFAMMGI , CDESCRIP , WTABLEG , WACTIF , NSEQ ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 2 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGCDD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LE                                                      
        ( CTABLEG2 , CCQCMGI , WTABLEG , WACTIF , LCQCMGI ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGCQC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LF                                                      
        ( CTABLEG2 , CDECMGI , WTABLEG , WACTIF , LDECMGI ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 32 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGDEC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LG                                                      
        ( CTABLEG2 , CETQMGI , WTABLEG , WACTIF , LETQMGI ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGETQ'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LH                                                      
        ( CTABLEG2 , CFAM , NSEQ , WTABLEG , WACTIF , CMARKET , CVMARKET        
        , CFAMMGI ) AS                                                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 ) , SUBSTR ( WTABLEG , 7 , 5 ) , SUBSTR ( WTABLEG , 12 , 4        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGFAM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LI                                                      
        ( CTABLEG2 , CFSMMGI , WTABLEG , WACTIF , LFSMMGI , CFAMMGI ,           
        CFAMGMGI ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 21 ) , SUBSTR (              
        WTABLEG , 23 , 4 ) , SUBSTR ( WTABLEG , 27 , 4 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGFSM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LJ                                                      
        ( CTABLEG2 , CGARMGI , WTABLEG , WACTIF , QGARMGI , LGARMGI ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR (               
        WTABLEG , 4 , 32 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGGAR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LK                                                      
        ( CTABLEG2 , CPRVMGI , WTABLEG , WACTIF , LPRVMGI ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 32 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGPRV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LL                                                      
        ( CTABLEG2 , CPSEMGI , WTABLEG , WACTIF , PPSEMGI , LPSEMGI ,           
        CTVAMGI , WDATAPSE ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR (               
        WTABLEG , 8 , 32 ) , SUBSTR ( WTABLEG , 40 , 1 ) , SUBSTR (             
        WTABLEG , 41 , 10 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGPSE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LM                                                      
        ( CTABLEG2 , CRUBMGI , WTABLEG , WACTIF , LRUBMGI ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGRUB'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LN                                                      
        ( CTABLEG2 , CRAYMGI , WTABLEG , WACTIF , LRAYMGI , CNIVMGI ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 2 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGRY1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LO                                                      
        ( CTABLEG2 , CTABLEG1 , WTABLEG , WACTIF , NSFICMGI , WFILIALE )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGTRF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LP                                                      
        ( CTABLEG2 , CTVAMGI , WTABLEG , WACTIF , PTVA1MGI , DEFFET ,           
        PTVA2MGI ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 4 ) , SUBSTR (               
        WTABLEG , 6 , 6 ) , SUBSTR ( WTABLEG , 12 , 4 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGTVA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LV                                                      
        ( CTABLEG2 , NATURE , CTYPE , WTABLEG , WACTIF ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGINT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LW                                                      
        ( CTABLEG2 , NATURE , CVENT , WTABLEG , WACTIF , LVENT , TYPEDC         
        , PRIMAIRE ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 30 ) , SUBSTR ( WTABLEG , 32 , 1 ) , SUBSTR ( WTABLEG , 33 ,        
        1 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGVEN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01LX                                                      
        ( CTABLEG2 , COMPTE , WTABLEG , WACTIF , LIBELLE , TYPE , NAUX ,        
        TYPCPTE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 1 ) , SUBSTR ( WTABLEG , 33 , 1 ) , SUBSTR (             
        WTABLEG , 34 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGPCG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01M0                                                      
        ( CTABLEG2 , CTYPCPT , WTABLEG , WACTIF , LTYPCPT ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 50 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TYCPT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01M2                                                      
        ( CTABLEG2 , CODETAT , WDARDAC , WTABLEG , CAPRET , TYPAP ,             
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 2 ) , SUBSTR ( WTABLEG , 4 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'APREM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01M3                                                      
        ( CTABLEG2 , NCSAV , WTABLEG , WACTIF , CTRAIT ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFSAV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01M4                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , WACTIF , CPRIX , CDATE ,          
        COEFF ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 4         
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFMTP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01M5                                                      
        ( CTABLEG2 , CTRAIT , CGARANTI , WTABLEG , WACTIF ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFGAR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01M6                                                      
        ( CTABLEG2 , CODETAT , WDARDAC , NSEQ , WTABLEG , CAPRET , TYPAP        
        , WACTIF ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , SUBSTR ( CTABLEG2 , 10 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR ( WTABLEG , 4 ,         
        1 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TYPAP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01M9                                                      
        ( CTABLEG2 , CPROG , NSEQ , WTABLEG , WACTIF , FLAGAVAR , TYOPAV        
        , TYOPAR ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 8 ) , SUBSTR ( WTABLEG , 10 , 6 ) , SUBSTR ( WTABLEG , 16 ,         
        6 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HEGOP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MC                                                      
        ( CTABLEG2 , NSOCC , WTABLEG , WACTIF , NSOCFISC , WFETAB ,             
        WALIM , LIBELLE ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 1 ) , SUBSTR ( WTABLEG , 6 , 1 ) , SUBSTR (               
        WTABLEG , 7 , 25 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGSOC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MF                                                      
        ( CTABLEG2 , NSOCC , NSOCDEP , WTABLEG , WACTIF ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGSGP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MG                                                      
        ( CTABLEG2 , NPOLPR , WTABLEG , WACTIF , NPOLPS , ABC , ORD ,           
        LAR ) AS                                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 4 ) , SUBSTR ( WTABLEG , 36 , 4 ) , SUBSTR (             
        WTABLEG , 40 , 4 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'POLPS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MI                                                      
        ( CTABLEG2 , CACID , WTABLEG , WACTIF , NSOCLIEU , WGROUPE ,            
        SERVICE , AUTOR ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR (               
        WTABLEG , 8 , 1 ) , SUBSTR ( WTABLEG , 9 , 5 ) , SUBSTR (               
        WTABLEG , 14 , 3 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGADR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ML                                                      
        ( CTABLEG2 , NSOC , CETAB , WTABLEG , WACTIF , LIBELLE , WFACT ,        
        WETBPRIN ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 ) , SUBSTR ( WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 ,        
        1 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGETB'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MQ                                                      
        ( CTABLEG2 , CTYPMT , WTABLEG , WACTIF , LIB , TYPLIG ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 3 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTMT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MR                                                      
        ( CTABLEG2 , NDEPOT , WTABLEG , WQJOUR , WGENGRO , GEMPLACT ,           
        WRESFOUR ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 1 ) , SUBSTR ( WTABLEG , 6 , 1 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GADEP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MS                                                      
        ( CTABLEG2 , CTYPM , WTABLEG , LTYPM ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TYPMG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MU                                                      
        ( CTABLEG2 , TYPTRAN , CAPRET , WTABLEG , CTRAN , TYPAP ,               
        LIBTRAN , LIBCTRAN ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 30 ) , SUBSTR ( WTABLEG , 33 ,         
        20 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CTIN2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MV                                                      
        ( CTABLEG2 , CMODRGLT , WTABLEG , LMODRGLT , WMTVTE , C36SEQV ,         
        RENDU , IWERC ) AS                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 ) , SUBSTR (            
        WTABLEG , 31 , 4 ) , SUBSTR ( WTABLEG , 35 , 5 ) , SUBSTR (             
        WTABLEG , 40 , 6 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MOPA3'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MW                                                      
        ( CTABLEG2 , CMODRGLT , WTABLEG , LMOD , WMTVTE , C36S , RENDU ,        
        IWERC ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 ) , SUBSTR (            
        WTABLEG , 31 , 4 ) , SUBSTR ( WTABLEG , 35 , 5 ) , SUBSTR (             
        WTABLEG , 40 , 6 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MOPA4'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MX                                                      
        ( CTABLEG2 , ORGA , SOCIETE , WTABLEG , ACTIF , APPC ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ORFIN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01MY                                                      
        ( CTABLEG2 , LIEU , WTABLEG , PWD06 , PWD07 , PWD08 , PWD09 ,           
        PWD11 ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 ) , SUBSTR (             
        WTABLEG , 21 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PWINV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01N0                                                      
        ( CTABLEG2 , ANCSAV , CSECTEUR , NUMSEQ , WTABLEG , NVSAV ,             
        POURC , WACTIF ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , SUBSTR ( CTABLEG2 , 9 , 2 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 3 ) , SUBSTR ( WTABLEG , 4 , 5 ) , SUBSTR ( WTABLEG , 9 , 1         
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PSSAV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01N1                                                      
        ( CTABLEG2 , NCLASSE , WTABLEG , LCLASSE ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CDCLS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01N2                                                      
        ( CTABLEG2 , SIGN , WTABLEG , USER ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 25 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GCASI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01N3                                                      
        ( CTABLEG2 , PREFT1 , OPTION , TYPDOC , WTABLEG , NATURE ,              
        CRITERE , AVIS ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        1 ) , SUBSTR ( CTABLEG2 , 3 , 2 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR ( WTABLEG , 11 , 2        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GCANT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01N4                                                      
        ( CTABLEG2 , CRUPT , CFAM , WTABLEG , SEMAINE ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RP961'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01N5                                                      
        ( CTABLEG2 , CPANNE , WTABLEG , WACTIF , LPANNE , WGENER ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HEPAN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01N6                                                      
        ( CTABLEG2 , NPOLPR , WTABLEG , P , SIZE , STYLE , BOLD , TYPO )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 4 ) , SUBSTR (               
        WTABLEG , 6 , 1 ) , SUBSTR ( WTABLEG , 7 , 1 ) , SUBSTR (               
        WTABLEG , 8 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'POLP5'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01N7                                                      
        ( CTABLEG2 , CFAM , WTABLEG , PT ) AS                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PAMPT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01N9                                                      
        ( CTABLEG2 , CACID , NLIEUHED , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HEADR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NA                                                      
        ( CTABLEG2 , CTCT , NSEQ , WTABLEG , WACTIF , ITF , LIG , LCDE )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 32 ) , SUBSTR ( WTABLEG , 34 , 3 ) , SUBSTR ( WTABLEG , 37 ,        
        18 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CSPAR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NB                                                      
        ( CTABLEG2 , NSOC , NATURE , CVENT , WTABLEG , WACTIF , SERVDEST        
        , WTRANSPO ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , SUBSTR ( CTABLEG2 , 9 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 , 1         
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGAFR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NC                                                      
        ( CTABLEG2 , NSOC , NATURE , CVENT , WTABLEG , ACTIF , SERVORIG         
        , WTRANSPO ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , SUBSTR ( CTABLEG2 , 9 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 , 1         
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGAFE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ND                                                      
        ( CTABLEG2 , CAPPLI , WTABLEG , WACTIF , CCHRONO , CVENT , CFAC         
        , TVA ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR (               
        WTABLEG , 4 , 1 ) , SUBSTR ( WTABLEG , 5 , 1 ) , SUBSTR (               
        WTABLEG , 6 , 7 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGAPP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NE                                                      
        ( CTABLEG2 , CAPPLI , CAVENT , WTABLEG , ACTIF , CVENT ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGAVE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NF                                                      
        ( CTABLEG2 , NSOCG , WTABLEG , WACTIF , NSOCS36 ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGCOS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NG                                                      
        ( CTABLEG2 , NSOC , NJRN , WTABLEG , WACTIF , CAPPLI ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,        
        2 , 3 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGDEV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NH                                                      
        ( CTABLEG2 , NSOC , SERVICE , WTABLEG , WACTIF , LIBELLE ,              
        EMETTEUR , ETAB ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 ) , SUBSTR ( WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 ,        
        3 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGSER'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NI                                                      
        ( CTABLEG2 , NSOC , CCHRONO1 , SERVICE , WTABLEG , WACTIF ,             
        CCHRONO2 ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , SUBSTR ( CTABLEG2 , 6 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTCC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NJ                                                      
        ( CTABLEG2 , NSOC , NJRN1 , NSEQ , WTABLEG , WACTIF , SERVICE ,         
        NJRN2 ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG        
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 ,         
        10 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTCJ'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NK                                                      
        ( CTABLEG2 , NSOC , NJRN1 , NSEQ , WTABLEG , WACTIF , NATFACT ,         
        NJRN2 ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG        
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 ,         
        10 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTCN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NL                                                      
        ( CTABLEG2 , CTYPMVT , WTABLEG ) AS                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 14 ) , WTABLEG                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HEGSA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NM                                                      
        ( CTABLEG2 , DSPER , WTABLEG , PERIOD , PTAUX2 , DARRETE ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG , 7 , 6 ) , SUBSTR (               
        WTABLEG , 13 , 8 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DSPER'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NN                                                      
        ( CTABLEG2 , CTAUXTVA , WTABLEG , WACTIF , PCOEFF ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 4 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TVACO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NO                                                      
        ( CTABLEG2 , CFAM , WTABLEG , CTYPE ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'REDEV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NP                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , WACTIF , WCTRLNHS , NBEXHE15 ,        
        WPANNE , WSAVELA ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 2 ) , SUBSTR ( WTABLEG , 5 , 1 ) , SUBSTR (               
        WTABLEG , 6 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HESOC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NQ                                                      
        ( CTABLEG2 , GD030 , WTABLEG , AIRE_EXC ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GD030'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NR                                                      
        ( CTABLEG2 , CODTABLE , WTABLEG , TYPEAUTO , W ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 1 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTOD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NT                                                      
        ( CTABLEG2 , CACID , WTABLEG , WACTIF , NSOCIETE , NLIEU ,              
        LIBELLE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 ) , SUBSTR ( WTABLEG , 8 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CAADR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NU                                                      
        ( CTABLEG2 , NMAG , WTABLEG , NGRPMAG ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CDGRP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NV                                                      
        ( CTABLEG2 , CGRP , TGRP , WTABLEG , LGRP , WACTIF ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 1 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PSGRP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NW                                                      
        ( CTABLEG2 , CCOUL , CUTCOUL , WTABLEG , WCADRE , R_V_B , NSAT ,        
        LCOUL ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 11 ) , SUBSTR ( WTABLEG , 13 , 3 ) , SUBSTR ( WTABLEG , 16 ,        
        20 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROCO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NX                                                      
        ( CTABLEG2 , CCOUL , WTABLEG , WACTIF , LCOUL ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 50 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROTP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NY                                                      
        ( CTABLEG2 , CODPRO , WTABLEG , CCOUL1 , CCOUL2 , CCOUL3 ,              
        CCOUL4 , CCOUL5 ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 ) , SUBSTR (             
        WTABLEG , 21 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROPP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01NZ                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RACOD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01O0                                                      
        ( CTABLEG2 , NSOCTR , NETABTR , WTABLEG , NATREMUN , CTAUXTVA ,         
        NBJOURS , TYPEDOC ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 5 ) , SUBSTR ( WTABLEG , 11 , 2 ) , SUBSTR ( WTABLEG , 13 ,         
        20 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGRMP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01O1                                                      
        ( CTABLEG2 , NSOCTR , NETABTR , WTABLEG , JRNSOC , JRNTRESO ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4         
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG        
        , 31 , 30 )                                                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGRMJ'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01O2                                                      
        ( CTABLEG2 , CETAT , WTABLEG , DDEBUT , DFIN ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 8 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'AVETA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01O3                                                      
        ( CTABLEG2 , ENTITE , WTABLEG , LIBELLE , DEVISE ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 3 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTENT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01O4                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE , NBPAGE ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG ,        
        31 , 5 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTDOC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01O5                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTDAS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01O6                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE , CODEGEO ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG ,        
        31 , 1 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTPAY'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01O7                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTGEO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01O8                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE , NBMOIS ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG ,        
        31 , 2 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTEPU'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01O9                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTTRE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OB                                                      
        ( CTABLEG2 , LIEUHC , WTABLEG , LLIEUHC , LIEUST , WCMOTIF ,            
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 14 ) , SUBSTR (            
        WTABLEG , 35 , 10 ) , SUBSTR ( WTABLEG , 45 , 1 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HCTLI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OC                                                      
        ( CTABLEG2 , NSOCLIEU , NSSLIEU , CLIEUTRT , WTABLEG , WACTIF ,         
        CAPPLI , WCONTROL ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , SUBSTR ( CTABLEG2 , 10 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR ( WTABLEG , 5 ,         
        4 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HSAPP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OD                                                      
        ( CTABLEG2 , DTRAIT , WTABLEG , WACTIF , MOIS , ABONNE , FACTURE        
        , REMUNE ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR (               
        WTABLEG , 8 , 1 ) , SUBSTR ( WTABLEG , 9 , 1 ) , SUBSTR (               
        WTABLEG , 10 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGCAL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OE                                                      
        ( CTABLEG2 , CTYPE , NATURE , NSEQ1 , NSEQ2 , WTABLEG , WACTIF ,        
        WCOMMENT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        5 ) , SUBSTR ( CTABLEG2 , 8 , 1 ) , SUBSTR ( CTABLEG2 , 9 , 2 )         
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        38 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGCOM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OF                                                      
        ( CTABLEG2 , CBATVA , WTABLEG , CTAUXTVA ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BATVA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OG                                                      
        ( CTABLEG2 , CACID , INTGEF , WTABLEG , WACTIF , CTA , CTYPOPT )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        11 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,        
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 5 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFADR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OH                                                      
        ( CTABLEG2 , CTRCTA , CTRAIT , WTABLEG , WACTIF ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFCTA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OI                                                      
        ( CTABLEG2 , NSEQ , WTABLEG , NLIGNE , NABC , WACTIF , LTEXTE ,         
        LCOMMENT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 4 ) , SUBSTR (               
        WTABLEG , 7 , 1 ) , SUBSTR ( WTABLEG , 8 , 40 ) , SUBSTR (              
        WTABLEG , 48 , 13 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROTX'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OJ                                                      
        ( CTABLEG2 , MGI , WTABLEG , CICS , TOR ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 , 4 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CIMGI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OK                                                      
        ( CTABLEG2 , CCLASSE , WTABLEG , WACTIF , CCOUL , LCOMMENT ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR (               
        WTABLEG , 7 , 50 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROEN'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OL                                                      
        ( CTABLEG2 , NSOC , WTABLEG , WFILIALE , COMMENT ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PCFFI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ON                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , NMAGFIC , ECTRAIT , TCTRAIT ,         
        CRENDU ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 5 ) , SUBSTR (               
        WTABLEG , 8 , 5 ) , SUBSTR ( WTABLEG , 13 , 5 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HSIN1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OO                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , DEPTRAIT , MAGTRAIT , HSDEP ,         
        HSMAG ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HSIN2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OP                                                      
        ( CTABLEG2 , CPANGRF , CFAMGRF , WTABLEG , CPANGHS ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HSCOP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OQ                                                      
        ( CTABLEG2 , TIERSGRF , WTABLEG , TIERSGHS ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HSCOT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OR                                                      
        ( CTABLEG2 , NOMPROG , NSEQ , WTABLEG , CSTATUT , WACTIF ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HCSTA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OS                                                      
        ( CTABLEG2 , CRAYON , NSEQ , WTABLEG , CFAM ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VADRA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OT                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , CGESTION , NLIEUINI , DEPOTGRF        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HSGES'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OU                                                      
        ( CTABLEG2 , CFAM , NSEQ , WTABLEG , WACTIF , CMARKET1 ,                
        CMARKET2 , CFAM36 ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 10 ) , SUBSTR ( WTABLEG , 12 , 10 ) , SUBSTR ( WTABLEG , 22         
        , 5 )                                                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FAM36'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OV                                                      
        ( CTABLEG2 , NSOCC , WTABLEG , WACTIF , NSOCIETE , NLIEU ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGLGP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OW                                                      
        ( CTABLEG2 , NLIEUHC , CSTATUT , WTABLEG , LSTATUT , WACTIF ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4         
        , 1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG        
        , 21 , 1 )                                                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HCCST'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OX                                                      
        ( CTABLEG2 , NSOCC , NETAB , SERVICE , SENS , WTABLEG , WACTIF ,        
        SERVCOR ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , SUBSTR ( CTABLEG2 , 12 , 1 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        5 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTSV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OY                                                      
        ( CTABLEG2 , CTAUX , DEFFET , NSOC , WTABLEG , TAUXPREC , TAUX )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        8 ) , SUBSTR ( CTABLEG2 , 10 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 6 ) , SUBSTR ( WTABLEG , 7 , 6 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTXI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01OZ                                                      
        ( CTABLEG2 , NSOCC , CETAB , COMPTE , CTIERS , WTABLEG , WACTIF         
        , CCOURANT ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , SUBSTR ( CTABLEG2 , 13 , 3 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        6 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGREG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01P0                                                      
        ( CTABLEG2 , CLIEUHET , CTRAIT , WTABLEG , WACTIF ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HEASS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01P2                                                      
        ( CTABLEG2 , CFAM , WTABLEG , WACTIF , LFAM , CRAYON , CTPSAV )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 5 ) , SUBSTR ( WTABLEG , 27 , 5 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCFMS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01P3                                                      
        ( CTABLEG2 , NSOC , WTABLEG , WACTIF , LORIG , LDEST ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 9 ) , SUBSTR (               
        WTABLEG , 11 , 9 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HSREG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01P4                                                      
        ( CTABLEG2 , NUMCLI , WTABLEG , NSOC , NLIEU ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CCDA1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01P8                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , CTCARTE , G36 , WTABLEG , WACTIF        
        , QUOTA ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , SUBSTR ( CTABLEG2 , 8 , 3 )         
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        4 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCNBS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01P9                                                      
        ( CTABLEG2 , CENTITE , WTABLEG , WACTIF , LENTITE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCCET'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PC                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , TRANCHED , TRANCHEF , WACTIF )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 7 ) , SUBSTR ( WTABLEG , 8 , 7 ) , SUBSTR (               
        WTABLEG , 15 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GATRA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PD                                                      
        ( CTABLEG2 , NCODIC , WTABLEG ) AS                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GAKAP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PG                                                      
        ( CTABLEG2 , CGCPLT , TGRP , WTABLEG , CGRP , WACTIF ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PSGRC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PI                                                      
        ( CTABLEG2 , CTPSAV , WTABLEG , LTPSAV , WACTIF ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TPSAV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PJ                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , ACTIF , ZPRIX , EXPO , W )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 2 ) , SUBSTR ( WTABLEG , 4 , 5 ) , SUBSTR ( WTABLEG , 9 , 1         
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'AVENV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PK                                                      
        ( CTABLEG2 , NSOC , CODE , WTABLEG , LIBELLE ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTETA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PL                                                      
        ( CTABLEG2 , NSOC , NOSAV , WTABLEG , WACTIF , CVENT ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGSAV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PM                                                      
        ( CTABLEG2 , CACID , WTABLEG , WACTIF , LACID ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HEFLU'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PN                                                      
        ( CTABLEG2 , NSOC , CODE , WTABLEG , LIBELLE , D_C ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 1 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTNAT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PO                                                      
        ( CTABLEG2 , LIEUHC , CTYPE , WTABLEG , WACTIF , LTYPE ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HCTPI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PP                                                      
        ( CTABLEG2 , NSOC , NATURE , AUX , WTABLEG , COMPTE ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , SUBSTR ( CTABLEG2 , 9 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 6 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTCPT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PQ                                                      
        ( CTABLEG2 , CACID , WTABLEG , NSOCLIEU ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CDADR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PR                                                      
        ( CTABLEG2 , NSOCLIEU , NSEQ , WTABLEG , CCLIENT , TCRITERE ,           
        CRITERE , NSOCCONS ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG ,         
        7 , 1 ) , SUBSTR ( WTABLEG , 8 , 5 ) , SUBSTR ( WTABLEG , 13 , 6        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CDDAC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PV                                                      
        ( CTABLEG2 , CENTITE , WTABLEG , WACTIF , LENTITE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCCEA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01PY                                                      
        ( CTABLEG2 , CTCARTE , WTABLEG , WACTIF , LCARTE ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCTCA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Q0                                                      
        ( CTABLEG2 , NSOC , CMOTIF , DEFFET , WTABLEG , TAUX ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , SUBSTR ( CTABLEG2 , 6 , 8 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 5 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RATVA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Q2                                                      
        ( CTABLEG2 , NPOL , NSEQ , WTABLEG , WACTIF , DATAS ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 59 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'POLLG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Q3                                                      
        ( CTABLEG2 , CTRANS , WTABLEG , CTRAN , TYPAP , LIBTRAN ,               
        LIBCTRAN , WCOMPTA ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 30 ) , SUBSTR ( WTABLEG , 33 , 17 ) , SUBSTR (            
        WTABLEG , 50 , 10 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'CTIN3'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Q4                                                      
        ( CTABLEG2 , CTLIEU , WTABLEG , WACTIF , LIBELLE , CTYPLIEU ,           
        CTYPSOC , WCONTROL ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 3 ) , SUBSTR (             
        WTABLEG , 26 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HCLIE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Q5                                                      
        ( CTABLEG2 , IDENT , WTABLEG , ACTIF , SA1 , SA2 , SA3 ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 ) , SUBSTR ( WTABLEG , 8 , 3 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'IDADR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Q6                                                      
        ( CTABLEG2 , NSOCC , WTABLEG , WACTIF ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGSCT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Q8                                                      
        ( CTABLEG2 , CPREST , WTABLEG , WACTIF , LPREST , PPRIME ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'VTPR2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Q9                                                      
        ( CTABLEG2 , CINSEE , WTABLEG , LREGION , CPAYS , WPARBUR ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 26 ) , SUBSTR ( WTABLEG , 27 , 5 ) , SUBSTR (             
        WTABLEG , 32 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GVETR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QB                                                      
        ( CTABLEG2 , CTRQ , WTABLEG , LTRQ , WTRQ ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG , 16 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GRTRQ'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QC                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PPMAG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QD                                                      
        ( CTABLEG2 , NDEPOT , WTLMELA , WTABLEG , QNBREF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GS805'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QE                                                      
        ( CTABLEG2 , NCISOC , NSOCIETE , NLIEU , WTABLEG , WACTIF ,             
        CENTITE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCEXT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QF                                                      
        ( CTABLEG2 , CENTITE , NSEQ , WTABLEG , WACTIF , NCISOC ,               
        NSOCLIEU , CTCARTE ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 6 ) , SUBSTR ( WTABLEG , 11 , 1        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCEXA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QI                                                      
        ( CTABLEG2 , CENTITE , NSEQ , WTABLEG , WACTIF , NCISOC , TYPES         
        , CTCARTE ) AS                                                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 4 ) , SUBSTR ( WTABLEG , 9 , 1         
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCGAA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QJ                                                      
        ( CTABLEG2 , NSOCLIEU , WTABLEG , WACTIF , LSAV , GRSAV ,               
        FEXTACT , SAVMGI ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 3 ) , SUBSTR ( WTABLEG , 25 , 2 ) , SUBSTR (             
        WTABLEG , 27 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCGRS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QK                                                      
        ( CTABLEG2 , CLIENT , WTABLEG , ACTIF , SOC , LIEU , CVENT ,            
        CRITERE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 ) , SUBSTR ( WTABLEG , 8 , 5 ) , SUBSTR (               
        WTABLEG , 13 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGDA1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QL                                                      
        ( CTABLEG2 , CPRIME , WTABLEG , WACTIF , LPRIME , CGRPRIME ,            
        LNOMZONE , CPRISIGA ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 5 ) , SUBSTR ( WTABLEG , 37 , 10 ) , SUBSTR (            
        WTABLEG , 47 , 3 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PPPRI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QM                                                      
        ( CTABLEG2 , CGRPRIME , WTABLEG , WACTIF , LGRPRIME ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PPGRP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QN                                                      
        ( CTABLEG2 , CTYPSIGA , WTABLEG , WACTIF , WCONST ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PPSIG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QO                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , WACTIF , QUOTA ,              
        CPERIM , GRVTE ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 4 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR ( WTABLEG , 11 , 3        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCNBV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QP                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , DMOIS ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PPSTA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QQ                                                      
        ( CTABLEG2 , CACID , WTABLEG , CCLIENT , WACTIF , WPASS ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG , 7 , 1 ) , SUBSTR (               
        WTABLEG , 8 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DACID'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QR                                                      
        ( CTABLEG2 , CCLIENT , WTABLEG , PASS , WACTIF , WPRIX ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 , 1 ) , SUBSTR (               
        WTABLEG , 6 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DACMP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QT                                                      
        ( CTABLEG2 , CTYPE , TYPDOC , WTABLEG , LIBELLE , WPARAM ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG ,        
        31 , 6 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GCATD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QU                                                      
        ( CTABLEG2 , NSOCMGI , CFAMMGI , WTABLEG , RUBVEN , RUBPSE ,            
        RUBREM ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        4 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 8 ) , SUBSTR ( WTABLEG , 17 , 32 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGFBI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QV                                                      
        ( CTABLEG2 , NJRN , NSOCC , WTABLEG , WACTIF ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                            
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGGCT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QW                                                      
        ( CTABLEG2 , CDFIL , CFAMMGI , CPSEMGI , WTABLEG , WACTIF ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        4 ) , SUBSTR ( CTABLEG2 , 8 , 2 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MGPSF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QX                                                      
        ( CTABLEG2 , NOMETAT , NOBJET , WTABLEG , WACTIF , DATAS ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 52 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROOG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01QZ                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , WACTIF , WTYPE , LLIEU ,          
        SECTION ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 25 ) , SUBSTR ( WTABLEG , 28 ,         
        6 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RALIE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01R1                                                      
        ( CTABLEG2 , CFAM , CINSEE , WTABLEG , WACTIF , CSECTEUR , NOSAV        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 ) , SUBSTR ( WTABLEG , 7 , 3 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PSREP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01R3                                                      
        ( CTABLEG2 , BGD025 , WTABLEG , DRAFALE , NRAFALE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 2 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GD025'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01R6                                                      
        ( CTABLEG2 , CPAYS , WTABLEG , LPAYS ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 26 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GVET2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01RA                                                      
        ( CTABLEG2 , CTYPLIG , WTABLEG , LTYPLIG , TLIEUSOC ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 24 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GBTLI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01RC                                                      
        ( CTABLEG2 , NUMECS , WTABLEG , WACTIF ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FXCTL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01RG                                                      
        ( CTABLEG2 , CENERGIE , NSEQ , WTABLEG , WACTIF , NLGABC ,              
        LTEXTE , LCOMMENT ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 6 ) , SUBSTR ( WTABLEG , 8 , 40 ) , SUBSTR ( WTABLEG , 48 ,         
        6 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROTE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01RJ                                                      
        ( CTABLEG2 , CSTABLE , WTABLEG , WACTIF , LCOMMEN ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MINUS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01RL                                                      
        ( CTABLEG2 , CSELART , CNATART , WTABLEG , AUTOMAJ , LCOMPENS )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 50 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GBQLI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01RP                                                      
        ( CTABLEG2 , SSF , FACTURE , WTABLEG , W , LIBELLE ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'BAFAC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01RR                                                      
        ( CTABLEG2 , SSF , SOC , MAG , WTABLEG , W , F ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        3 ) , SUBSTR ( CTABLEG2 , 6 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SRP34'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01TP                                                      
        ( CTABLEG2 , CTPREST , WTABLEG , WACTIF , DELMIN , DELMAX ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRTYL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01U1                                                      
        ( CTABLEG2 , CALERT , WTABLEG , LALERT ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD80'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01U3                                                      
        ( CTABLEG2 , CMPAIDEV , WTABLEG , DEFFFEFF , WRENDU , PREND1 ,          
        PREND2 , PARRON ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 16 ) , SUBSTR ( WTABLEG , 17 , 1 ) , SUBSTR (             
        WTABLEG , 18 , 5 ) , SUBSTR ( WTABLEG , 23 , 5 ) , SUBSTR (             
        WTABLEG , 28 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFM52'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01U4                                                      
        ( CTABLEG2 , CMOPAI , CMORMB , CDEV , WTABLEG , DEFFFEFF ,              
        SEUIL1 , SEUILM ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 16 ) , SUBSTR ( WTABLEG , 17 , 5 ) , SUBSTR ( WTABLEG , 22        
        , 5 )                                                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EFM53'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01U6                                                      
        ( CTABLEG2 , CREPRI , WTABLEG , LREPRI ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EGV19'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01U7                                                      
        ( CTABLEG2 , CNATUR , WTABLEG , LNATUR ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EGV86'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01U8                                                      
        ( CTABLEG2 , NTIERS , WTABLEG , LTIERS ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 9 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 25 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EPA53'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01UK                                                      
        ( CTABLEG2 , CFORMAT , WTABLEG , WACTIF , NOMETAT , LFORMAT ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR (               
        WTABLEG , 10 , 30 )                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FORMK'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01UL                                                      
        ( CTABLEG2 , NSOCIETE , MODP , WTABLEG , LIBMODP ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 25 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'AV004'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01UN                                                      
        ( CTABLEG2 , CETAT , WTABLEG , DDEBUT , DFIN ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 8 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NEMET'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01UV                                                      
        ( CTABLEG2 , CMOPAI , CDEVWTCO , WSEQAF , WTABLEG , PVALCP ,            
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        4 ) , SUBSTR ( CTABLEG2 , 10 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 1 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD01'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01UW                                                      
        ( CTABLEG2 , CTCAIS , CMOPAI , WTABLEG ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        5 ) , WTABLEG                                                           
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD02'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01UX                                                      
        ( CTABLEG2 , CTCAIS , CMVTFC , WTABLEG ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        2 ) , WTABLEG                                                           
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD03'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01UY                                                      
        ( CTABLEG2 , CTTRAN , CTCAIS , WTABLEG ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        1 ) , WTABLEG                                                           
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD06'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01UZ                                                      
        ( CTABLEG2 , CTCAIS , WTABLEG , LTCAIS , WTRNSF ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD21'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01V7                                                      
        ( CTABLEG2 , NSOC , NLIEU , NCAISS , WTABLEG , CTCAIS ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD04'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VC                                                      
        ( CTABLEG2 , CTTRAN , WTABLEG , LBTRAN , CANNUL ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM04'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VD                                                      
        ( CTABLEG2 , CMVTFC , WTABLEG , LBMVTF , LMEDIT ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 10 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM07'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VE                                                      
        ( CTABLEG2 , LPAPPT , CVCODE , CACT , WTABLEG , CTCODE , LPAPPE         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 4 ) , SUBSTR ( CTABLEG2 , 15 , 1 ) , WTABLEG , SUBSTR (               
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 10 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM11'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VG                                                      
        ( CTABLEG2 , CCOMV , CTCOMV , WTABLEG , LCOMV ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM30'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VH                                                      
        ( CTABLEG2 , NSOCLIEU , CTYPE , WSEQLA , WTABLEG , NPROG ,              
        DATEDP , PERIOD ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , SUBSTR ( CTABLEG2 , 12 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 8 ) , SUBSTR ( WTABLEG , 19        
        , 1 )                                                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM36'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VI                                                      
        ( CTABLEG2 , CMOTIF , WTABLEG , LMOTIF , WPASS ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 10 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM50'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VJ                                                      
        ( CTABLEG2 , TPCALC , CDEV , WTABLEG , WARRON , QARRON ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 10 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM60'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VL                                                      
        ( CTABLEG2 , CSTABLE , WTABLEG , WACTIF ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NEMAT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VN                                                      
        ( CTABLEG2 , SOCLIEU , DATE , NSEQ , WTABLEG , INDICATE ,               
        LIEUGEST , DOMAINE ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        8 ) , SUBSTR ( CTABLEG2 , 15 , 1 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 6 ) , SUBSTR ( WTABLEG , 12 ,        
        5 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NASL1'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VR                                                      
        ( CTABLEG2 , CACID , WTABLEG , ACTIF , SOCLIEU , COMPTA , GROUPE        
        , ENCNASL ) AS                                                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR (               
        WTABLEG , 8 , 1 ) , SUBSTR ( WTABLEG , 9 , 1 ) , SUBSTR (               
        WTABLEG , 10 , 2 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLADR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VS                                                      
        ( CTABLEG2 , DOMAINE , COMPTEUR , WTABLEG , ACTIF , LIBELLE ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6         
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 20 )                                                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NASL2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01VT                                                      
        ( CTABLEG2 , CMDRGT , WTABLEG , LMDRGL , NSEQED , CMDRGTH ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 2 ) , SUBSTR (             
        WTABLEG , 33 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EGV83'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01W0                                                      
        ( CTABLEG2 , CFLGMAG , NSEQ , WTABLEG , CGROUP ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FLCMG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01W3                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , LSOCIETE ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GP465'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01W4                                                      
        ( CTABLEG2 , NSOCENTR , LDEPLIV , CMODDEL , WTABLEG , WACTIF ,          
        CPLAGES ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , SUBSTR ( CTABLEG2 , 13 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GVCPL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01W5                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , WBMUTMUT , WBMUTDEM ,         
        WJMU242 , WJMU243 ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 1         
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FLMUT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01W6                                                      
        ( CTABLEG2 , NSOCENTR , LDEPLIV , CMODDEL , WTABLEG , WACTIF ,          
        QDELAI ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , SUBSTR ( CTABLEG2 , 13 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GVDEL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01W7                                                      
        ( CTABLEG2 , NSOCIETE , PROFIL , WTABLEG , ACTIF , NBJN , NBJM )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROST'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01W8                                                      
        ( CTABLEG2 , NSOCORIG , NSOCDEST , WTABLEG , SYSID ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 4 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SYSID'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01WA                                                      
        ( CTABLEG2 , SOCIETE , LIEU , TYPE , WTABLEG , ACTIF , DATE ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4         
        , 3 ) , SUBSTR ( CTABLEG2 , 7 , 2 ) , WTABLEG , SUBSTR ( WTABLEG        
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NASL0'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01WE                                                      
        ( CTABLEG2 , WVENTE , WTABLEG , WVVNTE ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'EGV07'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01WK                                                      
        ( CTABLEG2 , TYPE33 , WTABLEG , SURPLUS , HISTM , HISTH ,               
        DINVENT ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR (               
        WTABLEG , 4 , 2 ) , SUBSTR ( WTABLEG , 6 , 8 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'STOCK'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01WN                                                      
        ( CTABLEG2 , NSEQ , WTABLEG , CPROAFF , CMARQ , CFAM ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FLEXC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01WO                                                      
        ( CTABLEG2 , CTYPTRT , WTABLEG , LTYPTRT , NPRIORIT ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FLTRT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01WR                                                      
        ( CTABLEG2 , CUSINE , WTABLEG , LUSINE , WACTIF , NENTCDE ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'USINE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01WS                                                      
        ( CTABLEG2 , NSOCLIEU , TMVT , WTABLEG , ACTIF , NSSLIEUO ,             
        NSSLIEUD , COMMENT ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        8 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 9 ) , SUBSTR ( WTABLEG , 11 , 9 ) , SUBSTR ( WTABLEG , 20 ,         
        26 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PARMV'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01WW                                                      
        ( CTABLEG2 , CACID , WTABLEG , NSOCCICS , NSOCIETE , NLIEU ,            
        CFLGMAG ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 ) , SUBSTR (               
        WTABLEG , 7 , 3 ) , SUBSTR ( WTABLEG , 10 , 5 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MFL10'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01X0                                                      
        ( CTABLEG2 , CLIENT , WTABLEG , WACTIF , SOC , LIEU , CVENT ,           
        CRITERE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 ) , SUBSTR ( WTABLEG , 8 , 5 ) , SUBSTR (               
        WTABLEG , 13 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FGDA2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01X4                                                      
        ( CTABLEG2 , PREFT1 , WTABLEG , PBF , TOTLI , TOTFAC , TOTLIT )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 4 ) , SUBSTR (               
        WTABLEG , 8 , 5 ) , SUBSTR ( WTABLEG , 13 , 5 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GCAEE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01X5                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , WMG , CMAGPR , WSL ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 6 ) , SUBSTR ( WTABLEG , 8 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MDMAG'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01X7                                                      
        ( CTABLEG2 , NSAV , WTABLEG , FORMAT ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NM10 '                                                      
;                                                                               
CREATE  VIEW P999.RVGA01X8                                                      
        ( CTABLEG2 , NSOC , NDEPOT , NSSLIEU , CLIEUTRT , WTABLEG ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , SUBSTR ( CTABLEG2 , 10 , 5 )        
        , WTABLEG                                                               
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'INVHB'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01X9                                                      
        ( CTABLEG2 , CAPPLI , CMESS , WTABLEG , LIBEL , RESP , CGRA ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3         
        , 7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG        
        , 16 , 5 ) , SUBSTR ( WTABLEG , 21 , 2 )                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'HELPD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01XM                                                      
        ( CTABLEG2 , CFAM , WTABLEG , ACTIF , NBETIQ ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ITFAM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01XN                                                      
        ( CTABLEG2 , NCODIC , WTABLEG , WACTIF ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PAREP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01XS                                                      
        ( CTABLEG2 , APPLI , WTABLEG , LIBELLE , NBMAJ , DELAI , PARAM1         
        , PARAM2 ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 6 ) , SUBSTR (             
        WTABLEG , 27 , 6 ) , SUBSTR ( WTABLEG , 33 , 9 ) , SUBSTR (             
        WTABLEG , 42 , 9 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MDCNA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01XT                                                      
        ( CTABLEG2 , APPLI , TABLE , WTABLEG , LIBEL , CRTMSG , RCPMSG )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG         
        , 21 , 10 ) , SUBSTR ( WTABLEG , 31 , 10 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MDCNM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01XU                                                      
        ( CTABLEG2 , NOMPROG , WTABLEG , WACTIF , DEVISE , DEFFET ,             
        DEVANC ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 8 ) , SUBSTR ( WTABLEG , 13 , 3 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FEURO'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01XV                                                      
        ( CTABLEG2 , CPGM , NSEQ , WTABLEG , WTYPD , DELAI , LIB ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 5 ) , SUBSTR ( WTABLEG , 7 , 20 )                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'DELAS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01XW                                                      
        ( CTABLEG2 , CPARAM , WTABLEG , WACTIF ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NMDPF'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Y0                                                      
        ( CTABLEG2 , TYPAUX , WTABLEG , LTYPAX , WVND , WHS , WGHE ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLAXA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Y1                                                      
        ( CTABLEG2 , CAUX , WTABLEG , LAUX , TYPAUX , CTYPL , NSOCLIEU )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 6 ) , SUBSTR (             
        WTABLEG , 27 , 1 ) , SUBSTR ( WTABLEG , 28 , 6 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLAXB'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Y2                                                      
        ( CTABLEG2 , CAUX , WMAGPR , CMAGPR , WTABLEG , WAUTOR ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        1 ) , SUBSTR ( CTABLEG2 , 8 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLAXD'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Y3                                                      
        ( CTABLEG2 , CTYPD , WTABLEG , LTYPD , BMIN , BMAX ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 9 ) , SUBSTR (             
        WTABLEG , 30 , 9 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLDOC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Y4                                                      
        ( CTABLEG2 , CETAT , WTABLEG , LETAT , WVND , WTQ , WHS , WUT )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 ) , SUBSTR (             
        WTABLEG , 24 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLETA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Y5                                                      
        ( CTABLEG2 , COPAR , WOR , EVENT , NSEQ , WTABLEG , COPSL ,             
        WHOST ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , SUBSTR ( CTABLEG2 , 8 , 1 )         
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 ,         
        1 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPA'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Y6                                                      
        ( CTABLEG2 , CAUX , COPAR , WTABLEG , COPSD ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Y7                                                      
        ( CTABLEG2 , COPAR , WTABLEG , LOPAR ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Y8                                                      
        ( CTABLEG2 , COPSL , WMAGPR , CMAGPR , WTABLEG , WAUT , WARB )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01Y9                                                      
        ( CTABLEG2 , COPSL , CPRFID , WTABLEG , WAUT ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YA                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRRGL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YB                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRFAC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YD                                                      
        ( CTABLEG2 , NENTCDE , WABOREN , WTABLEG , NBL ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRNBL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YF                                                      
        ( CTABLEG2 , CPERIM , NSEQ , CTQUOTA , WTABLEG , CEQUIP ,               
        CMODDEL , WZONE ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , SUBSTR ( CTABLEG2 , 8 , 1 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR ( WTABLEG , 11 , 1        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LIVRP'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YI                                                      
        ( CTABLEG2 , CAR , WTABLEG , LAR ) AS                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRARE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YJ                                                      
        ( CTABLEG2 , NSOCLIVR , NDEPOT , WTABLEG , WACTIF , WLIBELLE ,          
        QUOTA1 , QUOTA2 ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 18 ) , SUBSTR ( WTABLEG , 20 , 4 ) , SUBSTR ( WTABLEG , 24 ,        
        4 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCNBL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YL                                                      
        ( CTABLEG2 , NSOCLIEU , TYPMVT , WTABLEG , ACTIF , NSSLIEUO ,           
        NSSLIEUD , COMMENT ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        9 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 9 ) , SUBSTR ( WTABLEG , 11 , 9 ) , SUBSTR ( WTABLEG , 20 ,         
        20 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PARMI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YM                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , CDINTT , WTABLEG , NGROUPE ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 10 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCTEC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YN                                                      
        ( CTABLEG2 , ENTCP , WTABLEG , ENTC1 , ENTC2 , ENTC3 , ENTCR )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PROPE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YQ                                                      
        ( CTABLEG2 , CTCARTE , CRAYON , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCRAY'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YR                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , LIBSOC , TIERS , CPT ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NM009'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YS                                                      
        ( CTABLEG2 , CETAT , LIEU , WTABLEG , WFLAG ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                            
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MD400'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YT                                                      
        ( CTABLEG2 , COPER , WTABLEG , TOTALON ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MD401'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YU                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , TOPNUM , VGRNUM ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'NMCPT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YV                                                      
        ( CTABLEG2 , CASSUR , WTABLEG , WACTIF , LASSUR , TXREMISE ,            
        DEFFET , ANCTXREM ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 25 ) , SUBSTR (              
        WTABLEG , 27 , 4 ) , SUBSTR ( WTABLEG , 31 , 8 ) , SUBSTR (             
        WTABLEG , 39 , 4 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FPASS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01YZ                                                      
        ( CTABLEG2 , CFONC , WMAGPR , CMAGPR , WTABLEG , WREPONSE ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 10 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MDPPM'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZA                                                      
        ( CTABLEG2 , COPAR , WTABLEG , CTYPDR , WSENS ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 1 )                          
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPR'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZB                                                      
        ( CTABLEG2 , COPSL , WOR , WMAGPR , CMAGPR , NSEQ , WTABLEG ,           
        CEMPL ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , SUBSTR ( CTABLEG2 , 8 , 6 )         
        , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1         
        , 5 )                                                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZC                                                      
        ( CTABLEG2 , CPGM , CTRT , WTABLEG , LTRT , COPSL ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG        
        , 21 , 5 )                                                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZD                                                      
        ( CTABLEG2 , CRSL , WTABLEG , LRSL , INDETAT , INDEMPL , CEMPL ,        
        NSEQ ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 4 ) , SUBSTR (             
        WTABLEG , 25 , 6 ) , SUBSTR ( WTABLEG , 31 , 5 ) , SUBSTR (             
        WTABLEG , 36 , 3 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLRSL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZE                                                      
        ( CTABLEG2 , CTYPRS , WTABLEG , LTYPRS , WVENTE , WATAFF ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLTRS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZF                                                      
        ( CTABLEG2 , TYPUTI , WTABLEG , LIBEL , COPSL ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 )                        
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLUTI'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZG                                                      
        ( CTABLEG2 , CTLIEU , WTABLEG , CAUX ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLAXL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZN                                                      
        ( CTABLEG2 , CPGM , NCOL , WTABLEG , CRSL , TQTE , MSGENT ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG         
        , 6 , 1 ) , SUBSTR ( WTABLEG , 7 , 7 )                                  
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLCOL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZO                                                      
        ( CTABLEG2 , COPSL , WMAGPR , CMAGPR , WTABLEG , WCONSO ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPC'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZP                                                      
        ( CTABLEG2 , SOCZP , WTABLEG , CPARAM1 , CPARAM2 , CPARAM3 ,            
        CPARAM4 ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 10 ) , SUBSTR (            
        WTABLEG , 21 , 10 ) , SUBSTR ( WTABLEG , 31 , 20 )                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'ZPFIL'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZQ                                                      
        ( CTABLEG2 , SOCIETE , MONTANT , WTABLEG , LETTRE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'COPR2'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZU                                                      
        ( CTABLEG2 , SOCORIG , SOCDEST , WTABLEG , WACTIF ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTFUS'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZV                                                      
        ( CTABLEG2 , NSOC , NETAB , COMPTE , WTABLEG , TRANSPO ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 6 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'FTFUT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA01ZY                                                      
        ( CTABLEG2 , CRSL , NSEQ , WTABLEG , CEMPL ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLRSE'                                                      
;                                                                               
CREATE  VIEW P999.RVGA1000                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT                                       
    FROM  P999.RTGA10                                                           
;                                                                               
CREATE  VIEW P999.RVGA1001                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT , CGRPMUT                             
    FROM  P999.RTGA10                                                           
;                                                                               
CREATE  VIEW P999.RVGA1002                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT , CGRPMUT , NSOCVALO ,                
        NLIEUVALO , CTYPLIEUVALO                                                
    FROM  P999.RTGA10                                                           
;                                                                               
CREATE  VIEW P999.RVGA1003                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT , CGRPMUT , NSOCVALO ,                
        NLIEUVALO , CTYPLIEUVALO , NSOCGRP , NORDRE , CACID1 , CACID2 ,         
        CACID3 , CACID4 , CACID5 , CACID6                                       
    FROM  P999.RTGA10                                                           
;                                                                               
CREATE  VIEW P999.RVGA1004                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT , CGRPMUT , NSOCVALO ,                
        NLIEUVALO , CTYPLIEUVALO , NSOCGRP , NORDRE , CACID1 , CACID2 ,         
        CACID3 , CACID4 , CACID5 , CACID6 , CFRAIS , SECTANA                    
    FROM  P999.RTGA10                                                           
;                                                                               
CREATE  VIEW P999.RVGA1005                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT , CGRPMUT , NSOCVALO ,                
        NLIEUVALO , CTYPLIEUVALO , NSOCGRP , NORDRE , CACID1 , CACID2 ,         
        CACID3 , CACID4 , CACID5 , CACID6 , CFRAIS , SECTANA , CCUMCA           
    FROM  P999.RTGA10                                                           
;                                                                               
CREATE  VIEW P999.RVGA7101                                                      
        AS                                                                      
SELECT  *                                                                       
    FROM  P999.RTGA71                                                           
;                                                                               
CREATE  VIEW P999.RVGA9900                                                      
        AS                                                                      
SELECT  ALL CNOMPGRM , NSEQERR , CERRUT , LIBERR                                
    FROM  P999.RTGA99                                                           
;                                                                               
CREATE  VIEW P999.RVGA9901                                                      
        AS                                                                      
SELECT  ALL CNOMPGRM , NSEQERR , CERRUT , LIBERR , CLANG                        
    FROM  P999.RTGA99                                                           
;                                                                               
CREATE  VIEW P999.RVGSELA                                                       
        ( CTABLEG2 , SOCENTRE , DEPOT , CSELART , WTABLEG , LSELART ,           
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 4 ) , SUBSTR ( WTABLEG , 5 , 1 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GSELA'                                                      
;                                                                               
CREATE  VIEW P999.RVGVCAD                                                       
        ( CTABLEG2 , CADRE , WTABLEG , TCADRE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'GVCAD'                                                      
;                                                                               
CREATE  VIEW P999.RVIMPME                                                       
        ( CTABLEG2 , SOCIETE , LIEU , NOMLOG , WTABLEG , NOMPHY ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 9 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 10 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'IMPME'                                                      
;                                                                               
CREATE  VIEW P999.RVIMPMF                                                       
        ( CTABLEG2 , NSOC , NLIEU , CFONC , NSEQ , WTABLEG , CIMP ,             
        CIMPH ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 7 ) , SUBSTR ( CTABLEG2 , 14 , 2 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11         
        , 4 )                                                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'IMPMF'                                                      
;                                                                               
CREATE  VIEW P999.RVIMPMG                                                       
        ( CTABLEG2 , CFONC , WTABLEG , OUTQ ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'IMPMG'                                                      
;                                                                               
CREATE  VIEW P999.RVLIBL                                                        
        ( CTABLEG2 , TYLBL , SOC , LIEU , CHRONO , WTABLEG , LIBELLE )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , SUBSTR ( CTABLEG2 , 9 , 3 ) , SUBSTR ( CTABLEG2 , 12 , 4 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 51 )                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LIBL '                                                      
;                                                                               
CREATE  VIEW P999.RVLLDAP                                                       
        ( CTABLEG2 , NSOCHR , NLIEUHR , WTABLEG , WTRT , NSOC , NLIEU )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 3 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'LLDAP'                                                      
;                                                                               
CREATE  VIEW P999.RVMDACC                                                       
        ( CTABLEG2 , SOC , MAG , CFAM , WTABLEG , CMODEL , WACTIF ,             
        VALEUR ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 3 ) , SUBSTR ( WTABLEG , 4 , 1 ) , SUBSTR ( WTABLEG , 5 , 20        
        )                                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'MDACC'                                                      
;                                                                               
CREATE  VIEW P999.RVPAINT                                                       
        ( CTABLEG2 , TYPVTE , CMOPAI , WTABLEG , DATEDEB , DATEFIN ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 8 )                                                                 
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PAINT'                                                      
;                                                                               
CREATE  VIEW P999.RVPARME                                                       
        ( CTABLEG2 , CPARAM , NSOCLIEU , CFAM , WTABLEG , CETAT , WFLAG         
        , LCOMMENT ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        6 ) , SUBSTR ( CTABLEG2 , 10 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 6 ) , SUBSTR ( WTABLEG , 7 , 1 ) , SUBSTR ( WTABLEG , 8 ,         
        40 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PARME'                                                      
;                                                                               
CREATE  VIEW P999.RVPETIQ                                                       
        ( CTABLEG2 , NSOCIETE , ZOPRIX , CPARAM , WTABLEG , VALPARM ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4         
        , 2 ) , SUBSTR ( CTABLEG2 , 6 , 5 ) , WTABLEG , SUBSTR ( WTABLEG        
        , 1 , 56 )                                                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PETIQ'                                                      
;                                                                               
CREATE  VIEW P999.RVPLAGE                                                       
        ( CTABLEG2 , CPLAGE , WTABLEG , LPLAGE ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PLAGE'                                                      
;                                                                               
CREATE  VIEW P999.RVPRALP                                                       
        ( CTABLEG2 , CPREST , WTABLEG , NEAN ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 13 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRALP'                                                      
;                                                                               
CREATE  VIEW P999.RVPRPAV                                                       
        ( CTABLEG2 , TYPRES , ZONE , DEFFET , WTABLEG , CPRESTAT ,              
        VALEUR , TCALCUL ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , SUBSTR ( CTABLEG2 , 8 , 8 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 5 ) , SUBSTR ( WTABLEG , 6 , 6 ) , SUBSTR ( WTABLEG , 12 ,          
        30 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PRPAV'                                                      
;                                                                               
CREATE  VIEW P999.RVPVACC                                                       
        ( CTABLEG2 , RAYFAM , WTABLEG , W , LIBELLE ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PVACC'                                                      
;                                                                               
CREATE  VIEW P999.RVPWCNC                                                       
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , PWCNC ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'PWCNC'                                                      
;                                                                               
CREATE  VIEW P999.RVQCPRE                                                       
        ( CTABLEG2 , CPRESTA , WTABLEG , WACTIF , CTYPSERV , TCARTE ,           
        LIEU , CFAM ) AS                                                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR (               
        WTABLEG , 7 , 1 ) , SUBSTR ( WTABLEG , 8 , 6 ) , SUBSTR (               
        WTABLEG , 14 , 5 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'QCPRE'                                                      
;                                                                               
CREATE  VIEW P999.RVRB050                                                       
        ( CTABLEG2 , NSEQ , WTABLEG , IDENTMSG , NOMDTD , LGCOPY ,              
        CPARAM ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 20 ) , SUBSTR (            
        WTABLEG , 41 , 5 ) , SUBSTR ( WTABLEG , 46 , 10 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RB050'                                                      
;                                                                               
CREATE  VIEW P999.RVRBMHS                                                       
        ( CTABLEG2 , NSOC , NLIEU , CFAM , WTABLEG , TYPAUX , CAUX ,            
        PARGHE ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 6 ) , SUBSTR ( WTABLEG , 7 , 6 ) , SUBSTR ( WTABLEG , 13 ,          
        10 )                                                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'RBMHS'                                                      
;                                                                               
CREATE  VIEW P999.RVSDA2I                                                       
        ( CTABLEG2 , NSOCIETE , DEFFET , WTABLEG , NSOCDA2I , LSOCDA2I )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        8 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 50 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SDA2I'                                                      
;                                                                               
CREATE  VIEW P999.RVSEQSA                                                       
        ( CTABLEG2 , CSELA , WTABLEG , NSEQ ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SEQSA'                                                      
;                                                                               
CREATE  VIEW P999.RVSG0100                                                      
        AS                                                                      
SELECT  ALL NSOC , NETAB , RUBPAIE , CONTRAT , CPTEBIL , SENSBIL ,              
        CPTECHG , SENSCHG , CSECTION , RUBRIQUE , CPTECONT , SENSCONT ,         
        SECCONT , RUBRCONT , DSYST                                              
    FROM  P999.RTSG01                                                           
;                                                                               
CREATE  VIEW P999.RVSG0200                                                      
        AS                                                                      
SELECT  ALL NSOC , NETAB , SECPAIE , CSECTION , SECGRP , DSYST                  
    FROM  P999.RTSG02                                                           
;                                                                               
CREATE  VIEW P999.RVSG0300                                                      
        AS                                                                      
SELECT  ALL NSOC , NETAB , NETABADM , COMPTE , NTIERS , DSYST                   
    FROM  P999.RTSG03                                                           
;                                                                               
CREATE  VIEW P999.RVSGFDC                                                       
        ( CTABLEG2 , CODELIEU , WTABLEG , WACTIF ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SGFDC'                                                      
;                                                                               
CREATE  VIEW P999.RVSIBVE                                                       
        ( CTABLEG2 , VENTE , WTABLEG , ACTIF , RESTE ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 14 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 50 )                         
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SIBVE'                                                      
;                                                                               
CREATE  VIEW P999.RVSLDAT                                                       
        ( CTABLEG2 , DATLIM , WTABLEG , WFLAG ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLDAT'                                                      
;                                                                               
CREATE  VIEW P999.RVSLDIM                                                       
        ( CTABLEG2 , NSOC , NLIEU , DINVDA , WTABLEG , DATDEB , DATINV )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 8 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 8 ) , SUBSTR ( WTABLEG , 9 , 8 )                                    
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLDIM'                                                      
;                                                                               
CREATE  VIEW P999.RVSLPSL                                                       
        ( CTABLEG2 , CFONC , CEMPL , WTABLEG , WREPONS ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLPSL'                                                      
;                                                                               
CREATE  VIEW P999.RVSLTDA                                                       
        ( CTABLEG2 , NSOC , NLIEU , WMAGPR , CMAGPR , CTYPDOC , WTABLEG         
        , WAUT ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , SUBSTR ( CTABLEG2 , 8 , 6 )         
        , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1         
        , 1 )                                                                   
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SLTDA'                                                      
;                                                                               
CREATE  VIEW P999.RVSM145                                                       
        ( CTABLEG2 , CHEFPROD , WTABLEG , LGDRAYON , LCOMMENT ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 20 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SM145'                                                      
;                                                                               
CREATE  VIEW P999.RVSTMAG                                                       
        ( CTABLEG2 , SOC , MAG , WTABLEG , WACTIF ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'STMAG'                                                      
;                                                                               
CREATE  VIEW P999.RVSVFAM                                                       
        ( CTABLEG2 , APPLICAT , CFAM , NSEQ , WTABLEG , CMARKET1 ,              
        CMARKET2 , CFAMCIB ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        5 ) , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 15 ) , SUBSTR ( WTABLEG , 16 , 15 ) , SUBSTR ( WTABLEG ,          
        31 , 5 )                                                                
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'SVFAM'                                                      
;                                                                               
CREATE  VIEW P999.RVTDANO                                                       
        ( CTABLEG2 , CDOSSIER , CODANO , WTABLEG , NAFFICHE , LIBANO ,          
        WCTRL , PROV ) AS                                                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG ,         
        3 , 36 ) , SUBSTR ( WTABLEG , 39 , 1 ) , SUBSTR ( WTABLEG , 40 ,        
        1 )                                                                     
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'TDANO'                                                      
;                                                                               
CREATE  VIEW P999.RVXMLCS                                                       
        ( CTABLEG2 , CARACT , WTABLEG , VALNUM , VALALPHA ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 20 )                       
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'XMLCS'                                                      
;                                                                               
CREATE  VIEW P999.RVXMLDT                                                       
        ( CTABLEG2 , CDATE , WTABLEG , FDATE ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 50 )                                                      
    FROM  P999.RTGA01                                                           
  WHERE CTABLEG1 = 'XMLDT'                                                      
;                                                                               
CREATE  VIEW P999.RVGA7100                                                 
  AS SELECT                                                                     
          CTABLE                                                                
        , CSTABLE                                                               
        , LTABLE                                                                
        , CRESP                                                                 
        , DDEBUT                                                                
        , DFIN                                                                  
        , DMAJ                                                                  
        , CHAMP1                                                                
        , LCHAMP1                                                               
        , QCHAMP1                                                               
        , WCHAMP1                                                               
        , QDEC1                                                                 
        , WCTLDIR1                                                              
        , WCTLIND1                                                              
        , CCTLIND11                                                             
        , CCTLIND12                                                             
        , CCTLIND13                                                             
        , CCTLIND14                                                             
        , CCTLIND15                                                             
        , CTABASS1                                                              
        , CSTABASS1                                                             
        , WPOSCLE1                                                              
        , CHAMP2                                                                
        , LCHAMP2                                                               
        , QCHAMP2                                                               
        , WCHAMP2                                                               
        , QDEC2                                                                 
        , WCTLDIR2                                                              
        , WCTLIND2                                                              
        , CCTLIND21                                                             
        , CCTLIND22                                                             
        , CCTLIND23                                                             
        , CCTLIND24                                                             
        , CCTLIND25                                                             
        , CTABASS2                                                              
        , CSTABASS2                                                             
        , WPOSCLE2                                                              
        , CHAMP3                                                                
        , LCHAMP3                                                               
        , QCHAMP3                                                               
        , WCHAMP3                                                               
        , QDEC3                                                                 
        , WCTLDIR3                                                              
        , WCTLIND3                                                              
        , CCTLIND31                                                             
        , CCTLIND32                                                             
        , CCTLIND33                                                             
        , CCTLIND34                                                             
        , CCTLIND35                                                             
        , CTABASS3                                                              
        , CSTABASS3                                                             
        , WPOSCLE3                                                              
        , CHAMP4                                                                
        , LCHAMP4                                                               
        , QCHAMP4                                                               
        , WCHAMP4                                                               
        , QDEC4                                                                 
        , WCTLDIR4                                                              
        , WCTLIND4                                                              
        , CCTLIND41                                                             
        , CCTLIND42                                                             
        , CCTLIND43                                                             
        , CCTLIND44                                                             
        , CCTLIND45                                                             
        , CTABASS4                                                              
        , CSTABASS4                                                             
        , WPOSCLE4                                                              
        , CHAMP5                                                                
        , LCHAMP5                                                               
        , QCHAMP5                                                               
        , WCHAMP5                                                               
        , QDEC5                                                                 
        , WCTLDIR5                                                              
        , WCTLIND5                                                              
        , CCTLIND51                                                             
        , CCTLIND52                                                             
        , CCTLIND53                                                             
        , CCTLIND54                                                             
        , CCTLIND55                                                             
        , CTABASS5                                                              
        , CSTABASS5                                                             
        , WPOSCLE5                                                              
        , CHAMP6                                                                
        , LCHAMP6                                                               
        , QCHAMP6                                                               
        , WCHAMP6                                                               
        , QDEC6                                                                 
        , WCTLDIR6                                                              
        , WCTLIND6                                                              
        , CCTLIND61                                                             
        , CCTLIND62                                                             
        , CCTLIND63                                                             
        , CCTLIND64                                                             
        , CCTLIND65                                                             
        , CTABASS6                                                              
        , CSTABASS6                                                             
        , WPOSCLE6                                                              
        , NOMVUE                                                                
FROM P999.RTGA71                                                                
;                                                                               
--MW DZ-400 CREATE  VIEW P999.RVGCTUNL                                                 
--MW DZ-400   AS SELECT                                                                     
--MW DZ-400           CTABLEG1                                                              
--MW DZ-400         , CTABLEG2                                                              
--MW DZ-400         , WTABLEG                                                               
--MW DZ-400 FROM P999.RTGA01 WHERE CTABLEG1 IN ( 'FTENT' , 'FTDAS' ,                        
--MW DZ-400   'FTGEO' , 'FTPAY' , 'FTTRE' , 'FTEPU' , 'FTDOC' )                             
--MW DZ-400 ;                                                                               
CREATE  VIEW P999.RVFX0001                                                 
  AS SELECT                                                                     
          NOECS                                                                 
        , CRITERE1                                                              
        , CRITERE2                                                              
        , CRITERE3                                                              
        , COMPTECG                                                              
        , DATEFF                                                                
        , DSYST                                                                 
        , WSEQFAM                                                               
        , SECTION                                                               
        , RUBRIQUE                                                              
        , WFX00                                                                 
FROM P999.RTFX00                                                                
;                                                                               
