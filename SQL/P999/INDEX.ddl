--MW CREATE  UNIQUE INDEX P999.RXIGSA0R ON P999.RTIGSA                               
--MW         ( DIGFA0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P999.RXIGSB0R ON P999.RTIGSB                               
--MW         ( DIGFA0 ASC                                                            
--MW         , NT_DIGSB ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P999.RXIGSC0R ON P999.RTIGSC                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFC0 ASC                                                            
--MW         , SM_DIGSC ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P999.RXIGSD0R ON P999.RTIGSD                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFD0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P999.RXIGSE0R ON P999.RTIGSE                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFE0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P999.RXIGSF0R ON P999.RTIGSF                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFF0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
CREATE  INDEX P999.RXEG101G ON P999.RTEG10                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P999.RXEG900G ON P999.RTEG90                                      
        ( IDENTTS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXEG000G ON P999.RTEG00                               
        ( NOMETAT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXEG050G ON P999.RTEG05                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXEG100G ON P999.RTEG10                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXEG110G ON P999.RTEG11                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXEG150G ON P999.RTEG15                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXEG200G ON P999.RTEG20                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXEG250G ON P999.RTEG25                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , LIGNECALC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXEG300G ON P999.RTEG30                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXEG400G ON P999.RTEG40                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM010G ON P999.RTFM01                               
        ( NENTITE ASC                                                           
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM020G ON P999.RTFM02                               
        ( CDEVISE ASC                                                           
        , WSENS ASC                                                             
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM030G ON P999.RTFM03                               
        ( NENTITE ASC                                                           
        , CMETHODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM040G ON P999.RTFM04                               
        ( CDEVISE ASC                                                           
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM050G ON P999.RTFM05                               
        ( CDEVORIG ASC                                                          
        , CDEVDEST ASC                                                          
        , DEFFET ASC                                                            
        , COPERAT ASC                                                           
        , PTAUX ASC                                                             
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM060G ON P999.RTFM06                               
        ( CETAT ASC                                                             
        , NDEMANDE ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM100G ON P999.RTFM10                               
        ( NENTITE ASC                                                           
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM110 ON P999.RTFM11                                
        ( CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM130 ON P999.RTFM13                                
        ( COMPTE ASC                                                            
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM140 ON P999.RTFM14                                
        ( CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM160 ON P999.RTFM16                                
        ( SECTION ASC                                                           
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM170 ON P999.RTFM17                                
        ( CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM190 ON P999.RTFM19                                
        ( RUBRIQUE ASC                                                          
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM200 ON P999.RTFM20                                
        ( NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM230 ON P999.RTFM23                                
        ( NENTITE ASC                                                           
        , NEXER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM250G ON P999.RTFM25                               
        ( NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM300G ON P999.RTFM30                               
        ( NATURE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM540G ON P999.RTFM54                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM550G ON P999.RTFM55                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM560G ON P999.RTFM56                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM570G ON P999.RTFM57                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM580G ON P999.RTFM58                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM590G ON P999.RTFM59                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM601 ON P999.RTFM60                                
        ( COMPTE ASC                                                            
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM611 ON P999.RTFM61                                
        ( SECTION ASC                                                           
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM621 ON P999.RTFM62                                
        ( RUBRIQUE ASC                                                          
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM641 ON P999.RTFM64                                
        ( STEAPP ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM650G ON P999.RTFM65                               
        ( CMASQUEC ASC                                                          
        , CMASQUES ASC                                                          
        , CMASQUER ASC                                                          
        , CMASQUEE ASC                                                          
        , CMASQUEA ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM691 ON P999.RTFM69                                
        ( COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM730G ON P999.RTFM73                               
        ( CNOMPROG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM801G ON P999.RTFM80                               
        ( CNATOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM811G ON P999.RTFM81                               
        ( CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM821G ON P999.RTFM82                               
        ( CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM831G ON P999.RTFM83                               
        ( CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM841G ON P999.RTFM84                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CTVA ASC                                                              
        , CGEO ASC                                                              
        , WGROUPE ASC                                                           
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM851G ON P999.RTFM85                               
        ( CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM861G ON P999.RTFM86                               
        ( DEFFET ASC                                                            
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM871G ON P999.RTFM87                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM881G ON P999.RTFM88                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM891G ON P999.RTFM89                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM901G ON P999.RTFM90                               
        ( CINTERFACE ASC                                                        
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM910G ON P999.RTFM91                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM921G ON P999.RTFM92                               
        ( CZONE ASC                                                             
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM930G ON P999.RTFM93                               
        ( CRITLIEU1 ASC                                                         
        , CRITLIEU2 ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM940G ON P999.RTFM94                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM971 ON P999.RTFM97                                
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CTVA ASC                                                              
        , CGEO ASC                                                              
        , WGROUPE ASC                                                           
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM981 ON P999.RTFM98                                
        ( DEFFET ASC                                                            
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFM991G ON P999.RTFM99                               
        ( CPTSAP ASC                                                            
        , WSTEAPP ASC                                                           
        , CPTGCT ASC                                                            
        , RUBRGCT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX000G ON P999.RTFX00                               
        ( NOECS ASC                                                             
        , DATEFF DESC                                                           
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , WFX00 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX010G ON P999.RTFX01                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX050G ON P999.RTFX05                               
        ( NJRN ASC                                                              
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX100G ON P999.RTFX10                               
        ( COMPTEGL ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX110G ON P999.RTFX11                               
        ( CZONE ASC                                                             
        , ZONEGL ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX150G ON P999.RTFX15                               
        ( WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX200G ON P999.RTFX20                               
        ( CTYPSTAT ASC                                                          
        , NDEPTLMELA ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX300G ON P999.RTFX30                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX350G ON P999.RTFX35                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX400G ON P999.RTFX40                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX450G ON P999.RTFX45                               
        ( CODEAPPLI ASC                                                         
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
        , DCREAT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXFX500G ON P999.RTFX50                               
        ( DATTRANS ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CTYPTRANS ASC                                                         
        , CAPPRET ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXGA010G ON P999.RTGA01                               
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXGA100G ON P999.RTGA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXGA710G ON P999.RTGA71                               
        ( CTABLE ASC                                                            
        , CSTABLE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXGA990G ON P999.RTGA99                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXSG010G ON P999.RTSG01                               
        ( RUBPAIE ASC                                                           
        , CONTRAT ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXSG020G ON P999.RTSG02                               
        ( SECPAIE ASC                                                           
        , CSECTION ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P999.RXSG030G ON P999.RTSG03                               
        ( COMPTE ASC                                                            
        , NETABADM ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
