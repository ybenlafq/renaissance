      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
           03  COMM-GS75-SUITE    REDEFINES COMM-GS75-LIBRE.                    
               05  COMM-GS75-ITEM            PIC S9(3) COMP-3.                  
               05  COMM-GS75-POS-MAX         PIC S9(3) COMP-3.                  
               05  COMM-GS75-ATTRIBUTS       OCCURS 13 TIMES.                   
                  10  COMM-GS75-LIGNE        OCCURS 7  TIMES.                   
                      15 COMM-GS75-ATTRIA    PIC X.                             
                      15 COMM-GS75-ATTRIC    PIC X.                             
                      15 COMM-GS75-ATTRIH    PIC X.                             
               05  COMM-GS76-COPER           PIC X(10).                         
               05  COMM-GS76-LOPER           PIC X(20).                         
               05  COMM-GS76-NSOCORI         PIC X(03).                         
               05  COMM-GS76-NLIEUOR         PIC X(03).                         
               05  COMM-GS76-CLIEUO          PIC X(01).                         
               05  COMM-GS76-NSSLORI         PIC X(03).                         
               05  COMM-GS76-NSOCDES         PIC X(03).                         
               05  COMM-GS76-NLIEUDE         PIC X(03).                         
               05  COMM-GS76-CLIEUD          PIC X(01).                         
               05  COMM-GS76-NSSLDES         PIC X(03).                         
               05  COMM-GS76-NLIGNE          PIC S9(3) COMP-3.                  
               05  COMM-GS76-PRESENCE-GS06   PIC X.                             
               05  COMM-GS76-APPLI.                                             
                   10  COMM-GS76-PAGE        PIC 9(03).                         
                   10  COMM-GS76-PAGEMAX     PIC 9(03).                         
                   10  COMM-GS76-MODIF-DONNEE PIC X.                            
                   10  COMM-GS76-ITEMAX      PIC 9(3).                          
                   10  COMM-GS76-NB-ITEM     PIC 9(3).                          
                   10  COMM-GS76-L-LIEUO     PIC X(3).                          
                   10  COMM-GS76-L-LIEUD     PIC X(3).                          
               05  FILLER                    PIC X(3342).                       
                                                                                
