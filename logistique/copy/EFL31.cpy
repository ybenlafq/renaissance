      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFL31   EFL31                                              00000020
      ***************************************************************** 00000030
       01   EFL31I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEYL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEYL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEYF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEYI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROAFFL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCPROAFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPROAFFF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCPROAFFI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROAFFL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLPROAFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPROAFFF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLPROAFFI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXCEPL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCEXCEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCEXCEPF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCEXCEPI  PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMILL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAMILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCFAMILF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMILI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT1L      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNDEPOT1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT1F      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNDEPOT1I      PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT2L      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNDEPOT2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT2F      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNDEPOT2I      PIC X(3).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT3L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNDEPOT3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT3F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNDEPOT3I      PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT4L      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNDEPOT4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT4F      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNDEPOT4I      PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT5L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MNDEPOT5L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT5F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNDEPOT5I      PIC X(3).                                  00000570
           02 MTABLEI OCCURS   15 TIMES .                               00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXCL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCEXCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCEXCF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCEXCI  PIC X.                                          00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCFAMI  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLFAMI  PIC X(20).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC1L  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MSOC1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC1F  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSOC1I  PIC X(3).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT1L  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MENT1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT1F  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MENT1I  PIC X(3).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC2L  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MSOC2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC2F  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MSOC2I  PIC X(3).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT2L  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MENT2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT2F  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MENT2I  PIC X(3).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC3L  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MSOC3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC3F  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MSOC3I  PIC X(3).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT3L  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MENT3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT3F  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MENT3I  PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC4L  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MSOC4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC4F  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MSOC4I  PIC X(3).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT4L  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MENT4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT4F  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MENT4I  PIC X(3).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC5L  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MSOC5L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC5F  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MSOC5I  PIC X(3).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT5L  COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MENT5L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT5F  PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MENT5I  PIC X(3).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MZONCMDI  PIC X(15).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MLIBERRI  PIC X(58).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCICSI    PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MNETNAMI  PIC X(8).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MSCREENI  PIC X(4).                                       00001340
      ***************************************************************** 00001350
      * SDF: EFL31   EFL31                                              00001360
      ***************************************************************** 00001370
       01   EFL31O REDEFINES EFL31I.                                    00001380
           02 FILLER    PIC X(12).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDATJOUA  PIC X.                                          00001410
           02 MDATJOUC  PIC X.                                          00001420
           02 MDATJOUP  PIC X.                                          00001430
           02 MDATJOUH  PIC X.                                          00001440
           02 MDATJOUV  PIC X.                                          00001450
           02 MDATJOUO  PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTIMJOUA  PIC X.                                          00001480
           02 MTIMJOUC  PIC X.                                          00001490
           02 MTIMJOUP  PIC X.                                          00001500
           02 MTIMJOUH  PIC X.                                          00001510
           02 MTIMJOUV  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MPAGEA    PIC X.                                          00001550
           02 MPAGEC    PIC X.                                          00001560
           02 MPAGEP    PIC X.                                          00001570
           02 MPAGEH    PIC X.                                          00001580
           02 MPAGEV    PIC X.                                          00001590
           02 MPAGEO    PIC X(3).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MPAGEYA   PIC X.                                          00001620
           02 MPAGEYC   PIC X.                                          00001630
           02 MPAGEYP   PIC X.                                          00001640
           02 MPAGEYH   PIC X.                                          00001650
           02 MPAGEYV   PIC X.                                          00001660
           02 MPAGEYO   PIC X(3).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MCPROAFFA      PIC X.                                     00001690
           02 MCPROAFFC PIC X.                                          00001700
           02 MCPROAFFP PIC X.                                          00001710
           02 MCPROAFFH PIC X.                                          00001720
           02 MCPROAFFV PIC X.                                          00001730
           02 MCPROAFFO      PIC X(5).                                  00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLPROAFFA      PIC X.                                     00001760
           02 MLPROAFFC PIC X.                                          00001770
           02 MLPROAFFP PIC X.                                          00001780
           02 MLPROAFFH PIC X.                                          00001790
           02 MLPROAFFV PIC X.                                          00001800
           02 MLPROAFFO      PIC X(20).                                 00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MCEXCEPA  PIC X.                                          00001830
           02 MCEXCEPC  PIC X.                                          00001840
           02 MCEXCEPP  PIC X.                                          00001850
           02 MCEXCEPH  PIC X.                                          00001860
           02 MCEXCEPV  PIC X.                                          00001870
           02 MCEXCEPO  PIC X.                                          00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MCFAMILA  PIC X.                                          00001900
           02 MCFAMILC  PIC X.                                          00001910
           02 MCFAMILP  PIC X.                                          00001920
           02 MCFAMILH  PIC X.                                          00001930
           02 MCFAMILV  PIC X.                                          00001940
           02 MCFAMILO  PIC X(5).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MNDEPOT1A      PIC X.                                     00001970
           02 MNDEPOT1C PIC X.                                          00001980
           02 MNDEPOT1P PIC X.                                          00001990
           02 MNDEPOT1H PIC X.                                          00002000
           02 MNDEPOT1V PIC X.                                          00002010
           02 MNDEPOT1O      PIC X(3).                                  00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MNDEPOT2A      PIC X.                                     00002040
           02 MNDEPOT2C PIC X.                                          00002050
           02 MNDEPOT2P PIC X.                                          00002060
           02 MNDEPOT2H PIC X.                                          00002070
           02 MNDEPOT2V PIC X.                                          00002080
           02 MNDEPOT2O      PIC X(3).                                  00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MNDEPOT3A      PIC X.                                     00002110
           02 MNDEPOT3C PIC X.                                          00002120
           02 MNDEPOT3P PIC X.                                          00002130
           02 MNDEPOT3H PIC X.                                          00002140
           02 MNDEPOT3V PIC X.                                          00002150
           02 MNDEPOT3O      PIC X(3).                                  00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MNDEPOT4A      PIC X.                                     00002180
           02 MNDEPOT4C PIC X.                                          00002190
           02 MNDEPOT4P PIC X.                                          00002200
           02 MNDEPOT4H PIC X.                                          00002210
           02 MNDEPOT4V PIC X.                                          00002220
           02 MNDEPOT4O      PIC X(3).                                  00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MNDEPOT5A      PIC X.                                     00002250
           02 MNDEPOT5C PIC X.                                          00002260
           02 MNDEPOT5P PIC X.                                          00002270
           02 MNDEPOT5H PIC X.                                          00002280
           02 MNDEPOT5V PIC X.                                          00002290
           02 MNDEPOT5O      PIC X(3).                                  00002300
           02 MTABLEO OCCURS   15 TIMES .                               00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MCEXCA  PIC X.                                          00002330
             03 MCEXCC  PIC X.                                          00002340
             03 MCEXCP  PIC X.                                          00002350
             03 MCEXCH  PIC X.                                          00002360
             03 MCEXCV  PIC X.                                          00002370
             03 MCEXCO  PIC X.                                          00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MCFAMA  PIC X.                                          00002400
             03 MCFAMC  PIC X.                                          00002410
             03 MCFAMP  PIC X.                                          00002420
             03 MCFAMH  PIC X.                                          00002430
             03 MCFAMV  PIC X.                                          00002440
             03 MCFAMO  PIC X(5).                                       00002450
             03 FILLER       PIC X(2).                                  00002460
             03 MLFAMA  PIC X.                                          00002470
             03 MLFAMC  PIC X.                                          00002480
             03 MLFAMP  PIC X.                                          00002490
             03 MLFAMH  PIC X.                                          00002500
             03 MLFAMV  PIC X.                                          00002510
             03 MLFAMO  PIC X(20).                                      00002520
             03 FILLER       PIC X(2).                                  00002530
             03 MSOC1A  PIC X.                                          00002540
             03 MSOC1C  PIC X.                                          00002550
             03 MSOC1P  PIC X.                                          00002560
             03 MSOC1H  PIC X.                                          00002570
             03 MSOC1V  PIC X.                                          00002580
             03 MSOC1O  PIC X(3).                                       00002590
             03 FILLER       PIC X(2).                                  00002600
             03 MENT1A  PIC X.                                          00002610
             03 MENT1C  PIC X.                                          00002620
             03 MENT1P  PIC X.                                          00002630
             03 MENT1H  PIC X.                                          00002640
             03 MENT1V  PIC X.                                          00002650
             03 MENT1O  PIC X(3).                                       00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MSOC2A  PIC X.                                          00002680
             03 MSOC2C  PIC X.                                          00002690
             03 MSOC2P  PIC X.                                          00002700
             03 MSOC2H  PIC X.                                          00002710
             03 MSOC2V  PIC X.                                          00002720
             03 MSOC2O  PIC X(3).                                       00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MENT2A  PIC X.                                          00002750
             03 MENT2C  PIC X.                                          00002760
             03 MENT2P  PIC X.                                          00002770
             03 MENT2H  PIC X.                                          00002780
             03 MENT2V  PIC X.                                          00002790
             03 MENT2O  PIC X(3).                                       00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MSOC3A  PIC X.                                          00002820
             03 MSOC3C  PIC X.                                          00002830
             03 MSOC3P  PIC X.                                          00002840
             03 MSOC3H  PIC X.                                          00002850
             03 MSOC3V  PIC X.                                          00002860
             03 MSOC3O  PIC X(3).                                       00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MENT3A  PIC X.                                          00002890
             03 MENT3C  PIC X.                                          00002900
             03 MENT3P  PIC X.                                          00002910
             03 MENT3H  PIC X.                                          00002920
             03 MENT3V  PIC X.                                          00002930
             03 MENT3O  PIC X(3).                                       00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MSOC4A  PIC X.                                          00002960
             03 MSOC4C  PIC X.                                          00002970
             03 MSOC4P  PIC X.                                          00002980
             03 MSOC4H  PIC X.                                          00002990
             03 MSOC4V  PIC X.                                          00003000
             03 MSOC4O  PIC X(3).                                       00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MENT4A  PIC X.                                          00003030
             03 MENT4C  PIC X.                                          00003040
             03 MENT4P  PIC X.                                          00003050
             03 MENT4H  PIC X.                                          00003060
             03 MENT4V  PIC X.                                          00003070
             03 MENT4O  PIC X(3).                                       00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MSOC5A  PIC X.                                          00003100
             03 MSOC5C  PIC X.                                          00003110
             03 MSOC5P  PIC X.                                          00003120
             03 MSOC5H  PIC X.                                          00003130
             03 MSOC5V  PIC X.                                          00003140
             03 MSOC5O  PIC X(3).                                       00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MENT5A  PIC X.                                          00003170
             03 MENT5C  PIC X.                                          00003180
             03 MENT5P  PIC X.                                          00003190
             03 MENT5H  PIC X.                                          00003200
             03 MENT5V  PIC X.                                          00003210
             03 MENT5O  PIC X(3).                                       00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MZONCMDA  PIC X.                                          00003240
           02 MZONCMDC  PIC X.                                          00003250
           02 MZONCMDP  PIC X.                                          00003260
           02 MZONCMDH  PIC X.                                          00003270
           02 MZONCMDV  PIC X.                                          00003280
           02 MZONCMDO  PIC X(15).                                      00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLIBERRA  PIC X.                                          00003310
           02 MLIBERRC  PIC X.                                          00003320
           02 MLIBERRP  PIC X.                                          00003330
           02 MLIBERRH  PIC X.                                          00003340
           02 MLIBERRV  PIC X.                                          00003350
           02 MLIBERRO  PIC X(58).                                      00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCODTRAA  PIC X.                                          00003380
           02 MCODTRAC  PIC X.                                          00003390
           02 MCODTRAP  PIC X.                                          00003400
           02 MCODTRAH  PIC X.                                          00003410
           02 MCODTRAV  PIC X.                                          00003420
           02 MCODTRAO  PIC X(4).                                       00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCICSA    PIC X.                                          00003450
           02 MCICSC    PIC X.                                          00003460
           02 MCICSP    PIC X.                                          00003470
           02 MCICSH    PIC X.                                          00003480
           02 MCICSV    PIC X.                                          00003490
           02 MCICSO    PIC X(5).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MNETNAMA  PIC X.                                          00003520
           02 MNETNAMC  PIC X.                                          00003530
           02 MNETNAMP  PIC X.                                          00003540
           02 MNETNAMH  PIC X.                                          00003550
           02 MNETNAMV  PIC X.                                          00003560
           02 MNETNAMO  PIC X(8).                                       00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MSCREENA  PIC X.                                          00003590
           02 MSCREENC  PIC X.                                          00003600
           02 MSCREENP  PIC X.                                          00003610
           02 MSCREENH  PIC X.                                          00003620
           02 MSCREENV  PIC X.                                          00003630
           02 MSCREENO  PIC X(4).                                       00003640
                                                                                
