      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS15   EGS15                                              00000020
      ***************************************************************** 00000030
       01   EGS15I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHHMML    COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MHHMML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MHHMMF    PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MHHMMI    PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCODICI   PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEMUTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MQTEMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTEMUTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MQTEMUTI  PIC 9(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATMUTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDATMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATMUTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDATMUTI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(18).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNSOCDEPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPOTF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNSOCDEPOTI    PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNDEPOTI  PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTODEPL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MSTODEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTODEPF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSTODEPI  PIC 9(4).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCMARQI   PIC X(18).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEFOURL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MCDEFOURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEFOURF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCDEFOURI      PIC 9(4).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFOURL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MDATFOURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATFOURF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDATFOURI      PIC X(5).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLREFI    PIC X(18).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCDEPL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MLSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSOCDEPF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLSOCDEPI      PIC X(12).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPO1L    COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MNSOCDEPO1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPO1F    PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNSOCDEPO1I    PIC X(3).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT1L      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MNDEPOT1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT1F      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNDEPOT1I      PIC X(3).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTODEP1L      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MSTODEP1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSTODEP1F      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSTODEP1I      PIC 9(4).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSTATUTI  PIC X(3).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCDEFOURL     COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MLCDEFOURL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCDEFOURF     PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLCDEFOURI     PIC X(21).                                 00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEFOUR1L     COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MCDEFOUR1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCDEFOUR1F     PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCDEFOUR1I     PIC 9(4).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFOUR1L     COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MDATFOUR1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDATFOUR1F     PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MDATFOUR1I     PIC X(5).                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MNSOCI    PIC X(3).                                       00001010
           02 FILLER  OCCURS   11 TIMES .                               00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAG1L      COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MNMAG1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNMAG1F      PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNMAG1I      PIC X(3).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBMAG1L    COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MLIBMAG1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBMAG1F    PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MLIBMAG1I    PIC X(20).                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOMAG1L    COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MSTOMAG1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSTOMAG1F    PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MSTOMAG1I    PIC 9(5).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAG2L      COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MNMAG2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNMAG2F      PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MNMAG2I      PIC X(3).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBMAG2L    COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MLIBMAG2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBMAG2F    PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MLIBMAG2I    PIC X(20).                                 00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOMAG2L    COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MSTOMAG2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSTOMAG2F    PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MSTOMAG2I    PIC 9(5).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MZONCMDI  PIC X(11).                                      00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLIBERRI  PIC X(58).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCODTRAI  PIC X(4).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCICSI    PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MNETNAMI  PIC X(8).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MSCREENI  PIC X(4).                                       00001500
      ***************************************************************** 00001510
      * SDF: EGS15   EGS15                                              00001520
      ***************************************************************** 00001530
       01   EGS15O REDEFINES EGS15I.                                    00001540
           02 FILLER    PIC X(12).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDATJOUA  PIC X.                                          00001570
           02 MDATJOUC  PIC X.                                          00001580
           02 MDATJOUP  PIC X.                                          00001590
           02 MDATJOUH  PIC X.                                          00001600
           02 MDATJOUV  PIC X.                                          00001610
           02 MDATJOUO  PIC X(10).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MHHMMA    PIC X.                                          00001640
           02 MHHMMC    PIC X.                                          00001650
           02 MHHMMP    PIC X.                                          00001660
           02 MHHMMH    PIC X.                                          00001670
           02 MHHMMV    PIC X.                                          00001680
           02 MHHMMO    PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MPAGEA    PIC X.                                          00001710
           02 MPAGEC    PIC X.                                          00001720
           02 MPAGEP    PIC X.                                          00001730
           02 MPAGEH    PIC X.                                          00001740
           02 MPAGEV    PIC X.                                          00001750
           02 MPAGEO    PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCODICA   PIC X.                                          00001780
           02 MCODICC   PIC X.                                          00001790
           02 MCODICP   PIC X.                                          00001800
           02 MCODICH   PIC X.                                          00001810
           02 MCODICV   PIC X.                                          00001820
           02 MCODICO   PIC X(7).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNLIEUA   PIC X.                                          00001850
           02 MNLIEUC   PIC X.                                          00001860
           02 MNLIEUP   PIC X.                                          00001870
           02 MNLIEUH   PIC X.                                          00001880
           02 MNLIEUV   PIC X.                                          00001890
           02 MNLIEUO   PIC X(3).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MQTEMUTA  PIC X.                                          00001920
           02 MQTEMUTC  PIC X.                                          00001930
           02 MQTEMUTP  PIC X.                                          00001940
           02 MQTEMUTH  PIC X.                                          00001950
           02 MQTEMUTV  PIC X.                                          00001960
           02 MQTEMUTO  PIC ZZZ.                                        00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MDATMUTA  PIC X.                                          00001990
           02 MDATMUTC  PIC X.                                          00002000
           02 MDATMUTP  PIC X.                                          00002010
           02 MDATMUTH  PIC X.                                          00002020
           02 MDATMUTV  PIC X.                                          00002030
           02 MDATMUTO  PIC X(5).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCFAMA    PIC X.                                          00002060
           02 MCFAMC    PIC X.                                          00002070
           02 MCFAMP    PIC X.                                          00002080
           02 MCFAMH    PIC X.                                          00002090
           02 MCFAMV    PIC X.                                          00002100
           02 MCFAMO    PIC X(18).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNSOCDEPOTA    PIC X.                                     00002130
           02 MNSOCDEPOTC    PIC X.                                     00002140
           02 MNSOCDEPOTP    PIC X.                                     00002150
           02 MNSOCDEPOTH    PIC X.                                     00002160
           02 MNSOCDEPOTV    PIC X.                                     00002170
           02 MNSOCDEPOTO    PIC X(3).                                  00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNDEPOTA  PIC X.                                          00002200
           02 MNDEPOTC  PIC X.                                          00002210
           02 MNDEPOTP  PIC X.                                          00002220
           02 MNDEPOTH  PIC X.                                          00002230
           02 MNDEPOTV  PIC X.                                          00002240
           02 MNDEPOTO  PIC X(3).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSTODEPA  PIC X.                                          00002270
           02 MSTODEPC  PIC X.                                          00002280
           02 MSTODEPP  PIC X.                                          00002290
           02 MSTODEPH  PIC X.                                          00002300
           02 MSTODEPV  PIC X.                                          00002310
           02 MSTODEPO  PIC -(4).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCMARQA   PIC X.                                          00002340
           02 MCMARQC   PIC X.                                          00002350
           02 MCMARQP   PIC X.                                          00002360
           02 MCMARQH   PIC X.                                          00002370
           02 MCMARQV   PIC X.                                          00002380
           02 MCMARQO   PIC X(18).                                      00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MCDEFOURA      PIC X.                                     00002410
           02 MCDEFOURC PIC X.                                          00002420
           02 MCDEFOURP PIC X.                                          00002430
           02 MCDEFOURH PIC X.                                          00002440
           02 MCDEFOURV PIC X.                                          00002450
           02 MCDEFOURO      PIC ZZZZ.                                  00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MDATFOURA      PIC X.                                     00002480
           02 MDATFOURC PIC X.                                          00002490
           02 MDATFOURP PIC X.                                          00002500
           02 MDATFOURH PIC X.                                          00002510
           02 MDATFOURV PIC X.                                          00002520
           02 MDATFOURO      PIC X(5).                                  00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MLREFA    PIC X.                                          00002550
           02 MLREFC    PIC X.                                          00002560
           02 MLREFP    PIC X.                                          00002570
           02 MLREFH    PIC X.                                          00002580
           02 MLREFV    PIC X.                                          00002590
           02 MLREFO    PIC X(18).                                      00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MLSOCDEPA      PIC X.                                     00002620
           02 MLSOCDEPC PIC X.                                          00002630
           02 MLSOCDEPP PIC X.                                          00002640
           02 MLSOCDEPH PIC X.                                          00002650
           02 MLSOCDEPV PIC X.                                          00002660
           02 MLSOCDEPO      PIC X(12).                                 00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MNSOCDEPO1A    PIC X.                                     00002690
           02 MNSOCDEPO1C    PIC X.                                     00002700
           02 MNSOCDEPO1P    PIC X.                                     00002710
           02 MNSOCDEPO1H    PIC X.                                     00002720
           02 MNSOCDEPO1V    PIC X.                                     00002730
           02 MNSOCDEPO1O    PIC X(3).                                  00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNDEPOT1A      PIC X.                                     00002760
           02 MNDEPOT1C PIC X.                                          00002770
           02 MNDEPOT1P PIC X.                                          00002780
           02 MNDEPOT1H PIC X.                                          00002790
           02 MNDEPOT1V PIC X.                                          00002800
           02 MNDEPOT1O      PIC X(3).                                  00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MSTODEP1A      PIC X.                                     00002830
           02 MSTODEP1C PIC X.                                          00002840
           02 MSTODEP1P PIC X.                                          00002850
           02 MSTODEP1H PIC X.                                          00002860
           02 MSTODEP1V PIC X.                                          00002870
           02 MSTODEP1O      PIC -(4).                                  00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MSTATUTA  PIC X.                                          00002900
           02 MSTATUTC  PIC X.                                          00002910
           02 MSTATUTP  PIC X.                                          00002920
           02 MSTATUTH  PIC X.                                          00002930
           02 MSTATUTV  PIC X.                                          00002940
           02 MSTATUTO  PIC X(3).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MLCDEFOURA     PIC X.                                     00002970
           02 MLCDEFOURC     PIC X.                                     00002980
           02 MLCDEFOURP     PIC X.                                     00002990
           02 MLCDEFOURH     PIC X.                                     00003000
           02 MLCDEFOURV     PIC X.                                     00003010
           02 MLCDEFOURO     PIC X(21).                                 00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MCDEFOUR1A     PIC X.                                     00003040
           02 MCDEFOUR1C     PIC X.                                     00003050
           02 MCDEFOUR1P     PIC X.                                     00003060
           02 MCDEFOUR1H     PIC X.                                     00003070
           02 MCDEFOUR1V     PIC X.                                     00003080
           02 MCDEFOUR1O     PIC ZZZZ.                                  00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MDATFOUR1A     PIC X.                                     00003110
           02 MDATFOUR1C     PIC X.                                     00003120
           02 MDATFOUR1P     PIC X.                                     00003130
           02 MDATFOUR1H     PIC X.                                     00003140
           02 MDATFOUR1V     PIC X.                                     00003150
           02 MDATFOUR1O     PIC X(5).                                  00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MNSOCA    PIC X.                                          00003180
           02 MNSOCC    PIC X.                                          00003190
           02 MNSOCP    PIC X.                                          00003200
           02 MNSOCH    PIC X.                                          00003210
           02 MNSOCV    PIC X.                                          00003220
           02 MNSOCO    PIC X(3).                                       00003230
           02 FILLER  OCCURS   11 TIMES .                               00003240
             03 FILLER       PIC X(2).                                  00003250
             03 MNMAG1A      PIC X.                                     00003260
             03 MNMAG1C PIC X.                                          00003270
             03 MNMAG1P PIC X.                                          00003280
             03 MNMAG1H PIC X.                                          00003290
             03 MNMAG1V PIC X.                                          00003300
             03 MNMAG1O      PIC X(3).                                  00003310
             03 FILLER       PIC X(2).                                  00003320
             03 MLIBMAG1A    PIC X.                                     00003330
             03 MLIBMAG1C    PIC X.                                     00003340
             03 MLIBMAG1P    PIC X.                                     00003350
             03 MLIBMAG1H    PIC X.                                     00003360
             03 MLIBMAG1V    PIC X.                                     00003370
             03 MLIBMAG1O    PIC X(20).                                 00003380
             03 FILLER       PIC X(2).                                  00003390
             03 MSTOMAG1A    PIC X.                                     00003400
             03 MSTOMAG1C    PIC X.                                     00003410
             03 MSTOMAG1P    PIC X.                                     00003420
             03 MSTOMAG1H    PIC X.                                     00003430
             03 MSTOMAG1V    PIC X.                                     00003440
             03 MSTOMAG1O    PIC -(5).                                  00003450
             03 FILLER       PIC X(2).                                  00003460
             03 MNMAG2A      PIC X.                                     00003470
             03 MNMAG2C PIC X.                                          00003480
             03 MNMAG2P PIC X.                                          00003490
             03 MNMAG2H PIC X.                                          00003500
             03 MNMAG2V PIC X.                                          00003510
             03 MNMAG2O      PIC X(3).                                  00003520
             03 FILLER       PIC X(2).                                  00003530
             03 MLIBMAG2A    PIC X.                                     00003540
             03 MLIBMAG2C    PIC X.                                     00003550
             03 MLIBMAG2P    PIC X.                                     00003560
             03 MLIBMAG2H    PIC X.                                     00003570
             03 MLIBMAG2V    PIC X.                                     00003580
             03 MLIBMAG2O    PIC X(20).                                 00003590
             03 FILLER       PIC X(2).                                  00003600
             03 MSTOMAG2A    PIC X.                                     00003610
             03 MSTOMAG2C    PIC X.                                     00003620
             03 MSTOMAG2P    PIC X.                                     00003630
             03 MSTOMAG2H    PIC X.                                     00003640
             03 MSTOMAG2V    PIC X.                                     00003650
             03 MSTOMAG2O    PIC -(5).                                  00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MZONCMDA  PIC X.                                          00003680
           02 MZONCMDC  PIC X.                                          00003690
           02 MZONCMDP  PIC X.                                          00003700
           02 MZONCMDH  PIC X.                                          00003710
           02 MZONCMDV  PIC X.                                          00003720
           02 MZONCMDO  PIC X(11).                                      00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MLIBERRA  PIC X.                                          00003750
           02 MLIBERRC  PIC X.                                          00003760
           02 MLIBERRP  PIC X.                                          00003770
           02 MLIBERRH  PIC X.                                          00003780
           02 MLIBERRV  PIC X.                                          00003790
           02 MLIBERRO  PIC X(58).                                      00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MCODTRAA  PIC X.                                          00003820
           02 MCODTRAC  PIC X.                                          00003830
           02 MCODTRAP  PIC X.                                          00003840
           02 MCODTRAH  PIC X.                                          00003850
           02 MCODTRAV  PIC X.                                          00003860
           02 MCODTRAO  PIC X(4).                                       00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MCICSA    PIC X.                                          00003890
           02 MCICSC    PIC X.                                          00003900
           02 MCICSP    PIC X.                                          00003910
           02 MCICSH    PIC X.                                          00003920
           02 MCICSV    PIC X.                                          00003930
           02 MCICSO    PIC X(5).                                       00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MNETNAMA  PIC X.                                          00003960
           02 MNETNAMC  PIC X.                                          00003970
           02 MNETNAMP  PIC X.                                          00003980
           02 MNETNAMH  PIC X.                                          00003990
           02 MNETNAMV  PIC X.                                          00004000
           02 MNETNAMO  PIC X(8).                                       00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MSCREENA  PIC X.                                          00004030
           02 MSCREENC  PIC X.                                          00004040
           02 MSCREENP  PIC X.                                          00004050
           02 MSCREENH  PIC X.                                          00004060
           02 MSCREENV  PIC X.                                          00004070
           02 MSCREENO  PIC X(4).                                       00004080
                                                                                
