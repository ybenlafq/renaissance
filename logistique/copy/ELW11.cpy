      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SUIVI DES MUTATIONS LM7                                         00000020
      ***************************************************************** 00000030
       01   ELW11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDEPF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCDEPI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDDEBUTL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MSDDEBUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDDEBUTF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSDDEBUTI      PIC X(8).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDFINL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MSDFINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSDFINF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSDFINI   PIC X(8).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNSOCL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSNSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSNSOCF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSNSOCI   PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNLIEUL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MSNLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSNLIEUF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSNLIEUI  PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSELARTL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MSSELARTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSSELARTF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSSELARTI      PIC X(5).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNMUTATIONL   COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MSNMUTATIONL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MSNMUTATIONF   PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSNMUTATIONI   PIC X(7).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSWMULTIL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MSWMULTIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSWMULTIF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSWMULTII      PIC X.                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDDSTOCKL     COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MSDDSTOCKL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSDDSTOCKF     PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSDDSTOCKI     PIC X(4).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDCHARGL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MSDCHARGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDCHARGF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSDCHARGI      PIC X(4).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSHCHARGL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MSHCHARGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSHCHARGF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSHCHARGI      PIC X(5).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDMUTATIONL   COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MSDMUTATIONL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MSDMUTATIONF   PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MSDMUTATIONI   PIC X(4).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDLM7L   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MSDLM7L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSDLM7F   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSDLM7I   PIC X(4).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDEXPEDL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MSDEXPEDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDEXPEDF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSDEXPEDI      PIC X(4).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDVALIDL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MSDVALIDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDVALIDF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSDVALIDI      PIC X(4).                                  00000890
           02 MLIGNEI OCCURS   12 TIMES .                               00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MNSOCI  PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MNLIEUI      PIC X(3).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELARTL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MSELARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELARTF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MSELARTI     PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTATIONL  COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MNMUTATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNMUTATIONF  PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNMUTATIONI  PIC X(7).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWMULTIL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MWMULTIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWMULTIF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MWMULTII     PIC X.                                     00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDSTOCKL    COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MDDSTOCKL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDDSTOCKF    PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MDDSTOCKI    PIC X(4).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCHARGL     COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MDCHARGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDCHARGF     PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MDCHARGI     PIC X(4).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHCHARGL     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MHCHARGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MHCHARGF     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MHCHARGI     PIC X(5).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMUTATIONL  COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MDMUTATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDMUTATIONF  PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MDMUTATIONI  PIC X(4).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDLM7L  COMP PIC S9(4).                                 00001270
      *--                                                                       
             03 MDLM7L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDLM7F  PIC X.                                          00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MDLM7I  PIC X(4).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQDEMANDEL   COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MQDEMANDEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQDEMANDEF   PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MQDEMANDEI   PIC X(5).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEXPEDL     COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MDEXPEDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEXPEDF     PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MDEXPEDI     PIC X(4).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPRELEVL    COMP PIC S9(4).                            00001390
      *--                                                                       
             03 MQPRELEVL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQPRELEVF    PIC X.                                     00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MQPRELEVI    PIC X(5).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDVALIDL     COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MDVALIDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDVALIDF     PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MDVALIDI     PIC X(4).                                  00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMUTEEL     COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MQMUTEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQMUTEEF     PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MQMUTEEI     PIC X(5).                                  00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MLIBERRI  PIC X(78).                                      00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MCODTRAI  PIC X(4).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MCICSI    PIC X(5).                                       00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MNETNAMI  PIC X(8).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MSCREENI  PIC X(4).                                       00001700
      ***************************************************************** 00001710
      * SUIVI DES MUTATIONS LM7                                         00001720
      ***************************************************************** 00001730
       01   ELW11O REDEFINES ELW11I.                                    00001740
           02 FILLER    PIC X(12).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MDATJOUA  PIC X.                                          00001770
           02 MDATJOUC  PIC X.                                          00001780
           02 MDATJOUP  PIC X.                                          00001790
           02 MDATJOUH  PIC X.                                          00001800
           02 MDATJOUV  PIC X.                                          00001810
           02 MDATJOUO  PIC X(10).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MTIMJOUA  PIC X.                                          00001840
           02 MTIMJOUC  PIC X.                                          00001850
           02 MTIMJOUP  PIC X.                                          00001860
           02 MTIMJOUH  PIC X.                                          00001870
           02 MTIMJOUV  PIC X.                                          00001880
           02 MTIMJOUO  PIC X(5).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MPAGEA    PIC X.                                          00001910
           02 MPAGEC    PIC X.                                          00001920
           02 MPAGEP    PIC X.                                          00001930
           02 MPAGEH    PIC X.                                          00001940
           02 MPAGEV    PIC X.                                          00001950
           02 MPAGEO    PIC X(4).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNPAGEA   PIC X.                                          00001980
           02 MNPAGEC   PIC X.                                          00001990
           02 MNPAGEP   PIC X.                                          00002000
           02 MNPAGEH   PIC X.                                          00002010
           02 MNPAGEV   PIC X.                                          00002020
           02 MNPAGEO   PIC X(4).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNSOCDEPA      PIC X.                                     00002050
           02 MNSOCDEPC PIC X.                                          00002060
           02 MNSOCDEPP PIC X.                                          00002070
           02 MNSOCDEPH PIC X.                                          00002080
           02 MNSOCDEPV PIC X.                                          00002090
           02 MNSOCDEPO      PIC X(3).                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MNDEPOTA  PIC X.                                          00002120
           02 MNDEPOTC  PIC X.                                          00002130
           02 MNDEPOTP  PIC X.                                          00002140
           02 MNDEPOTH  PIC X.                                          00002150
           02 MNDEPOTV  PIC X.                                          00002160
           02 MNDEPOTO  PIC X(3).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLLIEUA   PIC X.                                          00002190
           02 MLLIEUC   PIC X.                                          00002200
           02 MLLIEUP   PIC X.                                          00002210
           02 MLLIEUH   PIC X.                                          00002220
           02 MLLIEUV   PIC X.                                          00002230
           02 MLLIEUO   PIC X(20).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MSDDEBUTA      PIC X.                                     00002260
           02 MSDDEBUTC PIC X.                                          00002270
           02 MSDDEBUTP PIC X.                                          00002280
           02 MSDDEBUTH PIC X.                                          00002290
           02 MSDDEBUTV PIC X.                                          00002300
           02 MSDDEBUTO      PIC X(8).                                  00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MSDFINA   PIC X.                                          00002330
           02 MSDFINC   PIC X.                                          00002340
           02 MSDFINP   PIC X.                                          00002350
           02 MSDFINH   PIC X.                                          00002360
           02 MSDFINV   PIC X.                                          00002370
           02 MSDFINO   PIC X(8).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MSNSOCA   PIC X.                                          00002400
           02 MSNSOCC   PIC X.                                          00002410
           02 MSNSOCP   PIC X.                                          00002420
           02 MSNSOCH   PIC X.                                          00002430
           02 MSNSOCV   PIC X.                                          00002440
           02 MSNSOCO   PIC X(3).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MSNLIEUA  PIC X.                                          00002470
           02 MSNLIEUC  PIC X.                                          00002480
           02 MSNLIEUP  PIC X.                                          00002490
           02 MSNLIEUH  PIC X.                                          00002500
           02 MSNLIEUV  PIC X.                                          00002510
           02 MSNLIEUO  PIC X(3).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MSSELARTA      PIC X.                                     00002540
           02 MSSELARTC PIC X.                                          00002550
           02 MSSELARTP PIC X.                                          00002560
           02 MSSELARTH PIC X.                                          00002570
           02 MSSELARTV PIC X.                                          00002580
           02 MSSELARTO      PIC X(5).                                  00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MSNMUTATIONA   PIC X.                                     00002610
           02 MSNMUTATIONC   PIC X.                                     00002620
           02 MSNMUTATIONP   PIC X.                                     00002630
           02 MSNMUTATIONH   PIC X.                                     00002640
           02 MSNMUTATIONV   PIC X.                                     00002650
           02 MSNMUTATIONO   PIC X(7).                                  00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MSWMULTIA      PIC X.                                     00002680
           02 MSWMULTIC PIC X.                                          00002690
           02 MSWMULTIP PIC X.                                          00002700
           02 MSWMULTIH PIC X.                                          00002710
           02 MSWMULTIV PIC X.                                          00002720
           02 MSWMULTIO      PIC X.                                     00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MSDDSTOCKA     PIC X.                                     00002750
           02 MSDDSTOCKC     PIC X.                                     00002760
           02 MSDDSTOCKP     PIC X.                                     00002770
           02 MSDDSTOCKH     PIC X.                                     00002780
           02 MSDDSTOCKV     PIC X.                                     00002790
           02 MSDDSTOCKO     PIC X(4).                                  00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSDCHARGA      PIC X.                                     00002820
           02 MSDCHARGC PIC X.                                          00002830
           02 MSDCHARGP PIC X.                                          00002840
           02 MSDCHARGH PIC X.                                          00002850
           02 MSDCHARGV PIC X.                                          00002860
           02 MSDCHARGO      PIC X(4).                                  00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MSHCHARGA      PIC X.                                     00002890
           02 MSHCHARGC PIC X.                                          00002900
           02 MSHCHARGP PIC X.                                          00002910
           02 MSHCHARGH PIC X.                                          00002920
           02 MSHCHARGV PIC X.                                          00002930
           02 MSHCHARGO      PIC X(5).                                  00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MSDMUTATIONA   PIC X.                                     00002960
           02 MSDMUTATIONC   PIC X.                                     00002970
           02 MSDMUTATIONP   PIC X.                                     00002980
           02 MSDMUTATIONH   PIC X.                                     00002990
           02 MSDMUTATIONV   PIC X.                                     00003000
           02 MSDMUTATIONO   PIC X(4).                                  00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MSDLM7A   PIC X.                                          00003030
           02 MSDLM7C   PIC X.                                          00003040
           02 MSDLM7P   PIC X.                                          00003050
           02 MSDLM7H   PIC X.                                          00003060
           02 MSDLM7V   PIC X.                                          00003070
           02 MSDLM7O   PIC X(4).                                       00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MSDEXPEDA      PIC X.                                     00003100
           02 MSDEXPEDC PIC X.                                          00003110
           02 MSDEXPEDP PIC X.                                          00003120
           02 MSDEXPEDH PIC X.                                          00003130
           02 MSDEXPEDV PIC X.                                          00003140
           02 MSDEXPEDO      PIC X(4).                                  00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MSDVALIDA      PIC X.                                     00003170
           02 MSDVALIDC PIC X.                                          00003180
           02 MSDVALIDP PIC X.                                          00003190
           02 MSDVALIDH PIC X.                                          00003200
           02 MSDVALIDV PIC X.                                          00003210
           02 MSDVALIDO      PIC X(4).                                  00003220
           02 MLIGNEO OCCURS   12 TIMES .                               00003230
             03 FILLER       PIC X(2).                                  00003240
             03 MNSOCA  PIC X.                                          00003250
             03 MNSOCC  PIC X.                                          00003260
             03 MNSOCP  PIC X.                                          00003270
             03 MNSOCH  PIC X.                                          00003280
             03 MNSOCV  PIC X.                                          00003290
             03 MNSOCO  PIC X(3).                                       00003300
             03 FILLER       PIC X(2).                                  00003310
             03 MNLIEUA      PIC X.                                     00003320
             03 MNLIEUC PIC X.                                          00003330
             03 MNLIEUP PIC X.                                          00003340
             03 MNLIEUH PIC X.                                          00003350
             03 MNLIEUV PIC X.                                          00003360
             03 MNLIEUO      PIC X(3).                                  00003370
             03 FILLER       PIC X(2).                                  00003380
             03 MSELARTA     PIC X.                                     00003390
             03 MSELARTC     PIC X.                                     00003400
             03 MSELARTP     PIC X.                                     00003410
             03 MSELARTH     PIC X.                                     00003420
             03 MSELARTV     PIC X.                                     00003430
             03 MSELARTO     PIC X(5).                                  00003440
             03 FILLER       PIC X(2).                                  00003450
             03 MNMUTATIONA  PIC X.                                     00003460
             03 MNMUTATIONC  PIC X.                                     00003470
             03 MNMUTATIONP  PIC X.                                     00003480
             03 MNMUTATIONH  PIC X.                                     00003490
             03 MNMUTATIONV  PIC X.                                     00003500
             03 MNMUTATIONO  PIC X(7).                                  00003510
             03 FILLER       PIC X(2).                                  00003520
             03 MWMULTIA     PIC X.                                     00003530
             03 MWMULTIC     PIC X.                                     00003540
             03 MWMULTIP     PIC X.                                     00003550
             03 MWMULTIH     PIC X.                                     00003560
             03 MWMULTIV     PIC X.                                     00003570
             03 MWMULTIO     PIC X.                                     00003580
             03 FILLER       PIC X(2).                                  00003590
             03 MDDSTOCKA    PIC X.                                     00003600
             03 MDDSTOCKC    PIC X.                                     00003610
             03 MDDSTOCKP    PIC X.                                     00003620
             03 MDDSTOCKH    PIC X.                                     00003630
             03 MDDSTOCKV    PIC X.                                     00003640
             03 MDDSTOCKO    PIC X(4).                                  00003650
             03 FILLER       PIC X(2).                                  00003660
             03 MDCHARGA     PIC X.                                     00003670
             03 MDCHARGC     PIC X.                                     00003680
             03 MDCHARGP     PIC X.                                     00003690
             03 MDCHARGH     PIC X.                                     00003700
             03 MDCHARGV     PIC X.                                     00003710
             03 MDCHARGO     PIC X(4).                                  00003720
             03 FILLER       PIC X(2).                                  00003730
             03 MHCHARGA     PIC X.                                     00003740
             03 MHCHARGC     PIC X.                                     00003750
             03 MHCHARGP     PIC X.                                     00003760
             03 MHCHARGH     PIC X.                                     00003770
             03 MHCHARGV     PIC X.                                     00003780
             03 MHCHARGO     PIC X(5).                                  00003790
             03 FILLER       PIC X(2).                                  00003800
             03 MDMUTATIONA  PIC X.                                     00003810
             03 MDMUTATIONC  PIC X.                                     00003820
             03 MDMUTATIONP  PIC X.                                     00003830
             03 MDMUTATIONH  PIC X.                                     00003840
             03 MDMUTATIONV  PIC X.                                     00003850
             03 MDMUTATIONO  PIC X(4).                                  00003860
             03 FILLER       PIC X(2).                                  00003870
             03 MDLM7A  PIC X.                                          00003880
             03 MDLM7C  PIC X.                                          00003890
             03 MDLM7P  PIC X.                                          00003900
             03 MDLM7H  PIC X.                                          00003910
             03 MDLM7V  PIC X.                                          00003920
             03 MDLM7O  PIC X(4).                                       00003930
             03 FILLER       PIC X(2).                                  00003940
             03 MQDEMANDEA   PIC X.                                     00003950
             03 MQDEMANDEC   PIC X.                                     00003960
             03 MQDEMANDEP   PIC X.                                     00003970
             03 MQDEMANDEH   PIC X.                                     00003980
             03 MQDEMANDEV   PIC X.                                     00003990
             03 MQDEMANDEO   PIC X(5).                                  00004000
             03 FILLER       PIC X(2).                                  00004010
             03 MDEXPEDA     PIC X.                                     00004020
             03 MDEXPEDC     PIC X.                                     00004030
             03 MDEXPEDP     PIC X.                                     00004040
             03 MDEXPEDH     PIC X.                                     00004050
             03 MDEXPEDV     PIC X.                                     00004060
             03 MDEXPEDO     PIC X(4).                                  00004070
             03 FILLER       PIC X(2).                                  00004080
             03 MQPRELEVA    PIC X.                                     00004090
             03 MQPRELEVC    PIC X.                                     00004100
             03 MQPRELEVP    PIC X.                                     00004110
             03 MQPRELEVH    PIC X.                                     00004120
             03 MQPRELEVV    PIC X.                                     00004130
             03 MQPRELEVO    PIC X(5).                                  00004140
             03 FILLER       PIC X(2).                                  00004150
             03 MDVALIDA     PIC X.                                     00004160
             03 MDVALIDC     PIC X.                                     00004170
             03 MDVALIDP     PIC X.                                     00004180
             03 MDVALIDH     PIC X.                                     00004190
             03 MDVALIDV     PIC X.                                     00004200
             03 MDVALIDO     PIC X(4).                                  00004210
             03 FILLER       PIC X(2).                                  00004220
             03 MQMUTEEA     PIC X.                                     00004230
             03 MQMUTEEC     PIC X.                                     00004240
             03 MQMUTEEP     PIC X.                                     00004250
             03 MQMUTEEH     PIC X.                                     00004260
             03 MQMUTEEV     PIC X.                                     00004270
             03 MQMUTEEO     PIC X(5).                                  00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MLIBERRA  PIC X.                                          00004300
           02 MLIBERRC  PIC X.                                          00004310
           02 MLIBERRP  PIC X.                                          00004320
           02 MLIBERRH  PIC X.                                          00004330
           02 MLIBERRV  PIC X.                                          00004340
           02 MLIBERRO  PIC X(78).                                      00004350
           02 FILLER    PIC X(2).                                       00004360
           02 MCODTRAA  PIC X.                                          00004370
           02 MCODTRAC  PIC X.                                          00004380
           02 MCODTRAP  PIC X.                                          00004390
           02 MCODTRAH  PIC X.                                          00004400
           02 MCODTRAV  PIC X.                                          00004410
           02 MCODTRAO  PIC X(4).                                       00004420
           02 FILLER    PIC X(2).                                       00004430
           02 MCICSA    PIC X.                                          00004440
           02 MCICSC    PIC X.                                          00004450
           02 MCICSP    PIC X.                                          00004460
           02 MCICSH    PIC X.                                          00004470
           02 MCICSV    PIC X.                                          00004480
           02 MCICSO    PIC X(5).                                       00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MNETNAMA  PIC X.                                          00004510
           02 MNETNAMC  PIC X.                                          00004520
           02 MNETNAMP  PIC X.                                          00004530
           02 MNETNAMH  PIC X.                                          00004540
           02 MNETNAMV  PIC X.                                          00004550
           02 MNETNAMO  PIC X(8).                                       00004560
           02 FILLER    PIC X(2).                                       00004570
           02 MSCREENA  PIC X.                                          00004580
           02 MSCREENC  PIC X.                                          00004590
           02 MSCREENP  PIC X.                                          00004600
           02 MSCREENH  PIC X.                                          00004610
           02 MSCREENV  PIC X.                                          00004620
           02 MSCREENO  PIC X(4).                                       00004630
                                                                                
