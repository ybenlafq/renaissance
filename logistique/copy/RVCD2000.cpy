      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVCD2000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCD2000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVCD2000.                                                            
           02  CD20-NMAG                                                        
               PIC X(0003).                                                     
           02  CD20-QTCSEUIL                                                    
               PIC S9(1)V9(0002) COMP-3.                                        
           02  CD20-WSEUIL                                                      
               PIC X(0001).                                                     
           02  CD20-QTCMV                                                       
               PIC S9(1)V9(0002) COMP-3.                                        
           02  CD20-QTCRM                                                       
               PIC S9(1)V9(0002) COMP-3.                                        
           02  CD20-WPOLITIQUERF                                                
               PIC X(0001).                                                     
           02  CD20-QTCNBARTRF                                                  
               PIC S9(1)V9(0002) COMP-3.                                        
           02  CD20-WSTOCKMAX                                                   
               PIC X(0001).                                                     
           02  CD20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVCD2000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVCD2000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD20-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD20-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD20-QTCSEUIL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD20-QTCSEUIL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD20-WSEUIL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD20-WSEUIL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD20-QTCMV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD20-QTCMV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD20-QTCRM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD20-QTCRM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD20-WPOLITIQUERF-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD20-WPOLITIQUERF-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD20-QTCNBARTRF-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD20-QTCNBARTRF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD20-WSTOCKMAX-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD20-WSTOCKMAX-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
