      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD66   EGD66                                              00000020
      ***************************************************************** 00000030
       01   EGD66I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE JOUR TRAITEMENT                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE TRAITEMENT                                                00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * ZONE DE COMMANDE                                                00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MZONCMDI  PIC X(15).                                      00000200
      * ZONE FONCTION                                                   00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCCL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWFONCCF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCCI  PIC X(3).                                       00000250
      * NUMERO SOCIETE                                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCCL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNSOCCCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCCCF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNSOCCCI  PIC X(3).                                       00000300
      * NUMERO DE LIEU                                                  00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUUL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNLIEUUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEUUF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNLIEUUI  PIC X(3).                                       00000350
      * DATE DU JOUR                                                    00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJOURRL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MDJOURRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDJOURRF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MDJOURRI  PIC X(8).                                       00000400
      * NUMERO DE RAFALE                                                00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRAFFFL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNRAFFFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRAFFFF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNRAFFFI  PIC X(3).                                       00000450
      * NUMERO DE SATELL                                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSATELLITEL   COMP PIC S9(4).                            00000470
      *--                                                                       
           02 MNSATELLITEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNSATELLITEF   PIC X.                                     00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MNSATELLITEI   PIC X(2).                                  00000500
      * ZONE MESSAGE                                                    00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MLIBERRI  PIC X(78).                                      00000550
      * CODE TRANSACTION                                                00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MCODTRAI  PIC X(4).                                       00000600
      * NOM DU CICS                                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSSSL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCICSSSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCICSSSF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCICSSSI  PIC X(5).                                       00000650
      * NOM LIGNE VTAM                                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNETNAMI  PIC X(8).                                       00000700
      * CODE TERMINAL                                                   00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MSCREENI  PIC X(4).                                       00000750
      ***************************************************************** 00000760
      * SDF: EGD66   EGD66                                              00000770
      ***************************************************************** 00000780
       01   EGD66O REDEFINES EGD66I.                                    00000790
           02 FILLER    PIC X(12).                                      00000800
      * DATE JOUR TRAITEMENT                                            00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MDATJOUA  PIC X.                                          00000830
           02 MDATJOUC  PIC X.                                          00000840
           02 MDATJOUP  PIC X.                                          00000850
           02 MDATJOUH  PIC X.                                          00000860
           02 MDATJOUV  PIC X.                                          00000870
           02 MDATJOUO  PIC X(10).                                      00000880
      * HEURE TRAITEMENT                                                00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MTIMJOUA  PIC X.                                          00000910
           02 MTIMJOUC  PIC X.                                          00000920
           02 MTIMJOUP  PIC X.                                          00000930
           02 MTIMJOUH  PIC X.                                          00000940
           02 MTIMJOUV  PIC X.                                          00000950
           02 MTIMJOUO  PIC X(5).                                       00000960
      * ZONE DE COMMANDE                                                00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MZONCMDA  PIC X.                                          00000990
           02 MZONCMDC  PIC X.                                          00001000
           02 MZONCMDP  PIC X.                                          00001010
           02 MZONCMDH  PIC X.                                          00001020
           02 MZONCMDV  PIC X.                                          00001030
           02 MZONCMDO  PIC X(15).                                      00001040
      * ZONE FONCTION                                                   00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MWFONCCA  PIC X.                                          00001070
           02 MWFONCCC  PIC X.                                          00001080
           02 MWFONCCP  PIC X.                                          00001090
           02 MWFONCCH  PIC X.                                          00001100
           02 MWFONCCV  PIC X.                                          00001110
           02 MWFONCCO  PIC X(3).                                       00001120
      * NUMERO SOCIETE                                                  00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNSOCCCA  PIC X.                                          00001150
           02 MNSOCCCC  PIC X.                                          00001160
           02 MNSOCCCP  PIC X.                                          00001170
           02 MNSOCCCH  PIC X.                                          00001180
           02 MNSOCCCV  PIC X.                                          00001190
           02 MNSOCCCO  PIC X(3).                                       00001200
      * NUMERO DE LIEU                                                  00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MNLIEUUA  PIC X.                                          00001230
           02 MNLIEUUC  PIC X.                                          00001240
           02 MNLIEUUP  PIC X.                                          00001250
           02 MNLIEUUH  PIC X.                                          00001260
           02 MNLIEUUV  PIC X.                                          00001270
           02 MNLIEUUO  PIC X(3).                                       00001280
      * DATE DU JOUR                                                    00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MDJOURRA  PIC X.                                          00001310
           02 MDJOURRC  PIC X.                                          00001320
           02 MDJOURRP  PIC X.                                          00001330
           02 MDJOURRH  PIC X.                                          00001340
           02 MDJOURRV  PIC X.                                          00001350
           02 MDJOURRO  PIC X(8).                                       00001360
      * NUMERO DE RAFALE                                                00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNRAFFFA  PIC X.                                          00001390
           02 MNRAFFFC  PIC X.                                          00001400
           02 MNRAFFFP  PIC X.                                          00001410
           02 MNRAFFFH  PIC X.                                          00001420
           02 MNRAFFFV  PIC X.                                          00001430
           02 MNRAFFFO  PIC X(3).                                       00001440
      * NUMERO DE SATELL                                                00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNSATELLITEA   PIC X.                                     00001470
           02 MNSATELLITEC   PIC X.                                     00001480
           02 MNSATELLITEP   PIC X.                                     00001490
           02 MNSATELLITEH   PIC X.                                     00001500
           02 MNSATELLITEV   PIC X.                                     00001510
           02 MNSATELLITEO   PIC X(2).                                  00001520
      * ZONE MESSAGE                                                    00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MLIBERRA  PIC X.                                          00001550
           02 MLIBERRC  PIC X.                                          00001560
           02 MLIBERRP  PIC X.                                          00001570
           02 MLIBERRH  PIC X.                                          00001580
           02 MLIBERRV  PIC X.                                          00001590
           02 MLIBERRO  PIC X(78).                                      00001600
      * CODE TRANSACTION                                                00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCODTRAA  PIC X.                                          00001630
           02 MCODTRAC  PIC X.                                          00001640
           02 MCODTRAP  PIC X.                                          00001650
           02 MCODTRAH  PIC X.                                          00001660
           02 MCODTRAV  PIC X.                                          00001670
           02 MCODTRAO  PIC X(4).                                       00001680
      * NOM DU CICS                                                     00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCICSSSA  PIC X.                                          00001710
           02 MCICSSSC  PIC X.                                          00001720
           02 MCICSSSP  PIC X.                                          00001730
           02 MCICSSSH  PIC X.                                          00001740
           02 MCICSSSV  PIC X.                                          00001750
           02 MCICSSSO  PIC X(5).                                       00001760
      * NOM LIGNE VTAM                                                  00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MNETNAMA  PIC X.                                          00001790
           02 MNETNAMC  PIC X.                                          00001800
           02 MNETNAMP  PIC X.                                          00001810
           02 MNETNAMH  PIC X.                                          00001820
           02 MNETNAMV  PIC X.                                          00001830
           02 MNETNAMO  PIC X(8).                                       00001840
      * CODE TERMINAL                                                   00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MSCREENA  PIC X.                                          00001870
           02 MSCREENC  PIC X.                                          00001880
           02 MSCREENP  PIC X.                                          00001890
           02 MSCREENH  PIC X.                                          00001900
           02 MSCREENV  PIC X.                                          00001910
           02 MSCREENO  PIC X(4).                                       00001920
                                                                                
