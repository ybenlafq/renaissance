      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Efl32 Efl32                                                00000020
      ***************************************************************** 00000030
       01   EFL32I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPMAXL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPMAXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPMAXF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPMAXI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCIETEI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLIEUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIEUF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIEUI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPTRAITL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MTYPTRAITL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTYPTRAITF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPTRAITI     PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROAFF1L     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCPROAFF1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCPROAFF1F     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCPROAFF1I     PIC X(5).                                  00000370
           02 MTABLEI OCCURS   13 TIMES .                               00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCIETEL   COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNSOCIETEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCIETEF   PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNSOCIETEI   PIC X(3).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNLIEUI      PIC X(3).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPTRAITL  COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCTYPTRAITL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCTYPTRAITF  PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCTYPTRAITI  PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPROAFFL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCPROAFFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCPROAFFF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCPROAFFI    PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPROAFFL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLPROAFFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLPROAFFF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLPROAFFI    PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDEFFETI     PIC X(10).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(78).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * SDF: Efl32 Efl32                                                00000840
      ***************************************************************** 00000850
       01   EFL32O REDEFINES EFL32I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNPAGEA   PIC X.                                          00001030
           02 MNPAGEC   PIC X.                                          00001040
           02 MNPAGEP   PIC X.                                          00001050
           02 MNPAGEH   PIC X.                                          00001060
           02 MNPAGEV   PIC X.                                          00001070
           02 MNPAGEO   PIC ZZZ.                                        00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MNPMAXA   PIC X.                                          00001100
           02 MNPMAXC   PIC X.                                          00001110
           02 MNPMAXP   PIC X.                                          00001120
           02 MNPMAXH   PIC X.                                          00001130
           02 MNPMAXV   PIC X.                                          00001140
           02 MNPMAXO   PIC ZZZ.                                        00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MSOCIETEA      PIC X.                                     00001170
           02 MSOCIETEC PIC X.                                          00001180
           02 MSOCIETEP PIC X.                                          00001190
           02 MSOCIETEH PIC X.                                          00001200
           02 MSOCIETEV PIC X.                                          00001210
           02 MSOCIETEO      PIC X(3).                                  00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MLIEUA    PIC X.                                          00001240
           02 MLIEUC    PIC X.                                          00001250
           02 MLIEUP    PIC X.                                          00001260
           02 MLIEUH    PIC X.                                          00001270
           02 MLIEUV    PIC X.                                          00001280
           02 MLIEUO    PIC X(3).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MTYPTRAITA     PIC X.                                     00001310
           02 MTYPTRAITC     PIC X.                                     00001320
           02 MTYPTRAITP     PIC X.                                     00001330
           02 MTYPTRAITH     PIC X.                                     00001340
           02 MTYPTRAITV     PIC X.                                     00001350
           02 MTYPTRAITO     PIC X(5).                                  00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MCPROAFF1A     PIC X.                                     00001380
           02 MCPROAFF1C     PIC X.                                     00001390
           02 MCPROAFF1P     PIC X.                                     00001400
           02 MCPROAFF1H     PIC X.                                     00001410
           02 MCPROAFF1V     PIC X.                                     00001420
           02 MCPROAFF1O     PIC X(5).                                  00001430
           02 MTABLEO OCCURS   13 TIMES .                               00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MNSOCIETEA   PIC X.                                     00001460
             03 MNSOCIETEC   PIC X.                                     00001470
             03 MNSOCIETEP   PIC X.                                     00001480
             03 MNSOCIETEH   PIC X.                                     00001490
             03 MNSOCIETEV   PIC X.                                     00001500
             03 MNSOCIETEO   PIC X(3).                                  00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MNLIEUA      PIC X.                                     00001530
             03 MNLIEUC PIC X.                                          00001540
             03 MNLIEUP PIC X.                                          00001550
             03 MNLIEUH PIC X.                                          00001560
             03 MNLIEUV PIC X.                                          00001570
             03 MNLIEUO      PIC X(3).                                  00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MCTYPTRAITA  PIC X.                                     00001600
             03 MCTYPTRAITC  PIC X.                                     00001610
             03 MCTYPTRAITP  PIC X.                                     00001620
             03 MCTYPTRAITH  PIC X.                                     00001630
             03 MCTYPTRAITV  PIC X.                                     00001640
             03 MCTYPTRAITO  PIC X(5).                                  00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MCPROAFFA    PIC X.                                     00001670
             03 MCPROAFFC    PIC X.                                     00001680
             03 MCPROAFFP    PIC X.                                     00001690
             03 MCPROAFFH    PIC X.                                     00001700
             03 MCPROAFFV    PIC X.                                     00001710
             03 MCPROAFFO    PIC X(5).                                  00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MLPROAFFA    PIC X.                                     00001740
             03 MLPROAFFC    PIC X.                                     00001750
             03 MLPROAFFP    PIC X.                                     00001760
             03 MLPROAFFH    PIC X.                                     00001770
             03 MLPROAFFV    PIC X.                                     00001780
             03 MLPROAFFO    PIC X(20).                                 00001790
             03 FILLER       PIC X(2).                                  00001800
             03 MDEFFETA     PIC X.                                     00001810
             03 MDEFFETC     PIC X.                                     00001820
             03 MDEFFETP     PIC X.                                     00001830
             03 MDEFFETH     PIC X.                                     00001840
             03 MDEFFETV     PIC X.                                     00001850
             03 MDEFFETO     PIC X(10).                                 00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLIBERRA  PIC X.                                          00001880
           02 MLIBERRC  PIC X.                                          00001890
           02 MLIBERRP  PIC X.                                          00001900
           02 MLIBERRH  PIC X.                                          00001910
           02 MLIBERRV  PIC X.                                          00001920
           02 MLIBERRO  PIC X(78).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
