      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA570      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,06,BI,A,                          *        
      *                           18,01,BI,A,                          *        
      *                           19,03,BI,A,                          *        
      *                           22,03,BI,A,                          *        
      *                           25,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA570.                                                        
            05 NOMETAT-IVA570           PIC X(6) VALUE 'IVA570'.                
            05 RUPTURES-IVA570.                                                 
           10 IVA570-NSOC               PIC X(03).                      007  003
           10 IVA570-CSEQ               PIC X(02).                      010  002
           10 IVA570-NCONSO             PIC X(06).                      012  006
           10 IVA570-CTYPMVT            PIC X(01).                      018  001
           10 IVA570-NSOCMVT            PIC X(03).                      019  003
           10 IVA570-NLIEUMVT           PIC X(03).                      022  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA570-SEQUENCE           PIC S9(04) COMP.                025  002
      *--                                                                       
           10 IVA570-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA570.                                                   
           10 IVA570-CRAYON             PIC X(05).                      027  005
           10 IVA570-LCONSO             PIC X(20).                      032  020
           10 IVA570-LLIEU              PIC X(20).                      052  020
           10 IVA570-LTYPMVT            PIC X(26).                      072  026
           10 IVA570-PRECYCLENTREE      PIC S9(09)V9(6) COMP-3.         098  008
           10 IVA570-PRECYCLSORTIE      PIC S9(09)V9(6) COMP-3.         106  008
           10 IVA570-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         114  008
           10 IVA570-PSTOCKFINAL        PIC S9(09)V9(6) COMP-3.         122  008
           10 IVA570-PSTOCKINIT         PIC S9(09)V9(6) COMP-3.         130  008
           10 IVA570-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         138  008
           10 IVA570-QSTOCKENTREE       PIC S9(11)      COMP-3.         146  006
           10 IVA570-QSTOCKFINAL        PIC S9(11)      COMP-3.         152  006
           10 IVA570-QSTOCKINIT         PIC S9(11)      COMP-3.         158  006
           10 IVA570-QSTOCKSORTIE       PIC S9(11)      COMP-3.         164  006
            05 FILLER                      PIC X(343).                          
                                                                                
