      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU95   EMU95                                              00000020
      ***************************************************************** 00000030
       01   EMU95I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDEPLIL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MSOCDEPLIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSOCDEPLIF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCDEPLII     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPLIVL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDEPLIVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEPLIVF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDEPLIVI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEU1L  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEU1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEU1F  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEU1I  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDEPOTL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MSOCDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSOCDEPOTF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSOCDEPOTI     PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNDEPOTI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEU2L  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLLIEU2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEU2F  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLLIEU2I  PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELARTL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCSELARTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSELARTF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCSELARTI      PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMUTDXL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MIMUTDXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MIMUTDXF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MIMUTDXI  PIC X(2).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMUTDNL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MIMUTDNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MIMUTDNF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MIMUTDNI  PIC X(2).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLIGNESL     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNBLIGNESL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBLIGNESF     PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNBLIGNESI     PIC X(5).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMUTL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDMUTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDMUTF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDMUTI    PIC X(10).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPIECESL     COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNBPIECESL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBPIECESF     PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNBPIECESI     PIC X(5).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPQUOTAL     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MNBPQUOTAL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBPQUOTAF     PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNBPQUOTAI     PIC X(5).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTAUXNPL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MTAUXNPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTAUXNPF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MTAUXNPI  PIC X(4).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTDESTL     COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MNMUTDESTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNMUTDESTF     PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNMUTDESTI     PIC X(7).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVOLUMEL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MVOLUMEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MVOLUMEF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MVOLUMEI  PIC X(6).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDISPOL   COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MDISPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDISPOF   PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MDISPOI   PIC X(6).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MM3QUOTAL      COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MM3QUOTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MM3QUOTAF      PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MM3QUOTAI      PIC X(7).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTAUXM3L  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MTAUXM3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTAUXM3F  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MTAUXM3I  PIC X(4).                                       00000970
           02 MLIGNEI OCCURS   9 TIMES .                                00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDELIVL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MDDELIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDDELIVF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MDDELIVI     PIC X(8).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNMAGI  PIC X(3).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MNVENTEI     PIC X(7).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINDICEL     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MINDICEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MINDICEF     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MINDICEI     PIC X.                                     00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTL  COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MNMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMUTF  PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MNMUTI  PIC X(7).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MNCODICI     PIC X(7).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLIGNESL    COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MQLIGNESL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQLIGNESF    PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MQLIGNESI    PIC X(4).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPIECESL    COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MQPIECESL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQPIECESF    PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MQPIECESI    PIC X(5).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVOLUMEL    COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MQVOLUMEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQVOLUMEF    PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MQVOLUMEI    PIC X(6).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMESSAGEL    COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MMESSAGEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MMESSAGEF    PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MMESSAGEI    PIC X(20).                                 00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTIONL     COMP PIC S9(4).                            00001390
      *--                                                                       
             03 MACTIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MACTIONF     PIC X.                                     00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MACTIONI     PIC X.                                     00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMUTORIGL     COMP PIC S9(4).                            00001430
      *--                                                                       
           02 MDMUTORIGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDMUTORIGF     PIC X.                                     00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MDMUTORIGI     PIC X(10).                                 00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTLIGNEL     COMP PIC S9(4).                            00001470
      *--                                                                       
           02 MTOTLIGNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTOTLIGNEF     PIC X.                                     00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MTOTLIGNEI     PIC X(5).                                  00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPIECEL     COMP PIC S9(4).                            00001510
      *--                                                                       
           02 MTOTPIECEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTOTPIECEF     PIC X.                                     00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MTOTPIECEI     PIC X(5).                                  00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTVOLUML     COMP PIC S9(4).                            00001550
      *--                                                                       
           02 MTOTVOLUML COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTOTVOLUMF     PIC X.                                     00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MTOTVOLUMI     PIC X(6).                                  00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MZONCMDI  PIC X(15).                                      00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MLIBERRI  PIC X(58).                                      00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCODTRAI  PIC X(4).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MCICSI    PIC X(5).                                       00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MNETNAMI  PIC X(8).                                       00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001800
           02 FILLER    PIC X(4).                                       00001810
           02 MSCREENI  PIC X(4).                                       00001820
      ***************************************************************** 00001830
      * SDF: EMU95   EMU95                                              00001840
      ***************************************************************** 00001850
       01   EMU95O REDEFINES EMU95I.                                    00001860
           02 FILLER    PIC X(12).                                      00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MDATJOUA  PIC X.                                          00001890
           02 MDATJOUC  PIC X.                                          00001900
           02 MDATJOUP  PIC X.                                          00001910
           02 MDATJOUH  PIC X.                                          00001920
           02 MDATJOUV  PIC X.                                          00001930
           02 MDATJOUO  PIC X(10).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MTIMJOUA  PIC X.                                          00001960
           02 MTIMJOUC  PIC X.                                          00001970
           02 MTIMJOUP  PIC X.                                          00001980
           02 MTIMJOUH  PIC X.                                          00001990
           02 MTIMJOUV  PIC X.                                          00002000
           02 MTIMJOUO  PIC X(5).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MNPAGEA   PIC X.                                          00002030
           02 MNPAGEC   PIC X.                                          00002040
           02 MNPAGEP   PIC X.                                          00002050
           02 MNPAGEH   PIC X.                                          00002060
           02 MNPAGEV   PIC X.                                          00002070
           02 MNPAGEO   PIC ZZZ9.                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNBPA     PIC X.                                          00002100
           02 MNBPC     PIC X.                                          00002110
           02 MNBPP     PIC X.                                          00002120
           02 MNBPH     PIC X.                                          00002130
           02 MNBPV     PIC X.                                          00002140
           02 MNBPO     PIC X(4).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MSOCDEPLIA     PIC X.                                     00002170
           02 MSOCDEPLIC     PIC X.                                     00002180
           02 MSOCDEPLIP     PIC X.                                     00002190
           02 MSOCDEPLIH     PIC X.                                     00002200
           02 MSOCDEPLIV     PIC X.                                     00002210
           02 MSOCDEPLIO     PIC X(3).                                  00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MDEPLIVA  PIC X.                                          00002240
           02 MDEPLIVC  PIC X.                                          00002250
           02 MDEPLIVP  PIC X.                                          00002260
           02 MDEPLIVH  PIC X.                                          00002270
           02 MDEPLIVV  PIC X.                                          00002280
           02 MDEPLIVO  PIC X(3).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLLIEU1A  PIC X.                                          00002310
           02 MLLIEU1C  PIC X.                                          00002320
           02 MLLIEU1P  PIC X.                                          00002330
           02 MLLIEU1H  PIC X.                                          00002340
           02 MLLIEU1V  PIC X.                                          00002350
           02 MLLIEU1O  PIC X(20).                                      00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSOCDEPOTA     PIC X.                                     00002380
           02 MSOCDEPOTC     PIC X.                                     00002390
           02 MSOCDEPOTP     PIC X.                                     00002400
           02 MSOCDEPOTH     PIC X.                                     00002410
           02 MSOCDEPOTV     PIC X.                                     00002420
           02 MSOCDEPOTO     PIC X(3).                                  00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MNDEPOTA  PIC X.                                          00002450
           02 MNDEPOTC  PIC X.                                          00002460
           02 MNDEPOTP  PIC X.                                          00002470
           02 MNDEPOTH  PIC X.                                          00002480
           02 MNDEPOTV  PIC X.                                          00002490
           02 MNDEPOTO  PIC X(3).                                       00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MLLIEU2A  PIC X.                                          00002520
           02 MLLIEU2C  PIC X.                                          00002530
           02 MLLIEU2P  PIC X.                                          00002540
           02 MLLIEU2H  PIC X.                                          00002550
           02 MLLIEU2V  PIC X.                                          00002560
           02 MLLIEU2O  PIC X(20).                                      00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MCSELARTA      PIC X.                                     00002590
           02 MCSELARTC PIC X.                                          00002600
           02 MCSELARTP PIC X.                                          00002610
           02 MCSELARTH PIC X.                                          00002620
           02 MCSELARTV PIC X.                                          00002630
           02 MCSELARTO      PIC X(5).                                  00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MIMUTDXA  PIC X.                                          00002660
           02 MIMUTDXC  PIC X.                                          00002670
           02 MIMUTDXP  PIC X.                                          00002680
           02 MIMUTDXH  PIC X.                                          00002690
           02 MIMUTDXV  PIC X.                                          00002700
           02 MIMUTDXO  PIC X(2).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MIMUTDNA  PIC X.                                          00002730
           02 MIMUTDNC  PIC X.                                          00002740
           02 MIMUTDNP  PIC X.                                          00002750
           02 MIMUTDNH  PIC X.                                          00002760
           02 MIMUTDNV  PIC X.                                          00002770
           02 MIMUTDNO  PIC X(2).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MNBLIGNESA     PIC X.                                     00002800
           02 MNBLIGNESC     PIC X.                                     00002810
           02 MNBLIGNESP     PIC X.                                     00002820
           02 MNBLIGNESH     PIC X.                                     00002830
           02 MNBLIGNESV     PIC X.                                     00002840
           02 MNBLIGNESO     PIC X(5).                                  00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MDMUTA    PIC X.                                          00002870
           02 MDMUTC    PIC X.                                          00002880
           02 MDMUTP    PIC X.                                          00002890
           02 MDMUTH    PIC X.                                          00002900
           02 MDMUTV    PIC X.                                          00002910
           02 MDMUTO    PIC X(10).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MNBPIECESA     PIC X.                                     00002940
           02 MNBPIECESC     PIC X.                                     00002950
           02 MNBPIECESP     PIC X.                                     00002960
           02 MNBPIECESH     PIC X.                                     00002970
           02 MNBPIECESV     PIC X.                                     00002980
           02 MNBPIECESO     PIC X(5).                                  00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MNBPQUOTAA     PIC X.                                     00003010
           02 MNBPQUOTAC     PIC X.                                     00003020
           02 MNBPQUOTAP     PIC X.                                     00003030
           02 MNBPQUOTAH     PIC X.                                     00003040
           02 MNBPQUOTAV     PIC X.                                     00003050
           02 MNBPQUOTAO     PIC X(5).                                  00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MTAUXNPA  PIC X.                                          00003080
           02 MTAUXNPC  PIC X.                                          00003090
           02 MTAUXNPP  PIC X.                                          00003100
           02 MTAUXNPH  PIC X.                                          00003110
           02 MTAUXNPV  PIC X.                                          00003120
           02 MTAUXNPO  PIC X(4).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MNMUTDESTA     PIC X.                                     00003150
           02 MNMUTDESTC     PIC X.                                     00003160
           02 MNMUTDESTP     PIC X.                                     00003170
           02 MNMUTDESTH     PIC X.                                     00003180
           02 MNMUTDESTV     PIC X.                                     00003190
           02 MNMUTDESTO     PIC X(7).                                  00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MVOLUMEA  PIC X.                                          00003220
           02 MVOLUMEC  PIC X.                                          00003230
           02 MVOLUMEP  PIC X.                                          00003240
           02 MVOLUMEH  PIC X.                                          00003250
           02 MVOLUMEV  PIC X.                                          00003260
           02 MVOLUMEO  PIC X(6).                                       00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MDISPOA   PIC X.                                          00003290
           02 MDISPOC   PIC X.                                          00003300
           02 MDISPOP   PIC X.                                          00003310
           02 MDISPOH   PIC X.                                          00003320
           02 MDISPOV   PIC X.                                          00003330
           02 MDISPOO   PIC X(6).                                       00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MM3QUOTAA      PIC X.                                     00003360
           02 MM3QUOTAC PIC X.                                          00003370
           02 MM3QUOTAP PIC X.                                          00003380
           02 MM3QUOTAH PIC X.                                          00003390
           02 MM3QUOTAV PIC X.                                          00003400
           02 MM3QUOTAO      PIC X(7).                                  00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MTAUXM3A  PIC X.                                          00003430
           02 MTAUXM3C  PIC X.                                          00003440
           02 MTAUXM3P  PIC X.                                          00003450
           02 MTAUXM3H  PIC X.                                          00003460
           02 MTAUXM3V  PIC X.                                          00003470
           02 MTAUXM3O  PIC X(4).                                       00003480
           02 MLIGNEO OCCURS   9 TIMES .                                00003490
             03 FILLER       PIC X(2).                                  00003500
             03 MDDELIVA     PIC X.                                     00003510
             03 MDDELIVC     PIC X.                                     00003520
             03 MDDELIVP     PIC X.                                     00003530
             03 MDDELIVH     PIC X.                                     00003540
             03 MDDELIVV     PIC X.                                     00003550
             03 MDDELIVO     PIC X(8).                                  00003560
             03 FILLER       PIC X(2).                                  00003570
             03 MNMAGA  PIC X.                                          00003580
             03 MNMAGC  PIC X.                                          00003590
             03 MNMAGP  PIC X.                                          00003600
             03 MNMAGH  PIC X.                                          00003610
             03 MNMAGV  PIC X.                                          00003620
             03 MNMAGO  PIC X(3).                                       00003630
             03 FILLER       PIC X(2).                                  00003640
             03 MNVENTEA     PIC X.                                     00003650
             03 MNVENTEC     PIC X.                                     00003660
             03 MNVENTEP     PIC X.                                     00003670
             03 MNVENTEH     PIC X.                                     00003680
             03 MNVENTEV     PIC X.                                     00003690
             03 MNVENTEO     PIC X(7).                                  00003700
             03 FILLER       PIC X(2).                                  00003710
             03 MINDICEA     PIC X.                                     00003720
             03 MINDICEC     PIC X.                                     00003730
             03 MINDICEP     PIC X.                                     00003740
             03 MINDICEH     PIC X.                                     00003750
             03 MINDICEV     PIC X.                                     00003760
             03 MINDICEO     PIC X.                                     00003770
             03 FILLER       PIC X(2).                                  00003780
             03 MNMUTA  PIC X.                                          00003790
             03 MNMUTC  PIC X.                                          00003800
             03 MNMUTP  PIC X.                                          00003810
             03 MNMUTH  PIC X.                                          00003820
             03 MNMUTV  PIC X.                                          00003830
             03 MNMUTO  PIC X(7).                                       00003840
             03 FILLER       PIC X(2).                                  00003850
             03 MNCODICA     PIC X.                                     00003860
             03 MNCODICC     PIC X.                                     00003870
             03 MNCODICP     PIC X.                                     00003880
             03 MNCODICH     PIC X.                                     00003890
             03 MNCODICV     PIC X.                                     00003900
             03 MNCODICO     PIC X(7).                                  00003910
             03 FILLER       PIC X(2).                                  00003920
             03 MQLIGNESA    PIC X.                                     00003930
             03 MQLIGNESC    PIC X.                                     00003940
             03 MQLIGNESP    PIC X.                                     00003950
             03 MQLIGNESH    PIC X.                                     00003960
             03 MQLIGNESV    PIC X.                                     00003970
             03 MQLIGNESO    PIC X(4).                                  00003980
             03 FILLER       PIC X(2).                                  00003990
             03 MQPIECESA    PIC X.                                     00004000
             03 MQPIECESC    PIC X.                                     00004010
             03 MQPIECESP    PIC X.                                     00004020
             03 MQPIECESH    PIC X.                                     00004030
             03 MQPIECESV    PIC X.                                     00004040
             03 MQPIECESO    PIC X(5).                                  00004050
             03 FILLER       PIC X(2).                                  00004060
             03 MQVOLUMEA    PIC X.                                     00004070
             03 MQVOLUMEC    PIC X.                                     00004080
             03 MQVOLUMEP    PIC X.                                     00004090
             03 MQVOLUMEH    PIC X.                                     00004100
             03 MQVOLUMEV    PIC X.                                     00004110
             03 MQVOLUMEO    PIC X(6).                                  00004120
             03 FILLER       PIC X(2).                                  00004130
             03 MMESSAGEA    PIC X.                                     00004140
             03 MMESSAGEC    PIC X.                                     00004150
             03 MMESSAGEP    PIC X.                                     00004160
             03 MMESSAGEH    PIC X.                                     00004170
             03 MMESSAGEV    PIC X.                                     00004180
             03 MMESSAGEO    PIC X(20).                                 00004190
             03 FILLER       PIC X(2).                                  00004200
             03 MACTIONA     PIC X.                                     00004210
             03 MACTIONC     PIC X.                                     00004220
             03 MACTIONP     PIC X.                                     00004230
             03 MACTIONH     PIC X.                                     00004240
             03 MACTIONV     PIC X.                                     00004250
             03 MACTIONO     PIC X.                                     00004260
           02 FILLER    PIC X(2).                                       00004270
           02 MDMUTORIGA     PIC X.                                     00004280
           02 MDMUTORIGC     PIC X.                                     00004290
           02 MDMUTORIGP     PIC X.                                     00004300
           02 MDMUTORIGH     PIC X.                                     00004310
           02 MDMUTORIGV     PIC X.                                     00004320
           02 MDMUTORIGO     PIC X(10).                                 00004330
           02 FILLER    PIC X(2).                                       00004340
           02 MTOTLIGNEA     PIC X.                                     00004350
           02 MTOTLIGNEC     PIC X.                                     00004360
           02 MTOTLIGNEP     PIC X.                                     00004370
           02 MTOTLIGNEH     PIC X.                                     00004380
           02 MTOTLIGNEV     PIC X.                                     00004390
           02 MTOTLIGNEO     PIC X(5).                                  00004400
           02 FILLER    PIC X(2).                                       00004410
           02 MTOTPIECEA     PIC X.                                     00004420
           02 MTOTPIECEC     PIC X.                                     00004430
           02 MTOTPIECEP     PIC X.                                     00004440
           02 MTOTPIECEH     PIC X.                                     00004450
           02 MTOTPIECEV     PIC X.                                     00004460
           02 MTOTPIECEO     PIC X(5).                                  00004470
           02 FILLER    PIC X(2).                                       00004480
           02 MTOTVOLUMA     PIC X.                                     00004490
           02 MTOTVOLUMC     PIC X.                                     00004500
           02 MTOTVOLUMP     PIC X.                                     00004510
           02 MTOTVOLUMH     PIC X.                                     00004520
           02 MTOTVOLUMV     PIC X.                                     00004530
           02 MTOTVOLUMO     PIC X(6).                                  00004540
           02 FILLER    PIC X(2).                                       00004550
           02 MZONCMDA  PIC X.                                          00004560
           02 MZONCMDC  PIC X.                                          00004570
           02 MZONCMDP  PIC X.                                          00004580
           02 MZONCMDH  PIC X.                                          00004590
           02 MZONCMDV  PIC X.                                          00004600
           02 MZONCMDO  PIC X(15).                                      00004610
           02 FILLER    PIC X(2).                                       00004620
           02 MLIBERRA  PIC X.                                          00004630
           02 MLIBERRC  PIC X.                                          00004640
           02 MLIBERRP  PIC X.                                          00004650
           02 MLIBERRH  PIC X.                                          00004660
           02 MLIBERRV  PIC X.                                          00004670
           02 MLIBERRO  PIC X(58).                                      00004680
           02 FILLER    PIC X(2).                                       00004690
           02 MCODTRAA  PIC X.                                          00004700
           02 MCODTRAC  PIC X.                                          00004710
           02 MCODTRAP  PIC X.                                          00004720
           02 MCODTRAH  PIC X.                                          00004730
           02 MCODTRAV  PIC X.                                          00004740
           02 MCODTRAO  PIC X(4).                                       00004750
           02 FILLER    PIC X(2).                                       00004760
           02 MCICSA    PIC X.                                          00004770
           02 MCICSC    PIC X.                                          00004780
           02 MCICSP    PIC X.                                          00004790
           02 MCICSH    PIC X.                                          00004800
           02 MCICSV    PIC X.                                          00004810
           02 MCICSO    PIC X(5).                                       00004820
           02 FILLER    PIC X(2).                                       00004830
           02 MNETNAMA  PIC X.                                          00004840
           02 MNETNAMC  PIC X.                                          00004850
           02 MNETNAMP  PIC X.                                          00004860
           02 MNETNAMH  PIC X.                                          00004870
           02 MNETNAMV  PIC X.                                          00004880
           02 MNETNAMO  PIC X(8).                                       00004890
           02 FILLER    PIC X(2).                                       00004900
           02 MSCREENA  PIC X.                                          00004910
           02 MSCREENC  PIC X.                                          00004920
           02 MSCREENP  PIC X.                                          00004930
           02 MSCREENH  PIC X.                                          00004940
           02 MSCREENV  PIC X.                                          00004950
           02 MSCREENO  PIC X(4).                                       00004960
                                                                                
