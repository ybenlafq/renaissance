      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA620 AU 29/10/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,04,PD,A,                          *        
      *                           14,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA620.                                                        
            05 NOMETAT-IVA620           PIC X(6) VALUE 'IVA620'.                
            05 RUPTURES-IVA620.                                                 
           10 IVA620-NSOCVALO           PIC X(03).                      007  003
           10 IVA620-NSEQPRO            PIC S9(07)      COMP-3.         010  004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA620-SEQUENCE           PIC S9(04) COMP.                014  002
      *--                                                                       
           10 IVA620-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA620.                                                   
           10 IVA620-CDEVISE            PIC X(06).                      016  006
           10 IVA620-LSEQPRO            PIC X(26).                      022  026
           10 IVA620-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         048  008
           10 IVA620-PSTOCKFINAL        PIC S9(09)V9(6) COMP-3.         056  008
           10 IVA620-PSTOCKINIT         PIC S9(09)V9(6) COMP-3.         064  008
           10 IVA620-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         072  008
           10 IVA620-QSTOCKENTREE       PIC S9(11)      COMP-3.         080  006
           10 IVA620-QSTOCKFINAL        PIC S9(11)      COMP-3.         086  006
           10 IVA620-QSTOCKINIT         PIC S9(11)      COMP-3.         092  006
           10 IVA620-QSTOCKSORTIE       PIC S9(11)      COMP-3.         098  006
            05 FILLER                      PIC X(409).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IVA620-LONG           PIC S9(4)   COMP  VALUE +103.           
      *                                                                         
      *--                                                                       
        01  DSECT-IVA620-LONG           PIC S9(4) COMP-5  VALUE +103.           
                                                                                
      *}                                                                        
