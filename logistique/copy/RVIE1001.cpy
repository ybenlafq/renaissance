      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVIE1000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIE1000                         
      **********************************************************                
       01  RVIE1000.                                                            
           02  IE10-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  IE10-NDEPOT                                                      
               PIC X(0003).                                                     
           02  IE10-WJIE311                                                     
               PIC X(0001).                                                     
           02  IE10-WEMPL                                                       
               PIC X(0001).                                                     
           02  IE10-WSTOCKINV                                                   
               PIC X(0001).                                                     
           02  IE10-WRTGS10                                                     
               PIC X(0001).                                                     
           02  IE10-WSTBATCHTLM                                                 
               PIC X(0001).                                                     
           02  IE10-WSTBATCHELA                                                 
               PIC X(0001).                                                     
           02  IE10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  IE10-WTOPHS                                                      
               PIC X(0001).                                                     
           02  IE10-WTOPPRET                                                    
               PIC X(0001).                                                     
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVIE1000                                  
      **********************************************************                
       01  RVIE1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WJIE311-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WJIE311-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WEMPL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WEMPL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WSTOCKINV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WSTOCKINV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WRTGS10-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WRTGS10-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WSTBATCHTLM-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WSTBATCHTLM-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WSTBATCHELA-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WSTBATCHELA-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WTOPHS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WTOPHS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WTOPPRET-F                                                  
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  IE10-WTOPPRET-F                                                  
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
