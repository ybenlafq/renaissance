      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*00010000
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT  FGP100 AU 31/10/2008 *00020000
M3828 *  M3828 * 20/05/2011 PAR J .BICHY                               *00030000
      *----------------------------------------------------------------*00050000
       01  DSECT-:FGP100:.                                              00060000
         05 CHAMPS-:FGP100:.                                            00070000
           10 :FGP100:-CFAM             PIC X(05).                      00080000
           10 :FGP100:-LAGREGATED       PIC X(20).                      00090000
           10 :FGP100:-CMARQ            PIC X(05).                      00100000
           10 :FGP100:-LREFFOURN        PIC X(20).                      00110000
           10 :FGP100:-NCODIC           PIC X(07).                      00120000
           10 :FGP100:-CAPPRO           PIC X(03).                      00130000
           10 :FGP100:-LSTATCOMP        PIC X(03).                      00140000
           10 :FGP100:-WSENSAPPRO       PIC X(01).                      00150000
           10 :FGP100:-PRIX-VENTE       PIC S9(07)V9(2) COMP-3.         00160000
           10 :FGP100:-TX2080           PIC X(02).                      00170000
           10 :FGP100:-RANG             PIC S9(03)      COMP-3.         00180000
           10 :FGP100:-S-4              PIC S9(05)      COMP-3.         00190000
           10 :FGP100:-S-3              PIC S9(05)      COMP-3.         00200000
           10 :FGP100:-S-2              PIC S9(05)      COMP-3.         00210000
           10 :FGP100:-S-1              PIC S9(05)      COMP-3.         00220000
           10 :FGP100:-J-3              PIC S9(05)      COMP-3.         00230000
           10 :FGP100:-J-2              PIC S9(05)      COMP-3.         00240000
           10 :FGP100:-J-1              PIC S9(05)      COMP-3.         00250000
           10 :FGP100:-S-1-NORD         PIC S9(05)      COMP-3.         00260000
           10 :FGP100:-S-1-SUD          PIC S9(05)      COMP-3.         00270000
           10 :FGP100:-STK-OBJ          PIC X(06).                      00280000
           10 :FGP100:-TOP-STKOBJ       PIC X(01).                      00290000
           10 :FGP100:-QDEM-MUTMAGICN   PIC S9(05)      COMP-3.         00300001
           10 :FGP100:-QDEM-MUTMAGICS   PIC S9(05)      COMP-3.         00301001
           10 :FGP100:-COLISAGE         PIC X(05).                      00310000
           10 :FGP100:-COLIS  REDEFINES :FGP100:-COLISAGE.              00320000
              15 :FGP100:-QCOLIDSTOCK      PIC ZZZ9.                    00330000
              15 :FGP100:-CFETEMPL         PIC X(01).                   00340000
           10 :FGP100:-QSTOCK-DEPN      PIC S9(05)      COMP-3.         00350000
           10 :FGP100:-QSTOCK-DEPS      PIC S9(05)      COMP-3.         00360000
           10 :FGP100:-QSTOCK-DACEM     PIC S9(05)      COMP-3.         00370000
           10 :FGP100:-TOP-GP           PIC X(01).                      00380000
           10 :FGP100:-STATUT-PRODUIT   PIC X(01).                      00390000
           10 :FGP100:-COMMENTAIRE      PIC X(20).                      00400000
M3828      10 :FGP100:-FIN-RM50         PIC X(10).                      00401000
M3828      10 :FGP100:-DAPPRO           PIC X(10).                      00402000
M3828      10 :FGP100:-QAPPRO           PIC S9(05)      COMP-3.         00403000
M3828      10 :FGP100:-DJOUR            PIC X(08)   .                   00404000
M3828    05 FILLER                      PIC X(03).                      00410000
                                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-FGP100-LONG         PIC S9(4)   COMP  VALUE +172.             
      *                                                                         
      *--                                                                       
        01  DSECT-:FGP100:-LONG         PIC S9(4) COMP-5  VALUE +172.           
                                                                        00420000
      *}                                                                        
