      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVTL0900                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVTL0900                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTL0900.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTL0900.                                                            
      *}                                                                        
           02  TL09-NDEPOT                                                      
               PIC X(0003).                                                     
           02  TL09-CLIVR                                                       
               PIC X(0010).                                                     
           02  TL09-DMMLIVR                                                     
               PIC X(0006).                                                     
           02  TL09-CRETOUR                                                     
               PIC X(0005).                                                     
           02  TL09-QPIECES                                                     
               PIC S9(5) COMP-3.                                                
           02  TL09-QLIVR                                                       
               PIC S9(5) COMP-3.                                                
           02  TL09-QLIVRPAR                                                    
               PIC S9(5) COMP-3.                                                
           02  TL09-QJOURS                                                      
               PIC S9(3)V9(0002) COMP-3.                                        
           02  TL09-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVTL0900                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTL0900-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTL0900-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL09-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL09-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL09-CLIVR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL09-CLIVR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL09-DMMLIVR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL09-DMMLIVR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL09-CRETOUR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL09-CRETOUR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL09-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL09-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL09-QLIVR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL09-QLIVR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL09-QLIVRPAR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL09-QLIVRPAR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL09-QJOURS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL09-QJOURS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL09-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL09-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
