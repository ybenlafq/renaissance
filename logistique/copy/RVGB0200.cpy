      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGB0200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB0200                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGB0200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGB0200.                                                            
      *}                                                                        
           02  GB02-NLIGTRANS                                                   
               PIC X(0006).                                                     
           02  GB02-LLIGTRANS                                                   
               PIC X(0020).                                                     
           02  GB02-CTYPLIG                                                     
               PIC X(0005).                                                     
           02  GB02-WACTIF                                                      
               PIC X(0001).                                                     
           02  GB02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGB0200                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGB0200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGB0200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB02-NLIGTRANS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB02-NLIGTRANS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB02-LLIGTRANS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB02-LLIGTRANS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB02-CTYPLIG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB02-CTYPLIG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB02-WACTIF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB02-WACTIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
