      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*00000010
      *                                                                *00000020
      *   Gestion des statistiques de livraison                        *00000030
      *                                                                *00000040
      *   D�finition de la structure du fichier FTL110                 *00000050
      *     (Param�tres de traitement)                                 *00000060
      *                                                                *00000070
      *   Fichier cr�� par le programme BTL110.                         00000080
      *   S�quentiel ; Longueur 80                                     *00000090
      *                                                                *00000100
      *----------------------------------------------------------------*00000110
      *                                                                *00000120
      * FTL110-NSOC       Code soci�t�                         001/003 *00000130
      *       -DTRT       Date de traitement                   004/011 *00000140
      *       -CTRT       Code traitement (H/M)                012/012 *00000150
      *       -DDPER1     Date de d�but p�riode "d�tail"       013/020 *00000160
      *       -DDPER2     Date de d�but p�riode "cumul"        021/028 *00000170
      *       -DFPER      Date de fin de p�riode               029/036 *00000180
      *       -Dtrt-1     Date traitement pr�c�dent            037/044 *00000190
      *                   Disponible                           045/080 *00000200
      *                                                                *00000210
      *----------------------------------------------------------------*00000220
                                                                        00000230
       01  FTL110-DSECT.                                                00000240
           05  FTL110-NSOC             PIC  X(03).                      00000250
           05  FTL110-DTRT             PIC  X(08).                      00000260
           05  FTL110-CTRT             PIC  X(01).                      00000270
           05  FTL110-DDPER1           PIC  X(08).                      00000280
           05  FTL110-DDPER2           PIC  X(08).                      00000290
           05  FTL110-DFPER            PIC  X(08).                      00000300
           05  FTL110-DTRT-1           PIC  X(08).                      00000310
           05  FILLER                  PIC  X(36).                      00000320
      *                                                                 00000330
                                                                                
