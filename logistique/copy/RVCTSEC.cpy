      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CTSEC TRANSCO DES SECTIONS ANA         *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVCTSEC .                                                            
           05  CTSEC-CTABLEG2    PIC X(15).                                     
           05  CTSEC-CTABLEG2-REDEF REDEFINES CTSEC-CTABLEG2.                   
               10  CTSEC-SECTION         PIC X(06).                             
           05  CTSEC-WTABLEG     PIC X(80).                                     
           05  CTSEC-WTABLEG-REDEF  REDEFINES CTSEC-WTABLEG.                    
               10  CTSEC-WTRANSCO        PIC X(01).                             
               10  CTSEC-SECTIONT        PIC X(07).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCTSEC-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTSEC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CTSEC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTSEC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CTSEC-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
