      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-RM22-LONG             PIC S9(4) COMP-3 VALUE +420.                
       01  TS-RM22-RECORD.                                                      
           05 TS-RM22-LIGNE                OCCURS 4.                            
              10 TS-RM22-LAGREG     PIC X(20).                                  
              10 TS-RM22-WNAT       PIC X(1).                                   
              10 TS-RM22-WIMP       PIC X(1).                                   
              10 TS-RM22-QHV4       PIC 9(5).                                   
              10 TS-RM22-QHV3       PIC 9(5).                                   
              10 TS-RM22-QHV2       PIC 9(5).                                   
              10 TS-RM22-QHV1       PIC 9(5).                                   
              10 TS-RM22-QPM3       PIC 9(5).                                   
              10 TS-RM22-QPM2       PIC 9(5).                                   
              10 TS-RM22-QPM1       PIC 9(5).                                   
              10 TS-RM22-QSV4       PIC 9(6).                                   
              10 TS-RM22-QSV3       PIC 9(6).                                   
              10 TS-RM22-QSV2       PIC 9(6).                                   
              10 TS-RM22-QSV1       PIC 9(6).                                   
              10 TS-RM22-QPV0       PIC X(6).                                   
              10 TS-RM22-QPVF3      PIC X(6).                                   
              10 TS-RM22-QPVF2      PIC X(6).                                   
              10 TS-RM22-QPVF1      PIC X(6).                                   
                                                                                
