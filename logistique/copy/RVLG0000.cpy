      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLG0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLG0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLG0000.                                                            
           02  LG00-CINSEE                                                      
               PIC X(0005).                                                     
           02  LG00-NVOIE                                                       
               PIC X(0004).                                                     
           02  LG00-CTVOIE                                                      
               PIC X(0004).                                                     
           02  LG00-LNOMVOIE                                                    
               PIC X(0021).                                                     
           02  LG00-LNOMPVOIE                                                   
               PIC X(0042).                                                     
           02  LG00-CILOT                                                       
               PIC X(0008).                                                     
           02  LG00-CPARITE                                                     
               PIC X(0001).                                                     
           02  LG00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLG0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLG0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG00-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG00-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG00-NVOIE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG00-NVOIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG00-CTVOIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG00-CTVOIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG00-LNOMVOIE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG00-LNOMVOIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG00-LNOMPVOIE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG00-LNOMPVOIE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG00-CILOT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG00-CILOT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG00-CPARITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG00-CPARITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
