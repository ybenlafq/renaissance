      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVGB1501                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB1501                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGB1501.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGB1501.                                                            
      *}                                                                        
           02  GB15-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GB15-NCODIC                                                      
               PIC X(0007).                                                     
           02  GB15-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  GB15-CMARQ                                                       
               PIC X(0005).                                                     
           02  GB15-QDEMANDEE                                                   
               PIC S9(5) COMP-3.                                                
           02  GB15-QLIEE                                                       
               PIC S9(5) COMP-3.                                                
           02  GB15-QCDEPREL                                                    
               PIC S9(5) COMP-3.                                                
           02  GB15-QMUTEE                                                      
               PIC S9(5) COMP-3.                                                
           02  GB15-QPOIDS                                                      
               PIC S9(7) COMP-3.                                                
           02  GB15-QVOLUME                                                     
               PIC S9(7) COMP-3.                                                
           02  GB15-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  GB15-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GB15-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GB15-DRAFALE                                                     
               PIC X(0008).                                                     
           02  GB15-NRAFALE                                                     
               PIC X(0003).                                                     
           02  GB15-NSATELLITE                                                  
               PIC X(0002).                                                     
           02  GB15-NCASE                                                       
               PIC X(0003).                                                     
           02  GB15-WGROUPE                                                     
               PIC X(0001).                                                     
           02  GB15-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GB15-QDEMINIT                                                    
               PIC S9(5) COMP-3.                                                
           02  GB15-QCC                                                         
               PIC S9(5) COMP-3.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGB1501                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGB1501-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGB1501-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-QDEMANDEE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-QDEMANDEE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-QLIEE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-QLIEE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-QCDEPREL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-QCDEPREL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-QMUTEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-QMUTEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-QPOIDS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-QPOIDS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-QVOLUME-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-QVOLUME-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-DRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-DRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-NRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-NRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-NSATELLITE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-NSATELLITE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-NCASE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-NCASE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-WGROUPE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-WGROUPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-QDEMINIT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-QDEMINIT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB15-QCC-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB15-QCC-F                                                       
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
