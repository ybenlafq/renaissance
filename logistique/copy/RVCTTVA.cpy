      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CTTVA TRANSO CODE TVA SAP              *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVCTTVA.                                                             
           05  CTTVA-CTABLEG2    PIC X(15).                                     
           05  CTTVA-CTABLEG2-REDEF REDEFINES CTTVA-CTABLEG2.                   
               10  CTTVA-NATTVA          PIC X(01).                             
               10  CTTVA-TXTVA           PIC X(04).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  CTTVA-TXTVA-N        REDEFINES CTTVA-TXTVA                   
                                         PIC 9(02)V9(02).                       
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  CTTVA-WTABLEG     PIC X(80).                                     
           05  CTTVA-WTABLEG-REDEF  REDEFINES CTTVA-WTABLEG.                    
               10  CTTVA-WACTIF          PIC X(01).                             
               10  CTTVA-CTVASAP         PIC X(02).                             
               10  CTTVA-TYPTVA          PIC X(04).                             
               10  CTTVA-CPTSAP          PIC X(08).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCTTVA-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTTVA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CTTVA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTTVA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CTTVA-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}                                                                        
