      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      *                                                                 00050000
      **************************************************************    00020000
      *    MODIFICATION JUIN 2005 FACTURES PRO FORMA  (MM)         *    00030000
      **************************************************************    00040000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00060000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00070000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00080000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00090000
      *                                                                 00100000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00110000
      * COMPRENANT :                                                    00120000
      * 1 - LES ZONES RESERVEES A AIDA                                  00130000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00140000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00150000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00160000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00170000
      *                                                                 00180000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00190000
      * PAR AIDA                                                        00200000
      *                                                                 00210000
      *-------------------------------------------------------------    00220000
      *                                                                 00230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GS50-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00240001
      *--                                                                       
       01  COM-GS50-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00730000
           02 COMM-GS50-MENU.                                           00740000
              03 COMM-GS50-FONCT          PIC X(3).                     00970003
              03 COMM-GS50-SOCORIG        PIC X(3).                     00970004
              03 COMM-GS50-LIEUORIG       PIC X(3).                     00970005
              03 COMM-GS50-SLIEUORIG      PIC X(3).                     00970006
              03 COMM-GS50-CTYPLIEUORIG   PIC X(1).                     00970007
              03 COMM-GS50-LORIG          PIC X(20).                    00970008
              03 COMM-GS50-SOCDEST        PIC X(3).                     00970009
              03 COMM-GS50-LIEUDEST       PIC X(3).                     00970010
              03 COMM-GS50-SLIEUDEST      PIC X(3).                     00970011
              03 COMM-GS50-CTYPLIEUDEST   PIC X(1).                     00970012
              03 COMM-GS50-LDEST          PIC X(20).                    00970013
              03 COMM-GS50-COPER          PIC X(10).                    00970014
              03 COMM-GS50-DOPER          PIC X(6).                     00970015
              03 COMM-GS50-NHS            PIC X(7).                     00970016
              03 COMM-GS50-NCODIC         PIC X(7).                     00970017
              03 COMM-GS50-CFAM           PIC X(5).                     00970018
              03 COMM-GS50-CMARQ          PIC X(5).                     00970019
              03 COMM-GS50-LREFFOURN      PIC X(20).                    00970020
              03 COMM-GS50-WCODCART       PIC X(1).                     00970021
              03 COMM-GS50-QCONDT         PIC S9(05) COMP-3.            00970023
              03 COMM-GS50-WARTINC        PIC X(1).                     00970024
              03 COMM-GS50-COMMENT        PIC X(30).                    00970025
              03 COMM-GS50-QSTOCK         PIC S9(05) COMP-3.            00970026
              03 COMM-GS50-LCOMMENTSOR    PIC X(20).                    00970027
              03 COMM-GS50-MOTIFENT       PIC X(10).                    00970028
              03 COMM-GS50-NSOCENTRE      PIC X(3).                     00970029
              03 COMM-GS50-NDEPOT         PIC X(3).                     00970030
              03 COMM-GS50-NSOCLIEU       PIC X(3).                     00970031
              03 COMM-GS50-NLIEU          PIC X(3).                     00970032
              03 COMM-GS50-LDEPOT         PIC X(20).                    00970033
              03 COMM-GS50-NMUTATION      PIC X(7).                     00970034
              03 COMM-GS50-DMUTATION      PIC X(8).                     00970035
              03 COMM-GS50-CODEIMPRI      PIC X(4).                     00970036
              03 COMM-GS50-MESS           PIC X(80).                    00970037
              03 COMM-GS50-DJOUR          PIC X(08).                    00970038
              03 COMM-GS50-SELART         PIC X(05).                    00970039
              03 COMM-GS50-PROG           PIC X(05).                    00970040
              03 COMM-GS50-ITEM           PIC S9(5) COMP-3.             00970041
              03 COMM-GS50-CODRET         PIC S9(4).                    00970043
              03 COMM-GS50-MAX-PAGE       PIC S9(5) COMP-3.             00970044
              03 COMM-GS50-OPTIONS        PIC X.                        00970045
              03 COMM-GS50-LECT-TS        PIC 9.                        00970046
                 88  PAS-LIRE-TS-MU07           VALUE 0.                00970047
                 88  LIRE-TS-MU07               VALUE 1.                00970048
              03 COMM-GS50-SOCMUT         PIC X(03).                    00970049
              03 COMM-GS50-LIEMUT         PIC X(03).                    00970050
              03 COMM-GS50-NMVT           PIC X(07).                    00970051
              03 COMM-GS50-ORIGINE-MUT    PIC X(01).                    00970052
              03 COMM-GS50-DDESTOCK       PIC X(08).                    00970053
FRC001        03 COMM-GS50-TYPUSER        PIC X(08).                    00970053
FRC001           88 COMM-GS50-88-MONODEP  VALUE 'MONODEP '.                     
FRC001           88 COMM-GS50-88-MULTIDEP VALUE 'MULTIDEP'.                     
FRC001           88 COMM-GS50-88-MULTISOC VALUE 'MULTISOC'.                     
FRC001           88 COMM-GS50-88-INTEGRAL VALUE 'INTEGRAL'.                     
FRC001        03 COMM-GS50-AUTOR-DIXDEPOT.                                      
FRC001           05 COMM-GS50-AUTOR-UN-DEPOT OCCURS 100                         
FRC001                                       INDEXED BY IND1-GS50.              
FRC001              07 COMM-GS50-AUTOR-NSOCENTRE PIC X(3).                      
FRC001              07 COMM-GS50-AUTOR-NDEPOT    PIC X(3).                      
              03 COMM-GS50-NSOCCICS              PIC X(3).                      
              03 COMM-GS50-CFLGMAG               PIC X(5).                      
              03 COMM-GS50-MFL10-NSOCIETE        PIC X(3).                      
           02 COMM-GS50-APPLI.                                          00970054
              03 COMM-GS50-FILLER         PIC X(2731).                  00970060
      * ZONES APPLICATIVES FT10 - TABLES GENERALISEES ******************00970061
          02 COMM-GS50-FT10 REDEFINES COMM-GS50-APPLI.                  00970062
             03 COMM-GS50-FT10-TABLE       PIC X(5).                    00970063
             03 COMM-GS50-FT10-SUPPR       PIC X.                       00970064
             03 COMM-GS50-FT10-RPROG3      PIC X(8).                    00970065
             03 COMM-GS50-FT10-RPROG4      PIC X(8).                    00970066
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GS50-FT00-PAGE        PIC S9(4) COMP.              00970067
      *--                                                                       
             03 COMM-GS50-FT00-PAGE        PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GS50-FT00-NBP         PIC S9(4) COMP.              00970068
      *--                                                                       
             03 COMM-GS50-FT00-NBP         PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GS50-FT00-PAGE-B      PIC S9(4) COMP.              00970069
      *--                                                                       
             03 COMM-GS50-FT00-PAGE-B      PIC S9(4) COMP-5.                    
      *}                                                                        
             03 COMM-GS50-FT10-W-TAB.                                   00970071
      ******************************************************************00970072
      * POS = POSITION PHYSIQUE                                         00970073
      * CHP = POSITION DU CHAMP SUR MLIGNE                              00970074
      * DB2 = POSITION DU CHAMP SUR CTABLEG2 OU WTABLEG                 00970075
      * CLE = 'O'=CTABLEG2   'N'=WTABLEG ' '=N'EXISTE PAS               00970076
      * REMARQUER QUE, SAUF POUR CHAMP1 OU 'S' (=MLIGNEA),              00970077
      *   L'ATTRIBUT SE TROUVE EN CHP - 2.                              00970078
      * EXEMPLE:                                                        00970079
      *   S NAGREG LAGREG                                               00970080
      *   - -----  ------------     POS(1)=3 POS(2)=10                  00970081
      *   -SA-----SASA------------  CHP(1)=4 CHP(2)=13                  00970082
      ******************************************************************00970083
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS50-FT10-POS      PIC S9(4) COMP OCCURS 6.     00970084
      *--                                                                       
                04 COMM-GS50-FT10-POS      PIC S9(4) COMP-5 OCCURS 6.           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS50-FT10-CHP      PIC S9(4) COMP OCCURS 6.     00970085
      *--                                                                       
                04 COMM-GS50-FT10-CHP      PIC S9(4) COMP-5 OCCURS 6.           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS50-FT10-DB2      PIC S9(4) COMP OCCURS 6.     00970086
      *--                                                                       
                04 COMM-GS50-FT10-DB2      PIC S9(4) COMP-5 OCCURS 6.           
      *}                                                                        
                04 COMM-GS50-FT10-CLE      PIC X          OCCURS 6.     00970087
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS50-FT10-QCHAMP   PIC S9(4) COMP OCCURS 6.     00970088
      *--                                                                       
                04 COMM-GS50-FT10-QCHAMP   PIC S9(4) COMP-5 OCCURS 6.           
      *}                                                                        
                04 COMM-GS50-FT10-WCTLDIR  PIC X          OCCURS 6.     00970089
                04 COMM-GS50-FT10-CSTABASS PIC X(5)       OCCURS 6.     00970090
MM0605* ZONES APPLICATIVES GS50 - FACTURE PRO FORMA   ******************00970061
          02 COMM-GS50-PROFORMA REDEFINES COMM-GS50-APPLI.              00970062
              04 COMM-GS50-NFACTPROF      PIC X(8).                     00970034
              04 COMM-MU35-NSOCDEST       PIC X(03).                    00743004
              04 COMM-MU35-NLIEUDEST      PIC X(03).                    00743015
              04 COMM-MU35-WSELGLOB       PIC X(01).                    00743015
              04 COMM-MU35-DMUTATION      PIC X(08).                    00743015
              04 COMM-MU35-NMUTATION      PIC X(07).                    00743015
                                                                                
