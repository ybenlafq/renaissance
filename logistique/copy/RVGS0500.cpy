      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGS0500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS0500                         
      **********************************************************                
       01  RVGS0500.                                                            
           02  GS05-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  GS05-CTYPLIEUORIG                                                
               PIC X(0001).                                                     
           02  GS05-NSSLIEUORIG                                                 
               PIC X(0003).                                                     
           02  GS05-NSOCDEST                                                    
               PIC X(0003).                                                     
           02  GS05-CTYPLIEUDEST                                                
               PIC X(0001).                                                     
           02  GS05-NSSLIEUDEST                                                 
               PIC X(0003).                                                     
           02  GS05-COPER                                                       
               PIC X(0010).                                                     
           02  GS05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGS0500                                  
      **********************************************************                
       01  RVGS0500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS05-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS05-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS05-CTYPLIEUORIG-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS05-CTYPLIEUORIG-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS05-NSSLIEUORIG-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS05-NSSLIEUORIG-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS05-NSOCDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS05-NSOCDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS05-CTYPLIEUDEST-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS05-CTYPLIEUDEST-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS05-NSSLIEUDEST-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS05-NSSLIEUDEST-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS05-COPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS05-COPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GS05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
