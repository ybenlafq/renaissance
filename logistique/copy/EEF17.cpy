      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EEF17   EEF17                                              00000020
      ***************************************************************** 00000030
       01   EEF17I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * centre de traitement                                            00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000180
           02 FILLER    PIC X(2).                                       00000190
           02 MCTRAITI  PIC X(5).                                       00000200
      * lib centre trait.                                               00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCTRAITL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLCTRAITL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCTRAITF      PIC X.                                     00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MLCTRAITI      PIC X(20).                                 00000250
      * lieu d'entree en hs                                             00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHSL      COMP PIC S9(4).                            00000270
      *--                                                                       
           02 MNLIEUHSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHSF      PIC X.                                     00000280
           02 FILLER    PIC X(2).                                       00000290
           02 MNLIEUHSI      PIC X(3).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNHSL     COMP PIC S9(4).                                 00000310
      *--                                                                       
           02 MNHSL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNHSF     PIC X.                                          00000320
           02 FILLER    PIC X(2).                                       00000330
           02 MNHSI     PIC X(7).                                       00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 METATECL  COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 METATECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 METATECF  PIC X.                                          00000360
           02 FILLER    PIC X(2).                                       00000370
           02 METATECI  PIC X(6).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000400
           02 FILLER    PIC X(2).                                       00000410
           02 MNENVOII  PIC X(7).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000440
           02 FILLER    PIC X(2).                                       00000450
           02 MDENVOII  PIC X(10).                                      00000460
      * soc de l'inventaire                                             00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIVL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MNSOCIVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCIVF  PIC X.                                          00000490
           02 FILLER    PIC X(2).                                       00000500
           02 MNSOCIVI  PIC X(3).                                       00000510
      * lieu de l'inventaire                                            00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTOCIVL      COMP PIC S9(4).                            00000530
      *--                                                                       
           02 MLSTOCIVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSTOCIVF      PIC X.                                     00000540
           02 FILLER    PIC X(2).                                       00000550
           02 MLSTOCIVI      PIC X(3).                                  00000560
      * s/lieu de l'invent.                                             00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLSTOCIVL     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MSLSTOCIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSLSTOCIVF     PIC X.                                     00000590
           02 FILLER    PIC X(2).                                       00000600
           02 MSLSTOCIVI     PIC X(3).                                  00000610
      * lieu trait de invent.                                           00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITIVL     COMP PIC S9(4).                            00000630
      *--                                                                       
           02 MLTRAITIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTRAITIVF     PIC X.                                     00000640
           02 FILLER    PIC X(2).                                       00000650
           02 MLTRAITIVI     PIC X(5).                                  00000660
      * soc de l'encours                                                00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCECL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MNSOCECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCECF  PIC X.                                          00000690
           02 FILLER    PIC X(2).                                       00000700
           02 MNSOCECI  PIC X(3).                                       00000710
      * lieu de l'encours                                               00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTOCECL      COMP PIC S9(4).                            00000730
      *--                                                                       
           02 MLSTOCECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSTOCECF      PIC X.                                     00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MLSTOCECI      PIC X(3).                                  00000760
      * s/lieu de l'encours                                             00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLSTOCECL     COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MSLSTOCECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSLSTOCECF     PIC X.                                     00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MSLSTOCECI     PIC X(3).                                  00000810
      * lieu trait de encours                                           00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITECL     COMP PIC S9(4).                            00000830
      *--                                                                       
           02 MLTRAITECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTRAITECF     PIC X.                                     00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MLTRAITECI     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICIVL     COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MNCODICIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNCODICIVF     PIC X.                                     00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MNCODICIVI     PIC X(7).                                  00000900
      * codic de l'encours                                              00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICECL     COMP PIC S9(4).                            00000920
      *--                                                                       
           02 MNCODICECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNCODICECF     PIC X.                                     00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MNCODICECI     PIC X(7).                                  00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTENVL   COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MQTENVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQTENVF   PIC X.                                          00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MQTENVI   PIC X(5).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTRDUL   COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MQTRDUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQTRDUF   PIC X.                                          00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MQTRDUI   PIC X(5).                                       00001030
      * n� de rendu                                                     00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRENDUL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MNRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRENDUF  PIC X.                                          00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MNRENDUI  PIC X(20).                                      00001080
      * date de rendu                                                   00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRENDUL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MDRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRENDUF  PIC X.                                          00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDRENDUI  PIC X(10).                                      00001130
      * mode rendu attendu                                              00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRENDUIVL     COMP PIC S9(4).                            00001150
      *--                                                                       
           02 MCRENDUIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCRENDUIVF     PIC X.                                     00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MCRENDUIVI     PIC X(5).                                  00001180
      * lib rendu attendu                                               00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRENDUL  COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MLRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRENDUF  PIC X.                                          00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MLRENDUI  PIC X(20).                                      00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRENDUECL     COMP PIC S9(4).                            00001240
      *--                                                                       
           02 MCRENDUECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCRENDUECF     PIC X.                                     00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MCRENDUECI     PIC X(5).                                  00001270
      * code garantie                                                   00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGARANTIIVL   COMP PIC S9(4).                            00001290
      *--                                                                       
           02 MCGARANTIIVL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCGARANTIIVF   PIC X.                                     00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MCGARANTIIVI   PIC X(5).                                  00001320
      * lib code garantie                                               00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGARANTIL     COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MLGARANTIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLGARANTIF     PIC X.                                     00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MLGARANTII     PIC X(20).                                 00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGARANTIECL   COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MCGARANTIECL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCGARANTIECF   PIC X.                                     00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MCGARANTIECI   PIC X(5).                                  00001410
      * type de tiers                                                   00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTIERSL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MTTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTTIERSF  PIC X.                                          00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MTTIERSI  PIC X(20).                                      00001460
      * code tiers                                                      00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSIVL     COMP PIC S9(4).                            00001480
      *--                                                                       
           02 MCTIERSIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTIERSIVF     PIC X.                                     00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCTIERSIVI     PIC X(5).                                  00001510
      * lib tiers                                                       00001520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00001530
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MLTIERSI  PIC X(20).                                      00001560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSECL     COMP PIC S9(4).                            00001570
      *--                                                                       
           02 MCTIERSECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTIERSECF     PIC X.                                     00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCTIERSECI     PIC X(5).                                  00001600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOSERIEL      COMP PIC S9(4).                            00001610
      *--                                                                       
           02 MNOSERIEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNOSERIEF      PIC X.                                     00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNOSERIEI      PIC X(16).                                 00001640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCORDL      COMP PIC S9(4).                            00001650
      *--                                                                       
           02 MNACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNACCORDF      PIC X.                                     00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MNACCORDI      PIC X(12).                                 00001680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACCORDL      COMP PIC S9(4).                            00001690
      *--                                                                       
           02 MDACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDACCORDF      PIC X.                                     00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MDACCORDI      PIC X(10).                                 00001720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMACCORDL   COMP PIC S9(4).                            00001730
      *--                                                                       
           02 MLNOMACCORDL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLNOMACCORDF   PIC X.                                     00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLNOMACCORDI   PIC X(10).                                 00001760
      * MESSAGE ERREUR                                                  00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLIBERRI  PIC X(78).                                      00001810
      * CODE TRANSACTION                                                00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MCODTRAI  PIC X(4).                                       00001860
      * zone de commande                                                00001870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001880
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MZONCMDI  PIC X(15).                                      00001910
      * CICS DE TRAVAIL                                                 00001920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001930
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MCICSI    PIC X(5).                                       00001960
      * NETNAME                                                         00001970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001980
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MNETNAMI  PIC X(8).                                       00002010
      * CODE TERMINAL                                                   00002020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MSCREENI  PIC X(4).                                       00002060
      ***************************************************************** 00002070
      * SDF: EEF17   EEF17                                              00002080
      ***************************************************************** 00002090
       01   EEF17O REDEFINES EEF17I.                                    00002100
           02 FILLER    PIC X(12).                                      00002110
      * DATE DU JOUR                                                    00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MDATJOUA  PIC X.                                          00002140
           02 MDATJOUC  PIC X.                                          00002150
           02 MDATJOUH  PIC X.                                          00002160
           02 MDATJOUO  PIC X(10).                                      00002170
      * HEURE                                                           00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MTIMJOUA  PIC X.                                          00002200
           02 MTIMJOUC  PIC X.                                          00002210
           02 MTIMJOUH  PIC X.                                          00002220
           02 MTIMJOUO  PIC X(5).                                       00002230
      * centre de traitement                                            00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCTRAITA  PIC X.                                          00002260
           02 MCTRAITC  PIC X.                                          00002270
           02 MCTRAITH  PIC X.                                          00002280
           02 MCTRAITO  PIC X(5).                                       00002290
      * lib centre trait.                                               00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLCTRAITA      PIC X.                                     00002320
           02 MLCTRAITC PIC X.                                          00002330
           02 MLCTRAITH PIC X.                                          00002340
           02 MLCTRAITO      PIC X(20).                                 00002350
      * lieu d'entree en hs                                             00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MNLIEUHSA      PIC X.                                     00002380
           02 MNLIEUHSC PIC X.                                          00002390
           02 MNLIEUHSH PIC X.                                          00002400
           02 MNLIEUHSO      PIC X(3).                                  00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MNHSA     PIC X.                                          00002430
           02 MNHSC     PIC X.                                          00002440
           02 MNHSH     PIC X.                                          00002450
           02 MNHSO     PIC X(7).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 METATECA  PIC X.                                          00002480
           02 METATECC  PIC X.                                          00002490
           02 METATECH  PIC X.                                          00002500
           02 METATECO  PIC X(6).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNENVOIA  PIC X.                                          00002530
           02 MNENVOIC  PIC X.                                          00002540
           02 MNENVOIH  PIC X.                                          00002550
           02 MNENVOIO  PIC X(7).                                       00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MDENVOIA  PIC X.                                          00002580
           02 MDENVOIC  PIC X.                                          00002590
           02 MDENVOIH  PIC X.                                          00002600
           02 MDENVOIO  PIC X(10).                                      00002610
      * soc de l'inventaire                                             00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNSOCIVA  PIC X.                                          00002640
           02 MNSOCIVC  PIC X.                                          00002650
           02 MNSOCIVH  PIC X.                                          00002660
           02 MNSOCIVO  PIC X(3).                                       00002670
      * lieu de l'inventaire                                            00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MLSTOCIVA      PIC X.                                     00002700
           02 MLSTOCIVC PIC X.                                          00002710
           02 MLSTOCIVH PIC X.                                          00002720
           02 MLSTOCIVO      PIC X(3).                                  00002730
      * s/lieu de l'invent.                                             00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MSLSTOCIVA     PIC X.                                     00002760
           02 MSLSTOCIVC     PIC X.                                     00002770
           02 MSLSTOCIVH     PIC X.                                     00002780
           02 MSLSTOCIVO     PIC X(3).                                  00002790
      * lieu trait de invent.                                           00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MLTRAITIVA     PIC X.                                     00002820
           02 MLTRAITIVC     PIC X.                                     00002830
           02 MLTRAITIVH     PIC X.                                     00002840
           02 MLTRAITIVO     PIC X(5).                                  00002850
      * soc de l'encours                                                00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MNSOCECA  PIC X.                                          00002880
           02 MNSOCECC  PIC X.                                          00002890
           02 MNSOCECH  PIC X.                                          00002900
           02 MNSOCECO  PIC X(3).                                       00002910
      * lieu de l'encours                                               00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MLSTOCECA      PIC X.                                     00002940
           02 MLSTOCECC PIC X.                                          00002950
           02 MLSTOCECH PIC X.                                          00002960
           02 MLSTOCECO      PIC X(3).                                  00002970
      * s/lieu de l'encours                                             00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MSLSTOCECA     PIC X.                                     00003000
           02 MSLSTOCECC     PIC X.                                     00003010
           02 MSLSTOCECH     PIC X.                                     00003020
           02 MSLSTOCECO     PIC X(3).                                  00003030
      * lieu trait de encours                                           00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MLTRAITECA     PIC X.                                     00003060
           02 MLTRAITECC     PIC X.                                     00003070
           02 MLTRAITECH     PIC X.                                     00003080
           02 MLTRAITECO     PIC X(5).                                  00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MNCODICIVA     PIC X.                                     00003110
           02 MNCODICIVC     PIC X.                                     00003120
           02 MNCODICIVH     PIC X.                                     00003130
           02 MNCODICIVO     PIC X(7).                                  00003140
      * codic de l'encours                                              00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MNCODICECA     PIC X.                                     00003170
           02 MNCODICECC     PIC X.                                     00003180
           02 MNCODICECH     PIC X.                                     00003190
           02 MNCODICECO     PIC X(7).                                  00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MQTENVA   PIC X.                                          00003220
           02 MQTENVC   PIC X.                                          00003230
           02 MQTENVH   PIC X.                                          00003240
           02 MQTENVO   PIC ZZZZZ.                                      00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MQTRDUA   PIC X.                                          00003270
           02 MQTRDUC   PIC X.                                          00003280
           02 MQTRDUH   PIC X.                                          00003290
           02 MQTRDUO   PIC ZZZZZ.                                      00003300
      * n� de rendu                                                     00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MNRENDUA  PIC X.                                          00003330
           02 MNRENDUC  PIC X.                                          00003340
           02 MNRENDUH  PIC X.                                          00003350
           02 MNRENDUO  PIC X(20).                                      00003360
      * date de rendu                                                   00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MDRENDUA  PIC X.                                          00003390
           02 MDRENDUC  PIC X.                                          00003400
           02 MDRENDUH  PIC X.                                          00003410
           02 MDRENDUO  PIC X(10).                                      00003420
      * mode rendu attendu                                              00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCRENDUIVA     PIC X.                                     00003450
           02 MCRENDUIVC     PIC X.                                     00003460
           02 MCRENDUIVH     PIC X.                                     00003470
           02 MCRENDUIVO     PIC X(5).                                  00003480
      * lib rendu attendu                                               00003490
           02 FILLER    PIC X(2).                                       00003500
           02 MLRENDUA  PIC X.                                          00003510
           02 MLRENDUC  PIC X.                                          00003520
           02 MLRENDUH  PIC X.                                          00003530
           02 MLRENDUO  PIC X(20).                                      00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MCRENDUECA     PIC X.                                     00003560
           02 MCRENDUECC     PIC X.                                     00003570
           02 MCRENDUECH     PIC X.                                     00003580
           02 MCRENDUECO     PIC X(5).                                  00003590
      * code garantie                                                   00003600
           02 FILLER    PIC X(2).                                       00003610
           02 MCGARANTIIVA   PIC X.                                     00003620
           02 MCGARANTIIVC   PIC X.                                     00003630
           02 MCGARANTIIVH   PIC X.                                     00003640
           02 MCGARANTIIVO   PIC X(5).                                  00003650
      * lib code garantie                                               00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MLGARANTIA     PIC X.                                     00003680
           02 MLGARANTIC     PIC X.                                     00003690
           02 MLGARANTIH     PIC X.                                     00003700
           02 MLGARANTIO     PIC X(20).                                 00003710
           02 FILLER    PIC X(2).                                       00003720
           02 MCGARANTIECA   PIC X.                                     00003730
           02 MCGARANTIECC   PIC X.                                     00003740
           02 MCGARANTIECH   PIC X.                                     00003750
           02 MCGARANTIECO   PIC X(5).                                  00003760
      * type de tiers                                                   00003770
           02 FILLER    PIC X(2).                                       00003780
           02 MTTIERSA  PIC X.                                          00003790
           02 MTTIERSC  PIC X.                                          00003800
           02 MTTIERSH  PIC X.                                          00003810
           02 MTTIERSO  PIC X(20).                                      00003820
      * code tiers                                                      00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MCTIERSIVA     PIC X.                                     00003850
           02 MCTIERSIVC     PIC X.                                     00003860
           02 MCTIERSIVH     PIC X.                                     00003870
           02 MCTIERSIVO     PIC X(5).                                  00003880
      * lib tiers                                                       00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MLTIERSA  PIC X.                                          00003910
           02 MLTIERSC  PIC X.                                          00003920
           02 MLTIERSH  PIC X.                                          00003930
           02 MLTIERSO  PIC X(20).                                      00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MCTIERSECA     PIC X.                                     00003960
           02 MCTIERSECC     PIC X.                                     00003970
           02 MCTIERSECH     PIC X.                                     00003980
           02 MCTIERSECO     PIC X(5).                                  00003990
           02 FILLER    PIC X(2).                                       00004000
           02 MNOSERIEA      PIC X.                                     00004010
           02 MNOSERIEC PIC X.                                          00004020
           02 MNOSERIEH PIC X.                                          00004030
           02 MNOSERIEO      PIC X(16).                                 00004040
           02 FILLER    PIC X(2).                                       00004050
           02 MNACCORDA      PIC X.                                     00004060
           02 MNACCORDC PIC X.                                          00004070
           02 MNACCORDH PIC X.                                          00004080
           02 MNACCORDO      PIC X(12).                                 00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MDACCORDA      PIC X.                                     00004110
           02 MDACCORDC PIC X.                                          00004120
           02 MDACCORDH PIC X.                                          00004130
           02 MDACCORDO      PIC X(10).                                 00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MLNOMACCORDA   PIC X.                                     00004160
           02 MLNOMACCORDC   PIC X.                                     00004170
           02 MLNOMACCORDH   PIC X.                                     00004180
           02 MLNOMACCORDO   PIC X(10).                                 00004190
      * MESSAGE ERREUR                                                  00004200
           02 FILLER    PIC X(2).                                       00004210
           02 MLIBERRA  PIC X.                                          00004220
           02 MLIBERRC  PIC X.                                          00004230
           02 MLIBERRH  PIC X.                                          00004240
           02 MLIBERRO  PIC X(78).                                      00004250
      * CODE TRANSACTION                                                00004260
           02 FILLER    PIC X(2).                                       00004270
           02 MCODTRAA  PIC X.                                          00004280
           02 MCODTRAC  PIC X.                                          00004290
           02 MCODTRAH  PIC X.                                          00004300
           02 MCODTRAO  PIC X(4).                                       00004310
      * zone de commande                                                00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MZONCMDA  PIC X.                                          00004340
           02 MZONCMDC  PIC X.                                          00004350
           02 MZONCMDH  PIC X.                                          00004360
           02 MZONCMDO  PIC X(15).                                      00004370
      * CICS DE TRAVAIL                                                 00004380
           02 FILLER    PIC X(2).                                       00004390
           02 MCICSA    PIC X.                                          00004400
           02 MCICSC    PIC X.                                          00004410
           02 MCICSH    PIC X.                                          00004420
           02 MCICSO    PIC X(5).                                       00004430
      * NETNAME                                                         00004440
           02 FILLER    PIC X(2).                                       00004450
           02 MNETNAMA  PIC X.                                          00004460
           02 MNETNAMC  PIC X.                                          00004470
           02 MNETNAMH  PIC X.                                          00004480
           02 MNETNAMO  PIC X(8).                                       00004490
      * CODE TERMINAL                                                   00004500
           02 FILLER    PIC X(2).                                       00004510
           02 MSCREENA  PIC X.                                          00004520
           02 MSCREENC  PIC X.                                          00004530
           02 MSCREENH  PIC X.                                          00004540
           02 MSCREENO  PIC X(4).                                       00004550
                                                                                
