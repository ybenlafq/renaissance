      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM4300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM4300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM4300.                                                            
           02  RM43-NCODIC                                                      
               PIC X(0007).                                                     
           02  RM43-CTYPREG                                                     
               PIC X(0002).                                                     
           02  RM43-QEXPO                                                       
               PIC S9(05) COMP-3.                                               
           02  RM43-QLS                                                         
               PIC S9(05) COMP-3.                                               
           02  RM43-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM4300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM4300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM43-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM43-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM43-CTYPREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM43-CTYPREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM43-QEXPO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM43-QEXPO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM43-QLS-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM43-QLS-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM43-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM43-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
