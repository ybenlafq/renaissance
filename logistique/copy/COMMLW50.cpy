      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * COMMAREA DE L'ECOUTEUR DE LA TABLE RTLW98 DES STOCKS INNOVENTE          
      *****************************************************************         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COM-LW50-LONG-COMMAREA PIC S9(4) COMP VALUE +70.                      
      *--                                                                       
       01 COM-LW50-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +70.                    
      *}                                                                        
       01 COMM-LW50.                                                            
          02 COMM-LW50-ALLEE.                                                   
             03 COMM-LW50-CLE                 PIC X(50).                        
          02 COMM-LW50-RETOUR.                                                  
             03 COMM-LW50-CRETOUR             PIC X(02).                        
             03 COMM-LW50-LRETOUR             PIC X(20).                        
      *                                                                         
                                                                                
