      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGB6501                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB6501                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB6501.                                                            
           02  GB65-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GB65-NCODIC                                                      
               PIC X(0007).                                                     
           02  GB65-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  GB65-CMARQ                                                       
               PIC X(0005).                                                     
           02  GB65-QDEMANDEE                                                   
               PIC S9(5) COMP-3.                                                
           02  GB65-QLIEE                                                       
               PIC S9(5) COMP-3.                                                
           02  GB65-QCDEPREL                                                    
               PIC S9(5) COMP-3.                                                
           02  GB65-QMUTEE                                                      
               PIC S9(5) COMP-3.                                                
           02  GB65-QPOIDS                                                      
               PIC S9(7) COMP-3.                                                
           02  GB65-QVOLUME                                                     
               PIC S9(7) COMP-3.                                                
           02  GB65-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  GB65-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GB65-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GB65-DRAFALE                                                     
               PIC X(0008).                                                     
           02  GB65-NRAFALE                                                     
               PIC X(0003).                                                     
           02  GB65-NSATELLITE                                                  
               PIC X(0002).                                                     
           02  GB65-NCASE                                                       
               PIC X(0003).                                                     
           02  GB65-WGROUPE                                                     
               PIC X(0001).                                                     
           02  GB65-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GB65-QDEMINIT                                                    
               PIC S9(5) COMP-3.                                                
           02  GB65-QCC                                                         
               PIC S9(5) COMP-3.                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGB6501                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB6501-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-QDEMANDEE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-QDEMANDEE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-QLIEE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-QLIEE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-QCDEPREL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-QCDEPREL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-QMUTEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-QMUTEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-QPOIDS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-QPOIDS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-QVOLUME-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-QVOLUME-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-DRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-DRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-NRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-NRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-NSATELLITE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-NSATELLITE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-NCASE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-NCASE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-WGROUPE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-WGROUPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-QDEMINIT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-QDEMINIT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB65-QCC-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB65-QCC-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
