      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00000010
      * TS SPECIFIQUE TFA11                                          *  00000020
      ****************************************************************  00000030
      *------------------------------ LONGUEUR                          00000040
       01  TS1-LONG              PIC S9(3) COMP-3 VALUE 576.            00000050
      *------------------------------ DONNEES, 1 PAGE PAR ITEM          00000060
       01  TS1-DONNEES.                                                 00000070
           10 TS1-PAGE         OCCURS 12.                               00000080
              15 TS1-LIGNE.                                             00000090
                 20 TS1-ACTION    PIC X(1)        VALUE SPACE.          00000100
                    88 TS1-SELECTION              VALUE 'X'.            00000110
                 20 TS1-NMUTATION PIC X(7).                             00000120
                 20 TS1-DMUTATION PIC X(8).                             00000130
                 20 TS1-NSOCENTR  PIC X(3).                             00000140
                 20 TS1-NDEPOT    PIC X(3).                             00000150
                 20 TS1-LDEPOT    PIC X(20).                            00000160
                 20 TS1-QNBPIECES PIC Z(5).                             00000170
                 20 TS1-WVAL      PIC X(1).                             00000180
                                                                                
