      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGD0000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGD0000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGD0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGD0000.                                                            
      *}                                                                        
           02  GD00-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GD00-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GD00-DRAFALE                                                     
               PIC X(0008).                                                     
           02  GD00-NRAFALE                                                     
               PIC X(0003).                                                     
           02  GD00-DHSAISIE                                                    
               PIC X(0002).                                                     
           02  GD00-DMSAISIE                                                    
               PIC X(0002).                                                     
           02  GD00-DSAISIE                                                     
               PIC X(0008).                                                     
           02  GD00-DVALIDITE                                                   
               PIC X(0008).                                                     
           02  GD00-WSTOCKRACK                                                  
               PIC X(0001).                                                     
           02  GD00-WSTOCKVRAC                                                  
               PIC X(0001).                                                     
           02  GD00-WSTOCKSOL                                                   
               PIC X(0001).                                                     
           02  GD00-WETATRAFALE                                                 
               PIC X(0001).                                                     
           02  GD00-QTRANCVOL1                                                  
               PIC S9(5) COMP-3.                                                
           02  GD00-QTRANCVOL2                                                  
               PIC S9(5) COMP-3.                                                
           02  GD00-QTRANCPOI1                                                  
               PIC S9(5) COMP-3.                                                
           02  GD00-QTRANCPOI2                                                  
               PIC S9(5) COMP-3.                                                
           02  GD00-WLISTESAT                                                   
               PIC X(0001).                                                     
           02  GD00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGD0000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGD0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGD0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-DRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-DRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-NRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-NRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-DHSAISIE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-DHSAISIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-DMSAISIE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-DMSAISIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-DVALIDITE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-DVALIDITE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-WSTOCKRACK-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-WSTOCKRACK-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-WSTOCKVRAC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-WSTOCKVRAC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-WSTOCKSOL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-WSTOCKSOL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-WETATRAFALE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-WETATRAFALE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-QTRANCVOL1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-QTRANCVOL1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-QTRANCVOL2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-QTRANCVOL2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-QTRANCPOI1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-QTRANCPOI1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-QTRANCPOI2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-QTRANCPOI2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-WLISTESAT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD00-WLISTESAT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GD00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
