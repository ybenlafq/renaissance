      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE MGF20                                  *    00020000
      *           POUR VALORISATION AUTOMATIQUE                    *    00100000
      **************************************************************    00110000
      *                                                                 00880000
       01 COMM-MF20-LONG-COMMAREA   PIC S9(4) VALUE +4096.                      
       01 COMM-MF20-APPLI.                                              00890000
          05 COMM-MF20-PGRM           PIC X(5).                                 
          05 COMM-MF20-NTERMID        PIC X(4).                                 
          05 COMM-MF20-NSOCIETE       PIC X(3).                                 
          05 COMM-MF20-LIGNES OCCURS 10.                                        
             10 COMM-MF20-NCDE        PIC X(7).                                 
             10 COMM-MF20-NSOCLIVR    PIC X(3).                                 
             10 COMM-MF20-NDEPOT      PIC X(3).                                 
             10 COMM-MF20-CVALORISAT  PIC X(1).                                 
             10 COMM-MF20-MAJ         PIC X(1).                                 
          05 COMM-MF20-DVALIDITE      PIC X(8).                                 
          05 COMM-MF20-CTYPCDE        PIC X(5).                                 
          05 COMM-MF20-LENTCDE        PIC X(20).                        00970000
          05 COMM-MF20-LINTERLOCUT    PIC X(20).                                
E0278     05 COMM-MF20-NCODIC         PIC X(7).                                 
E0278     05 COMM-MF20-CDEMONOCDIC    PIC X(1).                                 
          05 COMM-MF20-SORTIE.                                                  
             10 COMM-MF20-NSEQERR     PIC X(4).                                 
             10 COMM-MF20-LIBERR      PIC X(52).                                
E0278 *   05 COMM-MF20-FILLER         PIC X(3818).                              
E0278     05 COMM-MF20-FILLER         PIC X(3810).                              
                                                                                
