      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
       01  TS-RM57-LONG             PIC S9(4) COMP-3 VALUE +1331.               
       01  TS-RM57-RECORD.                                                      
           05 TS-RM57-CLEREPART        PIC X(5).                                
           05 TS-RM57-LIGNE  OCCURS 17.                                         
              10 TS-RM57-NSOC1      PIC X(3).                                   
              10 TS-RM57-NMAG1      PIC X(3).                                   
              10 TS-RM57-LMAG1      PIC X(20).                                  
              10 TS-RM57-QTE1       PIC X(3).                                   
              10 TS-RM57-NSOC2      PIC X(3).                                   
              10 TS-RM57-NMAG2      PIC X(3).                                   
              10 TS-RM57-LMAG2      PIC X(20).                                  
              10 TS-RM57-QTE2       PIC X(3).                                   
              10 TS-RM57-NSOC3      PIC X(3).                                   
              10 TS-RM57-NMAG3      PIC X(3).                                   
              10 TS-RM57-LMAG3      PIC X(20).                                  
              10 TS-RM57-QTE3       PIC X(3).                                   
                                                                                
