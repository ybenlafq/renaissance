      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * COM-RM00-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-RM00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-RM00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA                                                  
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS                                   
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION                     
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC XX.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-FILLER     PIC X(14).                                   
      *   ZONE INTERNATIONALE                                                   
           02 COMM-ARIANE-CODLANG  PIC X(02).                                   
           02 COMM-ARIANE-CODPIC   PIC X(02).                                   
      *                                                                         
      * ZONES RESERVEES APPLICATIVES                                            
      *                                                                         
      *                                                                         
           02  COMM-RM00-APPLI.                                                 
      *=> ZONES DE SAUVEGARDE COMMUNES AU MENU PRINCIPAL                        
      *=> ET AUX TRANSACTIONS DE NIVEAU INFERIEUR                               
              03  COMM-00.                                                      
                  05  COMM-00-CFLGMAG        PIC X(05).                         
                  05  COMM-00-DPARIS         PIC X(01).                         
                  05  COMM-00-NSOC           PIC X(03).                         
                  05  COMM-00-MZONCMD        PIC X(15).                         
                  05  COMM-00-MCFONC         PIC X(3).                          
                  05  COMM-00-MCGROUP        PIC X(5).                          
                  05  COMM-00-MLGROUP        PIC X(20).                         
                  05  COMM-00-MCFAM          PIC X(5).                          
                  05  COMM-00-MNSOC          PIC X(3).                          
                  05  COMM-00-MNLIEU         PIC X(3).                          
                  05  COMM-00-MNCODIC        PIC X(7).                          
                  05  COMM-00-MNTOL          PIC X(2).                          
                  05  COMM-00-MNPDSJ         PIC X(2).                          
      *=> ZONES DE SAUVEGARDE PROPRES A UNE TRANSACTION ACTIVE                  
      *=> DE NIVEAU INFERIEUR                                                   
           02  COMM-01-02-03-04-06-07-08-09  PIC X(3798).                       
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM01                     
           02  COMM-01 REDEFINES                                                
               COMM-01-02-03-04-06-07-08-09.                                    
               03  COMM-01-SAI-GROUPE        PIC X(01).                         
               03  COMM-01-NEW-PAGE          PIC X(01).                         
               03  COMM-01-NB-CODE           PIC 9(04).                         
               03  COMM-01-NB-PAGE           PIC 9(02).                         
               03  COMM-01-CURRENT-PAGE      PIC 9(02).                         
               03  COMM-01-FLAG-DELETE       PIC X(01).                         
               03  COMM-01-MAJ               PIC X(01).                         
               03  COMM-01-NSOCGRP           PIC X(03).                         
      *-  TABLE DE CONTROLE DES DOUBLONS                                        
               03  COMM-01-TABLE.                                               
                   05  COMM-01-T-CODE OCCURS 450.                               
                       07   COMM-01-T-NSOC    PIC X(03).                        
                       07   COMM-01-T-NLIEU   PIC X(03).                        
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM09                     
           02  COMM-09 REDEFINES                                                
               COMM-01-02-03-04-06-07-08-09.                                    
               03  COMM-09-FLAG-NEW          PIC X(01).                         
               03  COMM-09-NEW-PAGE          PIC X(01).                         
               03  COMM-09-NB-PAGE           PIC 9(03).                         
               03  COMM-09-NB-COLS           PIC 9(02).                         
               03  COMM-09-CURRENT-PAGE      PIC 9(03).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  COMM-09-NB-TS-ITEM        PIC 9(18) COMP.                    
      *--                                                                       
               03  COMM-09-NB-TS-ITEM        PIC 9(18) COMP-5.                  
      *}                                                                        
               03  COMM-09-TABLE.                                               
                   05  COMM-09-T-INDISPO OCCURS 10.                             
                       07   COMM-09-INDISPO   PIC S9(03)V99 COMP-3.             
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM04                     
           02  COMM-04 REDEFINES                                                
               COMM-01-02-03-04-06-07-08-09.                                    
               03  COMM-04-PAGE              PIC 9(03).                         
               03  COMM-04-PAGE-MAX          PIC 9(03).                         
YS2594         03  COMM-04-PAGELAT           PIC 9(01).                         
               03  COMM-04-RESERV            PIC X(04).                         
               03  COMM-04-POIDSJG           PIC X(02).                         
               03  COMM-04-FREQG1            PIC X(04).                         
               03  COMM-04-FREQG2            PIC X(04).                         
               03  COMM-04-FREQG3            PIC X(04).                         
               03  COMM-04-FREQG4            PIC X(04).                         
               03  COMM-04-FREQG5            PIC X(04).                         
YS1864         03  COMM-04-FREQG6            PIC X(04).                         
YS2594         03  COMM-04-FREQG7            PIC X(04).                         
YS2594         03  COMM-04-FREQG8            PIC X(04).                         
YS2594         03  COMM-04-FREQG9            PIC X(04).                         
YS2594         03  COMM-04-FREQG10           PIC X(04).                         
YS2594         03  COMM-04-FREQG11           PIC X(04).                         
YS2594         03  COMM-04-FREQG12           PIC X(04).                         
               03  COMM-04-CSELA1            PIC X(05).                         
               03  COMM-04-CSELA2            PIC X(05).                         
               03  COMM-04-CSELA3            PIC X(05).                         
               03  COMM-04-CSELA4            PIC X(05).                         
               03  COMM-04-CSELA5            PIC X(05).                         
YS1864         03  COMM-04-CSELA6            PIC X(05).                         
YS2594         03  COMM-04-CSELA7            PIC X(05).                         
YS2594         03  COMM-04-CSELA8            PIC X(05).                         
YS2594         03  COMM-04-CSELA9            PIC X(05).                         
YS2594         03  COMM-04-CSELA10           PIC X(05).                         
YS2594         03  COMM-04-CSELA11           PIC X(05).                         
YS2594         03  COMM-04-CSELA12           PIC X(05).                         
               03  COMM-04-CSELA1-OLD        PIC X(05).                         
               03  COMM-04-CSELA2-OLD        PIC X(05).                         
               03  COMM-04-CSELA3-OLD        PIC X(05).                         
               03  COMM-04-CSELA4-OLD        PIC X(05).                         
               03  COMM-04-CSELA5-OLD        PIC X(05).                         
YS1864         03  COMM-04-CSELA6-OLD        PIC X(05).                         
YS2594         03  COMM-04-CSELA7-OLD        PIC X(05).                         
YS2594         03  COMM-04-CSELA8-OLD        PIC X(05).                         
YS2594         03  COMM-04-CSELA9-OLD        PIC X(05).                         
YS2594         03  COMM-04-CSELA10-OLD        PIC X(05).                        
YS2594         03  COMM-04-CSELA11-OLD        PIC X(05).                        
YS2594         03  COMM-04-CSELA12-OLD        PIC X(05).                        
               03  COMM-04-EXCFG             PIC X.                             
               03  COMM-04-NBR-FLAG          PIC 99.                            
               03  COMM-04-T-NSOCDIF.                                           
                   05  COMM-04-NSOCDIF  OCCURS 100 PIC XXX.                     
YS1864*        03  COMM-04-05-06             PIC X(3411).                       
YS1864*YS2594  03  COMM-04-05-06             PIC X(3399).                       
YS2594         03  COMM-04-05-06             PIC X(3229).                       
               03  COMM-05 REDEFINES COMM-04-05-06.                             
                   05 COMM-05-MNSOC          PIC X(03).                         
                   05 COMM-05-MNMAG          PIC X(03).                         
                   05 COMM-05-MLMAG          PIC X(20).                         
               03  COMM-06 REDEFINES COMM-04-05-06.                             
                   05 COMM-06-MNSOC          PIC X(03).                         
                   05 COMM-06-MNMAG          PIC X(03).                         
                   05 COMM-06-MLMAG          PIC X(20).                         
                   05 COMM-06-PAGE           PIC 9(03).                         
                   05 COMM-06-PAGE-MAX       PIC 9(03).                         
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM02                     
           02  COMM-OPTION2  REDEFINES COMM-01-02-03-04-06-07-08-09.            
               04 COMM-RM02-PAGE            PIC  9(3) COMP-3.                   
               04 COMM-RM02-PAGE-MAX        PIC  9(3) COMP-3.                   
               04 COMM-RM02-RUPTURE         PIC X(2).                           
               04 COMM-RM02-LISSAGE         PIC X(4).                           
               04 COMM-RM02-VENTE           PIC X(6).                           
               04 COMM-RM02-DEBAPPLI        PIC X(10).                          
               04 COMM-RM02-FINAPPLI        PIC X(10).                          
               04 COMM-RM02-ITEMAX          PIC S9(4) COMP-3.                   
               04 COMM-RM02-MODIF-DONNEE    PIC X.                              
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM03                     
           02  COMM-OPTION3  REDEFINES COMM-01-02-03-04-06-07-08-09.            
               04 COMM-RM03-PAGE            PIC  9(3) COMP-3.                   
               04 COMM-RM03-PAGE-MAX        PIC  9(3) COMP-3.                   
               04 COMM-RM03-INDISP          PIC  X(5).                          
               04 COMM-RM03-POND            PIC  X(4).                          
               04 COMM-RM03-INDICE          PIC  X.                             
               04 COMM-RM03-NBSEM           PIC  X.                             
               04 COMM-RM03-SEUIL           PIC  X(6).                          
               04 COMM-RM03-PVM             PIC  X(4).                          
               04 COMM-RM03-ITEMAX          PIC S9(4) COMP-3.                   
               04 COMM-RM03-MODIF-DONNEE    PIC X.                              
               04 COMM-RM03-NTABLE          PIC XX.                             
               04 COMM-RM03-NTABLESA        PIC XX.                             
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM05                     
           02  COMM-08 REDEFINES                                                
               COMM-01-02-03-04-06-07-08-09.                                    
               03  COMM-08-PAGE              PIC 9(03).                         
               03  COMM-08-PAGE-MAX          PIC 9(03).                         
           02  COMM-RM11 REDEFINES                                              
               COMM-01-02-03-04-06-07-08-09.                                    
               03  COMM-RM11-PAGE              PIC 9(03).                       
               03  COMM-RM11-PAGE-MAX          PIC 9(03).                       
               03  COMM-RM11-MODIF-DONNEE      PIC X.                           
               03  COMM-RM11-ITEMAX            PIC S9(5) COMP-3.                
               03  COMM-RM11-VALMAX            OCCURS 3.                        
                   05 COMM-RM11-VAL-MAX        PIC S9(5) COMP-3.                
           02  COMM-12 REDEFINES                                                
               COMM-01-02-03-04-06-07-08-09.                                    
               03 COMM-12-TABLE.                                                
                  05 COMM-12-SEMAINE OCCURS 11.                                 
                     10 COMM-12-NUMERO PIC 9(2).                                
                     10 COMM-12-SOMME PIC S9(4)V99 COMP-3.                      
                     10 COMM-12-JOUR OCCURS 7.                                  
                        15 COMM-12-QPOIDS PIC S9(3)V99 COMP-3.                  
               03 COMM-12-TABENT               PIC X(2).                        
           02  COMM-13 REDEFINES                                                
               COMM-01-02-03-04-06-07-08-09.                                    
               04 COMM-13-NUMPAG            PIC 9(3).                   00730818
               04 COMM-13-PAGE-MAX          PIC 9(3).                   00730818
               04 COMM-13-MAJ-TS            PIC X(1).                   00730818
           02  COMM-14 REDEFINES                                                
               COMM-01-02-03-04-06-07-08-09.                                    
               04 COMM-14-NUMPAG            PIC 9(3).                   00730818
               04 COMM-14-PAGE-MAX          PIC 9(3).                   00730818
               04 COMM-14-MAJ-TS            PIC X(1).                   00730818
CL1110     02  COMM-16 REDEFINES                                                
     |         COMM-01-02-03-04-06-07-08-09.                                    
     |         04 COMM-16-NSOC              PIC X(3).                   00730818
     |         04 COMM-16-NUMPAGE           PIC 9(3).                   00730818
     |         04 COMM-16-NB-PAGE-MAX       PIC 9(3).                   00730818
     |         04 COMM-16-MAJ-TS            PIC X(1).                   00730818
                                                                                
