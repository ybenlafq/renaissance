      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MUTTR MUTTR GESTION TRANSIT MUTATION   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMUTTR.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMUTTR.                                                             
      *}                                                                        
           05  MUTTR-CTABLEG2    PIC X(15).                                     
           05  MUTTR-CTABLEG2-REDEF REDEFINES MUTTR-CTABLEG2.                   
               10  MUTTR-SOCDEPO         PIC X(06).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  MUTTR-SOCDEPO-N      REDEFINES MUTTR-SOCDEPO                 
                                         PIC 9(06).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  MUTTR-SOCDEPD         PIC X(06).                             
           05  MUTTR-WTABLEG     PIC X(80).                                     
           05  MUTTR-WTABLEG-REDEF  REDEFINES MUTTR-WTABLEG.                    
               10  MUTTR-ACTIF           PIC X(01).                             
               10  MUTTR-TRANSIT         PIC X(01).                             
               10  MUTTR-WTYPPRET        PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMUTTR-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMUTTR-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MUTTR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MUTTR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MUTTR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MUTTR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
