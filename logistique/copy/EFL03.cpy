      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGQ01   EGQ01                                              00000020
      ***************************************************************** 00000030
       01   EFL03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPMAXL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPMAXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPMAXF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPMAXI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLDEPOTI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQDELAIAPPRO1L      COMP PIC S9(4).                       00000340
      *--                                                                       
           02 MQDELAIAPPRO1L COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MQDELAIAPPRO1F      PIC X.                                00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MQDELAIAPPRO1I      PIC X(3).                             00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCOLICDE1L    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MQCOLICDE1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQCOLICDE1F    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MQCOLICDE1I    PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCOLIRECEPT1L      COMP PIC S9(4).                       00000420
      *--                                                                       
           02 MQCOLIRECEPT1L COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MQCOLIRECEPT1F      PIC X.                                00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MQCOLIRECEPT1I      PIC X(5).                             00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCOLIDSTOCK1L      COMP PIC S9(4).                       00000460
      *--                                                                       
           02 MQCOLIDSTOCK1L COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MQCOLIDSTOCK1F      PIC X.                                00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MQCOLIDSTOCK1I      PIC X(5).                             00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOTEHOLD1L   COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCCOTEHOLD1L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCCOTEHOLD1F   PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCCOTEHOLD1I   PIC X.                                     00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCZONEACCES1L  COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MCZONEACCES1L COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MCZONEACCES1F  PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCZONEACCES1I  PIC X.                                     00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCQUOTA1L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCQUOTA1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCQUOTA1F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCQUOTA1I      PIC X(5).                                  00000610
           02 MTABLEI OCCURS   13 TIMES .                               00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCFAMI  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLFAMI  PIC X(20).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQDELAIAPPROL     COMP PIC S9(4).                       00000710
      *--                                                                       
             03 MQDELAIAPPROL COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 MQDELAIAPPROF     PIC X.                                00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQDELAIAPPROI     PIC X(3).                             00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOLICDEL   COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MQCOLICDEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQCOLICDEF   PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQCOLICDEI   PIC X(3).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOLIRECEPTL     COMP PIC S9(4).                       00000790
      *--                                                                       
             03 MQCOLIRECEPTL COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 MQCOLIRECEPTF     PIC X.                                00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQCOLIRECEPTI     PIC X(5).                             00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOLIDSTOCKL     COMP PIC S9(4).                       00000830
      *--                                                                       
             03 MQCOLIDSTOCKL COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 MQCOLIDSTOCKF     PIC X.                                00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQCOLIDSTOCKI     PIC X(5).                             00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCOTEHOLDL  COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCCOTEHOLDL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCCOTEHOLDF  PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCCOTEHOLDI  PIC X.                                     00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCZONEACCESL      COMP PIC S9(4).                       00000910
      *--                                                                       
             03 MCZONEACCESL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MCZONEACCESF      PIC X.                                00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCZONEACCESI      PIC X.                                00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCQUOTAL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MCQUOTAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCQUOTAF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MCQUOTAI     PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(78).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: EGQ01   EGQ01                                              00001200
      ***************************************************************** 00001210
       01   EFL03O REDEFINES EFL03I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNPAGEA   PIC X.                                          00001390
           02 MNPAGEC   PIC X.                                          00001400
           02 MNPAGEP   PIC X.                                          00001410
           02 MNPAGEH   PIC X.                                          00001420
           02 MNPAGEV   PIC X.                                          00001430
           02 MNPAGEO   PIC ZZZ.                                        00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNPMAXA   PIC X.                                          00001460
           02 MNPMAXC   PIC X.                                          00001470
           02 MNPMAXP   PIC X.                                          00001480
           02 MNPMAXH   PIC X.                                          00001490
           02 MNPMAXV   PIC X.                                          00001500
           02 MNPMAXO   PIC ZZZ.                                        00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MNSOCA    PIC X.                                          00001530
           02 MNSOCC    PIC X.                                          00001540
           02 MNSOCP    PIC X.                                          00001550
           02 MNSOCH    PIC X.                                          00001560
           02 MNSOCV    PIC X.                                          00001570
           02 MNSOCO    PIC X(3).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNDEPOTA  PIC X.                                          00001600
           02 MNDEPOTC  PIC X.                                          00001610
           02 MNDEPOTP  PIC X.                                          00001620
           02 MNDEPOTH  PIC X.                                          00001630
           02 MNDEPOTV  PIC X.                                          00001640
           02 MNDEPOTO  PIC X(3).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MLDEPOTA  PIC X.                                          00001670
           02 MLDEPOTC  PIC X.                                          00001680
           02 MLDEPOTP  PIC X.                                          00001690
           02 MLDEPOTH  PIC X.                                          00001700
           02 MLDEPOTV  PIC X.                                          00001710
           02 MLDEPOTO  PIC X(20).                                      00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MQDELAIAPPRO1A      PIC X.                                00001740
           02 MQDELAIAPPRO1C PIC X.                                     00001750
           02 MQDELAIAPPRO1P PIC X.                                     00001760
           02 MQDELAIAPPRO1H PIC X.                                     00001770
           02 MQDELAIAPPRO1V PIC X.                                     00001780
           02 MQDELAIAPPRO1O      PIC ZZZ.                              00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MQCOLICDE1A    PIC X.                                     00001810
           02 MQCOLICDE1C    PIC X.                                     00001820
           02 MQCOLICDE1P    PIC X.                                     00001830
           02 MQCOLICDE1H    PIC X.                                     00001840
           02 MQCOLICDE1V    PIC X.                                     00001850
           02 MQCOLICDE1O    PIC ZZZ.                                   00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MQCOLIRECEPT1A      PIC X.                                00001880
           02 MQCOLIRECEPT1C PIC X.                                     00001890
           02 MQCOLIRECEPT1P PIC X.                                     00001900
           02 MQCOLIRECEPT1H PIC X.                                     00001910
           02 MQCOLIRECEPT1V PIC X.                                     00001920
           02 MQCOLIRECEPT1O      PIC ZZZZZ.                            00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MQCOLIDSTOCK1A      PIC X.                                00001950
           02 MQCOLIDSTOCK1C PIC X.                                     00001960
           02 MQCOLIDSTOCK1P PIC X.                                     00001970
           02 MQCOLIDSTOCK1H PIC X.                                     00001980
           02 MQCOLIDSTOCK1V PIC X.                                     00001990
           02 MQCOLIDSTOCK1O      PIC ZZZZZ.                            00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCCOTEHOLD1A   PIC X.                                     00002020
           02 MCCOTEHOLD1C   PIC X.                                     00002030
           02 MCCOTEHOLD1P   PIC X.                                     00002040
           02 MCCOTEHOLD1H   PIC X.                                     00002050
           02 MCCOTEHOLD1V   PIC X.                                     00002060
           02 MCCOTEHOLD1O   PIC X.                                     00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MCZONEACCES1A  PIC X.                                     00002090
           02 MCZONEACCES1C  PIC X.                                     00002100
           02 MCZONEACCES1P  PIC X.                                     00002110
           02 MCZONEACCES1H  PIC X.                                     00002120
           02 MCZONEACCES1V  PIC X.                                     00002130
           02 MCZONEACCES1O  PIC X.                                     00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCQUOTA1A      PIC X.                                     00002160
           02 MCQUOTA1C PIC X.                                          00002170
           02 MCQUOTA1P PIC X.                                          00002180
           02 MCQUOTA1H PIC X.                                          00002190
           02 MCQUOTA1V PIC X.                                          00002200
           02 MCQUOTA1O      PIC X(5).                                  00002210
           02 MTABLEO OCCURS   13 TIMES .                               00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MCFAMA  PIC X.                                          00002240
             03 MCFAMC  PIC X.                                          00002250
             03 MCFAMP  PIC X.                                          00002260
             03 MCFAMH  PIC X.                                          00002270
             03 MCFAMV  PIC X.                                          00002280
             03 MCFAMO  PIC X(5).                                       00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MLFAMA  PIC X.                                          00002310
             03 MLFAMC  PIC X.                                          00002320
             03 MLFAMP  PIC X.                                          00002330
             03 MLFAMH  PIC X.                                          00002340
             03 MLFAMV  PIC X.                                          00002350
             03 MLFAMO  PIC X(20).                                      00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MQDELAIAPPROA     PIC X.                                00002380
             03 MQDELAIAPPROC     PIC X.                                00002390
             03 MQDELAIAPPROP     PIC X.                                00002400
             03 MQDELAIAPPROH     PIC X.                                00002410
             03 MQDELAIAPPROV     PIC X.                                00002420
             03 MQDELAIAPPROO     PIC ZZZ.                              00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MQCOLICDEA   PIC X.                                     00002450
             03 MQCOLICDEC   PIC X.                                     00002460
             03 MQCOLICDEP   PIC X.                                     00002470
             03 MQCOLICDEH   PIC X.                                     00002480
             03 MQCOLICDEV   PIC X.                                     00002490
             03 MQCOLICDEO   PIC ZZZ.                                   00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MQCOLIRECEPTA     PIC X.                                00002520
             03 MQCOLIRECEPTC     PIC X.                                00002530
             03 MQCOLIRECEPTP     PIC X.                                00002540
             03 MQCOLIRECEPTH     PIC X.                                00002550
             03 MQCOLIRECEPTV     PIC X.                                00002560
             03 MQCOLIRECEPTO     PIC ZZZZZ.                            00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MQCOLIDSTOCKA     PIC X.                                00002590
             03 MQCOLIDSTOCKC     PIC X.                                00002600
             03 MQCOLIDSTOCKP     PIC X.                                00002610
             03 MQCOLIDSTOCKH     PIC X.                                00002620
             03 MQCOLIDSTOCKV     PIC X.                                00002630
             03 MQCOLIDSTOCKO     PIC ZZZZZ.                            00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MCCOTEHOLDA  PIC X.                                     00002660
             03 MCCOTEHOLDC  PIC X.                                     00002670
             03 MCCOTEHOLDP  PIC X.                                     00002680
             03 MCCOTEHOLDH  PIC X.                                     00002690
             03 MCCOTEHOLDV  PIC X.                                     00002700
             03 MCCOTEHOLDO  PIC X.                                     00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MCZONEACCESA      PIC X.                                00002730
             03 MCZONEACCESC PIC X.                                     00002740
             03 MCZONEACCESP PIC X.                                     00002750
             03 MCZONEACCESH PIC X.                                     00002760
             03 MCZONEACCESV PIC X.                                     00002770
             03 MCZONEACCESO      PIC X.                                00002780
             03 FILLER       PIC X(2).                                  00002790
             03 MCQUOTAA     PIC X.                                     00002800
             03 MCQUOTAC     PIC X.                                     00002810
             03 MCQUOTAP     PIC X.                                     00002820
             03 MCQUOTAH     PIC X.                                     00002830
             03 MCQUOTAV     PIC X.                                     00002840
             03 MCQUOTAO     PIC X(5).                                  00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(78).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
