      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVEF1100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEF1100                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVEF1100.                                                            
           02  EF11-CTRAIT                                                      
               PIC X(0005).                                                     
           02  EF11-CTIERS                                                      
               PIC X(0005).                                                     
           02  EF11-NRENDU                                                      
               PIC X(0020).                                                     
           02  EF11-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  EF11-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  EF11-NORIGINE                                                    
               PIC X(0007).                                                     
           02  EF11-NCODIC                                                      
               PIC X(0007).                                                     
           02  EF11-NENVOI                                                      
               PIC X(0007).                                                     
           02  EF11-QTRENDU                                                     
               PIC S9(3) COMP-3.                                                
           02  EF11-MTAVO                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  EF11-NSOCMVTD                                                    
               PIC X(0003).                                                     
           02  EF11-NLIEUMVTD                                                   
               PIC X(0003).                                                     
           02  EF11-NSSLIEUMVTD                                                 
               PIC X(0003).                                                     
           02  EF11-CLIEUTRTMVTD                                                
               PIC X(0005).                                                     
           02  EF11-DANNUL                                                      
               PIC X(0008).                                                     
           02  EF11-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEF1100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVEF1100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-CTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-CTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-CTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-CTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-NRENDU-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-NRENDU-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-NORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-NORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-NENVOI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-NENVOI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-QTRENDU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-QTRENDU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-MTAVO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-MTAVO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-NSOCMVTD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-NSOCMVTD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-NLIEUMVTD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-NLIEUMVTD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-NSSLIEUMVTD-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-NSSLIEUMVTD-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-CLIEUTRTMVTD-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-CLIEUTRTMVTD-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF11-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF11-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
