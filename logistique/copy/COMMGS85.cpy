      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      * COMMAREA SPECIFIQUE PRG TGS85 (TGS50 -> MENU)    TR: GS50  *    00030000
      *                                                            *    00031000
      *                       STOCK TRANSIT D'UN ARTICLE           *    00040000
      *                                                            *    00050000
      * ZONES RESERVEES APPLICATIVES ------------------------------*    00060003
      *                                                            *    00070000
      *        TRANSACTION GS85                                    *    00080000
           02 COMM-GS85-APPLI   REDEFINES COMM-GS50-APPLI.              00721000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-GS85-ITEM       PIC S9(4)    COMP.                00743004
      *--                                                                       
              03 COMM-GS85-ITEM       PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-GS85-POS-MAX    PIC S9(4)    COMP.                00743005
      *--                                                                       
              03 COMM-GS85-POS-MAX    PIC S9(4) COMP-5.                         
      *}                                                                        
              03 COMM-GS85-VENDABLE   PIC S9(7)    COMP-3.              00743006
              03 COMM-GS85-COTE       PIC S9(7)    COMP-3.              00743007
              03 COMM-GS85-TRANSIT    PIC S9(7)    COMP-3.              00743008
              03 COMM-GS85-PRET       PIC S9(7)    COMP-3.              00743009
              03 COMM-GS85-HS         PIC S9(7)    COMP-3.              00743010
              03 COMM-GS85-TOTAL      PIC S9(7)    COMP-3.              00743011
              03 COMM-GS85-LITIGE     PIC S9(7)    COMP-3.              00743012
              03 COMM-GS85-DISPONIBLE PIC S9(7)    COMP-3.              00743013
              03 COMM-GS85-RESDISPO   PIC S9(7)    COMP-3.              00743014
              03 COMM-GS85-RESFOUR    PIC S9(7)    COMP-3.              00743015
              03 COMM-GS85-RESCDE     PIC S9(7)    COMP-3.              00743016
              03 FILLER               PIC X(2683).                      00743020
                                                                                
                                                                        00750000
