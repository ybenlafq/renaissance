      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVIE1000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIE1000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIE1000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIE1000.                                                            
      *}                                                                        
           02  IE10-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  IE10-NDEPOT                                                      
               PIC X(0003).                                                     
           02  IE10-WJIE311                                                     
               PIC X(0001).                                                     
           02  IE10-WEMPL                                                       
               PIC X(0001).                                                     
           02  IE10-WSTOCKINV                                                   
               PIC X(0001).                                                     
           02  IE10-WRTGS10                                                     
               PIC X(0001).                                                     
           02  IE10-WSTBATCHTLM                                                 
               PIC X(0001).                                                     
           02  IE10-WSTBATCHELA                                                 
               PIC X(0001).                                                     
           02  IE10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVIE1000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIE1000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIE1000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WJIE311-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WJIE311-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WEMPL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WEMPL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WSTOCKINV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WSTOCKINV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WRTGS10-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WRTGS10-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WSTBATCHTLM-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WSTBATCHTLM-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-WSTBATCHELA-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE10-WSTBATCHELA-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  IE10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
