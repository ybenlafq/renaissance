      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM38   ERM38                                              00000020
      ***************************************************************** 00000030
       01   ERM38I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPGENL    COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MCGROUPGENL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCGROUPGENF    PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCGROUPGENI    PIC X(5).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGROUPGENL    COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLGROUPGENL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLGROUPGENF    PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLGROUPGENI    PIC X(20).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMARQI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFFOURNI    PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCEXPOI   PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOLLECTL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCOLLECTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOLLECTF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCOLLECTI      PIC X(2).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSAPPL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MSENSAPPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSENSAPPF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSENSAPPI      PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREPACTL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MREPACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MREPACTF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MREPACTI  PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREPQEXPOL     COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MREPQEXPOL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MREPQEXPOF     PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MREPQEXPOI     PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREPQLSL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MREPQLSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MREPQLSF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MREPQLSI  PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWREFRL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MWREFRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWREFRF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MWREFRI   PIC X.                                          00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIBELLEI      PIC X(4).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICRL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MNCODICRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICRF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNCODICRI      PIC X(7).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMRL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCFAMRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMRF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCFAMRI   PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCMARQRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCMARQRI  PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFOURNRL    COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MLREFOURNRL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFOURNRF    PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLREFOURNRI    PIC X(20).                                 00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPORL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCEXPORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCEXPORF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCEXPORI  PIC X.                                          00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOLLECTRL     COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MCOLLECTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCOLLECTRF     PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCOLLECTRI     PIC X(2).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFORCAGEL     COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MWFORCAGEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWFORCAGEF     PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MWFORCAGEI     PIC X.                                     00000970
           02 MTABI OCCURS   10 TIMES .                                 00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGROUPL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MCGROUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCGROUPF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCGROUPI     PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLGROUPL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MLGROUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLGROUPF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MLGROUPI     PIC X(20).                                 00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMAGACTL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MMAGACTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMAGACTF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MMAGACTI     PIC X(5).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMAGSIACTL   COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MMAGSIACTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MMAGSIACTF   PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MMAGSIACTI   PIC X(5).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMAGASL      COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MMAGASL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMAGASF      PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MMAGASI      PIC X(5).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTOTL   COMP PIC S9(4).                                 00001190
      *--                                                                       
             03 MTOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MTOTF   PIC X.                                          00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MTOTI   PIC X(5).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00001230
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MACTI   PIC X.                                          00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPOL      COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MQEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQEXPOF      PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MQEXPOI      PIC X(3).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLSL   COMP PIC S9(4).                                 00001310
      *--                                                                       
             03 MQLSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQLSF   PIC X.                                          00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MQLSI   PIC X(3).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWREFL  COMP PIC S9(4).                                 00001350
      *--                                                                       
             03 MWREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWREFF  PIC X.                                          00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MWREFI  PIC X.                                          00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTACTL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MTACTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTACTF    PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MTACTI    PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTSIACTL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MTSIACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTSIACTF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MTSIACTI  PIC X(5).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTASL     COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MTASL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTASF     PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MTASI     PIC X(5).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTOTL    COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MTTOTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTTOTF    PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MTTOTI    PIC X(5).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MZONCMDI  PIC X(12).                                      00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MLIBERRI  PIC X(61).                                      00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MCODTRAI  PIC X(4).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCICSI    PIC X(5).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MNETNAMI  PIC X(8).                                       00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MSCREENI  PIC X(4).                                       00001780
      ***************************************************************** 00001790
      * SDF: ERM38   ERM38                                              00001800
      ***************************************************************** 00001810
       01   ERM38O REDEFINES ERM38I.                                    00001820
           02 FILLER    PIC X(12).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDATJOUA  PIC X.                                          00001850
           02 MDATJOUC  PIC X.                                          00001860
           02 MDATJOUP  PIC X.                                          00001870
           02 MDATJOUH  PIC X.                                          00001880
           02 MDATJOUV  PIC X.                                          00001890
           02 MDATJOUO  PIC X(10).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MTIMJOUA  PIC X.                                          00001920
           02 MTIMJOUC  PIC X.                                          00001930
           02 MTIMJOUP  PIC X.                                          00001940
           02 MTIMJOUH  PIC X.                                          00001950
           02 MTIMJOUV  PIC X.                                          00001960
           02 MTIMJOUO  PIC X(5).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCGROUPGENA    PIC X.                                     00001990
           02 MCGROUPGENC    PIC X.                                     00002000
           02 MCGROUPGENP    PIC X.                                     00002010
           02 MCGROUPGENH    PIC X.                                     00002020
           02 MCGROUPGENV    PIC X.                                     00002030
           02 MCGROUPGENO    PIC X(5).                                  00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MLGROUPGENA    PIC X.                                     00002060
           02 MLGROUPGENC    PIC X.                                     00002070
           02 MLGROUPGENP    PIC X.                                     00002080
           02 MLGROUPGENH    PIC X.                                     00002090
           02 MLGROUPGENV    PIC X.                                     00002100
           02 MLGROUPGENO    PIC X(20).                                 00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNCODICA  PIC X.                                          00002130
           02 MNCODICC  PIC X.                                          00002140
           02 MNCODICP  PIC X.                                          00002150
           02 MNCODICH  PIC X.                                          00002160
           02 MNCODICV  PIC X.                                          00002170
           02 MNCODICO  PIC X(7).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCFAMA    PIC X.                                          00002200
           02 MCFAMC    PIC X.                                          00002210
           02 MCFAMP    PIC X.                                          00002220
           02 MCFAMH    PIC X.                                          00002230
           02 MCFAMV    PIC X.                                          00002240
           02 MCFAMO    PIC X(5).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MCMARQA   PIC X.                                          00002270
           02 MCMARQC   PIC X.                                          00002280
           02 MCMARQP   PIC X.                                          00002290
           02 MCMARQH   PIC X.                                          00002300
           02 MCMARQV   PIC X.                                          00002310
           02 MCMARQO   PIC X(5).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MLREFFOURNA    PIC X.                                     00002340
           02 MLREFFOURNC    PIC X.                                     00002350
           02 MLREFFOURNP    PIC X.                                     00002360
           02 MLREFFOURNH    PIC X.                                     00002370
           02 MLREFFOURNV    PIC X.                                     00002380
           02 MLREFFOURNO    PIC X(20).                                 00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MCEXPOA   PIC X.                                          00002410
           02 MCEXPOC   PIC X.                                          00002420
           02 MCEXPOP   PIC X.                                          00002430
           02 MCEXPOH   PIC X.                                          00002440
           02 MCEXPOV   PIC X.                                          00002450
           02 MCEXPOO   PIC X.                                          00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MCOLLECTA      PIC X.                                     00002480
           02 MCOLLECTC PIC X.                                          00002490
           02 MCOLLECTP PIC X.                                          00002500
           02 MCOLLECTH PIC X.                                          00002510
           02 MCOLLECTV PIC X.                                          00002520
           02 MCOLLECTO      PIC X(2).                                  00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MSENSAPPA      PIC X.                                     00002550
           02 MSENSAPPC PIC X.                                          00002560
           02 MSENSAPPP PIC X.                                          00002570
           02 MSENSAPPH PIC X.                                          00002580
           02 MSENSAPPV PIC X.                                          00002590
           02 MSENSAPPO      PIC X.                                     00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MREPACTA  PIC X.                                          00002620
           02 MREPACTC  PIC X.                                          00002630
           02 MREPACTP  PIC X.                                          00002640
           02 MREPACTH  PIC X.                                          00002650
           02 MREPACTV  PIC X.                                          00002660
           02 MREPACTO  PIC X.                                          00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MREPQEXPOA     PIC X.                                     00002690
           02 MREPQEXPOC     PIC X.                                     00002700
           02 MREPQEXPOP     PIC X.                                     00002710
           02 MREPQEXPOH     PIC X.                                     00002720
           02 MREPQEXPOV     PIC X.                                     00002730
           02 MREPQEXPOO     PIC X(3).                                  00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MREPQLSA  PIC X.                                          00002760
           02 MREPQLSC  PIC X.                                          00002770
           02 MREPQLSP  PIC X.                                          00002780
           02 MREPQLSH  PIC X.                                          00002790
           02 MREPQLSV  PIC X.                                          00002800
           02 MREPQLSO  PIC X(3).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MWREFRA   PIC X.                                          00002830
           02 MWREFRC   PIC X.                                          00002840
           02 MWREFRP   PIC X.                                          00002850
           02 MWREFRH   PIC X.                                          00002860
           02 MWREFRV   PIC X.                                          00002870
           02 MWREFRO   PIC X.                                          00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MLIBELLEA      PIC X.                                     00002900
           02 MLIBELLEC PIC X.                                          00002910
           02 MLIBELLEP PIC X.                                          00002920
           02 MLIBELLEH PIC X.                                          00002930
           02 MLIBELLEV PIC X.                                          00002940
           02 MLIBELLEO      PIC X(4).                                  00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNCODICRA      PIC X.                                     00002970
           02 MNCODICRC PIC X.                                          00002980
           02 MNCODICRP PIC X.                                          00002990
           02 MNCODICRH PIC X.                                          00003000
           02 MNCODICRV PIC X.                                          00003010
           02 MNCODICRO      PIC X(7).                                  00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MCFAMRA   PIC X.                                          00003040
           02 MCFAMRC   PIC X.                                          00003050
           02 MCFAMRP   PIC X.                                          00003060
           02 MCFAMRH   PIC X.                                          00003070
           02 MCFAMRV   PIC X.                                          00003080
           02 MCFAMRO   PIC X(5).                                       00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MCMARQRA  PIC X.                                          00003110
           02 MCMARQRC  PIC X.                                          00003120
           02 MCMARQRP  PIC X.                                          00003130
           02 MCMARQRH  PIC X.                                          00003140
           02 MCMARQRV  PIC X.                                          00003150
           02 MCMARQRO  PIC X(5).                                       00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MLREFOURNRA    PIC X.                                     00003180
           02 MLREFOURNRC    PIC X.                                     00003190
           02 MLREFOURNRP    PIC X.                                     00003200
           02 MLREFOURNRH    PIC X.                                     00003210
           02 MLREFOURNRV    PIC X.                                     00003220
           02 MLREFOURNRO    PIC X(20).                                 00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MCEXPORA  PIC X.                                          00003250
           02 MCEXPORC  PIC X.                                          00003260
           02 MCEXPORP  PIC X.                                          00003270
           02 MCEXPORH  PIC X.                                          00003280
           02 MCEXPORV  PIC X.                                          00003290
           02 MCEXPORO  PIC X.                                          00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MCOLLECTRA     PIC X.                                     00003320
           02 MCOLLECTRC     PIC X.                                     00003330
           02 MCOLLECTRP     PIC X.                                     00003340
           02 MCOLLECTRH     PIC X.                                     00003350
           02 MCOLLECTRV     PIC X.                                     00003360
           02 MCOLLECTRO     PIC X(2).                                  00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MWFORCAGEA     PIC X.                                     00003390
           02 MWFORCAGEC     PIC X.                                     00003400
           02 MWFORCAGEP     PIC X.                                     00003410
           02 MWFORCAGEH     PIC X.                                     00003420
           02 MWFORCAGEV     PIC X.                                     00003430
           02 MWFORCAGEO     PIC X.                                     00003440
           02 MTABO OCCURS   10 TIMES .                                 00003450
             03 FILLER       PIC X(2).                                  00003460
             03 MCGROUPA     PIC X.                                     00003470
             03 MCGROUPC     PIC X.                                     00003480
             03 MCGROUPP     PIC X.                                     00003490
             03 MCGROUPH     PIC X.                                     00003500
             03 MCGROUPV     PIC X.                                     00003510
             03 MCGROUPO     PIC X(5).                                  00003520
             03 FILLER       PIC X(2).                                  00003530
             03 MLGROUPA     PIC X.                                     00003540
             03 MLGROUPC     PIC X.                                     00003550
             03 MLGROUPP     PIC X.                                     00003560
             03 MLGROUPH     PIC X.                                     00003570
             03 MLGROUPV     PIC X.                                     00003580
             03 MLGROUPO     PIC X(20).                                 00003590
             03 FILLER       PIC X(2).                                  00003600
             03 MMAGACTA     PIC X.                                     00003610
             03 MMAGACTC     PIC X.                                     00003620
             03 MMAGACTP     PIC X.                                     00003630
             03 MMAGACTH     PIC X.                                     00003640
             03 MMAGACTV     PIC X.                                     00003650
             03 MMAGACTO     PIC X(5).                                  00003660
             03 FILLER       PIC X(2).                                  00003670
             03 MMAGSIACTA   PIC X.                                     00003680
             03 MMAGSIACTC   PIC X.                                     00003690
             03 MMAGSIACTP   PIC X.                                     00003700
             03 MMAGSIACTH   PIC X.                                     00003710
             03 MMAGSIACTV   PIC X.                                     00003720
             03 MMAGSIACTO   PIC X(5).                                  00003730
             03 FILLER       PIC X(2).                                  00003740
             03 MMAGASA      PIC X.                                     00003750
             03 MMAGASC PIC X.                                          00003760
             03 MMAGASP PIC X.                                          00003770
             03 MMAGASH PIC X.                                          00003780
             03 MMAGASV PIC X.                                          00003790
             03 MMAGASO      PIC X(5).                                  00003800
             03 FILLER       PIC X(2).                                  00003810
             03 MTOTA   PIC X.                                          00003820
             03 MTOTC   PIC X.                                          00003830
             03 MTOTP   PIC X.                                          00003840
             03 MTOTH   PIC X.                                          00003850
             03 MTOTV   PIC X.                                          00003860
             03 MTOTO   PIC X(5).                                       00003870
             03 FILLER       PIC X(2).                                  00003880
             03 MACTA   PIC X.                                          00003890
             03 MACTC   PIC X.                                          00003900
             03 MACTP   PIC X.                                          00003910
             03 MACTH   PIC X.                                          00003920
             03 MACTV   PIC X.                                          00003930
             03 MACTO   PIC X.                                          00003940
             03 FILLER       PIC X(2).                                  00003950
             03 MQEXPOA      PIC X.                                     00003960
             03 MQEXPOC PIC X.                                          00003970
             03 MQEXPOP PIC X.                                          00003980
             03 MQEXPOH PIC X.                                          00003990
             03 MQEXPOV PIC X.                                          00004000
             03 MQEXPOO      PIC X(3).                                  00004010
             03 FILLER       PIC X(2).                                  00004020
             03 MQLSA   PIC X.                                          00004030
             03 MQLSC   PIC X.                                          00004040
             03 MQLSP   PIC X.                                          00004050
             03 MQLSH   PIC X.                                          00004060
             03 MQLSV   PIC X.                                          00004070
             03 MQLSO   PIC X(3).                                       00004080
             03 FILLER       PIC X(2).                                  00004090
             03 MWREFA  PIC X.                                          00004100
             03 MWREFC  PIC X.                                          00004110
             03 MWREFP  PIC X.                                          00004120
             03 MWREFH  PIC X.                                          00004130
             03 MWREFV  PIC X.                                          00004140
             03 MWREFO  PIC X.                                          00004150
           02 FILLER    PIC X(2).                                       00004160
           02 MTACTA    PIC X.                                          00004170
           02 MTACTC    PIC X.                                          00004180
           02 MTACTP    PIC X.                                          00004190
           02 MTACTH    PIC X.                                          00004200
           02 MTACTV    PIC X.                                          00004210
           02 MTACTO    PIC X(5).                                       00004220
           02 FILLER    PIC X(2).                                       00004230
           02 MTSIACTA  PIC X.                                          00004240
           02 MTSIACTC  PIC X.                                          00004250
           02 MTSIACTP  PIC X.                                          00004260
           02 MTSIACTH  PIC X.                                          00004270
           02 MTSIACTV  PIC X.                                          00004280
           02 MTSIACTO  PIC X(5).                                       00004290
           02 FILLER    PIC X(2).                                       00004300
           02 MTASA     PIC X.                                          00004310
           02 MTASC     PIC X.                                          00004320
           02 MTASP     PIC X.                                          00004330
           02 MTASH     PIC X.                                          00004340
           02 MTASV     PIC X.                                          00004350
           02 MTASO     PIC X(5).                                       00004360
           02 FILLER    PIC X(2).                                       00004370
           02 MTTOTA    PIC X.                                          00004380
           02 MTTOTC    PIC X.                                          00004390
           02 MTTOTP    PIC X.                                          00004400
           02 MTTOTH    PIC X.                                          00004410
           02 MTTOTV    PIC X.                                          00004420
           02 MTTOTO    PIC X(5).                                       00004430
           02 FILLER    PIC X(2).                                       00004440
           02 MZONCMDA  PIC X.                                          00004450
           02 MZONCMDC  PIC X.                                          00004460
           02 MZONCMDP  PIC X.                                          00004470
           02 MZONCMDH  PIC X.                                          00004480
           02 MZONCMDV  PIC X.                                          00004490
           02 MZONCMDO  PIC X(12).                                      00004500
           02 FILLER    PIC X(2).                                       00004510
           02 MLIBERRA  PIC X.                                          00004520
           02 MLIBERRC  PIC X.                                          00004530
           02 MLIBERRP  PIC X.                                          00004540
           02 MLIBERRH  PIC X.                                          00004550
           02 MLIBERRV  PIC X.                                          00004560
           02 MLIBERRO  PIC X(61).                                      00004570
           02 FILLER    PIC X(2).                                       00004580
           02 MCODTRAA  PIC X.                                          00004590
           02 MCODTRAC  PIC X.                                          00004600
           02 MCODTRAP  PIC X.                                          00004610
           02 MCODTRAH  PIC X.                                          00004620
           02 MCODTRAV  PIC X.                                          00004630
           02 MCODTRAO  PIC X(4).                                       00004640
           02 FILLER    PIC X(2).                                       00004650
           02 MCICSA    PIC X.                                          00004660
           02 MCICSC    PIC X.                                          00004670
           02 MCICSP    PIC X.                                          00004680
           02 MCICSH    PIC X.                                          00004690
           02 MCICSV    PIC X.                                          00004700
           02 MCICSO    PIC X(5).                                       00004710
           02 FILLER    PIC X(2).                                       00004720
           02 MNETNAMA  PIC X.                                          00004730
           02 MNETNAMC  PIC X.                                          00004740
           02 MNETNAMP  PIC X.                                          00004750
           02 MNETNAMH  PIC X.                                          00004760
           02 MNETNAMV  PIC X.                                          00004770
           02 MNETNAMO  PIC X(8).                                       00004780
           02 FILLER    PIC X(2).                                       00004790
           02 MSCREENA  PIC X.                                          00004800
           02 MSCREENC  PIC X.                                          00004810
           02 MSCREENP  PIC X.                                          00004820
           02 MSCREENH  PIC X.                                          00004830
           02 MSCREENV  PIC X.                                          00004840
           02 MSCREENO  PIC X(4).                                       00004850
                                                                                
