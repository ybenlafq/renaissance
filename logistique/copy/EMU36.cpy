      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU36   EMU36                                              00000020
      ***************************************************************** 00000030
       01   EMU36I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGEI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENTRL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCENTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCENTRF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCENTRI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMUTATIONL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MDMUTATIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDMUTATIONF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDMUTATIONI    PIC X(10).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDESTL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNSOCDESTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCDESTF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCDESTI     PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUDESTL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNLIEUDESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUDESTF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNLIEUDESTI    PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTATIONSL   COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNMUTATIONSL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNMUTATIONSF   PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNMUTATIONSI   PIC X(7).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSELGLOBL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MWSELGLOBL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWSELGLOBF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MWSELGLOBI     PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFACTPROFSL   COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNFACTPROFSL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNFACTPROFSF   PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNFACTPROFSI   PIC X(8).                                  00000530
           02 MLIGNEI OCCURS   11 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFACTPROFL  COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNFACTPROFL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNFACTPROFF  PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNFACTPROFI  PIC X(8).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTATIONL  COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNMUTATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNMUTATIONF  PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNMUTATIONI  PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDESTOCKL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDDESTOCKL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDDESTOCKF   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDDESTOCKI   PIC X(8).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCIETEL   COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNSOCIETEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCIETEF   PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNSOCIETEI   PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNLIEUI      PIC X(3).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSELARTL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCSELARTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCSELARTF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCSELARTI    PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBREFL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQNBREFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQNBREFF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQNBREFI     PIC X(6).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMUTEEL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQMUTEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQMUTEEF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQMUTEEI     PIC X(6).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFACTL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDFACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDFACTF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDFACTI      PIC X(8).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELINDL    COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MWSELINDL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWSELINDF    PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MWSELINDI    PIC X.                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWDOCEURL    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MWDOCEURL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWDOCEURF    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MWDOCEURI    PIC X.                                     00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MZONCMDI  PIC X(15).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLIBERRI  PIC X(58).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCICSI    PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MNETNAMI  PIC X(8).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MSCREENI  PIC X(4).                                       00001220
      ***************************************************************** 00001230
      * SDF: EMU36   EMU36                                              00001240
      ***************************************************************** 00001250
       01   EMU36O REDEFINES EMU36I.                                    00001260
           02 FILLER    PIC X(12).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTIMJOUA  PIC X.                                          00001360
           02 MTIMJOUC  PIC X.                                          00001370
           02 MTIMJOUP  PIC X.                                          00001380
           02 MTIMJOUH  PIC X.                                          00001390
           02 MTIMJOUV  PIC X.                                          00001400
           02 MTIMJOUO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MPAGEA    PIC X.                                          00001430
           02 MPAGEC    PIC X.                                          00001440
           02 MPAGEP    PIC X.                                          00001450
           02 MPAGEH    PIC X.                                          00001460
           02 MPAGEV    PIC X.                                          00001470
           02 MPAGEO    PIC X(3).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNBPAGEA  PIC X.                                          00001500
           02 MNBPAGEC  PIC X.                                          00001510
           02 MNBPAGEP  PIC X.                                          00001520
           02 MNBPAGEH  PIC X.                                          00001530
           02 MNBPAGEV  PIC X.                                          00001540
           02 MNBPAGEO  PIC X(3).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNSOCENTRA     PIC X.                                     00001570
           02 MNSOCENTRC     PIC X.                                     00001580
           02 MNSOCENTRP     PIC X.                                     00001590
           02 MNSOCENTRH     PIC X.                                     00001600
           02 MNSOCENTRV     PIC X.                                     00001610
           02 MNSOCENTRO     PIC X(3).                                  00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNDEPOTA  PIC X.                                          00001640
           02 MNDEPOTC  PIC X.                                          00001650
           02 MNDEPOTP  PIC X.                                          00001660
           02 MNDEPOTH  PIC X.                                          00001670
           02 MNDEPOTV  PIC X.                                          00001680
           02 MNDEPOTO  PIC X(3).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MDMUTATIONA    PIC X.                                     00001710
           02 MDMUTATIONC    PIC X.                                     00001720
           02 MDMUTATIONP    PIC X.                                     00001730
           02 MDMUTATIONH    PIC X.                                     00001740
           02 MDMUTATIONV    PIC X.                                     00001750
           02 MDMUTATIONO    PIC X(10).                                 00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNSOCDESTA     PIC X.                                     00001780
           02 MNSOCDESTC     PIC X.                                     00001790
           02 MNSOCDESTP     PIC X.                                     00001800
           02 MNSOCDESTH     PIC X.                                     00001810
           02 MNSOCDESTV     PIC X.                                     00001820
           02 MNSOCDESTO     PIC X(3).                                  00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNLIEUDESTA    PIC X.                                     00001850
           02 MNLIEUDESTC    PIC X.                                     00001860
           02 MNLIEUDESTP    PIC X.                                     00001870
           02 MNLIEUDESTH    PIC X.                                     00001880
           02 MNLIEUDESTV    PIC X.                                     00001890
           02 MNLIEUDESTO    PIC X(3).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MNMUTATIONSA   PIC X.                                     00001920
           02 MNMUTATIONSC   PIC X.                                     00001930
           02 MNMUTATIONSP   PIC X.                                     00001940
           02 MNMUTATIONSH   PIC X.                                     00001950
           02 MNMUTATIONSV   PIC X.                                     00001960
           02 MNMUTATIONSO   PIC X(7).                                  00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MWSELGLOBA     PIC X.                                     00001990
           02 MWSELGLOBC     PIC X.                                     00002000
           02 MWSELGLOBP     PIC X.                                     00002010
           02 MWSELGLOBH     PIC X.                                     00002020
           02 MWSELGLOBV     PIC X.                                     00002030
           02 MWSELGLOBO     PIC X.                                     00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MNFACTPROFSA   PIC X.                                     00002060
           02 MNFACTPROFSC   PIC X.                                     00002070
           02 MNFACTPROFSP   PIC X.                                     00002080
           02 MNFACTPROFSH   PIC X.                                     00002090
           02 MNFACTPROFSV   PIC X.                                     00002100
           02 MNFACTPROFSO   PIC X(8).                                  00002110
           02 MLIGNEO OCCURS   11 TIMES .                               00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MNFACTPROFA  PIC X.                                     00002140
             03 MNFACTPROFC  PIC X.                                     00002150
             03 MNFACTPROFP  PIC X.                                     00002160
             03 MNFACTPROFH  PIC X.                                     00002170
             03 MNFACTPROFV  PIC X.                                     00002180
             03 MNFACTPROFO  PIC X(8).                                  00002190
             03 FILLER       PIC X(2).                                  00002200
             03 MNMUTATIONA  PIC X.                                     00002210
             03 MNMUTATIONC  PIC X.                                     00002220
             03 MNMUTATIONP  PIC X.                                     00002230
             03 MNMUTATIONH  PIC X.                                     00002240
             03 MNMUTATIONV  PIC X.                                     00002250
             03 MNMUTATIONO  PIC X(7).                                  00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MDDESTOCKA   PIC X.                                     00002280
             03 MDDESTOCKC   PIC X.                                     00002290
             03 MDDESTOCKP   PIC X.                                     00002300
             03 MDDESTOCKH   PIC X.                                     00002310
             03 MDDESTOCKV   PIC X.                                     00002320
             03 MDDESTOCKO   PIC X(8).                                  00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MNSOCIETEA   PIC X.                                     00002350
             03 MNSOCIETEC   PIC X.                                     00002360
             03 MNSOCIETEP   PIC X.                                     00002370
             03 MNSOCIETEH   PIC X.                                     00002380
             03 MNSOCIETEV   PIC X.                                     00002390
             03 MNSOCIETEO   PIC X(3).                                  00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MNLIEUA      PIC X.                                     00002420
             03 MNLIEUC PIC X.                                          00002430
             03 MNLIEUP PIC X.                                          00002440
             03 MNLIEUH PIC X.                                          00002450
             03 MNLIEUV PIC X.                                          00002460
             03 MNLIEUO      PIC X(3).                                  00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MCSELARTA    PIC X.                                     00002490
             03 MCSELARTC    PIC X.                                     00002500
             03 MCSELARTP    PIC X.                                     00002510
             03 MCSELARTH    PIC X.                                     00002520
             03 MCSELARTV    PIC X.                                     00002530
             03 MCSELARTO    PIC X(5).                                  00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MQNBREFA     PIC X.                                     00002560
             03 MQNBREFC     PIC X.                                     00002570
             03 MQNBREFP     PIC X.                                     00002580
             03 MQNBREFH     PIC X.                                     00002590
             03 MQNBREFV     PIC X.                                     00002600
             03 MQNBREFO     PIC ZZZZZ9.                                00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MQMUTEEA     PIC X.                                     00002630
             03 MQMUTEEC     PIC X.                                     00002640
             03 MQMUTEEP     PIC X.                                     00002650
             03 MQMUTEEH     PIC X.                                     00002660
             03 MQMUTEEV     PIC X.                                     00002670
             03 MQMUTEEO     PIC ZZZZZ9.                                00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MDFACTA      PIC X.                                     00002700
             03 MDFACTC PIC X.                                          00002710
             03 MDFACTP PIC X.                                          00002720
             03 MDFACTH PIC X.                                          00002730
             03 MDFACTV PIC X.                                          00002740
             03 MDFACTO      PIC X(8).                                  00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MWSELINDA    PIC X.                                     00002770
             03 MWSELINDC    PIC X.                                     00002780
             03 MWSELINDP    PIC X.                                     00002790
             03 MWSELINDH    PIC X.                                     00002800
             03 MWSELINDV    PIC X.                                     00002810
             03 MWSELINDO    PIC X.                                     00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MWDOCEURA    PIC X.                                     00002840
             03 MWDOCEURC    PIC X.                                     00002850
             03 MWDOCEURP    PIC X.                                     00002860
             03 MWDOCEURH    PIC X.                                     00002870
             03 MWDOCEURV    PIC X.                                     00002880
             03 MWDOCEURO    PIC X.                                     00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MZONCMDA  PIC X.                                          00002910
           02 MZONCMDC  PIC X.                                          00002920
           02 MZONCMDP  PIC X.                                          00002930
           02 MZONCMDH  PIC X.                                          00002940
           02 MZONCMDV  PIC X.                                          00002950
           02 MZONCMDO  PIC X(15).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLIBERRA  PIC X.                                          00002980
           02 MLIBERRC  PIC X.                                          00002990
           02 MLIBERRP  PIC X.                                          00003000
           02 MLIBERRH  PIC X.                                          00003010
           02 MLIBERRV  PIC X.                                          00003020
           02 MLIBERRO  PIC X(58).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSO    PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNETNAMA  PIC X.                                          00003190
           02 MNETNAMC  PIC X.                                          00003200
           02 MNETNAMP  PIC X.                                          00003210
           02 MNETNAMH  PIC X.                                          00003220
           02 MNETNAMV  PIC X.                                          00003230
           02 MNETNAMO  PIC X(8).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSCREENA  PIC X.                                          00003260
           02 MSCREENC  PIC X.                                          00003270
           02 MSCREENP  PIC X.                                          00003280
           02 MSCREENH  PIC X.                                          00003290
           02 MSCREENV  PIC X.                                          00003300
           02 MSCREENO  PIC X(4).                                       00003310
                                                                                
