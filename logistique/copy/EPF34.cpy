      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Suivi codics pour 1 plateforme                                  00000020
      ***************************************************************** 00000030
       01   EPF34I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELCFAML      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MSELCFAML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSELCFAMF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSELCFAMI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCPTFL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNSOCPTFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCPTFF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCPTFI      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUPTFL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNLIEUPTFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNLIEUPTFF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNLIEUPTFI     PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBPTFL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLIBPTFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBPTFF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIBPTFI  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPACITEL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCAPACITEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCAPACITEF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCAPACITEI     PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELNCODICL    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MSELNCODICL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MSELNCODICF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSELNCODICI    PIC X(7).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCUMULSOL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCUMULSOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCUMULSOF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCUMULSOI      PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENCODICL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MSENCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSENCODICF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSENCODICI     PIC X(7).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCFAML   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSCFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSCFAMF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSCFAMI   PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNCODICL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MSNCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSNCODICF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSNCODICI      PIC X(7).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCMARQL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSCMARQL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCMARQF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSCMARQI  PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLREFFOL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MSLREFFOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSLREFFOF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSLREFFOI      PIC X(17).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOAL     COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSOAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSOAF     PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSOAI     PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCEXPOL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MSCEXPOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCEXPOF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MSCEXPOI  PIC X.                                          00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLSTATCOMPL   COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MSLSTATCOMPL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MSLSTATCOMPF   PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSLSTATCOMPI   PIC X(3).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSAL     COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MSSAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSSAF     PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSSAI     PIC X.                                          00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSV4SFILL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MSV4SFILL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSV4SFILF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSV4SFILI      PIC X(5).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSV4SPTFL      COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MSV4SPTFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSV4SPTFF      PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MSV4SPTFI      PIC X(4).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSTKRPL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSSTKRPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSSTKRPF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSSTKRPI  PIC X(3).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSTKDEPL      COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MSSTKDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSSTKDEPF      PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MSSTKDEPI      PIC X(5).                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSTKOBJL      COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MSSTKOBJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSSTKOBJF      PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSSTKOBJI      PIC X(3).                                  00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSACPRIL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MSACPRIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSACPRIF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MSACPRII  PIC X.                                          00001090
           02 M172I OCCURS   12 TIMES .                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MCFAMI  PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MNCODICI     PIC X(7).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MCMARQI      PIC X(5).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOURNL  COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MLREFFOURNL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLREFFOURNF  PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MLREFFOURNI  PIC X(17).                                 00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOAL    COMP PIC S9(4).                                 00001270
      *--                                                                       
             03 MOAL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MOAF    PIC X.                                          00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MOAI    PIC X.                                          00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXPOL      COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MCEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCEXPOF      PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MCEXPOI      PIC X.                                     00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSTATCOMPL  COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MLSTATCOMPL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLSTATCOMPF  PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MLSTATCOMPI  PIC X(3).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSAL    COMP PIC S9(4).                                 00001390
      *--                                                                       
             03 MSAL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MSAF    PIC X.                                          00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MSAI    PIC X.                                          00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MV4SFILL     COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MV4SFILL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MV4SFILF     PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MV4SFILI     PIC X(5).                                  00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MV4SPTFL     COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MV4SPTFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MV4SPTFF     PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MV4SPTFI     PIC X(4).                                  00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTKRPL      COMP PIC S9(4).                            00001510
      *--                                                                       
             03 MSTKRPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSTKRPF      PIC X.                                     00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MSTKRPI      PIC X(3).                                  00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTKDEPL     COMP PIC S9(4).                            00001550
      *--                                                                       
             03 MSTKDEPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTKDEPF     PIC X.                                     00001560
             03 FILLER  PIC X(4).                                       00001570
             03 MSTKDEPI     PIC X(5).                                  00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTKOBJL     COMP PIC S9(4).                            00001590
      *--                                                                       
             03 MSTKOBJL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTKOBJF     PIC X.                                     00001600
             03 FILLER  PIC X(4).                                       00001610
             03 MSTKOBJI     PIC X(3).                                  00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACPRIL      COMP PIC S9(4).                            00001630
      *--                                                                       
             03 MACPRIL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MACPRIF      PIC X.                                     00001640
             03 FILLER  PIC X(4).                                       00001650
             03 MACPRII      PIC X.                                     00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MLIBERRI  PIC X(80).                                      00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MCODTRAI  PIC X(4).                                       00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MCICSI    PIC X(5).                                       00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001800
           02 FILLER    PIC X(4).                                       00001810
           02 MNETNAMI  PIC X(8).                                       00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001840
           02 FILLER    PIC X(4).                                       00001850
           02 MSCREENI  PIC X(4).                                       00001860
      ***************************************************************** 00001870
      * Suivi codics pour 1 plateforme                                  00001880
      ***************************************************************** 00001890
       01   EPF34O REDEFINES EPF34I.                                    00001900
           02 FILLER    PIC X(12).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MDATJOUA  PIC X.                                          00001930
           02 MDATJOUC  PIC X.                                          00001940
           02 MDATJOUP  PIC X.                                          00001950
           02 MDATJOUH  PIC X.                                          00001960
           02 MDATJOUV  PIC X.                                          00001970
           02 MDATJOUO  PIC X(10).                                      00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MTIMJOUA  PIC X.                                          00002000
           02 MTIMJOUC  PIC X.                                          00002010
           02 MTIMJOUP  PIC X.                                          00002020
           02 MTIMJOUH  PIC X.                                          00002030
           02 MTIMJOUV  PIC X.                                          00002040
           02 MTIMJOUO  PIC X(5).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MPAGEA    PIC X.                                          00002070
           02 MPAGEC    PIC X.                                          00002080
           02 MPAGEP    PIC X.                                          00002090
           02 MPAGEH    PIC X.                                          00002100
           02 MPAGEV    PIC X.                                          00002110
           02 MPAGEO    PIC X(3).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MPAGEMAXA      PIC X.                                     00002140
           02 MPAGEMAXC PIC X.                                          00002150
           02 MPAGEMAXP PIC X.                                          00002160
           02 MPAGEMAXH PIC X.                                          00002170
           02 MPAGEMAXV PIC X.                                          00002180
           02 MPAGEMAXO      PIC X(3).                                  00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MSELCFAMA      PIC X.                                     00002210
           02 MSELCFAMC PIC X.                                          00002220
           02 MSELCFAMP PIC X.                                          00002230
           02 MSELCFAMH PIC X.                                          00002240
           02 MSELCFAMV PIC X.                                          00002250
           02 MSELCFAMO      PIC X(5).                                  00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MNSOCPTFA      PIC X.                                     00002280
           02 MNSOCPTFC PIC X.                                          00002290
           02 MNSOCPTFP PIC X.                                          00002300
           02 MNSOCPTFH PIC X.                                          00002310
           02 MNSOCPTFV PIC X.                                          00002320
           02 MNSOCPTFO      PIC X(3).                                  00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MNLIEUPTFA     PIC X.                                     00002350
           02 MNLIEUPTFC     PIC X.                                     00002360
           02 MNLIEUPTFP     PIC X.                                     00002370
           02 MNLIEUPTFH     PIC X.                                     00002380
           02 MNLIEUPTFV     PIC X.                                     00002390
           02 MNLIEUPTFO     PIC X(3).                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MLIBPTFA  PIC X.                                          00002420
           02 MLIBPTFC  PIC X.                                          00002430
           02 MLIBPTFP  PIC X.                                          00002440
           02 MLIBPTFH  PIC X.                                          00002450
           02 MLIBPTFV  PIC X.                                          00002460
           02 MLIBPTFO  PIC X(20).                                      00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MCAPACITEA     PIC X.                                     00002490
           02 MCAPACITEC     PIC X.                                     00002500
           02 MCAPACITEP     PIC X.                                     00002510
           02 MCAPACITEH     PIC X.                                     00002520
           02 MCAPACITEV     PIC X.                                     00002530
           02 MCAPACITEO     PIC X(3).                                  00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MSELNCODICA    PIC X.                                     00002560
           02 MSELNCODICC    PIC X.                                     00002570
           02 MSELNCODICP    PIC X.                                     00002580
           02 MSELNCODICH    PIC X.                                     00002590
           02 MSELNCODICV    PIC X.                                     00002600
           02 MSELNCODICO    PIC X(7).                                  00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MCUMULSOA      PIC X.                                     00002630
           02 MCUMULSOC PIC X.                                          00002640
           02 MCUMULSOP PIC X.                                          00002650
           02 MCUMULSOH PIC X.                                          00002660
           02 MCUMULSOV PIC X.                                          00002670
           02 MCUMULSOO      PIC X(3).                                  00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MSENCODICA     PIC X.                                     00002700
           02 MSENCODICC     PIC X.                                     00002710
           02 MSENCODICP     PIC X.                                     00002720
           02 MSENCODICH     PIC X.                                     00002730
           02 MSENCODICV     PIC X.                                     00002740
           02 MSENCODICO     PIC X(7).                                  00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MSCFAMA   PIC X.                                          00002770
           02 MSCFAMC   PIC X.                                          00002780
           02 MSCFAMP   PIC X.                                          00002790
           02 MSCFAMH   PIC X.                                          00002800
           02 MSCFAMV   PIC X.                                          00002810
           02 MSCFAMO   PIC X(5).                                       00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MSNCODICA      PIC X.                                     00002840
           02 MSNCODICC PIC X.                                          00002850
           02 MSNCODICP PIC X.                                          00002860
           02 MSNCODICH PIC X.                                          00002870
           02 MSNCODICV PIC X.                                          00002880
           02 MSNCODICO      PIC X(7).                                  00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MSCMARQA  PIC X.                                          00002910
           02 MSCMARQC  PIC X.                                          00002920
           02 MSCMARQP  PIC X.                                          00002930
           02 MSCMARQH  PIC X.                                          00002940
           02 MSCMARQV  PIC X.                                          00002950
           02 MSCMARQO  PIC X(5).                                       00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MSLREFFOA      PIC X.                                     00002980
           02 MSLREFFOC PIC X.                                          00002990
           02 MSLREFFOP PIC X.                                          00003000
           02 MSLREFFOH PIC X.                                          00003010
           02 MSLREFFOV PIC X.                                          00003020
           02 MSLREFFOO      PIC X(17).                                 00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MSOAA     PIC X.                                          00003050
           02 MSOAC     PIC X.                                          00003060
           02 MSOAP     PIC X.                                          00003070
           02 MSOAH     PIC X.                                          00003080
           02 MSOAV     PIC X.                                          00003090
           02 MSOAO     PIC X.                                          00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MSCEXPOA  PIC X.                                          00003120
           02 MSCEXPOC  PIC X.                                          00003130
           02 MSCEXPOP  PIC X.                                          00003140
           02 MSCEXPOH  PIC X.                                          00003150
           02 MSCEXPOV  PIC X.                                          00003160
           02 MSCEXPOO  PIC X.                                          00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MSLSTATCOMPA   PIC X.                                     00003190
           02 MSLSTATCOMPC   PIC X.                                     00003200
           02 MSLSTATCOMPP   PIC X.                                     00003210
           02 MSLSTATCOMPH   PIC X.                                     00003220
           02 MSLSTATCOMPV   PIC X.                                     00003230
           02 MSLSTATCOMPO   PIC X(3).                                  00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSSAA     PIC X.                                          00003260
           02 MSSAC     PIC X.                                          00003270
           02 MSSAP     PIC X.                                          00003280
           02 MSSAH     PIC X.                                          00003290
           02 MSSAV     PIC X.                                          00003300
           02 MSSAO     PIC X.                                          00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MSV4SFILA      PIC X.                                     00003330
           02 MSV4SFILC PIC X.                                          00003340
           02 MSV4SFILP PIC X.                                          00003350
           02 MSV4SFILH PIC X.                                          00003360
           02 MSV4SFILV PIC X.                                          00003370
           02 MSV4SFILO      PIC X(5).                                  00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MSV4SPTFA      PIC X.                                     00003400
           02 MSV4SPTFC PIC X.                                          00003410
           02 MSV4SPTFP PIC X.                                          00003420
           02 MSV4SPTFH PIC X.                                          00003430
           02 MSV4SPTFV PIC X.                                          00003440
           02 MSV4SPTFO      PIC X(4).                                  00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MSSTKRPA  PIC X.                                          00003470
           02 MSSTKRPC  PIC X.                                          00003480
           02 MSSTKRPP  PIC X.                                          00003490
           02 MSSTKRPH  PIC X.                                          00003500
           02 MSSTKRPV  PIC X.                                          00003510
           02 MSSTKRPO  PIC X(3).                                       00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MSSTKDEPA      PIC X.                                     00003540
           02 MSSTKDEPC PIC X.                                          00003550
           02 MSSTKDEPP PIC X.                                          00003560
           02 MSSTKDEPH PIC X.                                          00003570
           02 MSSTKDEPV PIC X.                                          00003580
           02 MSSTKDEPO      PIC X(5).                                  00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MSSTKOBJA      PIC X.                                     00003610
           02 MSSTKOBJC PIC X.                                          00003620
           02 MSSTKOBJP PIC X.                                          00003630
           02 MSSTKOBJH PIC X.                                          00003640
           02 MSSTKOBJV PIC X.                                          00003650
           02 MSSTKOBJO      PIC X(3).                                  00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MSACPRIA  PIC X.                                          00003680
           02 MSACPRIC  PIC X.                                          00003690
           02 MSACPRIP  PIC X.                                          00003700
           02 MSACPRIH  PIC X.                                          00003710
           02 MSACPRIV  PIC X.                                          00003720
           02 MSACPRIO  PIC X.                                          00003730
           02 M172O OCCURS   12 TIMES .                                 00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MCFAMA  PIC X.                                          00003760
             03 MCFAMC  PIC X.                                          00003770
             03 MCFAMP  PIC X.                                          00003780
             03 MCFAMH  PIC X.                                          00003790
             03 MCFAMV  PIC X.                                          00003800
             03 MCFAMO  PIC X(5).                                       00003810
             03 FILLER       PIC X(2).                                  00003820
             03 MNCODICA     PIC X.                                     00003830
             03 MNCODICC     PIC X.                                     00003840
             03 MNCODICP     PIC X.                                     00003850
             03 MNCODICH     PIC X.                                     00003860
             03 MNCODICV     PIC X.                                     00003870
             03 MNCODICO     PIC X(7).                                  00003880
             03 FILLER       PIC X(2).                                  00003890
             03 MCMARQA      PIC X.                                     00003900
             03 MCMARQC PIC X.                                          00003910
             03 MCMARQP PIC X.                                          00003920
             03 MCMARQH PIC X.                                          00003930
             03 MCMARQV PIC X.                                          00003940
             03 MCMARQO      PIC X(5).                                  00003950
             03 FILLER       PIC X(2).                                  00003960
             03 MLREFFOURNA  PIC X.                                     00003970
             03 MLREFFOURNC  PIC X.                                     00003980
             03 MLREFFOURNP  PIC X.                                     00003990
             03 MLREFFOURNH  PIC X.                                     00004000
             03 MLREFFOURNV  PIC X.                                     00004010
             03 MLREFFOURNO  PIC X(17).                                 00004020
             03 FILLER       PIC X(2).                                  00004030
             03 MOAA    PIC X.                                          00004040
             03 MOAC    PIC X.                                          00004050
             03 MOAP    PIC X.                                          00004060
             03 MOAH    PIC X.                                          00004070
             03 MOAV    PIC X.                                          00004080
             03 MOAO    PIC X.                                          00004090
             03 FILLER       PIC X(2).                                  00004100
             03 MCEXPOA      PIC X.                                     00004110
             03 MCEXPOC PIC X.                                          00004120
             03 MCEXPOP PIC X.                                          00004130
             03 MCEXPOH PIC X.                                          00004140
             03 MCEXPOV PIC X.                                          00004150
             03 MCEXPOO      PIC X.                                     00004160
             03 FILLER       PIC X(2).                                  00004170
             03 MLSTATCOMPA  PIC X.                                     00004180
             03 MLSTATCOMPC  PIC X.                                     00004190
             03 MLSTATCOMPP  PIC X.                                     00004200
             03 MLSTATCOMPH  PIC X.                                     00004210
             03 MLSTATCOMPV  PIC X.                                     00004220
             03 MLSTATCOMPO  PIC X(3).                                  00004230
             03 FILLER       PIC X(2).                                  00004240
             03 MSAA    PIC X.                                          00004250
             03 MSAC    PIC X.                                          00004260
             03 MSAP    PIC X.                                          00004270
             03 MSAH    PIC X.                                          00004280
             03 MSAV    PIC X.                                          00004290
             03 MSAO    PIC X.                                          00004300
             03 FILLER       PIC X(2).                                  00004310
             03 MV4SFILA     PIC X.                                     00004320
             03 MV4SFILC     PIC X.                                     00004330
             03 MV4SFILP     PIC X.                                     00004340
             03 MV4SFILH     PIC X.                                     00004350
             03 MV4SFILV     PIC X.                                     00004360
             03 MV4SFILO     PIC X(5).                                  00004370
             03 FILLER       PIC X(2).                                  00004380
             03 MV4SPTFA     PIC X.                                     00004390
             03 MV4SPTFC     PIC X.                                     00004400
             03 MV4SPTFP     PIC X.                                     00004410
             03 MV4SPTFH     PIC X.                                     00004420
             03 MV4SPTFV     PIC X.                                     00004430
             03 MV4SPTFO     PIC X(4).                                  00004440
             03 FILLER       PIC X(2).                                  00004450
             03 MSTKRPA      PIC X.                                     00004460
             03 MSTKRPC PIC X.                                          00004470
             03 MSTKRPP PIC X.                                          00004480
             03 MSTKRPH PIC X.                                          00004490
             03 MSTKRPV PIC X.                                          00004500
             03 MSTKRPO      PIC X(3).                                  00004510
             03 FILLER       PIC X(2).                                  00004520
             03 MSTKDEPA     PIC X.                                     00004530
             03 MSTKDEPC     PIC X.                                     00004540
             03 MSTKDEPP     PIC X.                                     00004550
             03 MSTKDEPH     PIC X.                                     00004560
             03 MSTKDEPV     PIC X.                                     00004570
             03 MSTKDEPO     PIC X(5).                                  00004580
             03 FILLER       PIC X(2).                                  00004590
             03 MSTKOBJA     PIC X.                                     00004600
             03 MSTKOBJC     PIC X.                                     00004610
             03 MSTKOBJP     PIC X.                                     00004620
             03 MSTKOBJH     PIC X.                                     00004630
             03 MSTKOBJV     PIC X.                                     00004640
             03 MSTKOBJO     PIC X(3).                                  00004650
             03 FILLER       PIC X(2).                                  00004660
             03 MACPRIA      PIC X.                                     00004670
             03 MACPRIC PIC X.                                          00004680
             03 MACPRIP PIC X.                                          00004690
             03 MACPRIH PIC X.                                          00004700
             03 MACPRIV PIC X.                                          00004710
             03 MACPRIO      PIC X.                                     00004720
           02 FILLER    PIC X(2).                                       00004730
           02 MLIBERRA  PIC X.                                          00004740
           02 MLIBERRC  PIC X.                                          00004750
           02 MLIBERRP  PIC X.                                          00004760
           02 MLIBERRH  PIC X.                                          00004770
           02 MLIBERRV  PIC X.                                          00004780
           02 MLIBERRO  PIC X(80).                                      00004790
           02 FILLER    PIC X(2).                                       00004800
           02 MCODTRAA  PIC X.                                          00004810
           02 MCODTRAC  PIC X.                                          00004820
           02 MCODTRAP  PIC X.                                          00004830
           02 MCODTRAH  PIC X.                                          00004840
           02 MCODTRAV  PIC X.                                          00004850
           02 MCODTRAO  PIC X(4).                                       00004860
           02 FILLER    PIC X(2).                                       00004870
           02 MCICSA    PIC X.                                          00004880
           02 MCICSC    PIC X.                                          00004890
           02 MCICSP    PIC X.                                          00004900
           02 MCICSH    PIC X.                                          00004910
           02 MCICSV    PIC X.                                          00004920
           02 MCICSO    PIC X(5).                                       00004930
           02 FILLER    PIC X(2).                                       00004940
           02 MNETNAMA  PIC X.                                          00004950
           02 MNETNAMC  PIC X.                                          00004960
           02 MNETNAMP  PIC X.                                          00004970
           02 MNETNAMH  PIC X.                                          00004980
           02 MNETNAMV  PIC X.                                          00004990
           02 MNETNAMO  PIC X(8).                                       00005000
           02 FILLER    PIC X(2).                                       00005010
           02 MSCREENA  PIC X.                                          00005020
           02 MSCREENC  PIC X.                                          00005030
           02 MSCREENP  PIC X.                                          00005040
           02 MSCREENH  PIC X.                                          00005050
           02 MSCREENV  PIC X.                                          00005060
           02 MSCREENO  PIC X(4).                                       00005070
                                                                                
