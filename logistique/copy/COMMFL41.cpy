      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TFL41 (TFL30 -> MENU)    TR: FL30  *    00002201
      *          GESTION DES PROFILS D'AFFILIATION                 *    00002301
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *  TRANSACTION FL41 : EXCEPTION DES CODIC                    *    00411001
      *                                                                 00412000
          02 COMM-FL41-APPLI REDEFINES COMM-FL30-APPLI.                 00420001
      *------------------------------ ZONE DONNEES TFL41                00510001
             03 COMM-FL41-DONNEES-1-TFL41.                              00520001
      *------------------------------ CODE FONCTION                     00550001
                04 COMM-FL41-FONCT             PIC X(3).                00560001
      *------------------------------ CODE PROFIL                       00561001
                04 COMM-FL41-CPROAFF           PIC X(5).                00562001
      *------------------------------ LIBELLE PROFIL                    00570001
                04 COMM-FL41-LPROAFF           PIC X(20).               00580001
      *------------------------------ DERNIER IND-TS                    00742401
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL41-PAGSUI            PIC S9(4) COMP.          00742501
      *--                                                                       
                04 COMM-FL41-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN IND-TS                     00742601
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL41-PAGENC            PIC S9(4) COMP.          00742701
      *--                                                                       
                04 COMM-FL41-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00742801
                04 COMM-FL41-NUMPAG            PIC 9(3).                00742901
      *------------------------------ NOMBRE DE PAGE                    00743001
                04 COMM-FL41-NBRPAG            PIC 9(3).                00743101
      *------------------------------ INDICATEUR FIN DE TABLE           00743201
                04 COMM-FL41-INDPAG            PIC 9.                   00743301
      *------------------------------ ZONE MODIFICATION TS              00743401
                04 COMM-FL41-MODIF             PIC 9(1).                00743501
      *------------------------------ ZONE CONFIRMATION PF4             00743601
                04 COMM-FL41-CONF-PF4          PIC 9(1).                00743701
      *------------------------------ OK MAJ SUR RTFL06 => VALID = '1'  00743801
                04 COMM-FL41-VALID-RTFL06   PIC  9.                     00743901
      *------------------------------ TABLE N� TS PR PAGINATION         00744201
                04 COMM-FL41-TABTS          OCCURS 100.                 00744301
      *------------------------------ 1ERS IND TS DES PAGES <=> FAMILLE 00744401
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               06 COMM-FL41-NUMTS          PIC S9(4) COMP.       00744501
      *--                                                                       
                      06 COMM-FL41-NUMTS          PIC S9(4) COMP-5.             
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS               00744601
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL41-IND-RECH-TS    PIC S9(4) COMP.             00745001
      *--                                                                       
                04 COMM-FL41-IND-RECH-TS    PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND TS MAX                        00746001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL41-IND-TS-MAX     PIC S9(4) COMP.             00747001
      *--                                                                       
                04 COMM-FL41-IND-TS-MAX     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ ZONE DONNEES 2 TFL41              00760001
             03 COMM-FL41-DONNEES-2-TFL41.                              00761001
      *------------------------------ CODES D'AFFILIATION PAR DEFAUT    00762001
                04 COMM-FL41-ENTAFF-DF      OCCURS 4.                   00763001
      *------------------------------ CODE SOCIETE D'AFFILIATION DEFAUT 00764001
                   05 COMM-FL41-CSOCAFF-DF  PIC X(03).                  00765001
      *------------------------------ CODE ENTREPOT D'AFFILIATION DEFAUT00766001
                   05 COMM-FL41-CDEPAFF-DF  PIC X(03).                  00767001
      *------------------------------ ZONE FAMILLES PAGE                00770001
                04 COMM-FL41-TABFAM.                                    00780001
      *------------------------------ ZONE LIGNES FAMILLES              00790001
                   05 COMM-FL41-LIGFAM  OCCURS 15.                      00800001
      *------------------------------ CODIC                             00801001
                      06 COMM-FL41-CDAC           PIC X(1).             00802001
      *------------------------------ CODIC                             00810001
                      06 COMM-FL41-CODIC          PIC X(7).             00820001
      *------------------------------ LIBELLE CODIC                     00830001
                      06 COMM-FL41-LCODIC         PIC X(20).            00840001
      *------------------------------ CODE MARQUE CODIC                 00841001
                      06 COMM-FL41-CDMARQ         PIC X(5).             00842001
      *------------------------------ CODES D'AFFILIATION               00850001
                      06 COMM-FL41-ENTAFF         OCCURS 4.             00860001
      *------------------------------ CODE SOCIETE D'AFFILIATION        00870001
                         07 COMM-FL41-CSOCAFF     PIC X(03).            00880001
      *------------------------------ CODE ENTREPOT D'AFFILIATION       00890001
                         07 COMM-FL41-CDEPAFF     PIC X(03).            00900001
      *------------------------------ ZONE DONNEES 3 TFL41              01650001
             03 COMM-FL41-DONNEES-3-TFL41.                              01651001
      *------------------------------ ZONES SWAP                        01670001
                   05 COMM-FL41-ATTR           PIC X OCCURS 500.        01680001
      *------------------------------ ZONE LIBRE                        01710001
             03 COMM-FL41-LIBRE          PIC X(2097).                   01720001
      ***************************************************************** 02170001
                                                                                
