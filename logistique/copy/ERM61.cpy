      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM01   ERM01                                              00000020
      ***************************************************************** 00000030
       01   ERM61I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CRMGROUPL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 CRMGROUPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 CRMGROUPF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 CRMGROUPI      PIC X(5).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LRMGROUPL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 LRMGROUPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 LRMGROUPF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 LRMGROUPI      PIC X(20).                                 00000210
           02 MCGROUPD OCCURS   10 TIMES .                              00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGROUPL     COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MCGROUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCGROUPF     PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MCGROUPI     PIC X(5).                                  00000260
           02 MLGROUPD OCCURS   10 TIMES .                              00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLGROUPL     COMP PIC S9(4).                            00000280
      *--                                                                       
             03 MLGROUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLGROUPF     PIC X.                                     00000290
             03 FILLER  PIC X(4).                                       00000300
             03 MLGROUPI     PIC X(20).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLIBERRI  PIC X(78).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCODTRAI  PIC X(4).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCICSI    PIC X(5).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNETNAMI  PIC X(8).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MSCREENI  PIC X(4).                                       00000510
      ***************************************************************** 00000520
      * SDF: ERM01   ERM01                                              00000530
      ***************************************************************** 00000540
       01   ERM61O REDEFINES ERM61I.                                    00000550
           02 FILLER    PIC X(12).                                      00000560
           02 FILLER    PIC X(2).                                       00000570
           02 MDATJOUA  PIC X.                                          00000580
           02 MDATJOUC  PIC X.                                          00000590
           02 MDATJOUP  PIC X.                                          00000600
           02 MDATJOUH  PIC X.                                          00000610
           02 MDATJOUV  PIC X.                                          00000620
           02 MDATJOUO  PIC X(10).                                      00000630
           02 FILLER    PIC X(2).                                       00000640
           02 MTIMJOUA  PIC X.                                          00000650
           02 MTIMJOUC  PIC X.                                          00000660
           02 MTIMJOUP  PIC X.                                          00000670
           02 MTIMJOUH  PIC X.                                          00000680
           02 MTIMJOUV  PIC X.                                          00000690
           02 MTIMJOUO  PIC X(5).                                       00000700
           02 FILLER    PIC X(2).                                       00000710
           02 CRMGROUPA      PIC X.                                     00000720
           02 CRMGROUPC PIC X.                                          00000730
           02 CRMGROUPP PIC X.                                          00000740
           02 CRMGROUPH PIC X.                                          00000750
           02 CRMGROUPV PIC X.                                          00000760
           02 CRMGROUPO      PIC X(5).                                  00000770
           02 FILLER    PIC X(2).                                       00000780
           02 LRMGROUPA      PIC X.                                     00000790
           02 LRMGROUPC PIC X.                                          00000800
           02 LRMGROUPP PIC X.                                          00000810
           02 LRMGROUPH PIC X.                                          00000820
           02 LRMGROUPV PIC X.                                          00000830
           02 LRMGROUPO      PIC X(20).                                 00000840
           02 DFHMS1 OCCURS   10 TIMES .                                00000850
             03 FILLER       PIC X(2).                                  00000860
             03 MCGROUPA     PIC X.                                     00000870
             03 MCGROUPC     PIC X.                                     00000880
             03 MCGROUPP     PIC X.                                     00000890
             03 MCGROUPH     PIC X.                                     00000900
             03 MCGROUPV     PIC X.                                     00000910
             03 MCGROUPO     PIC X(5).                                  00000920
           02 DFHMS2 OCCURS   10 TIMES .                                00000930
             03 FILLER       PIC X(2).                                  00000940
             03 MLGROUPA     PIC X.                                     00000950
             03 MLGROUPC     PIC X.                                     00000960
             03 MLGROUPP     PIC X.                                     00000970
             03 MLGROUPH     PIC X.                                     00000980
             03 MLGROUPV     PIC X.                                     00000990
             03 MLGROUPO     PIC X(20).                                 00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MLIBERRA  PIC X.                                          00001020
           02 MLIBERRC  PIC X.                                          00001030
           02 MLIBERRP  PIC X.                                          00001040
           02 MLIBERRH  PIC X.                                          00001050
           02 MLIBERRV  PIC X.                                          00001060
           02 MLIBERRO  PIC X(78).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MCODTRAA  PIC X.                                          00001090
           02 MCODTRAC  PIC X.                                          00001100
           02 MCODTRAP  PIC X.                                          00001110
           02 MCODTRAH  PIC X.                                          00001120
           02 MCODTRAV  PIC X.                                          00001130
           02 MCODTRAO  PIC X(4).                                       00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MCICSA    PIC X.                                          00001160
           02 MCICSC    PIC X.                                          00001170
           02 MCICSP    PIC X.                                          00001180
           02 MCICSH    PIC X.                                          00001190
           02 MCICSV    PIC X.                                          00001200
           02 MCICSO    PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MNETNAMA  PIC X.                                          00001230
           02 MNETNAMC  PIC X.                                          00001240
           02 MNETNAMP  PIC X.                                          00001250
           02 MNETNAMH  PIC X.                                          00001260
           02 MNETNAMV  PIC X.                                          00001270
           02 MNETNAMO  PIC X(8).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MSCREENA  PIC X.                                          00001300
           02 MSCREENC  PIC X.                                          00001310
           02 MSCREENP  PIC X.                                          00001320
           02 MSCREENH  PIC X.                                          00001330
           02 MSCREENV  PIC X.                                          00001340
           02 MSCREENO  PIC X(4).                                       00001350
                                                                                
