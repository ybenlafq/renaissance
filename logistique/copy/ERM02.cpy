      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM02   ERM02                                              00000020
      ***************************************************************** 00000030
       01   ERM02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCGROUPI  PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQSEUILSL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MQSEUILSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQSEUILSF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MQSEUILSI      PIC X(2).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCSTLISSL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MQCSTLISSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQCSTLISSF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MQCSTLISSI     PIC X(4).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQFAIBLEL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MQFAIBLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQFAIBLEF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MQFAIBLEI      PIC X(2).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQFORTESL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MQFORTESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQFORTESF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MQFORTESI      PIC X(2).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQSEUILEDL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MQSEUILEDL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQSEUILEDF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MQSEUILEDI     PIC X(6).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETDDL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MDEFFETDDL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDEFFETDDF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDEFFETDDI     PIC X(10).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINEFFDL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDFINEFFDL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDFINEFFDF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDFINEFFDI     PIC X(10).                                 00000490
           02 LIGNEI OCCURS   13 TIMES .                                00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCFAMI  PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLFAMI  PIC X(20).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MW20L   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MW20L COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MW20F   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MW20I   PIC X.                                          00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MW80L   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MW80L COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MW80F   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MW80I   PIC X.                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTXEMPL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MWTXEMPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWTXEMPF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MWTXEMPI     PIC X.                                     00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSEUILEXL   COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MQSEUILEXL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQSEUILEXF   PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQSEUILEXI   PIC X(6).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDEFFETI     PIC X(10).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINEFFL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MDFINEFFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDFINEFFF    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MDFINEFFI    PIC X(10).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MZONCMDI  PIC X(12).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(61).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: ERM02   ERM02                                              00001080
      ***************************************************************** 00001090
       01   ERM02O REDEFINES ERM02I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MCGROUPA  PIC X.                                          00001270
           02 MCGROUPC  PIC X.                                          00001280
           02 MCGROUPP  PIC X.                                          00001290
           02 MCGROUPH  PIC X.                                          00001300
           02 MCGROUPV  PIC X.                                          00001310
           02 MCGROUPO  PIC X(5).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MNPAGEA   PIC X.                                          00001340
           02 MNPAGEC   PIC X.                                          00001350
           02 MNPAGEP   PIC X.                                          00001360
           02 MNPAGEH   PIC X.                                          00001370
           02 MNPAGEV   PIC X.                                          00001380
           02 MNPAGEO   PIC X(3).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MQSEUILSA      PIC X.                                     00001410
           02 MQSEUILSC PIC X.                                          00001420
           02 MQSEUILSP PIC X.                                          00001430
           02 MQSEUILSH PIC X.                                          00001440
           02 MQSEUILSV PIC X.                                          00001450
           02 MQSEUILSO      PIC X(2).                                  00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MQCSTLISSA     PIC X.                                     00001480
           02 MQCSTLISSC     PIC X.                                     00001490
           02 MQCSTLISSP     PIC X.                                     00001500
           02 MQCSTLISSH     PIC X.                                     00001510
           02 MQCSTLISSV     PIC X.                                     00001520
           02 MQCSTLISSO     PIC X(4).                                  00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MQFAIBLEA      PIC X.                                     00001550
           02 MQFAIBLEC PIC X.                                          00001560
           02 MQFAIBLEP PIC X.                                          00001570
           02 MQFAIBLEH PIC X.                                          00001580
           02 MQFAIBLEV PIC X.                                          00001590
           02 MQFAIBLEO      PIC X(2).                                  00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MQFORTESA      PIC X.                                     00001620
           02 MQFORTESC PIC X.                                          00001630
           02 MQFORTESP PIC X.                                          00001640
           02 MQFORTESH PIC X.                                          00001650
           02 MQFORTESV PIC X.                                          00001660
           02 MQFORTESO      PIC X(2).                                  00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MQSEUILEDA     PIC X.                                     00001690
           02 MQSEUILEDC     PIC X.                                     00001700
           02 MQSEUILEDP     PIC X.                                     00001710
           02 MQSEUILEDH     PIC X.                                     00001720
           02 MQSEUILEDV     PIC X.                                     00001730
           02 MQSEUILEDO     PIC X(6).                                  00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MDEFFETDDA     PIC X.                                     00001760
           02 MDEFFETDDC     PIC X.                                     00001770
           02 MDEFFETDDP     PIC X.                                     00001780
           02 MDEFFETDDH     PIC X.                                     00001790
           02 MDEFFETDDV     PIC X.                                     00001800
           02 MDEFFETDDO     PIC X(10).                                 00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MDFINEFFDA     PIC X.                                     00001830
           02 MDFINEFFDC     PIC X.                                     00001840
           02 MDFINEFFDP     PIC X.                                     00001850
           02 MDFINEFFDH     PIC X.                                     00001860
           02 MDFINEFFDV     PIC X.                                     00001870
           02 MDFINEFFDO     PIC X(10).                                 00001880
           02 LIGNEO OCCURS   13 TIMES .                                00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MCFAMA  PIC X.                                          00001910
             03 MCFAMC  PIC X.                                          00001920
             03 MCFAMP  PIC X.                                          00001930
             03 MCFAMH  PIC X.                                          00001940
             03 MCFAMV  PIC X.                                          00001950
             03 MCFAMO  PIC X(5).                                       00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MLFAMA  PIC X.                                          00001980
             03 MLFAMC  PIC X.                                          00001990
             03 MLFAMP  PIC X.                                          00002000
             03 MLFAMH  PIC X.                                          00002010
             03 MLFAMV  PIC X.                                          00002020
             03 MLFAMO  PIC X(20).                                      00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MW20A   PIC X.                                          00002050
             03 MW20C   PIC X.                                          00002060
             03 MW20P   PIC X.                                          00002070
             03 MW20H   PIC X.                                          00002080
             03 MW20V   PIC X.                                          00002090
             03 MW20O   PIC X.                                          00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MW80A   PIC X.                                          00002120
             03 MW80C   PIC X.                                          00002130
             03 MW80P   PIC X.                                          00002140
             03 MW80H   PIC X.                                          00002150
             03 MW80V   PIC X.                                          00002160
             03 MW80O   PIC X.                                          00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MWTXEMPA     PIC X.                                     00002190
             03 MWTXEMPC     PIC X.                                     00002200
             03 MWTXEMPP     PIC X.                                     00002210
             03 MWTXEMPH     PIC X.                                     00002220
             03 MWTXEMPV     PIC X.                                     00002230
             03 MWTXEMPO     PIC X.                                     00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MQSEUILEXA   PIC X.                                     00002260
             03 MQSEUILEXC   PIC X.                                     00002270
             03 MQSEUILEXP   PIC X.                                     00002280
             03 MQSEUILEXH   PIC X.                                     00002290
             03 MQSEUILEXV   PIC X.                                     00002300
             03 MQSEUILEXO   PIC X(6).                                  00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MDEFFETA     PIC X.                                     00002330
             03 MDEFFETC     PIC X.                                     00002340
             03 MDEFFETP     PIC X.                                     00002350
             03 MDEFFETH     PIC X.                                     00002360
             03 MDEFFETV     PIC X.                                     00002370
             03 MDEFFETO     PIC X(10).                                 00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MDFINEFFA    PIC X.                                     00002400
             03 MDFINEFFC    PIC X.                                     00002410
             03 MDFINEFFP    PIC X.                                     00002420
             03 MDFINEFFH    PIC X.                                     00002430
             03 MDFINEFFV    PIC X.                                     00002440
             03 MDFINEFFO    PIC X(10).                                 00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MZONCMDA  PIC X.                                          00002470
           02 MZONCMDC  PIC X.                                          00002480
           02 MZONCMDP  PIC X.                                          00002490
           02 MZONCMDH  PIC X.                                          00002500
           02 MZONCMDV  PIC X.                                          00002510
           02 MZONCMDO  PIC X(12).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(61).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
