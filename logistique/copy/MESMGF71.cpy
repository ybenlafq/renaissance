      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * - MESMGF71 COPY MESSAGE POUR L ENVOI ET LA RECEPTION LONGUEUR  *      13
      * 105 DEFINITION ENTETE                                          *        
      * 245 DEFINITION DATA                                            *      13
      * COPY POUR LES PROGRAMMES MGF71                                 *      13
      ******************************************************************        
              05 MESMGF71-ENTETE.                                               
                 10 MESMGF71-TYPE        PIC X(03).                             
                 10 MESMGF71-NSOCMSG     PIC X(03).                             
                 10 MESMGF71-NLIEUMSG    PIC X(03).                             
                 10 MESMGF71-NSOCDST     PIC X(03).                             
                 10 MESMGF71-NLIEUDST    PIC X(03).                             
                 10 MESMGF71-NORD        PIC 9(08).                             
                 10 MESMGF71-LPROG       PIC X(10).                             
                 10 MESMGF71-DJOUR       PIC X(08).                             
                 10 MESMGF71-WSID        PIC X(10).                             
                 10 MESMGF71-USER        PIC X(10).                             
                 10 MESMGF71-CHRONO      PIC 9(07).                             
                 10 MESMGF71-NBRMSG      PIC 9(07).                             
                 10 MESMGF71-NBRENR      PIC 9(5).                              
                 10 MESMGF71-TAILLE      PIC 9(5).                              
                 10 MESMGF71-VERSION     PIC X(2).                              
                 10 MESMGF71-FILLER      PIC X(18).                             
              05 MESMGF71-DATA.                                                 
      *                                                                         
                 10 MESMGF71-NCDE          PIC X(7).                            
                 10 MESMGF71-CSTATUT       PIC X(3).                            
      *                                                                         
                 10 MESMGF71-FILLER-LIBRE  PIC X(200).                          
                 10 MESMGF71-RETOUR.                                            
                    15 MESMGF71-CODE-RETOUR.                                    
                       20  MESMGF71-CODRET  PIC X(1).                           
                            88  MESMGF71-CODRET-OK        VALUE ' '.            
                            88  MESMGF71-CODRET-ERREUR    VALUE '1'.            
                       20  MESMGF71-LIBERR  PIC X(34).                          
                                                                                
