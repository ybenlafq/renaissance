      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EEF15   EEF15                                              00000020
      ***************************************************************** 00000030
       01   EEF15I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGMAXL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MPAGMAXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAGMAXF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MPAGMAXI  PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCTRAITI  PIC X(5).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MLTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRAITF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLTRAITI  PIC X(30).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTIERL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MLIBTIERL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBTIERF      PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLIBTIERI      PIC X(15).                                 00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCTIERSI  PIC X(5).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLTIERSI  PIC X(15).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNENVOII  PIC X(7).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MDENVOII  PIC X(10).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCORDL      COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MNACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNACCORDF      PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNACCORDI      PIC X(12).                                 00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACCORDL      COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MDACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDACCORDF      PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MDACCORDI      PIC X(10).                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHSL      COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MNLIEUHSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHSF      PIC X.                                     00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MNLIEUHSI      PIC X(3).                                  00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNHSL     COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MNHSL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNHSF     PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MNHSI     PIC X(7).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEL1L  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MLIBEL1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBEL1F  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MLIBEL1I  PIC X(9).                                       00000710
           02 MLIGNETI OCCURS   11 TIMES .                              00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNEL      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MLIGNEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIGNEF      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MLIGNEI      PIC X(79).                                 00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEL2L  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MLIBEL2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBEL2F  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MLIBEL2I  PIC X(15).                                      00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTALL   COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MTOTALL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTOTALF   PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MTOTALI   PIC X(13).                                      00000840
      * MESSAGE ERREUR                                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLIBERRI  PIC X(78).                                      00000890
      * CODE TRANSACTION                                                00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      * CICS DE TRAVAIL                                                 00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MCICSI    PIC X(5).                                       00000990
      * NETNAME                                                         00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MNETNAMI  PIC X(8).                                       00001040
      * CODE TERMINAL                                                   00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MSCREENI  PIC X(5).                                       00001090
      ***************************************************************** 00001100
      * SDF: EEF15   EEF15                                              00001110
      ***************************************************************** 00001120
       01   EEF15O REDEFINES EEF15I.                                    00001130
           02 FILLER    PIC X(12).                                      00001140
      * DATE DU JOUR(JJ/MM/AAAA)                                        00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDATJOUA  PIC X.                                          00001170
           02 MDATJOUC  PIC X.                                          00001180
           02 MDATJOUP  PIC X.                                          00001190
           02 MDATJOUH  PIC X.                                          00001200
           02 MDATJOUV  PIC X.                                          00001210
           02 MDATJOUO  PIC X(10).                                      00001220
      * HEURE                                                           00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MTIMJOUA  PIC X.                                          00001250
           02 MTIMJOUC  PIC X.                                          00001260
           02 MTIMJOUP  PIC X.                                          00001270
           02 MTIMJOUH  PIC X.                                          00001280
           02 MTIMJOUV  PIC X.                                          00001290
           02 MTIMJOUO  PIC X(5).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MPAGEA    PIC X.                                          00001320
           02 MPAGEC    PIC X.                                          00001330
           02 MPAGEP    PIC X.                                          00001340
           02 MPAGEH    PIC X.                                          00001350
           02 MPAGEV    PIC X.                                          00001360
           02 MPAGEO    PIC 999.                                        00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MPAGMAXA  PIC X.                                          00001390
           02 MPAGMAXC  PIC X.                                          00001400
           02 MPAGMAXP  PIC X.                                          00001410
           02 MPAGMAXH  PIC X.                                          00001420
           02 MPAGMAXV  PIC X.                                          00001430
           02 MPAGMAXO  PIC 999.                                        00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCTRAITA  PIC X.                                          00001460
           02 MCTRAITC  PIC X.                                          00001470
           02 MCTRAITP  PIC X.                                          00001480
           02 MCTRAITH  PIC X.                                          00001490
           02 MCTRAITV  PIC X.                                          00001500
           02 MCTRAITO  PIC X(5).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MLTRAITA  PIC X.                                          00001530
           02 MLTRAITC  PIC X.                                          00001540
           02 MLTRAITP  PIC X.                                          00001550
           02 MLTRAITH  PIC X.                                          00001560
           02 MLTRAITV  PIC X.                                          00001570
           02 MLTRAITO  PIC X(30).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MLIBTIERA      PIC X.                                     00001600
           02 MLIBTIERC PIC X.                                          00001610
           02 MLIBTIERP PIC X.                                          00001620
           02 MLIBTIERH PIC X.                                          00001630
           02 MLIBTIERV PIC X.                                          00001640
           02 MLIBTIERO      PIC X(15).                                 00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCTIERSA  PIC X.                                          00001670
           02 MCTIERSC  PIC X.                                          00001680
           02 MCTIERSP  PIC X.                                          00001690
           02 MCTIERSH  PIC X.                                          00001700
           02 MCTIERSV  PIC X.                                          00001710
           02 MCTIERSO  PIC 99999.                                      00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLTIERSA  PIC X.                                          00001740
           02 MLTIERSC  PIC X.                                          00001750
           02 MLTIERSP  PIC X.                                          00001760
           02 MLTIERSH  PIC X.                                          00001770
           02 MLTIERSV  PIC X.                                          00001780
           02 MLTIERSO  PIC X(15).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MNENVOIA  PIC X.                                          00001810
           02 MNENVOIC  PIC X.                                          00001820
           02 MNENVOIP  PIC X.                                          00001830
           02 MNENVOIH  PIC X.                                          00001840
           02 MNENVOIV  PIC X.                                          00001850
           02 MNENVOIO  PIC 9(7).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MDENVOIA  PIC X.                                          00001880
           02 MDENVOIC  PIC X.                                          00001890
           02 MDENVOIP  PIC X.                                          00001900
           02 MDENVOIH  PIC X.                                          00001910
           02 MDENVOIV  PIC X.                                          00001920
           02 MDENVOIO  PIC X(10).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MNACCORDA      PIC X.                                     00001950
           02 MNACCORDC PIC X.                                          00001960
           02 MNACCORDP PIC X.                                          00001970
           02 MNACCORDH PIC X.                                          00001980
           02 MNACCORDV PIC X.                                          00001990
           02 MNACCORDO      PIC 9(12).                                 00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MDACCORDA      PIC X.                                     00002020
           02 MDACCORDC PIC X.                                          00002030
           02 MDACCORDP PIC X.                                          00002040
           02 MDACCORDH PIC X.                                          00002050
           02 MDACCORDV PIC X.                                          00002060
           02 MDACCORDO      PIC X(10).                                 00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNLIEUHSA      PIC X.                                     00002090
           02 MNLIEUHSC PIC X.                                          00002100
           02 MNLIEUHSP PIC X.                                          00002110
           02 MNLIEUHSH PIC X.                                          00002120
           02 MNLIEUHSV PIC X.                                          00002130
           02 MNLIEUHSO      PIC X(3).                                  00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MNHSA     PIC X.                                          00002160
           02 MNHSC     PIC X.                                          00002170
           02 MNHSP     PIC X.                                          00002180
           02 MNHSH     PIC X.                                          00002190
           02 MNHSV     PIC X.                                          00002200
           02 MNHSO     PIC 9(7).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MLIBEL1A  PIC X.                                          00002230
           02 MLIBEL1C  PIC X.                                          00002240
           02 MLIBEL1P  PIC X.                                          00002250
           02 MLIBEL1H  PIC X.                                          00002260
           02 MLIBEL1V  PIC X.                                          00002270
           02 MLIBEL1O  PIC X(9).                                       00002280
           02 MLIGNETO OCCURS   11 TIMES .                              00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MLIGNEA      PIC X.                                     00002310
             03 MLIGNEC PIC X.                                          00002320
             03 MLIGNEP PIC X.                                          00002330
             03 MLIGNEH PIC X.                                          00002340
             03 MLIGNEV PIC X.                                          00002350
             03 MLIGNEO      PIC X(79).                                 00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MLIBEL2A  PIC X.                                          00002380
           02 MLIBEL2C  PIC X.                                          00002390
           02 MLIBEL2P  PIC X.                                          00002400
           02 MLIBEL2H  PIC X.                                          00002410
           02 MLIBEL2V  PIC X.                                          00002420
           02 MLIBEL2O  PIC X(15).                                      00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MTOTALA   PIC X.                                          00002450
           02 MTOTALC   PIC X.                                          00002460
           02 MTOTALP   PIC X.                                          00002470
           02 MTOTALH   PIC X.                                          00002480
           02 MTOTALV   PIC X.                                          00002490
           02 MTOTALO   PIC X(13).                                      00002500
      * MESSAGE ERREUR                                                  00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MLIBERRA  PIC X.                                          00002530
           02 MLIBERRC  PIC X.                                          00002540
           02 MLIBERRP  PIC X.                                          00002550
           02 MLIBERRH  PIC X.                                          00002560
           02 MLIBERRV  PIC X.                                          00002570
           02 MLIBERRO  PIC X(78).                                      00002580
      * CODE TRANSACTION                                                00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
      * CICS DE TRAVAIL                                                 00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MCICSA    PIC X.                                          00002690
           02 MCICSC    PIC X.                                          00002700
           02 MCICSP    PIC X.                                          00002710
           02 MCICSH    PIC X.                                          00002720
           02 MCICSV    PIC X.                                          00002730
           02 MCICSO    PIC X(5).                                       00002740
      * NETNAME                                                         00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MNETNAMA  PIC X.                                          00002770
           02 MNETNAMC  PIC X.                                          00002780
           02 MNETNAMP  PIC X.                                          00002790
           02 MNETNAMH  PIC X.                                          00002800
           02 MNETNAMV  PIC X.                                          00002810
           02 MNETNAMO  PIC X(8).                                       00002820
      * CODE TERMINAL                                                   00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MSCREENA  PIC X.                                          00002850
           02 MSCREENC  PIC X.                                          00002860
           02 MSCREENP  PIC X.                                          00002870
           02 MSCREENH  PIC X.                                          00002880
           02 MSCREENV  PIC X.                                          00002890
           02 MSCREENO  PIC X(5).                                       00002900
                                                                                
