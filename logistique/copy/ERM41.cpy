      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM41   ERM41                                              00000020
      ***************************************************************** 00000030
       01   ERM41I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEMAXL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNPAGEMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNPAGEMAXF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEMAXI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMAGI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLMAGI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREGRL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MTYPREGRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREGRF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MTYPREGRI      PIC X(2).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMRL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCFAMRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMRF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCFAMRI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAMRL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLFAMRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFAMRF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLFAMRI   PIC X(20).                                      00000450
           02 LIGNEI OCCURS   13 TIMES .                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCFAMI  PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLFAMI  PIC X(20).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPREGL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MTYPREGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPREGF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MTYPREGI     PIC X(2).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(80).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * SDF: ERM41   ERM41                                              00000800
      ***************************************************************** 00000810
       01   ERM41O REDEFINES ERM41I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MNPAGEA   PIC X.                                          00000990
           02 MNPAGEC   PIC X.                                          00001000
           02 MNPAGEP   PIC X.                                          00001010
           02 MNPAGEH   PIC X.                                          00001020
           02 MNPAGEV   PIC X.                                          00001030
           02 MNPAGEO   PIC X(3).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MNPAGEMAXA     PIC X.                                     00001060
           02 MNPAGEMAXC     PIC X.                                     00001070
           02 MNPAGEMAXP     PIC X.                                     00001080
           02 MNPAGEMAXH     PIC X.                                     00001090
           02 MNPAGEMAXV     PIC X.                                     00001100
           02 MNPAGEMAXO     PIC X(3).                                  00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MNSOCA    PIC X.                                          00001130
           02 MNSOCC    PIC X.                                          00001140
           02 MNSOCP    PIC X.                                          00001150
           02 MNSOCH    PIC X.                                          00001160
           02 MNSOCV    PIC X.                                          00001170
           02 MNSOCO    PIC X(3).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MNMAGA    PIC X.                                          00001200
           02 MNMAGC    PIC X.                                          00001210
           02 MNMAGP    PIC X.                                          00001220
           02 MNMAGH    PIC X.                                          00001230
           02 MNMAGV    PIC X.                                          00001240
           02 MNMAGO    PIC X(3).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MLMAGA    PIC X.                                          00001270
           02 MLMAGC    PIC X.                                          00001280
           02 MLMAGP    PIC X.                                          00001290
           02 MLMAGH    PIC X.                                          00001300
           02 MLMAGV    PIC X.                                          00001310
           02 MLMAGO    PIC X(20).                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MTYPREGRA      PIC X.                                     00001340
           02 MTYPREGRC PIC X.                                          00001350
           02 MTYPREGRP PIC X.                                          00001360
           02 MTYPREGRH PIC X.                                          00001370
           02 MTYPREGRV PIC X.                                          00001380
           02 MTYPREGRO      PIC X(2).                                  00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MCFAMRA   PIC X.                                          00001410
           02 MCFAMRC   PIC X.                                          00001420
           02 MCFAMRP   PIC X.                                          00001430
           02 MCFAMRH   PIC X.                                          00001440
           02 MCFAMRV   PIC X.                                          00001450
           02 MCFAMRO   PIC X(5).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MLFAMRA   PIC X.                                          00001480
           02 MLFAMRC   PIC X.                                          00001490
           02 MLFAMRP   PIC X.                                          00001500
           02 MLFAMRH   PIC X.                                          00001510
           02 MLFAMRV   PIC X.                                          00001520
           02 MLFAMRO   PIC X(20).                                      00001530
           02 LIGNEO OCCURS   13 TIMES .                                00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MCFAMA  PIC X.                                          00001560
             03 MCFAMC  PIC X.                                          00001570
             03 MCFAMP  PIC X.                                          00001580
             03 MCFAMH  PIC X.                                          00001590
             03 MCFAMV  PIC X.                                          00001600
             03 MCFAMO  PIC X(5).                                       00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MLFAMA  PIC X.                                          00001630
             03 MLFAMC  PIC X.                                          00001640
             03 MLFAMP  PIC X.                                          00001650
             03 MLFAMH  PIC X.                                          00001660
             03 MLFAMV  PIC X.                                          00001670
             03 MLFAMO  PIC X(20).                                      00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MTYPREGA     PIC X.                                     00001700
             03 MTYPREGC     PIC X.                                     00001710
             03 MTYPREGP     PIC X.                                     00001720
             03 MTYPREGH     PIC X.                                     00001730
             03 MTYPREGV     PIC X.                                     00001740
             03 MTYPREGO     PIC X(2).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBERRA  PIC X.                                          00001770
           02 MLIBERRC  PIC X.                                          00001780
           02 MLIBERRP  PIC X.                                          00001790
           02 MLIBERRH  PIC X.                                          00001800
           02 MLIBERRV  PIC X.                                          00001810
           02 MLIBERRO  PIC X(80).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCODTRAA  PIC X.                                          00001840
           02 MCODTRAC  PIC X.                                          00001850
           02 MCODTRAP  PIC X.                                          00001860
           02 MCODTRAH  PIC X.                                          00001870
           02 MCODTRAV  PIC X.                                          00001880
           02 MCODTRAO  PIC X(4).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCICSA    PIC X.                                          00001910
           02 MCICSC    PIC X.                                          00001920
           02 MCICSP    PIC X.                                          00001930
           02 MCICSH    PIC X.                                          00001940
           02 MCICSV    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
