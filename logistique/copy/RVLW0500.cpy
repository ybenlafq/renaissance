      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVLW0500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLW0500                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLW0500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLW0500.                                                            
      *}                                                                        
           02  LW05-NSOCDEPOT      PIC X(0003).                                 
           02  LW05-NDEPOT         PIC X(0003).                                 
           02  LW05-NSOCIETE       PIC X(0003).                                 
           02  LW05-NLIEU          PIC X(0003).                                 
           02  LW05-NMUTATION      PIC X(0007).                                 
           02  LW05-TYPOP          PIC X(0002).                                 
           02  LW05-URGENCE        PIC X(0001).                                 
           02  LW05-CSELART        PIC X(0005).                                 
           02  LW05-DFINSAIS       PIC X(0008).                                 
           02  LW05-DDESTOCK       PIC X(0008).                                 
           02  LW05-HCHARG         PIC X(0005).                                 
           02  LW05-DCHARG         PIC X(0008).                                 
           02  LW05-DMUTATION      PIC X(0008).                                 
           02  LW05-WTYPLIVR       PIC X(0001).                                 
           02  LW05-WCPTCLIENT     PIC X(0001).                                 
           02  LW05-WFLAG          PIC X(0001).                                 
           02  LW05-DTRANSFERT     PIC X(0008).                                 
           02  LW05-CTRANSFERT     PIC X(0008).                                 
           02  LW05-DEXPED         PIC X(0008).                                 
           02  LW05-DSYST          PIC S9(13) COMP-3.                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLW0500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLW0500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLW0500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-TYPOP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-TYPOP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-URGENCE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-URGENCE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-CSELART-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-CSELART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-DFINSAIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-DFINSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-DDESTOCK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-DDESTOCK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-HCHARG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-HCHARG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-DCHARG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-DCHARG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-DMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-DMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-WTYPLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-WTYPLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-WCPTCLIENT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-WCPTCLIENT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-WFLAG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-WFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-DTRANSFERT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-DTRANSFERT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-CTRANSFERT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-CTRANSFERT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-DEXPED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-DEXPED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
