      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU07   EMU07                                              00000020
      ***************************************************************** 00000030
       01   EFA11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCIETEI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRIL     COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MTRIL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTRIF     PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MTRII     PIC X.                                          00000370
           02 M153I OCCURS   12 TIMES .                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTATIONL  COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNMUTATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNMUTATIONF  PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNMUTATIONI  PIC X(7).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMUTATIONL  COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MDMUTATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDMUTATIONF  PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MDMUTATIONI  PIC X(10).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCENTRL   COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNSOCENTRL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCENTRF   PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNSOCENTRI   PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEPOTL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNDEPOTF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNDEPOTI     PIC X(3).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDEPOTL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLDEPOTF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLDEPOTI     PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBPIECESL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNBPIECESL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNBPIECESF   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNBPIECESI   PIC X(6).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWVALL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MWVALL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWVALF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MWVALI  PIC X.                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MWSELL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWSELF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MWSELI  PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(78).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: EMU07   EMU07                                              00000920
      ***************************************************************** 00000930
       01   EFA11O REDEFINES EFA11I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MPAGEA    PIC X.                                          00001110
           02 MPAGEC    PIC X.                                          00001120
           02 MPAGEP    PIC X.                                          00001130
           02 MPAGEH    PIC X.                                          00001140
           02 MPAGEV    PIC X.                                          00001150
           02 MPAGEO    PIC X(3).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MPAGEMAXA      PIC X.                                     00001180
           02 MPAGEMAXC PIC X.                                          00001190
           02 MPAGEMAXP PIC X.                                          00001200
           02 MPAGEMAXH PIC X.                                          00001210
           02 MPAGEMAXV PIC X.                                          00001220
           02 MPAGEMAXO      PIC X(3).                                  00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNSOCIETEA     PIC X.                                     00001250
           02 MNSOCIETEC     PIC X.                                     00001260
           02 MNSOCIETEP     PIC X.                                     00001270
           02 MNSOCIETEH     PIC X.                                     00001280
           02 MNSOCIETEV     PIC X.                                     00001290
           02 MNSOCIETEO     PIC X(3).                                  00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MNLIEUA   PIC X.                                          00001320
           02 MNLIEUC   PIC X.                                          00001330
           02 MNLIEUP   PIC X.                                          00001340
           02 MNLIEUH   PIC X.                                          00001350
           02 MNLIEUV   PIC X.                                          00001360
           02 MNLIEUO   PIC X(3).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MLLIEUA   PIC X.                                          00001390
           02 MLLIEUC   PIC X.                                          00001400
           02 MLLIEUP   PIC X.                                          00001410
           02 MLLIEUH   PIC X.                                          00001420
           02 MLLIEUV   PIC X.                                          00001430
           02 MLLIEUO   PIC X(20).                                      00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MTRIA     PIC X.                                          00001460
           02 MTRIC     PIC X.                                          00001470
           02 MTRIP     PIC X.                                          00001480
           02 MTRIH     PIC X.                                          00001490
           02 MTRIV     PIC X.                                          00001500
           02 MTRIO     PIC X.                                          00001510
           02 M153O OCCURS   12 TIMES .                                 00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MNMUTATIONA  PIC X.                                     00001540
             03 MNMUTATIONC  PIC X.                                     00001550
             03 MNMUTATIONP  PIC X.                                     00001560
             03 MNMUTATIONH  PIC X.                                     00001570
             03 MNMUTATIONV  PIC X.                                     00001580
             03 MNMUTATIONO  PIC X(7).                                  00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MDMUTATIONA  PIC X.                                     00001610
             03 MDMUTATIONC  PIC X.                                     00001620
             03 MDMUTATIONP  PIC X.                                     00001630
             03 MDMUTATIONH  PIC X.                                     00001640
             03 MDMUTATIONV  PIC X.                                     00001650
             03 MDMUTATIONO  PIC X(10).                                 00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MNSOCENTRA   PIC X.                                     00001680
             03 MNSOCENTRC   PIC X.                                     00001690
             03 MNSOCENTRP   PIC X.                                     00001700
             03 MNSOCENTRH   PIC X.                                     00001710
             03 MNSOCENTRV   PIC X.                                     00001720
             03 MNSOCENTRO   PIC X(3).                                  00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MNDEPOTA     PIC X.                                     00001750
             03 MNDEPOTC     PIC X.                                     00001760
             03 MNDEPOTP     PIC X.                                     00001770
             03 MNDEPOTH     PIC X.                                     00001780
             03 MNDEPOTV     PIC X.                                     00001790
             03 MNDEPOTO     PIC X(3).                                  00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MLDEPOTA     PIC X.                                     00001820
             03 MLDEPOTC     PIC X.                                     00001830
             03 MLDEPOTP     PIC X.                                     00001840
             03 MLDEPOTH     PIC X.                                     00001850
             03 MLDEPOTV     PIC X.                                     00001860
             03 MLDEPOTO     PIC X(20).                                 00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MNBPIECESA   PIC X.                                     00001890
             03 MNBPIECESC   PIC X.                                     00001900
             03 MNBPIECESP   PIC X.                                     00001910
             03 MNBPIECESH   PIC X.                                     00001920
             03 MNBPIECESV   PIC X.                                     00001930
             03 MNBPIECESO   PIC X(6).                                  00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MWVALA  PIC X.                                          00001960
             03 MWVALC  PIC X.                                          00001970
             03 MWVALP  PIC X.                                          00001980
             03 MWVALH  PIC X.                                          00001990
             03 MWVALV  PIC X.                                          00002000
             03 MWVALO  PIC X.                                          00002010
             03 FILLER       PIC X(2).                                  00002020
             03 MWSELA  PIC X.                                          00002030
             03 MWSELC  PIC X.                                          00002040
             03 MWSELP  PIC X.                                          00002050
             03 MWSELH  PIC X.                                          00002060
             03 MWSELV  PIC X.                                          00002070
             03 MWSELO  PIC X.                                          00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(78).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
