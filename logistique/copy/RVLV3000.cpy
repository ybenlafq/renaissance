      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLV3000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLV3000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLV3000.                                                            
           02  LV30-NSOCDEPOT      PIC X(0003).                                 
           02  LV30-NDEPOT         PIC X(0003).                                 
           02  LV30-NRECQUAI       PIC X(0007).                                 
           02  LV30-NCODIC         PIC X(0007).                                 
           02  LV30-NCDE           PIC X(0007).                                 
           02  LV30-NENTCDE        PIC X(0005).                                 
           02  LV30-DJRECQUAI      PIC X(0008).                                 
           02  LV30-CTYPAGE        PIC X(0002).                                 
           02  LV30-QRECQUAI       PIC S9(05) COMP-3.                           
           02  LV30-USERID         PIC X(0008).                                 
           02  LV30-DSYST          PIC S9(13) COMP-3.                           
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLV3000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLV3000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV30-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV30-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV30-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV30-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV30-NRECQUAI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV30-NRECQUAI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV30-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV30-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV30-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV30-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV30-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV30-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV30-DJRECQUAI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV30-DJRECQUAI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV30-CTYPAGE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV30-CTYPAGE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV30-USERID-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV30-USERID-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
