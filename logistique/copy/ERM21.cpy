      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM21   ERM21                                              00000020
      ***************************************************************** 00000030
       01   ERM21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCGROUPI  PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCHEFPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFPF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCHEFPI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFPL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCHEFPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCHEFPF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLCHEFPI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAGREGL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLAGREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAGREGF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLAGREGI  PIC X(20).                                      00000410
           02 MDSEMD OCCURS   70 TIMES .                                00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDSEML  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MDSEML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDSEMF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MDSEMI  PIC X(6).                                       00000460
           02 MQPMD OCCURS   70 TIMES .                                 00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPML   COMP PIC S9(4).                                 00000480
      *--                                                                       
             03 MQPML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQPMF   PIC X.                                          00000490
             03 FILLER  PIC X(4).                                       00000500
             03 MQPMI   PIC X(5).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MZONCMDI  PIC X(12).                                      00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLIBERRI  PIC X(61).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCODTRAI  PIC X(4).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCICSI    PIC X(5).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MNETNAMI  PIC X(8).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MSCREENI  PIC X(4).                                       00000750
      ***************************************************************** 00000760
      * SDF: ERM21   ERM21                                              00000770
      ***************************************************************** 00000780
       01   ERM21O REDEFINES ERM21I.                                    00000790
           02 FILLER    PIC X(12).                                      00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MDATJOUA  PIC X.                                          00000820
           02 MDATJOUC  PIC X.                                          00000830
           02 MDATJOUP  PIC X.                                          00000840
           02 MDATJOUH  PIC X.                                          00000850
           02 MDATJOUV  PIC X.                                          00000860
           02 MDATJOUO  PIC X(10).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MTIMJOUA  PIC X.                                          00000890
           02 MTIMJOUC  PIC X.                                          00000900
           02 MTIMJOUP  PIC X.                                          00000910
           02 MTIMJOUH  PIC X.                                          00000920
           02 MTIMJOUV  PIC X.                                          00000930
           02 MTIMJOUO  PIC X(5).                                       00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MCGROUPA  PIC X.                                          00000960
           02 MCGROUPC  PIC X.                                          00000970
           02 MCGROUPP  PIC X.                                          00000980
           02 MCGROUPH  PIC X.                                          00000990
           02 MCGROUPV  PIC X.                                          00001000
           02 MCGROUPO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNPAGEA   PIC X.                                          00001030
           02 MNPAGEC   PIC X.                                          00001040
           02 MNPAGEP   PIC X.                                          00001050
           02 MNPAGEH   PIC X.                                          00001060
           02 MNPAGEV   PIC X.                                          00001070
           02 MNPAGEO   PIC X(5).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MCHEFPA   PIC X.                                          00001100
           02 MCHEFPC   PIC X.                                          00001110
           02 MCHEFPP   PIC X.                                          00001120
           02 MCHEFPH   PIC X.                                          00001130
           02 MCHEFPV   PIC X.                                          00001140
           02 MCHEFPO   PIC X(5).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MLCHEFPA  PIC X.                                          00001170
           02 MLCHEFPC  PIC X.                                          00001180
           02 MLCHEFPP  PIC X.                                          00001190
           02 MLCHEFPH  PIC X.                                          00001200
           02 MLCHEFPV  PIC X.                                          00001210
           02 MLCHEFPO  PIC X(20).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MCFAMA    PIC X.                                          00001240
           02 MCFAMC    PIC X.                                          00001250
           02 MCFAMP    PIC X.                                          00001260
           02 MCFAMH    PIC X.                                          00001270
           02 MCFAMV    PIC X.                                          00001280
           02 MCFAMO    PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MLFAMA    PIC X.                                          00001310
           02 MLFAMC    PIC X.                                          00001320
           02 MLFAMP    PIC X.                                          00001330
           02 MLFAMH    PIC X.                                          00001340
           02 MLFAMV    PIC X.                                          00001350
           02 MLFAMO    PIC X(20).                                      00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MLAGREGA  PIC X.                                          00001380
           02 MLAGREGC  PIC X.                                          00001390
           02 MLAGREGP  PIC X.                                          00001400
           02 MLAGREGH  PIC X.                                          00001410
           02 MLAGREGV  PIC X.                                          00001420
           02 MLAGREGO  PIC X(20).                                      00001430
           02 DFHMS1 OCCURS   70 TIMES .                                00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MDSEMA  PIC X.                                          00001460
             03 MDSEMC  PIC X.                                          00001470
             03 MDSEMP  PIC X.                                          00001480
             03 MDSEMH  PIC X.                                          00001490
             03 MDSEMV  PIC X.                                          00001500
             03 MDSEMO  PIC X(6).                                       00001510
           02 DFHMS2 OCCURS   70 TIMES .                                00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MQPMA   PIC X.                                          00001540
             03 MQPMC   PIC X.                                          00001550
             03 MQPMP   PIC X.                                          00001560
             03 MQPMH   PIC X.                                          00001570
             03 MQPMV   PIC X.                                          00001580
             03 MQPMO   PIC X(5).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MZONCMDA  PIC X.                                          00001610
           02 MZONCMDC  PIC X.                                          00001620
           02 MZONCMDP  PIC X.                                          00001630
           02 MZONCMDH  PIC X.                                          00001640
           02 MZONCMDV  PIC X.                                          00001650
           02 MZONCMDO  PIC X(12).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLIBERRA  PIC X.                                          00001680
           02 MLIBERRC  PIC X.                                          00001690
           02 MLIBERRP  PIC X.                                          00001700
           02 MLIBERRH  PIC X.                                          00001710
           02 MLIBERRV  PIC X.                                          00001720
           02 MLIBERRO  PIC X(61).                                      00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCODTRAA  PIC X.                                          00001750
           02 MCODTRAC  PIC X.                                          00001760
           02 MCODTRAP  PIC X.                                          00001770
           02 MCODTRAH  PIC X.                                          00001780
           02 MCODTRAV  PIC X.                                          00001790
           02 MCODTRAO  PIC X(4).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCICSA    PIC X.                                          00001820
           02 MCICSC    PIC X.                                          00001830
           02 MCICSP    PIC X.                                          00001840
           02 MCICSH    PIC X.                                          00001850
           02 MCICSV    PIC X.                                          00001860
           02 MCICSO    PIC X(5).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNETNAMA  PIC X.                                          00001890
           02 MNETNAMC  PIC X.                                          00001900
           02 MNETNAMP  PIC X.                                          00001910
           02 MNETNAMH  PIC X.                                          00001920
           02 MNETNAMV  PIC X.                                          00001930
           02 MNETNAMO  PIC X(8).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MSCREENA  PIC X.                                          00001960
           02 MSCREENC  PIC X.                                          00001970
           02 MSCREENP  PIC X.                                          00001980
           02 MSCREENH  PIC X.                                          00001990
           02 MSCREENV  PIC X.                                          00002000
           02 MSCREENO  PIC X(4).                                       00002010
                                                                                
