      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : DEFINITION DES PROFILS D'AFFILIATION (FAM / ENTREPOT ) *         
      *        POUR MISE A JOUR VUE RVGA01WN     (PGR : TFL22)        *         
      *****************************************************************         
      *                                                               *         
       01  TS-FL22.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-FL22-LONG                 PIC S9(3) COMP-3                     
                                           VALUE +57.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-FL22-DONNEES.                                                  
      *----------------------------------  MODE DE MAJ                          
      *                                         ' ' : PAS DE MAJ                
      *                                         'C' : CREATION                  
      *                                         'M' : MODIFICATION              
              03 TS-FL22-CMAJ              PIC X(1).                            
      *----------------------------------  CODIC                                
              03 TS-FL22-CDAC              PIC X(1).                            
      *----------------------------------  CODIC                                
              03 TS-FL22-CMAR              PIC X(5).                            
      *----------------------------------  LIBELLE CODIC                        
              03 TS-FL22-LMAR              PIC X(20).                           
      *----------------------------------  ZONE ENTREPOTS D'AFFILIATION         
              03 TS-FL22-ENTAFF            OCCURS 5.                            
      *----------------------------------  CODE SOCIETE                         
                 04 TS-FL22-CSOCAFF           PIC X(03).                        
      *----------------------------------  CODE ENTREPOT                        
                 04 TS-FL22-CDEPAFF           PIC X(03).                        
                                                                                
