      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TTL50                    TR: TL00  *    00020000
      *          GESTION LIVRAISON (MISE A JOUR TOPE LIVRE)        *    00030000
      *          GESTION DU HS   ( SUITE TTL27 / TTL28 )           *    00040000
      *                                                                 00050000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3601  00060001
      *                                                                 00070000
      *  TRANSACTION TL50 : SUPPRESSION QUANTITES DU HS            *    00080000
      *                                                                 00090000
          02 COMM-TL50-APPLI REDEFINES COMM-TL00-APPLI.                 00100000
      *------------------------------ ZONE DONNEES TTL27/28             00110000
             03 COMM-TL50-DONNEES-1            PIC X(2395).             00120002
      *------------------------------ ZONE DONNEES TTL50                00130000
             03 COMM-TL50-DONNEES-2.                                    00140000
      *------------------------------ CODE PROGRAMME APPELANT           00150000
                04 COMM-TL50-CPROG             PIC X(05).               00160000
      *------------------------------ CODE ARTICLE                      00170000
                04 COMM-TL50-NCODIC            PIC X(07).               00180000
      *------------------------------ LIBELLE ARTICLE                   00190000
                04 COMM-TL50-LREFFOURN         PIC X(20).               00200000
      *------------------------------ CODE FAMILLE                      00210000
                04 COMM-TL50-CFAM              PIC X(05).               00220000
      *------------------------------ LIBELLE FAMILLE                   00230000
                04 COMM-TL50-LFAM              PIC X(20).               00240000
      *------------------------------ CODE MARQUE                       00250000
                04 COMM-TL50-CMARQ             PIC X(05).               00260000
      *------------------------------ LIBELLE MARQUE                    00270000
                04 COMM-TL50-LMARQ             PIC X(20).               00280000
      *------------------------------ LIBELLE MARQUE                    00290001
                04 COMM-TL50-QTEHS             PIC 9(05).               00300001
      *------------------------------ CODE ARTICLE INEXISTANT           00310001
                04 COMM-TL50-ARTINEX           PIC X(01).               00320001
      *------------------------------ ZONE DONNEES TTL50                00330000
             03 COMM-TL50-DONNEES-3.                                    00340000
      *------------------------------ DERNIER IND-TS                    00350000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-TL50-PAGSUI            PIC S9(4) COMP.          00360000
      *--                                                                       
                04 COMM-TL50-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN IND-TS                     00370000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-TL50-PAGENC            PIC S9(4) COMP.          00380000
      *--                                                                       
                04 COMM-TL50-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00390000
                04 COMM-TL50-NUMPAG            PIC 9(3).                00400000
      *------------------------------ INDICATEUR FIN DE TABLE           00410000
                04 COMM-TL50-INDPAG            PIC 9.                   00420000
      *------------------------------ TABLE N� TS PR PAGINATION         00430000
                04 COMM-TL50-TABTS.                                     00440001
      *------------------------------ 1ERS IND TS DES PAGES <=> NHS     00450000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-TL50-NUMTS  PIC S9(4) COMP OCCURS 100.       00460001
      *--                                                                       
                   05 COMM-TL50-NUMTS  PIC S9(4) COMP-5 OCCURS 100.             
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS               00470000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-TL50-IND-RECH-TS       PIC S9(4) COMP.          00480000
      *--                                                                       
                04 COMM-TL50-IND-RECH-TS       PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ IND TS MAX                        00490000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-TL50-IND-TS-MAX        PIC S9(4) COMP.          00500000
      *--                                                                       
                04 COMM-TL50-IND-TS-MAX        PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ IND TS MAX CODIC TSTLH2           00510001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-TL50-IND-TS-CODIC      PIC S9(4) COMP.          00520000
      *--                                                                       
                04 COMM-TL50-IND-TS-CODIC      PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ IND TS MAX CODIC TSTLH1           00530001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-TL50-IND-MAX-CODIC     PIC S9(4) COMP.          00540001
      *--                                                                       
                04 COMM-TL50-IND-MAX-CODIC     PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ IND TS DEB CODIC                  00550000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-TL50-IND-ITEM-CODIC    PIC S9(4) COMP.          00560000
      *--                                                                       
                04 COMM-TL50-IND-ITEM-CODIC    PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ZONE DONNEES 4 TTL50              00570000
             03 COMM-TL50-DONNEES-4.                                    00580000
      *------------------------------ ZONE NHS PAGE                     00590000
                04 COMM-TL50-TABNHS.                                    00600000
      *------------------------------ ZONE LIGNES NHS                   00610000
                   05 COMM-TL50-LIGNHS OCCURS 11.                       00620000
      *------------------------------ CODE SELECTION                    00630000
                      06 COMM-TL50-CMVT           PIC X(01).            00640000
      *------------------------------ NUMERO DE HS                      00650000
                      06 COMM-TL50-NLIEUENT       PIC X(3).             00660001
                      06 COMM-TL50-NHS            PIC X(7).             00670001
      *------------------------------ QTE EN HS                         00680000
                      06 COMM-TL50-QHS            PIC X(05).            00690000
      *------------------------------ SOCIETE DESTINATION               00700000
                      06 COMM-TL50-SOCDEST        PIC X(03).            00710000
      *------------------------------ LIEU DESTINATION                  00720000
                      06 COMM-TL50-LIEUDEST       PIC X(03).            00730000
      *------------------------------ SOUS LIEU DESTINATION             00740000
                      06 COMM-TL50-SSLIEUDEST     PIC X(03).            00750000
      *------------------------------ MOTIF ENTREE EN HS                00760000
                      06 COMM-TL50-CMOTIFENT      PIC X(10).            00770000
      *------------------------------ ZONE DONNEES 5                    00780000
             03 COMM-TL50-DONNEES-5.                                    00790000
      *------------------------------ ZONES SWAP                        00800000
                   05 COMM-TL50-ATTR           PIC X OCCURS 390.        00810001
      *------------------------------ ZONE LIBRE                        00820000
      *      03 COMM-TL50-LIBRE          PIC X(01).                     00830001
      ***************************************************************** 00840000
                                                                                
