      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
YS1194* COM-LS00-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
YS1194* COM-LS00-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +8192            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
YS1194*01  COM-LS00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
YS1194*01  COM-LS00-LONG-COMMAREA PIC S9(4) COMP VALUE +8192.                   
      *--                                                                       
       01  COM-LS00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +8192.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA                                                  
      * ZONES RESERVEES A AIDA                                                  
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS                                   
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION                     
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC XX.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS PIC 99.                                        
              05 COMM-DATE-SEMAA PIC 99.                                        
              05 COMM-DATE-SEMNU PIC 99.                                        
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES APPLICATIVES                                            
      *                                                                         
      *                                                                         
           02  COMM-LS00-APPLI.                                                 
      *=> ZONES DE SAUVEGARDE COMMUNES AU MENU PRINCIPAL                        
      *=> ET AUX TRANSACTIONS DE NIVEAU INFERIEUR                               
              03  COMM-LS00.                                                    
                  05  COMM-LS00-MZONCMD          PIC X(15).                     
                  05  COMM-LS00-NSOC             PIC X(03).                     
                  05  COMM-LS00-NSOC2            PIC X(03).                     
                  05  COMM-LS00-LSELA            PIC X(05).                     
                  05  COMM-LS00-LSELA2           PIC X(05).                     
                  05  COMM-LS00-CFAM             PIC X(05).                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           05  COMM-SWAP-CURS             PIC S9(4) COMP.                
      *--                                                                       
                  05  COMM-SWAP-CURS             PIC S9(4) COMP-5.              
      *}                                                                        
                  05  COMM-SWAP-ATTR             OCCURS 150 PIC X(1).   00710000
                  05  COMM-LS00-CODLANG          PIC X(2).                      
                  05  COMM-LS00-CODPIC           PIC X(2).                      
                  05  COMM-LS00-TRI              PIC X(1).                      
                  05  COMM-LS00-FILLER           PIC X(183).                    
      *=> DE NIVEAU INFERIEUR                                                   
YS1194*      02  COMM-LS01-LS02-LS03             PIC X(3500).                   
YS1194       02  COMM-LS01-LS02-LS03             PIC X(7596).                   
             02  COMM-LS01-APPLI REDEFINES COMM-LS01-LS02-LS03.                 
                 03 COMM-LS01-PAGE               PIC 9.                         
                 03 COMM-LS01-PAGE-MAX           PIC 9.                         
                 03 COMM-LS01-MODIF-DONNEE       PIC X.                         
                 03 COMM-LS01-ITEMAX             PIC S9(3) COMP-3.              
YS1194           03 FILLER                       PIC X(7591).                   
             02  COMM-LS02-APPLI REDEFINES COMM-LS01-LS02-LS03.                 
                 03 COMM-LS02-PAG                PIC 9(3).                      
                 03 COMM-LS02-PAG-MAX            PIC 9(3).                      
                 03 COMM-LS02-PAGE               PIC 9.                         
                 03 COMM-LS02-PAGE-MAX           PIC 9.                         
                 03 COMM-LS02-MODIF-DONNEE       PIC X.                         
                 03 COMM-LS02-TABLEAU-FAM.                                      
YS1194*             04 COMM-LS02-LIGNE-FAM       OCCURS 600.                    
YS1194              04 COMM-LS02-LIGNE-FAM       OCCURS 1000.                   
                       05 COMM-LS02-CFAM         PIC X(5).                      
YS1194*          03 COMM-LS02-ITEMAX             PIC S9(3) COMP-3.              
YS1194           03 COMM-LS02-ITEMAX             PIC S9(5) COMP-3.              
                 03 COMM-LS02-TABLEAU-CSE.                                      
YS1194*             04 COMM-LS02-LIGNE-CSE       OCCURS 75.                     
YS1194              04 COMM-LS02-LIGNE-CSE       OCCURS 100.                    
                       05 COMM-LS02-CSELART      PIC X(5).                      
YS1194           03 FILLER                       PIC X(2084).                   
             02  COMM-LS03-APPLI REDEFINES COMM-LS01-LS02-LS03.                 
                 03 COMM-LS03-PAGE               PIC 9.                         
                 03 COMM-LS03-PAGE-MAX           PIC 9.                         
                 03 COMM-LS03-ITEMAX             PIC S9(3) COMP-3.              
YS1194           03 FILLER                       PIC X(7592).                   
             02  COMM-LS04-APPLI REDEFINES COMM-LS01-LS02-LS03.                 
                 03 COMM-LS04-PAGE               PIC 9.                         
                 03 COMM-LS04-PAGE-MAX           PIC 9.                         
                 03 COMM-LS04-ITEMAX             PIC S9(3) COMP-3.              
YS1194           03 FILLER                       PIC X(7592).                   
                                                                                
