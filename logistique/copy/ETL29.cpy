      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ETL29   ETL29                                              00000020
      ***************************************************************** 00000030
       01   ETL29I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MJOURL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJOURF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MJOURI    PIC X(8).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTOURL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLIBTOURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBTOURF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLIBTOURI      PIC X(16).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPROFILL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MPROFILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPROFILF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MPROFILI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOURNEEL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MTOURNEEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOURNEEF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTOURNEEI      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNMAGI    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNVENTEI  PIC X(7).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRETGL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCRETGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRETGF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCRETGI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRETGL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLRETGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLRETGF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLRETGI   PIC X(20).                                      00000530
           02 M209I OCCURS   11 TIMES .                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNCODICI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCFAMI  PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCMARQI      PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLREFI  PIC X(18).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCADRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MCADRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCADRF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCADRI  PIC X.                                          00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOMML      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MQCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQCOMMF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQCOMMI      PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLIVRL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQLIVRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQLIVRF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQLIVRI      PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRETL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MCRETL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCRETF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCRETI  PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLRETL  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MLRETL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLRETF  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MLRETI  PIC X(19).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAG2L    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MMAG2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMAG2F    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MMAG2I    PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTE2L      COMP PIC S9(4).                            00000950
      *--                                                                       
           02 MNVENTE2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNVENTE2F      PIC X.                                     00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNVENTE2I      PIC X(7).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJLIVR2L  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MJLIVR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJLIVR2F  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MJLIVR2I  PIC X(6).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(15).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(58).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * SDF: ETL29   ETL29                                              00001280
      ***************************************************************** 00001290
       01   ETL29O REDEFINES ETL29I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MPAGEA    PIC X.                                          00001470
           02 MPAGEC    PIC X.                                          00001480
           02 MPAGEP    PIC X.                                          00001490
           02 MPAGEH    PIC X.                                          00001500
           02 MPAGEV    PIC X.                                          00001510
           02 MPAGEO    PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MJOURA    PIC X.                                          00001540
           02 MJOURC    PIC X.                                          00001550
           02 MJOURP    PIC X.                                          00001560
           02 MJOURH    PIC X.                                          00001570
           02 MJOURV    PIC X.                                          00001580
           02 MJOURO    PIC X(8).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MLIBTOURA      PIC X.                                     00001610
           02 MLIBTOURC PIC X.                                          00001620
           02 MLIBTOURP PIC X.                                          00001630
           02 MLIBTOURH PIC X.                                          00001640
           02 MLIBTOURV PIC X.                                          00001650
           02 MLIBTOURO      PIC X(16).                                 00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MPROFILA  PIC X.                                          00001680
           02 MPROFILC  PIC X.                                          00001690
           02 MPROFILP  PIC X.                                          00001700
           02 MPROFILH  PIC X.                                          00001710
           02 MPROFILV  PIC X.                                          00001720
           02 MPROFILO  PIC X(5).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MTOURNEEA      PIC X.                                     00001750
           02 MTOURNEEC PIC X.                                          00001760
           02 MTOURNEEP PIC X.                                          00001770
           02 MTOURNEEH PIC X.                                          00001780
           02 MTOURNEEV PIC X.                                          00001790
           02 MTOURNEEO      PIC X(3).                                  00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNSOCA    PIC X.                                          00001820
           02 MNSOCC    PIC X.                                          00001830
           02 MNSOCP    PIC X.                                          00001840
           02 MNSOCH    PIC X.                                          00001850
           02 MNSOCV    PIC X.                                          00001860
           02 MNSOCO    PIC X(3).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNMAGA    PIC X.                                          00001890
           02 MNMAGC    PIC X.                                          00001900
           02 MNMAGP    PIC X.                                          00001910
           02 MNMAGH    PIC X.                                          00001920
           02 MNMAGV    PIC X.                                          00001930
           02 MNMAGO    PIC X(3).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MNVENTEA  PIC X.                                          00001960
           02 MNVENTEC  PIC X.                                          00001970
           02 MNVENTEP  PIC X.                                          00001980
           02 MNVENTEH  PIC X.                                          00001990
           02 MNVENTEV  PIC X.                                          00002000
           02 MNVENTEO  PIC X(7).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MCRETGA   PIC X.                                          00002030
           02 MCRETGC   PIC X.                                          00002040
           02 MCRETGP   PIC X.                                          00002050
           02 MCRETGH   PIC X.                                          00002060
           02 MCRETGV   PIC X.                                          00002070
           02 MCRETGO   PIC X(5).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLRETGA   PIC X.                                          00002100
           02 MLRETGC   PIC X.                                          00002110
           02 MLRETGP   PIC X.                                          00002120
           02 MLRETGH   PIC X.                                          00002130
           02 MLRETGV   PIC X.                                          00002140
           02 MLRETGO   PIC X(20).                                      00002150
           02 M209O OCCURS   11 TIMES .                                 00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MNCODICA     PIC X.                                     00002180
             03 MNCODICC     PIC X.                                     00002190
             03 MNCODICP     PIC X.                                     00002200
             03 MNCODICH     PIC X.                                     00002210
             03 MNCODICV     PIC X.                                     00002220
             03 MNCODICO     PIC X(7).                                  00002230
             03 FILLER       PIC X(2).                                  00002240
             03 MCFAMA  PIC X.                                          00002250
             03 MCFAMC  PIC X.                                          00002260
             03 MCFAMP  PIC X.                                          00002270
             03 MCFAMH  PIC X.                                          00002280
             03 MCFAMV  PIC X.                                          00002290
             03 MCFAMO  PIC X(5).                                       00002300
             03 FILLER       PIC X(2).                                  00002310
             03 MCMARQA      PIC X.                                     00002320
             03 MCMARQC PIC X.                                          00002330
             03 MCMARQP PIC X.                                          00002340
             03 MCMARQH PIC X.                                          00002350
             03 MCMARQV PIC X.                                          00002360
             03 MCMARQO      PIC X(5).                                  00002370
             03 FILLER       PIC X(2).                                  00002380
             03 MLREFA  PIC X.                                          00002390
             03 MLREFC  PIC X.                                          00002400
             03 MLREFP  PIC X.                                          00002410
             03 MLREFH  PIC X.                                          00002420
             03 MLREFV  PIC X.                                          00002430
             03 MLREFO  PIC X(18).                                      00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MCADRA  PIC X.                                          00002460
             03 MCADRC  PIC X.                                          00002470
             03 MCADRP  PIC X.                                          00002480
             03 MCADRH  PIC X.                                          00002490
             03 MCADRV  PIC X.                                          00002500
             03 MCADRO  PIC X.                                          00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MQCOMMA      PIC X.                                     00002530
             03 MQCOMMC PIC X.                                          00002540
             03 MQCOMMP PIC X.                                          00002550
             03 MQCOMMH PIC X.                                          00002560
             03 MQCOMMV PIC X.                                          00002570
             03 MQCOMMO      PIC ZZZZZ.                                 00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MQLIVRA      PIC X.                                     00002600
             03 MQLIVRC PIC X.                                          00002610
             03 MQLIVRP PIC X.                                          00002620
             03 MQLIVRH PIC X.                                          00002630
             03 MQLIVRV PIC X.                                          00002640
             03 MQLIVRO      PIC ZZZZZ.                                 00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MCRETA  PIC X.                                          00002670
             03 MCRETC  PIC X.                                          00002680
             03 MCRETP  PIC X.                                          00002690
             03 MCRETH  PIC X.                                          00002700
             03 MCRETV  PIC X.                                          00002710
             03 MCRETO  PIC X(5).                                       00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MLRETA  PIC X.                                          00002740
             03 MLRETC  PIC X.                                          00002750
             03 MLRETP  PIC X.                                          00002760
             03 MLRETH  PIC X.                                          00002770
             03 MLRETV  PIC X.                                          00002780
             03 MLRETO  PIC X(19).                                      00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MMAG2A    PIC X.                                          00002810
           02 MMAG2C    PIC X.                                          00002820
           02 MMAG2P    PIC X.                                          00002830
           02 MMAG2H    PIC X.                                          00002840
           02 MMAG2V    PIC X.                                          00002850
           02 MMAG2O    PIC X(3).                                       00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MNVENTE2A      PIC X.                                     00002880
           02 MNVENTE2C PIC X.                                          00002890
           02 MNVENTE2P PIC X.                                          00002900
           02 MNVENTE2H PIC X.                                          00002910
           02 MNVENTE2V PIC X.                                          00002920
           02 MNVENTE2O      PIC 9999999.                               00002930
           02 FILLER    PIC X(2).                                       00002940
           02 MJLIVR2A  PIC X.                                          00002950
           02 MJLIVR2C  PIC X.                                          00002960
           02 MJLIVR2P  PIC X.                                          00002970
           02 MJLIVR2H  PIC X.                                          00002980
           02 MJLIVR2V  PIC X.                                          00002990
           02 MJLIVR2O  PIC X(6).                                       00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MZONCMDA  PIC X.                                          00003020
           02 MZONCMDC  PIC X.                                          00003030
           02 MZONCMDP  PIC X.                                          00003040
           02 MZONCMDH  PIC X.                                          00003050
           02 MZONCMDV  PIC X.                                          00003060
           02 MZONCMDO  PIC X(15).                                      00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(58).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
