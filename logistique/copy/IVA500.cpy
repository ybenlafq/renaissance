      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA500      *        
      *                                                                *        
      *          CRITERES DE TRI  07,02,BI,A,                          *        
      *                           09,07,BI,A,                          *        
      *                           16,08,BI,A,                          *        
      *                           24,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA500.                                                        
            05 NOMETAT-IVA500           PIC X(6) VALUE 'IVA500'.                
            05 RUPTURES-IVA500.                                                 
           10 IVA500-TYPERR             PIC X(02).                      007  002
           10 IVA500-NCODIC             PIC X(07).                      009  007
           10 IVA500-DOPER              PIC X(08).                      016  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA500-SEQUENCE           PIC S9(04) COMP.                024  002
      *--                                                                       
           10 IVA500-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA500.                                                   
           10 IVA500-CLIEUTRTDEST       PIC X(05).                      026  005
           10 IVA500-CLIEUTRTORIG       PIC X(05).                      031  005
           10 IVA500-MESS1              PIC X(25).                      036  025
           10 IVA500-MESS2              PIC X(25).                      061  025
           10 IVA500-NLIEUDEST          PIC X(03).                      086  003
           10 IVA500-NLIEUORIG          PIC X(03).                      089  003
           10 IVA500-NSOCDEST           PIC X(03).                      092  003
           10 IVA500-NSOCORIG           PIC X(03).                      095  003
           10 IVA500-NSSLIEUDEST        PIC X(03).                      098  003
           10 IVA500-NSSLIEUORIG        PIC X(03).                      101  003
           10 IVA500-QMVT               PIC S9(05)      COMP-3.         104  003
            05 FILLER                      PIC X(406).                          
                                                                                
