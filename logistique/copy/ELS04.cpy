      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESF01   ESF01                                              00000020
      ***************************************************************** 00000030
       01   ELS04I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X.                                          00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X.                                     00000210
           02 MNSOCD OCCURS   75 TIMES .                                00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000230
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MNSOCI  PIC X(3).                                       00000260
           02 MSELD OCCURS   75 TIMES .                                 00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000280
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000290
             03 FILLER  PIC X(4).                                       00000300
             03 MSELI   PIC X(5).                                       00000310
           02 MNBD OCCURS   75 TIMES .                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBL    COMP PIC S9(4).                                 00000330
      *--                                                                       
             03 MNBL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MNBF    PIC X.                                          00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MNBI    PIC X(3).                                       00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MLIBERRI  PIC X(79).                                      00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MCODTRAI  PIC X(4).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MCICSI    PIC X(5).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MNETNAMI  PIC X(8).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MSCREENI  PIC X(4).                                       00000560
      ***************************************************************** 00000570
      * SDF: ESF01   ESF01                                              00000580
      ***************************************************************** 00000590
       01   ELS04O REDEFINES ELS04I.                                    00000600
           02 FILLER    PIC X(12).                                      00000610
           02 FILLER    PIC X(2).                                       00000620
           02 MDATJOUA  PIC X.                                          00000630
           02 MDATJOUC  PIC X.                                          00000640
           02 MDATJOUP  PIC X.                                          00000650
           02 MDATJOUH  PIC X.                                          00000660
           02 MDATJOUV  PIC X.                                          00000670
           02 MDATJOUO  PIC X(10).                                      00000680
           02 FILLER    PIC X(2).                                       00000690
           02 MTIMJOUA  PIC X.                                          00000700
           02 MTIMJOUC  PIC X.                                          00000710
           02 MTIMJOUP  PIC X.                                          00000720
           02 MTIMJOUH  PIC X.                                          00000730
           02 MTIMJOUV  PIC X.                                          00000740
           02 MTIMJOUO  PIC X(5).                                       00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MPAGEA    PIC X.                                          00000770
           02 MPAGEC    PIC X.                                          00000780
           02 MPAGEP    PIC X.                                          00000790
           02 MPAGEH    PIC X.                                          00000800
           02 MPAGEV    PIC X.                                          00000810
           02 MPAGEO    PIC X.                                          00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MPAGEMAXA      PIC X.                                     00000840
           02 MPAGEMAXC PIC X.                                          00000850
           02 MPAGEMAXP PIC X.                                          00000860
           02 MPAGEMAXH PIC X.                                          00000870
           02 MPAGEMAXV PIC X.                                          00000880
           02 MPAGEMAXO      PIC X.                                     00000890
           02 DFHMS1 OCCURS   75 TIMES .                                00000900
             03 FILLER       PIC X(2).                                  00000910
             03 MNSOCA  PIC X.                                          00000920
             03 MNSOCC  PIC X.                                          00000930
             03 MNSOCP  PIC X.                                          00000940
             03 MNSOCH  PIC X.                                          00000950
             03 MNSOCV  PIC X.                                          00000960
             03 MNSOCO  PIC X(3).                                       00000970
           02 DFHMS2 OCCURS   75 TIMES .                                00000980
             03 FILLER       PIC X(2).                                  00000990
             03 MSELA   PIC X.                                          00001000
             03 MSELC   PIC X.                                          00001010
             03 MSELP   PIC X.                                          00001020
             03 MSELH   PIC X.                                          00001030
             03 MSELV   PIC X.                                          00001040
             03 MSELO   PIC X(5).                                       00001050
           02 DFHMS3 OCCURS   75 TIMES .                                00001060
             03 FILLER       PIC X(2).                                  00001070
             03 MNBA    PIC X.                                          00001080
             03 MNBC    PIC X.                                          00001090
             03 MNBP    PIC X.                                          00001100
             03 MNBH    PIC X.                                          00001110
             03 MNBV    PIC X.                                          00001120
             03 MNBO    PIC X(3).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MLIBERRA  PIC X.                                          00001150
           02 MLIBERRC  PIC X.                                          00001160
           02 MLIBERRP  PIC X.                                          00001170
           02 MLIBERRH  PIC X.                                          00001180
           02 MLIBERRV  PIC X.                                          00001190
           02 MLIBERRO  PIC X(79).                                      00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MCODTRAA  PIC X.                                          00001220
           02 MCODTRAC  PIC X.                                          00001230
           02 MCODTRAP  PIC X.                                          00001240
           02 MCODTRAH  PIC X.                                          00001250
           02 MCODTRAV  PIC X.                                          00001260
           02 MCODTRAO  PIC X(4).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MCICSA    PIC X.                                          00001290
           02 MCICSC    PIC X.                                          00001300
           02 MCICSP    PIC X.                                          00001310
           02 MCICSH    PIC X.                                          00001320
           02 MCICSV    PIC X.                                          00001330
           02 MCICSO    PIC X(5).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNETNAMA  PIC X.                                          00001360
           02 MNETNAMC  PIC X.                                          00001370
           02 MNETNAMP  PIC X.                                          00001380
           02 MNETNAMH  PIC X.                                          00001390
           02 MNETNAMV  PIC X.                                          00001400
           02 MNETNAMO  PIC X(8).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MSCREENA  PIC X.                                          00001430
           02 MSCREENC  PIC X.                                          00001440
           02 MSCREENP  PIC X.                                          00001450
           02 MSCREENH  PIC X.                                          00001460
           02 MSCREENV  PIC X.                                          00001470
           02 MSCREENO  PIC X(4).                                       00001480
                                                                                
