      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVTL0102                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVTL0102                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVTL0102.                                                            
           02  TL01-DDELIV                                                      
               PIC X(0008).                                                     
           02  TL01-NSOC                                                        
               PIC X(0003).                                                     
           02  TL01-CPROTOUR                                                    
               PIC X(0005).                                                     
           02  TL01-CTOURNEE                                                    
               PIC X(0003).                                                     
           02  TL01-CLIVR1                                                      
               PIC X(0010).                                                     
           02  TL01-CLIVR2                                                      
               PIC X(0010).                                                     
           02  TL01-CLIVR3                                                      
               PIC X(0010).                                                     
           02  TL01-NSATELLITE                                                  
               PIC X(0002).                                                     
           02  TL01-NCASE                                                       
               PIC X(0003).                                                     
           02  TL01-CCOMMENT                                                    
               PIC X(0005).                                                     
           02  TL01-NFOLIO                                                      
               PIC X(0002).                                                     
           02  TL01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  TL01-CTOURNEELG                                                  
               PIC X(0003).                                                     
           02  TL01-CLOGI                                                       
               PIC X(0005).                                                     
           02  TL01-CTEQUI                                                      
               PIC X(0005).                                                     
           02  TL01-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  TL01-NLIEULIVR                                                   
               PIC X(0003).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVTL0101                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVTL0101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CPROTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CTOURNEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CLIVR1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CLIVR1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CLIVR2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CLIVR2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CLIVR3-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CLIVR3-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-NSATELLITE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-NSATELLITE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-NCASE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-NCASE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-NFOLIO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-NFOLIO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CTOURNEELG-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CTOURNEELG-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CLOGI-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CLOGI-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CTEQUI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CTEQUI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-NLIEULIVR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-NLIEULIVR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
