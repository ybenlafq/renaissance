      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00000010
       01  TS-GL1D-DONNEES.                                             00000070
           05 TS-GL1D-ECRAN.                                            00000090
              15 TS-GL1D-NFOUR         PIC X(05).                          00000
              15 TS-GL1D-CDE           PIC X(07).                          00000
              15 TS-GL1D-CODIC         PIC X(07).                          00000
              15 TS-GL1D-CQUOTA        PIC X(05).                          00000
              15 TS-GL1D-LREFF         PIC X(11).                          00000
              15 TS-GL1D-QTE           PIC 9(05).                          00000
              15 TS-GL1D-QNBUO         PIC 9(04).                          00000
              15 TS-GL1D-QNBPAL        PIC 9(04).                          00000
              15 TS-GL1D-ORIGDATE      PIC X(08).                               
              15 TS-GL1D-ORIGDATE-6    PIC X(08).                               
              15 TS-GL1D-CREASON       PIC X(05).                               
              15 TS-GL1D-TV-STAND      PIC X(01).                          00000
              15 TS-GL1D-DATA-CPT      PIC X(01).                          00000
           05 TS-GL1D-GESTION.                                          00000090
              15 TS-GL1D-COMPTEUR      PIC 9(01).                       00000090
           05 TS-GL1D-TABLE OCCURS 99.                                  00000090
              15 TS-GL1D-NSEQ          PIC X(02).                          00000
              15 TS-GL1D-NCDE          PIC X(07).                             00
              15 TS-GL1D-NCODIC        PIC X(07).                             00
              15 TS-GL1D-NLIVRAISON    PIC X(07).                             00
              15 TS-GL1D-DLIVRAISON    PIC X(08).                             00
              15 TS-GL1D-DSAISIE       PIC X(08).                             00
              15 TS-GL1D-QCDE          PIC 9(05).                          00000
              15 TS-GL1D-QUOLIVR       PIC 9(05).                          00000
              15 TS-GL1D-QUOPALETTIS   PIC 9(05).                          00000
              15 TS-GL1D-CMODSTOCK     PIC X(05).                               
      ****************************************************************  00000010
                                                                                
