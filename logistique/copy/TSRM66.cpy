      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM66                                          *  00020000
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-RM66-LONG         PIC S9(3) COMP-3 VALUE 87.              00060001
       01  TS-RM66-DONNEES.                                             00070001
              10 TS-RM66-MLAGREG PIC X(20).                             00080001
              10 TS-RM66-CGROUP  PIC X(5).                              00090001
              10 TS-RM66-WIMP    PIC X(1).                              00100001
              10 TS-RM66-WNAT    PIC X(1).                              00110001
              10 TS-RM66-QPVRM1  PIC X(6).                              00120001
              10 TS-RM66-QPVA1   PIC X(6).                              00130001
              10 TS-RM66-QPVN1   PIC X(6).                              00140001
              10 TS-RM66-MODIF1  PIC X(1).                              00150001
              10 TS-RM66-INSERT1 PIC X(1).                              00160001
              10 TS-RM66-QPVRM2  PIC X(6).                              00170001
              10 TS-RM66-QPVA2   PIC X(6).                              00180001
              10 TS-RM66-QPVN2   PIC X(6).                              00190001
              10 TS-RM66-MODIF2  PIC X(1).                              00200001
              10 TS-RM66-INSERT2 PIC X(1).                              00210001
              10 TS-RM66-QPVRM3  PIC X(6).                              00220001
              10 TS-RM66-QPVA3   PIC X(6).                              00230001
              10 TS-RM66-QPVN3   PIC X(6).                              00240001
              10 TS-RM66-MODIF3  PIC X(1).                              00250001
              10 TS-RM66-INSERT3 PIC X(1).                              00260001
                                                                                
