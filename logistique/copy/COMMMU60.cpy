      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TMU60                    TR: MU10  *    00020001
      *                         TMU61                              *    00050001
      *                         TMU62                              *    00050001
      *    SAISIE DES CONTREMARQUE POUR MUTATION MGC MGI           *    00060001
      *                                                            *    00070001
      * ZONES RESERVEES APPLICATIVES --------------------- 3519    *            
      *                                                            *    00080001
      **************************************************************            
      *         TRANSACTION MU60 : SAISIE CODIC CONTREMARQUE       *            
      *                            A MUTER                         *            
      **************************************************************            
      *                                                                 00790001
          02 COMM-MU60-APPLI    REDEFINES COMM-MU10-FILLER.             00800001
      *                                                                 00810001
              05 COMM-MU60-TRAIT             PIC X(01).                 00880001
              05 COMM-MU60-TRAIT-QTE         PIC X(01).                 00880001
              05 COMM-MU60-LDEPOT            PIC X(20).                 00870001
              05 COMM-MU60-NZONPRIX          PIC X(02).                 00870001
              05 COMM-MU60-TABLE.                                       00180001
                 07 COMM-MU60-LIGNE          OCCURS 12.                         
                    09 COMM-MU60-CFAM        PIC X(05).                         
                    09 COMM-MU60-CMARQ       PIC X(05).                         
                    09 COMM-MU60-REF         PIC X(20).                         
                    09 COMM-MU60-NCODIC      PIC 9(07).                         
                    09 COMM-MU60-QTE         PIC 9(04).                         
                    09 COMM-MU60-NDOC        PIC X(07).                         
                    09 COMM-MU60-DMUT        PIC X(06).                         
                    09 COMM-MU60-PVUN        PIC S9(06)V99 COMP-3.              
      *                                                                 00970000
             05 COMM-MU60-FILLER         PIC X(2751).                   01000001
      *                                                                 01010000
      *****************************************************************         
                                                                                
