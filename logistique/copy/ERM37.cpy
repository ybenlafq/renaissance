      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM37   ERM37                                              00000020
      ***************************************************************** 00000030
       01   ERM37I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICRL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNCODICRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICRF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNCODICRI      PIC X(7).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCGROUPI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGROUPL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLGROUPF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLGROUPI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCODICI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMARQI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLREFI    PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCEXPOI   PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTATCL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLSTATCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLSTATCF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLSTATCI  PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOLLECTL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MCOLLECTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOLLECTF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCOLLECTI      PIC X(2).                                  00000570
           02 MNSOCD OCCURS   39 TIMES .                                00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNSOCI  PIC X(3).                                       00000620
           02 MNMAGD OCCURS   39 TIMES .                                00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000640
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000650
             03 FILLER  PIC X(4).                                       00000660
             03 MNMAGI  PIC X(3).                                       00000670
           02 MWASSD OCCURS   39 TIMES .                                00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWASSL  COMP PIC S9(4).                                 00000690
      *--                                                                       
             03 MWASSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWASSF  PIC X.                                          00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MWASSI  PIC X.                                          00000720
           02 MWASSMD OCCURS   39 TIMES .                               00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWASSML      COMP PIC S9(4).                            00000740
      *--                                                                       
             03 MWASSML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWASSMF      PIC X.                                     00000750
             03 FILLER  PIC X(4).                                       00000760
             03 MWASSMI      PIC X.                                     00000770
           02 MQEXPOD OCCURS   39 TIMES .                               00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPOL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQEXPOF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQEXPOI      PIC X(3).                                  00000820
           02 MQLSD OCCURS   39 TIMES .                                 00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLSL   COMP PIC S9(4).                                 00000840
      *--                                                                       
             03 MQLSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQLSF   PIC X.                                          00000850
             03 FILLER  PIC X(4).                                       00000860
             03 MQLSI   PIC X(3).                                       00000870
           02 MQSTOCKD OCCURS   39 TIMES .                              00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKL     COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MQSTOCKL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQSTOCKF     PIC X.                                     00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MQSTOCKI     PIC X(5).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MZONCMDI  PIC X(12).                                      00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MLIBERRI  PIC X(61).                                      00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MCODTRAI  PIC X(4).                                       00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MCICSI    PIC X(5).                                       00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MNETNAMI  PIC X(8).                                       00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MSCREENI  PIC X(4).                                       00001160
      ***************************************************************** 00001170
      * SDF: ERM37   ERM37                                              00001180
      ***************************************************************** 00001190
       01   ERM37O REDEFINES ERM37I.                                    00001200
           02 FILLER    PIC X(12).                                      00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MDATJOUA  PIC X.                                          00001230
           02 MDATJOUC  PIC X.                                          00001240
           02 MDATJOUP  PIC X.                                          00001250
           02 MDATJOUH  PIC X.                                          00001260
           02 MDATJOUV  PIC X.                                          00001270
           02 MDATJOUO  PIC X(10).                                      00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MTIMJOUA  PIC X.                                          00001300
           02 MTIMJOUC  PIC X.                                          00001310
           02 MTIMJOUP  PIC X.                                          00001320
           02 MTIMJOUH  PIC X.                                          00001330
           02 MTIMJOUV  PIC X.                                          00001340
           02 MTIMJOUO  PIC X(5).                                       00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MNCODICRA      PIC X.                                     00001370
           02 MNCODICRC PIC X.                                          00001380
           02 MNCODICRP PIC X.                                          00001390
           02 MNCODICRH PIC X.                                          00001400
           02 MNCODICRV PIC X.                                          00001410
           02 MNCODICRO      PIC X(7).                                  00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MNPAGEA   PIC X.                                          00001440
           02 MNPAGEC   PIC X.                                          00001450
           02 MNPAGEP   PIC X.                                          00001460
           02 MNPAGEH   PIC X.                                          00001470
           02 MNPAGEV   PIC X.                                          00001480
           02 MNPAGEO   PIC X(3).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCGROUPA  PIC X.                                          00001510
           02 MCGROUPC  PIC X.                                          00001520
           02 MCGROUPP  PIC X.                                          00001530
           02 MCGROUPH  PIC X.                                          00001540
           02 MCGROUPV  PIC X.                                          00001550
           02 MCGROUPO  PIC X(5).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MLGROUPA  PIC X.                                          00001580
           02 MLGROUPC  PIC X.                                          00001590
           02 MLGROUPP  PIC X.                                          00001600
           02 MLGROUPH  PIC X.                                          00001610
           02 MLGROUPV  PIC X.                                          00001620
           02 MLGROUPO  PIC X(20).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNCODICA  PIC X.                                          00001650
           02 MNCODICC  PIC X.                                          00001660
           02 MNCODICP  PIC X.                                          00001670
           02 MNCODICH  PIC X.                                          00001680
           02 MNCODICV  PIC X.                                          00001690
           02 MNCODICO  PIC X(7).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MCFAMA    PIC X.                                          00001720
           02 MCFAMC    PIC X.                                          00001730
           02 MCFAMP    PIC X.                                          00001740
           02 MCFAMH    PIC X.                                          00001750
           02 MCFAMV    PIC X.                                          00001760
           02 MCFAMO    PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCMARQA   PIC X.                                          00001790
           02 MCMARQC   PIC X.                                          00001800
           02 MCMARQP   PIC X.                                          00001810
           02 MCMARQH   PIC X.                                          00001820
           02 MCMARQV   PIC X.                                          00001830
           02 MCMARQO   PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLREFA    PIC X.                                          00001860
           02 MLREFC    PIC X.                                          00001870
           02 MLREFP    PIC X.                                          00001880
           02 MLREFH    PIC X.                                          00001890
           02 MLREFV    PIC X.                                          00001900
           02 MLREFO    PIC X(20).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCEXPOA   PIC X.                                          00001930
           02 MCEXPOC   PIC X.                                          00001940
           02 MCEXPOP   PIC X.                                          00001950
           02 MCEXPOH   PIC X.                                          00001960
           02 MCEXPOV   PIC X.                                          00001970
           02 MCEXPOO   PIC X.                                          00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MLSTATCA  PIC X.                                          00002000
           02 MLSTATCC  PIC X.                                          00002010
           02 MLSTATCP  PIC X.                                          00002020
           02 MLSTATCH  PIC X.                                          00002030
           02 MLSTATCV  PIC X.                                          00002040
           02 MLSTATCO  PIC X(3).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MCOLLECTA      PIC X.                                     00002070
           02 MCOLLECTC PIC X.                                          00002080
           02 MCOLLECTP PIC X.                                          00002090
           02 MCOLLECTH PIC X.                                          00002100
           02 MCOLLECTV PIC X.                                          00002110
           02 MCOLLECTO      PIC X(2).                                  00002120
           02 DFHMS1 OCCURS   39 TIMES .                                00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MNSOCA  PIC X.                                          00002150
             03 MNSOCC  PIC X.                                          00002160
             03 MNSOCP  PIC X.                                          00002170
             03 MNSOCH  PIC X.                                          00002180
             03 MNSOCV  PIC X.                                          00002190
             03 MNSOCO  PIC X(3).                                       00002200
           02 DFHMS2 OCCURS   39 TIMES .                                00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MNMAGA  PIC X.                                          00002230
             03 MNMAGC  PIC X.                                          00002240
             03 MNMAGP  PIC X.                                          00002250
             03 MNMAGH  PIC X.                                          00002260
             03 MNMAGV  PIC X.                                          00002270
             03 MNMAGO  PIC X(3).                                       00002280
           02 DFHMS3 OCCURS   39 TIMES .                                00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MWASSA  PIC X.                                          00002310
             03 MWASSC  PIC X.                                          00002320
             03 MWASSP  PIC X.                                          00002330
             03 MWASSH  PIC X.                                          00002340
             03 MWASSV  PIC X.                                          00002350
             03 MWASSO  PIC X.                                          00002360
           02 DFHMS4 OCCURS   39 TIMES .                                00002370
             03 FILLER       PIC X(2).                                  00002380
             03 MWASSMA      PIC X.                                     00002390
             03 MWASSMC PIC X.                                          00002400
             03 MWASSMP PIC X.                                          00002410
             03 MWASSMH PIC X.                                          00002420
             03 MWASSMV PIC X.                                          00002430
             03 MWASSMO      PIC X.                                     00002440
           02 DFHMS5 OCCURS   39 TIMES .                                00002450
             03 FILLER       PIC X(2).                                  00002460
             03 MQEXPOA      PIC X.                                     00002470
             03 MQEXPOC PIC X.                                          00002480
             03 MQEXPOP PIC X.                                          00002490
             03 MQEXPOH PIC X.                                          00002500
             03 MQEXPOV PIC X.                                          00002510
             03 MQEXPOO      PIC X(3).                                  00002520
           02 DFHMS6 OCCURS   39 TIMES .                                00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MQLSA   PIC X.                                          00002550
             03 MQLSC   PIC X.                                          00002560
             03 MQLSP   PIC X.                                          00002570
             03 MQLSH   PIC X.                                          00002580
             03 MQLSV   PIC X.                                          00002590
             03 MQLSO   PIC X(3).                                       00002600
           02 DFHMS7 OCCURS   39 TIMES .                                00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MQSTOCKA     PIC X.                                     00002630
             03 MQSTOCKC     PIC X.                                     00002640
             03 MQSTOCKP     PIC X.                                     00002650
             03 MQSTOCKH     PIC X.                                     00002660
             03 MQSTOCKV     PIC X.                                     00002670
             03 MQSTOCKO     PIC X(5).                                  00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MZONCMDA  PIC X.                                          00002700
           02 MZONCMDC  PIC X.                                          00002710
           02 MZONCMDP  PIC X.                                          00002720
           02 MZONCMDH  PIC X.                                          00002730
           02 MZONCMDV  PIC X.                                          00002740
           02 MZONCMDO  PIC X(12).                                      00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MLIBERRA  PIC X.                                          00002770
           02 MLIBERRC  PIC X.                                          00002780
           02 MLIBERRP  PIC X.                                          00002790
           02 MLIBERRH  PIC X.                                          00002800
           02 MLIBERRV  PIC X.                                          00002810
           02 MLIBERRO  PIC X(61).                                      00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MCODTRAA  PIC X.                                          00002840
           02 MCODTRAC  PIC X.                                          00002850
           02 MCODTRAP  PIC X.                                          00002860
           02 MCODTRAH  PIC X.                                          00002870
           02 MCODTRAV  PIC X.                                          00002880
           02 MCODTRAO  PIC X(4).                                       00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MCICSA    PIC X.                                          00002910
           02 MCICSC    PIC X.                                          00002920
           02 MCICSP    PIC X.                                          00002930
           02 MCICSH    PIC X.                                          00002940
           02 MCICSV    PIC X.                                          00002950
           02 MCICSO    PIC X(5).                                       00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MNETNAMA  PIC X.                                          00002980
           02 MNETNAMC  PIC X.                                          00002990
           02 MNETNAMP  PIC X.                                          00003000
           02 MNETNAMH  PIC X.                                          00003010
           02 MNETNAMV  PIC X.                                          00003020
           02 MNETNAMO  PIC X(8).                                       00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MSCREENA  PIC X.                                          00003050
           02 MSCREENC  PIC X.                                          00003060
           02 MSCREENP  PIC X.                                          00003070
           02 MSCREENH  PIC X.                                          00003080
           02 MSCREENV  PIC X.                                          00003090
           02 MSCREENO  PIC X(4).                                       00003100
                                                                                
