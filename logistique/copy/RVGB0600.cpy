      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGB0600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB0600                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGB0600.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGB0600.                                                            
      *}                                                                        
           02  GB06-NSOCENTR                                                    
               PIC X(0003).                                                     
           02  GB06-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GB06-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GB06-NLIEU                                                       
               PIC X(0003).                                                     
           02  GB06-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GB06-DDEBSAIS                                                    
               PIC X(0008).                                                     
           02  GB06-DFINSAIS                                                    
               PIC X(0008).                                                     
           02  GB06-DDESTOCK                                                    
               PIC X(0008).                                                     
           02  GB06-DMUTATION                                                   
               PIC X(0008).                                                     
           02  GB06-CSELART                                                     
               PIC X(0005).                                                     
           02  GB06-DHEURMUT                                                    
               PIC X(0002).                                                     
           02  GB06-DMINUMUT                                                    
               PIC X(0002).                                                     
           02  GB06-QNBCAMIONS                                                  
               PIC S9(2) COMP-3.                                                
           02  GB06-QNBM3QUOTA                                                  
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GB06-QNBPQUOTA                                                   
               PIC S9(5) COMP-3.                                                
           02  GB06-QVOLUME                                                     
               PIC S9(11) COMP-3.                                               
           02  GB06-QNBPIECES                                                   
               PIC S9(5) COMP-3.                                                
           02  GB06-QNBLIGNES                                                   
               PIC S9(5) COMP-3.                                                
           02  GB06-QNBPLANCE                                                   
               PIC S9(5) COMP-3.                                                
           02  GB06-QNBLLANCE                                                   
               PIC S9(5) COMP-3.                                                
           02  GB06-WPROPERMIS                                                  
               PIC X(0001).                                                     
           02  GB06-WVAL                                                        
               PIC X(0001).                                                     
           02  GB06-LHEURLIMIT                                                  
               PIC X(0010).                                                     
           02  GB06-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GB06-DVALID                                                      
               PIC X(0008).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGB0600                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGB0600-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGB0600-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-NSOCENTR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-NSOCENTR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-DDEBSAIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-DDEBSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-DFINSAIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-DFINSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-DDESTOCK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-DDESTOCK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-DMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-DMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-CSELART-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-CSELART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-DHEURMUT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-DHEURMUT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-DMINUMUT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-DMINUMUT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-QNBCAMIONS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-QNBCAMIONS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-QNBM3QUOTA-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-QNBM3QUOTA-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-QNBPQUOTA-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-QNBPQUOTA-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-QVOLUME-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-QVOLUME-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-QNBPIECES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-QNBPIECES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-QNBLIGNES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-QNBLIGNES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-QNBPLANCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-QNBPLANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-QNBLLANCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-QNBLLANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-WPROPERMIS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-WPROPERMIS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-WVAL-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-WVAL-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-LHEURLIMIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-LHEURLIMIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB06-DVALID-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB06-DVALID-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
