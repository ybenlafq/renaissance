      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE CD2000                       
      ******************************************************************        
      *                                                                         
       CLEF-CD2000             SECTION.                                         
      *                                                                         
           MOVE 'RVCD2000          '       TO   TABLE-NAME.                     
           MOVE 'CD2000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-CD2000. EXIT.                                                   
                EJECT                                                           
                                                                                
