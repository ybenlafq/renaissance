      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM1300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM1300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1300.                                                            
           02  RM13-CGROUP       PIC X(005).                                    
           02  RM13-CFAM         PIC X(005).                                    
           02  RM13-LAGREGATED   PIC X(020).                                    
           02  RM13-CEXPO        PIC X(005).                                    
           02  RM13-CTYPREG      PIC X(002).                                    
           02  RM13-DEFFET       PIC X(008).                                    
           02  RM13-QEXPO        PIC S9(05) COMP-3.                             
           02  RM13-QLS          PIC S9(05) COMP-3.                             
           02  RM13-CACID        PIC X(010).                                    
           02  RM13-DSYST        PIC S9(13) COMP-3.                             
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM1300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM13-CGROUP-F     PIC S9(4) COMP.                                
      *--                                                                       
           02  RM13-CGROUP-F     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM13-CFAM-F       PIC S9(4) COMP.                                
      *--                                                                       
           02  RM13-CFAM-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM13-LAGREGATED-F PIC S9(4) COMP.                                
      *--                                                                       
           02  RM13-LAGREGATED-F PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM13-CEXPO-F      PIC S9(4) COMP.                                
      *--                                                                       
           02  RM13-CEXPO-F      PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM13-CTYPREG-F    PIC S9(4) COMP.                                
      *--                                                                       
           02  RM13-CTYPREG-F    PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM13-DEFFET-F     PIC S9(4) COMP.                                
      *--                                                                       
           02  RM13-DEFFET-F     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM13-QEXPO-F      PIC S9(4) COMP.                                
      *--                                                                       
           02  RM13-QEXPO-F      PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM13-QLS-F        PIC S9(4) COMP.                                
      *--                                                                       
           02  RM13-QLS-F        PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM13-CACID-F      PIC S9(4) COMP.                                
      *--                                                                       
           02  RM13-CACID-F      PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM13-DSYST-F      PIC S9(4) COMP.                                
      *--                                                                       
           02  RM13-DSYST-F      PIC S9(4) COMP-5.                              
      *}                                                                        
      *                                                                         
                                                                                
