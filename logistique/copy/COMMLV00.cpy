      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TLV00                    TR: LV00  *    00002209
      *                 DESCENTE DES INFORMATIONS VERS LM6         *    00002309
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *            TRANSACTION LV00 : DESCENTE DES INFOS VERS LM6     * 00411009
      *                                                                 00412000
      **************************************************************            
      * COMMAREA SPECIFIQUE PRG TLI13                              *            
      *                                                            *            
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-LV00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-LV00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
      *-------------------------------------------------------------            
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA      PIC X(100).                                   
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 027          
          02 COMM-CICS-APPLID     PIC X(8).                                     
          02 COMM-CICS-NETNAM     PIC X(8).                                     
          02 COMM-CICS-TRANSA     PIC X(4).                                     
          02 COMM-CICS-TSSACIDA   PIC X(7).                                     
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE     PIC XX.                                       
          02 COMM-DATE-ANNEE      PIC XX.                                       
          02 COMM-DATE-MOIS       PIC XX.                                       
          02 COMM-DATE-JOUR       PIC XX.                                       
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC 999.                                      
          02 COMM-DATE-QNT0       PIC 99999.                                    
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC 9.                                        
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC 9.                                        
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC XXX.                                      
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                 
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC XXX.                                      
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                 
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                                     
          02 COMM-DATE-AAMMJJ     PIC X(6).                                     
          02 COMM-DATE-JJMMSSAA   PIC X(8).                                     
          02 COMM-DATE-JJMMAA     PIC X(6).                                     
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                                     
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                    
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
          02 COMM-DATE-WEEK.                                                    
             05 COMM-DATE-SEMSS PIC 99.                                         
             05 COMM-DATE-SEMAA PIC 99.                                         
             05 COMM-DATE-SEMNU PIC 99.                                         
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-FILLER     PIC X(08).                                    
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                                
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3700          
      *                                                                         
      *            TRANSACTION LV00 : DESCENTE DES INFOS VERS LM6     *         
      *                                                                         
      *****************************************************************         
          02  COMM-LV00-MENU.                                                   
      *=> ZONES DE SAUVEGARDE COMMUNES AU MENU PRINCIPAL                        
      *=> ET AUX TRANSACTIONS DE NIVEAU INFERIEUR                               
      * ZONE COMM-LV00 ------------------------------------------- 200  00738020
              03  COMM-LV00.                                                    
                  05  COMM-LV00-MZONCMD          PIC X(02).                     
                  05  COMM-LV00-NSOCDEP          PIC X(03).                     
                  05  COMM-LV00-NDEPOT           PIC X(03).                     
                  05  COMM-LV00-NCDE             PIC X(07).                     
                  05  COMM-LV00-NSITE            PIC 9(03).                     
                  05  COMM-LV00-LETSITE          PIC X(01).                     
                  05  COMM-LV00-DEPOT-AUTOR.                                    
                      10 COMM-LV00-SOCDEP        PIC X(03) OCCURS 10.           
                      10 COMM-LV00-DEPOT         PIC X(03) OCCURS 10.           
                      10 COMM-LV00-SITE          PIC 9(03) OCCURS 10.           
                      10 COMM-LV00-LTSITE        PIC X(01) OCCURS 10.           
                  05  COMM-LV00-NBDEPOT          PIC S9(3) COMP-3.              
                  05  COMM-LV00-LLIEU            PIC X(20).                     
                  05  FILLER                     PIC X(59).                     
      *=> DE NIVEAU INFERIEUR                                                   
          02  COMM-LV00-APPLI          PIC X(3500).                     00738030
      *------------------------------ ZONES UTILISEES PAR LE TLI13      00738020
          02  COMM-LV01-APPLI REDEFINES COMM-LV00-APPLI.                00738030
              03 COMM-LV01-LIBRE            PIC X(613).                 00738030
              03 COMM-LI13.                                             00738030
                 05 COMM-LI13-NSOC          PIC X(03).                  00738030
                 05 COMM-LI13-NLIEU         PIC X(03).                  00738030
                 05 COMM-LI13-WTYPEADR      PIC X(01).                  00738030
                 05 COMM-LI13-CTYPLIEU      PIC X(01).                  00738030
                 05 COMM-LI13-LLIEU         PIC X(50).                  00738030
                 05 COMM-LI13-LADR1         PIC X(50).                  00738030
                 05 COMM-LI13-LADR2         PIC X(50).                  00738030
                 05 COMM-LI13-LADR3         PIC X(50).                  00738030
                 05 COMM-LI13-LCOMMUNE      PIC X(50).                  00738030
                 05 COMM-LI13-CPOSTAL       PIC X(05).                  00738030
                 05 COMM-LI13-LBUREAU       PIC X(50).                  00738030
                 05 COMM-LI13-CDEPT         PIC X(02).                  00738030
                 05 COMM-LI13-TEL           PIC X(15).                  00738030
                 05 COMM-LI13-FAX           PIC X(15).                  00738030
                 05 COMM-LI13-QDELAIAPPRO   PIC X(03).                  00738030
                 05 COMM-LI13-CINSEE        PIC X(05).                  00738030
                 05 COMM-LI13-CPAYS         PIC X(03).                  00738030
                 05 COMM-LI13-CLANGUE       PIC X(03).                  00738030
                 05 COMM-LI13-TYPCLIEN      PIC X(06).                  00738030
                 05 COMM-LI13-CREGROUPABLE  PIC X(01).                  00738030
                 05 COMM-LI13-CCOLISAGE     PIC 9(03).                  00738030
                 05 COMM-LI13-CUNIVERS      PIC X(05).                  00738030
                 05 COMM-LI13-CTRANSPORTEUR PIC X(13).                  00738030
                 05 COMM-LI13-CFACONNAGE    PIC X(02).                  00738030
      *                                                                 00738030
              03 COMM-LV01-LIBRE            PIC X(2498).                00738030
      *                                                                 00738030
      *------------------------------ ZONES UTILISEES PAR LE TLV02      00738020
          02  COMM-LV02-APPLI REDEFINES COMM-LV00-APPLI.                00738030
              03 COMM-LV02-MODIF-DONNEE       PIC X.                            
              03 COMM-LV02-PAGE               PIC 9(3).                         
              03 COMM-LV02-NBMODIF-TS         PIC 9(3).                         
              03 COMM-LV02-ITEMAX             PIC 9(3).                         
              03 COMM-LV02-NB-ITEM            PIC 9(3).                         
              03 COMM-LV02-PAGE-MAX           PIC 9(3).                         
              03 COMM-LV02-NMUT               PIC X(7).                         
              03 COMM-LV02-NSOC               PIC X(3).                         
              03 COMM-LV02-NLIEU              PIC X(3).                         
              03 COMM-LV02-CSELART            PIC X(5).                         
              03 COMM-LV02-QNBPIECES          PIC X(5).                         
              03 COMM-LV02-QNBVOLUME          PIC X(6).                         
              03 COMM-LV02-DDESTOCK           PIC X(10).                        
              03 COMM-LV02-HCHARGT            PIC X(5).                         
              03 COMM-LV02-TYPOP              PIC X(2).                         
              03 COMM-LV02-CTRANSPORTEUR      PIC X(13).                        
              03 COMM-LV02-SIMU-REEL          PIC X(4).                         
              03 COMM-LV02-PF5                PIC X.                            
                 88 COMM-LV02-PF5-1X    VALUE '1'.                              
                 88 COMM-LV02-PF5-2X    VALUE '2'.                              
      *                                                                         
      *------------------------------ ZONES UTILISEES PAR LE TLV31      00738020
      *----  TLV00 - OPTION 3 (TLV00) - OPTION 1 ( TLV31 )                      
             02  COMM-LV31-APPLI REDEFINES COMM-LV00-APPLI.                     
                 03 COMM-LV31-PAGE            PIC 9(04).                        
                 03 COMM-LV31-NBPAGES         PIC 9(04).                        
                 03 COMM-LV31-TSLONG          PIC 9(01).                        
                    88 TS31-PAS-TROP-LONGUE       VALUE 0.                      
                    88 TS31-TROP-LONGUE           VALUE 1.                      
                 03 COMM-LV31-ECART-JOURS     PIC 9(03).                        
                 03 COMM-LV31-SDDEBUT         PIC X(08).                        
                 03 COMM-LV31-SDFIN           PIC X(08).                        
                 03 COMM-LV31-SNSOC           PIC X(03).                        
                 03 COMM-LV31-SNLIEU          PIC X(03).                        
                 03 COMM-LV31-SSELART         PIC X(05).                        
                 03 COMM-LV31-SNMUTATION      PIC X(07).                        
                 03 COMM-LV31-SWMULTI         PIC X(01).                        
                 03 COMM-LV31-SDDSTOCK        PIC X(04).                        
                 03 COMM-LV31-SDCHARG         PIC X(04).                        
                 03 COMM-LV31-SHCHARG         PIC X(05).                        
                 03 COMM-LV31-SDMUTATION      PIC X(04).                        
                 03 COMM-LV31-SDLM6           PIC X(04).                        
                 03 COMM-LV31-SDEXPED         PIC X(04).                        
                 03 COMM-LV31-SDVALID         PIC X(04).                        
      ***************************************************************** 00740000
      *                                                                         
      *----                            ZONES UTILISEES PAR LE TLV04             
      *----  TLV00 - OPTION 4 (TLV04)                                           
             02  COMM-LV04-APPLI REDEFINES COMM-LV00-APPLI.                     
                 03 COMM-LV04-PAGE            PIC 9(04).                        
                 03 COMM-LV04-NBPAGES         PIC 9(04).                        
                 03 COMM-LV04-TSLONG          PIC 9(01).                        
                    88 TS04-PAS-TROP-LONGUE   VALUE 0.                          
                    88 TS04-TROP-LONGUE       VALUE 1.                          
                 03 COMM-LV04-DD-SELECTION    PIC X(08).                        
                 03 COMM-LV04-SDJRECQUAI      PIC X(04).                        
AA1214           03 COMM-LV04-SNRECQORIG      PIC X(07).                        
                 03 COMM-LV04-SNRECQUAI       PIC X(07).                        
                 03 COMM-LV04-SNDOSLM6        PIC X(08).                        
                 03 COMM-LV04-SDSAISREC       PIC X(04).                        
                 03 COMM-LV04-SNREC           PIC X(07).                        
                 03 COMM-LV04-SNCDE           PIC X(07).                        
                 03 COMM-LV04-SNCODIC         PIC X(07).                        
                 03 COMM-LV04-SNBL            PIC X(10).                        
                 03 COMM-LV04-SETAT           PIC X(02).                        
                 03 COMM-LV04-SNENTCDE        PIC X(05).                        
                 03 COMM-LV04-SLENTCDE        PIC X(20).                        
      *                                                                         
      ***************************************************************** 00740000
      *                                                                         
      *----                            ZONES UTILISEES PAR LE TLV05             
      *----  TLV00 - OPTION 5 (TLV05)                                           
             02  COMM-LV05-APPLI REDEFINES COMM-LV00-APPLI.                     
                 03 COMM-LV05-PAGE            PIC 9(04).                        
                 03 COMM-LV05-NBPAGES         PIC 9(04).                        
                 03 COMM-LV05-TSLONG          PIC 9(01).                        
                    88 TS05-PAS-TROP-LONGUE   VALUE 0.                          
                    88 TS05-TROP-LONGUE       VALUE 1.                          
                 03 COMM-LV05-DD-SELECTION    PIC X(08).                        
                 03 COMM-LV05-SNOP            PIC X(09).                        
                 03 COMM-LV05-SNMUTATION      PIC X(07).                        
                 03 COMM-LV05-SNLIEUDEST      PIC X(06).                        
                 03 COMM-LV05-SDDESTOCK       PIC X(04).                        
                 03 COMM-LV05-SCSELART        PIC X(05).                        
                 03 COMM-LV05-SHCHARGT        PIC X(05).                        
      *                                                                         
                                                                                
