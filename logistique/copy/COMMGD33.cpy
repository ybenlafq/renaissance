      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      *                                                                 00050000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GD31-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00240001
      *--                                                                       
       01  COM-GD31-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00730000
           02 COMM-GD31-MENU.                                           00740000
              03 COMM-GD31-MSOCIETE       PIC X(3).                     00741002
              03 COMM-GD31-MDEPOT         PIC X(3).                     00741002
              03 COMM-GD31-MZONCMD        PIC X(1).                     00742002
              03 COMM-GD31-MWFONC         PIC X(3).                     00742002
              03 COMM-GD31-MFAMILLE       PIC X(5).                     00742002
              03 COMM-GD31-MMARQUE        PIC X(5).                             
              03 COMM-GD31-MCODIC         PIC X(7).                             
              03 COMM-GD31-FILLER         PIC X(10).                    00970003
              03 COMM-GD31-TYPUSER        PIC X(8).                     00970003
              03 COMM-GD31-TAB-SOC.                                             
                 04 COMM-GD31-AUTOR-DEPOT      OCCURS 10.                       
                    05 COMM-GD31-AUTOR-NSOC    PIC X(3).                        
                    05 COMM-GD31-AUTOR-NDEPOT  PIC X(3).                        
              03 COMM-GD31-APPLI          PIC X(2619).                  00970003
           03  COMM-GD32-APPLI  REDEFINES  COMM-GD31-APPLI.             00010000
               04  COMM-GD32-ITEM      PIC 9(3).                        00020000
               04  COMM-GD32-POS-MAX   PIC 9(3).                        00030000
               04  COMM-GD32-DEF1      PIC X(5).                        00040000
               04  COMM-GD32-DEF2      PIC X(5).                        00050000
               04  COMM-GD32-DEF3      PIC X(5).                        00060000
               04  COMM-GD32-DEF4      PIC X(5).                        00070000
               04  COMM-GD32-DEF5      PIC X(5).                        00080000
           03  COMM-GD33-APPLI  REDEFINES  COMM-GD31-APPLI.                     
               04  COMM-GD33-AFFICH                 PIC X(1).                   
               04  TABLE92.                                                     
                   05  LIGNE-TAB  OCCURS 12.                                    
                      10  TAB-FAMILLE               PIC X(5).                   
                      10  TAB-MARQUE                PIC X(5).                   
                      10  TAB-CODIC                 PIC X(7).                   
                      10  TAB-REFERENCE             PIC X(20).                  
                      10  TAB-STOCK1                PIC X(5).                   
                      10  TAB-STOCK2                PIC X(5).                   
                      10  TAB-STOCK3                PIC X(5).                   
                      10  TAB-STOCK4                PIC X(5).                   
                      10  TAB-STOCK5                PIC X(5).                   
                      10  TAB-RENSEIG               PIC X(1).                   
                      10  TAB-EXISTE                PIC X(1).                   
                                                                                
