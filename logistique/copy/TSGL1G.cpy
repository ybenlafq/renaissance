      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00000010
       01  TS-GL1G-DONNEES.                                             00000070
           05 TS-GL1G-ECRAN.                                            00000090
              15 TS-GL1G-NFOUR         PIC X(05).                          00000
              15 TS-GL1G-CDE           PIC X(07).                          00000
              15 TS-GL1G-CODIC         PIC X(07).                          00000
              15 TS-GL1G-CQUOTA        PIC X(05).                          00000
              15 TS-GL1G-LREFF         PIC X(11).                          00000
              15 TS-GL1G-QTE           PIC 9(05).                               
              15 TS-GL1G-QNBUO         PIC 9(04).                               
              15 TS-GL1G-QNBPAL        PIC 9(04).                               
              15 TS-GL1G-ORIGDATE      PIC X(08).                               
              15 TS-GL1G-ORIGDATE-6    PIC X(08).                               
              15 TS-GL1G-CREASON       PIC X(05).                               
              15 TS-GL1G-TV-STAND      PIC X(01).                               
              15 TS-GL1G-DATA-CPT      PIC X(01).                               
           05 TS-GL1G-GESTION.                                          00000090
              15 TS-GL1G-WSEL          PIC X(01).                               
           05 TS-GL1G-TABLE.                                            00000090
              15 TS-GL1G-NSEQ          PIC X(02).                          00000
              15 TS-GL1G-NCDE          PIC X(07).                             00
              15 TS-GL1G-NCODIC        PIC X(07).                             00
              15 TS-GL1G-NLIVRAISON    PIC X(07).                             00
              15 TS-GL1G-DLIVRAISON    PIC X(08).                             00
              15 TS-GL1G-DSAISIE       PIC X(08).                             00
              15 TS-GL1G-QCDE          PIC 9(05).                          00000
              15 TS-GL1G-QUOLIVR       PIC 9(05).                          00000
              15 TS-GL1G-QUOPALETTIS   PIC 9(05).                          00000
              15 TS-GL1G-CMODSTOCK     PIC X(05).                               
      ****************************************************************  00000010
                                                                                
