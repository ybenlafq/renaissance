      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TMU62                    TR: MU10  *    00020001
      *                                                            *    00050001
      *                                                            *    00050001
      *    SAISIE DES CONTREMARQUE POUR MUTATION MGC MGI           *    00060001
      *                                                            *    00070001
      * ZONES RESERVEES APPLICATIVES --------------------- 3519    *            
      *                                                            *    00080001
      **************************************************************            
      *   TRANSACTION MU62 : CONSULTATION DES RESERVATIONS         *            
      *                      CODIC CONTRE-MARQUE                   *            
      **************************************************************            
      *                                                                 00790001
          02 COMM-MU62-APPLI    REDEFINES COMM-MU10-FILLER.             00800001
      *                                                                 00810001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-MU62-NB-CODIC          PIC S9(04) COMP.           00880001
      *--                                                                       
              05 COMM-MU62-NB-CODIC          PIC S9(04) COMP-5.                 
      *}                                                                        
              05 COMM-MU62-NB-PAGE           PIC 9(03).                 00880001
              05 COMM-MU62-NO-PAGE           PIC 9(03).                 00880001
              05 COMM-MU62-CODIC             PIC 9(07).                 00880001
              05 COMM-MU62-LDEPOT            PIC X(20).                 00870001
              05 COMM-MU62-TRAIT             PIC X(01).                 00870001
              05 COMM-MU62-TABLE.                                       00180001
                 07 COMM-MU62-LIGNE          OCCURS 13.                         
                    09 COMM-MU62-CFAM        PIC X(05).                         
                    09 COMM-MU62-CMARQ       PIC X(05).                         
                    09 COMM-MU62-REF         PIC X(20).                         
                    09 COMM-MU62-NCODIC      PIC X(07).                         
                    09 COMM-MU62-NDOC        PIC X(07).                         
                    09 COMM-MU62-NMUT        PIC X(07).                         
                    09 COMM-MU62-DMUT        PIC X(08).                         
                    09 COMM-MU62-QTE         PIC 9(04).                         
                    09 COMM-MU62-STAT        PIC X(01).                         
              05 COMM-MU62-T-RNG.                                       00180001
                 07 COMM-MU62-TRNG           OCCURS 200.                        
                    09 COMM-MU62-WCODIC      PIC  X(7).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             09 COMM-MU62-RNG         PIC S9(4) COMP.                    
      *--                                                                       
                    09 COMM-MU62-RNG         PIC S9(4) COMP-5.                  
      *}                                                                        
      *                                                                 00970000
             05 COMM-MU62-FILLER         PIC X(0448).                   01000001
      *                                                                 01010000
      *****************************************************************         
                                                                                
