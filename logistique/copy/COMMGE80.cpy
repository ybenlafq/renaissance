      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *  TRANSACTION GE00 : GESTION DES EMPLACEMENTS               *            
      *                                                                         
      * COMMAREA SPECIFIQUE PRG TGEXX (TGE00 -> MENU)    TR: GE00  *            
      *          GESTION DES EMPLACEMENTS : EDITION ETIQUETTES     *            
      *                   UTILISEES DANS MGE80                     *            
      *                                                            *            
      *                                                                         
        01 COMM-GE80-APPLI.                                                     
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-GE80-LONG           PIC S9(04) COMP VALUE +1900.             
      *--                                                                       
           02 COMM-GE80-LONG           PIC S9(04) COMP-5 VALUE +1900.           
      *}                                                                        
      *------------------------------ INDICE ETIQUETTE PAGE                     
           02 COMM-GE80-DATA.                                                   
      *------------------------------ INDICE ETIQUETTE PAGE                     
              03 COMM-GE80-IND            PIC 9(5).                             
      *------------------------------ NOMBRE D ETIQUETTE                        
              03 COMM-GE80-ETIQ           PIC S9(5) COMP-3.                     
      *------------------------------ COMPTEUR ETIQUETTE / DLI                  
              03 COMM-GE80-ETI            PIC 9(2).                             
      *------------------------------ INDICATEUR DEBUT ETAT                     
              03 COMM-GE80-DEB            PIC 9(1).                             
      *------------------------------ INDICATEUR FIN ETAT                       
              03 COMM-GE80-FIN            PIC 9(1).                             
      *------------------------------ CLE DOCUMENT                              
              03 COMM-GE80-DOC            PIC 9(15).                            
      *------------------------------ CLE DOCUMENT                              
              03 COMM-GE80-DST.                                                 
      *------------------------------ CODE SOCIETE                              
                 04 COMM-GE80-NSOCIETE       PIC X(03).                         
      *------------------------------ CODE ENTREPOT                             
                 04 COMM-GE80-NDEPOT         PIC X(03).                         
      *------------------------------ FILLER                                    
                 04 COMM-GE80-DSTLIB         PIC X(03).                         
      *------------------------------ DATE JOUR AAMMJJ                          
              03 COMM-GE80-DATJ           PIC X(06).                            
      *------------------------------ DATE JOUR AAMMJJ                          
              03 COMM-GE80-NRECQUAI       PIC X(07).                            
      *------------------------------ CODE ETAT ETIQUETTE DROITE                
      *                               (SOLVRA) (RACK  )                         
              03 COMM-GE80-TETAT1         PIC X(6).                             
      *------------------------------ TABLE ETIQUETTES                          
              03  COMM-GE80-TABLE-ETI OCCURS 2.                                 
                  04  COMM-GE80-ETIG.                                           
                      05  COMM-GE80-LIG1-ETIG.                                  
                          06  COMM-GE80-LIG11-ETIG   PIC X(01).                 
                          06  COMM-GE80-LIG15        PIC X(01).                 
                          06  COMM-GE80-LIG12-ETIG   PIC X(02).                 
                          06  COMM-GE80-LIG16        PIC X(01).                 
                          06  COMM-GE80-LIG13-ETIG   PIC X(02).                 
                          06  COMM-GE80-LIG17        PIC X(01).                 
                          06  COMM-GE80-LIG14-ETIG   PIC X(03).                 
                      05  COMM-GE80-LIG2-ETIG.                                  
                          06  COMM-GE80-LIG21-ETIG   PIC X(07).                 
                          06  COMM-GE80-LIG22-ETIG   PIC X(05).                 
                      05  COMM-GE80-LIG3-ETIG.                                  
                          06  COMM-GE80-LIG31-ETIG   PIC X(07).                 
                          06  COMM-GE80-LIG32-ETIG   PIC X(04).                 
                          06  COMM-GE80-LIG33-ETIG   PIC Z(05).                 
                      05  COMM-GE80-LIG4-ETIG.                                  
                          06  COMM-GE80-LIG41-ETIG   PIC X(05).                 
                          06  COMM-GE80-LIG44        PIC X(01).                 
                          06  COMM-GE80-LIG42-ETIG   PIC X(05).                 
                          06  COMM-GE80-LIG45        PIC X(01).                 
                          06  COMM-GE80-LIG43-ETIG   PIC X(20).                 
                      05  COMM-GE80-LIG5-ETIG.                                  
                          06  COMM-GE80-LIG51-ETIG   PIC X(25).                 
                          06  COMM-GE80-LIG52-ETIG   PIC X(25).                 
                          06  COMM-GE80-LIG53-ETIG.                             
                              07  COMM-GE80-LIG531-ETIG PIC X(08).              
                              07  COMM-GE80-LIG534      PIC X(01).              
                              07  COMM-GE80-LIG532-ETIG PIC X(07).              
                              07  COMM-GE80-LIG535      PIC X(01).              
                              07  COMM-GE80-LIG533-ETIG PIC X(05).              
                   04  COMM-GE80-ETID.                                          
                       05  COMM-GE80-LIG1-ETID.                                 
                           06  COMM-GE80-LIG11-ETID   PIC X(01).                
                           06  COMM-GE80-LIG12-ETID   PIC X(02).                
                       05  COMM-GE80-LIG2-ETID.                                 
                           06  COMM-GE80-LIG21-ETID   PIC X(01).                
                           06  COMM-GE80-LIG22-ETID   PIC X(01).                
                           06  COMM-GE80-LIG23-ETID   PIC X(01).                
                       05  COMM-GE80-LIG2B-ETID REDEFINES                       
                                             COMM-GE80-LIG2-ETID.               
                           06  COMM-GE80-LIG22B-ETID  PIC X(01).                
                           06  COMM-GE80-LIG21B-ETID  PIC X(01).                
                           06  COMM-GE80-LIG23B-ETID  PIC X(01).                
                       05  COMM-GE80-LIG3-ETID.                                 
                           06  COMM-GE80-LIG31-ETID   PIC X(01).                
                           06  COMM-GE80-LIG32-ETID   PIC X(02).                
                       05  COMM-GE80-LIG3B-ETID REDEFINES                       
                                            COMM-GE80-LIG3-ETID.                
                           06  COMM-GE80-LIG32B-ETID  PIC X(01).                
                           06  COMM-GE80-LIG31B-ETID  PIC X(01).                
                           06  COMM-GE80-LIG33B-ETID  PIC X(01).                
                       05  COMM-GE80-LIG4-ETID.                                 
                           06  COMM-GE80-LIG41-ETID   PIC X(01).                
                           06  COMM-GE80-LIG42-ETID   PIC X(02).                
                       05 COMM-GE80-LIG5-ETID.                                  
                           06 COMM-GE80-LIG51-ETID    PIC X(01).                
                           06 COMM-GE80-LIG52-ETID    PIC X(02).                
                       05 COMM-GE80-LIG5B-ETID REDEFINES                        
                                                COMM-GE80-LIG5-ETID.            
                           06 COMM-GE80-LIG52B-ETID   PIC X(01).                
                           06 COMM-GE80-LIG51B-ETID   PIC X(01).                
                           06 COMM-GE80-LIG53B-ETID   PIC X(01).                
      *------------------------------ ZONE DONNEES TGE80                        
             03 COMM-GE80-DGENE.                                                
      *------------------------------ ZONE DONNEES GENERALES TGE80              
                04 COMM-GE80-DGENE1.                                            
      *------------------------------ CODE PROGRAMME                            
                   05 COMM-GE80-PRG               PIC X(5).                     
      *------------------------------ CODE ETAT                                 
      *                               (SOLVRA) (RACK  )                         
                   05 COMM-GE80-TETAT             PIC X(6).                     
      *------------------------------ CODE RETOUR                               
                   05 COMM-GE80-CODRET            PIC X(1).                     
      *------------------------------ MESSAGE RETOUR                            
                   05 COMM-GE80-MESSAGE           PIC X(60).                    
      *------------------------------ CODE ARTICLE                              
                   05 COMM-GE80-NCODIC            PIC X(7).                     
      *------------------------------ QUANTITE STOCK                            
                   05 COMM-GE80-QSTOCK            PIC 9(5).                     
      *------------------------------ CONDITIONNEMENT UNITAIRE                  
                   05 COMM-GE80-QCONDT            PIC 9(5).                     
      *------------------------------ CODE FAMILLE                              
                   05 COMM-GE80-CFAM              PIC X(5).                     
      *------------------------------ CODE MARQUE                               
                   05 COMM-GE80-CMARQ             PIC X(5).                     
      *------------------------------ LIBELLE REFERANCE FOURNISSEUR             
                   05 COMM-GE80-LREFFOURN         PIC X(20).                    
      *------------------------------ LIBELLE EMBALLAGE                         
                   05 COMM-GE80-LEMBALLAGE        PIC X(50).                    
      *------------------------------ TYPE EMPLACEMENT ORIGINE                  
                   05 COMM-GE80-TEMPORIG          PIC X(01).                    
      *------------------------------ CODE EMPLACEMENT ORIGINE                  
                   05 COMM-GE80-CEMPORIG.                                       
      *------------------------------                  ORIGINE 1                
      *                             AIRE (SOL) ALLEE (RACK) ZONE (VRAC)         
                      06 COMM-GE80-EMPORIG1          PIC X(02).                 
      *------------------------------                  ORIGINE 2                
      *                         COTE (SOL) NIVEAU (RACK) SECTEUR (VRAC)         
                      06 COMM-GE80-EMPORIG2          PIC X(02).                 
      *------------------------------                  ORIGINE 3                
      *             NPOSITION (SOL) POSITION (RACK) SOUS-SECTEUR (VRAC)         
                      06 COMM-GE80-EMPORIG3          PIC X(03).                 
      *------------------------------ DATE D'ATTRIBUTION  (SSAAMMJJ)            
                   05 COMM-GE80-DATTR.                                          
                      06 COMM-GE80-DATTRSS           PIC X(02).                 
                      06 COMM-GE80-DATTRAA           PIC X(02).                 
                      06 COMM-GE80-DATTRMM           PIC X(02).                 
                      06 COMM-GE80-DATTRJJ           PIC X(02).                 
      *------------------------------ MODE STOCKAGE ( S / R / V )               
                   05 COMM-GE80-CMODSTOCK         PIC X(01).                    
      *------------------------------ COTE DE PREHENSION ( SOL / VRAC)          
      *                                                ( P = PROFONDEUR)        
      *                                                ( L = LARGEUR   )        
      *                                                ( H = HAUTEUR   )        
                   05 COMM-GE80-CCOTEHOLD         PIC X(01).                    
      *------------------------------ FLAG RUPTURE DE STOCK                     
                   05 COMM-GE80-RUPT-STOCK        PIC X(01).                    
      *------------------------------ ZONE DONNEES TGE80 SPECIFIQUES            
                04 COMM-GE80-DSPECIF.                                           
      *------------------------------ ZONE DONNEES SPECIFIQUES SOL              
                   05 COMM-GE80-DSPSOL.                                         
      *------------------------------ CODE AIRE                                 
                      06 COMM-GE80-CAIRE          PIC X(02).                    
      *------------------------------ CODE COTE ( A / B )                       
                      06 COMM-GE80-CCOTE          PIC X(02).                    
      *------------------------------ NUMERO D'EMPLACEMENT DS LA MAILLE         
                      06 COMM-GE80-NPOSITIONS     PIC X(03).                    
      *------------------------------ NUMERO PROFONDEUR DS EMPLACEMENT          
                      06 COMM-GE80-CCRAN          PIC 9(03).                    
      *------------------------------ NOMBRE PRODUITS DS LARGEUR MAILLE         
                      06 COMM-GE80-NBRANMAIL      PIC 9(03).                    
      *------------------------------ NOMBRE PRODUITS DS PROFOND.MAILLE         
                      06 COMM-GE80-NBPRNMAIL      PIC 9(03).                    
      *------------------------------ NOMBRE NIVEAUX GERBAGES                   
                      06 COMM-GE80-NBNIVGERB      PIC 9(03).                    
      *------------------------------ ZONE DONNEES SPECIFIQUES RACK             
                   05 COMM-GE80-DSPRACK.                                        
      *------------------------------ CODE ALLEE                                
                      06 COMM-GE80-CALLEE         PIC X(02).                    
      *------------------------------ CODE NIVEAU                               
                      06 COMM-GE80-CNIVEAU        PIC X(02).                    
      *------------------------------ POSITION DS L'ALLEE                       
                      06 COMM-GE80-NPOSITIONR     PIC X(03).                    
      *------------------------------ CODE SPECIFICITE                          
                      06 COMM-GE80-CSPECIF        PIC X(01).                    
      *------------------------------ CODE EMPLACEMENT                          
                      06 COMM-GE80-CTYPEMPL       PIC X(01).                    
      *------------------------------ CODE CONTENEUR                            
                      06 COMM-GE80-CCONTENEUR     PIC X(01).                    
      *------------------------------ ZONE DONNEES SPECIFIQUES VRAC             
                   05 COMM-GE80-DSPVRAC.                                        
      *------------------------------ CODE ZONE                                 
                      06 COMM-GE80-CZONE          PIC X(02).                    
      *------------------------------ CODE SECTEUR                              
                      06 COMM-GE80-CSECTEUR       PIC X(02).                    
      *------------------------------ CODE SOUS_SECTEUR                         
                      06 COMM-GE80-NSOUSSECT      PIC X(03).                    
      *------------------------------ FLAG DLI TERM (OUI = 1)                   
               03 COMM-GE80-WTERM                 PIC 9(1).                     
      *------------------------------ ZONE LIBRE                                
               03 COMM-GE80-FILLER                PIC X(1316).                  
      *****************************************************************         
                                                                                
