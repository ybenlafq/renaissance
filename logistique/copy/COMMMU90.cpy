      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      *                                                                 00050000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-MU90-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00240001
      *--                                                                       
       01  COM-MU90-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ------------------------------ 77  00730000
           02 COMM-MU90-APPLI.                                          00740000
              03 COMM-MU90-CTYPSOC        PIC X(3).                     00741002
              03 COMM-MU90-CTYPLIEU       PIC X(1).                     00741002
              03 COMM-MU90-FILIALE        PIC X(3).                     00741002
              03 COMM-MU90-SELART         PIC X(5).                     00741002
              03 COMM-MU90-LIGNE          PIC X(6).                     00741002
              03 COMM-MU90-SIEGE          PIC X(1).                     00741002
              03 COMM-MU90-NSOC           PIC X(3).                     00741002
              03 COMM-MU90-NLIEU          PIC X(3).                     00741002
              03 COMM-MU90-LLIEU          PIC X(20).                    00741002
              03 COMM-MU90-NBSOC          PIC S9(3) COMP-3.             00741002
              03 COMM-MU90-MATRICE.                                             
                 04 COMM-MU90-AUTOR-NSOC  PIC X(3) OCCURS 10.                   
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ------------------------------ 43  00730000
           02 COMM-MU91-APPLI.                                          00740000
              03 COMM-MU91-NLIGTR         PIC X(6).                     00741002
              03 COMM-MU91-LLIGTR         PIC X(20).                    00741002
              03 COMM-MU91-TYPLIG         PIC X(5).                     00741002
              03 COMM-MU91-WACTIF         PIC X.                        00741002
              03 COMM-MU91-SUPP           PIC X.                        00741002
              03 COMM-MU91-CODSUP         PIC X.                        00741002
              03 COMM-MU91-NUM-PAGE       PIC 9(3).                     00741002
              03 COMM-MU91-IND-PAGE       PIC 9(3).                             
              03 COMM-MU91-MAX-PAGE       PIC 9(3).                             
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ----------------------------- 866  00730000
           02 COMM-MU92-APPLI.                                          00740000
              03 COMM-MU92-LIGNE-MAX      PIC 9(02).                            
AA1015        03 COMM-MU92-PAGEENC        PIC 9(02).                            
AA1015        03 COMM-MU92-PAGEMAXI       PIC 9(02).                            
AA1015        03 FILLER                   PIC X(860).                           
      *                                                                 00810001
      * ZONES RESERVEES APPLICATIVES ----------------------------- 1989 00730000
          02 COMM-MU93-APPLI.                                           00800001
      *                                                                 00810001
              03 COMM-MU93-DMUTSAMJ          PIC X(08).                 00890001
              03 COMM-MU93-DMUTATION         PIC X(08).                 00890001
              03 COMM-MU93-SELART            PIC X(05).                 00890001
              03 COMM-MU93-NSEQ              PIC X(02).                 00890001
              03 COMM-MU93-NJOUR             PIC X(01).                 00890001
              03 COMM-MU93-NLIGNE            PIC X(06).                 00890001
              03 COMM-MU93-TS                PIC X(01).                 00890001
              03 COMM-MU93-NUM-PAGE          PIC 999    COMP-3.         00830001
              03 COMM-MU93-TOP-CURSEUR-M3    PIC 9.                     00830001
              03 COMM-MU93-IND-CURSEUR-M3    PIC 99.                    00830001
              03 COMM-MU93-TOP-CURSEUR-NP    PIC 9.                     00830001
              03 COMM-MU93-IND-CURSEUR-NP    PIC 99.                    00830001
              03 COMM-MU93-MAX-PAGE          PIC 999    COMP-3.         00830001
              03 COMM-MU93-IND-PAGE          PIC 999    COMP-3.         00830001
              03 COMM-MU93-NUM-PAGE2         PIC 999    COMP-3.         00830001
              03 COMM-MU93-MAX-PAGE2         PIC 999    COMP-3.         00830001
              03 COMM-MU93-IND-PAGE2         PIC 999    COMP-3.         00830001
              03 COMM-MU93-TABLE-LT.                                    00180001
                 05 COMM-MU93-LT             OCCURS 50.                         
                    10 COMM-MU93-NLIGTR      PIC X(06).                         
AL0209        03 COMM-MU93-TREAM3            PIC S9999V9 COMP-3.        00830001
AL0209        03 COMM-MU93-TCLIM3            PIC S9999V9 COMP-3.        00830001
AL0209        03 COMM-MU93-TTOTM3            PIC S9999V9 COMP-3.        00830001
AL0209        03 COMM-MU93-TQUOM3            PIC S9999V9 COMP-3.        00830001
              03 COMM-MU93-TTAUM3            PIC  999   COMP-3.         00830001
              03 COMM-MU93-ECARTM3           PIC S999V9 COMP-3.         00830001
1205          03 COMM-MU93-TREANP            PIC 9999   COMP-3.         00830001
1205          03 COMM-MU93-TCLINP            PIC 9999   COMP-3.         00830001
1205          03 COMM-MU93-TTOTNP            PIC S9999V9 COMP-3.        00830001
1205          03 COMM-MU93-TQUONP            PIC S9999V9 COMP-3.        00830001
              03 COMM-MU93-TTAUNP            PIC  999   COMP-3.         00830001
              03 COMM-MU93-ECARTNP           PIC S999V9 COMP-3.         00830001
              03 COMM-MU93-TABLE-MQ.                                    00180001
                 05 COMM-MU93-MQ             OCCURS 100.                        
                    10 COMM-MU93-NITEM       PIC S999    COMP-3.                
                    10 COMM-MU93-LITEM       PIC S999    COMP-3.                
                    10 COMM-MU93-SELAR       PIC X(05).                         
                    10 COMM-MU93-ECAM3       PIC S999V99 COMP-3.                
                    10 COMM-MU93-ECANP       PIC S999V99 COMP-3.                
                    10 COMM-MU93-MODIF       PIC X.                             
              03 COMM-MU93-DATEMUT           PIC X(8).                  00830001
      * ZONES RESERVEES APPLICATIVES -----------------------------   14 00730000
          02 COMM-MU94-APPLI.                                           00800001
      *                                                                 00810001
              03 COMM-MU94-NLIGTR            PIC X(08).                 00890001
              03 COMM-MU94-NUM-PAGE          PIC 999    COMP-3.         00830001
              03 COMM-MU94-MAX-PAGE          PIC 999    COMP-3.         00830001
              03 COMM-MU94-IND-PAGE          PIC 999    COMP-3.         00830001
      * ZONES RESERVEES APPLICATIVES ----------------------------- 548          
          02 COMM-MU95-APPLI.                                                   
             03 COMM-MU95-DATE-LENDEMAIN-TRT PIC X(8).                          
             03 COMM-MU95-NMUTDEST           PIC X(7).                          
             03 COMM-MU95-DMUTATION          PIC X(8).                          
             03 COMM-MU95-CSELART            PIC X(5).                          
             03 COMM-MU95-IMUTDX             PIC 9(2).                          
             03 COMM-MU95-IMUTDN             PIC 9(2).                          
             03 COMM-MU95-IND-MAX            PIC S9(5) COMP-3.                  
             03 COMM-MU95-IND                PIC S9(5) COMP-3.                  
             03 COMM-MU95-TOTPIECE           PIC 9(5).                          
             03 COMM-MU95-TOTLIGNE           PIC 9(5).                          
             03 COMM-MU95-TOTVOLUM           PIC S9(3)V999    COMP-3.           
             03 COMM-MU95-EN-TETE.                                              
                04 COMM-MU95-PAGE               PIC 9(4).                       
                04 COMM-MU95-NBPAGE             PIC 9(4).                       
                04 COMM-MU95-NSOCDEPOT          PIC X(3).                       
                04 COMM-MU95-NLIEUDEPOT         PIC X(3).                       
                04 COMM-MU95-LLIEUDEPOT         PIC X(20).                      
                04 COMM-MU95-QNBLIGNES          PIC X(5).                       
                04 COMM-MU95-QNBPIECES          PIC X(5).                       
                04 COMM-MU95-QNBPQUOTA          PIC X(5).                       
                04 COMM-MU95-QVOLUME            PIC X(6).                       
                04 COMM-MU95-QDISPO             PIC X(6).                       
                04 COMM-MU95-QNBM3QUOTA         PIC X(7).                       
                04 COMM-MU95-TAUXM3             PIC X(4).                       
                04 COMM-MU95-TAUXNP             PIC X(4).                       
      *--  TABLEAU DES MUTATIONS DESTINATAIRES                                  
             03 COMM-MU95-TAB-MUT-DEST.                                         
                05 COMM-MU95-T-MUT-DEST  OCCURS 20.                             
                   10 COMM-MU95-I-MUT-DEST             PIC 9(2).                
                   10 COMM-MU95-I-NMUTATION-DEST       PIC X(7).                
                   10 COMM-MU95-I-DMUTATION-DEST       PIC X(8).                
                   10 COMM-MU95-I-CSELART-DEST         PIC X(5).                
      *    02 COMM-MU90-FILLER            PIC X(0161).                  00970003
AL0209     02 COMM-MU90-FILLER            PIC X(0159).                  00970003
                                                                                
