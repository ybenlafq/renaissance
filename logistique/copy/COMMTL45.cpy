      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00020000
      * COMMAREA SPECIFIQUE PRG TTL45 (TTL00 -> MENU)    TR: TL00  *    00030000
      *                                                            *    00031000
           02 COMM-TL45-APPLI   REDEFINES COMM-TL00-APPLI.              00721000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-TL45-ITEM       PIC S9(4)    COMP.                00743004
      *--                                                                       
              03 COMM-TL45-ITEM       PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-TL45-POS-MAX    PIC S9(4)    COMP.                00743005
      *--                                                                       
              03 COMM-TL45-POS-MAX    PIC S9(4) COMP-5.                         
      *}                                                                        
              03 COMM-TL45-CHOIX      PIC X(01).                        00743006
              03 FILLER               PIC X(3468).                      00743030
                                                                                
                                                                        00750000
