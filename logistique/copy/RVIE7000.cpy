      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVIE7000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIE7000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVIE7000.                                                            
           02  IE70-NLIEUHS                                                     
               PIC X(0003).                                                     
           02  IE70-NHS                                                         
               PIC X(0007).                                                     
           02  IE70-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  IE70-NLIEU                                                       
               PIC X(0003).                                                     
           02  IE70-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  IE70-CLIEUTRT                                                    
               PIC X(0005).                                                     
           02  IE70-NCODIC                                                      
               PIC X(0007).                                                     
           02  IE70-QTEINVENT                                                   
               PIC S9(5) COMP-3.                                                
           02  IE70-CAPPLI                                                      
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVIE7000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVIE7000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE70-NLIEUHS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE70-NLIEUHS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE70-NHS-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE70-NHS-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE70-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE70-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE70-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE70-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE70-NSSLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE70-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE70-CLIEUTRT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE70-CLIEUTRT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE70-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE70-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE70-QTEINVENT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE70-QTEINVENT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE70-CAPPLI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  IE70-CAPPLI-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
