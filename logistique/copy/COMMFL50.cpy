      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * COM-FL50-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-FL50-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-FL50-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA                                                  
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS                                   
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION                     
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC XX.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00690010
          02 COMM-DATE-WEEK.                                            00690020
             05  COMM-DATE-SEMSS   PIC 99.                              00690030
             05  COMM-DATE-SEMAA   PIC 99.                              00690040
             05  COMM-DATE-SEMNU   PIC 99.                              00690050
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR       OCCURS 150 PIC X(1).                         
      *                                                                         
      * ZONES RESERVEES APPLICATIVES         RESTE = 3571                       
      *                                                                         
      *                                                                         
           02  COMM-FL50-APPLI.                                                 
      *=> ZONES DE SAUVEGARDE COMMUNES AU MENU PRINCIPAL                        
      *=> ET AUX TRANSACTIONS DE NIVEAU INFERIEUR        = 553                  
              03  COMM-FL50-DONNEES.                                            
                  05  COMM-FL50-CODIC          PIC X(07).                       
                  05  COMM-FL50-LREFFOURN      PIC X(20).                       
                  05  COMM-FL50-CFAM           PIC X(05).                       
                  05  COMM-FL50-LFAM           PIC X(20).                       
                  05  COMM-FL50-CMARQ          PIC X(05).                       
                  05  COMM-FL50-LMARQ          PIC X(20).                       
      * DONNEES DE CONTROLES                                                    
                  05  COMM-FL50-CGESTVTE       PIC X(1).                        
                  05  COMM-FL50-WMULTIFAM      PIC X(1).                        
                  05  COMM-FL50-WDACEM         PIC X(1).                        
                  05  COMM-FL50-QCONDT         PIC S9(5) COMP-3.                
      * DONNEES DES ENTRS.                                                      
                  05  COMM-FL50-PROTEGE        PIC X(1).                        
                  05  COMM-FL50-TOUS           PIC X(01).                       
                  05  COMM-FL50-IDEM           PIC X(01).                       
                  05  COMM-FL50-ITEMAX         PIC S9(01) COMP-3.               
                  05  COMM-FL50-ITEM           PIC S9(01) COMP-3.               
                  05  COMM-FL50-LISTE.                                          
                      15  COMM-FL50-TAB-ENT    OCCURS 5 TIMES.                  
                          20  COMM-FL50-SOCIETE PIC X(03).                      
                          20  COMM-FL50-NDEPOT  PIC X(03).                      
                          20  COMM-FL50-LDEPOT  PIC X(20).                      
                          20  COMM-FL50-CHGT1   PIC S9(02) COMP-3.              
                          20  COMM-FL50-CHGT2   PIC S9(02) COMP-3.              
                          20  COMM-FL50-DIF     PIC X(1).                       
                          20  COMM-FL50-DNPC    PIC X(1).                       
                          20  COMM-FL50-DN      PIC X(1).                       
                          20  COMM-FL50-DAL     PIC X(1).                       
                          20  COMM-FL50-DRA     PIC X(1).                       
                          20  COMM-FL50-DPM     PIC X(1).                       
                          20  COMM-FL50-WABC    PIC X(1).                       
                          20  COMM-FL50-WMS     PIC X(1).                       
                          20  COMM-FL50-DO      PIC X(1).                       
PB0203                    20  COMM-FL50-DLUX    PIC X(1).                       
PB0203        03  COMM-FL50-FILLER             PIC X(3436).                     
                                                                                
