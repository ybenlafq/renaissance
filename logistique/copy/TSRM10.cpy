      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00010000
      *             REAPPROVISONNEMENT MAGASIN                       *  00030000
      *         - TRANSACTION RM10                                   *  00040000
      *           (VISUALISATION DES LOIS)                           *  00041000
      ****************************************************************  00050000
       01  (P)-LONG        PIC S9(4) COMP-3 VALUE +2267.                00080000
       01  (P)-DONNEES.                                                 00090000
           02 FILLER       PIC X(12).                                   00750000
           02 FILLER       PIC X(2).                                    00760000
           02 (P)-MDATJOUA PIC X.                                       00770000
           02 (P)-MDATJOUC PIC X.                                       00780000
           02 (P)-MDATJOUP PIC X.                                       00790000
           02 (P)-MDATJOUH PIC X.                                       00800000
           02 (P)-MDATJOUV PIC X.                                       00810000
           02 (P)-MDATJOUO PIC X(10).                                   00820000
           02 FILLER       PIC X(2).                                    00830000
           02 (P)-MTIMJOUA PIC X.                                       00840000
           02 (P)-MTIMJOUC PIC X.                                       00850000
           02 (P)-MTIMJOUP PIC X.                                       00860000
           02 (P)-MTIMJOUH PIC X.                                       00870000
           02 (P)-MTIMJOUV PIC X.                                       00880000
           02 (P)-MTIMJOUO PIC X(5).                                    00890000
           02 FILLER       PIC X(2).                                    00900000
           02 (P)-MNPAGEA PIC X.                                        00910000
           02 (P)-MNPAGEC PIC X.                                        00920000
           02 (P)-MNPAGEP PIC X.                                        00930000
           02 (P)-MNPAGEH PIC X.                                        00940000
           02 (P)-MNPAGEV PIC X.                                        00950000
           02 (P)-MNPAGEO PIC X(3).                                     00960000
           02 (P)-L-MTINPOO.                                            00970000
              03 FILLER OCCURS 10.                                      00971000
                 05 FILLER       PIC X(2).                              00980000
                 05 (P)-MTINPOA  PIC X.                                 00990000
                 05 (P)-MTINPOC  PIC X.                                 01000000
                 05 (P)-MTINPOP  PIC X.                                 01010000
                 05 (P)-MTINPOH  PIC X.                                 01020000
                 05 (P)-MTINPOV  PIC X.                                 01030000
                 05 (P)-MTINPOO  PIC X(5).                              01040000
           02 (P)-LIGNEO OCCURS   14 TIMES .                            01050000
             03 FILLER     PIC X(2).                                    01060000
             03 (P)-MQV8SA PIC X.                                       01070000
             03 (P)-MQV8SC PIC X.                                       01080000
             03 (P)-MQV8SP PIC X.                                       01090000
             03 (P)-MQV8SH PIC X.                                       01100000
             03 (P)-MQV8SV PIC X.                                       01110000
             03 (P)-MQV8SO PIC X(5).                                    01120000
             03 FILLER OCCURS 10.                                       01130000
                05 FILLER        PIC X(2).                              01140000
                05 (P)-MQSOBJA   PIC X.                                 01150000
                05 (P)-MQSOBJC   PIC X.                                 01160000
                05 (P)-MQSOBJP   PIC X.                                 01170000
                05 (P)-MQSOBJH   PIC X.                                 01180000
                05 (P)-MQSOBJV   PIC X.                                 01190000
                05 (P)-MQSOBJO   PIC X(5).                              01200000
             03 FILLER        PIC X(2).                                 01201000
             03 (P)-MWREPEATA PIC X.                                    01210000
             03 (P)-MWREPEATC PIC X.                                    01220000
             03 (P)-MWREPEATP PIC X.                                    01230000
             03 (P)-MWREPEATH PIC X.                                    01240000
             03 (P)-MWREPEATV PIC X.                                    01250000
             03 (P)-MWREPEATO PIC X.                                    01260000
           02 FILLER       PIC X(2).                                    01270000
           02 (P)-MZONCMDA PIC X.                                       01280000
           02 (P)-MZONCMDC PIC X.                                       01290000
           02 (P)-MZONCMDP PIC X.                                       01300000
           02 (P)-MZONCMDH PIC X.                                       01310000
           02 (P)-MZONCMDV PIC X.                                       01320000
           02 (P)-MZONCMDO PIC X(12).                                   01330000
           02 FILLER       PIC X(2).                                    01340000
           02 (P)-MLIBERRA PIC X.                                       01350000
           02 (P)-MLIBERRC PIC X.                                       01360000
           02 (P)-MLIBERRP PIC X.                                       01370000
           02 (P)-MLIBERRH PIC X.                                       01380000
           02 (P)-MLIBERRV PIC X.                                       01390000
           02 (P)-MLIBERRO PIC X(61).                                   01400000
           02 FILLER       PIC X(2).                                    01410000
           02 (P)-MCODTRAA PIC X.                                       01420000
           02 (P)-MCODTRAC PIC X.                                       01430000
           02 (P)-MCODTRAP PIC X.                                       01440000
           02 (P)-MCODTRAH PIC X.                                       01450000
           02 (P)-MCODTRAV PIC X.                                       01460000
           02 (P)-MCODTRAO PIC X(4).                                    01470000
           02 FILLER       PIC X(2).                                    01480000
           02 (P)-MCICSA PIC X.                                         01490000
           02 (P)-MCICSC PIC X.                                         01500000
           02 (P)-MCICSP PIC X.                                         01510000
           02 (P)-MCICSH PIC X.                                         01520000
           02 (P)-MCICSV PIC X.                                         01530000
           02 (P)-MCICSO PIC X(5).                                      01540000
           02 FILLER       PIC X(2).                                    01550000
           02 (P)-MNETNAMA PIC X.                                       01560000
           02 (P)-MNETNAMC PIC X.                                       01570000
           02 (P)-MNETNAMP PIC X.                                       01580000
           02 (P)-MNETNAMH PIC X.                                       01590000
           02 (P)-MNETNAMV PIC X.                                       01600000
           02 (P)-MNETNAMO PIC X(8).                                    01610000
           02 FILLER       PIC X(2).                                    01620000
           02 (P)-MSCREENA PIC X.                                       01630000
           02 (P)-MSCREENC PIC X.                                       01640000
           02 (P)-MSCREENP PIC X.                                       01650000
           02 (P)-MSCREENH PIC X.                                       01660000
           02 (P)-MSCREENV PIC X.                                       01670000
           02 (P)-MSCREENO PIC X(4).                                    01680000
                                                                                
