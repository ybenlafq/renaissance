      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EZQ20   EZQ20                                              00000020
      ***************************************************************** 00000030
       01   EZQ20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPQUOTAL    COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MLTYPQUOTAL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLTYPQUOTAF    PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MLTYPQUOTAI    PIC X(6).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEQUIPEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MEQUIPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MEQUIPEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MEQUIPEI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJOURL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDJOURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDJOURF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDJOURI   PIC X(8).                                       00000290
           02 MLPLAGED OCCURS   6 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPLAGEL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MLPLAGEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLPLAGEF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MLPLAGEI     PIC X(8).                                  00000340
           02 MLIGNEI OCCURS   16 TIMES .                               00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000360
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 MLIBELLEI    PIC X(16).                                 00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQQUOTA-ZLL  COMP PIC S9(4).                            00000400
      *--                                                                       
             03 MQQUOTA-ZLL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQQUOTA-ZLF  PIC X.                                     00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MQQUOTA-ZLI  PIC X(4).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPRIS-ZLL   COMP PIC S9(4).                            00000440
      *--                                                                       
             03 MQPRIS-ZLL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQPRIS-ZLF   PIC X.                                     00000450
             03 FILLER  PIC X(4).                                       00000460
             03 MQPRIS-ZLI   PIC X(4).                                  00000470
             03 MQQUOTAD OCCURS   5 TIMES .                             00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQQUOTAL   COMP PIC S9(4).                            00000490
      *--                                                                       
               04 MQQUOTAL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MQQUOTAF   PIC X.                                     00000500
               04 FILLER     PIC X(4).                                  00000510
               04 MQQUOTAI   PIC X(3).                                  00000520
             03 MQPRISD OCCURS   5 TIMES .                              00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQPRISL    COMP PIC S9(4).                            00000540
      *--                                                                       
               04 MQPRISL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MQPRISF    PIC X.                                     00000550
               04 FILLER     PIC X(4).                                  00000560
               04 MQPRISI    PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQQUOTAJL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MQQUOTAJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQQUOTAJF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQQUOTAJI      PIC X(5).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPRISJL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MQPRISJL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPRISJF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQPRISJI  PIC X(4).                                       00000650
           02 MQQUOTA-PLD OCCURS   5 TIMES .                            00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQQUOTA-PLL  COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MQQUOTA-PLL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQQUOTA-PLF  PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQQUOTA-PLI  PIC X(3).                                  00000700
           02 MQPRIS-PLD OCCURS   5 TIMES .                             00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPRIS-PLL   COMP PIC S9(4).                            00000720
      *--                                                                       
             03 MQPRIS-PLL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQPRIS-PLF   PIC X.                                     00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MQPRIS-PLI   PIC X(3).                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MZONCMDI  PIC X(15).                                      00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MLIBERRI  PIC X(58).                                      00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MCODTRAI  PIC X(4).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MCICSI    PIC X(5).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MNETNAMI  PIC X(8).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MSCREENI  PIC X(4).                                       00000990
      ***************************************************************** 00001000
      * SDF: EZQ20   EZQ20                                              00001010
      ***************************************************************** 00001020
       01   EZQ20O REDEFINES EZQ20I.                                    00001030
           02 FILLER    PIC X(12).                                      00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MDATJOUA  PIC X.                                          00001060
           02 MDATJOUC  PIC X.                                          00001070
           02 MDATJOUP  PIC X.                                          00001080
           02 MDATJOUH  PIC X.                                          00001090
           02 MDATJOUV  PIC X.                                          00001100
           02 MDATJOUO  PIC X(10).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MTIMJOUA  PIC X.                                          00001130
           02 MTIMJOUC  PIC X.                                          00001140
           02 MTIMJOUP  PIC X.                                          00001150
           02 MTIMJOUH  PIC X.                                          00001160
           02 MTIMJOUV  PIC X.                                          00001170
           02 MTIMJOUO  PIC X(5).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MLTYPQUOTAA    PIC X.                                     00001200
           02 MLTYPQUOTAC    PIC X.                                     00001210
           02 MLTYPQUOTAP    PIC X.                                     00001220
           02 MLTYPQUOTAH    PIC X.                                     00001230
           02 MLTYPQUOTAV    PIC X.                                     00001240
           02 MLTYPQUOTAO    PIC X(6).                                  00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNPAGEA   PIC X.                                          00001270
           02 MNPAGEC   PIC X.                                          00001280
           02 MNPAGEP   PIC X.                                          00001290
           02 MNPAGEH   PIC X.                                          00001300
           02 MNPAGEV   PIC X.                                          00001310
           02 MNPAGEO   PIC X(3).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MEQUIPEA  PIC X.                                          00001340
           02 MEQUIPEC  PIC X.                                          00001350
           02 MEQUIPEP  PIC X.                                          00001360
           02 MEQUIPEH  PIC X.                                          00001370
           02 MEQUIPEV  PIC X.                                          00001380
           02 MEQUIPEO  PIC X(5).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDJOURA   PIC X.                                          00001410
           02 MDJOURC   PIC X.                                          00001420
           02 MDJOURP   PIC X.                                          00001430
           02 MDJOURH   PIC X.                                          00001440
           02 MDJOURV   PIC X.                                          00001450
           02 MDJOURO   PIC X(8).                                       00001460
           02 DFHMS1 OCCURS   6 TIMES .                                 00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MLPLAGEA     PIC X.                                     00001490
             03 MLPLAGEC     PIC X.                                     00001500
             03 MLPLAGEP     PIC X.                                     00001510
             03 MLPLAGEH     PIC X.                                     00001520
             03 MLPLAGEV     PIC X.                                     00001530
             03 MLPLAGEO     PIC X(8).                                  00001540
           02 MLIGNEO OCCURS   16 TIMES .                               00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MLIBELLEA    PIC X.                                     00001570
             03 MLIBELLEC    PIC X.                                     00001580
             03 MLIBELLEP    PIC X.                                     00001590
             03 MLIBELLEH    PIC X.                                     00001600
             03 MLIBELLEV    PIC X.                                     00001610
             03 MLIBELLEO    PIC X(16).                                 00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MQQUOTA-ZLA  PIC X.                                     00001640
             03 MQQUOTA-ZLC  PIC X.                                     00001650
             03 MQQUOTA-ZLP  PIC X.                                     00001660
             03 MQQUOTA-ZLH  PIC X.                                     00001670
             03 MQQUOTA-ZLV  PIC X.                                     00001680
             03 MQQUOTA-ZLO  PIC X(4).                                  00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MQPRIS-ZLA   PIC X.                                     00001710
             03 MQPRIS-ZLC   PIC X.                                     00001720
             03 MQPRIS-ZLP   PIC X.                                     00001730
             03 MQPRIS-ZLH   PIC X.                                     00001740
             03 MQPRIS-ZLV   PIC X.                                     00001750
             03 MQPRIS-ZLO   PIC X(4).                                  00001760
             03 DFHMS2 OCCURS   5 TIMES .                               00001770
               04 FILLER     PIC X(2).                                  00001780
               04 MQQUOTAA   PIC X.                                     00001790
               04 MQQUOTAC   PIC X.                                     00001800
               04 MQQUOTAP   PIC X.                                     00001810
               04 MQQUOTAH   PIC X.                                     00001820
               04 MQQUOTAV   PIC X.                                     00001830
               04 MQQUOTAO   PIC X(3).                                  00001840
             03 DFHMS3 OCCURS   5 TIMES .                               00001850
               04 FILLER     PIC X(2).                                  00001860
               04 MQPRISA    PIC X.                                     00001870
               04 MQPRISC    PIC X.                                     00001880
               04 MQPRISP    PIC X.                                     00001890
               04 MQPRISH    PIC X.                                     00001900
               04 MQPRISV    PIC X.                                     00001910
               04 MQPRISO    PIC X(3).                                  00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MQQUOTAJA      PIC X.                                     00001940
           02 MQQUOTAJC PIC X.                                          00001950
           02 MQQUOTAJP PIC X.                                          00001960
           02 MQQUOTAJH PIC X.                                          00001970
           02 MQQUOTAJV PIC X.                                          00001980
           02 MQQUOTAJO      PIC X(5).                                  00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MQPRISJA  PIC X.                                          00002010
           02 MQPRISJC  PIC X.                                          00002020
           02 MQPRISJP  PIC X.                                          00002030
           02 MQPRISJH  PIC X.                                          00002040
           02 MQPRISJV  PIC X.                                          00002050
           02 MQPRISJO  PIC X(4).                                       00002060
           02 DFHMS4 OCCURS   5 TIMES .                                 00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MQQUOTA-PLA  PIC X.                                     00002090
             03 MQQUOTA-PLC  PIC X.                                     00002100
             03 MQQUOTA-PLP  PIC X.                                     00002110
             03 MQQUOTA-PLH  PIC X.                                     00002120
             03 MQQUOTA-PLV  PIC X.                                     00002130
             03 MQQUOTA-PLO  PIC X(3).                                  00002140
           02 DFHMS5 OCCURS   5 TIMES .                                 00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MQPRIS-PLA   PIC X.                                     00002170
             03 MQPRIS-PLC   PIC X.                                     00002180
             03 MQPRIS-PLP   PIC X.                                     00002190
             03 MQPRIS-PLH   PIC X.                                     00002200
             03 MQPRIS-PLV   PIC X.                                     00002210
             03 MQPRIS-PLO   PIC X(3).                                  00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MZONCMDA  PIC X.                                          00002240
           02 MZONCMDC  PIC X.                                          00002250
           02 MZONCMDP  PIC X.                                          00002260
           02 MZONCMDH  PIC X.                                          00002270
           02 MZONCMDV  PIC X.                                          00002280
           02 MZONCMDO  PIC X(15).                                      00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLIBERRA  PIC X.                                          00002310
           02 MLIBERRC  PIC X.                                          00002320
           02 MLIBERRP  PIC X.                                          00002330
           02 MLIBERRH  PIC X.                                          00002340
           02 MLIBERRV  PIC X.                                          00002350
           02 MLIBERRO  PIC X(58).                                      00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MCODTRAA  PIC X.                                          00002380
           02 MCODTRAC  PIC X.                                          00002390
           02 MCODTRAP  PIC X.                                          00002400
           02 MCODTRAH  PIC X.                                          00002410
           02 MCODTRAV  PIC X.                                          00002420
           02 MCODTRAO  PIC X(4).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MCICSA    PIC X.                                          00002450
           02 MCICSC    PIC X.                                          00002460
           02 MCICSP    PIC X.                                          00002470
           02 MCICSH    PIC X.                                          00002480
           02 MCICSV    PIC X.                                          00002490
           02 MCICSO    PIC X(5).                                       00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MNETNAMA  PIC X.                                          00002520
           02 MNETNAMC  PIC X.                                          00002530
           02 MNETNAMP  PIC X.                                          00002540
           02 MNETNAMH  PIC X.                                          00002550
           02 MNETNAMV  PIC X.                                          00002560
           02 MNETNAMO  PIC X(8).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MSCREENA  PIC X.                                          00002590
           02 MSCREENC  PIC X.                                          00002600
           02 MSCREENP  PIC X.                                          00002610
           02 MSCREENH  PIC X.                                          00002620
           02 MSCREENV  PIC X.                                          00002630
           02 MSCREENO  PIC X(4).                                       00002640
                                                                                
