      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU66   EMU66                                              00000020
      ***************************************************************** 00000030
       01   EMU66I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCODICI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDEPF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCDEPI      PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNDEPOTI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLDEPOTI  PIC X(20).                                      00000450
           02 FILLER  OCCURS   13 TIMES .                               00000460
      * code famille                                                    00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000480
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000490
             03 FILLER  PIC X(4).                                       00000500
             03 MCFAMI  PIC X(5).                                       00000510
      * code marque                                                     00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MCMARQI      PIC X(5).                                  00000560
      * reference                                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000580
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000590
             03 FILLER  PIC X(4).                                       00000600
             03 MREFI   PIC X(20).                                      00000610
      * codic                                                           00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCODICI      PIC X(7).                                  00000660
      * code statut                                                     00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATL  COMP PIC S9(4).                                 00000680
      *--                                                                       
             03 MSTATL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSTATF  PIC X.                                          00000690
             03 FILLER  PIC X(4).                                       00000700
             03 MSTATI  PIC X.                                          00000710
      * n� document N�CDE                                               00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDOCL  COMP PIC S9(4).                                 00000730
      *--                                                                       
             03 MNDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNDOCF  PIC X.                                          00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MNDOCI  PIC X(7).                                       00000760
      * numero mutation                                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTL  COMP PIC S9(4).                                 00000780
      *--                                                                       
             03 MNMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMUTF  PIC X.                                          00000790
             03 FILLER  PIC X(4).                                       00000800
             03 MNMUTI  PIC X(7).                                       00000810
      * date deliv ou mutation                                          00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMUTL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MDMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDMUTF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MDMUTI  PIC X(8).                                       00000860
      * qte demandee                                                    00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000880
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000890
             03 FILLER  PIC X(4).                                       00000900
             03 MQTEI   PIC X(4).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MZONCMDI  PIC X(15).                                      00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MLIBERRI  PIC X(58).                                      00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MCODTRAI  PIC X(4).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MCICSI    PIC X(5).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MNETNAMI  PIC X(8).                                       00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MSCREENI  PIC X(4).                                       00001150
      ***************************************************************** 00001160
      * SDF: EMU66   EMU66                                              00001170
      ***************************************************************** 00001180
       01   EMU66O REDEFINES EMU66I.                                    00001190
           02 FILLER    PIC X(12).                                      00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MDATJOUA  PIC X.                                          00001220
           02 MDATJOUC  PIC X.                                          00001230
           02 MDATJOUP  PIC X.                                          00001240
           02 MDATJOUH  PIC X.                                          00001250
           02 MDATJOUV  PIC X.                                          00001260
           02 MDATJOUO  PIC X(10).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MTIMJOUA  PIC X.                                          00001290
           02 MTIMJOUC  PIC X.                                          00001300
           02 MTIMJOUP  PIC X.                                          00001310
           02 MTIMJOUH  PIC X.                                          00001320
           02 MTIMJOUV  PIC X.                                          00001330
           02 MTIMJOUO  PIC X(5).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MPAGEA    PIC X.                                          00001360
           02 MPAGEC    PIC X.                                          00001370
           02 MPAGEP    PIC X.                                          00001380
           02 MPAGEH    PIC X.                                          00001390
           02 MPAGEV    PIC X.                                          00001400
           02 MPAGEO    PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNSOCA    PIC X.                                          00001430
           02 MNSOCC    PIC X.                                          00001440
           02 MNSOCP    PIC X.                                          00001450
           02 MNSOCH    PIC X.                                          00001460
           02 MNSOCV    PIC X.                                          00001470
           02 MNSOCO    PIC X(3).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNLIEUA   PIC X.                                          00001500
           02 MNLIEUC   PIC X.                                          00001510
           02 MNLIEUP   PIC X.                                          00001520
           02 MNLIEUH   PIC X.                                          00001530
           02 MNLIEUV   PIC X.                                          00001540
           02 MNLIEUO   PIC X(3).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLLIEUA   PIC X.                                          00001570
           02 MLLIEUC   PIC X.                                          00001580
           02 MLLIEUP   PIC X.                                          00001590
           02 MLLIEUH   PIC X.                                          00001600
           02 MLLIEUV   PIC X.                                          00001610
           02 MLLIEUO   PIC X(20).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNCODICA  PIC X.                                          00001640
           02 MNCODICC  PIC X.                                          00001650
           02 MNCODICP  PIC X.                                          00001660
           02 MNCODICH  PIC X.                                          00001670
           02 MNCODICV  PIC X.                                          00001680
           02 MNCODICO  PIC X(7).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNSOCDEPA      PIC X.                                     00001710
           02 MNSOCDEPC PIC X.                                          00001720
           02 MNSOCDEPP PIC X.                                          00001730
           02 MNSOCDEPH PIC X.                                          00001740
           02 MNSOCDEPV PIC X.                                          00001750
           02 MNSOCDEPO      PIC X(3).                                  00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNDEPOTA  PIC X.                                          00001780
           02 MNDEPOTC  PIC X.                                          00001790
           02 MNDEPOTP  PIC X.                                          00001800
           02 MNDEPOTH  PIC X.                                          00001810
           02 MNDEPOTV  PIC X.                                          00001820
           02 MNDEPOTO  PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLDEPOTA  PIC X.                                          00001850
           02 MLDEPOTC  PIC X.                                          00001860
           02 MLDEPOTP  PIC X.                                          00001870
           02 MLDEPOTH  PIC X.                                          00001880
           02 MLDEPOTV  PIC X.                                          00001890
           02 MLDEPOTO  PIC X(20).                                      00001900
           02 FILLER  OCCURS   13 TIMES .                               00001910
      * code famille                                                    00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MCFAMA  PIC X.                                          00001940
             03 MCFAMC  PIC X.                                          00001950
             03 MCFAMP  PIC X.                                          00001960
             03 MCFAMH  PIC X.                                          00001970
             03 MCFAMV  PIC X.                                          00001980
             03 MCFAMO  PIC X(5).                                       00001990
      * code marque                                                     00002000
             03 FILLER       PIC X(2).                                  00002010
             03 MCMARQA      PIC X.                                     00002020
             03 MCMARQC PIC X.                                          00002030
             03 MCMARQP PIC X.                                          00002040
             03 MCMARQH PIC X.                                          00002050
             03 MCMARQV PIC X.                                          00002060
             03 MCMARQO      PIC X(5).                                  00002070
      * reference                                                       00002080
             03 FILLER       PIC X(2).                                  00002090
             03 MREFA   PIC X.                                          00002100
             03 MREFC   PIC X.                                          00002110
             03 MREFP   PIC X.                                          00002120
             03 MREFH   PIC X.                                          00002130
             03 MREFV   PIC X.                                          00002140
             03 MREFO   PIC X(20).                                      00002150
      * codic                                                           00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MCODICA      PIC X.                                     00002180
             03 MCODICC PIC X.                                          00002190
             03 MCODICP PIC X.                                          00002200
             03 MCODICH PIC X.                                          00002210
             03 MCODICV PIC X.                                          00002220
             03 MCODICO      PIC X(7).                                  00002230
      * code statut                                                     00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MSTATA  PIC X.                                          00002260
             03 MSTATC  PIC X.                                          00002270
             03 MSTATP  PIC X.                                          00002280
             03 MSTATH  PIC X.                                          00002290
             03 MSTATV  PIC X.                                          00002300
             03 MSTATO  PIC X.                                          00002310
      * n� document N�CDE                                               00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MNDOCA  PIC X.                                          00002340
             03 MNDOCC  PIC X.                                          00002350
             03 MNDOCP  PIC X.                                          00002360
             03 MNDOCH  PIC X.                                          00002370
             03 MNDOCV  PIC X.                                          00002380
             03 MNDOCO  PIC X(7).                                       00002390
      * numero mutation                                                 00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MNMUTA  PIC X.                                          00002420
             03 MNMUTC  PIC X.                                          00002430
             03 MNMUTP  PIC X.                                          00002440
             03 MNMUTH  PIC X.                                          00002450
             03 MNMUTV  PIC X.                                          00002460
             03 MNMUTO  PIC X(7).                                       00002470
      * date deliv ou mutation                                          00002480
             03 FILLER       PIC X(2).                                  00002490
             03 MDMUTA  PIC X.                                          00002500
             03 MDMUTC  PIC X.                                          00002510
             03 MDMUTP  PIC X.                                          00002520
             03 MDMUTH  PIC X.                                          00002530
             03 MDMUTV  PIC X.                                          00002540
             03 MDMUTO  PIC X(8).                                       00002550
      * qte demandee                                                    00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MQTEA   PIC X.                                          00002580
             03 MQTEC   PIC X.                                          00002590
             03 MQTEP   PIC X.                                          00002600
             03 MQTEH   PIC X.                                          00002610
             03 MQTEV   PIC X.                                          00002620
             03 MQTEO   PIC ZZZ9.                                       00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MZONCMDA  PIC X.                                          00002650
           02 MZONCMDC  PIC X.                                          00002660
           02 MZONCMDP  PIC X.                                          00002670
           02 MZONCMDH  PIC X.                                          00002680
           02 MZONCMDV  PIC X.                                          00002690
           02 MZONCMDO  PIC X(15).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MLIBERRA  PIC X.                                          00002720
           02 MLIBERRC  PIC X.                                          00002730
           02 MLIBERRP  PIC X.                                          00002740
           02 MLIBERRH  PIC X.                                          00002750
           02 MLIBERRV  PIC X.                                          00002760
           02 MLIBERRO  PIC X(58).                                      00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCODTRAA  PIC X.                                          00002790
           02 MCODTRAC  PIC X.                                          00002800
           02 MCODTRAP  PIC X.                                          00002810
           02 MCODTRAH  PIC X.                                          00002820
           02 MCODTRAV  PIC X.                                          00002830
           02 MCODTRAO  PIC X(4).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MCICSA    PIC X.                                          00002860
           02 MCICSC    PIC X.                                          00002870
           02 MCICSP    PIC X.                                          00002880
           02 MCICSH    PIC X.                                          00002890
           02 MCICSV    PIC X.                                          00002900
           02 MCICSO    PIC X(5).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MNETNAMA  PIC X.                                          00002930
           02 MNETNAMC  PIC X.                                          00002940
           02 MNETNAMP  PIC X.                                          00002950
           02 MNETNAMH  PIC X.                                          00002960
           02 MNETNAMV  PIC X.                                          00002970
           02 MNETNAMO  PIC X(8).                                       00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MSCREENA  PIC X.                                          00003000
           02 MSCREENC  PIC X.                                          00003010
           02 MSCREENP  PIC X.                                          00003020
           02 MSCREENH  PIC X.                                          00003030
           02 MSCREENV  PIC X.                                          00003040
           02 MSCREENO  PIC X(4).                                       00003050
                                                                                
