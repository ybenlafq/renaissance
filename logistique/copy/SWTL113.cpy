      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*00000010
      *                                                                *00000020
      *   Gestion des statistiques de livraison                        *00000030
      *                                                                *00000040
      *   D�finition de la structure du fichier FTL113                 *00000050
      *     (Les mouvements de livraison et de retour pour �dition)    *00000060
      *                                                                *00000070
      *   Extraction puis cumul sur fichier FTL111.                    *00000080
      *   S�quentiel ; Longueur 120                                    *00000090
      *                                                                *00000100
      *----------------------------------------------------------------*00000110
      *                                                                *00000120
      * FTL113-NSOC       Code soci�t�                         001/003 *00000130
      *       -NDEPOT     Code du d�p�t                        004/006 *00000140
      *       -DMVT       Date du mouvement (top�)             007/014 *00000150
      *       -PERIM      P�rim�tre de livraison               015/019 *00000160
      *       -CEQUIPE    Equipe de livraison                  020/024 *00000170
      *       -CEQUIP     Type d'�quipage                      025/029 *00000180
      *       -CLIVR1     chef d'equipage                      030/039 *00000190
      *       -COMPOS     Composition (1/2/3)                  040/040 *00000200
      *       -NMAG       Magasin de vente                     041/043 *00000210
      *       -NVENTE     N� de vente                          044/050 *00000220
      *       -DDELIV     Date de livraison                    051/058 *00000230
      *       -QEQUIP     Quantit� d'�quipage                  059/061 *00000240
      *       -QBLDEP     Quantit� de BL au d�part             062/064 *00000250
      *       -CRETOUR    Code retour                          065/069 *00000260
      *       -CIMPUT     Code retour imputable (0/1)          070/070 *00000270
      *       -QBLRETIMP  Quantit� de BL avec retour imputable 071/073 *00000280
      *       -QBLRETNIMP Quantit� de BL avec retour non imp.  074/076 *00000290
      *       -QPIECESDEP Quantit� de pi�ces au d�part         077/080 *00000300
      *       -QPIECESRETIMP  "    "    "  retour imput.       081/084 *00000310
      *       -QPIECESRETNIMP "    "    "  retour non imput.   085/088 *00000320
      *       -CAGR1-ITL112  Code agreg. (CAGR1/TEQUI)         089/090 *00000330
      *       -CAGR2-ITL112      "       (CAGR2/TEQUI)         091/092 *00000340
      *       -CAGR1-ITL113      "       (CAGR1/TRETO)         093/094 *00000350
      *       -CAGR2-ITL113      "       (CAGR2/TRETO)         095/096 *00000360
      *       -CAGR1-ITL114      "       (CAGR3/TRETO)         097/098 *00000370
      *       -CAGR1-ITL115      "       (CAGR4/TRETO)         099/100 *00000380
      *       -CEDIT-ITL116  Code �dition   (o/n)              101/101 *00000390
      *       -CGRPMAG       Groupe de vente                   102/103 *00000400
      *       -CGRPMUT       Groupe de mutation                104/105 *00000410
      *       -TRI           Groupe de vente ou de mutation    106/107 *00000420
      *                      Disponible                        108/120 *00000430
      *----------------------------------------------------------------*00000440
      *                                                                 00000450
       01  FTL113-DSECT.                                                00000460
           05  FTL113-NSOC             PIC  X(03).                      00000470
           05  FTL113-NDEPOT           PIC  X(03).                      00000480
           05  FTL113-DMVT             PIC  X(08).                      00000490
           05  FTL113-PERIM            PIC  X(05).                      00000500
           05  FTL113-CEQUIPE          PIC  X(05).                      00000510
           05  FTL113-CEQUIP           PIC  X(05).                      00000520
           05  FTL113-CLIVR1           PIC  X(10).                      00000530
           05  FTL113-COMPOS           PIC  X(01).                      00000540
           05  FTL113-NMAG             PIC  X(03).                      00000550
           05  FTL113-NVENTE           PIC  X(07).                      00000560
           05  FTL113-DDELIV           PIC  X(08).                      00000570
           05  FTL113-QEQUIP           PIC  9(03)V99   COMP-3.          00000580
           05  FTL113-QBLDEP           PIC  9(05)      COMP-3.          00000590
           05  FTL113-CRETOUR          PIC  X(05).                      00000600
           05  FTL113-CIMPUT           PIC  X(01).                      00000610
           05  FTL113-QBLRETIMP        PIC  9(05)      COMP-3.          00000620
           05  FTL113-QBLRETNIMP       PIC  9(05)      COMP-3.          00000630
           05  FTL113-QPIECESDEP       PIC  9(07)      COMP-3.          00000640
           05  FTL113-QPIECESRETIMP    PIC  9(07)      COMP-3.          00000650
           05  FTL113-QPIECESRETNIMP   PIC  9(07)      COMP-3.          00000660
           05  FTL113-CAGR1-ITL112     PIC  X(02).                      00000670
           05  FTL113-CAGR2-ITL112     PIC  X(02).                      00000680
           05  FTL113-CAGR1-ITL113     PIC  X(02).                      00000690
           05  FTL113-CAGR2-ITL113     PIC  X(02).                      00000700
           05  FTL113-CAGR1-ITL114     PIC  X(02).                      00000710
           05  FTL113-CAGR1-ITL115     PIC  X(02).                      00000720
           05  FTL113-CEDIT-ITL116     PIC  X(01).                      00000730
           05  FTL113-CGRPMAG          PIC  X(02).                      00000740
           05  FTL113-CGRPMUT          PIC  X(02).                      00000750
           05  FTL113-TRI-ITL115       PIC  X(02).                              
           05  FILLER                  PIC  X(13).                      00000760
      *                                                                 00000770
                                                                                
