      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU60   EMU60                                              00000020
      ***************************************************************** 00000030
       01   EMU60I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNLIEUI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLLIEUI   PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDEPF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCDEPI      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNDEPOTI  PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLDEPOTI  PIC X(20).                                      00000370
           02 FILLER  OCCURS   12 TIMES .                               00000380
      * code famille                                                    00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000400
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MCFAMI  PIC X(5).                                       00000430
      * code marque                                                     00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MCMARQI      PIC X(5).                                  00000480
      * reference                                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000500
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000510
             03 FILLER  PIC X(4).                                       00000520
             03 MREFI   PIC X(20).                                      00000530
      * codic                                                           00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCODICI      PIC X(7).                                  00000580
      * qte � muter                                                     00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000600
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000610
             03 FILLER  PIC X(4).                                       00000620
             03 MQTEI   PIC X(4).                                       00000630
      * n� document N�CDE                                               00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDOCL  COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MNDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNDOCF  PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MNDOCI  PIC X(7).                                       00000680
      * date mutation prochain                                          00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMUTL  COMP PIC S9(4).                                 00000700
      *--                                                                       
             03 MDMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDMUTF  PIC X.                                          00000710
             03 FILLER  PIC X(4).                                       00000720
             03 MDMUTI  PIC X(6).                                       00000730
      * prix vente unitaire                                             00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPVUNL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MPVUNL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPVUNF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MPVUNI  PIC X(9).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(15).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(58).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: EMU60   EMU60                                              00001040
      ***************************************************************** 00001050
       01   EMU60O REDEFINES EMU60I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MNSOCA    PIC X.                                          00001230
           02 MNSOCC    PIC X.                                          00001240
           02 MNSOCP    PIC X.                                          00001250
           02 MNSOCH    PIC X.                                          00001260
           02 MNSOCV    PIC X.                                          00001270
           02 MNSOCO    PIC X(3).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNLIEUA   PIC X.                                          00001300
           02 MNLIEUC   PIC X.                                          00001310
           02 MNLIEUP   PIC X.                                          00001320
           02 MNLIEUH   PIC X.                                          00001330
           02 MNLIEUV   PIC X.                                          00001340
           02 MNLIEUO   PIC X(3).                                       00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MLLIEUA   PIC X.                                          00001370
           02 MLLIEUC   PIC X.                                          00001380
           02 MLLIEUP   PIC X.                                          00001390
           02 MLLIEUH   PIC X.                                          00001400
           02 MLLIEUV   PIC X.                                          00001410
           02 MLLIEUO   PIC X(20).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MNSOCDEPA      PIC X.                                     00001440
           02 MNSOCDEPC PIC X.                                          00001450
           02 MNSOCDEPP PIC X.                                          00001460
           02 MNSOCDEPH PIC X.                                          00001470
           02 MNSOCDEPV PIC X.                                          00001480
           02 MNSOCDEPO      PIC X(3).                                  00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MNDEPOTA  PIC X.                                          00001510
           02 MNDEPOTC  PIC X.                                          00001520
           02 MNDEPOTP  PIC X.                                          00001530
           02 MNDEPOTH  PIC X.                                          00001540
           02 MNDEPOTV  PIC X.                                          00001550
           02 MNDEPOTO  PIC X(3).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MLDEPOTA  PIC X.                                          00001580
           02 MLDEPOTC  PIC X.                                          00001590
           02 MLDEPOTP  PIC X.                                          00001600
           02 MLDEPOTH  PIC X.                                          00001610
           02 MLDEPOTV  PIC X.                                          00001620
           02 MLDEPOTO  PIC X(20).                                      00001630
           02 FILLER  OCCURS   12 TIMES .                               00001640
      * code famille                                                    00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MCFAMA  PIC X.                                          00001670
             03 MCFAMC  PIC X.                                          00001680
             03 MCFAMP  PIC X.                                          00001690
             03 MCFAMH  PIC X.                                          00001700
             03 MCFAMV  PIC X.                                          00001710
             03 MCFAMO  PIC X(5).                                       00001720
      * code marque                                                     00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MCMARQA      PIC X.                                     00001750
             03 MCMARQC PIC X.                                          00001760
             03 MCMARQP PIC X.                                          00001770
             03 MCMARQH PIC X.                                          00001780
             03 MCMARQV PIC X.                                          00001790
             03 MCMARQO      PIC X(5).                                  00001800
      * reference                                                       00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MREFA   PIC X.                                          00001830
             03 MREFC   PIC X.                                          00001840
             03 MREFP   PIC X.                                          00001850
             03 MREFH   PIC X.                                          00001860
             03 MREFV   PIC X.                                          00001870
             03 MREFO   PIC X(20).                                      00001880
      * codic                                                           00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MCODICA      PIC X.                                     00001910
             03 MCODICC PIC X.                                          00001920
             03 MCODICP PIC X.                                          00001930
             03 MCODICH PIC X.                                          00001940
             03 MCODICV PIC X.                                          00001950
             03 MCODICO      PIC X(7).                                  00001960
      * qte � muter                                                     00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MQTEA   PIC X.                                          00001990
             03 MQTEC   PIC X.                                          00002000
             03 MQTEP   PIC X.                                          00002010
             03 MQTEH   PIC X.                                          00002020
             03 MQTEV   PIC X.                                          00002030
             03 MQTEO   PIC ZZZ9.                                       00002040
      * n� document N�CDE                                               00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MNDOCA  PIC X.                                          00002070
             03 MNDOCC  PIC X.                                          00002080
             03 MNDOCP  PIC X.                                          00002090
             03 MNDOCH  PIC X.                                          00002100
             03 MNDOCV  PIC X.                                          00002110
             03 MNDOCO  PIC X(7).                                       00002120
      * date mutation prochain                                          00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MDMUTA  PIC X.                                          00002150
             03 MDMUTC  PIC X.                                          00002160
             03 MDMUTP  PIC X.                                          00002170
             03 MDMUTH  PIC X.                                          00002180
             03 MDMUTV  PIC X.                                          00002190
             03 MDMUTO  PIC X(6).                                       00002200
      * prix vente unitaire                                             00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MPVUNA  PIC X.                                          00002230
             03 MPVUNC  PIC X.                                          00002240
             03 MPVUNP  PIC X.                                          00002250
             03 MPVUNH  PIC X.                                          00002260
             03 MPVUNV  PIC X.                                          00002270
             03 MPVUNO  PIC ZZZZZ9,99.                                  00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MZONCMDA  PIC X.                                          00002300
           02 MZONCMDC  PIC X.                                          00002310
           02 MZONCMDP  PIC X.                                          00002320
           02 MZONCMDH  PIC X.                                          00002330
           02 MZONCMDV  PIC X.                                          00002340
           02 MZONCMDO  PIC X(15).                                      00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MLIBERRA  PIC X.                                          00002370
           02 MLIBERRC  PIC X.                                          00002380
           02 MLIBERRP  PIC X.                                          00002390
           02 MLIBERRH  PIC X.                                          00002400
           02 MLIBERRV  PIC X.                                          00002410
           02 MLIBERRO  PIC X(58).                                      00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MCODTRAA  PIC X.                                          00002440
           02 MCODTRAC  PIC X.                                          00002450
           02 MCODTRAP  PIC X.                                          00002460
           02 MCODTRAH  PIC X.                                          00002470
           02 MCODTRAV  PIC X.                                          00002480
           02 MCODTRAO  PIC X(4).                                       00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MCICSA    PIC X.                                          00002510
           02 MCICSC    PIC X.                                          00002520
           02 MCICSP    PIC X.                                          00002530
           02 MCICSH    PIC X.                                          00002540
           02 MCICSV    PIC X.                                          00002550
           02 MCICSO    PIC X(5).                                       00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MNETNAMA  PIC X.                                          00002580
           02 MNETNAMC  PIC X.                                          00002590
           02 MNETNAMP  PIC X.                                          00002600
           02 MNETNAMH  PIC X.                                          00002610
           02 MNETNAMV  PIC X.                                          00002620
           02 MNETNAMO  PIC X(8).                                       00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MSCREENA  PIC X.                                          00002650
           02 MSCREENC  PIC X.                                          00002660
           02 MSCREENP  PIC X.                                          00002670
           02 MSCREENH  PIC X.                                          00002680
           02 MSCREENV  PIC X.                                          00002690
           02 MSCREENO  PIC X(4).                                       00002700
                                                                                
