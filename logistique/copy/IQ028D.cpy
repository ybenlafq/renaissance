      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IQ028D AU 26/04/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,20,BI,A,                          *        
      *                           30,05,BI,A,                          *        
      *                           35,05,BI,A,                          *        
      *                           40,05,BI,A,                          *        
      *                           45,03,BI,A,                          *        
      *                           48,07,BI,A,                          *        
      *                           55,08,BI,A,                          *        
      *                           63,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IQ028D.                                                        
            05 NOMETAT-IQ028D           PIC X(6) VALUE 'IQ028D'.                
            05 RUPTURES-IQ028D.                                                 
           10 IQ028D-NSOCIETE           PIC X(03).                      007  003
           10 IQ028D-LVPARAM            PIC X(20).                      010  020
           10 IQ028D-WSEQFAM            PIC X(05).                      030  005
           10 IQ028D-CMARQ              PIC X(05).                      035  005
           10 IQ028D-CAPPRO             PIC X(05).                      040  005
           10 IQ028D-STATCOMP           PIC X(03).                      045  003
           10 IQ028D-NCODIC             PIC X(07).                      048  007
           10 IQ028D-DCDE               PIC X(08).                      055  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IQ028D-SEQUENCE           PIC S9(04) COMP.                063  002
      *--                                                                       
           10 IQ028D-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IQ028D.                                                   
           10 IQ028D-CDEVISE            PIC X(03).                      065  003
           10 IQ028D-CDEVREF            PIC X(03).                      068  003
           10 IQ028D-CFAM               PIC X(05).                      071  005
           10 IQ028D-DTRECEP            PIC X(08).                      076  008
           10 IQ028D-ETOILE             PIC X(01).                      084  001
           10 IQ028D-JOURNEE            PIC X(10).                      085  010
           10 IQ028D-LIB1               PIC X(02).                      095  002
           10 IQ028D-LIB2               PIC X(02).                      097  002
           10 IQ028D-LREFFOURN          PIC X(20).                      099  020
           10 IQ028D-WSENSNF            PIC X(01).                      119  001
           10 IQ028D-DISPO-COMM         PIC S9(06)      COMP-3.         120  004
           10 IQ028D-INTER              PIC S9(04)V9(1) COMP-3.         124  003
           10 IQ028D-NBREART            PIC S9(07)      COMP-3.         127  004
           10 IQ028D-PSTDTTCEU          PIC S9(07)V9(2) COMP-3.         131  005
           10 IQ028D-PSTDTTCZ1          PIC S9(07)V9(2) COMP-3.         136  005
           10 IQ028D-QSTOCKDEP          PIC S9(07)      COMP-3.         141  004
           10 IQ028D-QSTOCKMAG          PIC S9(07)      COMP-3.         145  004
           10 IQ028D-TOTAL-COMM         PIC S9(06)      COMP-3.         149  004
            05 FILLER                      PIC X(360).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IQ028D-LONG           PIC S9(4)   COMP  VALUE +152.           
      *                                                                         
      *--                                                                       
        01  DSECT-IQ028D-LONG           PIC S9(4) COMP-5  VALUE +152.           
                                                                                
      *}                                                                        
