      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SUIVI DES MUTATIONS FUSIONNEES                                  00000020
      ***************************************************************** 00000030
       01   ELV05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDEPF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCDEPI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNOPL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSNOPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSNOPF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSNOPI    PIC X(9).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNMUTATIONL   COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MSNMUTATIONL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MSNMUTATIONF   PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSNMUTATIONI   PIC X(7).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDDESTOCKL    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MSDDESTOCKL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MSDDESTOCKF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSDDESTOCKI    PIC X(4).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNLIEUDESTL   COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MSNLIEUDESTL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MSNLIEUDESTF   PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSNLIEUDESTI   PIC X(6).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSHCHARGTL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MSHCHARGTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSHCHARGTF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSHCHARGTI     PIC X(5).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCSELARTL     COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MSCSELARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSCSELARTF     PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSCSELARTI     PIC X(5).                                  00000570
           02 MLIGNEI OCCURS   13 TIMES .                               00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOPL   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MNOPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNOPF   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNOPI   PIC X(9).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTATIONL  COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNMUTATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNMUTATIONF  PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNMUTATIONI  PIC X(7).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUDESTL  COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNLIEUDESTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNLIEUDESTF  PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNLIEUDESTI  PIC X(6).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDESTOCKL   COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDDESTOCKL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDDESTOCKF   PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDDESTOCKI   PIC X(4).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSELARTL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCSELARTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCSELARTF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCSELARTI    PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHCHARGTL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MHCHARGTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MHCHARGTF    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MHCHARGTI    PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(78).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SUIVI DES MUTATIONS FUSIONNEES                                  00001040
      ***************************************************************** 00001050
       01   ELV05O REDEFINES ELV05I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MPAGEA    PIC X.                                          00001230
           02 MPAGEC    PIC X.                                          00001240
           02 MPAGEP    PIC X.                                          00001250
           02 MPAGEH    PIC X.                                          00001260
           02 MPAGEV    PIC X.                                          00001270
           02 MPAGEO    PIC X(4).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNPAGEA   PIC X.                                          00001300
           02 MNPAGEC   PIC X.                                          00001310
           02 MNPAGEP   PIC X.                                          00001320
           02 MNPAGEH   PIC X.                                          00001330
           02 MNPAGEV   PIC X.                                          00001340
           02 MNPAGEO   PIC X(4).                                       00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MNSOCDEPA      PIC X.                                     00001370
           02 MNSOCDEPC PIC X.                                          00001380
           02 MNSOCDEPP PIC X.                                          00001390
           02 MNSOCDEPH PIC X.                                          00001400
           02 MNSOCDEPV PIC X.                                          00001410
           02 MNSOCDEPO      PIC X(3).                                  00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MNDEPOTA  PIC X.                                          00001440
           02 MNDEPOTC  PIC X.                                          00001450
           02 MNDEPOTP  PIC X.                                          00001460
           02 MNDEPOTH  PIC X.                                          00001470
           02 MNDEPOTV  PIC X.                                          00001480
           02 MNDEPOTO  PIC X(3).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MLLIEUA   PIC X.                                          00001510
           02 MLLIEUC   PIC X.                                          00001520
           02 MLLIEUP   PIC X.                                          00001530
           02 MLLIEUH   PIC X.                                          00001540
           02 MLLIEUV   PIC X.                                          00001550
           02 MLLIEUO   PIC X(20).                                      00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MSNOPA    PIC X.                                          00001580
           02 MSNOPC    PIC X.                                          00001590
           02 MSNOPP    PIC X.                                          00001600
           02 MSNOPH    PIC X.                                          00001610
           02 MSNOPV    PIC X.                                          00001620
           02 MSNOPO    PIC X(9).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MSNMUTATIONA   PIC X.                                     00001650
           02 MSNMUTATIONC   PIC X.                                     00001660
           02 MSNMUTATIONP   PIC X.                                     00001670
           02 MSNMUTATIONH   PIC X.                                     00001680
           02 MSNMUTATIONV   PIC X.                                     00001690
           02 MSNMUTATIONO   PIC X(7).                                  00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MSDDESTOCKA    PIC X.                                     00001720
           02 MSDDESTOCKC    PIC X.                                     00001730
           02 MSDDESTOCKP    PIC X.                                     00001740
           02 MSDDESTOCKH    PIC X.                                     00001750
           02 MSDDESTOCKV    PIC X.                                     00001760
           02 MSDDESTOCKO    PIC X(4).                                  00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MSNLIEUDESTA   PIC X.                                     00001790
           02 MSNLIEUDESTC   PIC X.                                     00001800
           02 MSNLIEUDESTP   PIC X.                                     00001810
           02 MSNLIEUDESTH   PIC X.                                     00001820
           02 MSNLIEUDESTV   PIC X.                                     00001830
           02 MSNLIEUDESTO   PIC X(6).                                  00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MSHCHARGTA     PIC X.                                     00001860
           02 MSHCHARGTC     PIC X.                                     00001870
           02 MSHCHARGTP     PIC X.                                     00001880
           02 MSHCHARGTH     PIC X.                                     00001890
           02 MSHCHARGTV     PIC X.                                     00001900
           02 MSHCHARGTO     PIC X(5).                                  00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MSCSELARTA     PIC X.                                     00001930
           02 MSCSELARTC     PIC X.                                     00001940
           02 MSCSELARTP     PIC X.                                     00001950
           02 MSCSELARTH     PIC X.                                     00001960
           02 MSCSELARTV     PIC X.                                     00001970
           02 MSCSELARTO     PIC X(5).                                  00001980
           02 MLIGNEO OCCURS   13 TIMES .                               00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MNOPA   PIC X.                                          00002010
             03 MNOPC   PIC X.                                          00002020
             03 MNOPP   PIC X.                                          00002030
             03 MNOPH   PIC X.                                          00002040
             03 MNOPV   PIC X.                                          00002050
             03 MNOPO   PIC X(9).                                       00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MNMUTATIONA  PIC X.                                     00002080
             03 MNMUTATIONC  PIC X.                                     00002090
             03 MNMUTATIONP  PIC X.                                     00002100
             03 MNMUTATIONH  PIC X.                                     00002110
             03 MNMUTATIONV  PIC X.                                     00002120
             03 MNMUTATIONO  PIC X(7).                                  00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MNLIEUDESTA  PIC X.                                     00002150
             03 MNLIEUDESTC  PIC X.                                     00002160
             03 MNLIEUDESTP  PIC X.                                     00002170
             03 MNLIEUDESTH  PIC X.                                     00002180
             03 MNLIEUDESTV  PIC X.                                     00002190
             03 MNLIEUDESTO  PIC X(6).                                  00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MDDESTOCKA   PIC X.                                     00002220
             03 MDDESTOCKC   PIC X.                                     00002230
             03 MDDESTOCKP   PIC X.                                     00002240
             03 MDDESTOCKH   PIC X.                                     00002250
             03 MDDESTOCKV   PIC X.                                     00002260
             03 MDDESTOCKO   PIC X(4).                                  00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MCSELARTA    PIC X.                                     00002290
             03 MCSELARTC    PIC X.                                     00002300
             03 MCSELARTP    PIC X.                                     00002310
             03 MCSELARTH    PIC X.                                     00002320
             03 MCSELARTV    PIC X.                                     00002330
             03 MCSELARTO    PIC X(5).                                  00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MHCHARGTA    PIC X.                                     00002360
             03 MHCHARGTC    PIC X.                                     00002370
             03 MHCHARGTP    PIC X.                                     00002380
             03 MHCHARGTH    PIC X.                                     00002390
             03 MHCHARGTV    PIC X.                                     00002400
             03 MHCHARGTO    PIC X(5).                                  00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(78).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
