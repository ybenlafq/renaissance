      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE CD3500                       
      ******************************************************************        
      *                                                                         
       CLEF-CD3500             SECTION.                                         
      *                                                                         
           MOVE 'RVCD3500          '       TO   TABLE-NAME.                     
           MOVE 'CD3500'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-CD3500. EXIT.                                                   
                EJECT                                                           
                                                                                
