      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD10   EGD10                                              00000020
      ***************************************************************** 00000030
       01   EGD17I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSOCIETEI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTREPOTL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MENTREPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MENTREPOTF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MENTREPOTI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURNEEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MJOURNEEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MJOURNEEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MJOURNEEI      PIC X(10).                                 00000290
           02 M172I OCCURS   12 TIMES .                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRAFALEL    COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MNRAFALEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNRAFALEF    PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNRAFALEI    PIC X(3).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCL   COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSOCF   PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MSOCI   PIC X(3).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIEUF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MLIEUI  PIC X(3).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLIBELLEI    PIC X(20).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMSTL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MMSTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMSTF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MMSTI   PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSML    COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MSML COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MSMF    PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MSMI    PIC X.                                          00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELARTL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MSELARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELARTF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MSELARTI     PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVOLL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MQVOLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQVOLF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MQVOLI  PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBLL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MQNBLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQNBLF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQNBLI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBPL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MQNBPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQNBPF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQNBPI  PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSATELITL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MSATELITL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSATELITF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSATELITI    PIC X(2).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCASEL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MCASEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCASEF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCASEI  PIC X(3).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(15).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(58).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: EGD10   EGD10                                              00001040
      ***************************************************************** 00001050
       01   EGD17O REDEFINES EGD17I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MPAGEA    PIC X.                                          00001230
           02 MPAGEC    PIC X.                                          00001240
           02 MPAGEP    PIC X.                                          00001250
           02 MPAGEH    PIC X.                                          00001260
           02 MPAGEV    PIC X.                                          00001270
           02 MPAGEO    PIC X(2).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MSOCIETEA      PIC X.                                     00001300
           02 MSOCIETEC PIC X.                                          00001310
           02 MSOCIETEP PIC X.                                          00001320
           02 MSOCIETEH PIC X.                                          00001330
           02 MSOCIETEV PIC X.                                          00001340
           02 MSOCIETEO      PIC X(3).                                  00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MENTREPOTA     PIC X.                                     00001370
           02 MENTREPOTC     PIC X.                                     00001380
           02 MENTREPOTP     PIC X.                                     00001390
           02 MENTREPOTH     PIC X.                                     00001400
           02 MENTREPOTV     PIC X.                                     00001410
           02 MENTREPOTO     PIC X(3).                                  00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MJOURNEEA      PIC X.                                     00001440
           02 MJOURNEEC PIC X.                                          00001450
           02 MJOURNEEP PIC X.                                          00001460
           02 MJOURNEEH PIC X.                                          00001470
           02 MJOURNEEV PIC X.                                          00001480
           02 MJOURNEEO      PIC X(10).                                 00001490
           02 M172O OCCURS   12 TIMES .                                 00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MNRAFALEA    PIC X.                                     00001520
             03 MNRAFALEC    PIC X.                                     00001530
             03 MNRAFALEP    PIC X.                                     00001540
             03 MNRAFALEH    PIC X.                                     00001550
             03 MNRAFALEV    PIC X.                                     00001560
             03 MNRAFALEO    PIC X(3).                                  00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MSOCA   PIC X.                                          00001590
             03 MSOCC   PIC X.                                          00001600
             03 MSOCP   PIC X.                                          00001610
             03 MSOCH   PIC X.                                          00001620
             03 MSOCV   PIC X.                                          00001630
             03 MSOCO   PIC X(3).                                       00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MLIEUA  PIC X.                                          00001660
             03 MLIEUC  PIC X.                                          00001670
             03 MLIEUP  PIC X.                                          00001680
             03 MLIEUH  PIC X.                                          00001690
             03 MLIEUV  PIC X.                                          00001700
             03 MLIEUO  PIC X(3).                                       00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MLIBELLEA    PIC X.                                     00001730
             03 MLIBELLEC    PIC X.                                     00001740
             03 MLIBELLEP    PIC X.                                     00001750
             03 MLIBELLEH    PIC X.                                     00001760
             03 MLIBELLEV    PIC X.                                     00001770
             03 MLIBELLEO    PIC X(20).                                 00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MMSTA   PIC X.                                          00001800
             03 MMSTC   PIC X.                                          00001810
             03 MMSTP   PIC X.                                          00001820
             03 MMSTH   PIC X.                                          00001830
             03 MMSTV   PIC X.                                          00001840
             03 MMSTO   PIC X(5).                                       00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MSMA    PIC X.                                          00001870
             03 MSMC    PIC X.                                          00001880
             03 MSMP    PIC X.                                          00001890
             03 MSMH    PIC X.                                          00001900
             03 MSMV    PIC X.                                          00001910
             03 MSMO    PIC X.                                          00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MSELARTA     PIC X.                                     00001940
             03 MSELARTC     PIC X.                                     00001950
             03 MSELARTP     PIC X.                                     00001960
             03 MSELARTH     PIC X.                                     00001970
             03 MSELARTV     PIC X.                                     00001980
             03 MSELARTO     PIC X(5).                                  00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MQVOLA  PIC X.                                          00002010
             03 MQVOLC  PIC X.                                          00002020
             03 MQVOLP  PIC X.                                          00002030
             03 MQVOLH  PIC X.                                          00002040
             03 MQVOLV  PIC X.                                          00002050
             03 MQVOLO  PIC X(5).                                       00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MQNBLA  PIC X.                                          00002080
             03 MQNBLC  PIC X.                                          00002090
             03 MQNBLP  PIC X.                                          00002100
             03 MQNBLH  PIC X.                                          00002110
             03 MQNBLV  PIC X.                                          00002120
             03 MQNBLO  PIC X(4).                                       00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MQNBPA  PIC X.                                          00002150
             03 MQNBPC  PIC X.                                          00002160
             03 MQNBPP  PIC X.                                          00002170
             03 MQNBPH  PIC X.                                          00002180
             03 MQNBPV  PIC X.                                          00002190
             03 MQNBPO  PIC X(5).                                       00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MSATELITA    PIC X.                                     00002220
             03 MSATELITC    PIC X.                                     00002230
             03 MSATELITP    PIC X.                                     00002240
             03 MSATELITH    PIC X.                                     00002250
             03 MSATELITV    PIC X.                                     00002260
             03 MSATELITO    PIC X(2).                                  00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MCASEA  PIC X.                                          00002290
             03 MCASEC  PIC X.                                          00002300
             03 MCASEP  PIC X.                                          00002310
             03 MCASEH  PIC X.                                          00002320
             03 MCASEV  PIC X.                                          00002330
             03 MCASEO  PIC X(3).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MZONCMDA  PIC X.                                          00002360
           02 MZONCMDC  PIC X.                                          00002370
           02 MZONCMDP  PIC X.                                          00002380
           02 MZONCMDH  PIC X.                                          00002390
           02 MZONCMDV  PIC X.                                          00002400
           02 MZONCMDO  PIC X(15).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(58).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
