      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGQ03 (TGQ00 -> MENU)    TR: GQ00  *    00002201
      *          GESTION DES PARAMETRES GENERAUX LIVRAISON         *    00002301
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *  TRANSACTION GQ03 : AFFECTATION DES EQUIPES DE LIVRAISON   *    00411001
      *  ( MODE DE DELIVRANCE / ZONE LIVRAISON / DATE EFFET //     *    00411101
      *    FAMILLES / EQUIPES DE LIVRAISON )                       *    00411201
      *                                                                 00412000
          02 COMM-GQ03-APPLI REDEFINES COMM-GQ00-APPLI.                 00420001
      *------------------------------ ZONE DONNEES TGQ03                00510001
             03 COMM-GQ03-LIBRE          PIC X(2492).                   00511002
             03 COMM-GQ03-DONNEES-1-TGQ03.                              00520001
      *------------------------------ CODE FONCTION                     00550001
                04 COMM-GQ03-FONCT             PIC X(3).                00560001
      *------------------------------ CODE MODE DE DELIVRANCE           00560101
                04 COMM-GQ03-CMODDEL           PIC X(3).                00560201
      *------------------------------ LIBELLE MODE DE DELIVRANCE        00560301
                04 COMM-GQ03-LMODDEL           PIC X(20).               00560401
      *------------------------------ CODE ZONE DE LIVRAISON            00561001
                04 COMM-GQ03-CPERIM            PIC X(5).                00562001
      *------------------------------ LIBELLE ZONE DE LIVRAISON         00563001
                04 COMM-GQ03-LPERIM            PIC X(20).               00564001
      *------------------------------ DATE D'EFFET                      00570001
                04 COMM-GQ03-DEFFET            PIC X(08).               00580001
      *------------------------------ DERNIER IND-TS                    00742401
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GQ03-PAGSUI            PIC S9(4) COMP.          00742501
      *--                                                                       
                04 COMM-GQ03-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN IND-TS                     00742601
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GQ03-PAGENC            PIC S9(4) COMP.          00742701
      *--                                                                       
                04 COMM-GQ03-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00742801
                04 COMM-GQ03-NUMPAG            PIC 9(3).                00742901
      *------------------------------ INDICATEUR FIN DE TABLE           00743001
                04 COMM-GQ03-INDPAG            PIC 9.                   00743101
      *------------------------------ ZONE CONFIRMATION PF3             00743201
                04 COMM-GQ03-CONF-PF3          PIC 9(1).                00743301
      *------------------------------ ZONE CONFIRMATION PF4             00743401
                04 COMM-GQ03-CONF-PF4          PIC 9(1).                00743501
      *------------------------------ TABLE N� TS PR PAGINATION         00743601
                04 COMM-GQ03-TABTS          OCCURS 100.                 00743701
      *------------------------------ 1ERS IND TS DES PAGES <=> FAMILLE 00743801
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-GQ03-NUMTS          PIC S9(4) COMP.          00743901
      *--                                                                       
                   05 COMM-GQ03-NUMTS          PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS               00744001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GQ03-IND-RECH-TS    PIC S9(4) COMP.             00745001
      *--                                                                       
                04 COMM-GQ03-IND-RECH-TS    PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND TS MAX                        00746001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GQ03-IND-TS-MAX     PIC S9(4) COMP.             00747001
      *--                                                                       
                04 COMM-GQ03-IND-TS-MAX     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ MAJ OK SUR RTGQ04 => VALID = '1'  00748001
                04 COMM-GQ03-VALID-RTGQ04   PIC  9.                     00749001
      *------------------------------ ZONE DONNEES 2 TGQ03              00760001
             03 COMM-GQ03-DONNEES-2-TGQ03.                              00761001
      *------------------------------ ZONE FAMILLES PAGE                00770001
                04 COMM-GQ03-TABFAM.                                    00780001
      *------------------------------ ZONE LIGNES FAMILLES              00790001
                   05 COMM-GQ03-LIGFAM  OCCURS 13.                      00800001
      *------------------------------ CODE FAMILLE                      00810001
                      06 COMM-GQ03-CFAM           PIC X(5).             00820001
      *------------------------------ LIBELLE FAMILLE                   00830001
                      06 COMM-GQ03-LFAM           PIC X(20).            00840001
      *------------------------------ CODES D'AFFILIATION               00850001
                      06 COMM-GQ03-ZONEQUIP       OCCURS 5.             00860001
      *------------------------------ CODE SOCIETE D'AFFILIATION        00870001
                         07 COMM-GQ03-CEQUIP      PIC X(05).            00880001
      *------------------------------ ZONE DONNEES 3 TGQ03              01650001
             03 COMM-GQ03-DONNEES-3-TGQ03.                              01651001
      *------------------------------ ZONES SWAP                        01670001
                   05 COMM-GQ03-ATTR           PIC X OCCURS 300.        01680001
      *------------------------------ ZONE LIBRE                        01710001
             03 COMM-GQ03-NSOCIETE       PIC X(0003).                   01711001
             03 COMM-GQ03-CADRE          PIC X(0005).                   01712002
      ***************************************************************** 02170001
                                                                                
