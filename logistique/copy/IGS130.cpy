      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGS130 AU 18/09/2002  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,05,BI,A,                          *        
      *                           26,05,BI,A,                          *        
      *                           31,05,BI,A,                          *        
      *                           36,20,BI,A,                          *        
      *                           56,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGS130.                                                        
            05 NOMETAT-IGS130           PIC X(6) VALUE 'IGS130'.                
            05 RUPTURES-IGS130.                                                 
           10 IGS130-NSOC               PIC X(03).                      007  003
           10 IGS130-NDEPOT             PIC X(03).                      010  003
           10 IGS130-ELATLM             PIC X(03).                      013  003
           10 IGS130-BRUNBLANC          PIC X(05).                      016  005
           10 IGS130-CHEFP              PIC X(05).                      021  005
           10 IGS130-CFAM               PIC X(05).                      026  005
           10 IGS130-CMARQ              PIC X(05).                      031  005
           10 IGS130-LREFFOURN          PIC X(20).                      036  020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGS130-SEQUENCE           PIC S9(04) COMP.                056  002
      *--                                                                       
           10 IGS130-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGS130.                                                   
           10 IGS130-CAPPRO             PIC X(05).                      058  005
           10 IGS130-NCODIC             PIC X(07).                      063  007
           10 IGS130-DISPO              PIC S9(05)      COMP-3.         070  003
           10 IGS130-PSTDTTC            PIC S9(07)V9(2) COMP-3.         073  005
           10 IGS130-QSTOCK             PIC S9(05)      COMP-3.         078  003
           10 IGS130-DATE               PIC X(08).                      081  008
            05 FILLER                      PIC X(424).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGS130-LONG           PIC S9(4)   COMP  VALUE +088.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGS130-LONG           PIC S9(4) COMP-5  VALUE +088.           
                                                                                
      *}                                                                        
