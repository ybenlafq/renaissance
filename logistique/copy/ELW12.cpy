      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SUIVI DES COMMANDES LM7                                         00000020
      ***************************************************************** 00000030
       01   ELW12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDEPF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCDEPI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDLIVRL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSDLIVRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSDLIVRF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSDLIVRI  PIC X(4).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNCODICL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MSNCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSNCODICF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSNCODICI      PIC X(7).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNCDEL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSNCDEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSNCDEF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSNCDEI   PIC X(7).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSETATL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MSETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSETATF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSETATI   PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDTSFTL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MSDTSFTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSDTSFTF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSDTSFTI  PIC X(4).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDRCPTL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSDRCPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSDRCPTF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSDRCPTI  PIC X(4).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNRECQUAIL    COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MSNRECQUAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MSNRECQUAIF    PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSNRECQUAII    PIC X(7).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNENTCDEL     COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MSNENTCDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSNENTCDEF     PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSNENTCDEI     PIC X(5).                                  00000650
           02 MLIGNEI OCCURS   12 TIMES .                               00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDLIVRL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MDLIVRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDLIVRF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MDLIVRI      PIC X(4).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNCODICI     PIC X(7).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MNCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCDEF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNCDEI  PIC X(7).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQENVOIL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQENVOIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQENVOIF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQENVOII     PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 METATL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 METATL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 METATF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 METATI  PIC X.                                          00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDTSFTL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDTSFTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDTSFTF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDTSFTI      PIC X(4).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQAFFECTEL   COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQAFFECTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQAFFECTEF   PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQAFFECTEI   PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRCPTL      COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MDRCPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDRCPTF      PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MDRCPTI      PIC X(4).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRECQUAIL   COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MNRECQUAIL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNRECQUAIF   PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MNRECQUAII   PIC X(7).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRECQUAIL   COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MQRECQUAIL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQRECQUAIF   PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQRECQUAII   PIC X(5).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRANGEEL    COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MQRANGEEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQRANGEEF    PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MQRANGEEI    PIC X(5).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTCDEL    COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MNENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTCDEF    PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MNENTCDEI    PIC X(5).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTCDEL    COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MLENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLENTCDEF    PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MLENTCDEI    PIC X(8).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MLIBERRI  PIC X(78).                                      00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCODTRAI  PIC X(4).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCICSI    PIC X(5).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MNETNAMI  PIC X(8).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MSCREENI  PIC X(4).                                       00001380
      ***************************************************************** 00001390
      * SUIVI DES COMMANDES LM7                                         00001400
      ***************************************************************** 00001410
       01   ELW12O REDEFINES ELW12I.                                    00001420
           02 FILLER    PIC X(12).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MDATJOUA  PIC X.                                          00001450
           02 MDATJOUC  PIC X.                                          00001460
           02 MDATJOUP  PIC X.                                          00001470
           02 MDATJOUH  PIC X.                                          00001480
           02 MDATJOUV  PIC X.                                          00001490
           02 MDATJOUO  PIC X(10).                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MTIMJOUA  PIC X.                                          00001520
           02 MTIMJOUC  PIC X.                                          00001530
           02 MTIMJOUP  PIC X.                                          00001540
           02 MTIMJOUH  PIC X.                                          00001550
           02 MTIMJOUV  PIC X.                                          00001560
           02 MTIMJOUO  PIC X(5).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MPAGEA    PIC X.                                          00001590
           02 MPAGEC    PIC X.                                          00001600
           02 MPAGEP    PIC X.                                          00001610
           02 MPAGEH    PIC X.                                          00001620
           02 MPAGEV    PIC X.                                          00001630
           02 MPAGEO    PIC X(4).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MNPAGEA   PIC X.                                          00001660
           02 MNPAGEC   PIC X.                                          00001670
           02 MNPAGEP   PIC X.                                          00001680
           02 MNPAGEH   PIC X.                                          00001690
           02 MNPAGEV   PIC X.                                          00001700
           02 MNPAGEO   PIC X(4).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MNSOCDEPA      PIC X.                                     00001730
           02 MNSOCDEPC PIC X.                                          00001740
           02 MNSOCDEPP PIC X.                                          00001750
           02 MNSOCDEPH PIC X.                                          00001760
           02 MNSOCDEPV PIC X.                                          00001770
           02 MNSOCDEPO      PIC X(3).                                  00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MNDEPOTA  PIC X.                                          00001800
           02 MNDEPOTC  PIC X.                                          00001810
           02 MNDEPOTP  PIC X.                                          00001820
           02 MNDEPOTH  PIC X.                                          00001830
           02 MNDEPOTV  PIC X.                                          00001840
           02 MNDEPOTO  PIC X(3).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MLLIEUA   PIC X.                                          00001870
           02 MLLIEUC   PIC X.                                          00001880
           02 MLLIEUP   PIC X.                                          00001890
           02 MLLIEUH   PIC X.                                          00001900
           02 MLLIEUV   PIC X.                                          00001910
           02 MLLIEUO   PIC X(20).                                      00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSDLIVRA  PIC X.                                          00001940
           02 MSDLIVRC  PIC X.                                          00001950
           02 MSDLIVRP  PIC X.                                          00001960
           02 MSDLIVRH  PIC X.                                          00001970
           02 MSDLIVRV  PIC X.                                          00001980
           02 MSDLIVRO  PIC X(4).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MSNCODICA      PIC X.                                     00002010
           02 MSNCODICC PIC X.                                          00002020
           02 MSNCODICP PIC X.                                          00002030
           02 MSNCODICH PIC X.                                          00002040
           02 MSNCODICV PIC X.                                          00002050
           02 MSNCODICO      PIC X(7).                                  00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MSNCDEA   PIC X.                                          00002080
           02 MSNCDEC   PIC X.                                          00002090
           02 MSNCDEP   PIC X.                                          00002100
           02 MSNCDEH   PIC X.                                          00002110
           02 MSNCDEV   PIC X.                                          00002120
           02 MSNCDEO   PIC X(7).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MSETATA   PIC X.                                          00002150
           02 MSETATC   PIC X.                                          00002160
           02 MSETATP   PIC X.                                          00002170
           02 MSETATH   PIC X.                                          00002180
           02 MSETATV   PIC X.                                          00002190
           02 MSETATO   PIC X.                                          00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MSDTSFTA  PIC X.                                          00002220
           02 MSDTSFTC  PIC X.                                          00002230
           02 MSDTSFTP  PIC X.                                          00002240
           02 MSDTSFTH  PIC X.                                          00002250
           02 MSDTSFTV  PIC X.                                          00002260
           02 MSDTSFTO  PIC X(4).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MSDRCPTA  PIC X.                                          00002290
           02 MSDRCPTC  PIC X.                                          00002300
           02 MSDRCPTP  PIC X.                                          00002310
           02 MSDRCPTH  PIC X.                                          00002320
           02 MSDRCPTV  PIC X.                                          00002330
           02 MSDRCPTO  PIC X(4).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MSNRECQUAIA    PIC X.                                     00002360
           02 MSNRECQUAIC    PIC X.                                     00002370
           02 MSNRECQUAIP    PIC X.                                     00002380
           02 MSNRECQUAIH    PIC X.                                     00002390
           02 MSNRECQUAIV    PIC X.                                     00002400
           02 MSNRECQUAIO    PIC X(7).                                  00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MSNENTCDEA     PIC X.                                     00002430
           02 MSNENTCDEC     PIC X.                                     00002440
           02 MSNENTCDEP     PIC X.                                     00002450
           02 MSNENTCDEH     PIC X.                                     00002460
           02 MSNENTCDEV     PIC X.                                     00002470
           02 MSNENTCDEO     PIC X(5).                                  00002480
           02 MLIGNEO OCCURS   12 TIMES .                               00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MDLIVRA      PIC X.                                     00002510
             03 MDLIVRC PIC X.                                          00002520
             03 MDLIVRP PIC X.                                          00002530
             03 MDLIVRH PIC X.                                          00002540
             03 MDLIVRV PIC X.                                          00002550
             03 MDLIVRO      PIC X(4).                                  00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MNCODICA     PIC X.                                     00002580
             03 MNCODICC     PIC X.                                     00002590
             03 MNCODICP     PIC X.                                     00002600
             03 MNCODICH     PIC X.                                     00002610
             03 MNCODICV     PIC X.                                     00002620
             03 MNCODICO     PIC X(7).                                  00002630
             03 FILLER       PIC X(2).                                  00002640
             03 MNCDEA  PIC X.                                          00002650
             03 MNCDEC  PIC X.                                          00002660
             03 MNCDEP  PIC X.                                          00002670
             03 MNCDEH  PIC X.                                          00002680
             03 MNCDEV  PIC X.                                          00002690
             03 MNCDEO  PIC X(7).                                       00002700
             03 FILLER       PIC X(2).                                  00002710
             03 MQENVOIA     PIC X.                                     00002720
             03 MQENVOIC     PIC X.                                     00002730
             03 MQENVOIP     PIC X.                                     00002740
             03 MQENVOIH     PIC X.                                     00002750
             03 MQENVOIV     PIC X.                                     00002760
             03 MQENVOIO     PIC X(5).                                  00002770
             03 FILLER       PIC X(2).                                  00002780
             03 METATA  PIC X.                                          00002790
             03 METATC  PIC X.                                          00002800
             03 METATP  PIC X.                                          00002810
             03 METATH  PIC X.                                          00002820
             03 METATV  PIC X.                                          00002830
             03 METATO  PIC X.                                          00002840
             03 FILLER       PIC X(2).                                  00002850
             03 MDTSFTA      PIC X.                                     00002860
             03 MDTSFTC PIC X.                                          00002870
             03 MDTSFTP PIC X.                                          00002880
             03 MDTSFTH PIC X.                                          00002890
             03 MDTSFTV PIC X.                                          00002900
             03 MDTSFTO      PIC X(4).                                  00002910
             03 FILLER       PIC X(2).                                  00002920
             03 MQAFFECTEA   PIC X.                                     00002930
             03 MQAFFECTEC   PIC X.                                     00002940
             03 MQAFFECTEP   PIC X.                                     00002950
             03 MQAFFECTEH   PIC X.                                     00002960
             03 MQAFFECTEV   PIC X.                                     00002970
             03 MQAFFECTEO   PIC X(5).                                  00002980
             03 FILLER       PIC X(2).                                  00002990
             03 MDRCPTA      PIC X.                                     00003000
             03 MDRCPTC PIC X.                                          00003010
             03 MDRCPTP PIC X.                                          00003020
             03 MDRCPTH PIC X.                                          00003030
             03 MDRCPTV PIC X.                                          00003040
             03 MDRCPTO      PIC X(4).                                  00003050
             03 FILLER       PIC X(2).                                  00003060
             03 MNRECQUAIA   PIC X.                                     00003070
             03 MNRECQUAIC   PIC X.                                     00003080
             03 MNRECQUAIP   PIC X.                                     00003090
             03 MNRECQUAIH   PIC X.                                     00003100
             03 MNRECQUAIV   PIC X.                                     00003110
             03 MNRECQUAIO   PIC X(7).                                  00003120
             03 FILLER       PIC X(2).                                  00003130
             03 MQRECQUAIA   PIC X.                                     00003140
             03 MQRECQUAIC   PIC X.                                     00003150
             03 MQRECQUAIP   PIC X.                                     00003160
             03 MQRECQUAIH   PIC X.                                     00003170
             03 MQRECQUAIV   PIC X.                                     00003180
             03 MQRECQUAIO   PIC X(5).                                  00003190
             03 FILLER       PIC X(2).                                  00003200
             03 MQRANGEEA    PIC X.                                     00003210
             03 MQRANGEEC    PIC X.                                     00003220
             03 MQRANGEEP    PIC X.                                     00003230
             03 MQRANGEEH    PIC X.                                     00003240
             03 MQRANGEEV    PIC X.                                     00003250
             03 MQRANGEEO    PIC X(5).                                  00003260
             03 FILLER       PIC X(2).                                  00003270
             03 MNENTCDEA    PIC X.                                     00003280
             03 MNENTCDEC    PIC X.                                     00003290
             03 MNENTCDEP    PIC X.                                     00003300
             03 MNENTCDEH    PIC X.                                     00003310
             03 MNENTCDEV    PIC X.                                     00003320
             03 MNENTCDEO    PIC X(5).                                  00003330
             03 FILLER       PIC X(2).                                  00003340
             03 MLENTCDEA    PIC X.                                     00003350
             03 MLENTCDEC    PIC X.                                     00003360
             03 MLENTCDEP    PIC X.                                     00003370
             03 MLENTCDEH    PIC X.                                     00003380
             03 MLENTCDEV    PIC X.                                     00003390
             03 MLENTCDEO    PIC X(8).                                  00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MLIBERRA  PIC X.                                          00003420
           02 MLIBERRC  PIC X.                                          00003430
           02 MLIBERRP  PIC X.                                          00003440
           02 MLIBERRH  PIC X.                                          00003450
           02 MLIBERRV  PIC X.                                          00003460
           02 MLIBERRO  PIC X(78).                                      00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MCODTRAA  PIC X.                                          00003490
           02 MCODTRAC  PIC X.                                          00003500
           02 MCODTRAP  PIC X.                                          00003510
           02 MCODTRAH  PIC X.                                          00003520
           02 MCODTRAV  PIC X.                                          00003530
           02 MCODTRAO  PIC X(4).                                       00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MCICSA    PIC X.                                          00003560
           02 MCICSC    PIC X.                                          00003570
           02 MCICSP    PIC X.                                          00003580
           02 MCICSH    PIC X.                                          00003590
           02 MCICSV    PIC X.                                          00003600
           02 MCICSO    PIC X(5).                                       00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MNETNAMA  PIC X.                                          00003630
           02 MNETNAMC  PIC X.                                          00003640
           02 MNETNAMP  PIC X.                                          00003650
           02 MNETNAMH  PIC X.                                          00003660
           02 MNETNAMV  PIC X.                                          00003670
           02 MNETNAMO  PIC X(8).                                       00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MSCREENA  PIC X.                                          00003700
           02 MSCREENC  PIC X.                                          00003710
           02 MSCREENP  PIC X.                                          00003720
           02 MSCREENH  PIC X.                                          00003730
           02 MSCREENV  PIC X.                                          00003740
           02 MSCREENO  PIC X(4).                                       00003750
                                                                                
