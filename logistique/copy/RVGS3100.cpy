      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGS3100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS3100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGS3100.                                                            
           02  GS31-NSOCMAG                                                     
               PIC X(0003).                                                     
           02  GS31-NMAG                                                        
               PIC X(0003).                                                     
           02  GS31-NCODIC                                                      
               PIC X(0007).                                                     
           02  GS31-WSTOCKMASQ                                                  
               PIC X(0001).                                                     
           02  GS31-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  GS31-QSTOCK                                                      
               PIC S9(5) COMP-3.                                                
           02  GS31-QSTOCKRES                                                   
               PIC S9(5) COMP-3.                                                
           02  GS31-DMAJSTOCK                                                   
               PIC X(0008).                                                     
           02  GS31-WARTINC                                                     
               PIC X(0001).                                                     
           02  GS31-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGS3100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGS3100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS31-NSOCMAG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS31-NSOCMAG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS31-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS31-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS31-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS31-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS31-WSTOCKMASQ-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS31-WSTOCKMASQ-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS31-NSSLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS31-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS31-QSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS31-QSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS31-QSTOCKRES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS31-QSTOCKRES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS31-DMAJSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS31-DMAJSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS31-WARTINC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS31-WARTINC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS31-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS31-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
