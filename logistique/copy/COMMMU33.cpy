      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00000010
      * COMMAREA SPECIFIQUE PRG TMU33 (TMU20 -> MENU)    TR: MU20  *    00000020
      *               REGUL APPROS-STOCKS MAGASINS                      00000030
      *                                                                 00000040
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3614  00000050
      *                                                                 00000060
      *        TRANSACTION MU33 : CONSULT DES ENTETES DE MUTATION       00000070
      *                                                                 00000080
          04 COMM-MU33-APPLI REDEFINES COMM-MU32-LIBRE.                 00000090
      *------------------------------ ZONE DONNEES TMU33                00000091
             05 COMM-MU33-DONNEES-TMU33.                                00000092
      *------------------------------                                   00000093
                 06  COMM-MU33-PAGESUIV        PIC X(01).               00000094
                 06  COMM-MU33-NPAGE           PIC 9(02).               00000095
                 06  COMM-MU33-DDEBUT-ET       PIC X(08).               00000096
                 06  COMM-MU33-TABLE-CLE.                               00000120
                     10  COMM-MU33-LAST-CLE  OCCURS 125.                00000121
                         15  COMM-MU33-LASTMUT     PIC X(07).           00000130
                         15  COMM-MU33-LASTDATE    PIC X(08).           00000131
                 06  COMM-MU33-TABLE-PAGE.                              00000132
                   08  COMM-MU33-LIGNE OCCURS 13.                       00000133
                     10  COMM-MU33-NMUTATION       PIC X(07).           00000140
                     10  COMM-MU33-DDEBSAIS        PIC X(04).           00000150
                     10  COMM-MU33-DFINSAIS        PIC X(04).           00000160
                     10  COMM-MU33-DDESTOCK        PIC X(04).           00000170
                     10  COMM-MU33-LIBJD           PIC X(02).           00000171
                     10  COMM-MU33-DMUTATION       PIC X(04).           00000180
                     10  COMM-MU33-DCHARGT         PIC X(04).           00000181
                     10  COMM-MU33-HCHARGT         PIC X(05).           00000181
                     10  COMM-MU33-PROPERMIS       PIC X(01).           00000190
                     10  COMM-MU33-DHEURMUT        PIC 9(02).           00000200
                     10  COMM-MU33-DMINUMUT        PIC 9(02).           00000210
                     10  COMM-MU33-QNBPIECES       PIC 9(05).           00000221
                     10  COMM-MU33-QNBM3QUOTA      PIC 9(04)V9.         00000230
                     10  COMM-MU33-QNBPQUOTA       PIC 9(05).           00000240
                     10  COMM-MU33-LHEURLIMIT      PIC X(05).           00000250
                 06  COMM-MU33-LIBRE               PIC X(2000).         00000260
      *                                                                 00000291
                                                                                
