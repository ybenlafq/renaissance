      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00000010
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *00000020
      ******************************************************************00000030
      *                                                                *00000040
      *  PROJET     :                                                   00000050
      *  PROGRAMME  : MLV05                                            *00000060
      *  TITRE      : COMMAREA DES VALIDATIONS DE TRANSIT RECEPTION    *00000070
      *  LONGUEUR   : 4096C                                            *00000100
      *                                                                *00000110
      ******************************************************************00000120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-LV05-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                  
      *--                                                                       
       01  COMM-LV05-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
       01  COMM-LV05-APPLI.                                             00000130
                05 COMM-LV05-ZONES-ENTREE.                              00000140
                  10 COMM-LV05-CTRAITEMENT   PIC X.                             
                  10 COMM-LV05-CACID         PIC X(08).                         
                  10 COMM-LV05-NPROG         PIC X(08).                         
                  10 COMM-LV05-APPLID        PIC X(08).                         
                  10 COMM-LV05-TERMID        PIC X(04).                         
                  10 COMM-LV05-USERID        PIC X(08).                         
                  10 COMM-LV05-DATEJOUR      PIC X(08).                         
                  10 COMM-LV05-NSOCORIG      PIC X(03).                 00000200
                  10 COMM-LV05-NLIEUORIG     PIC X(03).                 00000200
                  10 COMM-LV05-NSOCDEST      PIC X(03).                 00000200
                  10 COMM-LV05-NLIEUDEST     PIC X(03).                 00000200
                  10 COMM-LV05-CTYPDOC       PIC X(02).                 00000200
                  10 COMM-LV05-NUMDOC        PIC X(07).                 00000200
                  10 COMM-LV05-COPAR         PIC X(05).                 00000200
                05 COMM-LV05-ZONES-SORTIE.                              00000220
                  10 COMM-LV05-CRETOUR       PIC X(02).                 00000230
                  10 COMM-LV05-LRETOUR       PIC X(80).                 00000231
AA0314*         05 COMM-LV05-FILLER          PIC X(3943).                       
AA0314          05 COMM-LV05-DIVERS.                                            
AA0314            10 COMM-LV05-NB-MSG-MQ     PIC 9(05).                         
AA0314            10 FILLER                  PIC X(938).                        
AA0314          05 COMM-LV05-FILLER          PIC X(3000).                       
                                                                                
