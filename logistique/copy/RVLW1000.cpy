      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLW1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLW1000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW1000.                                                            
           02  LW10-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  LW10-NDEPOT                                                      
               PIC X(0003).                                                     
           02  LW10-NCODIC                                                      
               PIC X(0007).                                                     
           02  LW10-CEMPL                                                       
               PIC X(0005).                                                     
           02  LW10-DSTOCK                                                      
               PIC X(0008).                                                     
           02  LW10-QSTOCK                                                      
               PIC S9(9) COMP-3.                                                
           02  LW10-ADRESSE                                                     
               PIC X(0020).                                                     
           02  LW10-CETAT                                                       
               PIC X(0002).                                                     
           02  LW10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLW1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW10-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW10-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW10-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW10-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW10-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW10-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW10-CEMPL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW10-CEMPL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW10-DSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW10-DSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW10-QSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW10-QSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW10-ADRESSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW10-ADRESSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW10-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW10-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
