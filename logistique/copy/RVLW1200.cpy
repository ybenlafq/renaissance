      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLW1200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLW1200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW1200.                                                            
           02  LW12-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  LW12-NDEPOT                                                      
               PIC X(0003).                                                     
           02  LW12-DSTOCK                                                      
               PIC X(0008).                                                     
           02  LW12-NSEQ                                                        
               PIC S9(3) COMP-3.                                                
           02  LW12-NCODIC                                                      
               PIC X(0007).                                                     
           02  LW12-ADRESSE                                                     
               PIC X(0020).                                                     
           02  LW12-NSUPPORT                                                    
               PIC X(0018).                                                     
           02  LW12-QSTOCK                                                      
               PIC S9(9) COMP-3.                                                
           02  LW12-CETAT                                                       
               PIC X(0002).                                                     
           02  LW12-CBLOCAGE                                                    
               PIC X(0003).                                                     
           02  LW12-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLW1200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW1200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW12-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW12-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW12-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW12-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW12-DSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW12-DSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW12-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW12-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW12-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW12-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW12-ADRESSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW12-ADRESSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW12-NSUPPORT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW12-NSUPPORT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW12-QSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW12-QSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW12-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW12-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW12-CBLOCAGE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW12-CBLOCAGE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW12-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW12-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
