      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFL5100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFL5100                         
      *---------------------------------------------------------                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL5100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL5100.                                                            
      *}                                                                        
           02  FL51-NCODIC                                                      
               PIC X(0007).                                                     
           02  FL51-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  FL51-NDEPOT                                                      
               PIC X(0003).                                                     
           02  FL51-WMODSTOCK1                                                  
               PIC X(0001).                                                     
           02  FL51-WMODSTOCK2                                                  
               PIC X(0001).                                                     
           02  FL51-WMODSTOCK3                                                  
               PIC X(0001).                                                     
           02  FL51-CMODSTOCK1                                                  
               PIC X(0005).                                                     
           02  FL51-CMODSTOCK2                                                  
               PIC X(0005).                                                     
           02  FL51-CMODSTOCK3                                                  
               PIC X(0005).                                                     
           02  FL51-QCOLIDSTOCK                                                 
               PIC S9(5) COMP-3.                                                
           02  FL51-CFETEMPL                                                    
               PIC X(0001).                                                     
           02  FL51-CFAM                                                        
               PIC X(0005).                                                     
           02  FL51-CMARQ                                                       
               PIC X(0005).                                                     
           02  FL51-QPOIDS                                                      
               PIC S9(7) COMP-3.                                                
           02  FL51-QHAUTEUR                                                    
               PIC S9(3) COMP-3.                                                
           02  FL51-QLARGEUR                                                    
               PIC S9(3) COMP-3.                                                
           02  FL51-QPROFONDEUR                                                 
               PIC S9(3) COMP-3.                                                
           02  FL51-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFL5100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL5100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL5100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-WMODSTOCK1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-WMODSTOCK1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-WMODSTOCK2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-WMODSTOCK2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-WMODSTOCK3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-WMODSTOCK3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-CMODSTOCK1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-CMODSTOCK1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-CMODSTOCK2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-CMODSTOCK2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-CMODSTOCK3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-CMODSTOCK3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-QCOLIDSTOCK-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-QCOLIDSTOCK-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-CFETEMPL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-CFETEMPL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-QPOIDS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-QPOIDS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-QHAUTEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-QHAUTEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-QLARGEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-QLARGEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-QPROFONDEUR-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-QPROFONDEUR-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL51-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL51-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
