      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE IECAP CAPACITE CONTENEURS PAR DEPOT    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIECAP.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIECAP.                                                             
      *}                                                                        
           05  IECAP-CTABLEG2    PIC X(15).                                     
           05  IECAP-CTABLEG2-REDEF REDEFINES IECAP-CTABLEG2.                   
               10  IECAP-NSOCDEPO        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  IECAP-NSOCDEPO-N     REDEFINES IECAP-NSOCDEPO                
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  IECAP-NDEPOT          PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  IECAP-NDEPOT-N       REDEFINES IECAP-NDEPOT                  
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  IECAP-CTRACK          PIC X(01).                             
           05  IECAP-WTABLEG     PIC X(80).                                     
           05  IECAP-WTABLEG-REDEF  REDEFINES IECAP-WTABLEG.                    
               10  IECAP-LTRACK          PIC X(20).                             
               10  IECAP-QCAPCONT        PIC S9(03)V9(02)     COMP-3.           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIECAP-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIECAP-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IECAP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  IECAP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IECAP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  IECAP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
