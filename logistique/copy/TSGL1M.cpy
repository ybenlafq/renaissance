      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00000010
       01  TS-GL1M-DONNEES.                                             00000070
           05 TS-GL1M-ECRAN.                                            00000090
              15 TS-GL1M-NFOUR         PIC X(05).                          00000
              15 TS-GL1M-CDE           PIC X(07).                          00000
              15 TS-GL1M-CODIC         PIC X(07).                          00000
              15 TS-GL1M-CQUOTA        PIC X(05).                          00000
              15 TS-GL1M-LREFF         PIC X(11).                          00000
              15 TS-GL1M-QTE           PIC 9(05).                               
              15 TS-GL1M-QNBUO         PIC 9(04).                               
              15 TS-GL1M-QNBPAL        PIC 9(04).                               
              15 TS-GL1M-ORIGDATE      PIC X(08).                               
              15 TS-GL1M-ORIGDATE-6    PIC X(08).                               
              15 TS-GL1M-CREASON       PIC X(05).                               
              15 TS-GL1M-TV-STAND      PIC X(01).                               
              15 TS-GL1M-DATA-CPT      PIC X(01).                               
           05 TS-GL1M-TABLE.                                            00000090
              15 TS-GL1M-NSEQ          PIC X(02).                          00000
              15 TS-GL1M-NCDE          PIC X(07).                             00
              15 TS-GL1M-NCODIC        PIC X(07).                             00
              15 TS-GL1M-NLIVRAISON    PIC X(07).                             00
              15 TS-GL1M-DLIVRAISON    PIC X(08).                             00
              15 TS-GL1M-DSAISIE       PIC X(08).                             00
              15 TS-GL1M-QCDE          PIC 9(05).                          00000
              15 TS-GL1M-QUOLIVR       PIC 9(05).                          00000
              15 TS-GL1M-QUOPALETTIS   PIC 9(05).                          00000
              15 TS-GL1M-CMODSTOCK     PIC X(05).                               
      ****************************************************************  00000010
                                                                                
