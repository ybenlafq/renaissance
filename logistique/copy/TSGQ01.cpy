      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : DEFINITION DES PROFILS D'AFFILIATION (FAM / ENTREPOT ) *         
      *        POUR MISE A JOUR VUE RVGQ0500     (PGR : TGQ01)        *         
      *****************************************************************         
      *                                                               *         
       01  TS-GQ01.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GQ01-LONG                 PIC S9(3) COMP-3                     
                                           VALUE +56.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GQ01-DONNEES.                                                  
      *----------------------------------  MODE DE MAJ                          
      *                                         ' ' : PAS DE MAJ                
      *                                         'C' : CREATION                  
      *                                         'M' : MODIFICATION              
              03 TS-GQ01-CMAJ              PIC X(1).                            
      *----------------------------------  CODE FAMILLE                         
              03 TS-GQ01-CFAM              PIC X(5).                            
      *----------------------------------  LIBELLE FAMILLE                      
              03 TS-GQ01-LFAM              PIC X(20).                           
      *----------------------------------  ZONE ENTREPOTS D'AFFILIATION         
              03 TS-GQ01-ENTAFF            OCCURS 5.                            
      *----------------------------------  CODE SOCIETE                         
                 04 TS-GQ01-CSOCAFF           PIC X(03).                        
      *----------------------------------  CODE ENTREPOT                        
                 04 TS-GQ01-CDEPAFF           PIC X(03).                        
                                                                                
