      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM1500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM1500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1500.                                                            
           02  RM15-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RM15-NLIEU                                                       
               PIC X(0003).                                                     
           02  RM15-CSELART                                                     
               PIC X(0005).                                                     
           02  RM15-QFREQUENCE                                                  
               PIC S9(1)V9(0002) COMP-3.                                        
           02  RM15-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM1500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM15-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM15-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM15-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM15-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM15-CSELART-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM15-CSELART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM15-QFREQUENCE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM15-QFREQUENCE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM15-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
