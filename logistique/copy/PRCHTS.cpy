      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *=========================================================*               
      *                                                         *               
      *          MODULE DE CHARGEMENT D'UNE TS                  *               
      *          -----------------------------                  *               
      *                                                         *               
      *             DEPUIS UNE TABLE INTERNE                    *               
      *             ------------------------                    *               
      *                                                         *               
      *                                                         *               
      *      AVANT DE LANCER CE MODULE, IL FAUT DEFINIR :       *               
      *      - LE PREFIXE DE LA TS "DEBUT"                      *               
      *      - LE PREFIXE DE LA TS "FIN"                        *               
      *      - LE DEBUT DU NOM DE LA TS RECEPTRICE DANS WNPRG   *               
      *      - SON NOMBRE MAXI DE LIGNES DANS WNBLMAX           *               
      *                                                         *               
      *                                                         *               
      *      EXEMPLE :                                          *               
      *                                                         *               
      *      1�) INCLURE EN PROCEDURE DIVISION :                *               
      *                                                         *               
      *          COPY PRCHTS REPLACING ==:TSD:== BY ==TS==      *               
      *                                ==:TSF:== BY ==TS-T==.   *               
      *                                                         *               
      *      2�) PUIS ECRIRE :                                  *               
      *                                                         *               
      *          MOVE 'YYYY' TO WNPRG.                          *               
      *          MOVE    12  TO WNBLMAX.                        *               
      *          PERFORM PRCHTS.                                *               
      *                                                         *               
      *=========================================================*               
       PRCHTS               SECTION.                                            
      *    CHARGEMENT DE LA TS DEPUIS LA TABLE INTERNE                          
           MOVE    ZERO TO  WTJ.                                                
           INITIALIZE      :TSF:-DATA                                           
           PERFORM VARYING  WTI FROM  1  BY  1  UNTIL  WTI  >  WTIMAX           
               OR  WCLETRI (WTI)  = ( LOW-VALUE OR HIGH-VALUE )                 
              PERFORM      LEC-:TSD:                                            
              ADD       1   TO  WTJ                                             
              IF                WTJ     >  WNBLMAX                              
                 PERFORM   ECR-:TSF:                                            
                 INITIALIZE    :TSF:-DATA                                       
                 MOVE   1   TO  WTJ                                             
                 PERFORM   LEC-:TSD:                                            
              END-IF                                                            
              MOVE  WNLIGNE    (WTI) TO              WTK                        
              MOVE :TSD:-LIGNE (WTK) TO :TSF:-LIGNE (WTJ)                       
           END-PERFORM                                                          
           IF WCLETRI (WTI)  =  LOW-VALUE OR HIGH-VALUE                         
              PERFORM      ECR-:TSF:                                            
           END-IF.                                                              
       PRCHTS-FIN.          EXIT.                                               
      *                                                                         
       LEC-:TSD:      SECTION.                                                  
              MOVE WNBITEM (WTI)  TO   RANG-TS                                  
              MOVE :TSD:-LONG     TO   LONG-TS                                  
              MOVE :TSD:-NOM      TO  IDENT-TS                                  
              PERFORM                  READ-TS                                  
              MOVE Z-INOUT        TO       :TSD:-DATA.                          
       LEC-:TSD:-FIN. EXIT.                                                     
      *                                                                         
       ECR-:TSF:      SECTION.                                                  
              MOVE :TSF:-DATA   TO   Z-INOUT                                    
              MOVE       WNPRG  TO       :TSF:-NOM(1:4)                         
              MOVE :TSF:-LONG   TO   LONG-TS                                    
              MOVE :TSF:-NOM    TO  IDENT-TS                                    
              PERFORM               WRITE-TS.                                   
       ECR-:TSF:-FIN. EXIT.                                                     
                                                                                
