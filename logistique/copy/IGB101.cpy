      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGB101 AU 16/11/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,24,BI,A,                          *        
      *                           37,05,BI,A,                          *        
      *                           42,03,BI,A,                          *        
      *                           45,03,BI,A,                          *        
      *                           48,03,BI,A,                          *        
      *                           51,07,BI,A,                          *        
      *                           58,03,PD,A,                          *        
      *                           61,05,BI,A,                          *        
      *                           66,07,BI,A,                          *        
      *                           73,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGB101.                                                        
            05 NOMETAT-IGB101           PIC X(6) VALUE 'IGB101'.                
            05 RUPTURES-IGB101.                                                 
           10 IGB101-NSOCENTR           PIC X(03).                      007  003
           10 IGB101-NDEPOT             PIC X(03).                      010  003
           10 IGB101-DDESTOCK           PIC X(24).                      013  024
           10 IGB101-CSELART            PIC X(05).                      037  005
           10 IGB101-NSOCGRP            PIC X(03).                      042  003
           10 IGB101-NSOCIETE           PIC X(03).                      045  003
           10 IGB101-NLIEU              PIC X(03).                      048  003
           10 IGB101-NMUTATION          PIC X(07).                      051  007
           10 IGB101-WSEQFAM            PIC S9(05)      COMP-3.         058  003
           10 IGB101-CMARQ              PIC X(05).                      061  005
           10 IGB101-NCODIC             PIC X(07).                      066  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGB101-SEQUENCE           PIC S9(04) COMP.                073  002
      *--                                                                       
           10 IGB101-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGB101.                                                   
           10 IGB101-CFAM               PIC X(05).                      075  005
           10 IGB101-LLIEU              PIC X(20).                      080  020
           10 IGB101-LREFFOURN          PIC X(20).                      100  020
           10 IGB101-LSOCGRP            PIC X(13).                      120  013
           10 IGB101-QDEMANDEE          PIC S9(05)      COMP-3.         133  003
           10 IGB101-QVOLUME            PIC S9(03)V9(3) COMP-3.         136  004
            05 FILLER                      PIC X(373).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGB101-LONG           PIC S9(4)   COMP  VALUE +139.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGB101-LONG           PIC S9(4) COMP-5  VALUE +139.           
                                                                                
      *}                                                                        
