      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010016
      * COMMAREA SPECIFIQUE PRG TMU32 (TMU20 -> MENU)    TR: MU20  *    00020016
      *               REGUL APPROS-STOCKS MAGASINS                            16
      *                                                                 00040016
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7707  00050016
      *                                                                 00060016
      *        TRANSACTION MU32 : INT DES DATES DE MUTATIONS                  16
      *                                                                 00080016
          02 COMM-MU32-APPLI REDEFINES COMM-MU20-APPLI.                 00090016
      *------------------------------ ZONE DONNEES TMU20                00100016
             04 COMM-MU20-NMUTATION      PIC X(7).                              
             04 COMM-MU20-SOC2           PIC X(3).                              
             04 COMM-MU20-LIEU2          PIC X(3).                              
             04 COMM-MU20-LIBLIEU2       PIC X(20).                             
             04 COMM-MU20-SOC1           PIC X(3).                              
             04 COMM-MU20-LIEU1          PIC X(3).                              
             04 COMM-MU32-DEBSEM         PIC 9(6).                              
             04 FILLER                   PIC X(2).                              
             04 COMM-MU20-SELART         PIC X(5).                              
      *------------------------------ ZONE DONNEES TMU32                00100016
             04 COMM-MU32-DONNEES-TMU32.                                00110016
      *------------------------------                                   00120016
                 05  COMM-MU32-NPAGE           PIC 9(02).               00170016
                 05  COMM-MU32-NPAGMAX         PIC 9(02).               00190016
                 05  COMM-MU32-NLIGMAX         PIC 9(02).               00190016
                 05  COMM-MU32-NJOURPROT       PIC 9(03).               00190016
                 05  COMM-MU32-NAAGAUCHE       PIC 9(04).               00190016
                 05  COMM-MU32-NSEMGAUCHE      PIC 9(02).               00190016
                 05  COMM-MU32-DDEBUT-QU       PIC 9(05).               00190016
                 05  COMM-MU32-DDEBUT-ET       PIC X(08).               00190016
                 05  COMM-MU32-ENTETE.                                        16
                     06  COMM-MU32-JJMM    OCCURS 2.                            
                         10  COMM-MU32-ATZONE  OCCURS 21.                       
                             15  COMM-MU32-JM  PIC X(02).                       
                 05  COMM-MU32-TABLE               OCCURS 500.                  
                     06  COMM-MU32-NSOCIETE        PIC X(3).                    
                     06  COMM-MU32-NLIEU           PIC X(3).                    
                 05  COMM-MU32-INDICE              PIC S9(3) COMP-3.          16
      *------------------------------ ZONE LIBRE                        00650018
      *                                                                 00660016
             04 COMM-MU32-LIBRE             PIC X(4653).                00670030
      *                                                                 00680016
                                                                                
