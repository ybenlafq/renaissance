      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : ARIANE                                           *        
      *  TRANSACTION: MGL20                                            *        
      *  TITRE      : COMMAREA DE PASSAGE VERS LE MODULE MGL20         *        
      *  LONGUEUR   : 43860                                            *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00530000
      *                                                                 00230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MGL20-LONG-COMMAREA  PIC S9(5) COMP VALUE +43860.               
      *--                                                                       
       01  COMM-MGL20-LONG-COMMAREA  PIC S9(5) COMP-5 VALUE +43860.             
      *}                                                                        
       01  COMM-MGL20-APPLI.                                            00260000
E0011 *                        LONG = LONG1 + 10 ( LONG2 + 30 X LONG3 )         
E0105 *                        LONG = 32395                                     
         02 COMM-MGL20-ENTREE.                                          00260000
E0011 *                                            LONG1=135                    
           05 COMM-MGL20-NSOCIETE          PIC X(3).                            
           05 COMM-MGL20-NBENR             PIC 9(2).                            
           05 COMM-MGL20-ENT.                                                   
              10 COMM-MGL20-WFONC          PIC X(03).                           
              10 COMM-MGL20-NLIVR          PIC X(07).                           
              10 COMM-MGL20-NSOCLIVR       PIC X(03).                           
              10 COMM-MGL20-NDEPOT         PIC X(03).                           
              10 COMM-MGL20-CLIVR          PIC X(05).                           
              10 COMM-MGL20-LLIVR          PIC X(20).                           
              10 COMM-MGL20-DLIVR          PIC X(08).                           
              10 COMM-MGL20-DCRE           PIC X(08).                           
              10 COMM-MGL20-CREC           PIC X(05).                           
              10 COMM-MGL20-CMODTRANS      PIC X(05).                           
              10 COMM-MGL20-TOTQCD         PIC 9(07).                           
              10 COMM-MGL20-TOTQPL         PIC 9(07).                           
              10 COMM-MGL20-TOTQUT         PIC 9(07).                           
              10 COMM-MGL20-TOTPDS         PIC 9(13)V9(03).                     
              10 COMM-MGL20-TOTCUB         PIC 9(13)V9(03).                     
              10 COMM-MGL20-TOTQUO         PIC 9(05)V9(02).                     
              10 COMM-MGL20-FLPALT         PIC X(01).                           
              10 COMM-MGL20-NBOCC          PIC 9(02).                           
           05 COMM-MGL20-ENR.                                                   
E0011 *       10 COMM-MGL20-MESS    OCCURS 15 DEPENDING ON                      
E0011         10 COMM-MGL20-MESS    OCCURS 10 DEPENDING ON                      
                                              COMM-MGL20-NBOCC.                 
E0011 *                                                        LONG2=106        
                 15 COMM-MGL20-NCDE           PIC X(07).                   00320
                 15 COMM-MGL20-NENTCDE        PIC X(05).                   0034 
                 15 COMM-MGL20-DELAPP         PIC 9(03).                   00340
                 15 COMM-MGL20-CDEQCD         PIC 9(07).                        
                 15 COMM-MGL20-CDEQUT         PIC 9(07).                        
                 15 COMM-MGL20-CDEQCT         PIC 9(07).                        
                 15 COMM-MGL20-CDEQPL         PIC 9(07).                        
                 15 COMM-MGL20-CDEPDS         PIC 9(13)V9(03).                  
                 15 COMM-MGL20-CDECUB         PIC 9(13)V9(03).                  
                 15 COMM-MGL20-CDEQUO         PIC 9(05).                        
                 15 COMM-MGL20-CDEV           PIC X(03).                        
                 15 COMM-MGL20-CDEDEFFET      PIC X(08).                        
                 15 COMM-MGL20-CDEDSAIS       PIC X(08).                        
                 15 COMM-MGL20-WCDECNB        PIC 9(02).                        
E0105            15 COMM-MGL20-POCREAS        PIC X(5).                         
                 15 COMM-MGL20-DET.                                             
E0011 *             20 COMM-MGL20-PCOD OCCURS 20.                               
E0011               20 COMM-MGL20-PCOD OCCURS 30.                               
E0011**                                                        LONG3=104        
E0105 *                                                        LONG3=084        
                       25 COMM-MGL20-NCODIC      PIC X(07).                     
                       25 COMM-MGL20-NBUT        PIC 9(07).                     
                       25 COMM-MGL20-NBCT        PIC 9(07).                     
                       25 COMM-MGL20-NBPL        PIC 9(07).                     
                       25 COMM-MGL20-QCDE        PIC 9(05).                     
                       25 COMM-MGL20-POIDS       PIC 9(13)V9(03).               
                       25 COMM-MGL20-CUBE        PIC 9(13)V9(03).               
                       25 COMM-MGL20-QUOLIVR     PIC 9(05).                     
EP    *                25 COMM-MGL20-PBF         PIC 9(09).                     
EP                     25 COMM-MGL20-PBF         PIC 9(07)V99.                  
                       25 COMM-MGL20-QCDEREF     PIC 9(05).                     
E0105 *                25 COMM-MGL20-FIL1        PIC X(20).                     
         02 COMM-MGL20-SORTIE.                                          00260000
           05 COMM-MGL20-MESSAGE.                                               
                10 COMM-MGL20-CODRET         PIC X.                             
                   88  COMM-MGL20-OK         VALUE ' '.                         
                   88  COMM-MGL20-ERR        VALUE '1'.                         
                10 COMM-MGL20-LIBERR         PIC X(58).                         
         02 FILLER                    PIC X(005).                       00340000
                                                                                
