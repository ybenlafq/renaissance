      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFL50   EFL50                                              00000020
      ***************************************************************** 00000030
       01   EFL52I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLACHL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MSLACHL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSLACHF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSLACHI   PIC X.                                          00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPAGEMAXI      PIC X(2).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCODICI   PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCODICI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMARQI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLMARQI   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSOCIETEI      PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDEPOTI   PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLDEPOTI  PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPTL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MDRECEPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRECEPTF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDRECEPTI      PIC X(10).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEMBL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLIBEMBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBEMBF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIBEMBI  PIC X(50).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOLRECL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCOLRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOLRECF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCOLRECI  PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOLSTOKL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MCOLSTOKL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOLSTOKF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCOLSTOKI      PIC X(5).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACTIFL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MACTIFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MACTIFF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MACTIFI   PIC X.                                          00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCQUOTAL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCQUOTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCQUOTAF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCQUOTAI  PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLQUOTAL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLQUOTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLQUOTAF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLQUOTAI  PIC X(20).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPREHENL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MPREHENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPREHENF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MPREHENI  PIC X.                                          00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACCESSL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MACCESSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MACCESSF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MACCESSI  PIC X.                                          00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPALRAKL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MPALRAKL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPALRAKF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MPALRAKI  PIC X(5).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPALSOLL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MPALSOLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPALSOLF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MPALSOLI  PIC X(3).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODSTK1L      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MMODSTK1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MMODSTK1F      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MMODSTK1I      PIC X(5).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTD1L    COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MSTD1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSTD1F    PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MSTD1I    PIC X.                                          00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCONTENL      COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MTCONTENL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTCONTENF      PIC X.                                     00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MTCONTENI      PIC X.                                     00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAILLEL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MMAILLEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMAILLEF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MMAILLEI  PIC X(2).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODSTK2L      COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MMODSTK2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MMODSTK2F      PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MMODSTK2I      PIC X(5).                                  00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTD2L    COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MSTD2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSTD2F    PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MSTD2I    PIC X.                                          00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSPECIFL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MSPECIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSPECIFF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MSPECIFI  PIC X.                                          00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNIVGERBL      COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MNIVGERBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNIVGERBF      PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MNIVGERBI      PIC X(2).                                  00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODSTK3L      COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MMODSTK3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MMODSTK3F      PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MMODSTK3I      PIC X(5).                                  00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTD3L    COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MSTD3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSTD3F    PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MSTD3I    PIC X.                                          00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMECAL    COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MMECAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMECAF    PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MMECAI    PIC X.                                          00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLASSEL  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MCLASSEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLASSEF  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MCLASSEI  PIC X.                                          00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MZONCMDI  PIC X(15).                                      00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MLIBERRI  PIC X(58).                                      00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MCODTRAI  PIC X(4).                                       00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MCICSI    PIC X(5).                                       00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MNETNAMI  PIC X(8).                                       00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MSCREENI  PIC X(4).                                       00001770
      ***************************************************************** 00001780
      * SDF: EFL50   EFL50                                              00001790
      ***************************************************************** 00001800
       01   EFL52O REDEFINES EFL52I.                                    00001810
           02 FILLER    PIC X(12).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MDATJOUA  PIC X.                                          00001840
           02 MDATJOUC  PIC X.                                          00001850
           02 MDATJOUP  PIC X.                                          00001860
           02 MDATJOUH  PIC X.                                          00001870
           02 MDATJOUV  PIC X.                                          00001880
           02 MDATJOUO  PIC X(10).                                      00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MTIMJOUA  PIC X.                                          00001910
           02 MTIMJOUC  PIC X.                                          00001920
           02 MTIMJOUP  PIC X.                                          00001930
           02 MTIMJOUH  PIC X.                                          00001940
           02 MTIMJOUV  PIC X.                                          00001950
           02 MTIMJOUO  PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MPAGEA    PIC X.                                          00001980
           02 MPAGEC    PIC X.                                          00001990
           02 MPAGEP    PIC X.                                          00002000
           02 MPAGEH    PIC X.                                          00002010
           02 MPAGEV    PIC X.                                          00002020
           02 MPAGEO    PIC 99.                                         00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSLACHA   PIC X.                                          00002050
           02 MSLACHC   PIC X.                                          00002060
           02 MSLACHP   PIC X.                                          00002070
           02 MSLACHH   PIC X.                                          00002080
           02 MSLACHV   PIC X.                                          00002090
           02 MSLACHO   PIC X.                                          00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MPAGEMAXA      PIC X.                                     00002120
           02 MPAGEMAXC PIC X.                                          00002130
           02 MPAGEMAXP PIC X.                                          00002140
           02 MPAGEMAXH PIC X.                                          00002150
           02 MPAGEMAXV PIC X.                                          00002160
           02 MPAGEMAXO      PIC 99.                                    00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MCODICA   PIC X.                                          00002190
           02 MCODICC   PIC X.                                          00002200
           02 MCODICP   PIC X.                                          00002210
           02 MCODICH   PIC X.                                          00002220
           02 MCODICV   PIC X.                                          00002230
           02 MCODICO   PIC X(7).                                       00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MLCODICA  PIC X.                                          00002260
           02 MLCODICC  PIC X.                                          00002270
           02 MLCODICP  PIC X.                                          00002280
           02 MLCODICH  PIC X.                                          00002290
           02 MLCODICV  PIC X.                                          00002300
           02 MLCODICO  PIC X(20).                                      00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCFAMA    PIC X.                                          00002330
           02 MCFAMC    PIC X.                                          00002340
           02 MCFAMP    PIC X.                                          00002350
           02 MCFAMH    PIC X.                                          00002360
           02 MCFAMV    PIC X.                                          00002370
           02 MCFAMO    PIC X(5).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MLFAMA    PIC X.                                          00002400
           02 MLFAMC    PIC X.                                          00002410
           02 MLFAMP    PIC X.                                          00002420
           02 MLFAMH    PIC X.                                          00002430
           02 MLFAMV    PIC X.                                          00002440
           02 MLFAMO    PIC X(20).                                      00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MCMARQA   PIC X.                                          00002470
           02 MCMARQC   PIC X.                                          00002480
           02 MCMARQP   PIC X.                                          00002490
           02 MCMARQH   PIC X.                                          00002500
           02 MCMARQV   PIC X.                                          00002510
           02 MCMARQO   PIC X(5).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLMARQA   PIC X.                                          00002540
           02 MLMARQC   PIC X.                                          00002550
           02 MLMARQP   PIC X.                                          00002560
           02 MLMARQH   PIC X.                                          00002570
           02 MLMARQV   PIC X.                                          00002580
           02 MLMARQO   PIC X(20).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MSOCIETEA      PIC X.                                     00002610
           02 MSOCIETEC PIC X.                                          00002620
           02 MSOCIETEP PIC X.                                          00002630
           02 MSOCIETEH PIC X.                                          00002640
           02 MSOCIETEV PIC X.                                          00002650
           02 MSOCIETEO      PIC X(3).                                  00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MDEPOTA   PIC X.                                          00002680
           02 MDEPOTC   PIC X.                                          00002690
           02 MDEPOTP   PIC X.                                          00002700
           02 MDEPOTH   PIC X.                                          00002710
           02 MDEPOTV   PIC X.                                          00002720
           02 MDEPOTO   PIC X(3).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MLDEPOTA  PIC X.                                          00002750
           02 MLDEPOTC  PIC X.                                          00002760
           02 MLDEPOTP  PIC X.                                          00002770
           02 MLDEPOTH  PIC X.                                          00002780
           02 MLDEPOTV  PIC X.                                          00002790
           02 MLDEPOTO  PIC X(20).                                      00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MDRECEPTA      PIC X.                                     00002820
           02 MDRECEPTC PIC X.                                          00002830
           02 MDRECEPTP PIC X.                                          00002840
           02 MDRECEPTH PIC X.                                          00002850
           02 MDRECEPTV PIC X.                                          00002860
           02 MDRECEPTO      PIC X(10).                                 00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MLIBEMBA  PIC X.                                          00002890
           02 MLIBEMBC  PIC X.                                          00002900
           02 MLIBEMBP  PIC X.                                          00002910
           02 MLIBEMBH  PIC X.                                          00002920
           02 MLIBEMBV  PIC X.                                          00002930
           02 MLIBEMBO  PIC X(50).                                      00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MCOLRECA  PIC X.                                          00002960
           02 MCOLRECC  PIC X.                                          00002970
           02 MCOLRECP  PIC X.                                          00002980
           02 MCOLRECH  PIC X.                                          00002990
           02 MCOLRECV  PIC X.                                          00003000
           02 MCOLRECO  PIC X(5).                                       00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MCOLSTOKA      PIC X.                                     00003030
           02 MCOLSTOKC PIC X.                                          00003040
           02 MCOLSTOKP PIC X.                                          00003050
           02 MCOLSTOKH PIC X.                                          00003060
           02 MCOLSTOKV PIC X.                                          00003070
           02 MCOLSTOKO      PIC X(5).                                  00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MACTIFA   PIC X.                                          00003100
           02 MACTIFC   PIC X.                                          00003110
           02 MACTIFP   PIC X.                                          00003120
           02 MACTIFH   PIC X.                                          00003130
           02 MACTIFV   PIC X.                                          00003140
           02 MACTIFO   PIC X.                                          00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MCQUOTAA  PIC X.                                          00003170
           02 MCQUOTAC  PIC X.                                          00003180
           02 MCQUOTAP  PIC X.                                          00003190
           02 MCQUOTAH  PIC X.                                          00003200
           02 MCQUOTAV  PIC X.                                          00003210
           02 MCQUOTAO  PIC X(5).                                       00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MLQUOTAA  PIC X.                                          00003240
           02 MLQUOTAC  PIC X.                                          00003250
           02 MLQUOTAP  PIC X.                                          00003260
           02 MLQUOTAH  PIC X.                                          00003270
           02 MLQUOTAV  PIC X.                                          00003280
           02 MLQUOTAO  PIC X(20).                                      00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MPREHENA  PIC X.                                          00003310
           02 MPREHENC  PIC X.                                          00003320
           02 MPREHENP  PIC X.                                          00003330
           02 MPREHENH  PIC X.                                          00003340
           02 MPREHENV  PIC X.                                          00003350
           02 MPREHENO  PIC X.                                          00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MACCESSA  PIC X.                                          00003380
           02 MACCESSC  PIC X.                                          00003390
           02 MACCESSP  PIC X.                                          00003400
           02 MACCESSH  PIC X.                                          00003410
           02 MACCESSV  PIC X.                                          00003420
           02 MACCESSO  PIC X.                                          00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MPALRAKA  PIC X.                                          00003450
           02 MPALRAKC  PIC X.                                          00003460
           02 MPALRAKP  PIC X.                                          00003470
           02 MPALRAKH  PIC X.                                          00003480
           02 MPALRAKV  PIC X.                                          00003490
           02 MPALRAKO  PIC ZZZZZ.                                      00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MPALSOLA  PIC X.                                          00003520
           02 MPALSOLC  PIC X.                                          00003530
           02 MPALSOLP  PIC X.                                          00003540
           02 MPALSOLH  PIC X.                                          00003550
           02 MPALSOLV  PIC X.                                          00003560
           02 MPALSOLO  PIC ZZZ.                                        00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MMODSTK1A      PIC X.                                     00003590
           02 MMODSTK1C PIC X.                                          00003600
           02 MMODSTK1P PIC X.                                          00003610
           02 MMODSTK1H PIC X.                                          00003620
           02 MMODSTK1V PIC X.                                          00003630
           02 MMODSTK1O      PIC X(5).                                  00003640
           02 FILLER    PIC X(2).                                       00003650
           02 MSTD1A    PIC X.                                          00003660
           02 MSTD1C    PIC X.                                          00003670
           02 MSTD1P    PIC X.                                          00003680
           02 MSTD1H    PIC X.                                          00003690
           02 MSTD1V    PIC X.                                          00003700
           02 MSTD1O    PIC X.                                          00003710
           02 FILLER    PIC X(2).                                       00003720
           02 MTCONTENA      PIC X.                                     00003730
           02 MTCONTENC PIC X.                                          00003740
           02 MTCONTENP PIC X.                                          00003750
           02 MTCONTENH PIC X.                                          00003760
           02 MTCONTENV PIC X.                                          00003770
           02 MTCONTENO      PIC X.                                     00003780
           02 FILLER    PIC X(2).                                       00003790
           02 MMAILLEA  PIC X.                                          00003800
           02 MMAILLEC  PIC X.                                          00003810
           02 MMAILLEP  PIC X.                                          00003820
           02 MMAILLEH  PIC X.                                          00003830
           02 MMAILLEV  PIC X.                                          00003840
           02 MMAILLEO  PIC X(2).                                       00003850
           02 FILLER    PIC X(2).                                       00003860
           02 MMODSTK2A      PIC X.                                     00003870
           02 MMODSTK2C PIC X.                                          00003880
           02 MMODSTK2P PIC X.                                          00003890
           02 MMODSTK2H PIC X.                                          00003900
           02 MMODSTK2V PIC X.                                          00003910
           02 MMODSTK2O      PIC X(5).                                  00003920
           02 FILLER    PIC X(2).                                       00003930
           02 MSTD2A    PIC X.                                          00003940
           02 MSTD2C    PIC X.                                          00003950
           02 MSTD2P    PIC X.                                          00003960
           02 MSTD2H    PIC X.                                          00003970
           02 MSTD2V    PIC X.                                          00003980
           02 MSTD2O    PIC X.                                          00003990
           02 FILLER    PIC X(2).                                       00004000
           02 MSPECIFA  PIC X.                                          00004010
           02 MSPECIFC  PIC X.                                          00004020
           02 MSPECIFP  PIC X.                                          00004030
           02 MSPECIFH  PIC X.                                          00004040
           02 MSPECIFV  PIC X.                                          00004050
           02 MSPECIFO  PIC X.                                          00004060
           02 FILLER    PIC X(2).                                       00004070
           02 MNIVGERBA      PIC X.                                     00004080
           02 MNIVGERBC PIC X.                                          00004090
           02 MNIVGERBP PIC X.                                          00004100
           02 MNIVGERBH PIC X.                                          00004110
           02 MNIVGERBV PIC X.                                          00004120
           02 MNIVGERBO      PIC X(2).                                  00004130
           02 FILLER    PIC X(2).                                       00004140
           02 MMODSTK3A      PIC X.                                     00004150
           02 MMODSTK3C PIC X.                                          00004160
           02 MMODSTK3P PIC X.                                          00004170
           02 MMODSTK3H PIC X.                                          00004180
           02 MMODSTK3V PIC X.                                          00004190
           02 MMODSTK3O      PIC X(5).                                  00004200
           02 FILLER    PIC X(2).                                       00004210
           02 MSTD3A    PIC X.                                          00004220
           02 MSTD3C    PIC X.                                          00004230
           02 MSTD3P    PIC X.                                          00004240
           02 MSTD3H    PIC X.                                          00004250
           02 MSTD3V    PIC X.                                          00004260
           02 MSTD3O    PIC X.                                          00004270
           02 FILLER    PIC X(2).                                       00004280
           02 MMECAA    PIC X.                                          00004290
           02 MMECAC    PIC X.                                          00004300
           02 MMECAP    PIC X.                                          00004310
           02 MMECAH    PIC X.                                          00004320
           02 MMECAV    PIC X.                                          00004330
           02 MMECAO    PIC X.                                          00004340
           02 FILLER    PIC X(2).                                       00004350
           02 MCLASSEA  PIC X.                                          00004360
           02 MCLASSEC  PIC X.                                          00004370
           02 MCLASSEP  PIC X.                                          00004380
           02 MCLASSEH  PIC X.                                          00004390
           02 MCLASSEV  PIC X.                                          00004400
           02 MCLASSEO  PIC X.                                          00004410
           02 FILLER    PIC X(2).                                       00004420
           02 MZONCMDA  PIC X.                                          00004430
           02 MZONCMDC  PIC X.                                          00004440
           02 MZONCMDP  PIC X.                                          00004450
           02 MZONCMDH  PIC X.                                          00004460
           02 MZONCMDV  PIC X.                                          00004470
           02 MZONCMDO  PIC X(15).                                      00004480
           02 FILLER    PIC X(2).                                       00004490
           02 MLIBERRA  PIC X.                                          00004500
           02 MLIBERRC  PIC X.                                          00004510
           02 MLIBERRP  PIC X.                                          00004520
           02 MLIBERRH  PIC X.                                          00004530
           02 MLIBERRV  PIC X.                                          00004540
           02 MLIBERRO  PIC X(58).                                      00004550
           02 FILLER    PIC X(2).                                       00004560
           02 MCODTRAA  PIC X.                                          00004570
           02 MCODTRAC  PIC X.                                          00004580
           02 MCODTRAP  PIC X.                                          00004590
           02 MCODTRAH  PIC X.                                          00004600
           02 MCODTRAV  PIC X.                                          00004610
           02 MCODTRAO  PIC X(4).                                       00004620
           02 FILLER    PIC X(2).                                       00004630
           02 MCICSA    PIC X.                                          00004640
           02 MCICSC    PIC X.                                          00004650
           02 MCICSP    PIC X.                                          00004660
           02 MCICSH    PIC X.                                          00004670
           02 MCICSV    PIC X.                                          00004680
           02 MCICSO    PIC X(5).                                       00004690
           02 FILLER    PIC X(2).                                       00004700
           02 MNETNAMA  PIC X.                                          00004710
           02 MNETNAMC  PIC X.                                          00004720
           02 MNETNAMP  PIC X.                                          00004730
           02 MNETNAMH  PIC X.                                          00004740
           02 MNETNAMV  PIC X.                                          00004750
           02 MNETNAMO  PIC X(8).                                       00004760
           02 FILLER    PIC X(2).                                       00004770
           02 MSCREENA  PIC X.                                          00004780
           02 MSCREENC  PIC X.                                          00004790
           02 MSCREENP  PIC X.                                          00004800
           02 MSCREENH  PIC X.                                          00004810
           02 MSCREENV  PIC X.                                          00004820
           02 MSCREENO  PIC X(4).                                       00004830
                                                                                
