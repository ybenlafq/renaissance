      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Suivi des plateformes d'1 groupe                                00000020
      ***************************************************************** 00000030
       01   EPF33I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBGRPTFL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLIBGRPTFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBGRPTFF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLIBGRPTFI     PIC X(20).                                 00000250
           02 M172I OCCURS   14 TIMES .                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MNSOCI  PIC X(3).                                       00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNLIEUI      PIC X(3).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBPTFL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLIBPTFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBPTFF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLIBPTFI     PIC X(20).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBREFACTL   COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNBREFACTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNBREFACTF   PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNBREFACTI   PIC X(3).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCUMULQSOL   COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCUMULQSOL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCUMULQSOF   PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCUMULQSOI   PIC X(3).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAPACITEL   COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCAPACITEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCAPACITEF   PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCAPACITEI   PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTKRPL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MSTKRPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSTKRPF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MSTKRPI      PIC X(3).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MSELI   PIC X.                                          00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTKRPTOTL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MSTKRPTOTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MSTKRPTOTF   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MSTKRPTOTI   PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELCFAML      COMP PIC S9(4).                            00000630
      *--                                                                       
           02 MSELCFAML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSELCFAMF      PIC X.                                     00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MSELCFAMI      PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONNCODICL    COMP PIC S9(4).                            00000670
      *--                                                                       
           02 MZONNCODICL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MZONNCODICF    PIC X.                                     00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MZONNCODICI    PIC X(9).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELNCODICL    COMP PIC S9(4).                            00000710
      *--                                                                       
           02 MSELNCODICL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MSELNCODICF    PIC X.                                     00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MSELNCODICI    PIC X(7).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(79).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * Suivi des plateformes d'1 groupe                                00000960
      ***************************************************************** 00000970
       01   EPF33O REDEFINES EPF33I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEA    PIC X.                                          00001150
           02 MPAGEC    PIC X.                                          00001160
           02 MPAGEP    PIC X.                                          00001170
           02 MPAGEH    PIC X.                                          00001180
           02 MPAGEV    PIC X.                                          00001190
           02 MPAGEO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MPAGEMAXA      PIC X.                                     00001220
           02 MPAGEMAXC PIC X.                                          00001230
           02 MPAGEMAXP PIC X.                                          00001240
           02 MPAGEMAXH PIC X.                                          00001250
           02 MPAGEMAXV PIC X.                                          00001260
           02 MPAGEMAXO      PIC X(3).                                  00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MLIBGRPTFA     PIC X.                                     00001290
           02 MLIBGRPTFC     PIC X.                                     00001300
           02 MLIBGRPTFP     PIC X.                                     00001310
           02 MLIBGRPTFH     PIC X.                                     00001320
           02 MLIBGRPTFV     PIC X.                                     00001330
           02 MLIBGRPTFO     PIC X(20).                                 00001340
           02 M172O OCCURS   14 TIMES .                                 00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MNSOCA  PIC X.                                          00001370
             03 MNSOCC  PIC X.                                          00001380
             03 MNSOCP  PIC X.                                          00001390
             03 MNSOCH  PIC X.                                          00001400
             03 MNSOCV  PIC X.                                          00001410
             03 MNSOCO  PIC X(3).                                       00001420
             03 FILLER       PIC X(2).                                  00001430
             03 MNLIEUA      PIC X.                                     00001440
             03 MNLIEUC PIC X.                                          00001450
             03 MNLIEUP PIC X.                                          00001460
             03 MNLIEUH PIC X.                                          00001470
             03 MNLIEUV PIC X.                                          00001480
             03 MNLIEUO      PIC X(3).                                  00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MLIBPTFA     PIC X.                                     00001510
             03 MLIBPTFC     PIC X.                                     00001520
             03 MLIBPTFP     PIC X.                                     00001530
             03 MLIBPTFH     PIC X.                                     00001540
             03 MLIBPTFV     PIC X.                                     00001550
             03 MLIBPTFO     PIC X(20).                                 00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MNBREFACTA   PIC X.                                     00001580
             03 MNBREFACTC   PIC X.                                     00001590
             03 MNBREFACTP   PIC X.                                     00001600
             03 MNBREFACTH   PIC X.                                     00001610
             03 MNBREFACTV   PIC X.                                     00001620
             03 MNBREFACTO   PIC X(3).                                  00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MCUMULQSOA   PIC X.                                     00001650
             03 MCUMULQSOC   PIC X.                                     00001660
             03 MCUMULQSOP   PIC X.                                     00001670
             03 MCUMULQSOH   PIC X.                                     00001680
             03 MCUMULQSOV   PIC X.                                     00001690
             03 MCUMULQSOO   PIC X(3).                                  00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MCAPACITEA   PIC X.                                     00001720
             03 MCAPACITEC   PIC X.                                     00001730
             03 MCAPACITEP   PIC X.                                     00001740
             03 MCAPACITEH   PIC X.                                     00001750
             03 MCAPACITEV   PIC X.                                     00001760
             03 MCAPACITEO   PIC X(3).                                  00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MSTKRPA      PIC X.                                     00001790
             03 MSTKRPC PIC X.                                          00001800
             03 MSTKRPP PIC X.                                          00001810
             03 MSTKRPH PIC X.                                          00001820
             03 MSTKRPV PIC X.                                          00001830
             03 MSTKRPO      PIC X(3).                                  00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MSELA   PIC X.                                          00001860
             03 MSELC   PIC X.                                          00001870
             03 MSELP   PIC X.                                          00001880
             03 MSELH   PIC X.                                          00001890
             03 MSELV   PIC X.                                          00001900
             03 MSELO   PIC X.                                          00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MSTKRPTOTA   PIC X.                                     00001930
             03 MSTKRPTOTC   PIC X.                                     00001940
             03 MSTKRPTOTP   PIC X.                                     00001950
             03 MSTKRPTOTH   PIC X.                                     00001960
             03 MSTKRPTOTV   PIC X.                                     00001970
             03 MSTKRPTOTO   PIC X(5).                                  00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MSELCFAMA      PIC X.                                     00002000
           02 MSELCFAMC PIC X.                                          00002010
           02 MSELCFAMP PIC X.                                          00002020
           02 MSELCFAMH PIC X.                                          00002030
           02 MSELCFAMV PIC X.                                          00002040
           02 MSELCFAMO      PIC X(5).                                  00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MZONNCODICA    PIC X.                                     00002070
           02 MZONNCODICC    PIC X.                                     00002080
           02 MZONNCODICP    PIC X.                                     00002090
           02 MZONNCODICH    PIC X.                                     00002100
           02 MZONNCODICV    PIC X.                                     00002110
           02 MZONNCODICO    PIC X(9).                                  00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MSELNCODICA    PIC X.                                     00002140
           02 MSELNCODICC    PIC X.                                     00002150
           02 MSELNCODICP    PIC X.                                     00002160
           02 MSELNCODICH    PIC X.                                     00002170
           02 MSELNCODICV    PIC X.                                     00002180
           02 MSELNCODICO    PIC X(7).                                  00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(79).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
