      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EEF20   EEF20                                              00000020
      ***************************************************************** 00000030
       01   EEF20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE1L   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE1F   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGE1I   PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE2L   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MPAGE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE2F   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MPAGE2I   PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCENTREL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MLCENTREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCENTREF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLCENTREI      PIC X(15).                                 00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MCTRAITI  PIC X(5).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRAITF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLTRAITI  PIC X(20).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLTIERSI  PIC X(15).                                      00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCTIERSI  PIC X(5).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRENDUL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRENDUF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCRENDUI  PIC X(5).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRENDUL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MLRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRENDUF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MLRENDUI  PIC X(20).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRENDUL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRENDUF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNRENDUI  PIC X(20).                                      00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRENDUL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MDRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRENDUF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MDRENDUI  PIC X(10).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAUTOLGL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MAUTOLGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MAUTOLGF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MAUTOLGI  PIC X.                                          00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGLOBALL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MGLOBALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MGLOBALF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MGLOBALI  PIC X.                                          00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENVOIL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MLENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENVOIF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MLENVOII  PIC X(14).                                      00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MNENVOII  PIC X(7).                                       00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDUENVL   COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MDUENVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDUENVF   PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MDUENVI   PIC X(2).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MDENVOII  PIC X(10).                                      00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAUENVL   COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MAUENVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MAUENVF   PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MAUENVI   PIC X(2).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIFL      COMP PIC S9(4).                            00000880
      *--                                                                       
           02 MDENVOIFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDENVOIFF      PIC X.                                     00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MDENVOIFI      PIC X(10).                                 00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLACCORDL      COMP PIC S9(4).                            00000920
      *--                                                                       
           02 MLACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLACCORDF      PIC X.                                     00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MLACCORDI      PIC X(14).                                 00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCORDL      COMP PIC S9(4).                            00000960
      *--                                                                       
           02 MNACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNACCORDF      PIC X.                                     00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MNACCORDI      PIC X(12).                                 00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDUACCL   COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MDUACCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDUACCF   PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MDUACCI   PIC X(2).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACCORDL      COMP PIC S9(4).                            00001040
      *--                                                                       
           02 MDACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDACCORDF      PIC X.                                     00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MDACCORDI      PIC X(10).                                 00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB1L    COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MLIB1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB1F    PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MLIB1I    PIC X(13).                                      00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTAVOL  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MTOTAVOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTOTAVOF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MTOTAVOI  PIC X(10).                                      00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEVISEL  COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MDEVISEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEVISEF  PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MDEVISEI  PIC X(3).                                       00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLORIGL   COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MLORIGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLORIGF   PIC X.                                          00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MLORIGI   PIC X(14).                                      00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHSL      COMP PIC S9(4).                            00001240
      *--                                                                       
           02 MNLIEUHSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHSF      PIC X.                                     00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MNLIEUHSI      PIC X(3).                                  00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORIGINL      COMP PIC S9(4).                            00001280
      *--                                                                       
           02 MNORIGINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNORIGINF      PIC X.                                     00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MNORIGINI      PIC X(7).                                  00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB2L    COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MLIB2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB2F    PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MLIB2I    PIC X(8).                                       00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSELECTL      COMP PIC S9(4).                            00001360
      *--                                                                       
           02 MLSELECTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSELECTF      PIC X.                                     00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MLSELECTI      PIC X.                                     00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB3L    COMP PIC S9(4).                                 00001400
      *--                                                                       
           02 MLIB3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB3F    PIC X.                                          00001410
           02 FILLER    PIC X(4).                                       00001420
           02 MLIB3I    PIC X(8).                                       00001430
           02 MAFFICHEI OCCURS   10 TIMES .                             00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTL     COMP PIC S9(4).                            00001450
      *--                                                                       
             03 MSELECTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELECTF     PIC X.                                     00001460
             03 FILLER  PIC X(4).                                       00001470
             03 MSELECTI     PIC X.                                     00001480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNEL      COMP PIC S9(4).                            00001490
      *--                                                                       
             03 MLIGNEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIGNEF      PIC X.                                     00001500
             03 FILLER  PIC X(4).                                       00001510
             03 MLIGNEI      PIC X(66).                                 00001520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVALEURL     COMP PIC S9(4).                            00001530
      *--                                                                       
             03 MVALEURL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVALEURF     PIC X.                                     00001540
             03 FILLER  PIC X(4).                                       00001550
             03 MVALEURI     PIC X(10).                                 00001560
      * MESSAGE ERREUR                                                  00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MLIBERRI  PIC X(78).                                      00001610
      * CODE TRANSACTION                                                00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MCODTRAI  PIC X(4).                                       00001660
      * CICS DE TRAVAIL                                                 00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001680
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001690
           02 FILLER    PIC X(4).                                       00001700
           02 MCICSI    PIC X(5).                                       00001710
      * NETNAME                                                         00001720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001730
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001740
           02 FILLER    PIC X(4).                                       00001750
           02 MNETNAMI  PIC X(8).                                       00001760
      * CODE TERMINAL                                                   00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MSCREENI  PIC X(5).                                       00001810
      ***************************************************************** 00001820
      * SDF: EEF20   EEF20                                              00001830
      ***************************************************************** 00001840
       01   EEF20O REDEFINES EEF20I.                                    00001850
           02 FILLER    PIC X(12).                                      00001860
      * DATE DU JOUR                                                    00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MDATJOUA  PIC X.                                          00001890
           02 MDATJOUC  PIC X.                                          00001900
           02 MDATJOUP  PIC X.                                          00001910
           02 MDATJOUH  PIC X.                                          00001920
           02 MDATJOUV  PIC X.                                          00001930
           02 MDATJOUO  PIC X(10).                                      00001940
      * HEURE                                                           00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MTIMJOUA  PIC X.                                          00001970
           02 MTIMJOUC  PIC X.                                          00001980
           02 MTIMJOUP  PIC X.                                          00001990
           02 MTIMJOUH  PIC X.                                          00002000
           02 MTIMJOUV  PIC X.                                          00002010
           02 MTIMJOUO  PIC X(5).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MPAGE1A   PIC X.                                          00002040
           02 MPAGE1C   PIC X.                                          00002050
           02 MPAGE1P   PIC X.                                          00002060
           02 MPAGE1H   PIC X.                                          00002070
           02 MPAGE1V   PIC X.                                          00002080
           02 MPAGE1O   PIC 999.                                        00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MPAGE2A   PIC X.                                          00002110
           02 MPAGE2C   PIC X.                                          00002120
           02 MPAGE2P   PIC X.                                          00002130
           02 MPAGE2H   PIC X.                                          00002140
           02 MPAGE2V   PIC X.                                          00002150
           02 MPAGE2O   PIC 999.                                        00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MLCENTREA      PIC X.                                     00002180
           02 MLCENTREC PIC X.                                          00002190
           02 MLCENTREP PIC X.                                          00002200
           02 MLCENTREH PIC X.                                          00002210
           02 MLCENTREV PIC X.                                          00002220
           02 MLCENTREO      PIC X(15).                                 00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MCTRAITA  PIC X.                                          00002250
           02 MCTRAITC  PIC X.                                          00002260
           02 MCTRAITP  PIC X.                                          00002270
           02 MCTRAITH  PIC X.                                          00002280
           02 MCTRAITV  PIC X.                                          00002290
           02 MCTRAITO  PIC X(5).                                       00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLTRAITA  PIC X.                                          00002320
           02 MLTRAITC  PIC X.                                          00002330
           02 MLTRAITP  PIC X.                                          00002340
           02 MLTRAITH  PIC X.                                          00002350
           02 MLTRAITV  PIC X.                                          00002360
           02 MLTRAITO  PIC X(20).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MLTIERSA  PIC X.                                          00002390
           02 MLTIERSC  PIC X.                                          00002400
           02 MLTIERSP  PIC X.                                          00002410
           02 MLTIERSH  PIC X.                                          00002420
           02 MLTIERSV  PIC X.                                          00002430
           02 MLTIERSO  PIC X(15).                                      00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCTIERSA  PIC X.                                          00002460
           02 MCTIERSC  PIC X.                                          00002470
           02 MCTIERSP  PIC X.                                          00002480
           02 MCTIERSH  PIC X.                                          00002490
           02 MCTIERSV  PIC X.                                          00002500
           02 MCTIERSO  PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MCRENDUA  PIC X.                                          00002530
           02 MCRENDUC  PIC X.                                          00002540
           02 MCRENDUP  PIC X.                                          00002550
           02 MCRENDUH  PIC X.                                          00002560
           02 MCRENDUV  PIC X.                                          00002570
           02 MCRENDUO  PIC X(5).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MLRENDUA  PIC X.                                          00002600
           02 MLRENDUC  PIC X.                                          00002610
           02 MLRENDUP  PIC X.                                          00002620
           02 MLRENDUH  PIC X.                                          00002630
           02 MLRENDUV  PIC X.                                          00002640
           02 MLRENDUO  PIC X(20).                                      00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MNRENDUA  PIC X.                                          00002670
           02 MNRENDUC  PIC X.                                          00002680
           02 MNRENDUP  PIC X.                                          00002690
           02 MNRENDUH  PIC X.                                          00002700
           02 MNRENDUV  PIC X.                                          00002710
           02 MNRENDUO  PIC X(20).                                      00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MDRENDUA  PIC X.                                          00002740
           02 MDRENDUC  PIC X.                                          00002750
           02 MDRENDUP  PIC X.                                          00002760
           02 MDRENDUH  PIC X.                                          00002770
           02 MDRENDUV  PIC X.                                          00002780
           02 MDRENDUO  PIC X(10).                                      00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MAUTOLGA  PIC X.                                          00002810
           02 MAUTOLGC  PIC X.                                          00002820
           02 MAUTOLGP  PIC X.                                          00002830
           02 MAUTOLGH  PIC X.                                          00002840
           02 MAUTOLGV  PIC X.                                          00002850
           02 MAUTOLGO  PIC X.                                          00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MGLOBALA  PIC X.                                          00002880
           02 MGLOBALC  PIC X.                                          00002890
           02 MGLOBALP  PIC X.                                          00002900
           02 MGLOBALH  PIC X.                                          00002910
           02 MGLOBALV  PIC X.                                          00002920
           02 MGLOBALO  PIC X.                                          00002930
           02 FILLER    PIC X(2).                                       00002940
           02 MLENVOIA  PIC X.                                          00002950
           02 MLENVOIC  PIC X.                                          00002960
           02 MLENVOIP  PIC X.                                          00002970
           02 MLENVOIH  PIC X.                                          00002980
           02 MLENVOIV  PIC X.                                          00002990
           02 MLENVOIO  PIC X(14).                                      00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MNENVOIA  PIC X.                                          00003020
           02 MNENVOIC  PIC X.                                          00003030
           02 MNENVOIP  PIC X.                                          00003040
           02 MNENVOIH  PIC X.                                          00003050
           02 MNENVOIV  PIC X.                                          00003060
           02 MNENVOIO  PIC 9999999.                                    00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MDUENVA   PIC X.                                          00003090
           02 MDUENVC   PIC X.                                          00003100
           02 MDUENVP   PIC X.                                          00003110
           02 MDUENVH   PIC X.                                          00003120
           02 MDUENVV   PIC X.                                          00003130
           02 MDUENVO   PIC X(2).                                       00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MDENVOIA  PIC X.                                          00003160
           02 MDENVOIC  PIC X.                                          00003170
           02 MDENVOIP  PIC X.                                          00003180
           02 MDENVOIH  PIC X.                                          00003190
           02 MDENVOIV  PIC X.                                          00003200
           02 MDENVOIO  PIC X(10).                                      00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MAUENVA   PIC X.                                          00003230
           02 MAUENVC   PIC X.                                          00003240
           02 MAUENVP   PIC X.                                          00003250
           02 MAUENVH   PIC X.                                          00003260
           02 MAUENVV   PIC X.                                          00003270
           02 MAUENVO   PIC X(2).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MDENVOIFA      PIC X.                                     00003300
           02 MDENVOIFC PIC X.                                          00003310
           02 MDENVOIFP PIC X.                                          00003320
           02 MDENVOIFH PIC X.                                          00003330
           02 MDENVOIFV PIC X.                                          00003340
           02 MDENVOIFO      PIC X(10).                                 00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MLACCORDA      PIC X.                                     00003370
           02 MLACCORDC PIC X.                                          00003380
           02 MLACCORDP PIC X.                                          00003390
           02 MLACCORDH PIC X.                                          00003400
           02 MLACCORDV PIC X.                                          00003410
           02 MLACCORDO      PIC X(14).                                 00003420
           02 FILLER    PIC X(2).                                       00003430
           02 MNACCORDA      PIC X.                                     00003440
           02 MNACCORDC PIC X.                                          00003450
           02 MNACCORDP PIC X.                                          00003460
           02 MNACCORDH PIC X.                                          00003470
           02 MNACCORDV PIC X.                                          00003480
           02 MNACCORDO      PIC 999999999999.                          00003490
           02 FILLER    PIC X(2).                                       00003500
           02 MDUACCA   PIC X.                                          00003510
           02 MDUACCC   PIC X.                                          00003520
           02 MDUACCP   PIC X.                                          00003530
           02 MDUACCH   PIC X.                                          00003540
           02 MDUACCV   PIC X.                                          00003550
           02 MDUACCO   PIC X(2).                                       00003560
           02 FILLER    PIC X(2).                                       00003570
           02 MDACCORDA      PIC X.                                     00003580
           02 MDACCORDC PIC X.                                          00003590
           02 MDACCORDP PIC X.                                          00003600
           02 MDACCORDH PIC X.                                          00003610
           02 MDACCORDV PIC X.                                          00003620
           02 MDACCORDO      PIC X(10).                                 00003630
           02 FILLER    PIC X(2).                                       00003640
           02 MLIB1A    PIC X.                                          00003650
           02 MLIB1C    PIC X.                                          00003660
           02 MLIB1P    PIC X.                                          00003670
           02 MLIB1H    PIC X.                                          00003680
           02 MLIB1V    PIC X.                                          00003690
           02 MLIB1O    PIC X(13).                                      00003700
           02 FILLER    PIC X(2).                                       00003710
           02 MTOTAVOA  PIC X.                                          00003720
           02 MTOTAVOC  PIC X.                                          00003730
           02 MTOTAVOP  PIC X.                                          00003740
           02 MTOTAVOH  PIC X.                                          00003750
           02 MTOTAVOV  PIC X.                                          00003760
           02 MTOTAVOO  PIC ZZZZZZ9,99.                                 00003770
           02 FILLER    PIC X(2).                                       00003780
           02 MDEVISEA  PIC X.                                          00003790
           02 MDEVISEC  PIC X.                                          00003800
           02 MDEVISEP  PIC X.                                          00003810
           02 MDEVISEH  PIC X.                                          00003820
           02 MDEVISEV  PIC X.                                          00003830
           02 MDEVISEO  PIC X(3).                                       00003840
           02 FILLER    PIC X(2).                                       00003850
           02 MLORIGA   PIC X.                                          00003860
           02 MLORIGC   PIC X.                                          00003870
           02 MLORIGP   PIC X.                                          00003880
           02 MLORIGH   PIC X.                                          00003890
           02 MLORIGV   PIC X.                                          00003900
           02 MLORIGO   PIC X(14).                                      00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MNLIEUHSA      PIC X.                                     00003930
           02 MNLIEUHSC PIC X.                                          00003940
           02 MNLIEUHSP PIC X.                                          00003950
           02 MNLIEUHSH PIC X.                                          00003960
           02 MNLIEUHSV PIC X.                                          00003970
           02 MNLIEUHSO      PIC 999.                                   00003980
           02 FILLER    PIC X(2).                                       00003990
           02 MNORIGINA      PIC X.                                     00004000
           02 MNORIGINC PIC X.                                          00004010
           02 MNORIGINP PIC X.                                          00004020
           02 MNORIGINH PIC X.                                          00004030
           02 MNORIGINV PIC X.                                          00004040
           02 MNORIGINO      PIC 9999999.                               00004050
           02 FILLER    PIC X(2).                                       00004060
           02 MLIB2A    PIC X.                                          00004070
           02 MLIB2C    PIC X.                                          00004080
           02 MLIB2P    PIC X.                                          00004090
           02 MLIB2H    PIC X.                                          00004100
           02 MLIB2V    PIC X.                                          00004110
           02 MLIB2O    PIC X(8).                                       00004120
           02 FILLER    PIC X(2).                                       00004130
           02 MLSELECTA      PIC X.                                     00004140
           02 MLSELECTC PIC X.                                          00004150
           02 MLSELECTP PIC X.                                          00004160
           02 MLSELECTH PIC X.                                          00004170
           02 MLSELECTV PIC X.                                          00004180
           02 MLSELECTO      PIC X.                                     00004190
           02 FILLER    PIC X(2).                                       00004200
           02 MLIB3A    PIC X.                                          00004210
           02 MLIB3C    PIC X.                                          00004220
           02 MLIB3P    PIC X.                                          00004230
           02 MLIB3H    PIC X.                                          00004240
           02 MLIB3V    PIC X.                                          00004250
           02 MLIB3O    PIC X(8).                                       00004260
           02 MAFFICHEO OCCURS   10 TIMES .                             00004270
             03 FILLER       PIC X(2).                                  00004280
             03 MSELECTA     PIC X.                                     00004290
             03 MSELECTC     PIC X.                                     00004300
             03 MSELECTP     PIC X.                                     00004310
             03 MSELECTH     PIC X.                                     00004320
             03 MSELECTV     PIC X.                                     00004330
             03 MSELECTO     PIC X.                                     00004340
             03 FILLER       PIC X(2).                                  00004350
             03 MLIGNEA      PIC X.                                     00004360
             03 MLIGNEC PIC X.                                          00004370
             03 MLIGNEP PIC X.                                          00004380
             03 MLIGNEH PIC X.                                          00004390
             03 MLIGNEV PIC X.                                          00004400
             03 MLIGNEO      PIC X(66).                                 00004410
             03 FILLER       PIC X(2).                                  00004420
             03 MVALEURA     PIC X.                                     00004430
             03 MVALEURC     PIC X.                                     00004440
             03 MVALEURP     PIC X.                                     00004450
             03 MVALEURH     PIC X.                                     00004460
             03 MVALEURV     PIC X.                                     00004470
             03 MVALEURO     PIC X(10).                                 00004480
      * MESSAGE ERREUR                                                  00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MLIBERRA  PIC X.                                          00004510
           02 MLIBERRC  PIC X.                                          00004520
           02 MLIBERRP  PIC X.                                          00004530
           02 MLIBERRH  PIC X.                                          00004540
           02 MLIBERRV  PIC X.                                          00004550
           02 MLIBERRO  PIC X(78).                                      00004560
      * CODE TRANSACTION                                                00004570
           02 FILLER    PIC X(2).                                       00004580
           02 MCODTRAA  PIC X.                                          00004590
           02 MCODTRAC  PIC X.                                          00004600
           02 MCODTRAP  PIC X.                                          00004610
           02 MCODTRAH  PIC X.                                          00004620
           02 MCODTRAV  PIC X.                                          00004630
           02 MCODTRAO  PIC X(4).                                       00004640
      * CICS DE TRAVAIL                                                 00004650
           02 FILLER    PIC X(2).                                       00004660
           02 MCICSA    PIC X.                                          00004670
           02 MCICSC    PIC X.                                          00004680
           02 MCICSP    PIC X.                                          00004690
           02 MCICSH    PIC X.                                          00004700
           02 MCICSV    PIC X.                                          00004710
           02 MCICSO    PIC X(5).                                       00004720
      * NETNAME                                                         00004730
           02 FILLER    PIC X(2).                                       00004740
           02 MNETNAMA  PIC X.                                          00004750
           02 MNETNAMC  PIC X.                                          00004760
           02 MNETNAMP  PIC X.                                          00004770
           02 MNETNAMH  PIC X.                                          00004780
           02 MNETNAMV  PIC X.                                          00004790
           02 MNETNAMO  PIC X(8).                                       00004800
      * CODE TERMINAL                                                   00004810
           02 FILLER    PIC X(2).                                       00004820
           02 MSCREENA  PIC X.                                          00004830
           02 MSCREENC  PIC X.                                          00004840
           02 MSCREENP  PIC X.                                          00004850
           02 MSCREENH  PIC X.                                          00004860
           02 MSCREENV  PIC X.                                          00004870
           02 MSCREENO  PIC X(5).                                       00004880
                                                                                
