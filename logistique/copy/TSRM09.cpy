      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00010000
      *             REAPPROVISONNEMENT MAGASIN                       *  00030000
      *         - TRANSACTION RM09                                   *  00040001
      *           (CREATION/MISE A JOUR DES LOIS)                    *  00041001
      ****************************************************************  00050000
       01  (P)-LONG        PIC S9(4) COMP-3 VALUE +2267.                00080007
       01  (P)-DONNEES.                                                 00090000
           02 FILLER       PIC X(12).                                   00750001
           02 FILLER       PIC X(2).                                    00760001
           02 (P)-MDATJOUA PIC X.                                       00770001
           02 (P)-MDATJOUC PIC X.                                       00780001
           02 (P)-MDATJOUP PIC X.                                       00790001
           02 (P)-MDATJOUH PIC X.                                       00800001
           02 (P)-MDATJOUV PIC X.                                       00810001
           02 (P)-MDATJOUO PIC X(10).                                   00820001
           02 FILLER       PIC X(2).                                    00830001
           02 (P)-MTIMJOUA PIC X.                                       00840001
           02 (P)-MTIMJOUC PIC X.                                       00850001
           02 (P)-MTIMJOUP PIC X.                                       00860001
           02 (P)-MTIMJOUH PIC X.                                       00870001
           02 (P)-MTIMJOUV PIC X.                                       00880001
           02 (P)-MTIMJOUO PIC X(5).                                    00890001
           02 FILLER       PIC X(2).                                    00900001
           02 (P)-MNPAGEA PIC X.                                        00910001
           02 (P)-MNPAGEC PIC X.                                        00920001
           02 (P)-MNPAGEP PIC X.                                        00930001
           02 (P)-MNPAGEH PIC X.                                        00940001
           02 (P)-MNPAGEV PIC X.                                        00950001
           02 (P)-MNPAGEO PIC X(3).                                     00960001
           02 (P)-L-MTINPOO.                                            00970003
              03 FILLER OCCURS 10.                                      00971003
                 05 FILLER       PIC X(2).                              00980003
                 05 (P)-MTINPOA  PIC X.                                 00990003
                 05 (P)-MTINPOC  PIC X.                                 01000003
                 05 (P)-MTINPOP  PIC X.                                 01010003
                 05 (P)-MTINPOH  PIC X.                                 01020003
                 05 (P)-MTINPOV  PIC X.                                 01030003
                 05 (P)-MTINPOO  PIC X(5).                              01040003
           02 (P)-LIGNEO OCCURS   14 TIMES .                            01050005
             03 FILLER     PIC X(2).                                    01060001
             03 (P)-MQV8SA PIC X.                                       01070001
             03 (P)-MQV8SC PIC X.                                       01080001
             03 (P)-MQV8SP PIC X.                                       01090001
             03 (P)-MQV8SH PIC X.                                       01100001
             03 (P)-MQV8SV PIC X.                                       01110001
             03 (P)-MQV8SO PIC X(5).                                    01120001
             03 FILLER OCCURS 10.                                       01130001
                05 FILLER        PIC X(2).                              01140001
                05 (P)-MQSOBJA   PIC X.                                 01150001
                05 (P)-MQSOBJC   PIC X.                                 01160001
                05 (P)-MQSOBJP   PIC X.                                 01170001
                05 (P)-MQSOBJH   PIC X.                                 01180001
                05 (P)-MQSOBJV   PIC X.                                 01190001
                05 (P)-MQSOBJO   PIC X(5).                              01200001
             03 FILLER        PIC X(2).                                 01201007
             03 (P)-MWREPEATA PIC X.                                    01210001
             03 (P)-MWREPEATC PIC X.                                    01220001
             03 (P)-MWREPEATP PIC X.                                    01230001
             03 (P)-MWREPEATH PIC X.                                    01240001
             03 (P)-MWREPEATV PIC X.                                    01250001
             03 (P)-MWREPEATO PIC X.                                    01260001
           02 FILLER       PIC X(2).                                    01270001
           02 (P)-MZONCMDA PIC X.                                       01280001
           02 (P)-MZONCMDC PIC X.                                       01290001
           02 (P)-MZONCMDP PIC X.                                       01300001
           02 (P)-MZONCMDH PIC X.                                       01310001
           02 (P)-MZONCMDV PIC X.                                       01320001
           02 (P)-MZONCMDO PIC X(12).                                   01330001
           02 FILLER       PIC X(2).                                    01340001
           02 (P)-MLIBERRA PIC X.                                       01350001
           02 (P)-MLIBERRC PIC X.                                       01360001
           02 (P)-MLIBERRP PIC X.                                       01370001
           02 (P)-MLIBERRH PIC X.                                       01380001
           02 (P)-MLIBERRV PIC X.                                       01390001
           02 (P)-MLIBERRO PIC X(61).                                   01400001
           02 FILLER       PIC X(2).                                    01410001
           02 (P)-MCODTRAA PIC X.                                       01420001
           02 (P)-MCODTRAC PIC X.                                       01430001
           02 (P)-MCODTRAP PIC X.                                       01440001
           02 (P)-MCODTRAH PIC X.                                       01450001
           02 (P)-MCODTRAV PIC X.                                       01460001
           02 (P)-MCODTRAO PIC X(4).                                    01470001
           02 FILLER       PIC X(2).                                    01480001
           02 (P)-MCICSA PIC X.                                         01490001
           02 (P)-MCICSC PIC X.                                         01500001
           02 (P)-MCICSP PIC X.                                         01510001
           02 (P)-MCICSH PIC X.                                         01520001
           02 (P)-MCICSV PIC X.                                         01530001
           02 (P)-MCICSO PIC X(5).                                      01540001
           02 FILLER       PIC X(2).                                    01550001
           02 (P)-MNETNAMA PIC X.                                       01560001
           02 (P)-MNETNAMC PIC X.                                       01570001
           02 (P)-MNETNAMP PIC X.                                       01580001
           02 (P)-MNETNAMH PIC X.                                       01590001
           02 (P)-MNETNAMV PIC X.                                       01600001
           02 (P)-MNETNAMO PIC X(8).                                    01610001
           02 FILLER       PIC X(2).                                    01620001
           02 (P)-MSCREENA PIC X.                                       01630001
           02 (P)-MSCREENC PIC X.                                       01640001
           02 (P)-MSCREENP PIC X.                                       01650001
           02 (P)-MSCREENH PIC X.                                       01660001
           02 (P)-MSCREENV PIC X.                                       01670001
           02 (P)-MSCREENO PIC X(4).                                    01680001
                                                                                
