      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GCT - TABLES GENERALISEES                                       00000020
      ***************************************************************** 00000030
       01   EGS30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X.                                          00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X.                                          00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTABLEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTABLEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTABLEF   PIC X.                                          00000150
           02 FILLER    PIC X.                                          00000160
           02 MTABLEI   PIC X(34).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X.                                          00000200
           02 MPAGEI    PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000230
           02 FILLER    PIC X.                                          00000240
           02 MNBPI     PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINE1L   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLINE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLINE1F   PIC X.                                          00000270
           02 FILLER    PIC X.                                          00000280
           02 MLINE1I   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTETEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MENTETEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENTETEF  PIC X.                                          00000310
           02 FILLER    PIC X.                                          00000320
           02 MENTETEI  PIC X(79).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINE2L   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLINE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLINE2F   PIC X.                                          00000350
           02 FILLER    PIC X.                                          00000360
           02 MLINE2I   PIC X(12).                                      00000370
           02 FILLER  OCCURS   15 TIMES .                               00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNEL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MLIGNEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIGNEF      PIC X.                                     00000400
             03 FILLER  PIC X.                                          00000410
             03 MLIGNEI      PIC X(77).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000440
           02 FILLER    PIC X.                                          00000450
           02 MLIBERRI  PIC X(79).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000480
           02 FILLER    PIC X.                                          00000490
           02 MCODTRAI  PIC X(4).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000520
           02 FILLER    PIC X.                                          00000530
           02 MZONCMDI  PIC X(15).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000560
           02 FILLER    PIC X.                                          00000570
           02 MCICSI    PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000600
           02 FILLER    PIC X.                                          00000610
           02 MNETNAMI  PIC X(8).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000640
           02 FILLER    PIC X.                                          00000650
           02 MSCREENI  PIC X(4).                                       00000660
      ***************************************************************** 00000670
      * GCT - TABLES GENERALISEES                                       00000680
      ***************************************************************** 00000690
       01   EGS30O REDEFINES EGS30I.                                    00000700
           02 FILLER    PIC X(12).                                      00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MDATJOUA  PIC X.                                          00000730
           02 MDATJOUC  PIC X.                                          00000740
           02 MDATJOUO  PIC X(10).                                      00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MTIMJOUA  PIC X.                                          00000770
           02 MTIMJOUC  PIC X.                                          00000780
           02 MTIMJOUO  PIC X(5).                                       00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MTABLEA   PIC X.                                          00000810
           02 MTABLEC   PIC X.                                          00000820
           02 MTABLEO   PIC X(34).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MPAGEA    PIC X.                                          00000850
           02 MPAGEC    PIC X.                                          00000860
           02 MPAGEO    PIC Z9.                                         00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MNBPA     PIC X.                                          00000890
           02 MNBPC     PIC X.                                          00000900
           02 MNBPO     PIC Z9.                                         00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MLINE1A   PIC X.                                          00000930
           02 MLINE1C   PIC X.                                          00000940
           02 MLINE1O   PIC X(5).                                       00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MENTETEA  PIC X.                                          00000970
           02 MENTETEC  PIC X.                                          00000980
           02 MENTETEO  PIC X(79).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MLINE2A   PIC X.                                          00001010
           02 MLINE2C   PIC X.                                          00001020
           02 MLINE2O   PIC X(12).                                      00001030
           02 FILLER  OCCURS   15 TIMES .                               00001040
             03 FILLER       PIC X(2).                                  00001050
             03 MLIGNEA      PIC X.                                     00001060
             03 MLIGNEC PIC X.                                          00001070
             03 MLIGNEO      PIC X(77).                                 00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MLIBERRA  PIC X.                                          00001100
           02 MLIBERRC  PIC X.                                          00001110
           02 MLIBERRO  PIC X(79).                                      00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MCODTRAA  PIC X.                                          00001140
           02 MCODTRAC  PIC X.                                          00001150
           02 MCODTRAO  PIC X(4).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MZONCMDA  PIC X.                                          00001180
           02 MZONCMDC  PIC X.                                          00001190
           02 MZONCMDO  PIC X(15).                                      00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MCICSA    PIC X.                                          00001220
           02 MCICSC    PIC X.                                          00001230
           02 MCICSO    PIC X(5).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MNETNAMA  PIC X.                                          00001260
           02 MNETNAMC  PIC X.                                          00001270
           02 MNETNAMO  PIC X(8).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MSCREENA  PIC X.                                          00001300
           02 MSCREENC  PIC X.                                          00001310
           02 MSCREENO  PIC X(4).                                       00001320
                                                                                
