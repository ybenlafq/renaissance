      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM3501                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM3501                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM3501.                                                            
           02  RM35-DSIMULATION                                                 
               PIC X(0008).                                                     
           02  RM35-NSIMULATION                                                 
               PIC X(0003).                                                     
           02  RM35-CGROUP                                                      
               PIC X(0005).                                                     
           02  RM35-CDISTRIBUTION                                               
               PIC X(0001).                                                     
           02  RM35-DEFFET                                                      
               PIC X(0008).                                                     
           02  RM35-DFINEFFET                                                   
               PIC X(0008).                                                     
           02  RM35-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  RM35-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  RM35-NDEPOT                                                      
               PIC X(0003).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM3501                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM3501-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM35-DSIMULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM35-DSIMULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM35-NSIMULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM35-NSIMULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM35-CGROUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM35-CGROUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM35-CDISTRIBUTION-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM35-CDISTRIBUTION-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM35-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM35-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM35-DFINEFFET-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM35-DFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM35-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM35-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM35-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM35-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM35-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM35-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
