      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE EF9000                       
      ******************************************************************        
      *                                                                         
       CLEF-EF9000             SECTION.                                         
      *                                                                         
           MOVE 'RVEF9000          '       TO   TABLE-NAME.                     
           MOVE 'EF9000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-EF9000. EXIT.                                                   
                EJECT                                                           
                                                                                
