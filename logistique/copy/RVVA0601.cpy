      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVVA0601                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVA0601                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVVA0601.                                                            
           02  VA06-NCODIC                                                      
               PIC X(0007).                                                     
           02  VA06-NSOCVALO                                                    
               PIC X(0003).                                                     
           02  VA06-NLIEUVALO                                                   
               PIC X(0003).                                                     
           02  VA06-QSTOCKINIT                                                  
               PIC S9(11) COMP-3.                                               
           02  VA06-QSTOCKSORTIE                                                
               PIC S9(11) COMP-3.                                               
           02  VA06-QSTOCKENTREE                                                
               PIC S9(11) COMP-3.                                               
           02  VA06-QSTOCKFINAL                                                 
               PIC S9(11) COMP-3.                                               
           02  VA06-PSTOCKINIT                                                  
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA06-PSTOCKSORTIE                                                
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA06-PSTOCKENTREE                                                
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA06-PSTOCKFINAL                                                 
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA06-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  VA06-NSOCCOMPT                                                   
               PIC X(0003).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVVA0601                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVVA0601-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-NSOCVALO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-NSOCVALO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-NLIEUVALO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-NLIEUVALO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-QSTOCKINIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-QSTOCKINIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-QSTOCKSORTIE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-QSTOCKSORTIE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-QSTOCKENTREE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-QSTOCKENTREE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-QSTOCKFINAL-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-QSTOCKFINAL-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-PSTOCKINIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-PSTOCKINIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-PSTOCKSORTIE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-PSTOCKSORTIE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-PSTOCKENTREE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-PSTOCKENTREE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-PSTOCKFINAL-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-PSTOCKFINAL-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA06-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA06-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
