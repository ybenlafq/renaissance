      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : DEFINITION DES PROFILS D'AFFILIATION (FAM / ENTREPOT ) *         
      *        POUR MISE A JOUR VUE RVGQ0500     (PGR : TFL11)        *         
      *****************************************************************         
      *                                                               *         
       01  TS-FL11.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-FL11-LONG                 PIC S9(3) COMP-3                     
                                           VALUE +726.                          
      *----------------------------------  DONNEES  TS                          
           02 TS-FL11-DONNEES.                                                  
      *----------------------------------  MODE DE MAJ                          
      *                                         ' ' : PAS DE MAJ                
      *                                         'C' : CREATION                  
      *                                         'M' : MODIFICATION              
              03 TS-FL11-CMAJ              PIC X(1).                            
      *----------------------------------  CODE FAMILLE                         
              03 TS-FL11-CFAM              PIC X(5).                            
      *----------------------------------  LIBELLE FAMILLE                      
              03 TS-FL11-LFAM              PIC X(20).                           
      *----------------------------------  ZONE ENTREPOTS D'AFFILIATION         
              03 TS-FL11-ZONAFF.                                                
      *----------------------------------  ZONE ENTREPOTS D'AFFILIATION         
              04 TS-FL11-ENTAFF            OCCURS 100.                          
      *----------------------------------  CODE SOCIETE                         
                 05 TS-FL11-CSOCAFF        PIC X(03).                           
      *----------------------------------  CODE ENTREPOT                        
                 05 TS-FL11-CDEPAFF        PIC X(03).                           
                                                                                
