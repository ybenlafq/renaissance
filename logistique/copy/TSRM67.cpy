      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM67                                          *  00020002
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-LONG              PIC S9(3) COMP-3 VALUE 92.              00060003
       01  TS-DONNEES.                                                  00070000
              10 TS-MNCODIC      PIC X(07).                             00080000
              10 TS-MLREF        PIC X(20).                             00090003
              10 TS-CGROUP       PIC X(5).                              00100000
              10 TS-QPVRM1       PIC X(6).                              00110000
              10 TS-QPVA1        PIC X(6).                              00120000
              10 TS-QPVN1        PIC X(6).                              00130000
              10 TS-MODIF1       PIC X(1).                              00140000
              10 TS-INSERT1      PIC X(1).                              00150000
              10 TS-QPVRM2       PIC X(6).                              00160000
              10 TS-QPVA2        PIC X(6).                              00170000
              10 TS-QPVN2        PIC X(6).                              00180000
              10 TS-MODIF2       PIC X(1).                              00190000
              10 TS-INSERT2      PIC X(1).                              00200000
              10 TS-QPVRM3       PIC X(6).                              00210000
              10 TS-QPVA3        PIC X(6).                              00220000
              10 TS-QPVN3        PIC X(6).                              00230000
              10 TS-MODIF3       PIC X(1).                              00240000
              10 TS-INSERT3      PIC X(1).                              00250000
                                                                                
