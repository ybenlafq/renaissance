      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ETL60   ETL60                                              00000020
      ***************************************************************** 00000030
       01   ETL60I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MYYL      COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MYYL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MYYF      PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MYYI      PIC XX.                                         00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MXXL      COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MXXL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MXXF      PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MXXI      PIC XX.                                         00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLIEUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIEUF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIEUI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPLANL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MPLANL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPLANF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MPLANI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEBSSAAL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MDEBSSAAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEBSSAAF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDEBSSAAI      PIC XXXX.                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEBNUL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDEBNUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEBNUF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEBNUI   PIC XX.                                         00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPROFILL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPROFILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPROFILF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPROFILI  PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNJOURSL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNJOURSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNJOURSF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNJOURSI  PIC XX.                                         00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANNEE1L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MANNEE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MANNEE1F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MANNEE1I  PIC X(4).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEM1L    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSEM1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSEM1F    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSEM1I    PIC X(2).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANNEE2L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MANNEE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MANNEE2F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MANNEE2I  PIC X(4).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEM2L    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSEM2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSEM2F    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSEM2I    PIC X(2).                                       00000650
           02 MDATESI OCCURS   2 TIMES .                                00000660
             03 MDATED OCCURS   14 TIMES .                              00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDATEL     COMP PIC S9(4).                            00000680
      *--                                                                       
               04 MDATEL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MDATEF     PIC X.                                     00000690
               04 FILLER     PIC X(4).                                  00000700
               04 MDATEI     PIC X(2).                                  00000710
           02 MCODESI OCCURS   8 TIMES .                                00000720
             03 MCODED OCCURS   14 TIMES .                              00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCODEL     COMP PIC S9(4).                            00000740
      *--                                                                       
               04 MCODEL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MCODEF     PIC X.                                     00000750
               04 FILLER     PIC X(4).                                  00000760
               04 MCODEI     PIC X(2).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBERRI  PIC X(78).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCODTRAI  PIC X(4).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MZONCMDI  PIC X(19).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCICSI    PIC X(5).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNETNAMI  PIC X(8).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MSCREENI  PIC X(4).                                       00001010
      ***************************************************************** 00001020
      * SDF: ETL60   ETL60                                              00001030
      ***************************************************************** 00001040
       01   ETL60O REDEFINES ETL60I.                                    00001050
           02 FILLER    PIC X(12).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MDATJOUA  PIC X.                                          00001080
           02 MDATJOUC  PIC X.                                          00001090
           02 MDATJOUP  PIC X.                                          00001100
           02 MDATJOUH  PIC X.                                          00001110
           02 MDATJOUV  PIC X.                                          00001120
           02 MDATJOUO  PIC X(10).                                      00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MTIMJOUA  PIC X.                                          00001150
           02 MTIMJOUC  PIC X.                                          00001160
           02 MTIMJOUP  PIC X.                                          00001170
           02 MTIMJOUH  PIC X.                                          00001180
           02 MTIMJOUV  PIC X.                                          00001190
           02 MTIMJOUO  PIC X(5).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MYYA      PIC X.                                          00001220
           02 MYYC      PIC X.                                          00001230
           02 MYYP      PIC X.                                          00001240
           02 MYYH      PIC X.                                          00001250
           02 MYYV      PIC X.                                          00001260
           02 MYYO      PIC 99.                                         00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MXXA      PIC X.                                          00001290
           02 MXXC      PIC X.                                          00001300
           02 MXXP      PIC X.                                          00001310
           02 MXXH      PIC X.                                          00001320
           02 MXXV      PIC X.                                          00001330
           02 MXXO      PIC 99.                                         00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNSOCA    PIC X.                                          00001360
           02 MNSOCC    PIC X.                                          00001370
           02 MNSOCP    PIC X.                                          00001380
           02 MNSOCH    PIC X.                                          00001390
           02 MNSOCV    PIC X.                                          00001400
           02 MNSOCO    PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLIEUA    PIC X.                                          00001430
           02 MLIEUC    PIC X.                                          00001440
           02 MLIEUP    PIC X.                                          00001450
           02 MLIEUH    PIC X.                                          00001460
           02 MLIEUV    PIC X.                                          00001470
           02 MLIEUO    PIC X(3).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MPLANA    PIC X.                                          00001500
           02 MPLANC    PIC X.                                          00001510
           02 MPLANP    PIC X.                                          00001520
           02 MPLANH    PIC X.                                          00001530
           02 MPLANV    PIC X.                                          00001540
           02 MPLANO    PIC X(5).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDEBSSAAA      PIC X.                                     00001570
           02 MDEBSSAAC PIC X.                                          00001580
           02 MDEBSSAAP PIC X.                                          00001590
           02 MDEBSSAAH PIC X.                                          00001600
           02 MDEBSSAAV PIC X.                                          00001610
           02 MDEBSSAAO      PIC 9999.                                  00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MDEBNUA   PIC X.                                          00001640
           02 MDEBNUC   PIC X.                                          00001650
           02 MDEBNUP   PIC X.                                          00001660
           02 MDEBNUH   PIC X.                                          00001670
           02 MDEBNUV   PIC X.                                          00001680
           02 MDEBNUO   PIC 99.                                         00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MPROFILA  PIC X.                                          00001710
           02 MPROFILC  PIC X.                                          00001720
           02 MPROFILP  PIC X.                                          00001730
           02 MPROFILH  PIC X.                                          00001740
           02 MPROFILV  PIC X.                                          00001750
           02 MPROFILO  PIC 9.                                          00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNJOURSA  PIC X.                                          00001780
           02 MNJOURSC  PIC X.                                          00001790
           02 MNJOURSP  PIC X.                                          00001800
           02 MNJOURSH  PIC X.                                          00001810
           02 MNJOURSV  PIC X.                                          00001820
           02 MNJOURSO  PIC 99.                                         00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MANNEE1A  PIC X.                                          00001850
           02 MANNEE1C  PIC X.                                          00001860
           02 MANNEE1P  PIC X.                                          00001870
           02 MANNEE1H  PIC X.                                          00001880
           02 MANNEE1V  PIC X.                                          00001890
           02 MANNEE1O  PIC X(4).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MSEM1A    PIC X.                                          00001920
           02 MSEM1C    PIC X.                                          00001930
           02 MSEM1P    PIC X.                                          00001940
           02 MSEM1H    PIC X.                                          00001950
           02 MSEM1V    PIC X.                                          00001960
           02 MSEM1O    PIC X(2).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MANNEE2A  PIC X.                                          00001990
           02 MANNEE2C  PIC X.                                          00002000
           02 MANNEE2P  PIC X.                                          00002010
           02 MANNEE2H  PIC X.                                          00002020
           02 MANNEE2V  PIC X.                                          00002030
           02 MANNEE2O  PIC X(4).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MSEM2A    PIC X.                                          00002060
           02 MSEM2C    PIC X.                                          00002070
           02 MSEM2P    PIC X.                                          00002080
           02 MSEM2H    PIC X.                                          00002090
           02 MSEM2V    PIC X.                                          00002100
           02 MSEM2O    PIC X(2).                                       00002110
           02 MDATESO OCCURS   2 TIMES .                                00002120
             03 DFHMS1 OCCURS   14 TIMES .                              00002130
               04 FILLER     PIC X(2).                                  00002140
               04 MDATEA     PIC X.                                     00002150
               04 MDATEC     PIC X.                                     00002160
               04 MDATEP     PIC X.                                     00002170
               04 MDATEH     PIC X.                                     00002180
               04 MDATEV     PIC X.                                     00002190
               04 MDATEO     PIC X(2).                                  00002200
           02 MCODESO OCCURS   8 TIMES .                                00002210
             03 DFHMS2 OCCURS   14 TIMES .                              00002220
               04 FILLER     PIC X(2).                                  00002230
               04 MCODEA     PIC X.                                     00002240
               04 MCODEC     PIC X.                                     00002250
               04 MCODEP     PIC X.                                     00002260
               04 MCODEH     PIC X.                                     00002270
               04 MCODEV     PIC X.                                     00002280
               04 MCODEO     PIC X(2).                                  00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLIBERRA  PIC X.                                          00002310
           02 MLIBERRC  PIC X.                                          00002320
           02 MLIBERRP  PIC X.                                          00002330
           02 MLIBERRH  PIC X.                                          00002340
           02 MLIBERRV  PIC X.                                          00002350
           02 MLIBERRO  PIC X(78).                                      00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MCODTRAA  PIC X.                                          00002380
           02 MCODTRAC  PIC X.                                          00002390
           02 MCODTRAP  PIC X.                                          00002400
           02 MCODTRAH  PIC X.                                          00002410
           02 MCODTRAV  PIC X.                                          00002420
           02 MCODTRAO  PIC X(4).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MZONCMDA  PIC X.                                          00002450
           02 MZONCMDC  PIC X.                                          00002460
           02 MZONCMDP  PIC X.                                          00002470
           02 MZONCMDH  PIC X.                                          00002480
           02 MZONCMDV  PIC X.                                          00002490
           02 MZONCMDO  PIC X(19).                                      00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MCICSA    PIC X.                                          00002520
           02 MCICSC    PIC X.                                          00002530
           02 MCICSP    PIC X.                                          00002540
           02 MCICSH    PIC X.                                          00002550
           02 MCICSV    PIC X.                                          00002560
           02 MCICSO    PIC X(5).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MNETNAMA  PIC X.                                          00002590
           02 MNETNAMC  PIC X.                                          00002600
           02 MNETNAMP  PIC X.                                          00002610
           02 MNETNAMH  PIC X.                                          00002620
           02 MNETNAMV  PIC X.                                          00002630
           02 MNETNAMO  PIC X(8).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MSCREENA  PIC X.                                          00002660
           02 MSCREENC  PIC X.                                          00002670
           02 MSCREENP  PIC X.                                          00002680
           02 MSCREENH  PIC X.                                          00002690
           02 MSCREENV  PIC X.                                          00002700
           02 MSCREENO  PIC X(4).                                       00002710
                                                                                
