      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA600      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,04,PD,A,                          *        
      *                           14,03,BI,A,                          *        
      *                           17,06,BI,A,                          *        
      *                           23,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA600.                                                        
            05 NOMETAT-IVA600           PIC X(6) VALUE 'IVA600'.                
            05 RUPTURES-IVA600.                                                 
           10 IVA600-NSOCIETE           PIC X(03).                      007  003
           10 IVA600-NSEQPRO            PIC S9(07)      COMP-3.         010  004
           10 IVA600-NSOCMVT            PIC X(03).                      014  003
           10 IVA600-NLIEUMVT-CCONSO    PIC X(06).                      017  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA600-SEQUENCE           PIC S9(04) COMP.                023  002
      *--                                                                       
           10 IVA600-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA600.                                                   
           10 IVA600-LLIEU              PIC X(20).                      025  020
           10 IVA600-LSEQPRO            PIC X(26).                      045  026
           10 IVA600-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         071  008
           10 IVA600-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         079  008
           10 IVA600-QSTOCKENTREE       PIC S9(11)      COMP-3.         087  006
           10 IVA600-QSTOCKSORTIE       PIC S9(11)      COMP-3.         093  006
            05 FILLER                      PIC X(414).                          
                                                                                
