      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : APPLICATION LOGISTIQUE PARTENAIRE                *        
      *  PROGRAMMES : MLW099                                           *        
      *  TITRE      : COMMAREA DU MODULE D'ALIMENTATION TABLE RTLW99   *        
      *                   TABLE COMPTE-RENDU ACTIVITE DES CHAINES      *        
      *  LONGUEUR   :      C                                           *        
      *                                                                *        
      ******************************************************************        
       01  COMM-LW99-APPLI.                                                     
      *--- DONNEES EN ENTREE DU MODULE ------------------------------ 12        
           05 COMM-LW99-DONNEES-ENTREE.                                         
              10 COMM-LW99-CPROGRAMME       PIC X(06).                          
              10 COMM-LW99-NSOCIETE         PIC X(03).                          
              10 COMM-LW99-NLIEU            PIC X(03).                          
              10 COMM-LW99-DTRAIT           PIC X(08).                          
              10 COMM-LW99-NSEQ             PIC X(03).                          
              10 COMM-LW99-LTRAIT           PIC X(25).                          
              10 COMM-LW99-QTE1             PIC 9(7) COMP-3.                    
              10 COMM-LW99-QTE2             PIC 9(7) COMP-3.                    
      *--- CODE RETOUR + MESSAGE ------------------------------------ 59        
           05 COMM-LW99-MESSAGE.                                                
              10 COMM-LW99-CODRET           PIC X(01).                          
                 88 COMM-LW99-CODRET-OK                  VALUE ' '.             
                 88 COMM-LW99-CODRET-ERREUR              VALUE '1'.             
              10 COMM-LW99-LIBERR           PIC X(58).                          
                                                                                
