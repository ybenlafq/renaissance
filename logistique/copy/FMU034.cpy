      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DES COMMANDES OPCO              *        
      *----------------------------------------------------------------*        
       01  DSECT-FMU034.                                                        
           05 CHAMPS-FMU034.                                                    
              10 FMU034-NSOCDEP            PIC X(03).                           
              10 FILLER                    PIC X VALUE ';'.                     
              10 FMU034-NDEPOT             PIC X(03).                           
              10 FILLER                    PIC X VALUE ';'.                     
              10 FMU034-NCDE               PIC X(07).                           
              10 FILLER                    PIC X VALUE ';'.                     
              10 FMU034-CFAM               PIC X(05).                           
              10 FILLER                    PIC X VALUE ';'.                     
              10 FMU034-CMARQ              PIC X(05).                           
              10 FILLER                    PIC X VALUE ';'.                     
              10 FMU034-NCODIC             PIC X(07).                           
              10 FILLER                    PIC X VALUE ';'.                     
              10 FMU034-LREFFOURN          PIC X(20).                           
              10 FILLER                    PIC X VALUE ';'.                     
              10 FMU034-DLIVRAISON         PIC X(08).                           
           05 FMU034-QTES.                                                      
              10 FMU034-OCC OCCURS 21.                                          
                 15 FMU034-PV1                PIC X(01).                        
                 15 FMU034-QTE                PIC Z(10).                        
           05 FMU034-QTES-R REDEFINES FMU034-QTES.                              
              10 FMU034-OCC-R OCCURS 21.                                        
                 15 FMU034-PV1-R              PIC X(01).                        
                 15 FMU034-QTE-R              PIC X(10).                        
       01  FMU034-LIBELLES.                                                     
           05 FILLER                       PIC X(03) VALUE 'SOC'.               
           05 FILLER                       PIC X(01) VALUE ';'.                 
           05 FILLER                       PIC X(05) VALUE 'DEPOT'.             
           05 FILLER                       PIC X(01) VALUE ';'.                 
           05 FILLER                       PIC X(04) VALUE 'NCDE'.              
           05 FILLER                       PIC X(01) VALUE ';'.                 
           05 FILLER                       PIC X(04) VALUE 'CFAM'.              
           05 FILLER                       PIC X(01) VALUE ';'.                 
           05 FILLER                       PIC X(05) VALUE 'CMARQ'.             
           05 FILLER                       PIC X(01) VALUE ';'.                 
           05 FILLER                       PIC X(06) VALUE 'NCODIC'.            
           05 FILLER                       PIC X(01) VALUE ';'.                 
           05 FILLER                       PIC X(09) VALUE 'LREFFOURN'.         
           05 FILLER                       PIC X(01) VALUE ';'.                 
           05 FILLER                       PIC X(10) VALUE 'DLIVRAISON'.        
           05 FMU034-CDES OCCURS 21.                                            
              10 FMU034-PV2                PIC X(01) VALUE SPACES.              
              10 FMU034-QSOLDE             PIC X(06) VALUE SPACES.              
              10 FILLER                    PIC X(01) VALUE SPACES.              
              10 FMU034-SOC                PIC X(03) VALUE SPACES.              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FMU034-LONG               PIC S9(4) COMP  VALUE +284.          
      *                                                                         
      *--                                                                       
       01  DSECT-FMU034-LONG               PIC S9(4) COMP-5  VALUE +284.        
                                                                                
      *}                                                                        
