      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EFEXL TIERS EXCLUANT MAJ RTEF00        *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVEFEXL .                                                            
           05  EFEXL-CTABLEG2    PIC X(15).                                     
           05  EFEXL-CTABLEG2-REDEF REDEFINES EFEXL-CTABLEG2.                   
               10  EFEXL-CTIERS          PIC X(05).                             
           05  EFEXL-WTABLEG     PIC X(80).                                     
           05  EFEXL-WTABLEG-REDEF  REDEFINES EFEXL-WTABLEG.                    
               10  EFEXL-WFLAG           PIC X(01).                             
               10  EFEXL-COMMENT         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVEFEXL-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFEXL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EFEXL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EFEXL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EFEXL-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
