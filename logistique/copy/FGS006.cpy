      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00002000
      * FICHIER DE SORTIE DES MOUVEMENTS DE REGULS DE STOCKS RPMGD     *00002209
      ******************************************************************        
      *   DATES    * USERID * NOMS            * FAIRE FIND                      
      ******************************************************************        
      * JJ/MM/SSAA * DSAXXX * X.XXXXXXXXXXXXX * XXXXXXXXXXXXXXXXXX              
      *-----------------------------------------------------------------        
      *                                                                         
      ******************************************************************        
       01  WS-FGS006-ENR.                                                       
           10  FGS006-NCODIC          PIC X(07)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-CFAM            PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-CMARQ           PIC X(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-LREFFOURN       PIC X(20)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-LSTATUT         PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-NSOCORIG        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-NLIEUORIG       PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-NSOCDEST        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-NLIEUDEST       PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-DMVT            PIC X(08)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-QMVT            PIC -(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-PRMP            PIC -(9)9,9(2) VALUE ZERO.                
           10  FGS006-PRMP-0 REDEFINES FGS006-PRMP                              
                                      PIC -(10),-(2).                           
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS006-VAL             PIC -(9)9,9(2) VALUE ZERO.                
           10  FGS006-VAL-0 REDEFINES FGS006-VAL                                
                                      PIC -(10),-(2).                           
                                                                                
