      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM8000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM8000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM8000.                                                            
           02  RM80-CGROUP                                                      
               PIC X(0005).                                                     
           02  RM80-NCODIC                                                      
               PIC X(0007).                                                     
           02  RM80-DSEMAINE                                                    
               PIC X(0006).                                                     
           02  RM80-QPV                                                         
               PIC S9(7) COMP-3.                                                
           02  RM80-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM8000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM8000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM80-CGROUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM80-CGROUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM80-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM80-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM80-DSEMAINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM80-DSEMAINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM80-QPV-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM80-QPV-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM80-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM80-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
