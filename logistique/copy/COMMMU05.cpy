      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * ------------------------------------------------------------ *          
      * ZONE DE COMMUNICATION POUR APPEL MODULE MMU05                *          
      * SECURISATION QUOTA DE MUTATION                               *          
      * ------------------------------------------------------------ *          
       01 COMMAREA-MU05.                                                        
      * - PROGRAMME APPELANT.                                                   
          02 CO-MU05-NMUTATION         PIC X(07).                               
      * - RETOUR / ARRET TRAITEMENT D�S PREMI�RE ERREUR.                        
          02 CO-MU05-OUT.                                                       
      * - NATURE ERREUR BLOQUANTE, NON BLOQUANTE                                
             03 CO-MU05-CRETOUR.                                                
                04 CO-MU05-CRET           PIC X(04).                            
                04 CO-MU05-LRET           PIC X(58).                            
             03 CO-MU05-NATURE-ERREUR PIC 9(01) VALUE 2.                        
                88 CO-MU05-ERR-BLOQUANTE           VALUE 0.                     
                88 CO-MU05-ERR-NON-BLOQUANTE       VALUE 1.                     
                88 CO-MU05-PAS-D-ERREUR            VALUE 2.                     
                                                                                
