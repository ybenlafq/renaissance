      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CTSE1 CHRYSALIS TRANSCO DES SECT ANA   *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVCTSE1 .                                                            
           05  CTSE1-CTABLEG2    PIC X(15).                                     
           05  CTSE1-CTABLEG2-REDEF REDEFINES CTSE1-CTABLEG2.                   
               10  CTSE1-SECTION         PIC X(07).                             
               10  CTSE1-DEFFET          PIC X(08).                             
           05  CTSE1-WTABLEG     PIC X(80).                                     
           05  CTSE1-WTABLEG-REDEF  REDEFINES CTSE1-WTABLEG.                    
               10  CTSE1-WTRANSCO        PIC X(01).                             
               10  CTSE1-SECTIONT        PIC X(07).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCTSE1-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTSE1-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CTSE1-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTSE1-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CTSE1-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
