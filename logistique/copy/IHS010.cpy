      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHS010 AU 19/02/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,01,BI,A,                          *        
      *                           11,03,BI,A,                          *        
      *                           14,03,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,05,BI,A,                          *        
      *                           32,07,BI,A,                          *        
      *                           39,05,BI,A,                          *        
      *                           44,03,BI,A,                          *        
      *                           47,07,BI,A,                          *        
      *                           54,03,BI,A,                          *        
      *                           57,07,BI,A,                          *        
      *                           64,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHS010.                                                        
            05 NOMETAT-IHS010           PIC X(6) VALUE 'IHS010'.                
            05 RUPTURES-IHS010.                                                 
           10 IHS010-NSOCIETE           PIC X(03).                      007  003
           10 IHS010-CREGROUP           PIC X(01).                      010  001
           10 IHS010-NLIEU              PIC X(03).                      011  003
           10 IHS010-NSSLIEU            PIC X(03).                      014  003
           10 IHS010-CLIEUTRT           PIC X(05).                      017  005
           10 IHS010-CMARQ              PIC X(05).                      022  005
           10 IHS010-CFAM               PIC X(05).                      027  005
           10 IHS010-NCODIC             PIC X(07).                      032  007
           10 IHS010-CTRT               PIC X(05).                      039  005
           10 IHS010-NLIEUENT           PIC X(03).                      044  003
           10 IHS010-NHS                PIC X(07).                      047  007
           10 IHS010-NLIEUHS            PIC X(03).                      054  003
           10 IHS010-NHSENC             PIC X(07).                      057  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHS010-SEQUENCE           PIC S9(04) COMP.                064  002
      *--                                                                       
           10 IHS010-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHS010.                                                   
           10 IHS010-LLIEU              PIC X(20).                      066  020
           10 IHS010-LREF               PIC X(20).                      086  020
           10 IHS010-LREGROUP           PIC X(20).                      106  020
           10 IHS010-LSOCIETE           PIC X(20).                      126  020
           10 IHS010-LSSLIEU            PIC X(20).                      146  020
           10 IHS010-NB-ECART           PIC S9(09)      COMP-3.         166  005
           10 IHS010-QTE1               PIC S9(05)      COMP-3.         171  003
           10 IHS010-QTE2               PIC S9(05)      COMP-3.         174  003
            05 FILLER                      PIC X(336).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHS010-LONG           PIC S9(4)   COMP  VALUE +176.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHS010-LONG           PIC S9(4) COMP-5  VALUE +176.           
                                                                                
      *}                                                                        
