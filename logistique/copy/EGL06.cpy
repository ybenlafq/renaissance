      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGL06   EGL06                                              00000020
      ***************************************************************** 00000030
       01   EGL06I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRECL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCRECF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCRECI    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRECL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLRECF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLRECI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEMAINL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSEMAINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSEMAINF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSEMAINI  PIC X(6).                                       00000330
           02 MTQUOTAD OCCURS   8 TIMES .                               00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTQUOTAL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MTQUOTAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTQUOTAF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MTQUOTAI     PIC X(6).                                  00000380
           02 MTPRISD OCCURS   8 TIMES .                                00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTPRISL      COMP PIC S9(4).                            00000400
      *--                                                                       
             03 MTPRISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTPRISF      PIC X.                                     00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MTPRISI      PIC X(6).                                  00000430
           02 MTDISPD OCCURS   8 TIMES .                                00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTDISPL      COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MTDISPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTDISPF      PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MTDISPI      PIC X(6).                                  00000480
           02 MTNBPD OCCURS   8 TIMES .                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTNBPL  COMP PIC S9(4).                                 00000500
      *--                                                                       
             03 MTNBPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTNBPF  PIC X.                                          00000510
             03 FILLER  PIC X(4).                                       00000520
             03 MTNBPI  PIC X(6).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCQUOTA1L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MCQUOTA1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCQUOTA1F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCQUOTA1I      PIC X(5).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLQUOTA1L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MLQUOTA1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLQUOTA1F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLQUOTA1I      PIC X(20).                                 00000610
           02 MQUOTA1D OCCURS   8 TIMES .                               00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA1L     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MQUOTA1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA1F     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQUOTA1I     PIC X(6).                                  00000660
           02 MPRIS1D OCCURS   8 TIMES .                                00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIS1L      COMP PIC S9(4).                            00000680
      *--                                                                       
             03 MPRIS1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRIS1F      PIC X.                                     00000690
             03 FILLER  PIC X(4).                                       00000700
             03 MPRIS1I      PIC X(6).                                  00000710
           02 MDISP1D OCCURS   8 TIMES .                                00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDISP1L      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MDISP1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDISP1F      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MDISP1I      PIC X(6).                                  00000760
           02 MNBP1D OCCURS   8 TIMES .                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBP1L  COMP PIC S9(4).                                 00000780
      *--                                                                       
             03 MNBP1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNBP1F  PIC X.                                          00000790
             03 FILLER  PIC X(4).                                       00000800
             03 MNBP1I  PIC X(6).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCQUOTA2L      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MCQUOTA2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCQUOTA2F      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCQUOTA2I      PIC X(5).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLQUOTA2L      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MLQUOTA2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLQUOTA2F      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLQUOTA2I      PIC X(20).                                 00000890
           02 MQUOTA2D OCCURS   8 TIMES .                               00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA2L     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQUOTA2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA2F     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQUOTA2I     PIC X(6).                                  00000940
           02 MPRIS2D OCCURS   8 TIMES .                                00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIS2L      COMP PIC S9(4).                            00000960
      *--                                                                       
             03 MPRIS2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRIS2F      PIC X.                                     00000970
             03 FILLER  PIC X(4).                                       00000980
             03 MPRIS2I      PIC X(6).                                  00000990
           02 MDISP2D OCCURS   8 TIMES .                                00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDISP2L      COMP PIC S9(4).                            00001010
      *--                                                                       
             03 MDISP2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDISP2F      PIC X.                                     00001020
             03 FILLER  PIC X(4).                                       00001030
             03 MDISP2I      PIC X(6).                                  00001040
           02 MNBP2D OCCURS   8 TIMES .                                 00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBP2L  COMP PIC S9(4).                                 00001060
      *--                                                                       
             03 MNBP2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNBP2F  PIC X.                                          00001070
             03 FILLER  PIC X(4).                                       00001080
             03 MNBP2I  PIC X(6).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MZONCMDI  PIC X(15).                                      00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLIBERRI  PIC X(58).                                      00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCODTRAI  PIC X(4).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MCICSI    PIC X(5).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MNETNAMI  PIC X(8).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MSCREENI  PIC X(4).                                       00001330
      ***************************************************************** 00001340
      * SDF: EGL06   EGL06                                              00001350
      ***************************************************************** 00001360
       01   EGL06O REDEFINES EGL06I.                                    00001370
           02 FILLER    PIC X(12).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MDATJOUA  PIC X.                                          00001400
           02 MDATJOUC  PIC X.                                          00001410
           02 MDATJOUP  PIC X.                                          00001420
           02 MDATJOUH  PIC X.                                          00001430
           02 MDATJOUV  PIC X.                                          00001440
           02 MDATJOUO  PIC X(10).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MTIMJOUA  PIC X.                                          00001470
           02 MTIMJOUC  PIC X.                                          00001480
           02 MTIMJOUP  PIC X.                                          00001490
           02 MTIMJOUH  PIC X.                                          00001500
           02 MTIMJOUV  PIC X.                                          00001510
           02 MTIMJOUO  PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MPAGEA    PIC X.                                          00001540
           02 MPAGEC    PIC X.                                          00001550
           02 MPAGEP    PIC X.                                          00001560
           02 MPAGEH    PIC X.                                          00001570
           02 MPAGEV    PIC X.                                          00001580
           02 MPAGEO    PIC ZZ.                                         00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MPAGETOTA      PIC X.                                     00001610
           02 MPAGETOTC PIC X.                                          00001620
           02 MPAGETOTP PIC X.                                          00001630
           02 MPAGETOTH PIC X.                                          00001640
           02 MPAGETOTV PIC X.                                          00001650
           02 MPAGETOTO      PIC ZZ.                                    00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MCRECA    PIC X.                                          00001680
           02 MCRECC    PIC X.                                          00001690
           02 MCRECP    PIC X.                                          00001700
           02 MCRECH    PIC X.                                          00001710
           02 MCRECV    PIC X.                                          00001720
           02 MCRECO    PIC X(5).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MLRECA    PIC X.                                          00001750
           02 MLRECC    PIC X.                                          00001760
           02 MLRECP    PIC X.                                          00001770
           02 MLRECH    PIC X.                                          00001780
           02 MLRECV    PIC X.                                          00001790
           02 MLRECO    PIC X(20).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MSEMAINA  PIC X.                                          00001820
           02 MSEMAINC  PIC X.                                          00001830
           02 MSEMAINP  PIC X.                                          00001840
           02 MSEMAINH  PIC X.                                          00001850
           02 MSEMAINV  PIC X.                                          00001860
           02 MSEMAINO  PIC X(6).                                       00001870
           02 DFHMS1 OCCURS   8 TIMES .                                 00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MTQUOTAA     PIC X.                                     00001900
             03 MTQUOTAC     PIC X.                                     00001910
             03 MTQUOTAP     PIC X.                                     00001920
             03 MTQUOTAH     PIC X.                                     00001930
             03 MTQUOTAV     PIC X.                                     00001940
             03 MTQUOTAO     PIC ZZZZZZ.                                00001950
           02 DFHMS2 OCCURS   8 TIMES .                                 00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MTPRISA      PIC X.                                     00001980
             03 MTPRISC PIC X.                                          00001990
             03 MTPRISP PIC X.                                          00002000
             03 MTPRISH PIC X.                                          00002010
             03 MTPRISV PIC X.                                          00002020
             03 MTPRISO      PIC ZZZZZZ.                                00002030
           02 DFHMS3 OCCURS   8 TIMES .                                 00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MTDISPA      PIC X.                                     00002060
             03 MTDISPC PIC X.                                          00002070
             03 MTDISPP PIC X.                                          00002080
             03 MTDISPH PIC X.                                          00002090
             03 MTDISPV PIC X.                                          00002100
             03 MTDISPO      PIC ZZZZZZ.                                00002110
           02 DFHMS4 OCCURS   8 TIMES .                                 00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MTNBPA  PIC X.                                          00002140
             03 MTNBPC  PIC X.                                          00002150
             03 MTNBPP  PIC X.                                          00002160
             03 MTNBPH  PIC X.                                          00002170
             03 MTNBPV  PIC X.                                          00002180
             03 MTNBPO  PIC ZZZZZZ.                                     00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCQUOTA1A      PIC X.                                     00002210
           02 MCQUOTA1C PIC X.                                          00002220
           02 MCQUOTA1P PIC X.                                          00002230
           02 MCQUOTA1H PIC X.                                          00002240
           02 MCQUOTA1V PIC X.                                          00002250
           02 MCQUOTA1O      PIC X(5).                                  00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MLQUOTA1A      PIC X.                                     00002280
           02 MLQUOTA1C PIC X.                                          00002290
           02 MLQUOTA1P PIC X.                                          00002300
           02 MLQUOTA1H PIC X.                                          00002310
           02 MLQUOTA1V PIC X.                                          00002320
           02 MLQUOTA1O      PIC X(20).                                 00002330
           02 DFHMS5 OCCURS   8 TIMES .                                 00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MQUOTA1A     PIC X.                                     00002360
             03 MQUOTA1C     PIC X.                                     00002370
             03 MQUOTA1P     PIC X.                                     00002380
             03 MQUOTA1H     PIC X.                                     00002390
             03 MQUOTA1V     PIC X.                                     00002400
             03 MQUOTA1O     PIC ZZZZZZ.                                00002410
           02 DFHMS6 OCCURS   8 TIMES .                                 00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MPRIS1A      PIC X.                                     00002440
             03 MPRIS1C PIC X.                                          00002450
             03 MPRIS1P PIC X.                                          00002460
             03 MPRIS1H PIC X.                                          00002470
             03 MPRIS1V PIC X.                                          00002480
             03 MPRIS1O      PIC ZZZZZZ.                                00002490
           02 DFHMS7 OCCURS   8 TIMES .                                 00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MDISP1A      PIC X.                                     00002520
             03 MDISP1C PIC X.                                          00002530
             03 MDISP1P PIC X.                                          00002540
             03 MDISP1H PIC X.                                          00002550
             03 MDISP1V PIC X.                                          00002560
             03 MDISP1O      PIC ZZZZZZ.                                00002570
           02 DFHMS8 OCCURS   8 TIMES .                                 00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MNBP1A  PIC X.                                          00002600
             03 MNBP1C  PIC X.                                          00002610
             03 MNBP1P  PIC X.                                          00002620
             03 MNBP1H  PIC X.                                          00002630
             03 MNBP1V  PIC X.                                          00002640
             03 MNBP1O  PIC ZZZZZZ.                                     00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MCQUOTA2A      PIC X.                                     00002670
           02 MCQUOTA2C PIC X.                                          00002680
           02 MCQUOTA2P PIC X.                                          00002690
           02 MCQUOTA2H PIC X.                                          00002700
           02 MCQUOTA2V PIC X.                                          00002710
           02 MCQUOTA2O      PIC X(5).                                  00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MLQUOTA2A      PIC X.                                     00002740
           02 MLQUOTA2C PIC X.                                          00002750
           02 MLQUOTA2P PIC X.                                          00002760
           02 MLQUOTA2H PIC X.                                          00002770
           02 MLQUOTA2V PIC X.                                          00002780
           02 MLQUOTA2O      PIC X(20).                                 00002790
           02 DFHMS9 OCCURS   8 TIMES .                                 00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MQUOTA2A     PIC X.                                     00002820
             03 MQUOTA2C     PIC X.                                     00002830
             03 MQUOTA2P     PIC X.                                     00002840
             03 MQUOTA2H     PIC X.                                     00002850
             03 MQUOTA2V     PIC X.                                     00002860
             03 MQUOTA2O     PIC ZZZZZZ.                                00002870
           02 DFHMS10 OCCURS   8 TIMES .                                00002880
             03 FILLER       PIC X(2).                                  00002890
             03 MPRIS2A      PIC X.                                     00002900
             03 MPRIS2C PIC X.                                          00002910
             03 MPRIS2P PIC X.                                          00002920
             03 MPRIS2H PIC X.                                          00002930
             03 MPRIS2V PIC X.                                          00002940
             03 MPRIS2O      PIC ZZZZZZ.                                00002950
           02 DFHMS11 OCCURS   8 TIMES .                                00002960
             03 FILLER       PIC X(2).                                  00002970
             03 MDISP2A      PIC X.                                     00002980
             03 MDISP2C PIC X.                                          00002990
             03 MDISP2P PIC X.                                          00003000
             03 MDISP2H PIC X.                                          00003010
             03 MDISP2V PIC X.                                          00003020
             03 MDISP2O      PIC ZZZZZZ.                                00003030
           02 DFHMS12 OCCURS   8 TIMES .                                00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MNBP2A  PIC X.                                          00003060
             03 MNBP2C  PIC X.                                          00003070
             03 MNBP2P  PIC X.                                          00003080
             03 MNBP2H  PIC X.                                          00003090
             03 MNBP2V  PIC X.                                          00003100
             03 MNBP2O  PIC ZZZZZZ.                                     00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MZONCMDA  PIC X.                                          00003130
           02 MZONCMDC  PIC X.                                          00003140
           02 MZONCMDP  PIC X.                                          00003150
           02 MZONCMDH  PIC X.                                          00003160
           02 MZONCMDV  PIC X.                                          00003170
           02 MZONCMDO  PIC X(15).                                      00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MLIBERRA  PIC X.                                          00003200
           02 MLIBERRC  PIC X.                                          00003210
           02 MLIBERRP  PIC X.                                          00003220
           02 MLIBERRH  PIC X.                                          00003230
           02 MLIBERRV  PIC X.                                          00003240
           02 MLIBERRO  PIC X(58).                                      00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MCODTRAA  PIC X.                                          00003270
           02 MCODTRAC  PIC X.                                          00003280
           02 MCODTRAP  PIC X.                                          00003290
           02 MCODTRAH  PIC X.                                          00003300
           02 MCODTRAV  PIC X.                                          00003310
           02 MCODTRAO  PIC X(4).                                       00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MCICSA    PIC X.                                          00003340
           02 MCICSC    PIC X.                                          00003350
           02 MCICSP    PIC X.                                          00003360
           02 MCICSH    PIC X.                                          00003370
           02 MCICSV    PIC X.                                          00003380
           02 MCICSO    PIC X(5).                                       00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MNETNAMA  PIC X.                                          00003410
           02 MNETNAMC  PIC X.                                          00003420
           02 MNETNAMP  PIC X.                                          00003430
           02 MNETNAMH  PIC X.                                          00003440
           02 MNETNAMV  PIC X.                                          00003450
           02 MNETNAMO  PIC X(8).                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MSCREENA  PIC X.                                          00003480
           02 MSCREENC  PIC X.                                          00003490
           02 MSCREENP  PIC X.                                          00003500
           02 MSCREENH  PIC X.                                          00003510
           02 MSCREENV  PIC X.                                          00003520
           02 MSCREENO  PIC X(4).                                       00003530
                                                                                
