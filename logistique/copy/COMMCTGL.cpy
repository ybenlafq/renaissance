      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA PROGRAMMES MBCTGL ET MTCTGL                            00020001
      *     MBCTGL : MODULE DE CONTROLE / GL EN BATCH                   00030006
      *     MTCTGL : MODULE DE CONTROLE / GL EN TP                      00040006
      **************************************************************    00050000
       01 COMMCTGL.                                                     00060001
      *------------------------------ NUMERO ENTITE                     00070001
          02 COMM-CTGL-NENTITE        PIC  X(5).                        00080001
      *------------------------------ NUMERO JOURNAL                    00090001
          02 COMM-CTGL-NJRN           PIC  X(10).                       00100001
      *------------------------------ LIBELLE JOURNAL RETOURNE          00100002
          02 COMM-CTGL-LJRN           PIC  X(25).                       00100003
      *------------------------------ EXERCICE FISCAL                   00110001
          02 COMM-CTGL-NEXERCICE      PIC  X(4).                        00120001
          02 COMM-CTGL-NEXERCI-N     REDEFINES  COMM-CTGL-NEXERCICE     00121009
                          PIC    9(4).                                  00122009
      *------------------------------ PERIODE COMPTABLE                 00130001
          02 COMM-CTGL-NPERIODE       PIC  X(2).                        00140001
          02 COMM-CTGL-NPERIOD-N     REDEFINES COMM-CTGL-NPERIODE       00140002
                          PIC    9(2).                                  00140003
      *------------------------------ DATE COMPTABLE SSAAMMJJ           00150003
          02 COMM-CTGL-DCOMPT.                                          00160008
             03 COMM-CTGL-DCOMPT-SS      PIC  X(2).                     00161008
             03 COMM-CTGL-DCOMPT-AAS     PIC  X(6).                     00162008
      *------------------------------ CLEF COMPTABLE DE GL              00170001
          02 COMM-CTGL-CLEFGL.                                          00180002
             03 COMM-CTGL-NSOCGL      PIC  X(5).                        00190001
             03 COMM-CTGL-NCOMPTE     PIC  X(6).                        00200001
             03 COMM-CTGL-NSSCOMPTE   PIC  X(6).                        00210001
             03 COMM-CTGL-NSECTION    PIC  X(6).                        00220001
             03 COMM-CTGL-NRUBR       PIC  X(6).                        00230001
             03 COMM-CTGL-NSTEAPP     PIC  X(5).                        00250001
             03 COMM-CTGL-NETABCLEF   PIC  X(3).                        00260001
      *------------------------------ LIBELLE COMPTE  RETOURNE          00100002
          02 COMM-CTGL-LCOMPTE        PIC  X(25).                       00100003
      *------------------------------ ZONE FILLER                       00100002
          02 COMM-CTGL-FILLER         PIC  X(30).                       00100003
      *------------------------------ CODE RETOUR                       00270001
          02 COMM-CTGL-CODRET         PIC  X(1).                        00280001
             88 COMM-CTGL-OK          VALUE '0'.                        00290001
             88 COMM-CTGL-NONOK       VALUE '1'.                        00300001
      *------------------------------ CODES ANOMALIES                   00310001
          02 COMM-CTGL-ANOS.                                            00320007
             03 COMM-CTGL-TABANO            OCCURS 20.                  00321007
                04 COMM-CTGL-TYPE        PIC  X(1).                     00330007
                04 COMM-CTGL-NCODE       PIC  X(4).                     00340007
                                                                                
