      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS14   EGS14                                              00000020
      ***************************************************************** 00000030
       01   EGS14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDEPOTI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCASSORF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCASSORI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSORL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLASSORF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLASSORI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCAPPROI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAPPROL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAPPROF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLAPPROI  PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODIC1L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNCODIC1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODIC1F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNCODIC1I      PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCEXPOI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEXPOL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLEXPOF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLEXPOI   PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCMAGL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNSOCMAGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCMAGF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNSOCMAGI      PIC X(3).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAGL     COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMAGF     PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MMAGI     PIC X(3).                                       00000650
           02 M7I OCCURS   11 TIMES .                                   00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNCODICI     PIC X(7).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCMARQI      PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOURNL  COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MLREFFOURNL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLREFFOURNF  PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MLREFFOURNI  PIC X(20).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTOTALL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQTOTALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTOTALF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQTOTALI     PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQDISPTOTL   COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQDISPTOTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQDISPTOTF   PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQDISPTOTI   PIC X(4).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPOL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQEXPOF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQEXPOI      PIC X(4).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRESL  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MQRESL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQRESF  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQRESI  PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTRANSL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MQTRANSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTRANSF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQTRANSI     PIC X(3).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQHSL   COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MQHSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQHSF   PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQHSI   PIC X(3).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPRETL      COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MQPRETL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPRETF      PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQPRETI      PIC X(3).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQREGULL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MQREGULL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQREGULF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MQREGULI     PIC X(3).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MZONCMDI  PIC X(12).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MLIBERRI  PIC X(61).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCICSI    PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MNETNAMI  PIC X(8).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MSCREENI  PIC X(4).                                       00001340
      ***************************************************************** 00001350
      * SDF: EGS14   EGS14                                              00001360
      ***************************************************************** 00001370
       01   EGS14O REDEFINES EGS14I.                                    00001380
           02 FILLER    PIC X(12).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDATJOUA  PIC X.                                          00001410
           02 MDATJOUC  PIC X.                                          00001420
           02 MDATJOUP  PIC X.                                          00001430
           02 MDATJOUH  PIC X.                                          00001440
           02 MDATJOUV  PIC X.                                          00001450
           02 MDATJOUO  PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTIMJOUA  PIC X.                                          00001480
           02 MTIMJOUC  PIC X.                                          00001490
           02 MTIMJOUP  PIC X.                                          00001500
           02 MTIMJOUH  PIC X.                                          00001510
           02 MTIMJOUV  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNSOCA    PIC X.                                          00001550
           02 MNSOCC    PIC X.                                          00001560
           02 MNSOCP    PIC X.                                          00001570
           02 MNSOCH    PIC X.                                          00001580
           02 MNSOCV    PIC X.                                          00001590
           02 MNSOCO    PIC X(3).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MDEPOTA   PIC X.                                          00001620
           02 MDEPOTC   PIC X.                                          00001630
           02 MDEPOTP   PIC X.                                          00001640
           02 MDEPOTH   PIC X.                                          00001650
           02 MDEPOTV   PIC X.                                          00001660
           02 MDEPOTO   PIC X(3).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MCASSORA  PIC X.                                          00001690
           02 MCASSORC  PIC X.                                          00001700
           02 MCASSORP  PIC X.                                          00001710
           02 MCASSORH  PIC X.                                          00001720
           02 MCASSORV  PIC X.                                          00001730
           02 MCASSORO  PIC X(5).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLASSORA  PIC X.                                          00001760
           02 MLASSORC  PIC X.                                          00001770
           02 MLASSORP  PIC X.                                          00001780
           02 MLASSORH  PIC X.                                          00001790
           02 MLASSORV  PIC X.                                          00001800
           02 MLASSORO  PIC X(20).                                      00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MCFAMA    PIC X.                                          00001830
           02 MCFAMC    PIC X.                                          00001840
           02 MCFAMP    PIC X.                                          00001850
           02 MCFAMH    PIC X.                                          00001860
           02 MCFAMV    PIC X.                                          00001870
           02 MCFAMO    PIC X(5).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MLFAMA    PIC X.                                          00001900
           02 MLFAMC    PIC X.                                          00001910
           02 MLFAMP    PIC X.                                          00001920
           02 MLFAMH    PIC X.                                          00001930
           02 MLFAMV    PIC X.                                          00001940
           02 MLFAMO    PIC X(20).                                      00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MCAPPROA  PIC X.                                          00001970
           02 MCAPPROC  PIC X.                                          00001980
           02 MCAPPROP  PIC X.                                          00001990
           02 MCAPPROH  PIC X.                                          00002000
           02 MCAPPROV  PIC X.                                          00002010
           02 MCAPPROO  PIC X(5).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MLAPPROA  PIC X.                                          00002040
           02 MLAPPROC  PIC X.                                          00002050
           02 MLAPPROP  PIC X.                                          00002060
           02 MLAPPROH  PIC X.                                          00002070
           02 MLAPPROV  PIC X.                                          00002080
           02 MLAPPROO  PIC X(20).                                      00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MNCODIC1A      PIC X.                                     00002110
           02 MNCODIC1C PIC X.                                          00002120
           02 MNCODIC1P PIC X.                                          00002130
           02 MNCODIC1H PIC X.                                          00002140
           02 MNCODIC1V PIC X.                                          00002150
           02 MNCODIC1O      PIC X(7).                                  00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MCEXPOA   PIC X.                                          00002180
           02 MCEXPOC   PIC X.                                          00002190
           02 MCEXPOP   PIC X.                                          00002200
           02 MCEXPOH   PIC X.                                          00002210
           02 MCEXPOV   PIC X.                                          00002220
           02 MCEXPOO   PIC X(5).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MLEXPOA   PIC X.                                          00002250
           02 MLEXPOC   PIC X.                                          00002260
           02 MLEXPOP   PIC X.                                          00002270
           02 MLEXPOH   PIC X.                                          00002280
           02 MLEXPOV   PIC X.                                          00002290
           02 MLEXPOO   PIC X(20).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MNSOCMAGA      PIC X.                                     00002320
           02 MNSOCMAGC PIC X.                                          00002330
           02 MNSOCMAGP PIC X.                                          00002340
           02 MNSOCMAGH PIC X.                                          00002350
           02 MNSOCMAGV PIC X.                                          00002360
           02 MNSOCMAGO      PIC X(3).                                  00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MMAGA     PIC X.                                          00002390
           02 MMAGC     PIC X.                                          00002400
           02 MMAGP     PIC X.                                          00002410
           02 MMAGH     PIC X.                                          00002420
           02 MMAGV     PIC X.                                          00002430
           02 MMAGO     PIC X(3).                                       00002440
           02 M7O OCCURS   11 TIMES .                                   00002450
             03 FILLER       PIC X(2).                                  00002460
             03 MNCODICA     PIC X.                                     00002470
             03 MNCODICC     PIC X.                                     00002480
             03 MNCODICP     PIC X.                                     00002490
             03 MNCODICH     PIC X.                                     00002500
             03 MNCODICV     PIC X.                                     00002510
             03 MNCODICO     PIC X(7).                                  00002520
             03 FILLER       PIC X(2).                                  00002530
             03 MCMARQA      PIC X.                                     00002540
             03 MCMARQC PIC X.                                          00002550
             03 MCMARQP PIC X.                                          00002560
             03 MCMARQH PIC X.                                          00002570
             03 MCMARQV PIC X.                                          00002580
             03 MCMARQO      PIC X(5).                                  00002590
             03 FILLER       PIC X(2).                                  00002600
             03 MLREFFOURNA  PIC X.                                     00002610
             03 MLREFFOURNC  PIC X.                                     00002620
             03 MLREFFOURNP  PIC X.                                     00002630
             03 MLREFFOURNH  PIC X.                                     00002640
             03 MLREFFOURNV  PIC X.                                     00002650
             03 MLREFFOURNO  PIC X(20).                                 00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MQTOTALA     PIC X.                                     00002680
             03 MQTOTALC     PIC X.                                     00002690
             03 MQTOTALP     PIC X.                                     00002700
             03 MQTOTALH     PIC X.                                     00002710
             03 MQTOTALV     PIC X.                                     00002720
             03 MQTOTALO     PIC X(5).                                  00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MQDISPTOTA   PIC X.                                     00002750
             03 MQDISPTOTC   PIC X.                                     00002760
             03 MQDISPTOTP   PIC X.                                     00002770
             03 MQDISPTOTH   PIC X.                                     00002780
             03 MQDISPTOTV   PIC X.                                     00002790
             03 MQDISPTOTO   PIC X(4).                                  00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MQEXPOA      PIC X.                                     00002820
             03 MQEXPOC PIC X.                                          00002830
             03 MQEXPOP PIC X.                                          00002840
             03 MQEXPOH PIC X.                                          00002850
             03 MQEXPOV PIC X.                                          00002860
             03 MQEXPOO      PIC X(4).                                  00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MQRESA  PIC X.                                          00002890
             03 MQRESC  PIC X.                                          00002900
             03 MQRESP  PIC X.                                          00002910
             03 MQRESH  PIC X.                                          00002920
             03 MQRESV  PIC X.                                          00002930
             03 MQRESO  PIC X(5).                                       00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MQTRANSA     PIC X.                                     00002960
             03 MQTRANSC     PIC X.                                     00002970
             03 MQTRANSP     PIC X.                                     00002980
             03 MQTRANSH     PIC X.                                     00002990
             03 MQTRANSV     PIC X.                                     00003000
             03 MQTRANSO     PIC X(3).                                  00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MQHSA   PIC X.                                          00003030
             03 MQHSC   PIC X.                                          00003040
             03 MQHSP   PIC X.                                          00003050
             03 MQHSH   PIC X.                                          00003060
             03 MQHSV   PIC X.                                          00003070
             03 MQHSO   PIC X(3).                                       00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MQPRETA      PIC X.                                     00003100
             03 MQPRETC PIC X.                                          00003110
             03 MQPRETP PIC X.                                          00003120
             03 MQPRETH PIC X.                                          00003130
             03 MQPRETV PIC X.                                          00003140
             03 MQPRETO      PIC X(3).                                  00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MQREGULA     PIC X.                                     00003170
             03 MQREGULC     PIC X.                                     00003180
             03 MQREGULP     PIC X.                                     00003190
             03 MQREGULH     PIC X.                                     00003200
             03 MQREGULV     PIC X.                                     00003210
             03 MQREGULO     PIC X(3).                                  00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MZONCMDA  PIC X.                                          00003240
           02 MZONCMDC  PIC X.                                          00003250
           02 MZONCMDP  PIC X.                                          00003260
           02 MZONCMDH  PIC X.                                          00003270
           02 MZONCMDV  PIC X.                                          00003280
           02 MZONCMDO  PIC X(12).                                      00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLIBERRA  PIC X.                                          00003310
           02 MLIBERRC  PIC X.                                          00003320
           02 MLIBERRP  PIC X.                                          00003330
           02 MLIBERRH  PIC X.                                          00003340
           02 MLIBERRV  PIC X.                                          00003350
           02 MLIBERRO  PIC X(61).                                      00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCODTRAA  PIC X.                                          00003380
           02 MCODTRAC  PIC X.                                          00003390
           02 MCODTRAP  PIC X.                                          00003400
           02 MCODTRAH  PIC X.                                          00003410
           02 MCODTRAV  PIC X.                                          00003420
           02 MCODTRAO  PIC X(4).                                       00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCICSA    PIC X.                                          00003450
           02 MCICSC    PIC X.                                          00003460
           02 MCICSP    PIC X.                                          00003470
           02 MCICSH    PIC X.                                          00003480
           02 MCICSV    PIC X.                                          00003490
           02 MCICSO    PIC X(5).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MNETNAMA  PIC X.                                          00003520
           02 MNETNAMC  PIC X.                                          00003530
           02 MNETNAMP  PIC X.                                          00003540
           02 MNETNAMH  PIC X.                                          00003550
           02 MNETNAMV  PIC X.                                          00003560
           02 MNETNAMO  PIC X(8).                                       00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MSCREENA  PIC X.                                          00003590
           02 MSCREENC  PIC X.                                          00003600
           02 MSCREENP  PIC X.                                          00003610
           02 MSCREENH  PIC X.                                          00003620
           02 MSCREENV  PIC X.                                          00003630
           02 MSCREENO  PIC X(4).                                       00003640
                                                                                
