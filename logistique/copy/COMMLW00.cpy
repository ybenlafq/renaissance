      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * COM-LW00-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-LW00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-LW00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA                                                  
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS                                   
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION                     
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC XX.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS PIC 99.                                        
              05 COMM-DATE-SEMAA PIC 99.                                        
              05 COMM-DATE-SEMNU PIC 99.                                        
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES APPLICATIVES                                            
      *                                                                         
           02  COMM-LW00-MENU.                                                  
      *=> ZONES DE SAUVEGARDE COMMUNES AU MENU PRINCIPAL                        
      *=> ET AUX TRANSACTIONS DE NIVEAU INFERIEUR                               
              03  COMM-LW00.                                                    
                  05  COMM-LW00-MZONCMD          PIC X(15).                     
                  05  COMM-LW00-NSOCDEP          PIC X(03).                     
                  05  COMM-LW00-NDEPOT           PIC X(03).                     
                  05  COMM-LW00-NCDE             PIC X(07).                     
                  05  COMM-LW00-NSITE            PIC 9(03).                     
                  05  COMM-LW00-LETSITE          PIC X(01).                     
                  05  COMM-LW00-DEPOT-AUTOR.                                    
                      10 COMM-LW00-SOCDEP        PIC X(03) OCCURS 10.           
                      10 COMM-LW00-DEPOT         PIC X(03) OCCURS 10.           
                      10 COMM-LW00-SITE          PIC 9(03) OCCURS 10.           
                      10 COMM-LW00-LTSITE        PIC X(01) OCCURS 10.           
                  05  COMM-LW00-NBDEPOT          PIC S9(3) COMP-3.              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           05  COMM-SWAP-CURS             PIC S9(4) COMP.                
      *--                                                                       
                  05  COMM-SWAP-CURS             PIC S9(4) COMP-5.              
      *}                                                                        
                  05  COMM-LW00-DLIVRAISON       PIC X(08).                     
                  05  COMM-SWAP-ATTR             OCCURS 150 PIC X(1).   00710000
                  05  COMM-LW00-MAJ-AUTOR        PIC X(01).                     
                  05  COMM-LW00-LLIEU            PIC X(20).                     
AA0712            05  COMM-LW00-NCODIC           PIC X(007).                    
AA0712            05  COMM-LW00-NPROG            PIC X(008).                    
AA0712            05  COMM-LW00-FILLER           PIC X(046).                    
AA0712*           05  COMM-LW00-FILLER           PIC X(061).                    
      *=> DE NIVEAU INFERIEUR                                                   
             02  COMM-LW00-APPLI                 PIC X(3500).                   
             02  COMM-LW02-APPLI REDEFINES COMM-LW00-APPLI.                     
                 03 COMM-LW02-MODIF-DONNEE       PIC X.                         
                 03 COMM-LW02-PAGE               PIC 9(3).                      
                 03 COMM-LW02-NBMODIF-TS         PIC 9(3).                      
                 03 COMM-LW02-ITEMAX             PIC 9(3).                      
                 03 COMM-LW02-NB-ITEM            PIC 9(3).                      
                 03 COMM-LW02-PAGE-MAX           PIC 9(3).                      
                 03 COMM-LW02-NMUT               PIC X(7).                      
                 03 COMM-LW02-NSOC               PIC X(3).                      
                 03 COMM-LW02-NLIEU              PIC X(3).                      
                 03 COMM-LW02-CSELART            PIC X(5).                      
                 03 COMM-LW02-QNBPIECES          PIC X(5).                      
                 03 COMM-LW02-QNBVOLUME          PIC X(6).                      
                 03 COMM-LW02-QUOTAPIECES        PIC X(5).                      
                 03 COMM-LW02-QUOTAVOLUME        PIC X(6).                      
                 03 COMM-LW02-DDESTOCK           PIC X(10).                     
                 03 COMM-LW02-PF5                PIC X.                         
                    88 COMM-LW02-PF5-1X    VALUE '1'.                           
                    88 COMM-LW02-PF5-2X    VALUE '2'.                           
                 03 COMM-LW02-HCHARGT            PIC X(5).                      
AA0213           03 COMM-LW02-SIMU-REEL          PIC X(4).                      
      *                                                                         
             02  COMM-LW20-APPLI REDEFINES COMM-LW00-APPLI.                     
                 03 COMM-LW20-PAGE               PIC 9(3).                      
                 03 COMM-LW20-ITEM               PIC 9(3).                      
                 03 COMM-LW20-PAGE-MAX           PIC 9(3).                      
                 03 COMM-LW20-DDESTOCK           PIC X(8).                      
                 03 COMM-LW20-DDESTOCK-GFJMA-4   PIC X(8).                      
                 03 COMM-LW20-CODEIMPRI          PIC X(4).                      
                 03 COMM-LW20-MESS               PIC X(80).                     
                 03 COMM-LW20-CODRET             PIC S9(4).                     
                 03 COMM-LW20-NMUTATION          PIC X(07).                     
                 03 COMM-LW20-NSOCLIEU           PIC X(03).                     
                 03 COMM-LW20-NLIEU              PIC X(03).                     
                 03 COMM-LW20-CSELART            PIC X(05).                     
                 03 COMM-LW20-DMUTATION          PIC X(08).                     
                 03 COMM-LW20-OPTIONS            PIC X(01).                     
      *                                                                         
      *----  COMMAREA UTILISEE PAR TLW11                                        
      *----  TLW00 - OPTION 1 (TLW10) - OPTION 1 ( TLW11 )                      
             02  COMM-LW11-APPLI REDEFINES COMM-LW00-APPLI.                     
                 03 COMM-LW11-PAGE               PIC 9(04).                     
                 03 COMM-LW11-NBPAGES            PIC 9(04).                     
                 03 COMM-LW11-TSLONG          PIC 9(01).                        
                    88 TS11-PAS-TROP-LONGUE       VALUE 0.                      
                    88 TS11-TROP-LONGUE           VALUE 1.                      
                 03 COMM-LW11-ECART-JOURS     PIC 9(03).                        
                 03 COMM-LW11-SDDEBUT         PIC X(08).                        
                 03 COMM-LW11-SDFIN           PIC X(08).                        
                 03 COMM-LW11-SNSOC           PIC X(03).                        
                 03 COMM-LW11-SNLIEU          PIC X(03).                        
                 03 COMM-LW11-SSELART         PIC X(05).                        
                 03 COMM-LW11-SNMUTATION      PIC X(07).                        
                 03 COMM-LW11-SWMULTI         PIC X(01).                        
                 03 COMM-LW11-SDDSTOCK        PIC X(04).                        
                 03 COMM-LW11-SDCHARG         PIC X(04).                        
                 03 COMM-LW11-SHCHARG         PIC X(05).                        
                 03 COMM-LW11-SDMUTATION      PIC X(04).                        
                 03 COMM-LW11-SDLM7           PIC X(04).                        
                 03 COMM-LW11-SDEXPED         PIC X(04).                        
                 03 COMM-LW11-SDVALID         PIC X(04).                        
      *                                                                         
      *----  COMMAREA UTILISEE PAR TLW12                                        
      *----  TLW00 - OPTION 1 (TLW10) - OPTION 2 ( TLW12 )                      
             02  COMM-LW12-APPLI REDEFINES COMM-LW00-APPLI.                     
                 03 COMM-LW12-PAGE               PIC 9(04).                     
                 03 COMM-LW12-NBPAGES            PIC 9(04).                     
                 03 COMM-LW12-TSLONG          PIC 9(01).                        
                    88 TS12-PAS-TROP-LONGUE   VALUE 0.                          
                    88 TS12-TROP-LONGUE       VALUE 1.                          
                 03 COMM-LW12-SDLIVR          PIC X(04).                        
                 03 COMM-LW12-SNCODIC         PIC X(07).                        
                 03 COMM-LW12-SNCDE           PIC X(07).                        
                 03 COMM-LW12-SETAT           PIC X(01).                        
                 03 COMM-LW12-SDTSFT          PIC X(04).                        
                 03 COMM-LW12-SNRECQUAI       PIC X(07).                        
                 03 COMM-LW12-SDRCPT          PIC X(04).                        
                 03 COMM-LW12-SNENTCDE        PIC X(07).                        
      *                                                                         
      *----  COMMAREA UTILISEE PAR TLW13                                        
      *----  TLW00 - OPTION 1 (TLW10) - OPTION 3 ( TLW13 )                      
             02  COMM-LW13-APPLI REDEFINES COMM-LW00-APPLI.                     
                 03 COMM-LW13-PAGE               PIC 9(04).                     
                 03 COMM-LW13-NBPAGES            PIC 9(04).                     
                 03 COMM-LW13-TSLONG          PIC 9(01).                        
                    88 TS13-PAS-TROP-LONGUE   VALUE 0.                          
                    88 TS13-TROP-LONGUE       VALUE 1.                          
                 03 COMM-LW13-DD-SELECTION    PIC X(08).                        
                 03 COMM-LW13-SDRECEPT        PIC X(04).                        
                 03 COMM-LW13-SNDOSLM7        PIC X(08).                        
AA1215           03 COMM-LW13-SNRECQORIG      PIC X(07).                        
                 03 COMM-LW13-SNRECQUAI       PIC X(07).                        
                 03 COMM-LW13-SNREC           PIC X(07).                        
                 03 COMM-LW13-SNCDE           PIC X(07).                        
                 03 COMM-LW13-SNCODIC         PIC X(07).                        
                 03 COMM-LW13-SDLIVRAISON     PIC X(04).                        
                 03 COMM-LW13-SNBL            PIC X(10).                        
                 03 COMM-LW13-SETAT           PIC X(02).                        
                 03 COMM-LW13-SNENTCDE        PIC X(05).                        
                 03 COMM-LW13-SLENTCDE        PIC X(20).                        
      *                                                                         
      *----  COMMAREA UTILISEE PAR TLW14                                        
      *----  TLW00 - OPTION 1 (TLW10) - OPTION 4 ( TLW14 )                      
             02  COMM-LW14-APPLI REDEFINES COMM-LW00-APPLI.                     
                 03 COMM-LW14-PAGE               PIC 9(04).                     
                 03 COMM-LW14-NBPAGES            PIC 9(04).                     
                 03 COMM-LW14-DREC               PIC X(08).                     
                 03 COMM-LW14-WSEL               PIC X(01).                     
                 03 COMM-LW14-WTRI               PIC X(01).                     
                 03 COMM-LW14-QTOTVOL            PIC X(07).                     
                 03 COMM-LW14-QTOTCDE            PIC X(05).                     
                                                                                
