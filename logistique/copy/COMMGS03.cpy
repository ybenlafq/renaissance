      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
000397* ZONES RESERVEES APPLICATIVES ---------------------------- 3568  00730000
000720     03 COMM-GS03-APPLI REDEFINES COMM-GS00-FILLER.               00970010
000694*                                                                 00720000
000695*                                              QUANTITE DISPO           00
000696        05 COMM-GS03-QDISP          PIC S9(5) COMP-3.             00970003
000697*                                                                 00720000
000698*                                              QUANTITE DISPO +         00
000699*                                              QUANTITE COTE            00
000700        05 COMM-GS03-TOT-DISP-COT   PIC S9(5) COMP-3.             00970003
000701*                                                                 00720000
000702*                                              QUANTITE MIS DE COTE     00
000703        05 COMM-GS03-QCOTE          PIC S9(5) COMP-3.             00970003
000704*                                                                 00720000
000705*                                              QUANTITE MIS DE RESERVE  00
000706        05 COMM-GS03-QRES           PIC S9(5) COMP-3.             00970003
000707*                                                                 00720000
000708*                                              QUANTITE DISPO SAISIE    00
000709        05 COMM-GS03-QDISP-SAIS     PIC S9(5) COMP-3.             00970003
000710*                                                                 00720000
000711*                                        QUANTITE MIS DE COTE SAISIE    00
000712        05 COMM-GS03-QCOTE-SAIS     PIC S9(5) COMP-3.             00970003
000710*                                                                 00720000
000711*                                        INDICATEUR DE MIS DE COTEIE    00
000712        05 COMM-GS03-INDC           PIC  9.                       00970003
000710*                                                                 00720000
000711*                                        INDICATEUR DE DISPONIBLE IE    00
000712        05 COMM-GS03-INDV           PIC  9.                       00970003
000710*                                                                 00720000
000711*                                        INDICATEUR DE DISPONIBLE IE    00
000712        05 COMM-GS03-UTIL           PIC  X(05).                   00970003
000713*                                                                 00720000
000711*                           ANOMALIE DANS TRT AUTOMATIQUE         IE    00
000712        05 COMM-GS03-ANOM           PIC  X.                       00970003
FF0604*                                        INDICATEUR DE COMMENTAIREIE    00
FF0604        05 COMM-GS03-IND16          PIC  9.                       00970003
FF0604*            INDIQUE LE TRAITEMENT PRECEDENT                      IE    00
FF0604*            MS = MVT STOCK                                       IE    00
FF0604*            CS = CONTROLE SYNTAXIQUE                             IE    00
FF0604*            MC = MAJ COMMENTAIRES                                IE    00
FF0604        05 COMM-GS03-TRT-PRECED     PIC  X(2).                    00970003
FF0604*                                        COMMENTAIRES             IE    00
FF0604        05 COMM-GS03-COMMENT1       PIC  X(20).                   00970003
FF0604        05 COMM-GS03-COMMENT2       PIC  X(20).                   00970003
FF0604        05 COMM-GS03-COMMENT3       PIC  X(20).                   00970003
FF0604        05 COMM-GS03-COMMENT4       PIC  X(20).                   00970003
FF0604        05 COMM-GS03-COMMENT5       PIC  X(20).                   00970003
FF0604        05 COMM-GS03-COMMENT6       PIC  X(20).                   00970003
FF0604        05 COMM-GS03-COMMENT7       PIC  X(20).                   00970003
FF0604        05 COMM-GS03-COMMENT8       PIC  X(20).                   00970003
                                                                                
