      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVGS1700                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS1700.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS1700.                                                            
      *}                                                                        
           10 GS17-NCODIC          PIC X(7).                                    
           10 GS17-CSTOCK          PIC X(5).                                    
           10 GS17-NSOCDEPOT       PIC X(3).                                    
           10 GS17-NDEPOT          PIC X(3).                                    
           10 GS17-QSTOCK          PIC S9(5)V USAGE COMP-3.                     
           10 GS17-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
                                                                                
