      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS09   EGS09                                              00000020
      ***************************************************************** 00000030
       01   EGS09I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCODICI   PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLCODICI  PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MSTATUTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAML     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MFAML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFAMF     PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MFAMI     PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSAPPL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MSENSAPPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSENSAPPF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSENSAPPI      PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMARQUEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MMARQUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMARQUEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MMARQUEI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQUEL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLMARQUEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMARQUEF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLMARQUEI      PIC X(20).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSVTEL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MSENSVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSENSVTEF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSENSVTEI      PIC X.                                     00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNMAGI    PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLMAGI    PIC X(20).                                      00000610
           02 MLIGNI OCCURS   11 TIMES .                                00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCENTRL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNSOCENTRL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCENTRF   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNSOCENTRI   PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEPOTL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNDEPOTF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNDEPOTI     PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSELARTL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MCSELARTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCSELARTF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCSELARTI    PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMUTATIONL   COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MMUTATIONL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MMUTATIONF   PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MMUTATIONI   PIC X(7).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMUTEEL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQMUTEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQMUTEEF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQMUTEEI     PIC 9(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCCL   COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MQCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQCCF   PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQCCI   PIC 9(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTRANSITL   COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQTRANSITL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQTRANSITF   PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQTRANSITI   PIC 9(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMUTL  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MDMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDMUTF  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MDMUTI  PIC X(10).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MZONCMDI  PIC X(12).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(61).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: EGS09   EGS09                                              00001200
      ***************************************************************** 00001210
       01   EGS09O REDEFINES EGS09I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MPAGEA    PIC X.                                          00001390
           02 MPAGEC    PIC X.                                          00001400
           02 MPAGEP    PIC X.                                          00001410
           02 MPAGEH    PIC X.                                          00001420
           02 MPAGEV    PIC X.                                          00001430
           02 MPAGEO    PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCODICA   PIC X.                                          00001460
           02 MCODICC   PIC X.                                          00001470
           02 MCODICP   PIC X.                                          00001480
           02 MCODICH   PIC X.                                          00001490
           02 MCODICV   PIC X.                                          00001500
           02 MCODICO   PIC X(7).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MLCODICA  PIC X.                                          00001530
           02 MLCODICC  PIC X.                                          00001540
           02 MLCODICP  PIC X.                                          00001550
           02 MLCODICH  PIC X.                                          00001560
           02 MLCODICV  PIC X.                                          00001570
           02 MLCODICO  PIC X(20).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MSTATUTA  PIC X.                                          00001600
           02 MSTATUTC  PIC X.                                          00001610
           02 MSTATUTP  PIC X.                                          00001620
           02 MSTATUTH  PIC X.                                          00001630
           02 MSTATUTV  PIC X.                                          00001640
           02 MSTATUTO  PIC X(3).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MFAMA     PIC X.                                          00001670
           02 MFAMC     PIC X.                                          00001680
           02 MFAMP     PIC X.                                          00001690
           02 MFAMH     PIC X.                                          00001700
           02 MFAMV     PIC X.                                          00001710
           02 MFAMO     PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLFAMA    PIC X.                                          00001740
           02 MLFAMC    PIC X.                                          00001750
           02 MLFAMP    PIC X.                                          00001760
           02 MLFAMH    PIC X.                                          00001770
           02 MLFAMV    PIC X.                                          00001780
           02 MLFAMO    PIC X(20).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MSENSAPPA      PIC X.                                     00001810
           02 MSENSAPPC PIC X.                                          00001820
           02 MSENSAPPP PIC X.                                          00001830
           02 MSENSAPPH PIC X.                                          00001840
           02 MSENSAPPV PIC X.                                          00001850
           02 MSENSAPPO      PIC X.                                     00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MMARQUEA  PIC X.                                          00001880
           02 MMARQUEC  PIC X.                                          00001890
           02 MMARQUEP  PIC X.                                          00001900
           02 MMARQUEH  PIC X.                                          00001910
           02 MMARQUEV  PIC X.                                          00001920
           02 MMARQUEO  PIC X(5).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLMARQUEA      PIC X.                                     00001950
           02 MLMARQUEC PIC X.                                          00001960
           02 MLMARQUEP PIC X.                                          00001970
           02 MLMARQUEH PIC X.                                          00001980
           02 MLMARQUEV PIC X.                                          00001990
           02 MLMARQUEO      PIC X(20).                                 00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MSENSVTEA      PIC X.                                     00002020
           02 MSENSVTEC PIC X.                                          00002030
           02 MSENSVTEP PIC X.                                          00002040
           02 MSENSVTEH PIC X.                                          00002050
           02 MSENSVTEV PIC X.                                          00002060
           02 MSENSVTEO      PIC X.                                     00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNMAGA    PIC X.                                          00002090
           02 MNMAGC    PIC X.                                          00002100
           02 MNMAGP    PIC X.                                          00002110
           02 MNMAGH    PIC X.                                          00002120
           02 MNMAGV    PIC X.                                          00002130
           02 MNMAGO    PIC X(3).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MLMAGA    PIC X.                                          00002160
           02 MLMAGC    PIC X.                                          00002170
           02 MLMAGP    PIC X.                                          00002180
           02 MLMAGH    PIC X.                                          00002190
           02 MLMAGV    PIC X.                                          00002200
           02 MLMAGO    PIC X(20).                                      00002210
           02 MLIGNO OCCURS   11 TIMES .                                00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MNSOCENTRA   PIC X.                                     00002240
             03 MNSOCENTRC   PIC X.                                     00002250
             03 MNSOCENTRP   PIC X.                                     00002260
             03 MNSOCENTRH   PIC X.                                     00002270
             03 MNSOCENTRV   PIC X.                                     00002280
             03 MNSOCENTRO   PIC X(3).                                  00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MNDEPOTA     PIC X.                                     00002310
             03 MNDEPOTC     PIC X.                                     00002320
             03 MNDEPOTP     PIC X.                                     00002330
             03 MNDEPOTH     PIC X.                                     00002340
             03 MNDEPOTV     PIC X.                                     00002350
             03 MNDEPOTO     PIC X(3).                                  00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MCSELARTA    PIC X.                                     00002380
             03 MCSELARTC    PIC X.                                     00002390
             03 MCSELARTP    PIC X.                                     00002400
             03 MCSELARTH    PIC X.                                     00002410
             03 MCSELARTV    PIC X.                                     00002420
             03 MCSELARTO    PIC X(5).                                  00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MMUTATIONA   PIC X.                                     00002450
             03 MMUTATIONC   PIC X.                                     00002460
             03 MMUTATIONP   PIC X.                                     00002470
             03 MMUTATIONH   PIC X.                                     00002480
             03 MMUTATIONV   PIC X.                                     00002490
             03 MMUTATIONO   PIC X(7).                                  00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MQMUTEEA     PIC X.                                     00002520
             03 MQMUTEEC     PIC X.                                     00002530
             03 MQMUTEEP     PIC X.                                     00002540
             03 MQMUTEEH     PIC X.                                     00002550
             03 MQMUTEEV     PIC X.                                     00002560
             03 MQMUTEEO     PIC -(5).                                  00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MQCCA   PIC X.                                          00002590
             03 MQCCC   PIC X.                                          00002600
             03 MQCCP   PIC X.                                          00002610
             03 MQCCH   PIC X.                                          00002620
             03 MQCCV   PIC X.                                          00002630
             03 MQCCO   PIC -(5).                                       00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MQTRANSITA   PIC X.                                     00002660
             03 MQTRANSITC   PIC X.                                     00002670
             03 MQTRANSITP   PIC X.                                     00002680
             03 MQTRANSITH   PIC X.                                     00002690
             03 MQTRANSITV   PIC X.                                     00002700
             03 MQTRANSITO   PIC -(5).                                  00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MDMUTA  PIC X.                                          00002730
             03 MDMUTC  PIC X.                                          00002740
             03 MDMUTP  PIC X.                                          00002750
             03 MDMUTH  PIC X.                                          00002760
             03 MDMUTV  PIC X.                                          00002770
             03 MDMUTO  PIC X(10).                                      00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MZONCMDA  PIC X.                                          00002800
           02 MZONCMDC  PIC X.                                          00002810
           02 MZONCMDP  PIC X.                                          00002820
           02 MZONCMDH  PIC X.                                          00002830
           02 MZONCMDV  PIC X.                                          00002840
           02 MZONCMDO  PIC X(12).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(61).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
