      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS41   EGS41                                              00000020
      ***************************************************************** 00000030
       01   EGS41I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE                                                            00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MPAGEI    PIC X(3).                                       00000200
      * CODIC                                                           00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      * FAMILLE                                                         00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MCFAMI    PIC X(5).                                       00000300
      * LIBELLE FAMILLE                                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLFAMI    PIC X(19).                                      00000350
      * REFERENCE                                                       00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000370
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MLREFFOURNI    PIC X(20).                                 00000400
      * MARQUE                                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMARQI   PIC X(5).                                       00000450
      * LIBELLE MARQUE                                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MLMARQI   PIC X(19).                                      00000500
           02 M123I OCCURS   11 TIMES .                                 00000510
      * DATE DE MOUVEMENT                                               00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMVTL  COMP PIC S9(4).                                 00000530
      *--                                                                       
             03 MDMVTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDMVTF  PIC X.                                          00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MDMVTI  PIC X(8).                                       00000560
      * CODE OPERATION                                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOPERL      COMP PIC S9(4).                            00000580
      *--                                                                       
             03 MCOPERL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCOPERF      PIC X.                                     00000590
             03 FILLER  PIC X(4).                                       00000600
             03 MCOPERI      PIC X(10).                                 00000610
      * ORIG : SOCIETE                                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCOL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNSOCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSOCOF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNSOCOI      PIC X(3).                                  00000660
      * ORIG : LIEU                                                     00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUOL     COMP PIC S9(4).                            00000680
      *--                                                                       
             03 MNLIEUOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIEUOF     PIC X.                                     00000690
             03 FILLER  PIC X(4).                                       00000700
             03 MNLIEUOI     PIC X(3).                                  00000710
      * ORIG : SOUS-LIEU                                                00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSSLIEUOL   COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MNSSLIEUOL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSSLIEUOF   PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MNSSLIEUOI   PIC X(3).                                  00000760
      * ORIG : TRAITEMENT                                               00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIEUTRTOL  COMP PIC S9(4).                            00000780
      *--                                                                       
             03 MCLIEUTRTOL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCLIEUTRTOF  PIC X.                                     00000790
             03 FILLER  PIC X(4).                                       00000800
             03 MCLIEUTRTOI  PIC X(5).                                  00000810
      * DEST : SOCIETE                                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCDL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MNSOCDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSOCDF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNSOCDI      PIC X(3).                                  00000860
      * DEST : LIEU                                                     00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUDL     COMP PIC S9(4).                            00000880
      *--                                                                       
             03 MNLIEUDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIEUDF     PIC X.                                     00000890
             03 FILLER  PIC X(4).                                       00000900
             03 MNLIEUDI     PIC X(3).                                  00000910
      * DEST : SOUS-LIEU                                                00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSSLIEUDL   COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MNSSLIEUDL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSSLIEUDF   PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MNSSLIEUDI   PIC X(3).                                  00000960
      * DEST : TRAITEMENT                                               00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIEUTRTDL  COMP PIC S9(4).                            00000980
      *--                                                                       
             03 MCLIEUTRTDL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCLIEUTRTDF  PIC X.                                     00000990
             03 FILLER  PIC X(4).                                       00001000
             03 MCLIEUTRTDI  PIC X(5).                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUVTEL    COMP PIC S9(4).                            00001020
      *--                                                                       
             03 MLIEUVTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIEUVTEF    PIC X.                                     00001030
             03 FILLER  PIC X(4).                                       00001040
             03 MLIEUVTEI    PIC X(3).                                  00001050
      * NUMERO D'ORIGINE                                                00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNORIGINEL   COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MNORIGINEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNORIGINEF   PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MNORIGINEI   PIC X(7).                                  00001100
      * QUANTITE                                                        00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMVTL  COMP PIC S9(4).                                 00001120
      *--                                                                       
             03 MQMVTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQMVTF  PIC X.                                          00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MQMVTI  PIC X(6).                                       00001150
      * EDITION                                                         00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWEDITL   COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MWEDITL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWEDITF   PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MWEDITI   PIC X.                                          00001200
      * ZONE CMD AIDA                                                   00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MZONCMDI  PIC X(15).                                      00001250
      * MESSAGE ERREUR                                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLIBERRI  PIC X(58).                                      00001300
      * CODE TRANSACTION                                                00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MCODTRAI  PIC X(4).                                       00001350
      * CICS DE TRAVAIL                                                 00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001370
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001380
           02 FILLER    PIC X(4).                                       00001390
           02 MCICSI    PIC X(5).                                       00001400
      * NETNAME                                                         00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MNETNAMI  PIC X(8).                                       00001450
      * CODE TERMINAL                                                   00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MSCREENI  PIC X(5).                                       00001500
      ***************************************************************** 00001510
      * SDF: EGS41   EGS41                                              00001520
      ***************************************************************** 00001530
       01   EGS41O REDEFINES EGS41I.                                    00001540
           02 FILLER    PIC X(12).                                      00001550
      * DATE DU JOUR                                                    00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MDATJOUA  PIC X.                                          00001580
           02 MDATJOUC  PIC X.                                          00001590
           02 MDATJOUP  PIC X.                                          00001600
           02 MDATJOUH  PIC X.                                          00001610
           02 MDATJOUV  PIC X.                                          00001620
           02 MDATJOUO  PIC X(10).                                      00001630
      * HEURE                                                           00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MTIMJOUA  PIC X.                                          00001660
           02 MTIMJOUC  PIC X.                                          00001670
           02 MTIMJOUP  PIC X.                                          00001680
           02 MTIMJOUH  PIC X.                                          00001690
           02 MTIMJOUV  PIC X.                                          00001700
           02 MTIMJOUO  PIC X(5).                                       00001710
      * PAGE                                                            00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MPAGEA    PIC X.                                          00001740
           02 MPAGEC    PIC X.                                          00001750
           02 MPAGEP    PIC X.                                          00001760
           02 MPAGEH    PIC X.                                          00001770
           02 MPAGEV    PIC X.                                          00001780
           02 MPAGEO    PIC X(3).                                       00001790
      * CODIC                                                           00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNCODICA  PIC X.                                          00001820
           02 MNCODICC  PIC X.                                          00001830
           02 MNCODICP  PIC X.                                          00001840
           02 MNCODICH  PIC X.                                          00001850
           02 MNCODICV  PIC X.                                          00001860
           02 MNCODICO  PIC X(7).                                       00001870
      * FAMILLE                                                         00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MCFAMA    PIC X.                                          00001900
           02 MCFAMC    PIC X.                                          00001910
           02 MCFAMP    PIC X.                                          00001920
           02 MCFAMH    PIC X.                                          00001930
           02 MCFAMV    PIC X.                                          00001940
           02 MCFAMO    PIC X(5).                                       00001950
      * LIBELLE FAMILLE                                                 00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MLFAMA    PIC X.                                          00001980
           02 MLFAMC    PIC X.                                          00001990
           02 MLFAMP    PIC X.                                          00002000
           02 MLFAMH    PIC X.                                          00002010
           02 MLFAMV    PIC X.                                          00002020
           02 MLFAMO    PIC X(19).                                      00002030
      * REFERENCE                                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MLREFFOURNA    PIC X.                                     00002060
           02 MLREFFOURNC    PIC X.                                     00002070
           02 MLREFFOURNP    PIC X.                                     00002080
           02 MLREFFOURNH    PIC X.                                     00002090
           02 MLREFFOURNV    PIC X.                                     00002100
           02 MLREFFOURNO    PIC X(20).                                 00002110
      * MARQUE                                                          00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCMARQA   PIC X.                                          00002140
           02 MCMARQC   PIC X.                                          00002150
           02 MCMARQP   PIC X.                                          00002160
           02 MCMARQH   PIC X.                                          00002170
           02 MCMARQV   PIC X.                                          00002180
           02 MCMARQO   PIC X(5).                                       00002190
      * LIBELLE MARQUE                                                  00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MLMARQA   PIC X.                                          00002220
           02 MLMARQC   PIC X.                                          00002230
           02 MLMARQP   PIC X.                                          00002240
           02 MLMARQH   PIC X.                                          00002250
           02 MLMARQV   PIC X.                                          00002260
           02 MLMARQO   PIC X(19).                                      00002270
           02 M123O OCCURS   11 TIMES .                                 00002280
      * DATE DE MOUVEMENT                                               00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MDMVTA  PIC X.                                          00002310
             03 MDMVTC  PIC X.                                          00002320
             03 MDMVTP  PIC X.                                          00002330
             03 MDMVTH  PIC X.                                          00002340
             03 MDMVTV  PIC X.                                          00002350
             03 MDMVTO  PIC X(8).                                       00002360
      * CODE OPERATION                                                  00002370
             03 FILLER       PIC X(2).                                  00002380
             03 MCOPERA      PIC X.                                     00002390
             03 MCOPERC PIC X.                                          00002400
             03 MCOPERP PIC X.                                          00002410
             03 MCOPERH PIC X.                                          00002420
             03 MCOPERV PIC X.                                          00002430
             03 MCOPERO      PIC X(10).                                 00002440
      * ORIG : SOCIETE                                                  00002450
             03 FILLER       PIC X(2).                                  00002460
             03 MNSOCOA      PIC X.                                     00002470
             03 MNSOCOC PIC X.                                          00002480
             03 MNSOCOP PIC X.                                          00002490
             03 MNSOCOH PIC X.                                          00002500
             03 MNSOCOV PIC X.                                          00002510
             03 MNSOCOO      PIC X(3).                                  00002520
      * ORIG : LIEU                                                     00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MNLIEUOA     PIC X.                                     00002550
             03 MNLIEUOC     PIC X.                                     00002560
             03 MNLIEUOP     PIC X.                                     00002570
             03 MNLIEUOH     PIC X.                                     00002580
             03 MNLIEUOV     PIC X.                                     00002590
             03 MNLIEUOO     PIC X(3).                                  00002600
      * ORIG : SOUS-LIEU                                                00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MNSSLIEUOA   PIC X.                                     00002630
             03 MNSSLIEUOC   PIC X.                                     00002640
             03 MNSSLIEUOP   PIC X.                                     00002650
             03 MNSSLIEUOH   PIC X.                                     00002660
             03 MNSSLIEUOV   PIC X.                                     00002670
             03 MNSSLIEUOO   PIC X(3).                                  00002680
      * ORIG : TRAITEMENT                                               00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MCLIEUTRTOA  PIC X.                                     00002710
             03 MCLIEUTRTOC  PIC X.                                     00002720
             03 MCLIEUTRTOP  PIC X.                                     00002730
             03 MCLIEUTRTOH  PIC X.                                     00002740
             03 MCLIEUTRTOV  PIC X.                                     00002750
             03 MCLIEUTRTOO  PIC X(5).                                  00002760
      * DEST : SOCIETE                                                  00002770
             03 FILLER       PIC X(2).                                  00002780
             03 MNSOCDA      PIC X.                                     00002790
             03 MNSOCDC PIC X.                                          00002800
             03 MNSOCDP PIC X.                                          00002810
             03 MNSOCDH PIC X.                                          00002820
             03 MNSOCDV PIC X.                                          00002830
             03 MNSOCDO      PIC X(3).                                  00002840
      * DEST : LIEU                                                     00002850
             03 FILLER       PIC X(2).                                  00002860
             03 MNLIEUDA     PIC X.                                     00002870
             03 MNLIEUDC     PIC X.                                     00002880
             03 MNLIEUDP     PIC X.                                     00002890
             03 MNLIEUDH     PIC X.                                     00002900
             03 MNLIEUDV     PIC X.                                     00002910
             03 MNLIEUDO     PIC X(3).                                  00002920
      * DEST : SOUS-LIEU                                                00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MNSSLIEUDA   PIC X.                                     00002950
             03 MNSSLIEUDC   PIC X.                                     00002960
             03 MNSSLIEUDP   PIC X.                                     00002970
             03 MNSSLIEUDH   PIC X.                                     00002980
             03 MNSSLIEUDV   PIC X.                                     00002990
             03 MNSSLIEUDO   PIC X(3).                                  00003000
      * DEST : TRAITEMENT                                               00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MCLIEUTRTDA  PIC X.                                     00003030
             03 MCLIEUTRTDC  PIC X.                                     00003040
             03 MCLIEUTRTDP  PIC X.                                     00003050
             03 MCLIEUTRTDH  PIC X.                                     00003060
             03 MCLIEUTRTDV  PIC X.                                     00003070
             03 MCLIEUTRTDO  PIC X(5).                                  00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MLIEUVTEA    PIC X.                                     00003100
             03 MLIEUVTEC    PIC X.                                     00003110
             03 MLIEUVTEP    PIC X.                                     00003120
             03 MLIEUVTEH    PIC X.                                     00003130
             03 MLIEUVTEV    PIC X.                                     00003140
             03 MLIEUVTEO    PIC X(3).                                  00003150
      * NUMERO D'ORIGINE                                                00003160
             03 FILLER       PIC X(2).                                  00003170
             03 MNORIGINEA   PIC X.                                     00003180
             03 MNORIGINEC   PIC X.                                     00003190
             03 MNORIGINEP   PIC X.                                     00003200
             03 MNORIGINEH   PIC X.                                     00003210
             03 MNORIGINEV   PIC X.                                     00003220
             03 MNORIGINEO   PIC X(7).                                  00003230
      * QUANTITE                                                        00003240
             03 FILLER       PIC X(2).                                  00003250
             03 MQMVTA  PIC X.                                          00003260
             03 MQMVTC  PIC X.                                          00003270
             03 MQMVTP  PIC X.                                          00003280
             03 MQMVTH  PIC X.                                          00003290
             03 MQMVTV  PIC X.                                          00003300
             03 MQMVTO  PIC X(6).                                       00003310
      * EDITION                                                         00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MWEDITA   PIC X.                                          00003340
           02 MWEDITC   PIC X.                                          00003350
           02 MWEDITP   PIC X.                                          00003360
           02 MWEDITH   PIC X.                                          00003370
           02 MWEDITV   PIC X.                                          00003380
           02 MWEDITO   PIC X.                                          00003390
      * ZONE CMD AIDA                                                   00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MZONCMDA  PIC X.                                          00003420
           02 MZONCMDC  PIC X.                                          00003430
           02 MZONCMDP  PIC X.                                          00003440
           02 MZONCMDH  PIC X.                                          00003450
           02 MZONCMDV  PIC X.                                          00003460
           02 MZONCMDO  PIC X(15).                                      00003470
      * MESSAGE ERREUR                                                  00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MLIBERRA  PIC X.                                          00003500
           02 MLIBERRC  PIC X.                                          00003510
           02 MLIBERRP  PIC X.                                          00003520
           02 MLIBERRH  PIC X.                                          00003530
           02 MLIBERRV  PIC X.                                          00003540
           02 MLIBERRO  PIC X(58).                                      00003550
      * CODE TRANSACTION                                                00003560
           02 FILLER    PIC X(2).                                       00003570
           02 MCODTRAA  PIC X.                                          00003580
           02 MCODTRAC  PIC X.                                          00003590
           02 MCODTRAP  PIC X.                                          00003600
           02 MCODTRAH  PIC X.                                          00003610
           02 MCODTRAV  PIC X.                                          00003620
           02 MCODTRAO  PIC X(4).                                       00003630
      * CICS DE TRAVAIL                                                 00003640
           02 FILLER    PIC X(2).                                       00003650
           02 MCICSA    PIC X.                                          00003660
           02 MCICSC    PIC X.                                          00003670
           02 MCICSP    PIC X.                                          00003680
           02 MCICSH    PIC X.                                          00003690
           02 MCICSV    PIC X.                                          00003700
           02 MCICSO    PIC X(5).                                       00003710
      * NETNAME                                                         00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MNETNAMA  PIC X.                                          00003740
           02 MNETNAMC  PIC X.                                          00003750
           02 MNETNAMP  PIC X.                                          00003760
           02 MNETNAMH  PIC X.                                          00003770
           02 MNETNAMV  PIC X.                                          00003780
           02 MNETNAMO  PIC X(8).                                       00003790
      * CODE TERMINAL                                                   00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MSCREENA  PIC X.                                          00003820
           02 MSCREENC  PIC X.                                          00003830
           02 MSCREENP  PIC X.                                          00003840
           02 MSCREENH  PIC X.                                          00003850
           02 MSCREENV  PIC X.                                          00003860
           02 MSCREENO  PIC X(5).                                       00003870
                                                                                
