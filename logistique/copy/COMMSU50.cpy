      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      * ATTENTION COMMAREA DE 8K !!!!!!!!!!! LG = +8192            *            
      **************************************************************            
      *                                                                         
      * COM-SU50-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +8192            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-SU50-LONG-COMMAREA PIC S9(4) COMP VALUE +8192.                   
      *--                                                                       
       01  COM-SU50-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +8192.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA                                                  
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS                                   
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION                     
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC XX.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00690010
          02 COMM-DATE-WEEK.                                            00690020
             05  COMM-DATE-SEMSS   PIC 99.                              00690030
             05  COMM-DATE-SEMAA   PIC 99.                              00690040
             05  COMM-DATE-SEMNU   PIC 99.                              00690050
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
      *                                                                         
      * ZONES RESERVEES APPLICATIVES         RESTE = 7970                       
      *                                                                         
      *=> ZONES DE SAUVEGARDE COMMUNES AU MENU PRINCIPAL                        
      *=> ET AUX TRANSACTIONS DE NIVEAU INFERIEUR                               
      *=> COMM-SU50-APPLI   = 175                                               
      *=> COMM-SU50-NLG     = 340                                               
      *                                                                         
           02  COMM-SU50-APPLI.                                                 
              03  COMM-50.                                                      
                  05  COMM-50-DPARIS         PIC X(1).                          
                  05  COMM-50-MZONCMD        PIC X(15).                         
                  05  COMM-50-MCFONC         PIC X(3).                          
                  05  COMM-50-MCGROUP        PIC X(5).                          
                  05  COMM-50-MCFAM          PIC X(5).                          
                  05  COMM-50-MNSOCENT       PIC X(3).                          
                  05  COMM-50-MNDEPOT        PIC X(3).                          
                  05  COMM-50-MNCODIC        PIC X(7).                          
                  05  COMM-50-MCFAMG         PIC X(5).                          
                  05  COMM-50-DSIMULATION    PIC X(8).                          
                  05  COMM-50-NSIMULATION    PIC X(3).                          
                  05  COMM-50-QTE            PIC X(4).                          
                  05  COMM-50-RANG-QTE       PIC 9(1).                          
                  05  COMM-50-QSA            PIC X(3).                          
                  05  COMM-50-RANG-QSA       PIC 9(1).                          
                  05  COMM-50-NMAG           PIC X(3).                          
                  05  COMM-50-DEFFET         PIC X(8).                          
                  05  COMM-50-MCMARQG        PIC X(5).                          
                  05  COMM-50-MCLER          PIC X(5).                          
                  05  COMM-50-FLAG           PIC X(1).                          
                  05  COMM-50-QSTOCK         PIC S9(5) COMP-3 VALUE 0.          
                  05  COMM-50-MESSAGE        PIC X(78).                         
                  05  COMM-50-FILLER         PIC X(05).                         
      * AJOUT NLG                                                               
           02 COMM-SU50-NLG.                                                    
              03 COMM-50-TYPUSER             PIC X(8).                          
              03 COMM-50-TAB-SOC.                                               
                04 COMM-50-AUTOR-DEPOT          OCCURS 10.                      
                  05 COMM-50-AUTOR-NSOC      PIC X(3).                          
                  05 COMM-50-AUTOR-NDEPOT    PIC X(3).                          
              03 COMM-50-NSOCCICS            PIC X(8).                          
              03 COMM-50-CFLGMAG             PIC X(5).                          
              03 COMM-50-NSOC                PIC X(3).                          
NT            03 COMM-50-TCGROUP                OCCURS 50.                      
                 04 COMM-50-CGROUP           PIC X(5).                          
              03 COMM-50-NSOCIETE            PIC X(3).                          
              03 COMM-50-NLIEU               PIC X(3).                          
      *=> ZONES DE SAUVEGARDE PROPRES A UNE TRANSACTION ACTIVE                  
      *=> DE NIVEAU INFERIEUR                                    = 7455         
           02  COMM-51-52-53-54-55           PIC X(7455).                       
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM51                     
           02  COMM-51 REDEFINES COMM-51-52-53-54-55.                           
               03  COMM-51-TYPE-SIMU         PIC X(01).                         
               03  COMM-51-DEFFET-SIMU       PIC X(10).                         
               03  COMM-51-DEFFET-SAMJ       PIC X(8).                          
               03  COMM-51-DFINEFFET-SIMU    PIC X(10).                         
               03  COMM-51-DFINEFFET-SAMJ    PIC X(8).                          
               03  COMM-51-CLE-REPAR         PIC X(01).                         
               03  COMM-51-AFFICH-MESS       PIC X(01).                         
               03  COMM-51-CREATE-NSIMU      PIC X(01).                         
               03  COMM-51-DSYST             PIC S9(13) COMP-3.                 
               03  COMM-51-CODRET            PIC S9(4)  COMP-3.                 
               03  COMM-51-NUMERR            PIC X(04).                         
               03  COMM-51-TRIEDIT           PIC X.                             
               03  COMM-51-MESS              PIC X(78).                         
      *           TABLE DE CONTROLE DES DOUBLONS         12 X 73 = -876         
               03  COMM-51-TABLE.                                               
                   05  COMM-51-T-CODE OCCURS 12.                                
                       07   COMM-51-T-NCODIC  PIC X(07).                        
                       07   COMM-51-T-CEXPO   PIC X(01).                        
                       07   COMM-51-T-CFAM    PIC X(05).                        
                       07   COMM-51-T-LREF    PIC X(20).                        
                       07   COMM-51-T-CLEREP  PIC X(01).                        
                       07   COMM-51-T-GROUPE  PIC X(01).                        
                       07   COMM-51-T-NCODICM PIC X(07).                        
                       07   COMM-51-E-NCODICM PIC X(07).                        
                       07   COMM-51-E-NCODFLG PIC X(01).                        
                       07   COMM-51-T-QSP     PIC X(02).                        
                       07   COMM-51-T-QTE1    PIC X(04).                        
                       07   COMM-51-T-QSA1    PIC X(03).                        
                       07   COMM-51-T-QTE2    PIC X(04).                        
                       07   COMM-51-T-QSA2    PIC X(03).                        
                       07   COMM-51-T-QTE3    PIC X(04).                        
                       07   COMM-51-T-QSA3    PIC X(03).                        
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM52                     
           02  COMM-52-APPLI    REDEFINES COMM-51-52-53-54-55.                  
               05  COMM-52-MENU-20              PIC X(01).                      
               05  COMM-52-TYPE-CODIC           PIC X(01).                      
               05  COMM-52-LMARQ                PIC X(20).                      
               05  COMM-52-LFAM                 PIC X(20).                      
               05  COMM-52-LREFFOURN            PIC X(20).                      
AA0613         05  COMM-52-LSTATCOMP            PIC X(03).                      
               05  COMM-52-DISENT               PIC S9(05) COMP-3.              
               05  COMM-52-ERR                  PIC 9(02).                      
               05  COMM-52-ERR-LAST             PIC 9(02).                      
               05  COMM-52-QCOLIDSTOCK          PIC S9(5) COMP-3.               
      *-----> TABLE DES CODES SELART ASSOCIES                                   
               05  COMM-52-TABLE-SEL-ART.                                       
                   10  COMM-52-POSTE-SEL-ART    OCCURS 50                       
                       INDEXED BY COMM-52-IND1.                                 
                       15  ITEM-SEL-ART              PIC X(05).                 
      *-----> TABLE DES CODICS (MAXIMUM 40)                                     
      *-----> LE CODIC DE RANG 1 EST SYSTEMATIQUEMENT RENSEIGNE                 
      *-----> LES AUTRES POSTES EN TABLE NE SONT UTILISES QUE DANS              
      *-----> LE CAS D'UN CODIC GROUPE (CODICS ELEMENTS DU GROUPE)              
               05  COMM-52-TABLE-CODIC.                                         
                   10  COMM-52-POSTE            OCCURS 40.                      
                       15  COMM-52-NCODIC            PIC X(7).                  
                       15  COMM-52-QTYPLIEN          PIC S9(3) COMP-3.          
                       15  COMM-52-WSEQFAM           PIC S9(5) COMP-3.          
                       15  COMM-52-QNBPIECE          PIC S9(5) COMP-3.          
                       15  COMM-52-WMULTIFAM         PIC X.                     
                       15  COMM-52-CMARQ             PIC X(5).                  
                       15  COMM-52-CFAM              PIC X(5).                  
                       15  COMM-52-CRAYONFAM         PIC X(5).                  
                       15  COMM-52-WRAYONFAM         PIC X.                     
                       15  COMM-52-WSEQED            PIC S9(5) COMP-3.          
                       15  COMM-52-QPOIDS            PIC S9(7) COMP-3.          
                       15  COMM-52-QVOLUME           PIC S9(11) COMP-3.         
                       15  COMM-52-CMODSTOCK         PIC X(5).                  
                       15  COMM-52-CTYPCONDT         PIC X(5).                  
                       15  COMM-52-QCONDT            PIC S9(5) COMP-3.          
      *-----> IMAGE DES INFOS ECRAN  (12 LIGNES MAX)                            
               05  COMM-52-ECRAN.                                               
                   10  COMM-52-LIGNE            OCCURS 12.                      
                       15  COMM-52-NMUTATION         PIC X(7).                  
                       15  COMM-52-DMUTATION         PIC X(8).                  
                       15  COMM-52-CSELART           PIC X(5).                  
                       15  COMM-52-QNBM3QUOTA        PIC S9(11) COMP-3.         
                       15  COMM-52-M3QUOTA          PIC S9(3)V99 COMP-3.        
                       15  COMM-52-QNBPQUOTA         PIC S9(5) COMP-3.          
                       15  COMM-52-QVOLUME-TRANS     PIC S9(11) COMP-3.         
                       15  COMM-52-QNBPIECES-TRANS PIC S9(5) COMP-3.            
                       15  COMM-52-QNBLIGNES         PIC S9(5) COMP-3.          
                       15  COMM-52-QSAISIE           PIC S9(5) COMP-3.          
ADBRID****     AJOUT DES ZONES NECESSAIRE AU BRIDAGE DE LA SASIE                
               05  COMM-52-FLAG-USER             PIC X.                         
                   88  COMM-52-USER-BRIDE        VALUE 'O'.                     
                   88  COMM-52-USER-PAS-BRIDE    VALUE 'N'.                     
               05  COMM-RM52-DATE-MAX            PIC X(08).                     
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM53                     
           02  COMM-53-APPLI    REDEFINES COMM-51-52-53-54-55.                  
               05  COMM-53-FLAG-NEW          PIC X(01).                         
               05  COMM-53-NEW-PAGE          PIC X(01).                         
               05  COMM-53-NB-PAGE           PIC 9(02).                         
               05  COMM-53-NB-COLS           PIC 9(02).                         
               05  COMM-53-CURRENT-PAGE      PIC 9(02).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05  COMM-53-NB-TS-ITEM        PIC 9(18) COMP.                    
      *--                                                                       
               05  COMM-53-NB-TS-ITEM        PIC 9(18) COMP-5.                  
      *}                                                                        
               05  COMM-53-TYPE-CODIC           PIC X(01).                      
               05  COMM-53-LMARQ                PIC X(20).                      
               05  COMM-53-LFAM                 PIC X(20).                      
               05  COMM-53-LREFFOURN            PIC X(20).                      
               05  COMM-53-DISENT               PIC S9(05) COMP-3.              
               05  COMM-53-ERR                  PIC 9(02).                      
               05  COMM-53-ERR-PAGE             PIC 9(02).                      
               05  COMM-53-ERR-LAST             PIC 9(02).                      
               05  COMM-53-QCOLIDSTOCK          PIC S9(5) COMP-3.               
      *-----> TABLE DES CODES SELART ASSOCIES                                   
               05  COMM-53-TABLE-SEL-ART.                                       
                   10  COMM-53-POSTE-SEL-ART    OCCURS 50                       
                       INDEXED BY COMM-53-IND1.                                 
                       15  FILLER                    PIC X(05).                 
      *-----> TABLE DES CODICS (MAXIMUM 40)                                     
      *-----> LE CODIC DE RANG 1 EST SYSTEMATIQUEMENT RENSEIGNE                 
      *-----> LES AUTRES POSTES EN TABLE NE SONT UTILIS�S QUE DANS              
      *-----> LE CAS D'UN CODIC GROUPE (CODICS ELEMENTS DU GROUPE)              
               05  COMM-53-TABLE-CODIC.                                         
                   10  COMM-53-POSTE            OCCURS 40.                      
                       15  COMM-53-NCODIC            PIC X(7).                  
                       15  COMM-53-QTYPLIEN          PIC S9(3) COMP-3.          
                       15  COMM-53-WSEQFAM           PIC S9(5) COMP-3.          
                       15  COMM-53-QNBPIECE          PIC S9(5) COMP-3.          
                       15  COMM-53-WMULTIFAM         PIC X.                     
                       15  COMM-53-CMARQ             PIC X(5).                  
                       15  COMM-53-CFAM              PIC X(5).                  
                       15  COMM-53-CRAYONFAM         PIC X(5).                  
                       15  COMM-53-WRAYONFAM         PIC X.                     
                       15  COMM-53-WSEQED            PIC S9(5) COMP-3.          
                       15  COMM-53-QPOIDS            PIC S9(7) COMP-3.          
                       15  COMM-53-QVOLUME           PIC S9(11) COMP-3.         
                       15  COMM-53-CMODSTOCK         PIC X(5).                  
                       15  COMM-53-CTYPCONDT         PIC X(5).                  
                       15  COMM-53-QCONDT            PIC S9(5) COMP-3.          
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM54        -131         
           02  COMM-54 REDEFINES COMM-51-52-53-54-55.                           
               03  COMM-54-PAGE              PIC 9(2) COMP-3.                   
               03  COMM-54-PAGE-MAX          PIC 9(2) COMP-3.                   
               03  COMM-54-MNCODIC           PIC X(07).                         
               03  COMM-54-MREFFOURN         PIC X(20).                         
               03  COMM-54-MDSIMUL           PIC X(10).                         
               03  COMM-54-MCFAM             PIC X(05).                         
               03  COMM-54-MLFAM             PIC X(20).                         
               03  COMM-54-MNSIMUL           PIC X(03).                         
               03  COMM-54-MCMARQ            PIC X(05).                         
               03  COMM-54-MLMARQ            PIC X(20).                         
               03  COMM-54-MCGROUP           PIC X(05).                         
               03  COMM-54-MNSOCEN           PIC X(03).                         
               03  COMM-54-MNDEPOT           PIC X(03).                         
               03  COMM-54-MDEFFET           PIC X(10).                         
               03  COMM-54-MDFINEFF          PIC X(10).                         
               03  COMM-54-MQSOD             PIC X(03).                         
               03  COMM-54-MQSAD             PIC X(03).                         
               03  COMM-54-TOT-QSO           PIC 9(03).                         
               03  COMM-54-TOT-QSA           PIC 9(03).                         
               03  COMM-54-FLAG              PIC X(01).                         
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM55         -45         
           02  COMM-55 REDEFINES COMM-51-52-53-54-55.                           
               03  COMM-55-PAGE              PIC S9(3) COMP-3.                  
               03  COMM-55-PAGE-MAX          PIC S9(3) COMP-3.                  
               03  COMM-55-MCGROUP           PIC X(05).                         
      * DEB MOD NLG                                                             
               03  COMM-55-MNSOCR            PIC X(03).                         
      * FIN MOD NLG                                                             
               03  COMM-55-MNMAGR            PIC X(03).                         
               03  COMM-55-MNCODICR          PIC X(07).                         
               03  COMM-55-MCFAMR            PIC X(05).                         
               03  COMM-55-MCMARQR           PIC X(05).                         
               03  COMM-55-MFLAGR            PIC X(01).                         
               03  COMM-55-MDEFFETM          PIC X(08).                         
               03  COMM-55-MDEFFETR          PIC X(08).                         
               03  COMM-55-MDFINEFFR         PIC X(08).                         
           02  COMM-55-APPLI REDEFINES COMM-51-52-53-54-55.                     
               03  COMM-55-CDRET             PIC X(4).                          
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM56/RM57    -30         
           02  COMM-56 REDEFINES COMM-51-52-53-54-55.                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  COMM-56-PAGE              PIC S9(4) COMP.                    
      *--                                                                       
               03  COMM-56-PAGE              PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  COMM-56-PAGE-TOT          PIC S9(4) COMP.                    
      *--                                                                       
               03  COMM-56-PAGE-TOT          PIC S9(4) COMP-5.                  
      *}                                                                        
               03  COMM-56-NSOCIETE          PIC X(3).                          
               03  COMM-56-LGROUP            PIC X(20).                         
               03  COMM-56-MAJ               PIC X(1).                          
      *    TABLE DE TRAVAIL                        3 X 17 X 69  = -3519         
               03  COMM-56-TABLE.                                               
                   05  COMM-56-T-PAGE OCCURS 3.                                 
                       10  COMM-56-T-CODE OCCURS 17.                            
                           15   COMM-56-T-NMAG1   PIC X(03).                    
                           15   COMM-56-T-LMAG1   PIC X(17).                    
                           15   COMM-56-T-QTE1    PIC X(03).                    
                           15   COMM-56-T-NMAG2   PIC X(03).                    
                           15   COMM-56-T-LMAG2   PIC X(17).                    
                           15   COMM-56-T-QTE2    PIC X(03).                    
                           15   COMM-56-T-NMAG3   PIC X(03).                    
                           15   COMM-56-T-LMAG3   PIC X(17).                    
                           15   COMM-56-T-QTE3    PIC X(03).                    
           02  COMM-RM58 REDEFINES COMM-51-52-53-54-55.                         
               03  COMM-RM58-PAGE            PIC 9(3) COMP-3.                   
               03  COMM-RM58-PAGE-MAX        PIC 9(3) COMP-3.                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  COMM-RM58-ITEMAX          PIC S9(4) COMP.                    
      *--                                                                       
               03  COMM-RM58-ITEMAX          PIC S9(4) COMP-5.                  
      *}                                                                        
               03  COMM-RM58-MODIF-DONNEE    PIC X.                             
               03  COMM-RM58-NSOCIETE        PIC X(3).                          
               03  COMM-RM58-DEMAIN          PIC X(8).                          
               03  COMM-RM58-MNCODIC         PIC X(7).                          
               03  COMM-RM58-LREFFOURN       PIC X(20).                         
               03  COMM-RM58-LAGREG          PIC X(20).                         
               03  COMM-RM58-CMARQ           PIC X(20).                         
               03  COMM-RM58-LSTATCOMP       PIC X(03).                         
               03  COMM-RM58-CFAM            PIC X(5).                          
               03  COMM-RM58-W20             PIC X.                             
               03  COMM-RM58-W80             PIC X.                             
               03  COMM-RM58-NTABLE          PIC XX.                            
               03  COMM-RM58-NTABLESA        PIC XX.                            
               03  COMM-RM58-LGROUP          PIC X(20).                         
               03  COMM-RM58-MAJ             PIC X(1).                          
                                                                                
