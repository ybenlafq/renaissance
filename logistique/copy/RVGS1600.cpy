      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGS1600                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS1600                         
      **********************************************************                
       01  RVGS1000.                                                            
           02  GS16-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GS16-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GS16-NCODIC                                                      
               PIC X(0007).                                                     
           02  GS16-LCOMMENT1            PIC X(20).                             
           02  GS16-LCOMMENT2            PIC X(20).                             
           02  GS16-LCOMMENT3            PIC X(20).                             
           02  GS16-LCOMMENT4            PIC X(20).                             
           02  GS16-LCOMMENT5            PIC X(20).                             
           02  GS16-LCOMMENT6            PIC X(20).                             
           02  GS16-LCOMMENT7            PIC X(20).                             
           02  GS16-LCOMMENT8            PIC X(20).                             
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGS1600                                  
      **********************************************************                
       01  RVGS1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GS16-NSOCDEPOT-F        PIC S9(4) USAGE COMP.                     
      *--                                                                       
           02 GS16-NSOCDEPOT-F        PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GS16-NDEPOT-F           PIC S9(4) USAGE COMP.                     
      *--                                                                       
           02 GS16-NDEPOT-F           PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GS16-NCODIC-F           PIC S9(4) USAGE COMP.                     
      *--                                                                       
           02 GS16-NCODIC-F           PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GS16-LCOMMENT1-F        PIC S9(4) USAGE COMP.                     
      *--                                                                       
           02 GS16-LCOMMENT1-F        PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GS16-LCOMMENT2-F        PIC S9(4) USAGE COMP.                     
      *--                                                                       
           02 GS16-LCOMMENT2-F        PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GS16-LCOMMENT3-F        PIC S9(4) USAGE COMP.                     
      *--                                                                       
           02 GS16-LCOMMENT3-F        PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GS16-LCOMMENT4-F        PIC S9(4) USAGE COMP.                     
      *--                                                                       
           02 GS16-LCOMMENT4-F        PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GS16-LCOMMENT5-F        PIC S9(4) USAGE COMP.                     
      *--                                                                       
           02 GS16-LCOMMENT5-F        PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GS16-LCOMMENT6-F        PIC S9(4) USAGE COMP.                     
      *--                                                                       
           02 GS16-LCOMMENT6-F        PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GS16-LCOMMENT7-F        PIC S9(4) USAGE COMP.                     
      *--                                                                       
           02 GS16-LCOMMENT7-F        PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 GS16-LCOMMENT8-F        PIC S9(4) USAGE COMP.                     
      *                                                                         
      *--                                                                       
           02 GS16-LCOMMENT8-F        PIC S9(4) COMP-5.                         
                                                                                
      *}                                                                        
