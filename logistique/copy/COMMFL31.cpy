      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TFL31 (TFL30 -> MENU)    TR: FL30  *    00002201
      *          GESTION DES PARAMETRES GENERAUX LIVRAISON         *    00002301
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *   RANSACTION FL31 : DESCRIPTION DES PROFILS                *    00411001
      *                     D'AFFILIATION ( FAMILLE / ENTREPOTS )  *    00411101
      *                                                                 00412000
          03 COMM-FL31-APPLI REDEFINES COMM-FL30-FILLER.                00420005
      *------------------------------ ZONE DONNEES TFL31                00510001
             04 COMM-FL31-DONNEES-1-TFL31.                              00520005
      *------------------------------ CODE FONCTION                     00550001
                05 COMM-FL31-FONCT             PIC X(3).                00560005
      *------------------------------ CODE PROFIL                       00561001
                05 COMM-FL31-CPROAFF           PIC X(5).                00562005
      *------------------------------ LIBELLE PROFIL                    00570001
                05 COMM-FL31-LPROAFF           PIC X(20).               00580005
      *------------------------------ CODE FAMILLE                      00590001
                05 COMM-FL31-CFAMIL            PIC X(5).                00600005
      *------------------------------ LIBELLE FAMILLE                   00610001
                05 COMM-FL31-LFAMIL            PIC X(20).               00620005
      *------------------------------ NUMERO SEQUENCE FAMILLE           00630001
                05 COMM-FL31-WSEQFAM           PIC S9(5).               00640005
      *------------------------------ DERNIER IND-TS                    00742401
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-FL31-PAGSUI            PIC S9(4) COMP.          00742505
      *--                                                                       
                05 COMM-FL31-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN IND-TS                     00742601
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-FL31-PAGENC            PIC S9(4) COMP.          00742705
      *--                                                                       
                05 COMM-FL31-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00742801
                05 COMM-FL31-NUMPAG            PIC 9(3).                00742905
      *------------------------------ NOMBRE DE PAGE TOTAL              00743001
                05 COMM-FL31-NBRPAG            PIC 9(3).                00743105
      *------------------------------ INDICATEUR FIN DE TABLE           00743201
                05 COMM-FL31-INDPAG            PIC 9.                   00743305
      *------------------------------ ZONE MODIF TS                     00743401
                05 COMM-FL31-MODIF-TS          PIC 9(1).                00743505
      *------------------------------ ZONE EXCEPTION                    00743601
                05 COMM-FL31-EXCEPTION         PIC 9(1).                00743705
      *------------------------------ ZONE TEST NUM RENTRE              00743908
                05 COMM-FL31-FLAG-PAG          PIC 9(1).                00744009
      *------------------------------ ZONE BROWSING                     00744106
                05 COMM-FL31-BROWSING          PIC 9(1).                00744205
      *------------------------------ PROG XCTL                         00744301
                05 COMM-FL31-NOM-PROG-XCTL     PIC X(5).                00744405
      *------------------------------ ZONE BROWSING                     00744501
                05 COMM-FL31-COM-PGMPRC        PIC X(5).                00744605
      *------------------------------ OK MAJ SUR RTGQ05 => VALID = '1'  00744701
                05 COMM-FL31-VALID-RTGQ05   PIC  9.                     00744805
      *------------------------------ OK MAJ SUR RTGA01 => VALID = '1'  00744901
                05 COMM-FL31-VALID-RTGA01   PIC  9.                     00745005
      *------------------------------    MAJ SUR RTGA01 => VALID = 'M'  00745101
                05 COMM-FL31-CMAJ-RTGA01    PIC  X.                     00745205
      *------------------------------ TABLE N� TS PR PAGINATION         00745301
                05 COMM-FL31-TABTS          OCCURS 100.                 00745405
      *------------------------------ 1ERS IND TS DES PAGES <=> FAMILLE 00745501
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               07 COMM-FL31-NUMTS          PIC S9(4) COMP.       00745605
      *--                                                                       
                      07 COMM-FL31-NUMTS          PIC S9(4) COMP-5.             
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS LE DEPOT      00745701
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-FL31-IND-NDEPOT     PIC S9(4) COMP.             00745805
      *--                                                                       
                05 COMM-FL31-IND-NDEPOT     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND DE SVG DE TS                  00745901
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-FL31-TS-ENCOURS     PIC S9(4) COMP.             00746005
      *--                                                                       
                05 COMM-FL31-TS-ENCOURS     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS               00746101
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-FL31-IND-RECH-TS    PIC S9(4) COMP.             00746205
      *--                                                                       
                05 COMM-FL31-IND-RECH-TS    PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND TS MAX                        00746301
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-FL31-IND-TS-MAX     PIC S9(4) COMP.             00747005
      *--                                                                       
                05 COMM-FL31-IND-TS-MAX     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ ZONE DONNEES 2 TFL31              00760001
             04 COMM-FL31-DONNEES-2-TFL31.                              00761005
      *------------------------------ CODES D'AFFILIATION PAR DEFAUT    00762001
                05 COMM-FL31-ENTAFF-DF      OCCURS 5.                   00763005
      *------------------------------ CODE SOCIETE D'AFFILIATION DEFAUT 00764001
                   06 COMM-FL31-CSOCAFF-DF  PIC X(03).                  00765005
      *------------------------------ CODE ENTREPOT D'AFFILIATION DEFAUT00766001
                   06 COMM-FL31-CDEPAFF-DF  PIC X(03).                  00767005
      *------------------------------ ZONE FAMILLES PAGE                00770001
                05 COMM-FL31-TABFAM.                                    00780005
      *------------------------------ ZONE LIGNES FAMILLES              00790001
                   06 COMM-FL31-LIGFAM  OCCURS 15.                      00800005
      *------------------------------ CODE EXCEPTION                    00801001
                      07 COMM-FL31-CEXC           PIC X(1).             00802005
      *------------------------------ CODE FAMILLE                      00810001
                      07 COMM-FL31-CFAM           PIC X(5).             00820005
      *------------------------------ LIBELLE FAMILLE                   00830001
                      07 COMM-FL31-LFAM           PIC X(20).            00840005
      *------------------------------ CODES D'AFFILIATION               00850001
                      07 COMM-FL31-ENTAFF         OCCURS 5.             00860005
      *------------------------------ CODE SOCIETE D'AFFILIATION        00870001
                         08 COMM-FL31-CSOCAFF     PIC X(03).            00880005
      *------------------------------ CODE ENTREPOT D'AFFILIATION       00890001
                         08 COMM-FL31-CDEPAFF     PIC X(03).            00900005
      *------------------------------ ZONE DONNEES 3 TFL31              01650001
             04 COMM-FL31-DONNEES-3-TFL31.                              01651005
      *------------------------------ ZONES SWAP                        01670001
                   06 COMM-FL31-ATTR           PIC X OCCURS 500.        01680005
      *------------------------------ ZONE LIBRE                        01710001
      *      03 FILLER                   PIC X(2060).                   01711004
      *      03 COMM-FL31-LIBRE          PIC X(2061).                   01720002
      ***************************************************************** 02170001
                                                                                
