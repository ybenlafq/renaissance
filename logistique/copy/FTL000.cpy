      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
***********************************************************************         
************* FICHIER DES VENTES / ARTICLES EXTRAIRES POUR LES TOURNEES         
*******       LONGUEUR : 74 CARACTERES                          *******         
***********************************************************************         
       01  FTL000-DSECT.                                                        
           05 FTL000-CPLAN         PIC  X(5).                                   
           05 FTL000-NSOC          PIC  X(3).                                   
           05 FTL000-CMODDEL       PIC  X(3).                                   
           05 FTL000-CPROTOUR      PIC  X(5).                                   
           05 FTL000-DLIVR         PIC  X(8).                                   
           05 FTL000-NMAG          PIC  X(3).                                   
           05 FTL000-NVENTE        PIC  X(7).                                   
           05 FTL000-CADRESSE      PIC  X(1).                                   
           05 FTL000-NCODIC        PIC  X(7).                                   
           05 FTL000-CPLAGE        PIC  X(2).                                   
           05 FTL000-CFAM          PIC  X(5).                                   
           05 FTL000-WVISITE       PIC  X(1).                                   
           05 FTL000-LPRODLIVR.                                                 
              10 FTL000-LIVR       PIC  9         OCCURS 10.                    
           05 FTL000-LPRODLIVR-R   REDEFINES      FTL000-LPRODLIVR.             
              10 FTL000-LIVR-R     PIC  X         OCCURS 10.                    
           05 FTL000-LPRODREPR.                                                 
              10 FTL000-REPR       PIC  9         OCCURS 10.                    
           05 FTL000-LPRODREPR-R   REDEFINES      FTL000-LPRODREPR.             
              10 FTL000-REPR-R     PIC  X         OCCURS 10.                    
           05 FTL000-QVENDUE       PIC  9(5)      COMP-3.                       
           05 FTL000-WTOURNEE      PIC  X(1).                                   
                                                                                
