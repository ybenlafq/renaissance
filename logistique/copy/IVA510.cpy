      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA510 AU 31/05/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,02,BI,A,                          *        
      *                           09,03,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,07,BI,A,                          *        
      *                           22,03,BI,A,                          *        
      *                           25,03,PD,A,                          *        
      *                           28,05,BI,A,                          *        
      *                           33,07,BI,A,                          *        
      *                           40,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA510.                                                        
            05 NOMETAT-IVA510           PIC X(6) VALUE 'IVA510'.                
            05 RUPTURES-IVA510.                                                 
           10 IVA510-CSEQRAYON          PIC X(02).                      007  002
           10 IVA510-NSOCVALO           PIC X(03).                      009  003
           10 IVA510-NLIEUVALO          PIC X(03).                      012  003
           10 IVA510-NORIGINE           PIC X(07).                      015  007
           10 IVA510-CNATURE            PIC X(03).                      022  003
           10 IVA510-WSEQFAM            PIC S9(05)      COMP-3.         025  003
           10 IVA510-CMARQ              PIC X(05).                      028  005
           10 IVA510-NCODIC             PIC X(07).                      033  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA510-SEQUENCE           PIC S9(04) COMP.                040  002
      *--                                                                       
           10 IVA510-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA510.                                                   
           10 IVA510-CFAM               PIC X(05).                      042  005
           10 IVA510-CRAYON1            PIC X(05).                      047  005
           10 IVA510-CTYPMVTVALO-EDT    PIC X(01).                      052  001
           10 IVA510-DEVISE             PIC X(03).                      053  003
           10 IVA510-DOPER              PIC X(06).                      056  006
           10 IVA510-LIB1               PIC X(01).                      062  001
           10 IVA510-LIB11              PIC X(01).                      063  001
           10 IVA510-LIB2               PIC X(01).                      064  001
           10 IVA510-LIB21              PIC X(01).                      065  001
           10 IVA510-LLIEU              PIC X(20).                      066  020
           10 IVA510-LREFFOURN          PIC X(20).                      086  020
           10 IVA510-NSOCLIEUVALODEST   PIC X(06).                      106  006
           10 IVA510-NSOCLIEUVALOORIG   PIC X(06).                      112  006
           10 IVA510-PRA                PIC S9(07)V9(2) COMP-3.         118  005
           10 IVA510-PRMP               PIC S9(07)V9(6) COMP-3.         123  007
           10 IVA510-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         130  008
           10 IVA510-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         138  008
           10 IVA510-QSTOCKENTREE       PIC S9(11)      COMP-3.         146  006
           10 IVA510-QSTOCKSORTIE       PIC S9(11)      COMP-3.         152  006
           10 IVA510-TXEURO             PIC S9(01)V9(5) COMP-3.         158  004
            05 FILLER                      PIC X(351).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IVA510-LONG           PIC S9(4)   COMP  VALUE +161.           
      *                                                                         
      *--                                                                       
        01  DSECT-IVA510-LONG           PIC S9(4) COMP-5  VALUE +161.           
                                                                                
      *}                                                                        
