      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ETL05   ETL05                                              00000020
      ***************************************************************** 00000030
       01   ETL05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLSOCI    PIC X(20).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLIVRL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDLIVRF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDLIVRI   PIC X(8).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWLIVRL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MWLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWLIVRF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MWLIVRI   PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWREPRL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MWREPRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWREPRF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MWREPRI   PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLISTL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLISTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLISTF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLISTI    PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPBLL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MIMPBLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MIMPBLF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MIMPBLI   PIC X(4).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPBRL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MIMPBRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MIMPBRF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MIMPBRI   PIC X(4).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPLISTL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MIMPLISTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MIMPLISTF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MIMPLISTI      PIC X(4).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CPROFL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 CPROFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CPROFF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 CPROFI    PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTOURNL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLTOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTOURNF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLTOURNI  PIC X(14).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTOURNL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCTOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTOURNF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCTOURNI  PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLOGITL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLLOGITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLOGITF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLLOGITI  PIC X(17).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLOGITL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCLOGITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLOGITF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCLOGITI  PIC X.                                          00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFGCOMPL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MLFGCOMPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLFGCOMPF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLFGCOMPI      PIC X(13).                                 00000730
      * flag complement                                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFGCOMPLL      COMP PIC S9(4).                            00000750
      *--                                                                       
           02 MFGCOMPLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFGCOMPLF      PIC X.                                     00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MFGCOMPLI      PIC X.                                     00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFGVENTL      COMP PIC S9(4).                            00000790
      *--                                                                       
           02 MLFGVENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLFGVENTF      PIC X.                                     00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLFGVENTI      PIC X(13).                                 00000820
      * flag profil vente                                               00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFGVENTL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MFGVENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFGVENTF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MFGVENTI  PIC X.                                          00000870
           02 M147I OCCURS   11 TIMES .                                 00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000890
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MNMAGI  PIC X(3).                                       00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MNVENTEI     PIC X(7).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MZONCMDI  PIC X(15).                                      00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MLIBERRI  PIC X(58).                                      00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MCODTRAI  PIC X(4).                                       00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MCICSI    PIC X(5).                                       00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MNETNAMI  PIC X(8).                                       00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MSCREENI  PIC X(4).                                       00001200
      ***************************************************************** 00001210
      * SDF: ETL05   ETL05                                              00001220
      ***************************************************************** 00001230
       01   ETL05O REDEFINES ETL05I.                                    00001240
           02 FILLER    PIC X(12).                                      00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MDATJOUA  PIC X.                                          00001270
           02 MDATJOUC  PIC X.                                          00001280
           02 MDATJOUP  PIC X.                                          00001290
           02 MDATJOUH  PIC X.                                          00001300
           02 MDATJOUV  PIC X.                                          00001310
           02 MDATJOUO  PIC X(10).                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MTIMJOUA  PIC X.                                          00001340
           02 MTIMJOUC  PIC X.                                          00001350
           02 MTIMJOUP  PIC X.                                          00001360
           02 MTIMJOUH  PIC X.                                          00001370
           02 MTIMJOUV  PIC X.                                          00001380
           02 MTIMJOUO  PIC X(5).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNSOCA    PIC X.                                          00001410
           02 MNSOCC    PIC X.                                          00001420
           02 MNSOCP    PIC X.                                          00001430
           02 MNSOCH    PIC X.                                          00001440
           02 MNSOCV    PIC X.                                          00001450
           02 MNSOCO    PIC X(3).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MLSOCA    PIC X.                                          00001480
           02 MLSOCC    PIC X.                                          00001490
           02 MLSOCP    PIC X.                                          00001500
           02 MLSOCH    PIC X.                                          00001510
           02 MLSOCV    PIC X.                                          00001520
           02 MLSOCO    PIC X(20).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MDLIVRA   PIC X.                                          00001550
           02 MDLIVRC   PIC X.                                          00001560
           02 MDLIVRP   PIC X.                                          00001570
           02 MDLIVRH   PIC X.                                          00001580
           02 MDLIVRV   PIC X.                                          00001590
           02 MDLIVRO   PIC X(8).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MWLIVRA   PIC X.                                          00001620
           02 MWLIVRC   PIC X.                                          00001630
           02 MWLIVRP   PIC X.                                          00001640
           02 MWLIVRH   PIC X.                                          00001650
           02 MWLIVRV   PIC X.                                          00001660
           02 MWLIVRO   PIC X.                                          00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MWREPRA   PIC X.                                          00001690
           02 MWREPRC   PIC X.                                          00001700
           02 MWREPRP   PIC X.                                          00001710
           02 MWREPRH   PIC X.                                          00001720
           02 MWREPRV   PIC X.                                          00001730
           02 MWREPRO   PIC X.                                          00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLISTA    PIC X.                                          00001760
           02 MLISTC    PIC X.                                          00001770
           02 MLISTP    PIC X.                                          00001780
           02 MLISTH    PIC X.                                          00001790
           02 MLISTV    PIC X.                                          00001800
           02 MLISTO    PIC X.                                          00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MIMPBLA   PIC X.                                          00001830
           02 MIMPBLC   PIC X.                                          00001840
           02 MIMPBLP   PIC X.                                          00001850
           02 MIMPBLH   PIC X.                                          00001860
           02 MIMPBLV   PIC X.                                          00001870
           02 MIMPBLO   PIC X(4).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MIMPBRA   PIC X.                                          00001900
           02 MIMPBRC   PIC X.                                          00001910
           02 MIMPBRP   PIC X.                                          00001920
           02 MIMPBRH   PIC X.                                          00001930
           02 MIMPBRV   PIC X.                                          00001940
           02 MIMPBRO   PIC X(4).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MIMPLISTA      PIC X.                                     00001970
           02 MIMPLISTC PIC X.                                          00001980
           02 MIMPLISTP PIC X.                                          00001990
           02 MIMPLISTH PIC X.                                          00002000
           02 MIMPLISTV PIC X.                                          00002010
           02 MIMPLISTO      PIC X(4).                                  00002020
           02 FILLER    PIC X(2).                                       00002030
           02 CPROFA    PIC X.                                          00002040
           02 CPROFC    PIC X.                                          00002050
           02 CPROFP    PIC X.                                          00002060
           02 CPROFH    PIC X.                                          00002070
           02 CPROFV    PIC X.                                          00002080
           02 CPROFO    PIC X(5).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MLTOURNA  PIC X.                                          00002110
           02 MLTOURNC  PIC X.                                          00002120
           02 MLTOURNP  PIC X.                                          00002130
           02 MLTOURNH  PIC X.                                          00002140
           02 MLTOURNV  PIC X.                                          00002150
           02 MLTOURNO  PIC X(14).                                      00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MCTOURNA  PIC X.                                          00002180
           02 MCTOURNC  PIC X.                                          00002190
           02 MCTOURNP  PIC X.                                          00002200
           02 MCTOURNH  PIC X.                                          00002210
           02 MCTOURNV  PIC X.                                          00002220
           02 MCTOURNO  PIC X(3).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MLLOGITA  PIC X.                                          00002250
           02 MLLOGITC  PIC X.                                          00002260
           02 MLLOGITP  PIC X.                                          00002270
           02 MLLOGITH  PIC X.                                          00002280
           02 MLLOGITV  PIC X.                                          00002290
           02 MLLOGITO  PIC X(17).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MCLOGITA  PIC X.                                          00002320
           02 MCLOGITC  PIC X.                                          00002330
           02 MCLOGITP  PIC X.                                          00002340
           02 MCLOGITH  PIC X.                                          00002350
           02 MCLOGITV  PIC X.                                          00002360
           02 MCLOGITO  PIC X.                                          00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MLFGCOMPA      PIC X.                                     00002390
           02 MLFGCOMPC PIC X.                                          00002400
           02 MLFGCOMPP PIC X.                                          00002410
           02 MLFGCOMPH PIC X.                                          00002420
           02 MLFGCOMPV PIC X.                                          00002430
           02 MLFGCOMPO      PIC X(13).                                 00002440
      * flag complement                                                 00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MFGCOMPLA      PIC X.                                     00002470
           02 MFGCOMPLC PIC X.                                          00002480
           02 MFGCOMPLP PIC X.                                          00002490
           02 MFGCOMPLH PIC X.                                          00002500
           02 MFGCOMPLV PIC X.                                          00002510
           02 MFGCOMPLO      PIC X.                                     00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLFGVENTA      PIC X.                                     00002540
           02 MLFGVENTC PIC X.                                          00002550
           02 MLFGVENTP PIC X.                                          00002560
           02 MLFGVENTH PIC X.                                          00002570
           02 MLFGVENTV PIC X.                                          00002580
           02 MLFGVENTO      PIC X(13).                                 00002590
      * flag profil vente                                               00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MFGVENTA  PIC X.                                          00002620
           02 MFGVENTC  PIC X.                                          00002630
           02 MFGVENTP  PIC X.                                          00002640
           02 MFGVENTH  PIC X.                                          00002650
           02 MFGVENTV  PIC X.                                          00002660
           02 MFGVENTO  PIC X.                                          00002670
           02 M147O OCCURS   11 TIMES .                                 00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MNMAGA  PIC X.                                          00002700
             03 MNMAGC  PIC X.                                          00002710
             03 MNMAGP  PIC X.                                          00002720
             03 MNMAGH  PIC X.                                          00002730
             03 MNMAGV  PIC X.                                          00002740
             03 MNMAGO  PIC X(3).                                       00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MNVENTEA     PIC X.                                     00002770
             03 MNVENTEC     PIC X.                                     00002780
             03 MNVENTEP     PIC X.                                     00002790
             03 MNVENTEH     PIC X.                                     00002800
             03 MNVENTEV     PIC X.                                     00002810
             03 MNVENTEO     PIC X(7).                                  00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MZONCMDA  PIC X.                                          00002840
           02 MZONCMDC  PIC X.                                          00002850
           02 MZONCMDP  PIC X.                                          00002860
           02 MZONCMDH  PIC X.                                          00002870
           02 MZONCMDV  PIC X.                                          00002880
           02 MZONCMDO  PIC X(15).                                      00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MLIBERRA  PIC X.                                          00002910
           02 MLIBERRC  PIC X.                                          00002920
           02 MLIBERRP  PIC X.                                          00002930
           02 MLIBERRH  PIC X.                                          00002940
           02 MLIBERRV  PIC X.                                          00002950
           02 MLIBERRO  PIC X(58).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MCODTRAA  PIC X.                                          00002980
           02 MCODTRAC  PIC X.                                          00002990
           02 MCODTRAP  PIC X.                                          00003000
           02 MCODTRAH  PIC X.                                          00003010
           02 MCODTRAV  PIC X.                                          00003020
           02 MCODTRAO  PIC X(4).                                       00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCICSA    PIC X.                                          00003050
           02 MCICSC    PIC X.                                          00003060
           02 MCICSP    PIC X.                                          00003070
           02 MCICSH    PIC X.                                          00003080
           02 MCICSV    PIC X.                                          00003090
           02 MCICSO    PIC X(5).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MNETNAMA  PIC X.                                          00003120
           02 MNETNAMC  PIC X.                                          00003130
           02 MNETNAMP  PIC X.                                          00003140
           02 MNETNAMH  PIC X.                                          00003150
           02 MNETNAMV  PIC X.                                          00003160
           02 MNETNAMO  PIC X(8).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MSCREENA  PIC X.                                          00003190
           02 MSCREENC  PIC X.                                          00003200
           02 MSCREENP  PIC X.                                          00003210
           02 MSCREENH  PIC X.                                          00003220
           02 MSCREENV  PIC X.                                          00003230
           02 MSCREENO  PIC X(4).                                       00003240
                                                                                
