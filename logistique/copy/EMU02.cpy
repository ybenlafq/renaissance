      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU02   EMU02                                              00000020
      ***************************************************************** 00000030
       01   EMU02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTSOCL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNENTSOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTSOCF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNENTSOCI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTLIEUL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNENTLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTLIEUF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNENTLIEUI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELARTL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCSELARTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSELARTF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCSELARTI      PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGSOCL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNMAGSOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMAGSOCF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNMAGSOCI      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGLIEUL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNMAGLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNMAGLIEUF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNMAGLIEUI     PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLIEUL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLIBLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBLIEUF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIBLIEUI      PIC X(20).                                 00000410
           02 MMUTATIONI OCCURS   13 TIMES .                            00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MNMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMUTF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNMUTI  PIC X(7).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEOL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MDATEOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEOF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MDATEOI      PIC X(4).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEFL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MDATEFL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEFF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MDATEFI      PIC X(4).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBJDL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLIBJDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIBJDF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLIBJDI      PIC X(2).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEDL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDATEDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEDF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDATEDI      PIC X(4).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATECL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDATECL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATECF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDATECI      PIC X(4).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHDATECL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MHDATECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MHDATECF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MHDATECI     PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEML      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDATEML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEMF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDATEMI      PIC X(4).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDHHMUTL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDHHMUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDHHMUTF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDHHMUTI     PIC X(2).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMMMUTL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MDMMMUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDMMMUTF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MDMMMUTI     PIC X(2).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPROL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MCPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCPROF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCPROI  PIC X.                                          00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTASM3L   COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQUOTASM3L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQUOTASM3F   PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQUOTASM3I   PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTASPROL  COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQUOTASPROL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQUOTASPROF  PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQUOTASPROI  PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDHEURLIML   COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MDHEURLIML COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDHEURLIMF   PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MDHEURLIMI   PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MZONCMDI  PIC X(15).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLIBERRI  PIC X(58).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCICSI    PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MNETNAMI  PIC X(8).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MSCREENI  PIC X(4).                                       00001220
      ***************************************************************** 00001230
      * SDF: EMU02   EMU02                                              00001240
      ***************************************************************** 00001250
       01   EMU02O REDEFINES EMU02I.                                    00001260
           02 FILLER    PIC X(12).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTIMJOUA  PIC X.                                          00001360
           02 MTIMJOUC  PIC X.                                          00001370
           02 MTIMJOUP  PIC X.                                          00001380
           02 MTIMJOUH  PIC X.                                          00001390
           02 MTIMJOUV  PIC X.                                          00001400
           02 MTIMJOUO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNPAGEA   PIC X.                                          00001430
           02 MNPAGEC   PIC X.                                          00001440
           02 MNPAGEP   PIC X.                                          00001450
           02 MNPAGEH   PIC X.                                          00001460
           02 MNPAGEV   PIC X.                                          00001470
           02 MNPAGEO   PIC X(2).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNENTSOCA      PIC X.                                     00001500
           02 MNENTSOCC PIC X.                                          00001510
           02 MNENTSOCP PIC X.                                          00001520
           02 MNENTSOCH PIC X.                                          00001530
           02 MNENTSOCV PIC X.                                          00001540
           02 MNENTSOCO      PIC X(3).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNENTLIEUA     PIC X.                                     00001570
           02 MNENTLIEUC     PIC X.                                     00001580
           02 MNENTLIEUP     PIC X.                                     00001590
           02 MNENTLIEUH     PIC X.                                     00001600
           02 MNENTLIEUV     PIC X.                                     00001610
           02 MNENTLIEUO     PIC X(3).                                  00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MCSELARTA      PIC X.                                     00001640
           02 MCSELARTC PIC X.                                          00001650
           02 MCSELARTP PIC X.                                          00001660
           02 MCSELARTH PIC X.                                          00001670
           02 MCSELARTV PIC X.                                          00001680
           02 MCSELARTO      PIC X(5).                                  00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNMAGSOCA      PIC X.                                     00001710
           02 MNMAGSOCC PIC X.                                          00001720
           02 MNMAGSOCP PIC X.                                          00001730
           02 MNMAGSOCH PIC X.                                          00001740
           02 MNMAGSOCV PIC X.                                          00001750
           02 MNMAGSOCO      PIC X(3).                                  00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNMAGLIEUA     PIC X.                                     00001780
           02 MNMAGLIEUC     PIC X.                                     00001790
           02 MNMAGLIEUP     PIC X.                                     00001800
           02 MNMAGLIEUH     PIC X.                                     00001810
           02 MNMAGLIEUV     PIC X.                                     00001820
           02 MNMAGLIEUO     PIC X(3).                                  00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLIBLIEUA      PIC X.                                     00001850
           02 MLIBLIEUC PIC X.                                          00001860
           02 MLIBLIEUP PIC X.                                          00001870
           02 MLIBLIEUH PIC X.                                          00001880
           02 MLIBLIEUV PIC X.                                          00001890
           02 MLIBLIEUO      PIC X(20).                                 00001900
           02 MMUTATIONO OCCURS   13 TIMES .                            00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MNMUTA  PIC X.                                          00001930
             03 MNMUTC  PIC X.                                          00001940
             03 MNMUTP  PIC X.                                          00001950
             03 MNMUTH  PIC X.                                          00001960
             03 MNMUTV  PIC X.                                          00001970
             03 MNMUTO  PIC X(7).                                       00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MDATEOA      PIC X.                                     00002000
             03 MDATEOC PIC X.                                          00002010
             03 MDATEOP PIC X.                                          00002020
             03 MDATEOH PIC X.                                          00002030
             03 MDATEOV PIC X.                                          00002040
             03 MDATEOO      PIC X(4).                                  00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MDATEFA      PIC X.                                     00002070
             03 MDATEFC PIC X.                                          00002080
             03 MDATEFP PIC X.                                          00002090
             03 MDATEFH PIC X.                                          00002100
             03 MDATEFV PIC X.                                          00002110
             03 MDATEFO      PIC X(4).                                  00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MLIBJDA      PIC X.                                     00002140
             03 MLIBJDC PIC X.                                          00002150
             03 MLIBJDP PIC X.                                          00002160
             03 MLIBJDH PIC X.                                          00002170
             03 MLIBJDV PIC X.                                          00002180
             03 MLIBJDO      PIC X(2).                                  00002190
             03 FILLER       PIC X(2).                                  00002200
             03 MDATEDA      PIC X.                                     00002210
             03 MDATEDC PIC X.                                          00002220
             03 MDATEDP PIC X.                                          00002230
             03 MDATEDH PIC X.                                          00002240
             03 MDATEDV PIC X.                                          00002250
             03 MDATEDO      PIC X(4).                                  00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MDATECA      PIC X.                                     00002280
             03 MDATECC PIC X.                                          00002290
             03 MDATECP PIC X.                                          00002300
             03 MDATECH PIC X.                                          00002310
             03 MDATECV PIC X.                                          00002320
             03 MDATECO      PIC X(4).                                  00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MHDATECA     PIC X.                                     00002350
             03 MHDATECC     PIC X.                                     00002360
             03 MHDATECP     PIC X.                                     00002370
             03 MHDATECH     PIC X.                                     00002380
             03 MHDATECV     PIC X.                                     00002390
             03 MHDATECO     PIC X(5).                                  00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MDATEMA      PIC X.                                     00002420
             03 MDATEMC PIC X.                                          00002430
             03 MDATEMP PIC X.                                          00002440
             03 MDATEMH PIC X.                                          00002450
             03 MDATEMV PIC X.                                          00002460
             03 MDATEMO      PIC X(4).                                  00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MDHHMUTA     PIC X.                                     00002490
             03 MDHHMUTC     PIC X.                                     00002500
             03 MDHHMUTP     PIC X.                                     00002510
             03 MDHHMUTH     PIC X.                                     00002520
             03 MDHHMUTV     PIC X.                                     00002530
             03 MDHHMUTO     PIC X(2).                                  00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MDMMMUTA     PIC X.                                     00002560
             03 MDMMMUTC     PIC X.                                     00002570
             03 MDMMMUTP     PIC X.                                     00002580
             03 MDMMMUTH     PIC X.                                     00002590
             03 MDMMMUTV     PIC X.                                     00002600
             03 MDMMMUTO     PIC X(2).                                  00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MCPROA  PIC X.                                          00002630
             03 MCPROC  PIC X.                                          00002640
             03 MCPROP  PIC X.                                          00002650
             03 MCPROH  PIC X.                                          00002660
             03 MCPROV  PIC X.                                          00002670
             03 MCPROO  PIC X.                                          00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MQUOTASM3A   PIC X.                                     00002700
             03 MQUOTASM3C   PIC X.                                     00002710
             03 MQUOTASM3P   PIC X.                                     00002720
             03 MQUOTASM3H   PIC X.                                     00002730
             03 MQUOTASM3V   PIC X.                                     00002740
             03 MQUOTASM3O   PIC X(5).                                  00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MQUOTASPROA  PIC X.                                     00002770
             03 MQUOTASPROC  PIC X.                                     00002780
             03 MQUOTASPROP  PIC X.                                     00002790
             03 MQUOTASPROH  PIC X.                                     00002800
             03 MQUOTASPROV  PIC X.                                     00002810
             03 MQUOTASPROO  PIC X(5).                                  00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MDHEURLIMA   PIC X.                                     00002840
             03 MDHEURLIMC   PIC X.                                     00002850
             03 MDHEURLIMP   PIC X.                                     00002860
             03 MDHEURLIMH   PIC X.                                     00002870
             03 MDHEURLIMV   PIC X.                                     00002880
             03 MDHEURLIMO   PIC X(5).                                  00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MZONCMDA  PIC X.                                          00002910
           02 MZONCMDC  PIC X.                                          00002920
           02 MZONCMDP  PIC X.                                          00002930
           02 MZONCMDH  PIC X.                                          00002940
           02 MZONCMDV  PIC X.                                          00002950
           02 MZONCMDO  PIC X(15).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLIBERRA  PIC X.                                          00002980
           02 MLIBERRC  PIC X.                                          00002990
           02 MLIBERRP  PIC X.                                          00003000
           02 MLIBERRH  PIC X.                                          00003010
           02 MLIBERRV  PIC X.                                          00003020
           02 MLIBERRO  PIC X(58).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSO    PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNETNAMA  PIC X.                                          00003190
           02 MNETNAMC  PIC X.                                          00003200
           02 MNETNAMP  PIC X.                                          00003210
           02 MNETNAMH  PIC X.                                          00003220
           02 MNETNAMV  PIC X.                                          00003230
           02 MNETNAMO  PIC X(8).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSCREENA  PIC X.                                          00003260
           02 MSCREENC  PIC X.                                          00003270
           02 MSCREENP  PIC X.                                          00003280
           02 MSCREENH  PIC X.                                          00003290
           02 MSCREENV  PIC X.                                          00003300
           02 MSCREENO  PIC X(4).                                       00003310
                                                                                
