      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JRM651 AU 09/05/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,20,BI,A,                          *        
      *                           40,03,PD,A,                          *        
      *                           43,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JRM651.                                                        
            05 NOMETAT-JRM651           PIC X(6) VALUE 'JRM651'.                
            05 RUPTURES-JRM651.                                                 
           10 JRM651-CRMGROUP           PIC X(05).                      007  005
           10 JRM651-NLIEU              PIC X(03).                      012  003
           10 JRM651-CFAM               PIC X(05).                      015  005
           10 JRM651-LAGREGATED         PIC X(20).                      020  020
           10 JRM651-QRANG              PIC S9(05)      COMP-3.         040  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JRM651-SEQUENCE           PIC S9(04) COMP.                043  002
      *--                                                                       
           10 JRM651-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JRM651.                                                   
           10 JRM651-CEXPO              PIC X(05).                      045  005
           10 JRM651-CMARQ              PIC X(05).                      050  005
           10 JRM651-FORCE              PIC X(01).                      055  001
           10 JRM651-LFAM               PIC X(20).                      056  020
           10 JRM651-LLIEU              PIC X(20).                      076  020
           10 JRM651-LREFFOURN          PIC X(20).                      096  020
           10 JRM651-LSTATCOMP          PIC X(03).                      116  003
           10 JRM651-NCODIC             PIC X(07).                      119  007
           10 JRM651-NSOCIETE           PIC X(03).                      126  003
           10 JRM651-WE9                PIC X(01).                      129  001
           10 JRM651-WRM80              PIC X(01).                      130  001
           10 JRM651-W8020-RUP          PIC X(01).                      131  001
           10 JRM651-PRIX-VENTE         PIC S9(07)V9(2) COMP-3.         132  005
           10 JRM651-QEXPO              PIC S9(05)      COMP-3.         137  003
           10 JRM651-QLS                PIC S9(05)      COMP-3.         140  003
           10 JRM651-QMUTATT            PIC S9(05)      COMP-3.         143  003
           10 JRM651-QPV                PIC S9(07)      COMP-3.         146  004
           10 JRM651-QPV-SIGNE          PIC S9(07)      COMP-3.         150  004
           10 JRM651-QSA                PIC S9(07)      COMP-3.         154  004
           10 JRM651-QSASIMU            PIC S9(07)      COMP-3.         158  004
           10 JRM651-QSO                PIC S9(07)      COMP-3.         162  004
           10 JRM651-QSOSIMU            PIC S9(07)      COMP-3.         166  004
           10 JRM651-QSTOCKMAG          PIC S9(05)      COMP-3.         170  003
           10 JRM651-S-1                PIC S9(05)      COMP-3.         173  003
           10 JRM651-S-2                PIC S9(05)      COMP-3.         176  003
           10 JRM651-S-3                PIC S9(05)      COMP-3.         179  003
           10 JRM651-S-4                PIC S9(05)      COMP-3.         182  003
            05 FILLER                      PIC X(328).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JRM651-LONG           PIC S9(4)   COMP  VALUE +184.           
      *                                                                         
      *--                                                                       
        01  DSECT-JRM651-LONG           PIC S9(4) COMP-5  VALUE +184.           
                                                                                
      *}                                                                        
