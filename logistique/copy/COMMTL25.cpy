      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
            03 COMM-TL25-DATA REDEFINES COMM-TL00-FILLER.               03700000
               05 COMM-TL25-WPAGIN.                                     03710000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           07 COMM-TL25-NPAGE     PIC S9(4) COMP.                03720000
      *--                                                                       
                  07 COMM-TL25-NPAGE     PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           07 COMM-TL25-NPAGE-MAX PIC S9(4) COMP.                03730000
      *--                                                                       
                  07 COMM-TL25-NPAGE-MAX PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           07 COMM-TL26-ITEM-MAX  PIC S9(4) COMP.                03740000
      *--                                                                       
                  07 COMM-TL26-ITEM-MAX  PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           07 COMM-TLHS-ITEM-MAX  PIC S9(4) COMP.                03750000
      *--                                                                       
                  07 COMM-TLHS-ITEM-MAX  PIC S9(4) COMP-5.                      
      *}                                                                        
               05 COMM-TL25-TAB99 OCCURS 50.                            03760000
                  07 COMM-TL25-CMAJ OCCURS 10 PIC X(01).                03770000
      * CMAJ = 'M' <=> LIGNE EXISTANTE EN BASE                          03780000
      * CMAJ = ' ' <=> LIGNE NON EXISTANTE EN BASE                      03790000
               05 COMM-TL25-TRAITEMENT PIC X.                           03800000
      * TRAITEMENT  = ' ' <=> TRAITEMENT HS PAS EFFECTUE       => PF6   03810000
      *                                                        => PF5.  03820000
      * TRAITEMENT  = '1' <=> TRAITEMENT HS DEJA EFFECTUE      => RE-PF503830000
      *                       POUR EFFECTUER TOUTES LES MAJ.            03840000
      * TRAITEMENT  = '2' <=> AUCUNES MAJ N'ONT ETE EFFECTUEES => PF3.  03850000
      * TRAITEMENT  = '3' <=> TOUTES LES MAJ ONT ETE EFFECTUE  => PF3.  03860000
               05 COMM-TL25-POSITION.                                   03870000
                  07 COMM-TL25-FIN-ACCES PIC X.                         03880000
      * FIN-ACCES = '1' <=> PLUS DE LIGNE DB2 A AFFICHER                03890000
                  07 COMM-TL25-NORDRE    PIC X(2).                      03900000
               05 COMM-TL25-CLIVR1       PIC X(10).                     03910000
               05 COMM-TL25-LLIVR1       PIC X(20).                     03920000
               05 COMM-TL25-CLIVR2       PIC X(10).                     03930000
               05 COMM-TL25-LLIVR2       PIC X(20).                     03940000
               05 COMM-TL25-CLIVR3       PIC X(10).                     03950000
               05 COMM-TL25-LLIVR3       PIC X(20).                     03960000
               05 COMM-TL26-XCTL.                                       03970000
                  07 COMM-TL26-NMAG         PIC X(3).                   03980000
                  07 COMM-TL26-NVENTE       PIC X(7).                   03990000
                  07 COMM-TL26-CADRTOUR     PIC X.                      04000000
               05 COMM-TL25-RET-ANTICIP     PIC 9(1).                   04010000
               05 COMM-TL25-LIBRE           PIC X(6).                   04020000
      *                                                                 04030000
      *--- AJOUTE EN 01/92 POUR CONTROLE PAR MODE DE REGLEMENT          04040000
      *--- (SELON CODES DEFINIS DANS LA TABLE COPAI DE GA01)            04050000
      *                                                                 04060000
               05 COMM-TL25-SOLDE           PIC X.                      04070000
               05 COMM-TL25-CODES-CUMUL.                                04080000
                  07 COMM-TL25-CUMCOD1      PIC X(3).                   04090000
                  07 COMM-TL25-CUMCOD2      PIC X(3).                   04100000
                  07 COMM-TL25-CUMCOD3      PIC X(3).                   04110000
               05 FILLER REDEFINES COMM-TL25-CODES-CUMUL.               04120000
                  07 COMM-TL25-CUMCOD       PIC X(3) OCCURS 3.          04130000
                                                                        04140000
               05 COMM-TL25-MONTANTS-CUMUL.                             04150000
                  07 COMM-TL25-CUMMNTA0.                                04160000
                     09 COMM-TL25-CUMMNT0      PIC 9(6)V99.             04170000
                  07 COMM-TL25-CUMMNTA1.                                04171000
                     09 COMM-TL25-CUMMNT1      PIC 9(6)V99.             04172000
                  07 COMM-TL25-CUMMNTA2.                                04180000
                     09 COMM-TL25-CUMMNT2      PIC 9(6)V99.             04190000
                  07 COMM-TL25-CUMMNTA3.                                04200000
                     09 COMM-TL25-CUMMNT3      PIC 9(6)V99.             04210000
                  07 COMM-TL25-CUMMNTA4.                                04220000
                     09 COMM-TL25-CUMMNT4      PIC 9(6)V99.             04230000
               05 FILLER REDEFINES COMM-TL25-MONTANTS-CUMUL.            04240000
                  07 COMM-TL25-CUMMNT       PIC 9(6)V99 OCCURS 5.       04250000
                                                                        04260000
               05 COMM-TL25-MODPAI.                                     04270000
                  07 COMM-TL25-PMODPAI OCCURS 10.                       04270100
                     09 COMM-TL25-CODPAI PIC X(3).                      04270200
                     09 COMM-TL25-MOPAI.                                04270300
                        10 COMM-TL25-CODDEV PIC X(3).                   04270310
                        10 COMM-TL25-CECART PIC X(1).                   04270400
                                                                        04260000
               05 COMM-TL25-TAB-MAG.                                    04270000
                  07 COMM-TL25-TMAG OCCURS 100.                         04270100
                     09 COMM-TL25-NSOCIETE    PIC X(3).                 04270200
                     09 COMM-TL25-NLIEUDEST   PIC X(3).                 04270300
                     09 COMM-TL25-FLAG-MAG    PIC X(1).                 04270300
               05 COMM-TL25-NB-POS            PIC 9(3).                 04270000
                                                                        04260000
               05 COMM-TL25-FILLER       PIC X(2057).                   04271000
                                                                                
