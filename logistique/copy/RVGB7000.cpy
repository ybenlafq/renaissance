      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGB7000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB7000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB7000.                                                            
           02  GB70-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GB70-NCODIC                                                      
               PIC X(0007).                                                     
           02  GB70-CTYPCDE                                                     
               PIC X(0001).                                                     
           02  GB70-QDEMANDEE                                                   
               PIC S9(5) COMP-3.                                                
           02  GB70-QLIEE                                                       
               PIC S9(5) COMP-3.                                                
           02  GB70-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GB70-DMUTATION                                                   
               PIC X(0008).                                                     
           02  GB70-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  GB70-CMARQ                                                       
               PIC X(0005).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGB7000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB7000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB70-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB70-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB70-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB70-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB70-CTYPCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB70-CTYPCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB70-QDEMANDEE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB70-QDEMANDEE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB70-QLIEE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB70-QLIEE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB70-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB70-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB70-DMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB70-DMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB70-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB70-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB70-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB70-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
