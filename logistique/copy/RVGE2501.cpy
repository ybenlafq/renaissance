      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGE2501                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGE2501                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGE2501.                                                            
           02  GE25-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GE25-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GE25-CAIRE                                                       
               PIC X(0002).                                                     
           02  GE25-CCOTE                                                       
               PIC X(0002).                                                     
           02  GE25-NPOSITION                                                   
               PIC X(0003).                                                     
           02  GE25-CZONEACCES                                                  
               PIC X(0001).                                                     
           02  GE25-QFONDEMP                                                    
               PIC S9(3) COMP-3.                                                
           02  GE25-NCODIC                                                      
               PIC X(0007).                                                     
           02  GE25-QSTOCK                                                      
               PIC S9(5) COMP-3.                                                
           02  GE25-QTAUXOCC                                                    
               PIC S9(3) COMP-3.                                                
           02  GE25-QCAPACITE                                                   
               PIC S9(5) COMP-3.                                                
           02  GE25-DATTR                                                       
               PIC X(0008).                                                     
           02  GE25-QSTOCKRES                                                   
               PIC S9(5) COMP-3.                                                
           02  GE25-NORDRE                                                      
               PIC S9(7) COMP-3.                                                
           02  GE25-NRUPT                                                       
               PIC S9(5) COMP-3.                                                
           02  GE25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GE25-CLASSE                                                      
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGE2501                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGE2501-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-CAIRE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-CAIRE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-CCOTE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-CCOTE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-NPOSITION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-NPOSITION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-CZONEACCES-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-CZONEACCES-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-QFONDEMP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-QFONDEMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-QSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-QSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-QTAUXOCC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-QTAUXOCC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-QCAPACITE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-QCAPACITE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-DATTR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-DATTR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-QSTOCKRES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-QSTOCKRES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-NRUPT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-NRUPT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE25-CLASSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE25-CLASSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
