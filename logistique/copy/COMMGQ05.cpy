      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGQ05 (TGQ00 -> MENU)    TR: GQ00  *    00002237
      *   GESTION DES PARAMETRES CAPACITES VEHICULES LIVRAISONS    *    00002337
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *  TRANSACTION GQ05 : GESTION DES PARAMETRES CAPACITES VEHI. *    00411037
      *                                                                 00412000
          02 COMM-GQ05-APPLI REDEFINES COMM-GQ00-APPLI.                 00420037
      *                                                                 00430036
      *------------------------------ CODE CAPACITE                     00550037
             03 COMM-GQ05-CCAPVEH        PIC X(5).                      00560037
      *------------------------------ ZONE LIBRE                        01710035
             03 COMM-GQ05-LIBRE          PIC X(3719).                   01720037
      ***************************************************************** 02170035
                                                                                
