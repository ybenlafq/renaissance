      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS76   EGS76                                              00000020
      ***************************************************************** 00000030
       01   EGS76I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEMXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNPAGEMXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNPAGEMXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEMXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPERL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPERF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCOPERI   PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPERL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOPERF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLOPERI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCORIL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNSOCORIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCORIF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNSOCORII      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUORL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNLIEUORL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUORF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNLIEUORI      PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSSLORIL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNSSLORIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSSLORIF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNSSLORII      PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDESL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNSOCDESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDESF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNSOCDESI      PIC X(3).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUDEL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNLIEUDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUDEF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNLIEUDEI      PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSSLDESL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNSSLDESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSSLDESF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNSSLDESI      PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLSOCOL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNLSOCOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLSOCOF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNLSOCOI  PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLLIEUOL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNLLIEUOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLLIEUOF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNLLIEUOI      PIC X(3).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLSOCDL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNLSOCDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLSOCDF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNLSOCDI  PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLLIEUDL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNLLIEUDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLLIEUDF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNLLIEUDI      PIC X(3).                                  00000690
           02 M6I OCCURS   13 TIMES .                                   00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSELI   PIC X.                                          00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCOL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNSOCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSOCOF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNSOCOI      PIC X(3).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUOL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MNLIEUOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIEUOF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNLIEUOI     PIC X(3).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCDL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MNSOCDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSOCDF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNSOCDI      PIC X(3).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUDL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MNLIEUDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIEUDF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MNLIEUDI     PIC X(3).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLIBERRI  PIC X(80).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCICSI    PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MNETNAMI  PIC X(8).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MSCREENI  PIC X(4).                                       00001100
      ***************************************************************** 00001110
      * SDF: EGS76   EGS76                                              00001120
      ***************************************************************** 00001130
       01   EGS76O REDEFINES EGS76I.                                    00001140
           02 FILLER    PIC X(12).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDATJOUA  PIC X.                                          00001170
           02 MDATJOUC  PIC X.                                          00001180
           02 MDATJOUP  PIC X.                                          00001190
           02 MDATJOUH  PIC X.                                          00001200
           02 MDATJOUV  PIC X.                                          00001210
           02 MDATJOUO  PIC X(10).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MNPAGEA   PIC X.                                          00001310
           02 MNPAGEC   PIC X.                                          00001320
           02 MNPAGEP   PIC X.                                          00001330
           02 MNPAGEH   PIC X.                                          00001340
           02 MNPAGEV   PIC X.                                          00001350
           02 MNPAGEO   PIC X(3).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MNPAGEMXA      PIC X.                                     00001380
           02 MNPAGEMXC PIC X.                                          00001390
           02 MNPAGEMXP PIC X.                                          00001400
           02 MNPAGEMXH PIC X.                                          00001410
           02 MNPAGEMXV PIC X.                                          00001420
           02 MNPAGEMXO      PIC X(3).                                  00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MCOPERA   PIC X.                                          00001450
           02 MCOPERC   PIC X.                                          00001460
           02 MCOPERP   PIC X.                                          00001470
           02 MCOPERH   PIC X.                                          00001480
           02 MCOPERV   PIC X.                                          00001490
           02 MCOPERO   PIC X(10).                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MLOPERA   PIC X.                                          00001520
           02 MLOPERC   PIC X.                                          00001530
           02 MLOPERP   PIC X.                                          00001540
           02 MLOPERH   PIC X.                                          00001550
           02 MLOPERV   PIC X.                                          00001560
           02 MLOPERO   PIC X(20).                                      00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNSOCORIA      PIC X.                                     00001590
           02 MNSOCORIC PIC X.                                          00001600
           02 MNSOCORIP PIC X.                                          00001610
           02 MNSOCORIH PIC X.                                          00001620
           02 MNSOCORIV PIC X.                                          00001630
           02 MNSOCORIO      PIC X(3).                                  00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MNLIEUORA      PIC X.                                     00001660
           02 MNLIEUORC PIC X.                                          00001670
           02 MNLIEUORP PIC X.                                          00001680
           02 MNLIEUORH PIC X.                                          00001690
           02 MNLIEUORV PIC X.                                          00001700
           02 MNLIEUORO      PIC X(3).                                  00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MNSSLORIA      PIC X.                                     00001730
           02 MNSSLORIC PIC X.                                          00001740
           02 MNSSLORIP PIC X.                                          00001750
           02 MNSSLORIH PIC X.                                          00001760
           02 MNSSLORIV PIC X.                                          00001770
           02 MNSSLORIO      PIC X(3).                                  00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MNSOCDESA      PIC X.                                     00001800
           02 MNSOCDESC PIC X.                                          00001810
           02 MNSOCDESP PIC X.                                          00001820
           02 MNSOCDESH PIC X.                                          00001830
           02 MNSOCDESV PIC X.                                          00001840
           02 MNSOCDESO      PIC X(3).                                  00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNLIEUDEA      PIC X.                                     00001870
           02 MNLIEUDEC PIC X.                                          00001880
           02 MNLIEUDEP PIC X.                                          00001890
           02 MNLIEUDEH PIC X.                                          00001900
           02 MNLIEUDEV PIC X.                                          00001910
           02 MNLIEUDEO      PIC X(3).                                  00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MNSSLDESA      PIC X.                                     00001940
           02 MNSSLDESC PIC X.                                          00001950
           02 MNSSLDESP PIC X.                                          00001960
           02 MNSSLDESH PIC X.                                          00001970
           02 MNSSLDESV PIC X.                                          00001980
           02 MNSSLDESO      PIC X(3).                                  00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MNLSOCOA  PIC X.                                          00002010
           02 MNLSOCOC  PIC X.                                          00002020
           02 MNLSOCOP  PIC X.                                          00002030
           02 MNLSOCOH  PIC X.                                          00002040
           02 MNLSOCOV  PIC X.                                          00002050
           02 MNLSOCOO  PIC X(3).                                       00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MNLLIEUOA      PIC X.                                     00002080
           02 MNLLIEUOC PIC X.                                          00002090
           02 MNLLIEUOP PIC X.                                          00002100
           02 MNLLIEUOH PIC X.                                          00002110
           02 MNLLIEUOV PIC X.                                          00002120
           02 MNLLIEUOO      PIC X(3).                                  00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MNLSOCDA  PIC X.                                          00002150
           02 MNLSOCDC  PIC X.                                          00002160
           02 MNLSOCDP  PIC X.                                          00002170
           02 MNLSOCDH  PIC X.                                          00002180
           02 MNLSOCDV  PIC X.                                          00002190
           02 MNLSOCDO  PIC X(3).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MNLLIEUDA      PIC X.                                     00002220
           02 MNLLIEUDC PIC X.                                          00002230
           02 MNLLIEUDP PIC X.                                          00002240
           02 MNLLIEUDH PIC X.                                          00002250
           02 MNLLIEUDV PIC X.                                          00002260
           02 MNLLIEUDO      PIC X(3).                                  00002270
           02 M6O OCCURS   13 TIMES .                                   00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MSELA   PIC X.                                          00002300
             03 MSELC   PIC X.                                          00002310
             03 MSELP   PIC X.                                          00002320
             03 MSELH   PIC X.                                          00002330
             03 MSELV   PIC X.                                          00002340
             03 MSELO   PIC X.                                          00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MNSOCOA      PIC X.                                     00002370
             03 MNSOCOC PIC X.                                          00002380
             03 MNSOCOP PIC X.                                          00002390
             03 MNSOCOH PIC X.                                          00002400
             03 MNSOCOV PIC X.                                          00002410
             03 MNSOCOO      PIC X(3).                                  00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MNLIEUOA     PIC X.                                     00002440
             03 MNLIEUOC     PIC X.                                     00002450
             03 MNLIEUOP     PIC X.                                     00002460
             03 MNLIEUOH     PIC X.                                     00002470
             03 MNLIEUOV     PIC X.                                     00002480
             03 MNLIEUOO     PIC X(3).                                  00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MNSOCDA      PIC X.                                     00002510
             03 MNSOCDC PIC X.                                          00002520
             03 MNSOCDP PIC X.                                          00002530
             03 MNSOCDH PIC X.                                          00002540
             03 MNSOCDV PIC X.                                          00002550
             03 MNSOCDO      PIC X(3).                                  00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MNLIEUDA     PIC X.                                     00002580
             03 MNLIEUDC     PIC X.                                     00002590
             03 MNLIEUDP     PIC X.                                     00002600
             03 MNLIEUDH     PIC X.                                     00002610
             03 MNLIEUDV     PIC X.                                     00002620
             03 MNLIEUDO     PIC X(3).                                  00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MLIBERRA  PIC X.                                          00002650
           02 MLIBERRC  PIC X.                                          00002660
           02 MLIBERRP  PIC X.                                          00002670
           02 MLIBERRH  PIC X.                                          00002680
           02 MLIBERRV  PIC X.                                          00002690
           02 MLIBERRO  PIC X(80).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCODTRAA  PIC X.                                          00002720
           02 MCODTRAC  PIC X.                                          00002730
           02 MCODTRAP  PIC X.                                          00002740
           02 MCODTRAH  PIC X.                                          00002750
           02 MCODTRAV  PIC X.                                          00002760
           02 MCODTRAO  PIC X(4).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCICSA    PIC X.                                          00002790
           02 MCICSC    PIC X.                                          00002800
           02 MCICSP    PIC X.                                          00002810
           02 MCICSH    PIC X.                                          00002820
           02 MCICSV    PIC X.                                          00002830
           02 MCICSO    PIC X(5).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MNETNAMA  PIC X.                                          00002860
           02 MNETNAMC  PIC X.                                          00002870
           02 MNETNAMP  PIC X.                                          00002880
           02 MNETNAMH  PIC X.                                          00002890
           02 MNETNAMV  PIC X.                                          00002900
           02 MNETNAMO  PIC X(8).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MSCREENA  PIC X.                                          00002930
           02 MSCREENC  PIC X.                                          00002940
           02 MSCREENP  PIC X.                                          00002950
           02 MSCREENH  PIC X.                                          00002960
           02 MSCREENV  PIC X.                                          00002970
           02 MSCREENO  PIC X(4).                                       00002980
                                                                                
