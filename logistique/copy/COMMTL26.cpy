      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
               05 COMM-TL26-DATA REDEFINES COMM-TL25-FILLER.            00001790
                  07 COMM-TL26-CRETG-INIT      PIC X(5).                00001800
                  07 COMM-TL26-CRETG-FINAL     PIC X(5).                00001800
                  07 COMM-TL26-LRETG           PIC X(20).               00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           07 COMM-TL26-NPAGE           PIC S9(4) COMP.          00001820
      *--                                                                       
                  07 COMM-TL26-NPAGE           PIC S9(4) COMP-5.                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           07 COMM-TL26-NPAGE-MAX       PIC S9(4) COMP.          00001830
      *--                                                                       
                  07 COMM-TL26-NPAGE-MAX       PIC S9(4) COMP-5.                
      *}                                                                        
      ******> LE NOMBRE MAXIMUM DE LIGNES AFFICHABLES EST DE 45 LIGNES  00001840
      ******> <=> 3 PAGES MAXIMUM .                                     00001850
                  07 COMM-TL26-ECRAN  OCCURS 3.                         00001860
                     09 COMM-TL26-LIGNE  OCCURS 13.                     00001870
                        11 COMM-TL26-NCODIC          PIC X(7).          00001880
                        11 COMM-TL26-NCODICGRP       PIC X(7).          00001890
                        11 COMM-TL26-WLIVREP         PIC X(1).          00001900
                        11 COMM-TL26-NSEQ            PIC X(2).          00001910
                        11 COMM-TL26-CADR            PIC X.             00001920
                        11 COMM-TL26-DIFF-QTE        PIC X(01).                 
                        11 COMM-TL26-QCOMM           PIC 9(5).          00001930
                        11 COMM-TL26-QCOMM-X  REDEFINES                 00001940
                               COMM-TL26-QCOMM       PIC X(5).          00001950
                        11 COMM-TL26-QLIVR-INIT      PIC X(5).          00001960
                        11 COMM-TL26-CRET-INIT       PIC X(5).          00001970
                        11 COMM-TL26-QLIVR-FINAL     PIC X(5).          00001960
                        11 COMM-TL26-CRET-FINAL      PIC X(5).          00001970
      ******> COMM-TL26-TS26-RET EST RENSEIGNE QUAND                    00001980
      ******> LA LIGNE EXISTE DANS TS26-DONNEES (LISTE DES RETOURS)     00001990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                 11 COMM-TL26-TS26-RET        PIC S9(4) COMP.    00002000
      *--                                                                       
                        11 COMM-TL26-TS26-RET        PIC S9(4) COMP-5.          
      *}                                                                        
MB    *           07 COMM-TL26-FILLER          PIC X(419).              00002010
MB                07 COMM-TL26-FILLER          PIC X(229).              00002010
                                                                                
