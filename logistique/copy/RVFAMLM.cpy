      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FAMLM ENTREPOT EXTERIEUR - FAMILLE     *        
      *----------------------------------------------------------------*        
       01  RVFAMLM .                                                            
           05  FAMLM-CTABLEG2    PIC X(15).                                     
           05  FAMLM-CTABLEG2-REDEF REDEFINES FAMLM-CTABLEG2.                   
               10  FAMLM-CFAM            PIC X(05).                             
               10  FAMLM-NSOCDEP         PIC X(06).                             
               10  FAMLM-CREINIT         PIC X(01).                             
           05  FAMLM-WTABLEG     PIC X(80).                                     
           05  FAMLM-WTABLEG-REDEF  REDEFINES FAMLM-WTABLEG.                    
               10  FAMLM-WACTIF          PIC X(01).                             
               10  FAMLM-DREINIT         PIC X(08).                             
               10  FAMLM-CCTRL           PIC X(05).                             
               10  FAMLM-CCTRL-N        REDEFINES FAMLM-CCTRL                   
                                         PIC 9(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVFAMLM-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FAMLM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FAMLM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FAMLM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FAMLM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
