      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:28 >
      
      *----------------------------------------------------------------*
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JTL163 AU 03/06/1999  *
      *                                                                *
      *          CRITERES DE TRI  07,05,BI,A,                          *
      *                           12,02,BI,A,                          *
      *                           14,02,BI,A,                          *
      *                                                                *
      *----------------------------------------------------------------*
       01  DSECT-JTL163.
            05 NOMETAT-JTL163           PIC X(6) VALUE 'JTL163'.
            05 RUPTURES-JTL163.
           10 JTL163-CPROTOUR           PIC X(05).
           10 JTL163-NSATELLITE         PIC X(02).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    10 JTL163-SEQUENCE           PIC S9(04) COMP.
      *--
           10 JTL163-SEQUENCE           PIC S9(04) COMP-5.
      *}
            05 CHAMPS-JTL163.
           10 JTL163-CANN               PIC X(01).
           10 JTL163-CARA               PIC X(01).
           10 JTL163-CARC               PIC X(01).
           10 JTL163-CARF               PIC X(01).
           10 JTL163-CARM               PIC X(01).
           10 JTL163-CCRE               PIC X(01).
           10 JTL163-CMODDELIN          PIC X(03).
           10 JTL163-CMODDELIN2         PIC X(03).
           10 JTL163-CPROTOURIN         PIC X(05).
           10 JTL163-HEUREIN            PIC X(05).
           10 JTL163-LNOM               PIC X(20).
           10 JTL163-NCASE              PIC X(03).
           10 JTL163-NDEPOT             PIC X(03).
           10 JTL163-NFA                PIC X(03).
           10 JTL163-NMAG               PIC X(03).
           10 JTL163-NSOCIETE           PIC X(03).
           10 JTL163-NVENTE             PIC X(07).
           10 JTL163-NFOLIO             PIC 9(02)     .
           10 JTL163-DATEIN             PIC X(08).
           10 JTL163-DLIVR              PIC X(08).
            05 FILLER                      PIC X(415).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      * 01  DSECT-JTL163-LONG           PIC S9(4)   COMP  VALUE +097.
      *
      *--
        01  DSECT-JTL163-LONG           PIC S9(4) COMP-5  VALUE +097.
      
      *}
