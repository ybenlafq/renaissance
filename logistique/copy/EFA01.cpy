      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFA01   EFA01                                              00000020
      ***************************************************************** 00000030
       01   EFA01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSATL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSATL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSATF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSATI    PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCASEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCASEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCASEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCASEI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMTAIREL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCMTAIREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMTAIREF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMTAIREI      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLSOCI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIV1L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCLIV1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIV1F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCLIV1I   PIC X(10).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIV1L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLLIV1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIV1F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLLIV1I   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCPROFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPROFF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCPROFI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROFL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLPROFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPROFF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLPROFI   PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIV2L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCLIV2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIV2F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCLIV2I   PIC X(10).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIV2L   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLLIV2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIV2F   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLLIV2I   PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDELIVL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDDELIVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDELIVF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDDELIVI  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTOURNEEL     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MCTOURNEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTOURNEEF     PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCTOURNEEI     PIC X(3).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIV3L   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCLIV3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIV3F   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCLIV3I   PIC X(10).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIV3L   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLLIV3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIV3F   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLLIV3I   PIC X(20).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOGISL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLOGISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLOGISF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLOGISI   PIC X(9).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLOGIL   COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCLOGIL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLOGIF   PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCLOGII   PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTOURNLGL     COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MCTOURNLGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTOURNLGF     PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCTOURNLGI     PIC X(3).                                  00000930
           02 M267I OCCURS   12 TIMES .                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MNMAGI  PIC X(3).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MNVENTEI     PIC X(7).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPEL      COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MCTYPEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCTYPEF      PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MCTYPEI      PIC X.                                     00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPEASSL   COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MCTYPEASSL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPEASSF   PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MCTYPEASSI   PIC X(2).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPLAGEL     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MLPLAGEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLPLAGEF     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MLPLAGEI     PIC X(8).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLNOML  COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MLNOML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLNOMF  PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MLNOMI  PIC X(15).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPTTL  COMP PIC S9(4).                                 00001190
      *--                                                                       
             03 MCPTTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCPTTF  PIC X.                                          00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MCPTTI  PIC X(5).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLOCALITEL   COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MLOCALITEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLOCALITEF   PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MLOCALITEI   PIC X(23).                                 00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRENEAUL    COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MCRENEAUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCRENEAUF    PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MCRENEAUI    PIC X(7).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MZONCMDI  PIC X(15).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MLIBERRI  PIC X(58).                                      00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCODTRAI  PIC X(4).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MCICSI    PIC X(5).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MNETNAMI  PIC X(8).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MSCREENI  PIC X(4).                                       00001540
      ***************************************************************** 00001550
      * SDF: EFA01   EFA01                                              00001560
      ***************************************************************** 00001570
       01   EFA01O REDEFINES EFA01I.                                    00001580
           02 FILLER    PIC X(12).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MDATJOUA  PIC X.                                          00001610
           02 MDATJOUC  PIC X.                                          00001620
           02 MDATJOUP  PIC X.                                          00001630
           02 MDATJOUH  PIC X.                                          00001640
           02 MDATJOUV  PIC X.                                          00001650
           02 MDATJOUO  PIC X(10).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MTIMJOUA  PIC X.                                          00001680
           02 MTIMJOUC  PIC X.                                          00001690
           02 MTIMJOUP  PIC X.                                          00001700
           02 MTIMJOUH  PIC X.                                          00001710
           02 MTIMJOUV  PIC X.                                          00001720
           02 MTIMJOUO  PIC X(5).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MPAGEA    PIC X.                                          00001750
           02 MPAGEC    PIC X.                                          00001760
           02 MPAGEP    PIC X.                                          00001770
           02 MPAGEH    PIC X.                                          00001780
           02 MPAGEV    PIC X.                                          00001790
           02 MPAGEO    PIC X(3).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MWFONCA   PIC X.                                          00001820
           02 MWFONCC   PIC X.                                          00001830
           02 MWFONCP   PIC X.                                          00001840
           02 MWFONCH   PIC X.                                          00001850
           02 MWFONCV   PIC X.                                          00001860
           02 MWFONCO   PIC X(3).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNSATA    PIC X.                                          00001890
           02 MNSATC    PIC X.                                          00001900
           02 MNSATP    PIC X.                                          00001910
           02 MNSATH    PIC X.                                          00001920
           02 MNSATV    PIC X.                                          00001930
           02 MNSATO    PIC X(2).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MNCASEA   PIC X.                                          00001960
           02 MNCASEC   PIC X.                                          00001970
           02 MNCASEP   PIC X.                                          00001980
           02 MNCASEH   PIC X.                                          00001990
           02 MNCASEV   PIC X.                                          00002000
           02 MNCASEO   PIC X(3).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MCMTAIREA      PIC X.                                     00002030
           02 MCMTAIREC PIC X.                                          00002040
           02 MCMTAIREP PIC X.                                          00002050
           02 MCMTAIREH PIC X.                                          00002060
           02 MCMTAIREV PIC X.                                          00002070
           02 MCMTAIREO      PIC 9,999.                                 00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNSOCA    PIC X.                                          00002100
           02 MNSOCC    PIC X.                                          00002110
           02 MNSOCP    PIC X.                                          00002120
           02 MNSOCH    PIC X.                                          00002130
           02 MNSOCV    PIC X.                                          00002140
           02 MNSOCO    PIC X(3).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MLSOCA    PIC X.                                          00002170
           02 MLSOCC    PIC X.                                          00002180
           02 MLSOCP    PIC X.                                          00002190
           02 MLSOCH    PIC X.                                          00002200
           02 MLSOCV    PIC X.                                          00002210
           02 MLSOCO    PIC X(20).                                      00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCLIV1A   PIC X.                                          00002240
           02 MCLIV1C   PIC X.                                          00002250
           02 MCLIV1P   PIC X.                                          00002260
           02 MCLIV1H   PIC X.                                          00002270
           02 MCLIV1V   PIC X.                                          00002280
           02 MCLIV1O   PIC X(10).                                      00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLLIV1A   PIC X.                                          00002310
           02 MLLIV1C   PIC X.                                          00002320
           02 MLLIV1P   PIC X.                                          00002330
           02 MLLIV1H   PIC X.                                          00002340
           02 MLLIV1V   PIC X.                                          00002350
           02 MLLIV1O   PIC X(20).                                      00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MCPROFA   PIC X.                                          00002380
           02 MCPROFC   PIC X.                                          00002390
           02 MCPROFP   PIC X.                                          00002400
           02 MCPROFH   PIC X.                                          00002410
           02 MCPROFV   PIC X.                                          00002420
           02 MCPROFO   PIC X(5).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MLPROFA   PIC X.                                          00002450
           02 MLPROFC   PIC X.                                          00002460
           02 MLPROFP   PIC X.                                          00002470
           02 MLPROFH   PIC X.                                          00002480
           02 MLPROFV   PIC X.                                          00002490
           02 MLPROFO   PIC X(20).                                      00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MCLIV2A   PIC X.                                          00002520
           02 MCLIV2C   PIC X.                                          00002530
           02 MCLIV2P   PIC X.                                          00002540
           02 MCLIV2H   PIC X.                                          00002550
           02 MCLIV2V   PIC X.                                          00002560
           02 MCLIV2O   PIC X(10).                                      00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MLLIV2A   PIC X.                                          00002590
           02 MLLIV2C   PIC X.                                          00002600
           02 MLLIV2P   PIC X.                                          00002610
           02 MLLIV2H   PIC X.                                          00002620
           02 MLLIV2V   PIC X.                                          00002630
           02 MLLIV2O   PIC X(20).                                      00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MDDELIVA  PIC X.                                          00002660
           02 MDDELIVC  PIC X.                                          00002670
           02 MDDELIVP  PIC X.                                          00002680
           02 MDDELIVH  PIC X.                                          00002690
           02 MDDELIVV  PIC X.                                          00002700
           02 MDDELIVO  PIC X(8).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCTOURNEEA     PIC X.                                     00002730
           02 MCTOURNEEC     PIC X.                                     00002740
           02 MCTOURNEEP     PIC X.                                     00002750
           02 MCTOURNEEH     PIC X.                                     00002760
           02 MCTOURNEEV     PIC X.                                     00002770
           02 MCTOURNEEO     PIC X(3).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCLIV3A   PIC X.                                          00002800
           02 MCLIV3C   PIC X.                                          00002810
           02 MCLIV3P   PIC X.                                          00002820
           02 MCLIV3H   PIC X.                                          00002830
           02 MCLIV3V   PIC X.                                          00002840
           02 MCLIV3O   PIC X(10).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLLIV3A   PIC X.                                          00002870
           02 MLLIV3C   PIC X.                                          00002880
           02 MLLIV3P   PIC X.                                          00002890
           02 MLLIV3H   PIC X.                                          00002900
           02 MLLIV3V   PIC X.                                          00002910
           02 MLLIV3O   PIC X(20).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MLOGISA   PIC X.                                          00002940
           02 MLOGISC   PIC X.                                          00002950
           02 MLOGISP   PIC X.                                          00002960
           02 MLOGISH   PIC X.                                          00002970
           02 MLOGISV   PIC X.                                          00002980
           02 MLOGISO   PIC X(9).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCLOGIA   PIC X.                                          00003010
           02 MCLOGIC   PIC X.                                          00003020
           02 MCLOGIP   PIC X.                                          00003030
           02 MCLOGIH   PIC X.                                          00003040
           02 MCLOGIV   PIC X.                                          00003050
           02 MCLOGIO   PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MCTOURNLGA     PIC X.                                     00003080
           02 MCTOURNLGC     PIC X.                                     00003090
           02 MCTOURNLGP     PIC X.                                     00003100
           02 MCTOURNLGH     PIC X.                                     00003110
           02 MCTOURNLGV     PIC X.                                     00003120
           02 MCTOURNLGO     PIC X(3).                                  00003130
           02 M267O OCCURS   12 TIMES .                                 00003140
             03 FILLER       PIC X(2).                                  00003150
             03 MNMAGA  PIC X.                                          00003160
             03 MNMAGC  PIC X.                                          00003170
             03 MNMAGP  PIC X.                                          00003180
             03 MNMAGH  PIC X.                                          00003190
             03 MNMAGV  PIC X.                                          00003200
             03 MNMAGO  PIC X(3).                                       00003210
             03 FILLER       PIC X(2).                                  00003220
             03 MNVENTEA     PIC X.                                     00003230
             03 MNVENTEC     PIC X.                                     00003240
             03 MNVENTEP     PIC X.                                     00003250
             03 MNVENTEH     PIC X.                                     00003260
             03 MNVENTEV     PIC X.                                     00003270
             03 MNVENTEO     PIC X(7).                                  00003280
             03 FILLER       PIC X(2).                                  00003290
             03 MCTYPEA      PIC X.                                     00003300
             03 MCTYPEC PIC X.                                          00003310
             03 MCTYPEP PIC X.                                          00003320
             03 MCTYPEH PIC X.                                          00003330
             03 MCTYPEV PIC X.                                          00003340
             03 MCTYPEO      PIC X.                                     00003350
             03 FILLER       PIC X(2).                                  00003360
             03 MCTYPEASSA   PIC X.                                     00003370
             03 MCTYPEASSC   PIC X.                                     00003380
             03 MCTYPEASSP   PIC X.                                     00003390
             03 MCTYPEASSH   PIC X.                                     00003400
             03 MCTYPEASSV   PIC X.                                     00003410
             03 MCTYPEASSO   PIC X(2).                                  00003420
             03 FILLER       PIC X(2).                                  00003430
             03 MLPLAGEA     PIC X.                                     00003440
             03 MLPLAGEC     PIC X.                                     00003450
             03 MLPLAGEP     PIC X.                                     00003460
             03 MLPLAGEH     PIC X.                                     00003470
             03 MLPLAGEV     PIC X.                                     00003480
             03 MLPLAGEO     PIC X(8).                                  00003490
             03 FILLER       PIC X(2).                                  00003500
             03 MLNOMA  PIC X.                                          00003510
             03 MLNOMC  PIC X.                                          00003520
             03 MLNOMP  PIC X.                                          00003530
             03 MLNOMH  PIC X.                                          00003540
             03 MLNOMV  PIC X.                                          00003550
             03 MLNOMO  PIC X(15).                                      00003560
             03 FILLER       PIC X(2).                                  00003570
             03 MCPTTA  PIC X.                                          00003580
             03 MCPTTC  PIC X.                                          00003590
             03 MCPTTP  PIC X.                                          00003600
             03 MCPTTH  PIC X.                                          00003610
             03 MCPTTV  PIC X.                                          00003620
             03 MCPTTO  PIC X(5).                                       00003630
             03 FILLER       PIC X(2).                                  00003640
             03 MLOCALITEA   PIC X.                                     00003650
             03 MLOCALITEC   PIC X.                                     00003660
             03 MLOCALITEP   PIC X.                                     00003670
             03 MLOCALITEH   PIC X.                                     00003680
             03 MLOCALITEV   PIC X.                                     00003690
             03 MLOCALITEO   PIC X(23).                                 00003700
             03 FILLER       PIC X(2).                                  00003710
             03 MCRENEAUA    PIC X.                                     00003720
             03 MCRENEAUC    PIC X.                                     00003730
             03 MCRENEAUP    PIC X.                                     00003740
             03 MCRENEAUH    PIC X.                                     00003750
             03 MCRENEAUV    PIC X.                                     00003760
             03 MCRENEAUO    PIC X(7).                                  00003770
           02 FILLER    PIC X(2).                                       00003780
           02 MZONCMDA  PIC X.                                          00003790
           02 MZONCMDC  PIC X.                                          00003800
           02 MZONCMDP  PIC X.                                          00003810
           02 MZONCMDH  PIC X.                                          00003820
           02 MZONCMDV  PIC X.                                          00003830
           02 MZONCMDO  PIC X(15).                                      00003840
           02 FILLER    PIC X(2).                                       00003850
           02 MLIBERRA  PIC X.                                          00003860
           02 MLIBERRC  PIC X.                                          00003870
           02 MLIBERRP  PIC X.                                          00003880
           02 MLIBERRH  PIC X.                                          00003890
           02 MLIBERRV  PIC X.                                          00003900
           02 MLIBERRO  PIC X(58).                                      00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MCODTRAA  PIC X.                                          00003930
           02 MCODTRAC  PIC X.                                          00003940
           02 MCODTRAP  PIC X.                                          00003950
           02 MCODTRAH  PIC X.                                          00003960
           02 MCODTRAV  PIC X.                                          00003970
           02 MCODTRAO  PIC X(4).                                       00003980
           02 FILLER    PIC X(2).                                       00003990
           02 MCICSA    PIC X.                                          00004000
           02 MCICSC    PIC X.                                          00004010
           02 MCICSP    PIC X.                                          00004020
           02 MCICSH    PIC X.                                          00004030
           02 MCICSV    PIC X.                                          00004040
           02 MCICSO    PIC X(5).                                       00004050
           02 FILLER    PIC X(2).                                       00004060
           02 MNETNAMA  PIC X.                                          00004070
           02 MNETNAMC  PIC X.                                          00004080
           02 MNETNAMP  PIC X.                                          00004090
           02 MNETNAMH  PIC X.                                          00004100
           02 MNETNAMV  PIC X.                                          00004110
           02 MNETNAMO  PIC X(8).                                       00004120
           02 FILLER    PIC X(2).                                       00004130
           02 MSCREENA  PIC X.                                          00004140
           02 MSCREENC  PIC X.                                          00004150
           02 MSCREENP  PIC X.                                          00004160
           02 MSCREENH  PIC X.                                          00004170
           02 MSCREENV  PIC X.                                          00004180
           02 MSCREENO  PIC X(4).                                       00004190
                                                                                
