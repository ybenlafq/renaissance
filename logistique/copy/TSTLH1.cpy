      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      *  TS : VALIDATION DE TOURNEE                                   * 00020000
      ***************************************************************** 00030000
       01  TS-TLH1.                                                     00040000
           02 TSH1-NOM.                                                 00050000
               03 TSH1-NOM1       PIC X(04) VALUE 'TLH1'.               00060000
               03 TSH1-TERM       PIC X(04).                            00070000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     02 TSH1-LONG          PIC S9(04) COMP VALUE +103.           00080000
      *--                                                                       
            02 TSH1-LONG          PIC S9(04) COMP-5 VALUE +103.                 
      *}                                                                        
            02 TSH1-DONNEES.                                            00100000
               04 TSH1-NLIEUENT         PIC X(03).                      00120000
               04 TSH1-NHS              PIC X(07).                      00120000
               04 TSH1-NCODIC           PIC X(07).                      00120000
               04 TSH1-WARTINEX         PIC X(01).                      00120010
               04 TSH1-LCOMMENTENT.                                     00120020
                  05 TSH1-NSOCIETE      PIC X(03).                      00120100
                  05 TSH1-NLIEU         PIC X(03).                      00120200
                  05 TSH1-NVENTE        PIC X(07).                      00130000
                  05 TSH1-NSEQ          PIC X(02).                      00130100
                  05 TSH1-FIL1          PIC X(05).                      00130200
               04 TSH1-QTEHS            PIC 9(05).                      00140000
               04 TSH1-LREF             PIC X(20).                      00140100
               04 TSH1-CFAM             PIC X(05).                      00150000
               04 TSH1-LFAM             PIC X(20).                      00150100
               04 TSH1-CMARQ            PIC X(05).                      00160000
               04 TSH1-LMARQ            PIC X(20).                      00160100
                                                                                
                                                                        00420000
