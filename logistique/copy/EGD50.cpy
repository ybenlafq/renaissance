      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD50   EGD50                                              00000020
      ***************************************************************** 00000030
       01   EGD50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDEPOTL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MSOCDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSOCDEPOTF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSOCDEPOTI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDEPOTI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURNEEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MJOURNEEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MJOURNEEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MJOURNEEI      PIC X(8).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRAFALEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MRAFALEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MRAFALEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MRAFALEI  PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR1L   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MJOUR1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR1F   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MJOUR1I   PIC X(8).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRAF1L    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MRAF1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRAF1F    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MRAF1I    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR4L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MJOUR4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR4F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MJOUR4I   PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRAF4L    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MRAF4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRAF4F    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MRAF4I    PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODESTOCKL    COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MMODESTOCKL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MMODESTOCKF    PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MMODESTOCKI    PIC X(5).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPEMPLL     COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MWTYPEMPLL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWTYPEMPLF     PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MWTYPEMPLI     PIC X.                                     00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR2L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MJOUR2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR2F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MJOUR2I   PIC X(8).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRAF2L    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MRAF2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRAF2F    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MRAF2I    PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR5L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MJOUR5L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR5F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MJOUR5I   PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRAF5L    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MRAF5L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRAF5F    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MRAF5I    PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCALLEE1L      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MCALLEE1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCALLEE1F      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCALLEE1I      PIC X(2).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCALLEE2L      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MCALLEE2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCALLEE2F      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCALLEE2I      PIC X(2).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR3L   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MJOUR3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR3F   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MJOUR3I   PIC X(8).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRAF3L    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MRAF3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRAF3F    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MRAF3I    PIC X(3).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR6L   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MJOUR6L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR6F   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MJOUR6I   PIC X(8).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRAF6L    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MRAF6L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRAF6F    PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MRAF6I    PIC X(3).                                       00000970
           02 MSTRUCTUREI OCCURS   10 TIMES .                           00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCODICI      PIC X(7).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MFAMI   PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMQEL   COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MMQEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMQEF   PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MMQEI   PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MREFI   PIC X(20).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEMP1L  COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MEMP1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MEMP1F  PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MEMP1I  PIC X(2).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEMP2L  COMP PIC S9(4).                                 00001190
      *--                                                                       
             03 MEMP2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MEMP2F  PIC X.                                          00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MEMP2I  PIC X(2).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEMP3L  COMP PIC S9(4).                                 00001230
      *--                                                                       
             03 MEMP3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MEMP3F  PIC X.                                          00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MEMP3I  PIC X(3).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEDEML     COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MQTEDEML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTEDEMF     PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MQTEDEMI     PIC X(5).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTERESL     COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MQTERESL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTERESF     PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MQTERESI     PIC X(5).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEPRELL    COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MQTEPRELL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQTEPRELF    PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MQTEPRELI    PIC X(5).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MZONCMDI  PIC X(15).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MLIBERRI  PIC X(58).                                      00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCODTRAI  PIC X(4).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MCICSI    PIC X(5).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MNETNAMI  PIC X(8).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MSCREENI  PIC X(4).                                       00001620
      ***************************************************************** 00001630
      * SDF: EGD50   EGD50                                              00001640
      ***************************************************************** 00001650
       01   EGD50O REDEFINES EGD50I.                                    00001660
           02 FILLER    PIC X(12).                                      00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MDATJOUA  PIC X.                                          00001690
           02 MDATJOUC  PIC X.                                          00001700
           02 MDATJOUP  PIC X.                                          00001710
           02 MDATJOUH  PIC X.                                          00001720
           02 MDATJOUV  PIC X.                                          00001730
           02 MDATJOUO  PIC X(10).                                      00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MTIMJOUA  PIC X.                                          00001760
           02 MTIMJOUC  PIC X.                                          00001770
           02 MTIMJOUP  PIC X.                                          00001780
           02 MTIMJOUH  PIC X.                                          00001790
           02 MTIMJOUV  PIC X.                                          00001800
           02 MTIMJOUO  PIC X(5).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MPAGEA    PIC X.                                          00001830
           02 MPAGEC    PIC X.                                          00001840
           02 MPAGEP    PIC X.                                          00001850
           02 MPAGEH    PIC X.                                          00001860
           02 MPAGEV    PIC X.                                          00001870
           02 MPAGEO    PIC X(3).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MSOCDEPOTA     PIC X.                                     00001900
           02 MSOCDEPOTC     PIC X.                                     00001910
           02 MSOCDEPOTP     PIC X.                                     00001920
           02 MSOCDEPOTH     PIC X.                                     00001930
           02 MSOCDEPOTV     PIC X.                                     00001940
           02 MSOCDEPOTO     PIC X(3).                                  00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MDEPOTA   PIC X.                                          00001970
           02 MDEPOTC   PIC X.                                          00001980
           02 MDEPOTP   PIC X.                                          00001990
           02 MDEPOTH   PIC X.                                          00002000
           02 MDEPOTV   PIC X.                                          00002010
           02 MDEPOTO   PIC X(3).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MJOURNEEA      PIC X.                                     00002040
           02 MJOURNEEC PIC X.                                          00002050
           02 MJOURNEEP PIC X.                                          00002060
           02 MJOURNEEH PIC X.                                          00002070
           02 MJOURNEEV PIC X.                                          00002080
           02 MJOURNEEO      PIC X(8).                                  00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MRAFALEA  PIC X.                                          00002110
           02 MRAFALEC  PIC X.                                          00002120
           02 MRAFALEP  PIC X.                                          00002130
           02 MRAFALEH  PIC X.                                          00002140
           02 MRAFALEV  PIC X.                                          00002150
           02 MRAFALEO  PIC X(3).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MJOUR1A   PIC X.                                          00002180
           02 MJOUR1C   PIC X.                                          00002190
           02 MJOUR1P   PIC X.                                          00002200
           02 MJOUR1H   PIC X.                                          00002210
           02 MJOUR1V   PIC X.                                          00002220
           02 MJOUR1O   PIC X(8).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MRAF1A    PIC X.                                          00002250
           02 MRAF1C    PIC X.                                          00002260
           02 MRAF1P    PIC X.                                          00002270
           02 MRAF1H    PIC X.                                          00002280
           02 MRAF1V    PIC X.                                          00002290
           02 MRAF1O    PIC X(3).                                       00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MJOUR4A   PIC X.                                          00002320
           02 MJOUR4C   PIC X.                                          00002330
           02 MJOUR4P   PIC X.                                          00002340
           02 MJOUR4H   PIC X.                                          00002350
           02 MJOUR4V   PIC X.                                          00002360
           02 MJOUR4O   PIC X(8).                                       00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MRAF4A    PIC X.                                          00002390
           02 MRAF4C    PIC X.                                          00002400
           02 MRAF4P    PIC X.                                          00002410
           02 MRAF4H    PIC X.                                          00002420
           02 MRAF4V    PIC X.                                          00002430
           02 MRAF4O    PIC X(3).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MMODESTOCKA    PIC X.                                     00002460
           02 MMODESTOCKC    PIC X.                                     00002470
           02 MMODESTOCKP    PIC X.                                     00002480
           02 MMODESTOCKH    PIC X.                                     00002490
           02 MMODESTOCKV    PIC X.                                     00002500
           02 MMODESTOCKO    PIC X(5).                                  00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MWTYPEMPLA     PIC X.                                     00002530
           02 MWTYPEMPLC     PIC X.                                     00002540
           02 MWTYPEMPLP     PIC X.                                     00002550
           02 MWTYPEMPLH     PIC X.                                     00002560
           02 MWTYPEMPLV     PIC X.                                     00002570
           02 MWTYPEMPLO     PIC X.                                     00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MJOUR2A   PIC X.                                          00002600
           02 MJOUR2C   PIC X.                                          00002610
           02 MJOUR2P   PIC X.                                          00002620
           02 MJOUR2H   PIC X.                                          00002630
           02 MJOUR2V   PIC X.                                          00002640
           02 MJOUR2O   PIC X(8).                                       00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MRAF2A    PIC X.                                          00002670
           02 MRAF2C    PIC X.                                          00002680
           02 MRAF2P    PIC X.                                          00002690
           02 MRAF2H    PIC X.                                          00002700
           02 MRAF2V    PIC X.                                          00002710
           02 MRAF2O    PIC X(3).                                       00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MJOUR5A   PIC X.                                          00002740
           02 MJOUR5C   PIC X.                                          00002750
           02 MJOUR5P   PIC X.                                          00002760
           02 MJOUR5H   PIC X.                                          00002770
           02 MJOUR5V   PIC X.                                          00002780
           02 MJOUR5O   PIC X(8).                                       00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MRAF5A    PIC X.                                          00002810
           02 MRAF5C    PIC X.                                          00002820
           02 MRAF5P    PIC X.                                          00002830
           02 MRAF5H    PIC X.                                          00002840
           02 MRAF5V    PIC X.                                          00002850
           02 MRAF5O    PIC X(3).                                       00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MCALLEE1A      PIC X.                                     00002880
           02 MCALLEE1C PIC X.                                          00002890
           02 MCALLEE1P PIC X.                                          00002900
           02 MCALLEE1H PIC X.                                          00002910
           02 MCALLEE1V PIC X.                                          00002920
           02 MCALLEE1O      PIC X(2).                                  00002930
           02 FILLER    PIC X(2).                                       00002940
           02 MCALLEE2A      PIC X.                                     00002950
           02 MCALLEE2C PIC X.                                          00002960
           02 MCALLEE2P PIC X.                                          00002970
           02 MCALLEE2H PIC X.                                          00002980
           02 MCALLEE2V PIC X.                                          00002990
           02 MCALLEE2O      PIC X(2).                                  00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MJOUR3A   PIC X.                                          00003020
           02 MJOUR3C   PIC X.                                          00003030
           02 MJOUR3P   PIC X.                                          00003040
           02 MJOUR3H   PIC X.                                          00003050
           02 MJOUR3V   PIC X.                                          00003060
           02 MJOUR3O   PIC X(8).                                       00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MRAF3A    PIC X.                                          00003090
           02 MRAF3C    PIC X.                                          00003100
           02 MRAF3P    PIC X.                                          00003110
           02 MRAF3H    PIC X.                                          00003120
           02 MRAF3V    PIC X.                                          00003130
           02 MRAF3O    PIC X(3).                                       00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MJOUR6A   PIC X.                                          00003160
           02 MJOUR6C   PIC X.                                          00003170
           02 MJOUR6P   PIC X.                                          00003180
           02 MJOUR6H   PIC X.                                          00003190
           02 MJOUR6V   PIC X.                                          00003200
           02 MJOUR6O   PIC X(8).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MRAF6A    PIC X.                                          00003230
           02 MRAF6C    PIC X.                                          00003240
           02 MRAF6P    PIC X.                                          00003250
           02 MRAF6H    PIC X.                                          00003260
           02 MRAF6V    PIC X.                                          00003270
           02 MRAF6O    PIC X(3).                                       00003280
           02 MSTRUCTUREO OCCURS   10 TIMES .                           00003290
             03 FILLER       PIC X(2).                                  00003300
             03 MCODICA      PIC X.                                     00003310
             03 MCODICC PIC X.                                          00003320
             03 MCODICP PIC X.                                          00003330
             03 MCODICH PIC X.                                          00003340
             03 MCODICV PIC X.                                          00003350
             03 MCODICO      PIC 9(7).                                  00003360
             03 FILLER       PIC X(2).                                  00003370
             03 MFAMA   PIC X.                                          00003380
             03 MFAMC   PIC X.                                          00003390
             03 MFAMP   PIC X.                                          00003400
             03 MFAMH   PIC X.                                          00003410
             03 MFAMV   PIC X.                                          00003420
             03 MFAMO   PIC X(5).                                       00003430
             03 FILLER       PIC X(2).                                  00003440
             03 MMQEA   PIC X.                                          00003450
             03 MMQEC   PIC X.                                          00003460
             03 MMQEP   PIC X.                                          00003470
             03 MMQEH   PIC X.                                          00003480
             03 MMQEV   PIC X.                                          00003490
             03 MMQEO   PIC X(5).                                       00003500
             03 FILLER       PIC X(2).                                  00003510
             03 MREFA   PIC X.                                          00003520
             03 MREFC   PIC X.                                          00003530
             03 MREFP   PIC X.                                          00003540
             03 MREFH   PIC X.                                          00003550
             03 MREFV   PIC X.                                          00003560
             03 MREFO   PIC X(20).                                      00003570
             03 FILLER       PIC X(2).                                  00003580
             03 MEMP1A  PIC X.                                          00003590
             03 MEMP1C  PIC X.                                          00003600
             03 MEMP1P  PIC X.                                          00003610
             03 MEMP1H  PIC X.                                          00003620
             03 MEMP1V  PIC X.                                          00003630
             03 MEMP1O  PIC X(2).                                       00003640
             03 FILLER       PIC X(2).                                  00003650
             03 MEMP2A  PIC X.                                          00003660
             03 MEMP2C  PIC X.                                          00003670
             03 MEMP2P  PIC X.                                          00003680
             03 MEMP2H  PIC X.                                          00003690
             03 MEMP2V  PIC X.                                          00003700
             03 MEMP2O  PIC X(2).                                       00003710
             03 FILLER       PIC X(2).                                  00003720
             03 MEMP3A  PIC X.                                          00003730
             03 MEMP3C  PIC X.                                          00003740
             03 MEMP3P  PIC X.                                          00003750
             03 MEMP3H  PIC X.                                          00003760
             03 MEMP3V  PIC X.                                          00003770
             03 MEMP3O  PIC 9(3).                                       00003780
             03 FILLER       PIC X(2).                                  00003790
             03 MQTEDEMA     PIC X.                                     00003800
             03 MQTEDEMC     PIC X.                                     00003810
             03 MQTEDEMP     PIC X.                                     00003820
             03 MQTEDEMH     PIC X.                                     00003830
             03 MQTEDEMV     PIC X.                                     00003840
             03 MQTEDEMO     PIC ZZZZ9.                                 00003850
             03 FILLER       PIC X(2).                                  00003860
             03 MQTERESA     PIC X.                                     00003870
             03 MQTERESC     PIC X.                                     00003880
             03 MQTERESP     PIC X.                                     00003890
             03 MQTERESH     PIC X.                                     00003900
             03 MQTERESV     PIC X.                                     00003910
             03 MQTERESO     PIC ZZZZ9.                                 00003920
             03 FILLER       PIC X(2).                                  00003930
             03 MQTEPRELA    PIC X.                                     00003940
             03 MQTEPRELC    PIC X.                                     00003950
             03 MQTEPRELP    PIC X.                                     00003960
             03 MQTEPRELH    PIC X.                                     00003970
             03 MQTEPRELV    PIC X.                                     00003980
             03 MQTEPRELO    PIC ZZZZ9.                                 00003990
           02 FILLER    PIC X(2).                                       00004000
           02 MZONCMDA  PIC X.                                          00004010
           02 MZONCMDC  PIC X.                                          00004020
           02 MZONCMDP  PIC X.                                          00004030
           02 MZONCMDH  PIC X.                                          00004040
           02 MZONCMDV  PIC X.                                          00004050
           02 MZONCMDO  PIC X(15).                                      00004060
           02 FILLER    PIC X(2).                                       00004070
           02 MLIBERRA  PIC X.                                          00004080
           02 MLIBERRC  PIC X.                                          00004090
           02 MLIBERRP  PIC X.                                          00004100
           02 MLIBERRH  PIC X.                                          00004110
           02 MLIBERRV  PIC X.                                          00004120
           02 MLIBERRO  PIC X(58).                                      00004130
           02 FILLER    PIC X(2).                                       00004140
           02 MCODTRAA  PIC X.                                          00004150
           02 MCODTRAC  PIC X.                                          00004160
           02 MCODTRAP  PIC X.                                          00004170
           02 MCODTRAH  PIC X.                                          00004180
           02 MCODTRAV  PIC X.                                          00004190
           02 MCODTRAO  PIC X(4).                                       00004200
           02 FILLER    PIC X(2).                                       00004210
           02 MCICSA    PIC X.                                          00004220
           02 MCICSC    PIC X.                                          00004230
           02 MCICSP    PIC X.                                          00004240
           02 MCICSH    PIC X.                                          00004250
           02 MCICSV    PIC X.                                          00004260
           02 MCICSO    PIC X(5).                                       00004270
           02 FILLER    PIC X(2).                                       00004280
           02 MNETNAMA  PIC X.                                          00004290
           02 MNETNAMC  PIC X.                                          00004300
           02 MNETNAMP  PIC X.                                          00004310
           02 MNETNAMH  PIC X.                                          00004320
           02 MNETNAMV  PIC X.                                          00004330
           02 MNETNAMO  PIC X(8).                                       00004340
           02 FILLER    PIC X(2).                                       00004350
           02 MSCREENA  PIC X.                                          00004360
           02 MSCREENC  PIC X.                                          00004370
           02 MSCREENP  PIC X.                                          00004380
           02 MSCREENH  PIC X.                                          00004390
           02 MSCREENV  PIC X.                                          00004400
           02 MSCREENO  PIC X(4).                                       00004410
                                                                                
