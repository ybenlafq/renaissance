      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *-------------------------------------------------------------    00000040
      *-------------------------------------------------------------    00000040
      *       COPIE POUR LE MODULE MFA01                                00000040
      *            SUIVI DES TOURNEES DE  LIVRAISON                     00000070
      *            => LOG DANS RTAN02, DES SUPPRESIONS DE LIGNES        00000080
      *               DANS RTTL02                                       00000090
      *  LE 17/12/2009                                                  00000100
      *  C.LAVAURE - DSA065                                             00000110
      *-------------------------------------------------------------    00000130
      *-------------------------------------------------------------    00000150
       01 COMM-MFA01-APPLI.                                             00260000
          02 COMM-MFA01-ENTREE.                                         00260000
             03 COMM-MFA01-NPROG       PIC X(08).                       00320000
             03 COMM-MFA01-APPLID      PIC X(08).                       00330000
             03 COMM-MFA01-TERMID      PIC X(04).                       00340000
             03 COMM-MFA01-USERID      PIC X(08).                       00340000
             03 COMM-MFA01-DDELIV      PIC X(08).                               
             03 COMM-MFA01-NSOC        PIC X(03).                               
             03 COMM-MFA01-CPROTOUR    PIC X(08).                               
             03 COMM-MFA01-CTOURNEE    PIC X(08).                               
             03 COMM-MFA01-NMAG        PIC X(03).                               
             03 COMM-MFA01-NVENTE      PIC X(07).                               
             03 COMM-MFA01-CADRTOUR    PIC X(03).                       00320000
             03 COMM-MFA01-CTRAITEMENT PIC X(01).                       00320000
         02 COMM-MFA01-SORTIE.                                          00260000
             03 COMM-MFA01-CODE-RETOUR PIC X(01).                       00340000
             03 COMM-MFA01-MESSAGE     PIC X(30).                               
         02  FILLER                    PIC X(500).                      00340000
                                                                                
