      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFL00   EFL00                                              00000020
      ***************************************************************** 00000030
       01   EFL02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNDEPOTI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLDEPOTI  PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWGENGROL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MWGENGROL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWGENGROF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MWGENGROI      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFML     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MWFML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MWFMF     PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MWFMI     PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMSL     COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MWMSL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MWMSF     PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MWMSI     PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGEMPLACTL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MGEMPLACTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MGEMPLACTF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MGEMPLACTI     PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWABCL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MWABCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWABCF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MWABCI    PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWRPL     COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MWRPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MWRPF     PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MWRPI     PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWQJOURL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MWQJOURL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWQJOURF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MWQJOURI  PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWGRQUAIL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MWGRQUAIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWGRQUAIF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MWGRQUAII      PIC X.                                     00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTGL00L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MWTGL00L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWTGL00F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MWTGL00I  PIC X.                                          00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTGL71L  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MWTGL71L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWTGL71F  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MWTGL71I  PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPRCTEL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MQPRCTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPRCTEF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MQPRCTEI  PIC X(6).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQVOLGD22L     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MQVOLGD22L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQVOLGD22F     PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MQVOLGD22I     PIC X(4).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBMAXGD22L   COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MQNBMAXGD22L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MQNBMAXGD22F   PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MQNBMAXGD22I   PIC X(2).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFLAGCDL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MWFLAGCDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWFLAGCDF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MWFLAGCDI      PIC X.                                     00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTGD93L  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MWTGD93L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWTGD93F  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MWTGD93I  PIC X.                                          00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWRETHSL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MWRETHSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWRETHSF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MWRETHSI  PIC X.                                          00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWRESFOURL     COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MWRESFOURL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWRESFOURF     PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MWRESFOURI     PIC X.                                     00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWGVEMDL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MWGVEMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWGVEMDF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MWGVEMDI  PIC X.                                          00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQDELAIDEPL    COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MQDELAIDEPL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQDELAIDEPF    PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MQDELAIDEPI    PIC X(3).                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTLML    COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MQTLML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQTLMF    PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MQTLMI    PIC X(5).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTRTL  COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MNSOCDEPOTRTL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNSOCDEPOTRTF  PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MNSOCDEPOTRTI  PIC X(3).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTRTL     COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MNDEPOTRTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNDEPOTRTF     PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNDEPOTRTI     PIC X(3).                                  00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQELAL    COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MQELAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQELAF    PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MQELAI    PIC X(5).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MLIBERRI  PIC X(78).                                      00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MCODTRAI  PIC X(4).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MCICSI    PIC X(5).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MNETNAMI  PIC X(8).                                       00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MSCREENI  PIC X(4).                                       00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPALL    COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MWPALL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWPALF    PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MWPALI    PIC X.                                          00001410
      ***************************************************************** 00001420
      * SDF: EFL00   EFL00                                              00001430
      ***************************************************************** 00001440
       01   EFL02O REDEFINES EFL02I.                                    00001450
           02 FILLER    PIC X(12).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MDATJOUA  PIC X.                                          00001480
           02 MDATJOUC  PIC X.                                          00001490
           02 MDATJOUP  PIC X.                                          00001500
           02 MDATJOUH  PIC X.                                          00001510
           02 MDATJOUV  PIC X.                                          00001520
           02 MDATJOUO  PIC X(10).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MTIMJOUA  PIC X.                                          00001550
           02 MTIMJOUC  PIC X.                                          00001560
           02 MTIMJOUP  PIC X.                                          00001570
           02 MTIMJOUH  PIC X.                                          00001580
           02 MTIMJOUV  PIC X.                                          00001590
           02 MTIMJOUO  PIC X(5).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MNSOCA    PIC X.                                          00001620
           02 MNSOCC    PIC X.                                          00001630
           02 MNSOCP    PIC X.                                          00001640
           02 MNSOCH    PIC X.                                          00001650
           02 MNSOCV    PIC X.                                          00001660
           02 MNSOCO    PIC X(3).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MNDEPOTA  PIC X.                                          00001690
           02 MNDEPOTC  PIC X.                                          00001700
           02 MNDEPOTP  PIC X.                                          00001710
           02 MNDEPOTH  PIC X.                                          00001720
           02 MNDEPOTV  PIC X.                                          00001730
           02 MNDEPOTO  PIC X(3).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLDEPOTA  PIC X.                                          00001760
           02 MLDEPOTC  PIC X.                                          00001770
           02 MLDEPOTP  PIC X.                                          00001780
           02 MLDEPOTH  PIC X.                                          00001790
           02 MLDEPOTV  PIC X.                                          00001800
           02 MLDEPOTO  PIC X(20).                                      00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MWGENGROA      PIC X.                                     00001830
           02 MWGENGROC PIC X.                                          00001840
           02 MWGENGROP PIC X.                                          00001850
           02 MWGENGROH PIC X.                                          00001860
           02 MWGENGROV PIC X.                                          00001870
           02 MWGENGROO      PIC X(3).                                  00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MWFMA     PIC X.                                          00001900
           02 MWFMC     PIC X.                                          00001910
           02 MWFMP     PIC X.                                          00001920
           02 MWFMH     PIC X.                                          00001930
           02 MWFMV     PIC X.                                          00001940
           02 MWFMO     PIC X.                                          00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MWMSA     PIC X.                                          00001970
           02 MWMSC     PIC X.                                          00001980
           02 MWMSP     PIC X.                                          00001990
           02 MWMSH     PIC X.                                          00002000
           02 MWMSV     PIC X.                                          00002010
           02 MWMSO     PIC X.                                          00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MGEMPLACTA     PIC X.                                     00002040
           02 MGEMPLACTC     PIC X.                                     00002050
           02 MGEMPLACTP     PIC X.                                     00002060
           02 MGEMPLACTH     PIC X.                                     00002070
           02 MGEMPLACTV     PIC X.                                     00002080
           02 MGEMPLACTO     PIC X.                                     00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MWABCA    PIC X.                                          00002110
           02 MWABCC    PIC X.                                          00002120
           02 MWABCP    PIC X.                                          00002130
           02 MWABCH    PIC X.                                          00002140
           02 MWABCV    PIC X.                                          00002150
           02 MWABCO    PIC X.                                          00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MWRPA     PIC X.                                          00002180
           02 MWRPC     PIC X.                                          00002190
           02 MWRPP     PIC X.                                          00002200
           02 MWRPH     PIC X.                                          00002210
           02 MWRPV     PIC X.                                          00002220
           02 MWRPO     PIC X.                                          00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MWQJOURA  PIC X.                                          00002250
           02 MWQJOURC  PIC X.                                          00002260
           02 MWQJOURP  PIC X.                                          00002270
           02 MWQJOURH  PIC X.                                          00002280
           02 MWQJOURV  PIC X.                                          00002290
           02 MWQJOURO  PIC X.                                          00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MWGRQUAIA      PIC X.                                     00002320
           02 MWGRQUAIC PIC X.                                          00002330
           02 MWGRQUAIP PIC X.                                          00002340
           02 MWGRQUAIH PIC X.                                          00002350
           02 MWGRQUAIV PIC X.                                          00002360
           02 MWGRQUAIO      PIC X.                                     00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MWTGL00A  PIC X.                                          00002390
           02 MWTGL00C  PIC X.                                          00002400
           02 MWTGL00P  PIC X.                                          00002410
           02 MWTGL00H  PIC X.                                          00002420
           02 MWTGL00V  PIC X.                                          00002430
           02 MWTGL00O  PIC X.                                          00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MWTGL71A  PIC X.                                          00002460
           02 MWTGL71C  PIC X.                                          00002470
           02 MWTGL71P  PIC X.                                          00002480
           02 MWTGL71H  PIC X.                                          00002490
           02 MWTGL71V  PIC X.                                          00002500
           02 MWTGL71O  PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MQPRCTEA  PIC X.                                          00002530
           02 MQPRCTEC  PIC X.                                          00002540
           02 MQPRCTEP  PIC X.                                          00002550
           02 MQPRCTEH  PIC X.                                          00002560
           02 MQPRCTEV  PIC X.                                          00002570
           02 MQPRCTEO  PIC ZZ9,99.                                     00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MQVOLGD22A     PIC X.                                     00002600
           02 MQVOLGD22C     PIC X.                                     00002610
           02 MQVOLGD22P     PIC X.                                     00002620
           02 MQVOLGD22H     PIC X.                                     00002630
           02 MQVOLGD22V     PIC X.                                     00002640
           02 MQVOLGD22O     PIC 9,99.                                  00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MQNBMAXGD22A   PIC X.                                     00002670
           02 MQNBMAXGD22C   PIC X.                                     00002680
           02 MQNBMAXGD22P   PIC X.                                     00002690
           02 MQNBMAXGD22H   PIC X.                                     00002700
           02 MQNBMAXGD22V   PIC X.                                     00002710
           02 MQNBMAXGD22O   PIC Z9.                                    00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MWFLAGCDA      PIC X.                                     00002740
           02 MWFLAGCDC PIC X.                                          00002750
           02 MWFLAGCDP PIC X.                                          00002760
           02 MWFLAGCDH PIC X.                                          00002770
           02 MWFLAGCDV PIC X.                                          00002780
           02 MWFLAGCDO      PIC X.                                     00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MWTGD93A  PIC X.                                          00002810
           02 MWTGD93C  PIC X.                                          00002820
           02 MWTGD93P  PIC X.                                          00002830
           02 MWTGD93H  PIC X.                                          00002840
           02 MWTGD93V  PIC X.                                          00002850
           02 MWTGD93O  PIC X.                                          00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MWRETHSA  PIC X.                                          00002880
           02 MWRETHSC  PIC X.                                          00002890
           02 MWRETHSP  PIC X.                                          00002900
           02 MWRETHSH  PIC X.                                          00002910
           02 MWRETHSV  PIC X.                                          00002920
           02 MWRETHSO  PIC X.                                          00002930
           02 FILLER    PIC X(2).                                       00002940
           02 MWRESFOURA     PIC X.                                     00002950
           02 MWRESFOURC     PIC X.                                     00002960
           02 MWRESFOURP     PIC X.                                     00002970
           02 MWRESFOURH     PIC X.                                     00002980
           02 MWRESFOURV     PIC X.                                     00002990
           02 MWRESFOURO     PIC X.                                     00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MWGVEMDA  PIC X.                                          00003020
           02 MWGVEMDC  PIC X.                                          00003030
           02 MWGVEMDP  PIC X.                                          00003040
           02 MWGVEMDH  PIC X.                                          00003050
           02 MWGVEMDV  PIC X.                                          00003060
           02 MWGVEMDO  PIC X.                                          00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MQDELAIDEPA    PIC X.                                     00003090
           02 MQDELAIDEPC    PIC X.                                     00003100
           02 MQDELAIDEPP    PIC X.                                     00003110
           02 MQDELAIDEPH    PIC X.                                     00003120
           02 MQDELAIDEPV    PIC X.                                     00003130
           02 MQDELAIDEPO    PIC ZZ9.                                   00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MQTLMA    PIC X.                                          00003160
           02 MQTLMC    PIC X.                                          00003170
           02 MQTLMP    PIC X.                                          00003180
           02 MQTLMH    PIC X.                                          00003190
           02 MQTLMV    PIC X.                                          00003200
           02 MQTLMO    PIC ZZZZ9.                                      00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MNSOCDEPOTRTA  PIC X.                                     00003230
           02 MNSOCDEPOTRTC  PIC X.                                     00003240
           02 MNSOCDEPOTRTP  PIC X.                                     00003250
           02 MNSOCDEPOTRTH  PIC X.                                     00003260
           02 MNSOCDEPOTRTV  PIC X.                                     00003270
           02 MNSOCDEPOTRTO  PIC X(3).                                  00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNDEPOTRTA     PIC X.                                     00003300
           02 MNDEPOTRTC     PIC X.                                     00003310
           02 MNDEPOTRTP     PIC X.                                     00003320
           02 MNDEPOTRTH     PIC X.                                     00003330
           02 MNDEPOTRTV     PIC X.                                     00003340
           02 MNDEPOTRTO     PIC X(3).                                  00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MQELAA    PIC X.                                          00003370
           02 MQELAC    PIC X.                                          00003380
           02 MQELAP    PIC X.                                          00003390
           02 MQELAH    PIC X.                                          00003400
           02 MQELAV    PIC X.                                          00003410
           02 MQELAO    PIC ZZZZ9.                                      00003420
           02 FILLER    PIC X(2).                                       00003430
           02 MLIBERRA  PIC X.                                          00003440
           02 MLIBERRC  PIC X.                                          00003450
           02 MLIBERRP  PIC X.                                          00003460
           02 MLIBERRH  PIC X.                                          00003470
           02 MLIBERRV  PIC X.                                          00003480
           02 MLIBERRO  PIC X(78).                                      00003490
           02 FILLER    PIC X(2).                                       00003500
           02 MCODTRAA  PIC X.                                          00003510
           02 MCODTRAC  PIC X.                                          00003520
           02 MCODTRAP  PIC X.                                          00003530
           02 MCODTRAH  PIC X.                                          00003540
           02 MCODTRAV  PIC X.                                          00003550
           02 MCODTRAO  PIC X(4).                                       00003560
           02 FILLER    PIC X(2).                                       00003570
           02 MCICSA    PIC X.                                          00003580
           02 MCICSC    PIC X.                                          00003590
           02 MCICSP    PIC X.                                          00003600
           02 MCICSH    PIC X.                                          00003610
           02 MCICSV    PIC X.                                          00003620
           02 MCICSO    PIC X(5).                                       00003630
           02 FILLER    PIC X(2).                                       00003640
           02 MNETNAMA  PIC X.                                          00003650
           02 MNETNAMC  PIC X.                                          00003660
           02 MNETNAMP  PIC X.                                          00003670
           02 MNETNAMH  PIC X.                                          00003680
           02 MNETNAMV  PIC X.                                          00003690
           02 MNETNAMO  PIC X(8).                                       00003700
           02 FILLER    PIC X(2).                                       00003710
           02 MSCREENA  PIC X.                                          00003720
           02 MSCREENC  PIC X.                                          00003730
           02 MSCREENP  PIC X.                                          00003740
           02 MSCREENH  PIC X.                                          00003750
           02 MSCREENV  PIC X.                                          00003760
           02 MSCREENO  PIC X(4).                                       00003770
           02 FILLER    PIC X(2).                                       00003780
           02 MWPALA    PIC X.                                          00003790
           02 MWPALC    PIC X.                                          00003800
           02 MWPALP    PIC X.                                          00003810
           02 MWPALH    PIC X.                                          00003820
           02 MWPALV    PIC X.                                          00003830
           02 MWPALO    PIC X.                                          00003840
                                                                                
