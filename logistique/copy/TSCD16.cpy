      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE POUR LE MODULE MCD16                           *  00020001
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-CD16-LONG         PIC S9(2) COMP-3 VALUE 23.              00080001
       01  TS-CD16-DATA.                                                00090000
      *    05 TS-NSOCIETE    PIC X(03).                                 00100001
      *    05 TS-NLIEU       PIC X(03).                                         
           05 TS-CD16-NCODIC      PIC X(07).                            00120001
           05 TS-CD16-QLIVR       PIC S9(7) COMP-3.                             
           05 TS-CD16-PRA         PIC S9(7)V99 COMP-3.                  00122001
           05 TS-CD16-SRP         PIC S9(7)V9(6) COMP-3.                00122001
                                                                                
