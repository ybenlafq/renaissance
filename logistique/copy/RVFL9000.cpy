      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFL9000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFL9000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL9000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL9000.                                                            
      *}                                                                        
           02  FL90-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  FL90-NDEPOT                                                      
               PIC X(0003).                                                     
           02  FL90-WQJOUR                                                      
               PIC X(0001).                                                     
           02  FL90-WGENGRO                                                     
               PIC X(0003).                                                     
           02  FL90-GEMPLACT                                                    
               PIC X(0001).                                                     
           02  FL90-WRESFOUR                                                    
               PIC X(0001).                                                     
           02  FL90-WGRQUAI                                                     
               PIC X(0001).                                                     
           02  FL90-WGVEMD                                                      
               PIC X(0001).                                                     
           02  FL90-WTGD93                                                      
               PIC X(0001).                                                     
           02  FL90-QPRCTE                                                      
               PIC S9(3)V9(0002) COMP-3.                                        
           02  FL90-QVOLGD22                                                    
               PIC S9(1)V9(0002) COMP-3.                                        
           02  FL90-QNBMAXGD22                                                  
               PIC S9(2) COMP-3.                                                
           02  FL90-WTGL00                                                      
               PIC X(0001).                                                     
           02  FL90-WTGL71                                                      
               PIC X(0005).                                                     
           02  FL90-WRETHS                                                      
               PIC X(0001).                                                     
           02  FL90-QTLM                                                        
               PIC S9(5) COMP-3.                                                
           02  FL90-QELA                                                        
               PIC S9(5) COMP-3.                                                
           02  FL90-WFM                                                         
               PIC X(0001).                                                     
           02  FL90-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFL9000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL9000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL9000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-WQJOUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-WQJOUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-WGENGRO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-WGENGRO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-GEMPLACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-GEMPLACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-WRESFOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-WRESFOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-WGRQUAI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-WGRQUAI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-WGVEMD-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-WGVEMD-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-WTGD93-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-WTGD93-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-QPRCTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-QPRCTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-QVOLGD22-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-QVOLGD22-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-QNBMAXGD22-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-QNBMAXGD22-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-WTGL00-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-WTGL00-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-WTGL71-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-WTGL71-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-WRETHS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-WRETHS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-QTLM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-QTLM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-QELA-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-QELA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-WFM-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-WFM-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL90-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL90-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
