      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS04   EGS04                                              00000020
      ***************************************************************** 00000030
       01   EGS04I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MSOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSOCF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSOCI     PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDEPOTI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCROSSDOCKL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCROSSDOCKL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCROSSDOCKF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCROSSDOCKI    PIC X.                                     00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCODICI   PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLCODICI  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSTATUTI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAML     COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MFAML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFAMF     PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MFAMI     PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSAPPL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MSENSAPPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSENSAPPF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSENSAPPI      PIC X.                                     00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMARQUEL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MMARQUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMARQUEF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MMARQUEI  PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQUEL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MLMARQUEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMARQUEF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLMARQUEI      PIC X(20).                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSVTEL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MSENSVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSENSVTEF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSENSVTEI      PIC X.                                     00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCODICL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MWCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWCODICF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MWCODICI  PIC X(7).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREPCDEL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MREPCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MREPCDEF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MREPCDEI  PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBERRI  PIC X(78).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCODTRAI  PIC X(4).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCICSI    PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MNETNAMI  PIC X(8).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MSCREENI  PIC X(4).                                       00000930
      ***************************************************************** 00000940
      * SDF: EGS04   EGS04                                              00000950
      ***************************************************************** 00000960
       01   EGS04O REDEFINES EGS04I.                                    00000970
           02 FILLER    PIC X(12).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MDATJOUA  PIC X.                                          00001000
           02 MDATJOUC  PIC X.                                          00001010
           02 MDATJOUP  PIC X.                                          00001020
           02 MDATJOUH  PIC X.                                          00001030
           02 MDATJOUV  PIC X.                                          00001040
           02 MDATJOUO  PIC X(10).                                      00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MTIMJOUA  PIC X.                                          00001070
           02 MTIMJOUC  PIC X.                                          00001080
           02 MTIMJOUP  PIC X.                                          00001090
           02 MTIMJOUH  PIC X.                                          00001100
           02 MTIMJOUV  PIC X.                                          00001110
           02 MTIMJOUO  PIC X(5).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MPAGEA    PIC X.                                          00001140
           02 MPAGEC    PIC X.                                          00001150
           02 MPAGEP    PIC X.                                          00001160
           02 MPAGEH    PIC X.                                          00001170
           02 MPAGEV    PIC X.                                          00001180
           02 MPAGEO    PIC X(3).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MSOCA     PIC X.                                          00001210
           02 MSOCC     PIC X.                                          00001220
           02 MSOCP     PIC X.                                          00001230
           02 MSOCH     PIC X.                                          00001240
           02 MSOCV     PIC X.                                          00001250
           02 MSOCO     PIC X(3).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MDEPOTA   PIC X.                                          00001280
           02 MDEPOTC   PIC X.                                          00001290
           02 MDEPOTP   PIC X.                                          00001300
           02 MDEPOTH   PIC X.                                          00001310
           02 MDEPOTV   PIC X.                                          00001320
           02 MDEPOTO   PIC X(3).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCROSSDOCKA    PIC X.                                     00001350
           02 MCROSSDOCKC    PIC X.                                     00001360
           02 MCROSSDOCKP    PIC X.                                     00001370
           02 MCROSSDOCKH    PIC X.                                     00001380
           02 MCROSSDOCKV    PIC X.                                     00001390
           02 MCROSSDOCKO    PIC X.                                     00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCODICA   PIC X.                                          00001420
           02 MCODICC   PIC X.                                          00001430
           02 MCODICP   PIC X.                                          00001440
           02 MCODICH   PIC X.                                          00001450
           02 MCODICV   PIC X.                                          00001460
           02 MCODICO   PIC X(7).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLCODICA  PIC X.                                          00001490
           02 MLCODICC  PIC X.                                          00001500
           02 MLCODICP  PIC X.                                          00001510
           02 MLCODICH  PIC X.                                          00001520
           02 MLCODICV  PIC X.                                          00001530
           02 MLCODICO  PIC X(20).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MSTATUTA  PIC X.                                          00001560
           02 MSTATUTC  PIC X.                                          00001570
           02 MSTATUTP  PIC X.                                          00001580
           02 MSTATUTH  PIC X.                                          00001590
           02 MSTATUTV  PIC X.                                          00001600
           02 MSTATUTO  PIC X(3).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MFAMA     PIC X.                                          00001630
           02 MFAMC     PIC X.                                          00001640
           02 MFAMP     PIC X.                                          00001650
           02 MFAMH     PIC X.                                          00001660
           02 MFAMV     PIC X.                                          00001670
           02 MFAMO     PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MLFAMA    PIC X.                                          00001700
           02 MLFAMC    PIC X.                                          00001710
           02 MLFAMP    PIC X.                                          00001720
           02 MLFAMH    PIC X.                                          00001730
           02 MLFAMV    PIC X.                                          00001740
           02 MLFAMO    PIC X(20).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MSENSAPPA      PIC X.                                     00001770
           02 MSENSAPPC PIC X.                                          00001780
           02 MSENSAPPP PIC X.                                          00001790
           02 MSENSAPPH PIC X.                                          00001800
           02 MSENSAPPV PIC X.                                          00001810
           02 MSENSAPPO      PIC X.                                     00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MMARQUEA  PIC X.                                          00001840
           02 MMARQUEC  PIC X.                                          00001850
           02 MMARQUEP  PIC X.                                          00001860
           02 MMARQUEH  PIC X.                                          00001870
           02 MMARQUEV  PIC X.                                          00001880
           02 MMARQUEO  PIC X(5).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MLMARQUEA      PIC X.                                     00001910
           02 MLMARQUEC PIC X.                                          00001920
           02 MLMARQUEP PIC X.                                          00001930
           02 MLMARQUEH PIC X.                                          00001940
           02 MLMARQUEV PIC X.                                          00001950
           02 MLMARQUEO      PIC X(20).                                 00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MSENSVTEA      PIC X.                                     00001980
           02 MSENSVTEC PIC X.                                          00001990
           02 MSENSVTEP PIC X.                                          00002000
           02 MSENSVTEH PIC X.                                          00002010
           02 MSENSVTEV PIC X.                                          00002020
           02 MSENSVTEO      PIC X.                                     00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MWCODICA  PIC X.                                          00002050
           02 MWCODICC  PIC X.                                          00002060
           02 MWCODICP  PIC X.                                          00002070
           02 MWCODICH  PIC X.                                          00002080
           02 MWCODICV  PIC X.                                          00002090
           02 MWCODICO  PIC X(7).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MREPCDEA  PIC X.                                          00002120
           02 MREPCDEC  PIC X.                                          00002130
           02 MREPCDEP  PIC X.                                          00002140
           02 MREPCDEH  PIC X.                                          00002150
           02 MREPCDEV  PIC X.                                          00002160
           02 MREPCDEO  PIC X.                                          00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLIBERRA  PIC X.                                          00002190
           02 MLIBERRC  PIC X.                                          00002200
           02 MLIBERRP  PIC X.                                          00002210
           02 MLIBERRH  PIC X.                                          00002220
           02 MLIBERRV  PIC X.                                          00002230
           02 MLIBERRO  PIC X(78).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCODTRAA  PIC X.                                          00002260
           02 MCODTRAC  PIC X.                                          00002270
           02 MCODTRAP  PIC X.                                          00002280
           02 MCODTRAH  PIC X.                                          00002290
           02 MCODTRAV  PIC X.                                          00002300
           02 MCODTRAO  PIC X(4).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCICSA    PIC X.                                          00002330
           02 MCICSC    PIC X.                                          00002340
           02 MCICSP    PIC X.                                          00002350
           02 MCICSH    PIC X.                                          00002360
           02 MCICSV    PIC X.                                          00002370
           02 MCICSO    PIC X(5).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MNETNAMA  PIC X.                                          00002400
           02 MNETNAMC  PIC X.                                          00002410
           02 MNETNAMP  PIC X.                                          00002420
           02 MNETNAMH  PIC X.                                          00002430
           02 MNETNAMV  PIC X.                                          00002440
           02 MNETNAMO  PIC X(8).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MSCREENA  PIC X.                                          00002470
           02 MSCREENC  PIC X.                                          00002480
           02 MSCREENP  PIC X.                                          00002490
           02 MSCREENH  PIC X.                                          00002500
           02 MSCREENV  PIC X.                                          00002510
           02 MSCREENO  PIC X(4).                                       00002520
                                                                                
