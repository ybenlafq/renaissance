      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM48   D4497  Maquette                                    00000020
      ***************************************************************** 00000030
       01   ERM48I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCIETEI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCGROUPI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMPL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCFAMPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMPF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCFAMPI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRTYPREGL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MRTYPREGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MRTYPREGF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MRTYPREGI      PIC X(2).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRGSTDL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MRGSTDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRGSTDF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MRGSTDI   PIC X(10).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRGMANUL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MRGMANUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MRGMANUF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MRGMANUI  PIC X(10).                                      00000530
           02 MLIGNEI OCCURS   13 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCFAMI  PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLFAMI  PIC X(20).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPREGL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MTYPREGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPREGF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MTYPREGI     PIC X(2).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MGSTDL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MGSTDL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MGSTDF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MGSTDI  PIC X(10).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MGMANUL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MGMANUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MGMANUF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MGMANUI      PIC X(10).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXPOAL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCEXPOAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCEXPOAF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCEXPOAI     PIC X(10).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINITL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MINITL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MINITF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MINITI  PIC X.                                          00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MZONCMDI  PIC X(15).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(58).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: ERM48   D4497  Maquette                                    00001080
      ***************************************************************** 00001090
       01   ERM48O REDEFINES ERM48I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MPAGEA    PIC X.                                          00001270
           02 MPAGEC    PIC X.                                          00001280
           02 MPAGEP    PIC X.                                          00001290
           02 MPAGEH    PIC X.                                          00001300
           02 MPAGEV    PIC X.                                          00001310
           02 MPAGEO    PIC X(3).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MPAGEMAXA      PIC X.                                     00001340
           02 MPAGEMAXC PIC X.                                          00001350
           02 MPAGEMAXP PIC X.                                          00001360
           02 MPAGEMAXH PIC X.                                          00001370
           02 MPAGEMAXV PIC X.                                          00001380
           02 MPAGEMAXO      PIC X(3).                                  00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNSOCIETEA     PIC X.                                     00001410
           02 MNSOCIETEC     PIC X.                                     00001420
           02 MNSOCIETEP     PIC X.                                     00001430
           02 MNSOCIETEH     PIC X.                                     00001440
           02 MNSOCIETEV     PIC X.                                     00001450
           02 MNSOCIETEO     PIC X(3).                                  00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MNLIEUA   PIC X.                                          00001480
           02 MNLIEUC   PIC X.                                          00001490
           02 MNLIEUP   PIC X.                                          00001500
           02 MNLIEUH   PIC X.                                          00001510
           02 MNLIEUV   PIC X.                                          00001520
           02 MNLIEUO   PIC X(3).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MLLIEUA   PIC X.                                          00001550
           02 MLLIEUC   PIC X.                                          00001560
           02 MLLIEUP   PIC X.                                          00001570
           02 MLLIEUH   PIC X.                                          00001580
           02 MLLIEUV   PIC X.                                          00001590
           02 MLLIEUO   PIC X(20).                                      00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MCGROUPA  PIC X.                                          00001620
           02 MCGROUPC  PIC X.                                          00001630
           02 MCGROUPP  PIC X.                                          00001640
           02 MCGROUPH  PIC X.                                          00001650
           02 MCGROUPV  PIC X.                                          00001660
           02 MCGROUPO  PIC X(5).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MCFAMPA   PIC X.                                          00001690
           02 MCFAMPC   PIC X.                                          00001700
           02 MCFAMPP   PIC X.                                          00001710
           02 MCFAMPH   PIC X.                                          00001720
           02 MCFAMPV   PIC X.                                          00001730
           02 MCFAMPO   PIC X(5).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MRTYPREGA      PIC X.                                     00001760
           02 MRTYPREGC PIC X.                                          00001770
           02 MRTYPREGP PIC X.                                          00001780
           02 MRTYPREGH PIC X.                                          00001790
           02 MRTYPREGV PIC X.                                          00001800
           02 MRTYPREGO      PIC X(2).                                  00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MRGSTDA   PIC X.                                          00001830
           02 MRGSTDC   PIC X.                                          00001840
           02 MRGSTDP   PIC X.                                          00001850
           02 MRGSTDH   PIC X.                                          00001860
           02 MRGSTDV   PIC X.                                          00001870
           02 MRGSTDO   PIC X(10).                                      00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MRGMANUA  PIC X.                                          00001900
           02 MRGMANUC  PIC X.                                          00001910
           02 MRGMANUP  PIC X.                                          00001920
           02 MRGMANUH  PIC X.                                          00001930
           02 MRGMANUV  PIC X.                                          00001940
           02 MRGMANUO  PIC X(10).                                      00001950
           02 MLIGNEO OCCURS   13 TIMES .                               00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MCFAMA  PIC X.                                          00001980
             03 MCFAMC  PIC X.                                          00001990
             03 MCFAMP  PIC X.                                          00002000
             03 MCFAMH  PIC X.                                          00002010
             03 MCFAMV  PIC X.                                          00002020
             03 MCFAMO  PIC X(5).                                       00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MLFAMA  PIC X.                                          00002050
             03 MLFAMC  PIC X.                                          00002060
             03 MLFAMP  PIC X.                                          00002070
             03 MLFAMH  PIC X.                                          00002080
             03 MLFAMV  PIC X.                                          00002090
             03 MLFAMO  PIC X(20).                                      00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MTYPREGA     PIC X.                                     00002120
             03 MTYPREGC     PIC X.                                     00002130
             03 MTYPREGP     PIC X.                                     00002140
             03 MTYPREGH     PIC X.                                     00002150
             03 MTYPREGV     PIC X.                                     00002160
             03 MTYPREGO     PIC X(2).                                  00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MGSTDA  PIC X.                                          00002190
             03 MGSTDC  PIC X.                                          00002200
             03 MGSTDP  PIC X.                                          00002210
             03 MGSTDH  PIC X.                                          00002220
             03 MGSTDV  PIC X.                                          00002230
             03 MGSTDO  PIC X(10).                                      00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MGMANUA      PIC X.                                     00002260
             03 MGMANUC PIC X.                                          00002270
             03 MGMANUP PIC X.                                          00002280
             03 MGMANUH PIC X.                                          00002290
             03 MGMANUV PIC X.                                          00002300
             03 MGMANUO      PIC X(10).                                 00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MCEXPOAA     PIC X.                                     00002330
             03 MCEXPOAC     PIC X.                                     00002340
             03 MCEXPOAP     PIC X.                                     00002350
             03 MCEXPOAH     PIC X.                                     00002360
             03 MCEXPOAV     PIC X.                                     00002370
             03 MCEXPOAO     PIC X(10).                                 00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MINITA  PIC X.                                          00002400
             03 MINITC  PIC X.                                          00002410
             03 MINITP  PIC X.                                          00002420
             03 MINITH  PIC X.                                          00002430
             03 MINITV  PIC X.                                          00002440
             03 MINITO  PIC X.                                          00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MZONCMDA  PIC X.                                          00002470
           02 MZONCMDC  PIC X.                                          00002480
           02 MZONCMDP  PIC X.                                          00002490
           02 MZONCMDH  PIC X.                                          00002500
           02 MZONCMDV  PIC X.                                          00002510
           02 MZONCMDO  PIC X(15).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(58).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
