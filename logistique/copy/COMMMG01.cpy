      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  COMM-MG01-APPLI.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-MG01-CODRET       COMP     PIC  S9(4).                      
      *--                                                                       
           05  COMM-MG01-CODRET COMP-5     PIC  S9(4).                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-MG01-TOTAL        COMP     PIC  S9(4).                      
      *--                                                                       
           05  COMM-MG01-TOTAL COMP-5     PIC  S9(4).                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-MG01-RAYON-MAX    COMP     PIC  S9(4).                      
      *--                                                                       
           05  COMM-MG01-RAYON-MAX COMP-5     PIC  S9(4).                       
      *}                                                                        
           05  COMM-MG01-RAYON   OCCURS  800   PIC  X(5).                       
           05  COMM-MG01-FAMILLE.                                               
               10 COMM-MG01-FAM OCCURS  800    PIC  X(5).                       
           05  FILLER                          PIC  X(90).                      
                                                                                
