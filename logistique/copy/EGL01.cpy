      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGL01   EGL01                                              00000020
      ***************************************************************** 00000030
       01   EGL01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGGL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGGF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGGI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRECL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCRECF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCRECI    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRECL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLRECF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLRECI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGDL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MWPAGDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGDF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MWPAGDI   PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIVRL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIVRF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNLIVRI   PIC X(8).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJOURL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDJOURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDJOURF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDJOURI   PIC X(10).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDHEUREL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDHEUREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDHEUREF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDHEUREI  PIC X(2).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMINUTL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MDMINUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDMINUTF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDMINUTI  PIC X(2).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNSOCI    PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNDEPOTI  PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPALETL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MWPALETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWPALETF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MWPALETI  PIC X.                                          00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIVRL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIVRF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCLIVRI   PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIVRL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIVRF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLLIVRI   PIC X(20).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMML   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLCOMML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCOMMF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLCOMMI   PIC X(20).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRANSL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRANSF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCTRANSI  PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRANSL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRANSF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLTRANSI  PIC X(20).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MQNBPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQNBPF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MQNBPI    PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTNBUOL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MQTNBUOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTNBUOF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MQTNBUOI  PIC X(4).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURL   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MNFOURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNFOURF   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNFOURI   PIC X(5).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFOURL   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MLFOURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFOURF   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLFOURI   PIC X(20).                                      00000970
           02 MNLIGNI OCCURS   10 TIMES .                               00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEGL      COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MNCDEGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNCDEGF      PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MNCDEGI      PIC X(7).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEGL  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MQTEGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTEGF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQTEGI  PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELGL      COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MWSELGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWSELGF      PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MWSELGI      PIC X.                                     00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELDL      COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MWSELDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWSELDF      PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MWSELDI      PIC X.                                     00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFOURDL     COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MNFOURDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNFOURDF     PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MNFOURDI     PIC X(5).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFOURDL     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MLFOURDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLFOURDF     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MLFOURDI     PIC X(20).                                 00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEDL      COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MNCDEDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNCDEDF      PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MNCDEDI      PIC X(7).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEDL  COMP PIC S9(4).                                 00001270
      *--                                                                       
             03 MQTEDL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTEDF  PIC X.                                          00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MQTEDI  PIC X(5).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBUODL     COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MQNBUODL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQNBUODF     PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MQNBUODI     PIC X(4).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MZONCMDI  PIC X(15).                                      00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MLIBERRI  PIC X(58).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MCODTRAI  PIC X(4).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCICSI    PIC X(5).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MNETNAMI  PIC X(8).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MSCREENI  PIC X(4).                                       00001580
      ***************************************************************** 00001590
      * SDF: EGL01   EGL01                                              00001600
      ***************************************************************** 00001610
       01   EGL01O REDEFINES EGL01I.                                    00001620
           02 FILLER    PIC X(12).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDATJOUA  PIC X.                                          00001650
           02 MDATJOUC  PIC X.                                          00001660
           02 MDATJOUP  PIC X.                                          00001670
           02 MDATJOUH  PIC X.                                          00001680
           02 MDATJOUV  PIC X.                                          00001690
           02 MDATJOUO  PIC X(10).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MTIMJOUA  PIC X.                                          00001720
           02 MTIMJOUC  PIC X.                                          00001730
           02 MTIMJOUP  PIC X.                                          00001740
           02 MTIMJOUH  PIC X.                                          00001750
           02 MTIMJOUV  PIC X.                                          00001760
           02 MTIMJOUO  PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MWPAGGA   PIC X.                                          00001790
           02 MWPAGGC   PIC X.                                          00001800
           02 MWPAGGP   PIC X.                                          00001810
           02 MWPAGGH   PIC X.                                          00001820
           02 MWPAGGV   PIC X.                                          00001830
           02 MWPAGGO   PIC X(3).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MWFONCA   PIC X.                                          00001860
           02 MWFONCC   PIC X.                                          00001870
           02 MWFONCP   PIC X.                                          00001880
           02 MWFONCH   PIC X.                                          00001890
           02 MWFONCV   PIC X.                                          00001900
           02 MWFONCO   PIC X(3).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCRECA    PIC X.                                          00001930
           02 MCRECC    PIC X.                                          00001940
           02 MCRECP    PIC X.                                          00001950
           02 MCRECH    PIC X.                                          00001960
           02 MCRECV    PIC X.                                          00001970
           02 MCRECO    PIC X(5).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MLRECA    PIC X.                                          00002000
           02 MLRECC    PIC X.                                          00002010
           02 MLRECP    PIC X.                                          00002020
           02 MLRECH    PIC X.                                          00002030
           02 MLRECV    PIC X.                                          00002040
           02 MLRECO    PIC X(20).                                      00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MWPAGDA   PIC X.                                          00002070
           02 MWPAGDC   PIC X.                                          00002080
           02 MWPAGDP   PIC X.                                          00002090
           02 MWPAGDH   PIC X.                                          00002100
           02 MWPAGDV   PIC X.                                          00002110
           02 MWPAGDO   PIC X(3).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MNLIVRA   PIC X.                                          00002140
           02 MNLIVRC   PIC X.                                          00002150
           02 MNLIVRP   PIC X.                                          00002160
           02 MNLIVRH   PIC X.                                          00002170
           02 MNLIVRV   PIC X.                                          00002180
           02 MNLIVRO   PIC X(8).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MDJOURA   PIC X.                                          00002210
           02 MDJOURC   PIC X.                                          00002220
           02 MDJOURP   PIC X.                                          00002230
           02 MDJOURH   PIC X.                                          00002240
           02 MDJOURV   PIC X.                                          00002250
           02 MDJOURO   PIC X(10).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MDHEUREA  PIC X.                                          00002280
           02 MDHEUREC  PIC X.                                          00002290
           02 MDHEUREP  PIC X.                                          00002300
           02 MDHEUREH  PIC X.                                          00002310
           02 MDHEUREV  PIC X.                                          00002320
           02 MDHEUREO  PIC X(2).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MDMINUTA  PIC X.                                          00002350
           02 MDMINUTC  PIC X.                                          00002360
           02 MDMINUTP  PIC X.                                          00002370
           02 MDMINUTH  PIC X.                                          00002380
           02 MDMINUTV  PIC X.                                          00002390
           02 MDMINUTO  PIC X(2).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNSOCA    PIC X.                                          00002420
           02 MNSOCC    PIC X.                                          00002430
           02 MNSOCP    PIC X.                                          00002440
           02 MNSOCH    PIC X.                                          00002450
           02 MNSOCV    PIC X.                                          00002460
           02 MNSOCO    PIC X(3).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MNDEPOTA  PIC X.                                          00002490
           02 MNDEPOTC  PIC X.                                          00002500
           02 MNDEPOTP  PIC X.                                          00002510
           02 MNDEPOTH  PIC X.                                          00002520
           02 MNDEPOTV  PIC X.                                          00002530
           02 MNDEPOTO  PIC X(3).                                       00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MWPALETA  PIC X.                                          00002560
           02 MWPALETC  PIC X.                                          00002570
           02 MWPALETP  PIC X.                                          00002580
           02 MWPALETH  PIC X.                                          00002590
           02 MWPALETV  PIC X.                                          00002600
           02 MWPALETO  PIC X.                                          00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MCLIVRA   PIC X.                                          00002630
           02 MCLIVRC   PIC X.                                          00002640
           02 MCLIVRP   PIC X.                                          00002650
           02 MCLIVRH   PIC X.                                          00002660
           02 MCLIVRV   PIC X.                                          00002670
           02 MCLIVRO   PIC X(5).                                       00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MLLIVRA   PIC X.                                          00002700
           02 MLLIVRC   PIC X.                                          00002710
           02 MLLIVRP   PIC X.                                          00002720
           02 MLLIVRH   PIC X.                                          00002730
           02 MLLIVRV   PIC X.                                          00002740
           02 MLLIVRO   PIC X(20).                                      00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MLCOMMA   PIC X.                                          00002770
           02 MLCOMMC   PIC X.                                          00002780
           02 MLCOMMP   PIC X.                                          00002790
           02 MLCOMMH   PIC X.                                          00002800
           02 MLCOMMV   PIC X.                                          00002810
           02 MLCOMMO   PIC X(20).                                      00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MCTRANSA  PIC X.                                          00002840
           02 MCTRANSC  PIC X.                                          00002850
           02 MCTRANSP  PIC X.                                          00002860
           02 MCTRANSH  PIC X.                                          00002870
           02 MCTRANSV  PIC X.                                          00002880
           02 MCTRANSO  PIC X(5).                                       00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MLTRANSA  PIC X.                                          00002910
           02 MLTRANSC  PIC X.                                          00002920
           02 MLTRANSP  PIC X.                                          00002930
           02 MLTRANSH  PIC X.                                          00002940
           02 MLTRANSV  PIC X.                                          00002950
           02 MLTRANSO  PIC X(20).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MQNBPA    PIC X.                                          00002980
           02 MQNBPC    PIC X.                                          00002990
           02 MQNBPP    PIC X.                                          00003000
           02 MQNBPH    PIC X.                                          00003010
           02 MQNBPV    PIC X.                                          00003020
           02 MQNBPO    PIC X(5).                                       00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MQTNBUOA  PIC X.                                          00003050
           02 MQTNBUOC  PIC X.                                          00003060
           02 MQTNBUOP  PIC X.                                          00003070
           02 MQTNBUOH  PIC X.                                          00003080
           02 MQTNBUOV  PIC X.                                          00003090
           02 MQTNBUOO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MNFOURA   PIC X.                                          00003120
           02 MNFOURC   PIC X.                                          00003130
           02 MNFOURP   PIC X.                                          00003140
           02 MNFOURH   PIC X.                                          00003150
           02 MNFOURV   PIC X.                                          00003160
           02 MNFOURO   PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MLFOURA   PIC X.                                          00003190
           02 MLFOURC   PIC X.                                          00003200
           02 MLFOURP   PIC X.                                          00003210
           02 MLFOURH   PIC X.                                          00003220
           02 MLFOURV   PIC X.                                          00003230
           02 MLFOURO   PIC X(20).                                      00003240
           02 MNLIGNO OCCURS   10 TIMES .                               00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MNCDEGA      PIC X.                                     00003270
             03 MNCDEGC PIC X.                                          00003280
             03 MNCDEGP PIC X.                                          00003290
             03 MNCDEGH PIC X.                                          00003300
             03 MNCDEGV PIC X.                                          00003310
             03 MNCDEGO      PIC X(7).                                  00003320
             03 FILLER       PIC X(2).                                  00003330
             03 MQTEGA  PIC X.                                          00003340
             03 MQTEGC  PIC X.                                          00003350
             03 MQTEGP  PIC X.                                          00003360
             03 MQTEGH  PIC X.                                          00003370
             03 MQTEGV  PIC X.                                          00003380
             03 MQTEGO  PIC X(5).                                       00003390
             03 FILLER       PIC X(2).                                  00003400
             03 MWSELGA      PIC X.                                     00003410
             03 MWSELGC PIC X.                                          00003420
             03 MWSELGP PIC X.                                          00003430
             03 MWSELGH PIC X.                                          00003440
             03 MWSELGV PIC X.                                          00003450
             03 MWSELGO      PIC X.                                     00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MWSELDA      PIC X.                                     00003480
             03 MWSELDC PIC X.                                          00003490
             03 MWSELDP PIC X.                                          00003500
             03 MWSELDH PIC X.                                          00003510
             03 MWSELDV PIC X.                                          00003520
             03 MWSELDO      PIC X.                                     00003530
             03 FILLER       PIC X(2).                                  00003540
             03 MNFOURDA     PIC X.                                     00003550
             03 MNFOURDC     PIC X.                                     00003560
             03 MNFOURDP     PIC X.                                     00003570
             03 MNFOURDH     PIC X.                                     00003580
             03 MNFOURDV     PIC X.                                     00003590
             03 MNFOURDO     PIC X(5).                                  00003600
             03 FILLER       PIC X(2).                                  00003610
             03 MLFOURDA     PIC X.                                     00003620
             03 MLFOURDC     PIC X.                                     00003630
             03 MLFOURDP     PIC X.                                     00003640
             03 MLFOURDH     PIC X.                                     00003650
             03 MLFOURDV     PIC X.                                     00003660
             03 MLFOURDO     PIC X(20).                                 00003670
             03 FILLER       PIC X(2).                                  00003680
             03 MNCDEDA      PIC X.                                     00003690
             03 MNCDEDC PIC X.                                          00003700
             03 MNCDEDP PIC X.                                          00003710
             03 MNCDEDH PIC X.                                          00003720
             03 MNCDEDV PIC X.                                          00003730
             03 MNCDEDO      PIC X(7).                                  00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MQTEDA  PIC X.                                          00003760
             03 MQTEDC  PIC X.                                          00003770
             03 MQTEDP  PIC X.                                          00003780
             03 MQTEDH  PIC X.                                          00003790
             03 MQTEDV  PIC X.                                          00003800
             03 MQTEDO  PIC X(5).                                       00003810
             03 FILLER       PIC X(2).                                  00003820
             03 MQNBUODA     PIC X.                                     00003830
             03 MQNBUODC     PIC X.                                     00003840
             03 MQNBUODP     PIC X.                                     00003850
             03 MQNBUODH     PIC X.                                     00003860
             03 MQNBUODV     PIC X.                                     00003870
             03 MQNBUODO     PIC X(4).                                  00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MZONCMDA  PIC X.                                          00003900
           02 MZONCMDC  PIC X.                                          00003910
           02 MZONCMDP  PIC X.                                          00003920
           02 MZONCMDH  PIC X.                                          00003930
           02 MZONCMDV  PIC X.                                          00003940
           02 MZONCMDO  PIC X(15).                                      00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MLIBERRA  PIC X.                                          00003970
           02 MLIBERRC  PIC X.                                          00003980
           02 MLIBERRP  PIC X.                                          00003990
           02 MLIBERRH  PIC X.                                          00004000
           02 MLIBERRV  PIC X.                                          00004010
           02 MLIBERRO  PIC X(58).                                      00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MCODTRAA  PIC X.                                          00004040
           02 MCODTRAC  PIC X.                                          00004050
           02 MCODTRAP  PIC X.                                          00004060
           02 MCODTRAH  PIC X.                                          00004070
           02 MCODTRAV  PIC X.                                          00004080
           02 MCODTRAO  PIC X(4).                                       00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MCICSA    PIC X.                                          00004110
           02 MCICSC    PIC X.                                          00004120
           02 MCICSP    PIC X.                                          00004130
           02 MCICSH    PIC X.                                          00004140
           02 MCICSV    PIC X.                                          00004150
           02 MCICSO    PIC X(5).                                       00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MNETNAMA  PIC X.                                          00004180
           02 MNETNAMC  PIC X.                                          00004190
           02 MNETNAMP  PIC X.                                          00004200
           02 MNETNAMH  PIC X.                                          00004210
           02 MNETNAMV  PIC X.                                          00004220
           02 MNETNAMO  PIC X(8).                                       00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MSCREENA  PIC X.                                          00004250
           02 MSCREENC  PIC X.                                          00004260
           02 MSCREENP  PIC X.                                          00004270
           02 MSCREENH  PIC X.                                          00004280
           02 MSCREENV  PIC X.                                          00004290
           02 MSCREENO  PIC X(4).                                       00004300
                                                                                
