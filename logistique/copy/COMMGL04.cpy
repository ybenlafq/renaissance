      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGL04                             *            
      *       TR : GL00  GESTION DES LIVRAISON                     *            
      *       PG : TGL04 CONSULTATION D'UNE LIVRAISON              *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ----------------------------  3724         
      *                                                                         
      *                                                                         
          02 COMM-GL04-APPLI REDEFINES COMM-GL00-APPLI.                         
      *----------------------------        CODE FONCTION                        
             03 COMM-GL04-WFONC          PIC X(3).                              
      *-------                           CHOIX                                  
             03 COMM-GL04-ZONCMD         PIC X(15).                             
      *-------                                                                  
             03 COMM-GL04-NLIVRAIS       PIC 9(7).                              
      *-------                                                                  
             03 COMM-GL04-NSOCIETE       PIC X(3).                              
      *-------                                                                  
             03 COMM-GL04-NDEPOT         PIC X(3).                              
      *-------                                                                  
             03 COMM-GL04-CREC           PIC X(5).                              
             03 COMM-GL04-LREC           PIC X(20).                             
      *                                                                         
      *-------                           EN SSAAMMJJ EN OEUF                    
             03 COMM-GL04-DATLIVRN       PIC 9(8).                              
      *                                                                         
      *-------                           EN SSAAMMJJ EN IXE                     
             03 COMM-GL04-DATLIVRX REDEFINES COMM-GL04-DATLIVRN                 
                                         PIC X(8).                              
      *                                                                         
      *-------                           EN JJ/MM/AA                            
             03 COMM-GL04-DATSLACH       PIC X(8).                              
      *                                                                         
      *-------                           EN JJ /MM / AA SEPAREE                 
             03 COMM-GL04-DATTE  REDEFINES COMM-GL04-DATSLACH.                  
                  05 JLIV                PIC XX.                                
                  05 SLIV1               PIC X.                                 
                  05 MLIV                PIC XX.                                
                  05 SLIV2               PIC X.                                 
                  05 ALIV                PIC XX.                                
      *                                                                         
      *-------                           ANNEE SEMAINE DE LIVRAISON             
             03 COMM-GL04-SEMAALV        PIC 99.                                
      *                                                                         
      *-------                           SEMAINE DE LIVRAISON                   
             03 COMM-GL04-SEMAINLV       PIC 99.                                
      *                                                                         
      *-------                           JOUR DE LA LIVRAISON                   
             03 COMM-GL04-JOURLIV        PIC 9.                                 
      *                                                                         
      *-------                                                                  
             03 COMM-GL04-HEURLIVR       PIC X(2).                              
      *-------                                                                  
             03 COMM-GL04-MINUTLIV       PIC X(2).                              
      *-------                                                                  
             03 COMM-GL04-WPALETIS       PIC X(1).                              
      *-------                                                                  
             03 COMM-GL04-CLIVREUR       PIC X(5).                              
      *-------                                                                  
             03 COMM-GL04-LLIVREUR       PIC X(20).                             
      *-------                                                                  
             03 COMM-GL04-COMMENT        PIC X(20).                             
      *-------                                                                  
             03 COMM-GL04-CTRANS         PIC X(5).                              
      *-------                                                                  
             03 COMM-GL04-LTRANS         PIC X(20).                             
      *-------                                                                  
             03 COMM-GL04-QNBP           PIC 9(5).                              
      *-------                                                                  
             03 COMM-GL04-QBUO           PIC 9(4).                              
      *-------                                                                  
             03 COMM-GL04-NFOUR          PIC X(5).                              
      *-------                                                                  
             03 COMM-GL04-LFOUR          PIC X(20).                             
      *-------                                                                  
             03 COMM-GL04-PAGDROIT       PIC 9(3).                              
      *-------                                                                  
             03 COMM-GL04-PAGD-MAX       PIC 9(3).                              
      *-------                                                                  
      *                                                                         
      *-------                                                                  
             03 LIGNES-DROITES.                                                 
                05 COMM-GL04-NFOUR-DATES OCCURS 10.                             
                   07 COMM-GL04-NENT     PIC X(5).                              
                   07 COMM-GL04-LENT     PIC X(20).                             
                   07 COMM-GL04-NCDED    PIC X(7).                              
                   07 COMM-GL04-QTED     PIC 9(5).                              
                   07 COMM-GL04-QUOD     PIC 9(4).                              
                   07 COMM-GL04-QPAL     PIC 9(4).                              
      *                                                                         
                                                                                
