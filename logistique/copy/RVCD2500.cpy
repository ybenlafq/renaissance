      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVCD2500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCD2500                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCD2500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCD2500.                                                            
      *}                                                                        
           02  CD25-NCODIC                                                      
               PIC X(0007).                                                     
           02  CD25-NMAG                                                        
               PIC X(0003).                                                     
           02  CD25-NCLASSE                                                     
               PIC X(0001).                                                     
           02  CD25-QSTOCKMAXPRO                                                
               PIC S9(5) COMP-3.                                                
           02  CD25-QSEUILMAG                                                   
               PIC S9(5) COMP-3.                                                
           02  CD25-QPIECES41                                                   
               PIC S9(5) COMP-3.                                                
           02  CD25-QPIECES85                                                   
               PIC S9(5) COMP-3.                                                
           02  CD25-QPIECES129                                                  
               PIC S9(5) COMP-3.                                                
           02  CD25-QPIECESCORR41                                               
               PIC S9(5) COMP-3.                                                
           02  CD25-QPIECESCORR85                                               
               PIC S9(5) COMP-3.                                                
           02  CD25-QPIECESCORR129                                              
               PIC S9(5) COMP-3.                                                
           02  CD25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVCD2500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCD2500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCD2500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-NCLASSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-NCLASSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-QSTOCKMAXPRO-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-QSTOCKMAXPRO-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-QSEUILMAG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-QSEUILMAG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-QPIECES41-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-QPIECES41-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-QPIECES85-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-QPIECES85-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-QPIECES129-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-QPIECES129-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-QPIECESCORR41-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-QPIECESCORR41-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-QPIECESCORR85-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-QPIECESCORR85-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-QPIECESCORR129-F                                            
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-QPIECESCORR129-F                                            
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
