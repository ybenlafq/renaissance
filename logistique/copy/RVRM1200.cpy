      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM1200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM1200                         
      *---------------------------------------------------------                
      *                                                                         
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVRM1200.                                                            
           02  RM12-CGROUP                                                      
               PIC X(0005).                                                     
           02  RM12-CFAM                                                        
               PIC X(0005).                                                     
           02  RM12-LAGREGATED                                                  
               PIC X(0020).                                                     
           02  RM12-W20                                                         
               PIC X(0001).                                                     
           02  RM12-W80                                                         
               PIC X(0001).                                                     
           02  RM12-WTXEMP                                                      
               PIC X(0001).                                                     
           02  RM12-QSEUILEXC                                                   
               PIC S9(7) COMP-3.                                                
           02  RM12-DEFFETD                                                     
               PIC X(0008).                                                     
           02  RM12-DFINEFFETD                                                  
               PIC X(0008).                                                     
           02  RM12-QINDISPO                                                    
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM12-QPONDERATION                                                
               PIC S9(1)V9(0002) COMP-3.                                        
           02  RM12-NINDICE                                                     
               PIC X(0001).                                                     
           02  RM12-QSEUIL8S                                                    
               PIC S9(7) COMP-3.                                                
           02  RM12-QPVM                                                        
               PIC S9(1)V9(0002) COMP-3.                                        
           02  RM12-QFREQUENCE                                                  
               PIC S9(1)V9(0002) COMP-3.                                        
           02  RM12-QEXPOMAX                                                    
               PIC S9(5) COMP-3.                                                
           02  RM12-QLSMAX                                                      
               PIC S9(5) COMP-3.                                                
           02  RM12-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM1200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-CGROUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-CGROUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-LAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-LAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-W20-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-W20-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-W80-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-W80-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-WTXEMP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-WTXEMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-QSEUILEXC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-QSEUILEXC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-DEFFETD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-DEFFETD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-DFINEFFETD-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-DFINEFFETD-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-QINDISPO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-QINDISPO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-QPONDERATION-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-QPONDERATION-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-NINDICE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-NINDICE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-QSEUIL8S-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-QSEUIL8S-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-QPVM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-QPVM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-QFREQUENCE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-QFREQUENCE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-QEXPOMAX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-QEXPOMAX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-QLSMAX-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-QLSMAX-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM12-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM12-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
           EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
