      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -------------------------------------------------------------- *00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      *                                                                 00100000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00110000
      * COMPRENANT :                                                    00120000
      * 1 - LES ZONES RESERVEES A AIDA                                  00130000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00140000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00150000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00160000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00170000
      *                                                                 00180000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00190000
      * PAR AIDA                                                        00200000
      *                                                                 00210000
      *-------------------------------------------------------------    00220000
      *                                                                 00230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-ZQ02-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00240001
      *--                                                                       
       01  COM-ZQ02-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
                                                                        00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
                                                                        00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00730000
           02 COMM-ZQ02-APPLICATIF      PIC X(3724).                            
      *                                 ----------------------------->          
      *                                 ACCES AUX OPTIONS DE FA00               
      *                                 ----------------------------->          
           02 COMM-FA00-MENU-R REDEFINES COMM-ZQ02-APPLICATIF.          00740000
              03 COMM-FA00-MENU.                                                
                 05 COMM-FA00-FONC           PIC X(3).                  00970003
                 05 COMM-FA00-NSOC           PIC X(3).                  00970004
                 05 COMM-FA00-LSOC           PIC X(20).                 00970005
                 05 COMM-FA00-DLIVRAISON     PIC X(8).                     00970
                 05 COMM-FA00-CPROTOUR       PIC X(5).                     00970
                 05 COMM-FA00-LCPROTOUR      PIC X(20).                    00970
                 05 COMM-FA00-CTOURNEE       PIC X(3).                     00970
                 05 COMM-FA00-MESS           PIC X(80).                    00970
                 05 COMM-FA00-IMPRIMANTE     PIC X(4).                     00970
                 05 COMM-FA00-TOP-SUP        PIC X.                        00970
                    88 SUPPRESSION-DEMANDEE  VALUE '1'.                    00970
                    88 SUPPRESSION-EXIGEE    VALUE '2'.                    00970
                 05 COMM-FA00-CLOGI          PIC X(5).                     00970
                 05 COMM-FA00-CTOURNEELG     PIC X(3).                     00970
              03 COMM-FA00-CONSTANTES.                                     00970
                 05 COMM-FA00-CDEVREF        PIC X(0003).                  00970
                 05 COMM-FA00-CDEVISE        PIC X(0003).                  00970
                 05 COMM-FA00-FILLER         PIC X(0094).                  00970
              03 COMM-FA00-APPLI.                                         009700
                 05 COMM-FA00-FILLER         PIC X(3469).                  00970
      *                                   ----------------------------->        
      *                                   ACCES AUX OPTIONS DE TL00             
      *                                   ----------------------------->        
           02 COMM-TL00-MENU-R REDEFINES COMM-ZQ02-APPLICATIF.          00740000
              03 COMM-TL00-MENU.                                                
                 05 COMM-TL00-FONC           PIC X(3).                          
                 05 COMM-TL00-NSOC           PIC X(3).                          
                 05 COMM-TL00-LSOC           PIC X(20).                         
                 05 COMM-TL00-DLIVRAISON     PIC X(8).                          
                 05 COMM-TL00-CPROTOUR       PIC X(5).                          
                 05 COMM-TL00-CTOURNEE       PIC X(3).                          
                 05 COMM-TL00-NLIEU          PIC X(3).                          
                 05 COMM-TL00-NVENTE         PIC X(7).                          
                 05 COMM-TL00-IMP            PIC X(4).                          
                 05 COMM-TL00-MESS           PIC X(80).                         
                 05 COMM-TL00-WRETOUR        PIC X(1).                          
                 05 COMM-TL00-ENCAISSEMENT   PIC X(1).                          
                 05 COMM-TL00-DEJA           PIC X(1).                          
                 05 COMM-TL00-GCT            PIC X(1).                          
                 05 COMM-TL00-CDEVREF        PIC X(3).                          
                 05 COMM-TL00-CDEVISE        PIC X(3).                          
                 05 COMM-TL00-CECART         PIC X(5).                          
             03 COMM-TL00-APPLI.                                                
                05 COMM-TL00-ZIMP      PIC X(4).                                
                05 COMM-TL04-SOCIETE   PIC X(03).                               
                05 COMM-TL04-CPROTOUR  PIC X(05).                               
                05 COMM-TL04-DDELIV    PIC X(08).                               
                05 COMM-TL00-CPROTOUR2 PIC X(05).                               
                05 COMM-TL05-CPROFI    PIC X(05).                               
                05 COMM-TL05-NBJ       PIC X(01).                               
                05 COMM-TL05-DATEN     PIC X(08).                               
                05 COMM-TL05-NBJM      PIC X(01).                               
                05 COMM-TL05-DATEM     PIC X(08).                               
                05 COMM-TL05-DOUBLE    PIC X(01).                               
                05 FILLER              PIC X(3451).                             
CR1812*                                   ----------------------------->        
      *                                   ACCES AUX OPTIONS DE PF10             
      *                                   ----------------------------->        
           02 COMM-PF10-APPLI-R REDEFINES COMM-ZQ02-APPLICATIF.         00740000
              03 COMM-PF10-APPLI.                                               
                 05 FILLER                   PIC X(1000).                       
                 05 COMM-PF10-NSOC           PIC X(3).                          
                 05 COMM-PF10-NLIEU          PIC X(3).                          
                 05 COMM-PF10-LSOC           PIC X(20).                         
                 05 COMM-PF10-DLIVRAISON     PIC X(8).                          
                 05 COMM-PF10-CPROTOUR       PIC X(5).                          
                 05 COMM-PF10-CTOURNEE       PIC X(3).                          
                 05 COMM-PF10-NVENTE         PIC X(7).                          
                 05 COMM-PF10-IMP            PIC X(4).                          
                 05 COMM-PF10-MESS           PIC X(80).                         
                 05 FILLER                   PIC X(2585).                       
                                                                                
