      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===============================================================* 00000010
      *                                                               * 00000020
      *      TS ASSOCIE A LA TRANSACTION TL05                         * 00000030
      *      CONTIENT LES RENSEIGNEMENTS POUR L'EDITION               * 00000040
      *      DE LA LISTE DE LIVRAISON                                 * 00000050
      *                                                               * 00000070
      *      NOM: 'TL04' + EIBTRMID                                   * 00000080
      *                                                               * 00000091
      *===============================================================* 00000092
                                                                        00000093
       01  TS-TL04.                                                     00000094
           02 TS-TL04-LONG              PIC S9(5) COMP-3     VALUE +15. 00000095
           02 TS-TL04-DONNEES.                                          00000096
              03  TS-TL04-LIEU          PIC X(03).                      00000098
              03  TS-TL04-VENTE         PIC X(07).                      00000099
              03  TS-TL04-CPOSTAL       PIC X(05).                      00000099
                                                                        00000120
      *===============================================================* 00000130
                                                                                
