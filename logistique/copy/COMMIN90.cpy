      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      *                                                                 00050000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-IN90-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.          00240001
      *--                                                                       
       01  COMM-IN90-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00730000
           02 COMM-IN90-APPLI.                                          00740000
              03 COMM-IN90-NSOCIETE       PIC X(3).                     00741002
              03 COMM-IN90-NENTREPOT      PIC X(3).                     00741002
              03 COMM-IN90-NSSLIEU        PIC X(3).                     00742002
              03 COMM-IN90-LTRT           PIC X(5).                     00742002
              03 COMM-IN90-MFONCT         PIC X(3).                     00742002
              03 COMM-IN90-DATEINV        PIC X(8).                     00742002
              03 COMM-IN90-TABDEP.                                              
                 05 COMM-IN90-NDEPOT         OCCURS 10 PIC X(3).                
              03 COMM-IN90-TABLE.                                               
                 05 COMM-IN90-TABSLIEU     OCCURS 25.                           
                    10 COMM-IN90-SSLIEU   PIC X(3).                             
                    10 COMM-IN90-TFIRST   PIC S999     COMP-3.                  
                    10 COMM-IN90-TLAST    PIC S999     COMP-3.                  
                 05 COMM-IN90-TRTDIF OCCURS 250.                                
                    10 COMM-IN90-LIEUTRT  PIC X(5).                             
                    10 COMM-IN90-NSOCDEP  PIC X(6).                             
              03 COMM-IN90-MAX-DEP        PIC S999     COMP-3.          00970003
              03 COMM-IN90-MAX-LIEU       PIC S999     COMP-3.          00970003
              03 COMM-IN90-MAX-TRT        PIC S999     COMP-3.          00970003
              03 COMM-IN90-ACCES          PIC X.                                
                 88 COMM-IN90-IE60        VALUE '1'.                            
                 88 COMM-IN90-IE05        VALUE '2'.                            
                 88 COMM-IN90-IE60-IE05   VALUE '3'.                            
              03 COMM-IN90-HSGES          PIC X.                                
                 88 COMM-IN90-HS-AU-CODIC  VALUE '1'.                           
                 88 COMM-IN90-HS-AU-HS     VALUE '2'.                           
                 88 COMM-IN90-HS-AU-BISEAU VALUE '3'.                           
              03 COMM-IN90-NLIEUINI       PIC X(3).                     00970003
              03 COMM-IN90-HSAPP-UNITE    PIC X.                                
              03 COMM-IN90-HSAPP-GESTION  PIC X.                                
              03 COMM-IN90-HSAPP-CAPPLI   PIC X(05).                            
              03 COMM-IN90-CREGROUP       PIC X.                        00970003
              03 FILLER                   PIC X(47).                    00970003
              03 COMM-IN90-FILLER         PIC X(676).                   00970003
              03 COMM-IN92-ZONES  REDEFINES COMM-IN90-FILLER.                   
                 05 COMM-IN92-PAGE-MAX    PIC 999.                              
                 05 COMM-IN92-PAGE        PIC 999.                              
                 05 COMM-IN92-FIRST-PAS   PIC X.                                
              03 COMM-IN91-ZONES  REDEFINES COMM-IN90-FILLER.                   
                 05 COMM-IN91-PAGE-SUP    PIC 999.                              
                 05 COMM-IN91-PAGE        PIC 999.                              
                 05 COMM-IN91-PAGC REDEFINES COMM-IN91-PAGE PIC X(3).           
                 05 COMM-IN91-CHGT-ENT    PIC X.                                
              03 COMM-IN93-ZONES  REDEFINES COMM-IN90-FILLER.                   
                 05 COMM-IN93-PASSAGE     PIC 9.                                
              03 COMM-IN94-ZONES  REDEFINES COMM-IN90-FILLER.                   
                 05 COMM-IN94-NLIEUENT    PIC X(3).                             
                 05 COMM-IN94-NHS         PIC X(7).                             
                 05 COMM-IN94-ACTION      PIC 9.                                
                    88 COMM-IN94-RIEN        VALUE 0.                           
                    88 COMM-IN94-MAJ         VALUE 1.                           
                    88 COMM-IN94-SOLDE       VALUE 2.                           
                    88 COMM-IN94-CREATION    VALUE 3.                           
                 05 COMM-IN94-PASSAGE     PIC 9.                                
              03 COMM-IV55-ZONES  REDEFINES COMM-IN90-FILLER.                   
                 05 COMM-IV55-CDRET       PIC X(4).                             
                 05 COMM-IV55-MESS        PIC X(66).                            
              03 COMM-IV56-ZONES  REDEFINES COMM-IN90-FILLER.                   
                 05 COMM-IV56-CDRET       PIC X(4).                             
                 05 COMM-IV56-MESS        PIC X(66).                            
                                                                                
