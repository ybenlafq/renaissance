      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLG0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLG0100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLG0100.                                                            
           02  LG01-CINSEE                                                      
               PIC X(0005).                                                     
           02  LG01-NVOIE                                                       
               PIC X(0004).                                                     
           02  LG01-CPARITE                                                     
               PIC X(0001).                                                     
           02  LG01-NDEBTR                                                      
               PIC X(0004).                                                     
           02  LG01-NFINTR                                                      
               PIC X(0004).                                                     
           02  LG01-CILOT                                                       
               PIC X(0008).                                                     
           02  LG01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLG0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLG0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG01-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG01-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG01-NVOIE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG01-NVOIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG01-CPARITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG01-CPARITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG01-NDEBTR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG01-NDEBTR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG01-NFINTR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG01-NFINTR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG01-CILOT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG01-CILOT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
