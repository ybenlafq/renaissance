      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM22   ERM22                                              00000020
      ***************************************************************** 00000030
       01   ERM67I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CRMGROUPL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 CRMGROUPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 CRMGROUPF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 CRMGROUPI      PIC X(5).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGESL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNPAGESL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNPAGESF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNPAGESI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFGL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCHEFGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFGF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCHEFGI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFGL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLCHEFGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCHEFGF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCHEFGI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAGREGL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLAGREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAGREGF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLAGREGI  PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEBSEM1L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDEBSEM1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEBSEM1F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDEBSEM1I      PIC X(10).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFINSEM1L      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MFINSEM1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFINSEM1F      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MFINSEM1I      PIC X(10).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEBSEM2L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MDEBSEM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEBSEM2F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDEBSEM2I      PIC X(10).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFINSEM2L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MFINSEM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFINSEM2F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MFINSEM2I      PIC X(10).                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEBSEM3L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MDEBSEM3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEBSEM3F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDEBSEM3I      PIC X(10).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFINSEM3L      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MFINSEM3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFINSEM3F      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MFINSEM3I      PIC X(10).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNCODICI  PIC X(7).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLREFI    PIC X(20).                                      00000770
           02 MCGROUPD OCCURS   10 TIMES .                              00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGROUPL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCGROUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCGROUPF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCGROUPI     PIC X(5).                                  00000820
           02 MQPVN1D OCCURS   10 TIMES .                               00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVN1L      COMP PIC S9(4).                            00000840
      *--                                                                       
             03 MQPVN1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPVN1F      PIC X.                                     00000850
             03 FILLER  PIC X(4).                                       00000860
             03 MQPVN1I      PIC X(6).                                  00000870
           02 MQPVRM2D OCCURS   10 TIMES .                              00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVRM2L     COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MQPVRM2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQPVRM2F     PIC X.                                     00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MQPVRM2I     PIC X(6).                                  00000920
           02 MQPVN2D OCCURS   10 TIMES .                               00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVN2L      COMP PIC S9(4).                            00000940
      *--                                                                       
             03 MQPVN2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPVN2F      PIC X.                                     00000950
             03 FILLER  PIC X(4).                                       00000960
             03 MQPVN2I      PIC X(6).                                  00000970
           02 MQPVA3D OCCURS   10 TIMES .                               00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVA3L      COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MQPVA3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPVA3F      PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQPVA3I      PIC X(6).                                  00001020
           02 MQPVRM3D OCCURS   10 TIMES .                              00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVRM3L     COMP PIC S9(4).                            00001040
      *--                                                                       
             03 MQPVRM3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQPVRM3F     PIC X.                                     00001050
             03 FILLER  PIC X(4).                                       00001060
             03 MQPVRM3I     PIC X(6).                                  00001070
           02 MQPVN3D OCCURS   10 TIMES .                               00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVN3L      COMP PIC S9(4).                            00001090
      *--                                                                       
             03 MQPVN3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPVN3F      PIC X.                                     00001100
             03 FILLER  PIC X(4).                                       00001110
             03 MQPVN3I      PIC X(6).                                  00001120
           02 MQPVA1D OCCURS   10 TIMES .                               00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVA1L      COMP PIC S9(4).                            00001140
      *--                                                                       
             03 MQPVA1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPVA1F      PIC X.                                     00001150
             03 FILLER  PIC X(4).                                       00001160
             03 MQPVA1I      PIC X(6).                                  00001170
           02 MQPVA2D OCCURS   10 TIMES .                               00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVA2L      COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MQPVA2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPVA2F      PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MQPVA2I      PIC X(6).                                  00001220
           02 MQPVRM1D OCCURS   10 TIMES .                              00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVRM1L     COMP PIC S9(4).                            00001240
      *--                                                                       
             03 MQPVRM1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQPVRM1F     PIC X.                                     00001250
             03 FILLER  PIC X(4).                                       00001260
             03 MQPVRM1I     PIC X(6).                                  00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MLIBERRI  PIC X(78).                                      00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MCODTRAI  PIC X(4).                                       00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MCICSI    PIC X(5).                                       00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001400
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001410
           02 FILLER    PIC X(4).                                       00001420
           02 MNETNAMI  PIC X(8).                                       00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001440
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001450
           02 FILLER    PIC X(4).                                       00001460
           02 MSCREENI  PIC X(4).                                       00001470
      ***************************************************************** 00001480
      * SDF: ERM22   ERM22                                              00001490
      ***************************************************************** 00001500
       01   ERM67O REDEFINES ERM67I.                                    00001510
           02 FILLER    PIC X(12).                                      00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MDATJOUA  PIC X.                                          00001540
           02 MDATJOUC  PIC X.                                          00001550
           02 MDATJOUP  PIC X.                                          00001560
           02 MDATJOUH  PIC X.                                          00001570
           02 MDATJOUV  PIC X.                                          00001580
           02 MDATJOUO  PIC X(10).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MTIMJOUA  PIC X.                                          00001610
           02 MTIMJOUC  PIC X.                                          00001620
           02 MTIMJOUP  PIC X.                                          00001630
           02 MTIMJOUH  PIC X.                                          00001640
           02 MTIMJOUV  PIC X.                                          00001650
           02 MTIMJOUO  PIC X(5).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 CRMGROUPA      PIC X.                                     00001680
           02 CRMGROUPC PIC X.                                          00001690
           02 CRMGROUPP PIC X.                                          00001700
           02 CRMGROUPH PIC X.                                          00001710
           02 CRMGROUPV PIC X.                                          00001720
           02 CRMGROUPO      PIC X(5).                                  00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MNPAGEA   PIC X.                                          00001750
           02 MNPAGEC   PIC X.                                          00001760
           02 MNPAGEP   PIC X.                                          00001770
           02 MNPAGEH   PIC X.                                          00001780
           02 MNPAGEV   PIC X.                                          00001790
           02 MNPAGEO   PIC X(3).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNPAGESA  PIC X.                                          00001820
           02 MNPAGESC  PIC X.                                          00001830
           02 MNPAGESP  PIC X.                                          00001840
           02 MNPAGESH  PIC X.                                          00001850
           02 MNPAGESV  PIC X.                                          00001860
           02 MNPAGESO  PIC X(3).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MCHEFGA   PIC X.                                          00001890
           02 MCHEFGC   PIC X.                                          00001900
           02 MCHEFGP   PIC X.                                          00001910
           02 MCHEFGH   PIC X.                                          00001920
           02 MCHEFGV   PIC X.                                          00001930
           02 MCHEFGO   PIC X(5).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MLCHEFGA  PIC X.                                          00001960
           02 MLCHEFGC  PIC X.                                          00001970
           02 MLCHEFGP  PIC X.                                          00001980
           02 MLCHEFGH  PIC X.                                          00001990
           02 MLCHEFGV  PIC X.                                          00002000
           02 MLCHEFGO  PIC X(20).                                      00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MCFAMA    PIC X.                                          00002030
           02 MCFAMC    PIC X.                                          00002040
           02 MCFAMP    PIC X.                                          00002050
           02 MCFAMH    PIC X.                                          00002060
           02 MCFAMV    PIC X.                                          00002070
           02 MCFAMO    PIC X(5).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLFAMA    PIC X.                                          00002100
           02 MLFAMC    PIC X.                                          00002110
           02 MLFAMP    PIC X.                                          00002120
           02 MLFAMH    PIC X.                                          00002130
           02 MLFAMV    PIC X.                                          00002140
           02 MLFAMO    PIC X(20).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MLAGREGA  PIC X.                                          00002170
           02 MLAGREGC  PIC X.                                          00002180
           02 MLAGREGP  PIC X.                                          00002190
           02 MLAGREGH  PIC X.                                          00002200
           02 MLAGREGV  PIC X.                                          00002210
           02 MLAGREGO  PIC X(20).                                      00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MDEBSEM1A      PIC X.                                     00002240
           02 MDEBSEM1C PIC X.                                          00002250
           02 MDEBSEM1P PIC X.                                          00002260
           02 MDEBSEM1H PIC X.                                          00002270
           02 MDEBSEM1V PIC X.                                          00002280
           02 MDEBSEM1O      PIC X(10).                                 00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MFINSEM1A      PIC X.                                     00002310
           02 MFINSEM1C PIC X.                                          00002320
           02 MFINSEM1P PIC X.                                          00002330
           02 MFINSEM1H PIC X.                                          00002340
           02 MFINSEM1V PIC X.                                          00002350
           02 MFINSEM1O      PIC X(10).                                 00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MDEBSEM2A      PIC X.                                     00002380
           02 MDEBSEM2C PIC X.                                          00002390
           02 MDEBSEM2P PIC X.                                          00002400
           02 MDEBSEM2H PIC X.                                          00002410
           02 MDEBSEM2V PIC X.                                          00002420
           02 MDEBSEM2O      PIC X(10).                                 00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MFINSEM2A      PIC X.                                     00002450
           02 MFINSEM2C PIC X.                                          00002460
           02 MFINSEM2P PIC X.                                          00002470
           02 MFINSEM2H PIC X.                                          00002480
           02 MFINSEM2V PIC X.                                          00002490
           02 MFINSEM2O      PIC X(10).                                 00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MDEBSEM3A      PIC X.                                     00002520
           02 MDEBSEM3C PIC X.                                          00002530
           02 MDEBSEM3P PIC X.                                          00002540
           02 MDEBSEM3H PIC X.                                          00002550
           02 MDEBSEM3V PIC X.                                          00002560
           02 MDEBSEM3O      PIC X(10).                                 00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MFINSEM3A      PIC X.                                     00002590
           02 MFINSEM3C PIC X.                                          00002600
           02 MFINSEM3P PIC X.                                          00002610
           02 MFINSEM3H PIC X.                                          00002620
           02 MFINSEM3V PIC X.                                          00002630
           02 MFINSEM3O      PIC X(10).                                 00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MNCODICA  PIC X.                                          00002660
           02 MNCODICC  PIC X.                                          00002670
           02 MNCODICP  PIC X.                                          00002680
           02 MNCODICH  PIC X.                                          00002690
           02 MNCODICV  PIC X.                                          00002700
           02 MNCODICO  PIC X(7).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MLREFA    PIC X.                                          00002730
           02 MLREFC    PIC X.                                          00002740
           02 MLREFP    PIC X.                                          00002750
           02 MLREFH    PIC X.                                          00002760
           02 MLREFV    PIC X.                                          00002770
           02 MLREFO    PIC X(20).                                      00002780
           02 DFHMS1 OCCURS   10 TIMES .                                00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MCGROUPA     PIC X.                                     00002810
             03 MCGROUPC     PIC X.                                     00002820
             03 MCGROUPP     PIC X.                                     00002830
             03 MCGROUPH     PIC X.                                     00002840
             03 MCGROUPV     PIC X.                                     00002850
             03 MCGROUPO     PIC X(5).                                  00002860
           02 DFHMS2 OCCURS   10 TIMES .                                00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MQPVN1A      PIC X.                                     00002890
             03 MQPVN1C PIC X.                                          00002900
             03 MQPVN1P PIC X.                                          00002910
             03 MQPVN1H PIC X.                                          00002920
             03 MQPVN1V PIC X.                                          00002930
             03 MQPVN1O      PIC X(6).                                  00002940
           02 DFHMS3 OCCURS   10 TIMES .                                00002950
             03 FILLER       PIC X(2).                                  00002960
             03 MQPVRM2A     PIC X.                                     00002970
             03 MQPVRM2C     PIC X.                                     00002980
             03 MQPVRM2P     PIC X.                                     00002990
             03 MQPVRM2H     PIC X.                                     00003000
             03 MQPVRM2V     PIC X.                                     00003010
             03 MQPVRM2O     PIC X(6).                                  00003020
           02 DFHMS4 OCCURS   10 TIMES .                                00003030
             03 FILLER       PIC X(2).                                  00003040
             03 MQPVN2A      PIC X.                                     00003050
             03 MQPVN2C PIC X.                                          00003060
             03 MQPVN2P PIC X.                                          00003070
             03 MQPVN2H PIC X.                                          00003080
             03 MQPVN2V PIC X.                                          00003090
             03 MQPVN2O      PIC X(6).                                  00003100
           02 DFHMS5 OCCURS   10 TIMES .                                00003110
             03 FILLER       PIC X(2).                                  00003120
             03 MQPVA3A      PIC X.                                     00003130
             03 MQPVA3C PIC X.                                          00003140
             03 MQPVA3P PIC X.                                          00003150
             03 MQPVA3H PIC X.                                          00003160
             03 MQPVA3V PIC X.                                          00003170
             03 MQPVA3O      PIC X(6).                                  00003180
           02 DFHMS6 OCCURS   10 TIMES .                                00003190
             03 FILLER       PIC X(2).                                  00003200
             03 MQPVRM3A     PIC X.                                     00003210
             03 MQPVRM3C     PIC X.                                     00003220
             03 MQPVRM3P     PIC X.                                     00003230
             03 MQPVRM3H     PIC X.                                     00003240
             03 MQPVRM3V     PIC X.                                     00003250
             03 MQPVRM3O     PIC X(6).                                  00003260
           02 DFHMS7 OCCURS   10 TIMES .                                00003270
             03 FILLER       PIC X(2).                                  00003280
             03 MQPVN3A      PIC X.                                     00003290
             03 MQPVN3C PIC X.                                          00003300
             03 MQPVN3P PIC X.                                          00003310
             03 MQPVN3H PIC X.                                          00003320
             03 MQPVN3V PIC X.                                          00003330
             03 MQPVN3O      PIC X(6).                                  00003340
           02 DFHMS8 OCCURS   10 TIMES .                                00003350
             03 FILLER       PIC X(2).                                  00003360
             03 MQPVA1A      PIC X.                                     00003370
             03 MQPVA1C PIC X.                                          00003380
             03 MQPVA1P PIC X.                                          00003390
             03 MQPVA1H PIC X.                                          00003400
             03 MQPVA1V PIC X.                                          00003410
             03 MQPVA1O      PIC ZZZZZZ.                                00003420
           02 DFHMS9 OCCURS   10 TIMES .                                00003430
             03 FILLER       PIC X(2).                                  00003440
             03 MQPVA2A      PIC X.                                     00003450
             03 MQPVA2C PIC X.                                          00003460
             03 MQPVA2P PIC X.                                          00003470
             03 MQPVA2H PIC X.                                          00003480
             03 MQPVA2V PIC X.                                          00003490
             03 MQPVA2O      PIC ZZZZZZ.                                00003500
           02 DFHMS10 OCCURS   10 TIMES .                               00003510
             03 FILLER       PIC X(2).                                  00003520
             03 MQPVRM1A     PIC X.                                     00003530
             03 MQPVRM1C     PIC X.                                     00003540
             03 MQPVRM1P     PIC X.                                     00003550
             03 MQPVRM1H     PIC X.                                     00003560
             03 MQPVRM1V     PIC X.                                     00003570
             03 MQPVRM1O     PIC ZZZZZZ.                                00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MLIBERRA  PIC X.                                          00003600
           02 MLIBERRC  PIC X.                                          00003610
           02 MLIBERRP  PIC X.                                          00003620
           02 MLIBERRH  PIC X.                                          00003630
           02 MLIBERRV  PIC X.                                          00003640
           02 MLIBERRO  PIC X(78).                                      00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MCODTRAA  PIC X.                                          00003670
           02 MCODTRAC  PIC X.                                          00003680
           02 MCODTRAP  PIC X.                                          00003690
           02 MCODTRAH  PIC X.                                          00003700
           02 MCODTRAV  PIC X.                                          00003710
           02 MCODTRAO  PIC X(4).                                       00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MCICSA    PIC X.                                          00003740
           02 MCICSC    PIC X.                                          00003750
           02 MCICSP    PIC X.                                          00003760
           02 MCICSH    PIC X.                                          00003770
           02 MCICSV    PIC X.                                          00003780
           02 MCICSO    PIC X(5).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MNETNAMA  PIC X.                                          00003810
           02 MNETNAMC  PIC X.                                          00003820
           02 MNETNAMP  PIC X.                                          00003830
           02 MNETNAMH  PIC X.                                          00003840
           02 MNETNAMV  PIC X.                                          00003850
           02 MNETNAMO  PIC X(8).                                       00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MSCREENA  PIC X.                                          00003880
           02 MSCREENC  PIC X.                                          00003890
           02 MSCREENP  PIC X.                                          00003900
           02 MSCREENH  PIC X.                                          00003910
           02 MSCREENV  PIC X.                                          00003920
           02 MSCREENO  PIC X(4).                                       00003930
                                                                                
