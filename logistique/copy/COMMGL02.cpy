      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGL02                             *            
      *       TR : GL00  GESTION DES LIVRAISON                     *            
      *       PG : TGL02 PROGRAMMATION DES LIVRAISONS              *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                                         
      *                                                                         
          02 COMM-GL02-APPLI REDEFINES COMM-GL00-APPLI.                         
      *------------------------------ CODE FONCTION                             
             03 COMM-GL02-WFONC          PIC X(3).                              
      *---------                         CHOIX                                  
             03 COMM-GL02-ZONCMD         PIC X(15).                             
      *---------                                                                
             03 COMM-GL02-NSOC           PIC 9(3).                              
      *---------                                                                
             03 COMM-GL02-NDEPOT         PIC 9(3).                              
      *---------                                                                
             03 COMM-GL02-TYPREC         OCCURS 5.                              
      *      03 COMM-GL02-CREC           PIC X(5).                              
      *      03 COMM-GL02-LREC           PIC X(25).                             
                04 COMM-GL02-CREC           PIC X(5).                           
                04 COMM-GL02-LREC           PIC X(25).                          
      *---------                                 EN  SSAANN                     
             03 COMM-GL02-SEMAIN         PIC X(6).                              
             03 COMM-GL02-SEMAIN1        PIC X(6).                              
      *-------                                   EN SSAAMMJJ                    
             03 COMM-GL02-JOURNEE        PIC X(08).                             
             03 COMM-GL02-JOURNEE1       PIC X(08).                             
      *-------                           NUMERO DE SEMAINE                      
             03 COMM-GL02-SEMNN          PIC XX.                                
      *-------                           JOUR DE LA SEMAINE                     
             03 COMM-GL02-NJOUR          PIC X.                                 
      *-------                                                                  
             03 COMM-GL02-PAGE           PIC 9(3).                              
             03 COMM-GL02-PAGETOT        PIC 9(3).                              
      *-------                                                                  
             03 COMM-GL02-CUMUL.                                                
               04 COMM-GL02-POSTE    OCCURS 8.                                  
                05 COMM-GL02-QUOTA       PIC S9(7) COMP-3.                      
                05 COMM-GL02-DISP        PIC S9(7) COMP-3.                      
                05 COMM-GL02-PRIS        PIC S9(7) COMP-3.                      
                05 COMM-GL02-NBP         PIC S9(7) COMP-3.                      
      *-------                                                                  
             03 COMM-GL06-SEMAIN         PIC X(6).                              
             03 COMM-GL06-PAGE           PIC 9(3).                              
             03 COMM-GL06-PAGETOT        PIC 9(3).                              
             03 COMM-GL02-NOMPROG        PIC X(6).                              
             03 COMM-GL02-CREC-MAX       PIC 9(2).                              
             03 COMM-GL02-CREC-CURRENT   PIC 9(2).                              
             03 COMM-GL02-CODPIC         PIC X(2).                              
             03 COMM-GL02-CODLANG        PIC X(2).                              
      *****************************************************************         
                                                                                
