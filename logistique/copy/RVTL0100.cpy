      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVTL0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVTL0100                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVTL0100.                                                            
           02  TL01-DDELIV                                                      
               PIC X(0008).                                                     
           02  TL01-NSOC                                                        
               PIC X(0003).                                                     
           02  TL01-CPROTOUR                                                    
               PIC X(0005).                                                     
           02  TL01-CTOURNEE                                                    
               PIC X(0003).                                                     
           02  TL01-CLIVR1                                                      
               PIC X(0010).                                                     
           02  TL01-CLIVR2                                                      
               PIC X(0010).                                                     
           02  TL01-CLIVR3                                                      
               PIC X(0010).                                                     
           02  TL01-NSATELLITE                                                  
               PIC X(0002).                                                     
           02  TL01-NCASE                                                       
               PIC X(0003).                                                     
           02  TL01-CCOMMENT                                                    
               PIC X(0005).                                                     
           02  TL01-NFOLIO                                                      
               PIC X(0002).                                                     
           02  TL01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVTL0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVTL0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CPROTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CTOURNEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CLIVR1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CLIVR1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CLIVR2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CLIVR2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CLIVR3-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CLIVR3-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-NSATELLITE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-NSATELLITE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-NCASE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-NCASE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-CCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-CCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-NFOLIO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-NFOLIO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
