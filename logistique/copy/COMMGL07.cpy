      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGL07                             *            
      *       TR : GL00  GESTION DES LIVRAISON                     *            
      *       PG : TGL07 CONSULTATION D'UNE LIVRAISON              *            
      **************************************************************            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-GL07-LONG-COMMAREA PIC S9(4) COMP VALUE +116.                    
      *--                                                                       
       01 COMM-GL07-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +116.                  
      *}                                                                        
       01 COMM-GL07-APPLI.                                                      
      *                                                                         
          03 COMM-GL07-NSOCIETE       PIC X(3).                                 
      *-------                                                                  
          03 COMM-GL07-NDEPOT         PIC X(3).                                 
      *-------                                                                  
          03 COMM-GL07-CREC           PIC X(5).                                 
      *-------                                                                  
          03 COMM-GL07-LREC           PIC X(20).                                
      *-------                                                                  
          03 COMM-GL07-JOURNEE        PIC X(8).                                 
      *-------                                                                  
          03 COMM-GL07-SEMAIN         PIC X(6).                                 
      *-------                                                                  
          03 COMM-GL07-MSG            PIC X(70).                                
      *---------------------------------------CODE RETOUR                       
          03 COMM-GL07-CODRET    PIC    X VALUE '0'.                            
             88 COMM-GL07-OK     VALUE '0'.                                     
             88 COMM-GL07-KO     VALUE '1'.                                     
      *                                                                         
                                                                                
