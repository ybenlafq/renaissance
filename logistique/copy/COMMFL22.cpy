      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TFL22 (TFL10 -> MENU)    TR: FL10  *    00002217
      *          GESTION DES PROFILS D'AFFILIATION                 *    00002311
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *  TRANSACTION FL22 : EXCEPTION DES MARQUES                  *    00411017
      *                                                                 00412000
          02 COMM-FL22-APPLI REDEFINES COMM-FL10-APPLI.                 00420017
      *------------------------------ ZONE DONNEES TFL22                00510017
             03 COMM-FL22-DONNEES-1-TFL22.                              00520017
      *------------------------------ CODE FONCTION                     00550001
                04 COMM-FL22-FONCT             PIC X(3).                00560017
      *------------------------------ CODE PROFIL                       00561001
                04 COMM-FL22-CPROAFF           PIC X(5).                00562017
      *------------------------------ LIBELLE PROFIL                    00570001
                04 COMM-FL22-LPROAFF           PIC X(20).               00580017
      *------------------------------ CODE FAMILLE                      00590018
                04 COMM-FL22-CEFAMIL           PIC X(5).                00600018
      *------------------------------ LIBELLE FAMILLE                   00610018
                04 COMM-FL22-LEFAMIL           PIC X(20).               00620018
      *------------------------------ DERNIER IND-TS                    00742401
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL22-PAGSUI            PIC S9(4) COMP.          00742517
      *--                                                                       
                04 COMM-FL22-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN IND-TS                     00742601
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL22-PAGENC            PIC S9(4) COMP.          00742717
      *--                                                                       
                04 COMM-FL22-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00742801
                04 COMM-FL22-NUMPAG            PIC 9(3).                00742917
      *------------------------------ NOMBRE DE PAGE                    00743013
                04 COMM-FL22-NBRPAG            PIC 9(3).                00743117
      *------------------------------ INDICATEUR FIN DE TABLE           00743201
                04 COMM-FL22-INDPAG            PIC 9.                   00743317
      *------------------------------ ZONE MODIFICATION TS              00743416
                04 COMM-FL22-MODIF             PIC 9(1).                00743517
      *------------------------------ ZONE CONFIRMATION PF4             00743601
                04 COMM-FL22-CONF-PF4          PIC 9(1).                00743717
      *------------------------------ OK MAJ SUR RTFL06 => VALID = '1'  00743811
                04 COMM-FL22-VALID-RTFL06   PIC  9.                     00743917
      *------------------------------ TABLE N� TS PR PAGINATION         00744201
                04 COMM-FL22-TABTS          OCCURS 100.                 00744317
      *------------------------------ 1ERS IND TS DES PAGES <=> FAMILLE 00744401
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               06 COMM-FL22-NUMTS          PIC S9(4) COMP.       00744517
      *--                                                                       
                      06 COMM-FL22-NUMTS          PIC S9(4) COMP-5.             
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS               00744601
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL22-IND-RECH-TS    PIC S9(4) COMP.             00745017
      *--                                                                       
                04 COMM-FL22-IND-RECH-TS    PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND TS MAX                        00746001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL22-IND-TS-MAX     PIC S9(4) COMP.             00747017
      *--                                                                       
                04 COMM-FL22-IND-TS-MAX     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ ZONE DONNEES 2 TFL22              00760017
             03 COMM-FL22-DONNEES-2-TFL22.                              00761017
      *------------------------------ CODES D'AFFILIATION PAR DEFAUT    00762001
                04 COMM-FL22-ENTAFF-DF      OCCURS 5.                   00763017
      *------------------------------ CODE SOCIETE D'AFFILIATION DEFAUT 00764001
                   05 COMM-FL22-CSOCAFF-DF  PIC X(03).                  00765017
      *------------------------------ CODE ENTREPOT D'AFFILIATION DEFAUT00766001
                   05 COMM-FL22-CDEPAFF-DF  PIC X(03).                  00767017
      *------------------------------ ZONE FAMILLES PAGE                00770001
                04 COMM-FL22-TABFAM.                                    00780017
      *------------------------------ ZONE LIGNES FAMILLES              00790001
                   05 COMM-FL22-LIGFAM  OCCURS 15.                      00800017
      *------------------------------ CODIC                             00801013
                      06 COMM-FL22-CDAC           PIC X(1).             00802017
      *------------------------------ CODIC                             00810011
                      06 COMM-FL22-CMAR           PIC X(5).             00820017
      *------------------------------ LIBELLE CODIC                     00830011
                      06 COMM-FL22-LMAR           PIC X(20).            00840017
      *------------------------------ CODES D'AFFILIATION               00850001
                      06 COMM-FL22-ENTAFF         OCCURS 5.             00860017
      *------------------------------ CODE SOCIETE D'AFFILIATION        00870001
                         07 COMM-FL22-CSOCAFF     PIC X(03).            00880017
      *------------------------------ CODE ENTREPOT D'AFFILIATION       00890001
                         07 COMM-FL22-CDEPAFF     PIC X(03).            00900017
      *------------------------------ ZONE DONNEES 3 TFL22              01650017
             03 COMM-FL22-DONNEES-3-TFL22.                              01651017
      *------------------------------ ZONES SWAP                        01670001
                   05 COMM-FL22-ATTR           PIC X OCCURS 500.        01680017
      *------------------------------ ZONE LIBRE                        01710001
             03 COMM-FL22-LIBRE          PIC X(2067).                   01720018
      ***************************************************************** 02170001
                                                                                
