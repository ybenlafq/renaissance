      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EZQ40   EZQ40                                              00000020
      ***************************************************************** 00000030
       01   EZQ40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * Option                                                          00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000150
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MZONCMDI  PIC X.                                          00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCTL  COMP PIC S9(4).                                 00000190
      *--                                                                       
           02 MCFONCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCFONCTF  PIC X.                                          00000200
           02 FILLER    PIC X(4).                                       00000210
           02 MCFONCTI  PIC X(3).                                       00000220
      * Numero societe PF                                               00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNSOCI    PIC X(3).                                       00000270
      * Nlieu PF                                                        00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000290
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MNLIEUI   PIC X(3).                                       00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEXCEPTL  COMP PIC S9(4).                                 00000330
      *--                                                                       
           02 MEXCEPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MEXCEPTF  PIC X.                                          00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MEXCEPTI  PIC X(7).                                       00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MCFAMI    PIC X(5).                                       00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARKL   COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MCMARKL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARKF   PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MCMARKI   PIC X(5).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVMARKL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MCVMARKL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCVMARKF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MCVMARKI  PIC X(5).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMLL   COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MCFAMLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMLF   PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MCFAMLI   PIC X(5).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARKLL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MCMARKLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARKLF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MCMARKLI  PIC X(5).                                       00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVMARKLL      COMP PIC S9(4).                            00000570
      *--                                                                       
           02 MCVMARKLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCVMARKLF      PIC X.                                     00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MCVMARKLI      PIC X(5).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MLIBERRI  PIC X(78).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MCODTRAI  PIC X(4).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCICSI    PIC X(5).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MNETNAMI  PIC X(8).                                       00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MSCREENI  PIC X(4).                                       00000800
      ***************************************************************** 00000810
      * SDF: EZQ40   EZQ40                                              00000820
      ***************************************************************** 00000830
       01   EZQ40O REDEFINES EZQ40I.                                    00000840
           02 FILLER    PIC X(12).                                      00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MDATJOUA  PIC X.                                          00000870
           02 MDATJOUC  PIC X.                                          00000880
           02 MDATJOUP  PIC X.                                          00000890
           02 MDATJOUH  PIC X.                                          00000900
           02 MDATJOUV  PIC X.                                          00000910
           02 MDATJOUO  PIC X(10).                                      00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MTIMJOUA  PIC X.                                          00000940
           02 MTIMJOUC  PIC X.                                          00000950
           02 MTIMJOUP  PIC X.                                          00000960
           02 MTIMJOUH  PIC X.                                          00000970
           02 MTIMJOUV  PIC X.                                          00000980
           02 MTIMJOUO  PIC X(5).                                       00000990
      * Option                                                          00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MZONCMDA  PIC X.                                          00001020
           02 MZONCMDC  PIC X.                                          00001030
           02 MZONCMDP  PIC X.                                          00001040
           02 MZONCMDH  PIC X.                                          00001050
           02 MZONCMDV  PIC X.                                          00001060
           02 MZONCMDO  PIC X.                                          00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MCFONCTA  PIC X.                                          00001090
           02 MCFONCTC  PIC X.                                          00001100
           02 MCFONCTP  PIC X.                                          00001110
           02 MCFONCTH  PIC X.                                          00001120
           02 MCFONCTV  PIC X.                                          00001130
           02 MCFONCTO  PIC X(3).                                       00001140
      * Numero societe PF                                               00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNSOCA    PIC X.                                          00001170
           02 MNSOCC    PIC X.                                          00001180
           02 MNSOCP    PIC X.                                          00001190
           02 MNSOCH    PIC X.                                          00001200
           02 MNSOCV    PIC X.                                          00001210
           02 MNSOCO    PIC X(3).                                       00001220
      * Nlieu PF                                                        00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNLIEUA   PIC X.                                          00001250
           02 MNLIEUC   PIC X.                                          00001260
           02 MNLIEUP   PIC X.                                          00001270
           02 MNLIEUH   PIC X.                                          00001280
           02 MNLIEUV   PIC X.                                          00001290
           02 MNLIEUO   PIC X(3).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MEXCEPTA  PIC X.                                          00001320
           02 MEXCEPTC  PIC X.                                          00001330
           02 MEXCEPTP  PIC X.                                          00001340
           02 MEXCEPTH  PIC X.                                          00001350
           02 MEXCEPTV  PIC X.                                          00001360
           02 MEXCEPTO  PIC X(7).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MCFAMA    PIC X.                                          00001390
           02 MCFAMC    PIC X.                                          00001400
           02 MCFAMP    PIC X.                                          00001410
           02 MCFAMH    PIC X.                                          00001420
           02 MCFAMV    PIC X.                                          00001430
           02 MCFAMO    PIC X(5).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCMARKA   PIC X.                                          00001460
           02 MCMARKC   PIC X.                                          00001470
           02 MCMARKP   PIC X.                                          00001480
           02 MCMARKH   PIC X.                                          00001490
           02 MCMARKV   PIC X.                                          00001500
           02 MCMARKO   PIC X(5).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MCVMARKA  PIC X.                                          00001530
           02 MCVMARKC  PIC X.                                          00001540
           02 MCVMARKP  PIC X.                                          00001550
           02 MCVMARKH  PIC X.                                          00001560
           02 MCVMARKV  PIC X.                                          00001570
           02 MCVMARKO  PIC X(5).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCFAMLA   PIC X.                                          00001600
           02 MCFAMLC   PIC X.                                          00001610
           02 MCFAMLP   PIC X.                                          00001620
           02 MCFAMLH   PIC X.                                          00001630
           02 MCFAMLV   PIC X.                                          00001640
           02 MCFAMLO   PIC X(5).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCMARKLA  PIC X.                                          00001670
           02 MCMARKLC  PIC X.                                          00001680
           02 MCMARKLP  PIC X.                                          00001690
           02 MCMARKLH  PIC X.                                          00001700
           02 MCMARKLV  PIC X.                                          00001710
           02 MCMARKLO  PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MCVMARKLA      PIC X.                                     00001740
           02 MCVMARKLC PIC X.                                          00001750
           02 MCVMARKLP PIC X.                                          00001760
           02 MCVMARKLH PIC X.                                          00001770
           02 MCVMARKLV PIC X.                                          00001780
           02 MCVMARKLO      PIC X(5).                                  00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLIBERRA  PIC X.                                          00001810
           02 MLIBERRC  PIC X.                                          00001820
           02 MLIBERRP  PIC X.                                          00001830
           02 MLIBERRH  PIC X.                                          00001840
           02 MLIBERRV  PIC X.                                          00001850
           02 MLIBERRO  PIC X(78).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCODTRAA  PIC X.                                          00001880
           02 MCODTRAC  PIC X.                                          00001890
           02 MCODTRAP  PIC X.                                          00001900
           02 MCODTRAH  PIC X.                                          00001910
           02 MCODTRAV  PIC X.                                          00001920
           02 MCODTRAO  PIC X(4).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCICSA    PIC X.                                          00001950
           02 MCICSC    PIC X.                                          00001960
           02 MCICSP    PIC X.                                          00001970
           02 MCICSH    PIC X.                                          00001980
           02 MCICSV    PIC X.                                          00001990
           02 MCICSO    PIC X(5).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNETNAMA  PIC X.                                          00002020
           02 MNETNAMC  PIC X.                                          00002030
           02 MNETNAMP  PIC X.                                          00002040
           02 MNETNAMH  PIC X.                                          00002050
           02 MNETNAMV  PIC X.                                          00002060
           02 MNETNAMO  PIC X(8).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MSCREENA  PIC X.                                          00002090
           02 MSCREENC  PIC X.                                          00002100
           02 MSCREENP  PIC X.                                          00002110
           02 MSCREENH  PIC X.                                          00002120
           02 MSCREENV  PIC X.                                          00002130
           02 MSCREENO  PIC X(4).                                       00002140
                                                                                
