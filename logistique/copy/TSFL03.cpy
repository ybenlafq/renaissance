      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      * TS SPECIFIQUE TFL03 / TFL04                                  *          
      ****************************************************************          
      *                                                                         
      *01  TS-LONG            PIC S9(4) COMP-3 VALUE 988.                       
       01  TS-LONG            PIC S9(4) COMP-3 VALUE 1001.                      
       01  TS-DATA.                                                             
        02 TS-LIGNE OCCURS 13.                                                  
           05  TS-CFAM          PIC X(5).                                       
           05  TS-LFAM          PIC X(20).                                      
           05  TS-QDELAIAPPRO   PIC X(3).                                       
           05  TS-QCOLICDE      PIC X(3).                                       
           05  TS-QCOLIRECEPT   PIC X(5).                                       
           05  TS-QCOLIDSTOCK   PIC X(5).                                       
           05  TS-CCOTEHOLD     PIC X(1).                                       
           05  TS-CZONEACCES    PIC X(1).                                       
           05  TS-CQUOTA        PIC X(5).                                       
           05  TS-QUOTAPAL      PIC X(1).                                       
           05  TS-CMODSTOCK     PIC X(5).                                       
           05  TS-QNBPRACK      PIC X(5).                                       
           05  TS-CCONTENEUR    PIC X(1).                                       
           05  TS-CSPECIFSTK    PIC X(1).                                       
           05  TS-QNBPVSOL      PIC X(3).                                       
           05  TS-QNBRANMAIL    PIC X(3).                                       
           05  TS-QNBNIVGERB    PIC X(3).                                       
           05  TS-CTPSUP        PIC X(1).                                       
           05  TS-FLAG          PIC X(3).                                       
           05  TS-TRT           PIC X(3).                                       
                                                                                
