      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EEF31   EEF31                                              00000020
      ***************************************************************** 00000030
       01   EEF31I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MPAGEMAXI      PIC X(3).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCTRAITI  PIC X(5).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MLTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRAITF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLTRAITI  PIC X(20).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTIERSI  PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLTIERSI  PIC X(20).                                      00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGLOBALL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MGLOBALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MGLOBALF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MGLOBALI  PIC X.                                          00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRENDUL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRENDUF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNRENDUI  PIC X(20).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRENDUL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MDRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRENDUF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MDRENDUI  PIC X(10).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMRENDUL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MMRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMRENDUF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MMRENDUI  PIC X(6).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLIBELLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBELLF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLIBELLI  PIC X(8).                                       00000590
           02 MAFFICHEI OCCURS   11 TIMES .                             00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTL     COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MSELECTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELECTF     PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MSELECTI     PIC X.                                     00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUHSL    COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MNLIEUHSL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNLIEUHSF    PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MNLIEUHSI    PIC X(3).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLOTHSL     COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MNLOTHSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLOTHSF     PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MNLOTHSI     PIC X(7).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MNCODICI     PIC X(7).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000770
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MCFAMI  PIC X(5).                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MCMARQI      PIC X(5).                                  00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000850
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MLREFI  PIC X(16).                                      00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENVOIL     COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MNENVOIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNENVOIF     PIC X.                                     00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MNENVOII     PIC X(7).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDENVOIL     COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MDENVOIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDENVOIF     PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MDENVOII     PIC X(8).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000970
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MQTEI   PIC X(10).                                      00001000
      * MESSAGE ERREUR                                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLIBERRI  PIC X(78).                                      00001050
      * CODE TRANSACTION                                                00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      * CICS DE TRAVAIL                                                 00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MCICSI    PIC X(5).                                       00001150
      * NETNAME                                                         00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MNETNAMI  PIC X(8).                                       00001200
      * CODE TERMINAL                                                   00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MSCREENI  PIC X(5).                                       00001250
      ***************************************************************** 00001260
      * SDF: EEF31   EEF31                                              00001270
      ***************************************************************** 00001280
       01   EEF31O REDEFINES EEF31I.                                    00001290
           02 FILLER    PIC X(12).                                      00001300
      * DATE DU JOUR                                                    00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
      * HEURE                                                           00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MTIMJOUA  PIC X.                                          00001410
           02 MTIMJOUC  PIC X.                                          00001420
           02 MTIMJOUP  PIC X.                                          00001430
           02 MTIMJOUH  PIC X.                                          00001440
           02 MTIMJOUV  PIC X.                                          00001450
           02 MTIMJOUO  PIC X(5).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MPAGEA    PIC X.                                          00001480
           02 MPAGEC    PIC X.                                          00001490
           02 MPAGEP    PIC X.                                          00001500
           02 MPAGEH    PIC X.                                          00001510
           02 MPAGEV    PIC X.                                          00001520
           02 MPAGEO    PIC 999.                                        00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MPAGEMAXA      PIC X.                                     00001550
           02 MPAGEMAXC PIC X.                                          00001560
           02 MPAGEMAXP PIC X.                                          00001570
           02 MPAGEMAXH PIC X.                                          00001580
           02 MPAGEMAXV PIC X.                                          00001590
           02 MPAGEMAXO      PIC 999.                                   00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MCTRAITA  PIC X.                                          00001620
           02 MCTRAITC  PIC X.                                          00001630
           02 MCTRAITP  PIC X.                                          00001640
           02 MCTRAITH  PIC X.                                          00001650
           02 MCTRAITV  PIC X.                                          00001660
           02 MCTRAITO  PIC X(5).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MLTRAITA  PIC X.                                          00001690
           02 MLTRAITC  PIC X.                                          00001700
           02 MLTRAITP  PIC X.                                          00001710
           02 MLTRAITH  PIC X.                                          00001720
           02 MLTRAITV  PIC X.                                          00001730
           02 MLTRAITO  PIC X(20).                                      00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MCTIERSA  PIC X.                                          00001760
           02 MCTIERSC  PIC X.                                          00001770
           02 MCTIERSP  PIC X.                                          00001780
           02 MCTIERSH  PIC X.                                          00001790
           02 MCTIERSV  PIC X.                                          00001800
           02 MCTIERSO  PIC X(5).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MLTIERSA  PIC X.                                          00001830
           02 MLTIERSC  PIC X.                                          00001840
           02 MLTIERSP  PIC X.                                          00001850
           02 MLTIERSH  PIC X.                                          00001860
           02 MLTIERSV  PIC X.                                          00001870
           02 MLTIERSO  PIC X(20).                                      00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MGLOBALA  PIC X.                                          00001900
           02 MGLOBALC  PIC X.                                          00001910
           02 MGLOBALP  PIC X.                                          00001920
           02 MGLOBALH  PIC X.                                          00001930
           02 MGLOBALV  PIC X.                                          00001940
           02 MGLOBALO  PIC X.                                          00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MNRENDUA  PIC X.                                          00001970
           02 MNRENDUC  PIC X.                                          00001980
           02 MNRENDUP  PIC X.                                          00001990
           02 MNRENDUH  PIC X.                                          00002000
           02 MNRENDUV  PIC X.                                          00002010
           02 MNRENDUO  PIC X(20).                                      00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MDRENDUA  PIC X.                                          00002040
           02 MDRENDUC  PIC X.                                          00002050
           02 MDRENDUP  PIC X.                                          00002060
           02 MDRENDUH  PIC X.                                          00002070
           02 MDRENDUV  PIC X.                                          00002080
           02 MDRENDUO  PIC X(10).                                      00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MMRENDUA  PIC X.                                          00002110
           02 MMRENDUC  PIC X.                                          00002120
           02 MMRENDUP  PIC X.                                          00002130
           02 MMRENDUH  PIC X.                                          00002140
           02 MMRENDUV  PIC X.                                          00002150
           02 MMRENDUO  PIC X(6).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MLIBELLA  PIC X.                                          00002180
           02 MLIBELLC  PIC X.                                          00002190
           02 MLIBELLP  PIC X.                                          00002200
           02 MLIBELLH  PIC X.                                          00002210
           02 MLIBELLV  PIC X.                                          00002220
           02 MLIBELLO  PIC X(8).                                       00002230
           02 MAFFICHEO OCCURS   11 TIMES .                             00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MSELECTA     PIC X.                                     00002260
             03 MSELECTC     PIC X.                                     00002270
             03 MSELECTP     PIC X.                                     00002280
             03 MSELECTH     PIC X.                                     00002290
             03 MSELECTV     PIC X.                                     00002300
             03 MSELECTO     PIC X.                                     00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MNLIEUHSA    PIC X.                                     00002330
             03 MNLIEUHSC    PIC X.                                     00002340
             03 MNLIEUHSP    PIC X.                                     00002350
             03 MNLIEUHSH    PIC X.                                     00002360
             03 MNLIEUHSV    PIC X.                                     00002370
             03 MNLIEUHSO    PIC X(3).                                  00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MNLOTHSA     PIC X.                                     00002400
             03 MNLOTHSC     PIC X.                                     00002410
             03 MNLOTHSP     PIC X.                                     00002420
             03 MNLOTHSH     PIC X.                                     00002430
             03 MNLOTHSV     PIC X.                                     00002440
             03 MNLOTHSO     PIC 9999999.                               00002450
             03 FILLER       PIC X(2).                                  00002460
             03 MNCODICA     PIC X.                                     00002470
             03 MNCODICC     PIC X.                                     00002480
             03 MNCODICP     PIC X.                                     00002490
             03 MNCODICH     PIC X.                                     00002500
             03 MNCODICV     PIC X.                                     00002510
             03 MNCODICO     PIC 9999999.                               00002520
             03 FILLER       PIC X(2).                                  00002530
             03 MCFAMA  PIC X.                                          00002540
             03 MCFAMC  PIC X.                                          00002550
             03 MCFAMP  PIC X.                                          00002560
             03 MCFAMH  PIC X.                                          00002570
             03 MCFAMV  PIC X.                                          00002580
             03 MCFAMO  PIC X(5).                                       00002590
             03 FILLER       PIC X(2).                                  00002600
             03 MCMARQA      PIC X.                                     00002610
             03 MCMARQC PIC X.                                          00002620
             03 MCMARQP PIC X.                                          00002630
             03 MCMARQH PIC X.                                          00002640
             03 MCMARQV PIC X.                                          00002650
             03 MCMARQO      PIC X(5).                                  00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MLREFA  PIC X.                                          00002680
             03 MLREFC  PIC X.                                          00002690
             03 MLREFP  PIC X.                                          00002700
             03 MLREFH  PIC X.                                          00002710
             03 MLREFV  PIC X.                                          00002720
             03 MLREFO  PIC X(16).                                      00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MNENVOIA     PIC X.                                     00002750
             03 MNENVOIC     PIC X.                                     00002760
             03 MNENVOIP     PIC X.                                     00002770
             03 MNENVOIH     PIC X.                                     00002780
             03 MNENVOIV     PIC X.                                     00002790
             03 MNENVOIO     PIC X(7).                                  00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MDENVOIA     PIC X.                                     00002820
             03 MDENVOIC     PIC X.                                     00002830
             03 MDENVOIP     PIC X.                                     00002840
             03 MDENVOIH     PIC X.                                     00002850
             03 MDENVOIV     PIC X.                                     00002860
             03 MDENVOIO     PIC X(8).                                  00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MQTEA   PIC X.                                          00002890
             03 MQTEC   PIC X.                                          00002900
             03 MQTEP   PIC X.                                          00002910
             03 MQTEH   PIC X.                                          00002920
             03 MQTEV   PIC X.                                          00002930
             03 MQTEO   PIC X(10).                                      00002940
      * MESSAGE ERREUR                                                  00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MLIBERRA  PIC X.                                          00002970
           02 MLIBERRC  PIC X.                                          00002980
           02 MLIBERRP  PIC X.                                          00002990
           02 MLIBERRH  PIC X.                                          00003000
           02 MLIBERRV  PIC X.                                          00003010
           02 MLIBERRO  PIC X(78).                                      00003020
      * CODE TRANSACTION                                                00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
      * CICS DE TRAVAIL                                                 00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MCICSA    PIC X.                                          00003130
           02 MCICSC    PIC X.                                          00003140
           02 MCICSP    PIC X.                                          00003150
           02 MCICSH    PIC X.                                          00003160
           02 MCICSV    PIC X.                                          00003170
           02 MCICSO    PIC X(5).                                       00003180
      * NETNAME                                                         00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MNETNAMA  PIC X.                                          00003210
           02 MNETNAMC  PIC X.                                          00003220
           02 MNETNAMP  PIC X.                                          00003230
           02 MNETNAMH  PIC X.                                          00003240
           02 MNETNAMV  PIC X.                                          00003250
           02 MNETNAMO  PIC X(8).                                       00003260
      * CODE TERMINAL                                                   00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MSCREENA  PIC X.                                          00003290
           02 MSCREENC  PIC X.                                          00003300
           02 MSCREENP  PIC X.                                          00003310
           02 MSCREENH  PIC X.                                          00003320
           02 MSCREENV  PIC X.                                          00003330
           02 MSCREENO  PIC X(5).                                       00003340
                                                                                
