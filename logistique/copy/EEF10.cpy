      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EEF10   EEF10                                              00000020
      ***************************************************************** 00000030
       01   EEF10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCTRAITI  PIC X(5).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MLTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRAITF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLTRAITI  PIC X(30).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLTIERSI  PIC X(15).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MCTIERSI  PIC X(5).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTIERSL     COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MLIBTIERSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBTIERSF     PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLIBTIERSI     PIC X(20).                                 00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENVOIL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MLENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENVOIF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLENVOII  PIC X(21).                                      00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MNENVOII  PIC X(7).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MDENVOII  PIC X(10).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCORDL      COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MNACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNACCORDF      PIC X.                                     00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MNACCORDI      PIC X(12).                                 00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACCORDL      COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MDACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDACCORDF      PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MDACCORDI      PIC X(10).                                 00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRECEDL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MPRECEDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPRECEDF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MPRECEDI  PIC X(17).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLACCORDL      COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MLACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLACCORDF      PIC X.                                     00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MLACCORDI      PIC X(10).                                 00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MNCODICI  PIC X(7).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICPL      COMP PIC S9(4).                            00000680
      *--                                                                       
           02 MNCODICPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICPF      PIC X.                                     00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MNCODICPI      PIC X(7).                                  00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHSL      COMP PIC S9(4).                            00000720
      *--                                                                       
           02 MNLIEUHSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHSF      PIC X.                                     00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MNLIEUHSI      PIC X(3).                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNIDENTIFL     COMP PIC S9(4).                            00000760
      *--                                                                       
           02 MNIDENTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNIDENTIFF     PIC X.                                     00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MNIDENTIFI     PIC X(7).                                  00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHSPL     COMP PIC S9(4).                            00000800
      *--                                                                       
           02 MNLIEUHSPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNLIEUHSPF     PIC X.                                     00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MNLIEUHSPI     PIC X(3).                                  00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNIDENTPL      COMP PIC S9(4).                            00000840
      *--                                                                       
           02 MNIDENTPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNIDENTPF      PIC X.                                     00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MNIDENTPI      PIC X(7).                                  00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEL     COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MQTEL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MQTEF     PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MQTEI     PIC X(3).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEPL    COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MQTEPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQTEPF    PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MQTEPI    PIC X(3).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSERIEL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MNSERIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSERIEF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MNSERIEI  PIC X(16).                                      00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSERIEPL      COMP PIC S9(4).                            00001000
      *--                                                                       
           02 MNSERIEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSERIEPF      PIC X.                                     00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MNSERIEPI      PIC X(16).                                 00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGARANTIL     COMP PIC S9(4).                            00001040
      *--                                                                       
           02 MCGARANTIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCGARANTIF     PIC X.                                     00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MCGARANTII     PIC X(5).                                  00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGARANTIL     COMP PIC S9(4).                            00001080
      *--                                                                       
           02 MLGARANTIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLGARANTIF     PIC X.                                     00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MLGARANTII     PIC X(20).                                 00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRENDUL  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MCRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRENDUF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MCRENDUI  PIC X(5).                                       00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRENDUL  COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MLRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRENDUF  PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MLRENDUI  PIC X(20).                                      00001190
      * MESSAGE ERREUR                                                  00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MLIBERRI  PIC X(78).                                      00001240
      * CODE TRANSACTION                                                00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MCODTRAI  PIC X(4).                                       00001290
      * CICS DE TRAVAIL                                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCICSI    PIC X(5).                                       00001340
      * NETNAME                                                         00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MNETNAMI  PIC X(8).                                       00001390
      * CODE TERMINAL                                                   00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001410
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001420
           02 FILLER    PIC X(4).                                       00001430
           02 MSCREENI  PIC X(5).                                       00001440
      ***************************************************************** 00001450
      * SDF: EEF10   EEF10                                              00001460
      ***************************************************************** 00001470
       01   EEF10O REDEFINES EEF10I.                                    00001480
           02 FILLER    PIC X(12).                                      00001490
      * DATE DU JOUR                                                    00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MDATJOUA  PIC X.                                          00001520
           02 MDATJOUC  PIC X.                                          00001530
           02 MDATJOUP  PIC X.                                          00001540
           02 MDATJOUH  PIC X.                                          00001550
           02 MDATJOUV  PIC X.                                          00001560
           02 MDATJOUO  PIC X(10).                                      00001570
      * HEURE                                                           00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MTIMJOUA  PIC X.                                          00001600
           02 MTIMJOUC  PIC X.                                          00001610
           02 MTIMJOUP  PIC X.                                          00001620
           02 MTIMJOUH  PIC X.                                          00001630
           02 MTIMJOUV  PIC X.                                          00001640
           02 MTIMJOUO  PIC X(5).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCTRAITA  PIC X.                                          00001670
           02 MCTRAITC  PIC X.                                          00001680
           02 MCTRAITP  PIC X.                                          00001690
           02 MCTRAITH  PIC X.                                          00001700
           02 MCTRAITV  PIC X.                                          00001710
           02 MCTRAITO  PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLTRAITA  PIC X.                                          00001740
           02 MLTRAITC  PIC X.                                          00001750
           02 MLTRAITP  PIC X.                                          00001760
           02 MLTRAITH  PIC X.                                          00001770
           02 MLTRAITV  PIC X.                                          00001780
           02 MLTRAITO  PIC X(30).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLTIERSA  PIC X.                                          00001810
           02 MLTIERSC  PIC X.                                          00001820
           02 MLTIERSP  PIC X.                                          00001830
           02 MLTIERSH  PIC X.                                          00001840
           02 MLTIERSV  PIC X.                                          00001850
           02 MLTIERSO  PIC X(15).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCTIERSA  PIC X.                                          00001880
           02 MCTIERSC  PIC X.                                          00001890
           02 MCTIERSP  PIC X.                                          00001900
           02 MCTIERSH  PIC X.                                          00001910
           02 MCTIERSV  PIC X.                                          00001920
           02 MCTIERSO  PIC X(5).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLIBTIERSA     PIC X.                                     00001950
           02 MLIBTIERSC     PIC X.                                     00001960
           02 MLIBTIERSP     PIC X.                                     00001970
           02 MLIBTIERSH     PIC X.                                     00001980
           02 MLIBTIERSV     PIC X.                                     00001990
           02 MLIBTIERSO     PIC X(20).                                 00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLENVOIA  PIC X.                                          00002020
           02 MLENVOIC  PIC X.                                          00002030
           02 MLENVOIP  PIC X.                                          00002040
           02 MLENVOIH  PIC X.                                          00002050
           02 MLENVOIV  PIC X.                                          00002060
           02 MLENVOIO  PIC X(21).                                      00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNENVOIA  PIC X.                                          00002090
           02 MNENVOIC  PIC X.                                          00002100
           02 MNENVOIP  PIC X.                                          00002110
           02 MNENVOIH  PIC X.                                          00002120
           02 MNENVOIV  PIC X.                                          00002130
           02 MNENVOIO  PIC 9999999.                                    00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MDENVOIA  PIC X.                                          00002160
           02 MDENVOIC  PIC X.                                          00002170
           02 MDENVOIP  PIC X.                                          00002180
           02 MDENVOIH  PIC X.                                          00002190
           02 MDENVOIV  PIC X.                                          00002200
           02 MDENVOIO  PIC X(10).                                      00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MNACCORDA      PIC X.                                     00002230
           02 MNACCORDC PIC X.                                          00002240
           02 MNACCORDP PIC X.                                          00002250
           02 MNACCORDH PIC X.                                          00002260
           02 MNACCORDV PIC X.                                          00002270
           02 MNACCORDO      PIC 999999999999.                          00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MDACCORDA      PIC X.                                     00002300
           02 MDACCORDC PIC X.                                          00002310
           02 MDACCORDP PIC X.                                          00002320
           02 MDACCORDH PIC X.                                          00002330
           02 MDACCORDV PIC X.                                          00002340
           02 MDACCORDO      PIC X(10).                                 00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MPRECEDA  PIC X.                                          00002370
           02 MPRECEDC  PIC X.                                          00002380
           02 MPRECEDP  PIC X.                                          00002390
           02 MPRECEDH  PIC X.                                          00002400
           02 MPRECEDV  PIC X.                                          00002410
           02 MPRECEDO  PIC X(17).                                      00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MLACCORDA      PIC X.                                     00002440
           02 MLACCORDC PIC X.                                          00002450
           02 MLACCORDP PIC X.                                          00002460
           02 MLACCORDH PIC X.                                          00002470
           02 MLACCORDV PIC X.                                          00002480
           02 MLACCORDO      PIC X(10).                                 00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNCODICA  PIC X.                                          00002510
           02 MNCODICC  PIC X.                                          00002520
           02 MNCODICP  PIC X.                                          00002530
           02 MNCODICH  PIC X.                                          00002540
           02 MNCODICV  PIC X.                                          00002550
           02 MNCODICO  PIC 9999999.                                    00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MNCODICPA      PIC X.                                     00002580
           02 MNCODICPC PIC X.                                          00002590
           02 MNCODICPP PIC X.                                          00002600
           02 MNCODICPH PIC X.                                          00002610
           02 MNCODICPV PIC X.                                          00002620
           02 MNCODICPO      PIC X(7).                                  00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MNLIEUHSA      PIC X.                                     00002650
           02 MNLIEUHSC PIC X.                                          00002660
           02 MNLIEUHSP PIC X.                                          00002670
           02 MNLIEUHSH PIC X.                                          00002680
           02 MNLIEUHSV PIC X.                                          00002690
           02 MNLIEUHSO      PIC X(3).                                  00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MNIDENTIFA     PIC X.                                     00002720
           02 MNIDENTIFC     PIC X.                                     00002730
           02 MNIDENTIFP     PIC X.                                     00002740
           02 MNIDENTIFH     PIC X.                                     00002750
           02 MNIDENTIFV     PIC X.                                     00002760
           02 MNIDENTIFO     PIC 9999999.                               00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MNLIEUHSPA     PIC X.                                     00002790
           02 MNLIEUHSPC     PIC X.                                     00002800
           02 MNLIEUHSPP     PIC X.                                     00002810
           02 MNLIEUHSPH     PIC X.                                     00002820
           02 MNLIEUHSPV     PIC X.                                     00002830
           02 MNLIEUHSPO     PIC X(3).                                  00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MNIDENTPA      PIC X.                                     00002860
           02 MNIDENTPC PIC X.                                          00002870
           02 MNIDENTPP PIC X.                                          00002880
           02 MNIDENTPH PIC X.                                          00002890
           02 MNIDENTPV PIC X.                                          00002900
           02 MNIDENTPO      PIC X(7).                                  00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MQTEA     PIC X.                                          00002930
           02 MQTEC     PIC X.                                          00002940
           02 MQTEP     PIC X.                                          00002950
           02 MQTEH     PIC X.                                          00002960
           02 MQTEV     PIC X.                                          00002970
           02 MQTEO     PIC ZZ9.                                        00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MQTEPA    PIC X.                                          00003000
           02 MQTEPC    PIC X.                                          00003010
           02 MQTEPP    PIC X.                                          00003020
           02 MQTEPH    PIC X.                                          00003030
           02 MQTEPV    PIC X.                                          00003040
           02 MQTEPO    PIC X(3).                                       00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MNSERIEA  PIC X.                                          00003070
           02 MNSERIEC  PIC X.                                          00003080
           02 MNSERIEP  PIC X.                                          00003090
           02 MNSERIEH  PIC X.                                          00003100
           02 MNSERIEV  PIC X.                                          00003110
           02 MNSERIEO  PIC X(16).                                      00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MNSERIEPA      PIC X.                                     00003140
           02 MNSERIEPC PIC X.                                          00003150
           02 MNSERIEPP PIC X.                                          00003160
           02 MNSERIEPH PIC X.                                          00003170
           02 MNSERIEPV PIC X.                                          00003180
           02 MNSERIEPO      PIC X(16).                                 00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MCGARANTIA     PIC X.                                     00003210
           02 MCGARANTIC     PIC X.                                     00003220
           02 MCGARANTIP     PIC X.                                     00003230
           02 MCGARANTIH     PIC X.                                     00003240
           02 MCGARANTIV     PIC X.                                     00003250
           02 MCGARANTIO     PIC X(5).                                  00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MLGARANTIA     PIC X.                                     00003280
           02 MLGARANTIC     PIC X.                                     00003290
           02 MLGARANTIP     PIC X.                                     00003300
           02 MLGARANTIH     PIC X.                                     00003310
           02 MLGARANTIV     PIC X.                                     00003320
           02 MLGARANTIO     PIC X(20).                                 00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MCRENDUA  PIC X.                                          00003350
           02 MCRENDUC  PIC X.                                          00003360
           02 MCRENDUP  PIC X.                                          00003370
           02 MCRENDUH  PIC X.                                          00003380
           02 MCRENDUV  PIC X.                                          00003390
           02 MCRENDUO  PIC X(5).                                       00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MLRENDUA  PIC X.                                          00003420
           02 MLRENDUC  PIC X.                                          00003430
           02 MLRENDUP  PIC X.                                          00003440
           02 MLRENDUH  PIC X.                                          00003450
           02 MLRENDUV  PIC X.                                          00003460
           02 MLRENDUO  PIC X(20).                                      00003470
      * MESSAGE ERREUR                                                  00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MLIBERRA  PIC X.                                          00003500
           02 MLIBERRC  PIC X.                                          00003510
           02 MLIBERRP  PIC X.                                          00003520
           02 MLIBERRH  PIC X.                                          00003530
           02 MLIBERRV  PIC X.                                          00003540
           02 MLIBERRO  PIC X(78).                                      00003550
      * CODE TRANSACTION                                                00003560
           02 FILLER    PIC X(2).                                       00003570
           02 MCODTRAA  PIC X.                                          00003580
           02 MCODTRAC  PIC X.                                          00003590
           02 MCODTRAP  PIC X.                                          00003600
           02 MCODTRAH  PIC X.                                          00003610
           02 MCODTRAV  PIC X.                                          00003620
           02 MCODTRAO  PIC X(4).                                       00003630
      * CICS DE TRAVAIL                                                 00003640
           02 FILLER    PIC X(2).                                       00003650
           02 MCICSA    PIC X.                                          00003660
           02 MCICSC    PIC X.                                          00003670
           02 MCICSP    PIC X.                                          00003680
           02 MCICSH    PIC X.                                          00003690
           02 MCICSV    PIC X.                                          00003700
           02 MCICSO    PIC X(5).                                       00003710
      * NETNAME                                                         00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MNETNAMA  PIC X.                                          00003740
           02 MNETNAMC  PIC X.                                          00003750
           02 MNETNAMP  PIC X.                                          00003760
           02 MNETNAMH  PIC X.                                          00003770
           02 MNETNAMV  PIC X.                                          00003780
           02 MNETNAMO  PIC X(8).                                       00003790
      * CODE TERMINAL                                                   00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MSCREENA  PIC X.                                          00003820
           02 MSCREENC  PIC X.                                          00003830
           02 MSCREENP  PIC X.                                          00003840
           02 MSCREENH  PIC X.                                          00003850
           02 MSCREENV  PIC X.                                          00003860
           02 MSCREENO  PIC X(5).                                       00003870
                                                                                
