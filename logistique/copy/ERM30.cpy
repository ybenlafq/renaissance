      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM30   ERM30                                              00000020
      ***************************************************************** 00000030
       01   ERM30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNMAGI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLMAGI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAGREGL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLAGREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAGREGF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLAGREGI  PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCODICI  PIC X(7).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCGROUPI  PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPT7L    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MOPT7L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MOPT7F    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MOPT7I    PIC X(58).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLIBERRI  PIC X(78).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCODTRAI  PIC X(4).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCICSI    PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNETNAMI  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSCREENI  PIC X(4).                                       00000730
      ***************************************************************** 00000740
      * SDF: ERM30   ERM30                                              00000750
      ***************************************************************** 00000760
       01   ERM30O REDEFINES ERM30I.                                    00000770
           02 FILLER    PIC X(12).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MDATJOUA  PIC X.                                          00000800
           02 MDATJOUC  PIC X.                                          00000810
           02 MDATJOUP  PIC X.                                          00000820
           02 MDATJOUH  PIC X.                                          00000830
           02 MDATJOUV  PIC X.                                          00000840
           02 MDATJOUO  PIC X(10).                                      00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MTIMJOUA  PIC X.                                          00000870
           02 MTIMJOUC  PIC X.                                          00000880
           02 MTIMJOUP  PIC X.                                          00000890
           02 MTIMJOUH  PIC X.                                          00000900
           02 MTIMJOUV  PIC X.                                          00000910
           02 MTIMJOUO  PIC X(5).                                       00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MZONCMDA  PIC X.                                          00000940
           02 MZONCMDC  PIC X.                                          00000950
           02 MZONCMDP  PIC X.                                          00000960
           02 MZONCMDH  PIC X.                                          00000970
           02 MZONCMDV  PIC X.                                          00000980
           02 MZONCMDO  PIC X(15).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MNSOCA    PIC X.                                          00001010
           02 MNSOCC    PIC X.                                          00001020
           02 MNSOCP    PIC X.                                          00001030
           02 MNSOCH    PIC X.                                          00001040
           02 MNSOCV    PIC X.                                          00001050
           02 MNSOCO    PIC X(3).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MNMAGA    PIC X.                                          00001080
           02 MNMAGC    PIC X.                                          00001090
           02 MNMAGP    PIC X.                                          00001100
           02 MNMAGH    PIC X.                                          00001110
           02 MNMAGV    PIC X.                                          00001120
           02 MNMAGO    PIC X(3).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MLMAGA    PIC X.                                          00001150
           02 MLMAGC    PIC X.                                          00001160
           02 MLMAGP    PIC X.                                          00001170
           02 MLMAGH    PIC X.                                          00001180
           02 MLMAGV    PIC X.                                          00001190
           02 MLMAGO    PIC X(20).                                      00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MCFAMA    PIC X.                                          00001220
           02 MCFAMC    PIC X.                                          00001230
           02 MCFAMP    PIC X.                                          00001240
           02 MCFAMH    PIC X.                                          00001250
           02 MCFAMV    PIC X.                                          00001260
           02 MCFAMO    PIC X(5).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MLFAMA    PIC X.                                          00001290
           02 MLFAMC    PIC X.                                          00001300
           02 MLFAMP    PIC X.                                          00001310
           02 MLFAMH    PIC X.                                          00001320
           02 MLFAMV    PIC X.                                          00001330
           02 MLFAMO    PIC X(20).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLAGREGA  PIC X.                                          00001360
           02 MLAGREGC  PIC X.                                          00001370
           02 MLAGREGP  PIC X.                                          00001380
           02 MLAGREGH  PIC X.                                          00001390
           02 MLAGREGV  PIC X.                                          00001400
           02 MLAGREGO  PIC X(20).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNCODICA  PIC X.                                          00001430
           02 MNCODICC  PIC X.                                          00001440
           02 MNCODICP  PIC X.                                          00001450
           02 MNCODICH  PIC X.                                          00001460
           02 MNCODICV  PIC X.                                          00001470
           02 MNCODICO  PIC X(7).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MCGROUPA  PIC X.                                          00001500
           02 MCGROUPC  PIC X.                                          00001510
           02 MCGROUPP  PIC X.                                          00001520
           02 MCGROUPH  PIC X.                                          00001530
           02 MCGROUPV  PIC X.                                          00001540
           02 MCGROUPO  PIC X(5).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MOPT7A    PIC X.                                          00001570
           02 MOPT7C    PIC X.                                          00001580
           02 MOPT7P    PIC X.                                          00001590
           02 MOPT7H    PIC X.                                          00001600
           02 MOPT7V    PIC X.                                          00001610
           02 MOPT7O    PIC X(58).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLIBERRA  PIC X.                                          00001640
           02 MLIBERRC  PIC X.                                          00001650
           02 MLIBERRP  PIC X.                                          00001660
           02 MLIBERRH  PIC X.                                          00001670
           02 MLIBERRV  PIC X.                                          00001680
           02 MLIBERRO  PIC X(78).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCODTRAA  PIC X.                                          00001710
           02 MCODTRAC  PIC X.                                          00001720
           02 MCODTRAP  PIC X.                                          00001730
           02 MCODTRAH  PIC X.                                          00001740
           02 MCODTRAV  PIC X.                                          00001750
           02 MCODTRAO  PIC X(4).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCICSA    PIC X.                                          00001780
           02 MCICSC    PIC X.                                          00001790
           02 MCICSP    PIC X.                                          00001800
           02 MCICSH    PIC X.                                          00001810
           02 MCICSV    PIC X.                                          00001820
           02 MCICSO    PIC X(5).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNETNAMA  PIC X.                                          00001850
           02 MNETNAMC  PIC X.                                          00001860
           02 MNETNAMP  PIC X.                                          00001870
           02 MNETNAMH  PIC X.                                          00001880
           02 MNETNAMV  PIC X.                                          00001890
           02 MNETNAMO  PIC X(8).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MSCREENA  PIC X.                                          00001920
           02 MSCREENC  PIC X.                                          00001930
           02 MSCREENP  PIC X.                                          00001940
           02 MSCREENH  PIC X.                                          00001950
           02 MSCREENV  PIC X.                                          00001960
           02 MSCREENO  PIC X(4).                                       00001970
                                                                                
