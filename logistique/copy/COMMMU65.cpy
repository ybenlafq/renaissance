      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PRG TMU40 (MENU)             TR: MU40  *            
      *                          TMU65                             *            
      *                                                            *            
      *                                                            *            
      *                                                            *            
      *           POUR L'ADMINISTATION DES DONNEES                 *            
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-------------------------------------------------------------            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-MU40-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-MU40-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA      PIC X(100).                                   
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02 COMM-CICS-APPLID     PIC X(8).                                     
          02 COMM-CICS-NETNAM     PIC X(8).                                     
          02 COMM-CICS-TRANSA     PIC X(4).                                     
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE     PIC XX.                                       
          02 COMM-DATE-ANNEE      PIC XX.                                       
          02 COMM-DATE-MOIS       PIC XX.                                       
          02 COMM-DATE-JOUR       PIC XX.                                       
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC 999.                                      
          02 COMM-DATE-QNT0       PIC 99999.                                    
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC 9.                                        
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC 9.                                        
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC XXX.                                      
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                 
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC XXX.                                      
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                 
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                                     
          02 COMM-DATE-AAMMJJ     PIC X(6).                                     
          02 COMM-DATE-JJMMSSAA   PIC X(8).                                     
          02 COMM-DATE-JJMMAA     PIC X(6).                                     
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                                     
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                    
      * DIFFERENTES FORMES DE DATE                                              
          02 COMM-DATE-SEMSS      PIC 9(02).                                    
          02 COMM-DATE-SEMAA      PIC 9(02).                                    
          02 COMM-DATE-SEMNU      PIC 9(02).                                    
          02 COMM-DATE-FILLER     PIC X(08).                                    
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                                
      *                                                                         
      * ZONES DE COMMAREA CHARGEES PAR LE PROG TMU40 ------------ 1150          
      *                                                                         
          02 COMM-MU40-APPLI.                                                   
      *                                                                         
              03 COMM-MU40-DMUTATION         PIC X(08).                         
              03 COMM-MU40-NMUTATION         PIC X(07).                         
              03 COMM-MU40-NSOCENTR          PIC X(03).                         
              03 COMM-MU40-NDEPOT            PIC X(03).                         
              03 COMM-MU40-LLIEUDEPOT        PIC X(20).                         
              03 COMM-MU40-CSELART           PIC X(05).                         
              03 COMM-MU40-NSOCIETE          PIC X(03).                         
              03 COMM-MU40-NLIEU             PIC X(03).                         
              03 COMM-MU40-LLIEUDEM          PIC X(20).                         
              03 COMM-MU40-CTYPCDE           PIC X(01).                         
              03 COMM-MU40-LTYPCDE           PIC X(20).                         
              03 COMM-MU40-NCODIC            PIC X(07).                         
              03 COMM-MU40-TABLE-TYPCDE.                                        
                 05 COMM-MU40-TYPCDE         OCCURS 50.                         
                    10 COMM-MU40-TABCTYPCDE  PIC X(01).                         
                    10 COMM-MU40-TABLTYPCDE  PIC X(20).                         
      *****************************************************************         
      **************************************************************            
      * COMMAREA SPECIFIQUE PRG TMU65                    TR: MU40  *            
      *                                                            *            
      *                                                            *            
      *    SAISIE DES CONTREMARQUE POUR MUTATION MGC MGI           *            
      *                                                            *            
      * ZONES RESERVEES APPLICATIVES --------------------- 3519    *            
      *                                                    2574    *            
      **************************************************************            
      *   TRANSACTION MU65 : CONSULTATION DES RESERVATIONS         *            
      *                      CODIC CONTRE-MARQUE                   *            
      **************************************************************            
      *                                                                         
          02 COMM-MU65-APPLI.                                                   
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-MU65-NB-CODIC          PIC S9(04) COMP.                   
      *--                                                                       
              05 COMM-MU65-NB-CODIC          PIC S9(04) COMP-5.                 
      *}                                                                        
              05 COMM-MU65-NB-PAGE           PIC 9(03).                         
              05 COMM-MU65-NO-PAGE           PIC 9(03).                         
              05 COMM-MU65-CODIC             PIC 9(07).                         
              05 COMM-MU65-TRAIT             PIC X(01).                         
              05 COMM-MU65-TABLE.                                               
                 07 COMM-MU65-LIGNE          OCCURS 13.                         
                    09 COMM-MU65-CFAM        PIC X(05).                         
                    09 COMM-MU65-CMARQ       PIC X(05).                         
                    09 COMM-MU65-REF         PIC X(20).                         
                    09 COMM-MU65-NCODIC      PIC X(07).                         
                    09 COMM-MU65-NDOC        PIC X(07).                         
                    09 COMM-MU65-NMUT        PIC X(07).                         
                    09 COMM-MU65-DMUT        PIC X(08).                         
                    09 COMM-MU65-QTE         PIC 9(04).                         
                    09 COMM-MU65-STAT        PIC X(01).                         
              05 COMM-MU65-T-RNG.                                               
                 07 COMM-MU65-TRNG           OCCURS 140.                        
                    09 COMM-MU65-WCODIC      PIC  X(7).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             09 COMM-MU65-RNG         PIC S9(4) COMP.                    
      *--                                                                       
                    09 COMM-MU65-RNG         PIC S9(4) COMP-5.                  
      *}                                                                        
      *                                                                         
             05 COMM-MU65-FILLER         PIC X(0466).                           
      *                                                                         
      *****************************************************************         
                                                                                
