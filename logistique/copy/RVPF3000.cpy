      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVPF3000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPF3000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPF3000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPF3000.                                                            
      *}                                                                        
           10 PF30-NSOC            PIC X(3).                                    
           10 PF30-NLIEU           PIC X(3).                                    
           10 PF30-NCODIC          PIC X(7).                                    
           10 PF30-WACTIF          PIC X(1).                                    
           10 PF30-DEFFET          PIC X(8).                                    
           10 PF30-QSO             PIC S9(5)V USAGE COMP-3.                     
           10 PF30-PRIOR           PIC X(1).                                    
           10 PF30-CACID           PIC X(8).                                    
           10 PF30-CPROG           PIC X(6).                                    
           10 PF30-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      ******************************************************************        
      * LISTE DES FLAGS DE LA TABLE RVPF3000                                    
      ******************************************************************        
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPF3000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPF3000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF30-NSOC-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 PF30-NSOC-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF30-NLIEU-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 PF30-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF30-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 PF30-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF30-WACTIF-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 PF30-WACTIF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF30-DEFFET-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 PF30-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF30-QSO-F           PIC S9(4) COMP.                              
      *--                                                                       
           10 PF30-QSO-F           PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF30-PRIOR-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 PF30-PRIOR-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF30-CACID-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 PF30-CACID-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF30-CPROG-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 PF30-CPROG-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF30-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 PF30-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
