      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                  LONGUEUR 3224          
               05 COMM-TL28-DATA       REDEFINES COMM-TL27-FILLER.              
                  07  COMM-TL28-NPAGE            PIC 9(3).                      
                  07  COMM-TL28-NPAGE-MAX        PIC 9(3).                      
                  07  COMM-TL28-CRETG-I          PIC X(5).                      
                  07  COMM-TL28-CRETG-F          PIC X(5).                      
                  07  COMM-TL28-LRETG            PIC X(20).                     
      *    LE NOMBRE MAXIMUM DE LIGNES AFFICHABLES EST DE 45                    
      *                                            <=> 4 PAGES MAXIMUM .        
                  07 COMM-TL28-ECRAN             OCCURS 4.                      
                     09 COMM-TL28-LIGNE          OCCURS 13.                     
                        11 COMM-TL28-NCODIC      PIC X(7).                      
                        11 COMM-TL28-NCODICGRP   PIC X(7).                      
                        11 COMM-TL28-WREP        PIC X.                         
                        11 COMM-TL28-NSEQ        PIC X(2).                      
                        11 COMM-TL28-QVENDUE     PIC 9(5).                      
                        11 COMM-TL28-QLIVREE-I   PIC 9(5).                      
                        11 COMM-TL28-QLIVREE-F   PIC 9(5).                      
                        11 COMM-TL28-CRETOUR-I   PIC X(5).                      
                        11 COMM-TL28-CRETOUR-F   PIC X(5).                      
                        11 COMM-TL28-MAJ         PIC X.                         
                  07 COMM-TL28-CODERM            PIC X.                         
                  07 COMM-TL28-FILLER            PIC X(0941).                   
      *           07 COMM-TL28-FILLER            PIC X(0951).                   
                                                                                
