      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGB0300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB0300                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGB0300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGB0300.                                                            
      *}                                                                        
           02  GB03-NLIGTRANS                                                   
               PIC X(0006).                                                     
           02  GB03-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GB03-NLIEU                                                       
               PIC X(0003).                                                     
           02  GB03-NSEQUENCE                                                   
               PIC X(0002).                                                     
           02  GB03-NJOUR                                                       
               PIC X(0001).                                                     
           02  GB03-CMAJ                                                        
               PIC X(0001).                                                     
           02  GB03-WACTIF                                                      
               PIC X(0001).                                                     
           02  GB03-CSELART1                                                    
               PIC X(0005).                                                     
           02  GB03-CSELART2                                                    
               PIC X(0005).                                                     
           02  GB03-CSELART3                                                    
               PIC X(0005).                                                     
           02  GB03-CSELART4                                                    
               PIC X(0005).                                                     
           02  GB03-CSELART5                                                    
               PIC X(0005).                                                     
           02  GB03-CSELART6                                                    
               PIC X(0005).                                                     
           02  GB03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGB0300                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGB0300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGB0300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-NLIGTRANS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-NLIGTRANS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-NSEQUENCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-NSEQUENCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-NJOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-NJOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-CMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-CMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-WACTIF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-WACTIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-CSELART1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-CSELART1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-CSELART2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-CSELART2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-CSELART3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-CSELART3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-CSELART4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-CSELART4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-CSELART5-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-CSELART5-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-CSELART6-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-CSELART6-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
