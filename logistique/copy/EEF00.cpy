      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EEF00   EEF00                                              00000020
      ***************************************************************** 00000030
       01   EEF00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPTCHOIL      COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MOPTCHOIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MOPTCHOIF      PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MOPTCHOII      PIC X(2).                                  00000190
           02 MMENUI OCCURS   12 TIMES .                                00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOPTL  COMP PIC S9(4).                                 00000210
      *--                                                                       
             03 MNOPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNOPTF  PIC X.                                          00000220
             03 FILLER  PIC X(4).                                       00000230
             03 MNOPTI  PIC X(2).                                       00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTIRETL      COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MTIRETL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTIRETF      PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MTIRETI      PIC X.                                     00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBOPTL     COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MLIBOPTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBOPTF     PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MLIBOPTI     PIC X(30).                                 00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOPT2L      COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MNOPT2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNOPT2F      PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MNOPT2I      PIC X(2).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTIRET2L     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MTIRET2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTIRET2F     PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MTIRET2I     PIC X.                                     00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBOPT2L    COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MLIBOPT2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBOPT2F    PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MLIBOPT2I    PIC X(30).                                 00000440
      * MESSAGE ERREUR                                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIBERRI  PIC X(78).                                      00000490
      * CODE TRANSACTION                                                00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCODTRAI  PIC X(4).                                       00000540
      * CICS DE TRAVAIL                                                 00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCICSI    PIC X(5).                                       00000590
      * NETNAME                                                         00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MNETNAMI  PIC X(8).                                       00000640
      * CODE TERMINAL                                                   00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSCREENI  PIC X(5).                                       00000690
      ***************************************************************** 00000700
      * SDF: EEF00   EEF00                                              00000710
      ***************************************************************** 00000720
       01   EEF00O REDEFINES EEF00I.                                    00000730
           02 FILLER    PIC X(12).                                      00000740
      * DATE DU JOUR                                                    00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MDATJOUA  PIC X.                                          00000770
           02 MDATJOUC  PIC X.                                          00000780
           02 MDATJOUP  PIC X.                                          00000790
           02 MDATJOUH  PIC X.                                          00000800
           02 MDATJOUV  PIC X.                                          00000810
           02 MDATJOUO  PIC X(10).                                      00000820
      * HEURE                                                           00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MTIMJOUA  PIC X.                                          00000850
           02 MTIMJOUC  PIC X.                                          00000860
           02 MTIMJOUP  PIC X.                                          00000870
           02 MTIMJOUH  PIC X.                                          00000880
           02 MTIMJOUV  PIC X.                                          00000890
           02 MTIMJOUO  PIC X(5).                                       00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MOPTCHOIA      PIC X.                                     00000920
           02 MOPTCHOIC PIC X.                                          00000930
           02 MOPTCHOIP PIC X.                                          00000940
           02 MOPTCHOIH PIC X.                                          00000950
           02 MOPTCHOIV PIC X.                                          00000960
           02 MOPTCHOIO      PIC X(2).                                  00000970
           02 MMENUO OCCURS   12 TIMES .                                00000980
             03 FILLER       PIC X(2).                                  00000990
             03 MNOPTA  PIC X.                                          00001000
             03 MNOPTC  PIC X.                                          00001010
             03 MNOPTP  PIC X.                                          00001020
             03 MNOPTH  PIC X.                                          00001030
             03 MNOPTV  PIC X.                                          00001040
             03 MNOPTO  PIC X(2).                                       00001050
             03 FILLER       PIC X(2).                                  00001060
             03 MTIRETA      PIC X.                                     00001070
             03 MTIRETC PIC X.                                          00001080
             03 MTIRETP PIC X.                                          00001090
             03 MTIRETH PIC X.                                          00001100
             03 MTIRETV PIC X.                                          00001110
             03 MTIRETO      PIC X.                                     00001120
             03 FILLER       PIC X(2).                                  00001130
             03 MLIBOPTA     PIC X.                                     00001140
             03 MLIBOPTC     PIC X.                                     00001150
             03 MLIBOPTP     PIC X.                                     00001160
             03 MLIBOPTH     PIC X.                                     00001170
             03 MLIBOPTV     PIC X.                                     00001180
             03 MLIBOPTO     PIC X(30).                                 00001190
             03 FILLER       PIC X(2).                                  00001200
             03 MNOPT2A      PIC X.                                     00001210
             03 MNOPT2C PIC X.                                          00001220
             03 MNOPT2P PIC X.                                          00001230
             03 MNOPT2H PIC X.                                          00001240
             03 MNOPT2V PIC X.                                          00001250
             03 MNOPT2O      PIC X(2).                                  00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MTIRET2A     PIC X.                                     00001280
             03 MTIRET2C     PIC X.                                     00001290
             03 MTIRET2P     PIC X.                                     00001300
             03 MTIRET2H     PIC X.                                     00001310
             03 MTIRET2V     PIC X.                                     00001320
             03 MTIRET2O     PIC X.                                     00001330
             03 FILLER       PIC X(2).                                  00001340
             03 MLIBOPT2A    PIC X.                                     00001350
             03 MLIBOPT2C    PIC X.                                     00001360
             03 MLIBOPT2P    PIC X.                                     00001370
             03 MLIBOPT2H    PIC X.                                     00001380
             03 MLIBOPT2V    PIC X.                                     00001390
             03 MLIBOPT2O    PIC X(30).                                 00001400
      * MESSAGE ERREUR                                                  00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLIBERRA  PIC X.                                          00001430
           02 MLIBERRC  PIC X.                                          00001440
           02 MLIBERRP  PIC X.                                          00001450
           02 MLIBERRH  PIC X.                                          00001460
           02 MLIBERRV  PIC X.                                          00001470
           02 MLIBERRO  PIC X(78).                                      00001480
      * CODE TRANSACTION                                                00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCODTRAA  PIC X.                                          00001510
           02 MCODTRAC  PIC X.                                          00001520
           02 MCODTRAP  PIC X.                                          00001530
           02 MCODTRAH  PIC X.                                          00001540
           02 MCODTRAV  PIC X.                                          00001550
           02 MCODTRAO  PIC X(4).                                       00001560
      * CICS DE TRAVAIL                                                 00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MCICSA    PIC X.                                          00001590
           02 MCICSC    PIC X.                                          00001600
           02 MCICSP    PIC X.                                          00001610
           02 MCICSH    PIC X.                                          00001620
           02 MCICSV    PIC X.                                          00001630
           02 MCICSO    PIC X(5).                                       00001640
      * NETNAME                                                         00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MNETNAMA  PIC X.                                          00001670
           02 MNETNAMC  PIC X.                                          00001680
           02 MNETNAMP  PIC X.                                          00001690
           02 MNETNAMH  PIC X.                                          00001700
           02 MNETNAMV  PIC X.                                          00001710
           02 MNETNAMO  PIC X(8).                                       00001720
      * CODE TERMINAL                                                   00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MSCREENA  PIC X.                                          00001750
           02 MSCREENC  PIC X.                                          00001760
           02 MSCREENP  PIC X.                                          00001770
           02 MSCREENH  PIC X.                                          00001780
           02 MSCREENV  PIC X.                                          00001790
           02 MSCREENO  PIC X(5).                                       00001800
                                                                                
