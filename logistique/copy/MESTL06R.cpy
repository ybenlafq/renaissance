      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:10 >
      
      *****************************************************************
      *   COPY MESSAGES MQ REPONSE DU HOST
      *
      *****************************************************************
      *
      *
       01 WS-MESSAGE.
      *---
           15 MES-ENTETE.
              20 MES-TYPE                 PIC    X(03).
              20 MES-NSOCMSG              PIC    X(03).
              20 MES-NLIEUMSG             PIC    X(03).
              20 MES-NSOCDST              PIC    X(03).
              20 MES-NLIEUDST             PIC    X(03).
              20 MES-NORD                 PIC    9(08).
              20 MES-LPROG                PIC    X(10).
              20 MES-DJOUR                PIC    X(08).
              20 MES-WSID                 PIC    X(10).
              20 MES-USER                 PIC    X(10).
              20 MES-CHRONO               PIC    9(07).
              20 MES-NBRMSG               PIC    9(07).
              20 MES-NBROCC               PIC    9(05).
              20 MES-LNGOCC               PIC    9(05).
              20 MES-VERSION              PIC    X(02).
              20 MES-DSYST                PIC    S9(13).
              20 MES-FILLER               PIC    X(05).
           15 MES-DATA.
              20 MES-W-NTOUR              PIC    X(03).
              20 MES-W-DDELIV             PIC    X(08).
              20 MES-W-INDTOUR            PIC    X(01).
              20 MES-W-CLIVR1             PIC    X(10).
              20 MES-W-LLIVR1             PIC    X(20).
              20 MES-W-CLIVR2             PIC    X(10).
              20 MES-W-LLIVR2             PIC    X(20).
              20 MES-W-CLIVR3             PIC    X(10).
              20 MES-W-LLIVR3             PIC    X(20).
              20 MES-W-NBVTE              PIC    9(03).
              20 MES-W-NBLIG              PIC    9(03).
           15 MES-TABLE.
              20 MES-LIGNE OCCURS 500  INDEXED  IK.
                 25 MES-W-NLIEU           PIC    X(03).
                 25 MES-W-NORDV           PIC    X(07).
                 25 MES-W-INDVTE          PIC    X(01).
                 25 MES-W-NCODIC          PIC    X(07).
                 25 MES-W-NSEQNQ          PIC    9(05).
                 25 MES-W-QLIVRE          PIC    9(05).
                 25 MES-W-TYPADR          PIC    X(01).
                 25 MES-W-MTBL            PIC   S9(07)V99.
                 25 MES-W-PLIVRE          PIC   S9(07)V99.
                 25 MES-W-CTYPE           PIC    X(01).
                 25 MES-W-NOMCLI          PIC    X(10).
      
