      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE MODULE TP MMU10                       *            
      *     DETERMINATION DES SEUILS AUTORISES DE SAISIE DE MUTS   *            
      **************************************************************            
      * CETTE COMM EST UTILISEE PAR LES PROGRAMMES :               *            
      * - TMU12                                                    *            
      * - TMU28                                                    *            
      * - TRM52                                                    *            
      * - TRM69                                                    *            
      **************************************************************            
      **************************************************************            
      *                                                                         
       01 COMM-MMU1A-APPLI.                                                     
          02 COMM-MMU1A-ENTREE.                                                 
             03 COMM-MMU1A-DEPOT.                                               
                05 COMM-MMU1A-NSOCDEP     PIC X(03).                            
                05 COMM-MMU1A-NLIEUDEP    PIC X(03).                            
             03 COMM-MMU1A-SELART         PIC X(05).                            
             03 COMM-MMU1A-DDESTOCK       PIC X(08).                            
           02 COMM-MMU1A-SORTIE.                                                
              03 COMM-MMU1A-SEUIL-VOLUME  PIC 9(3) COMP-3.                      
              03 COMM-MMU1A-SEUIL-PIECE   PIC 9(3) COMP-3.                      
                                                                                
