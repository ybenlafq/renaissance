      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      *             REAPPROVISONNEMENT MAGASIN                       *  00030000
      *         - TRANSACTION RM53                                   *  00040000
      *           (DISTRIBUTION ARTICLES EN PENURIE APRES SIMULATION *  00041000
      ****************************************************************  00050000
      *01 (P)-LONG        PIC S9(4) COMP-3 VALUE +1288.                 00080010
       01 (P)-LONG        PIC S9(4) COMP-3 VALUE +1290.                         
       01 (P)-DONNEES.                                                  00090000
          03 (P)-DONNEES-ECRAN.                                         00100000
             05 FILLER    PIC X(12).                                    01240000
             05 FILLER    PIC X(2).                                     01250000
             05 (P)-MDATJOUA PIC X.                                     01260000
             05 (P)-MDATJOUC PIC X.                                     01270000
             05 (P)-MDATJOUP PIC X.                                     01280000
             05 (P)-MDATJOUH PIC X.                                     01290000
             05 (P)-MDATJOUV PIC X.                                     01300000
             05 (P)-MDATJOUO PIC X(10).                                 01310000
             05 FILLER    PIC X(2).                                     01320000
             05 (P)-MTIMJOUA PIC X.                                     01330000
             05 (P)-MTIMJOUC PIC X.                                     01340000
             05 (P)-MTIMJOUP PIC X.                                     01350000
             05 (P)-MTIMJOUH PIC X.                                     01360000
             05 (P)-MTIMJOUV PIC X.                                     01370000
             05 (P)-MTIMJOUO PIC X(5).                                  01380000
             05 FILLER    PIC X(2).                                     01390000
             05 (P)-MNPAGEA PIC X.                                      01400000
             05 (P)-MNPAGEC PIC X.                                      01410000
             05 (P)-MNPAGEP PIC X.                                      01420000
             05 (P)-MNPAGEH PIC X.                                      01430000
             05 (P)-MNPAGEV PIC X.                                      01440000
             05 (P)-MNPAGEO PIC X(2).                                   01450000
             05 FILLER    PIC X(2).                                     01460000
             05 (P)-MNCODICA PIC X.                                     01470000
             05 (P)-MNCODICC PIC X.                                     01480000
             05 (P)-MNCODICP PIC X.                                     01490000
             05 (P)-MNCODICH PIC X.                                     01500000
             05 (P)-MNCODICV PIC X.                                     01510000
             05 (P)-MNCODICO PIC X(7).                                  01520000
             05 FILLER    PIC X(2).                                     01530000
             05 (P)-MREFFOURNA PIC X.                                   01540000
             05 (P)-MREFFOURNC PIC X.                                   01550000
             05 (P)-MREFFOURNP PIC X.                                   01560000
             05 (P)-MREFFOURNH PIC X.                                   01570000
             05 (P)-MREFFOURNV PIC X.                                   01580000
             05 (P)-MREFFOURNO PIC X(20).                               01590000
             05 FILLER    PIC X(2).                                     01600000
             05 (P)-MDSIMULA PIC X.                                     01610000
             05 (P)-MDSIMULC PIC X.                                     01620000
             05 (P)-MDSIMULP PIC X.                                     01630000
             05 (P)-MDSIMULH PIC X.                                     01640000
             05 (P)-MDSIMULV PIC X.                                     01650000
             05 (P)-MDSIMULO PIC X(10).                                 01660000
             05 FILLER    PIC X(2).                                     01670000
             05 (P)-MCFAMA PIC X.                                       01680000
             05 (P)-MCFAMC PIC X.                                       01690000
             05 (P)-MCFAMP PIC X.                                       01700000
             05 (P)-MCFAMH PIC X.                                       01710000
             05 (P)-MCFAMV PIC X.                                       01720000
             05 (P)-MCFAMO PIC X(5).                                    01730000
             05 FILLER    PIC X(2).                                     01740000
             05 (P)-MLFAMA PIC X.                                       01750000
             05 (P)-MLFAMC PIC X.                                       01760000
             05 (P)-MLFAMP PIC X.                                       01770000
             05 (P)-MLFAMH PIC X.                                       01780000
             05 (P)-MLFAMV PIC X.                                       01790000
             05 (P)-MLFAMO PIC X(20).                                   01800000
             05 FILLER    PIC X(2).                                     01810000
             05 (P)-MNSIMULA PIC X.                                     01820000
             05 (P)-MNSIMULC PIC X.                                     01830000
             05 (P)-MNSIMULP PIC X.                                     01840000
             05 (P)-MNSIMULH PIC X.                                     01850000
             05 (P)-MNSIMULV PIC X.                                     01860000
             05 (P)-MNSIMULO PIC X(3).                                  01870000
             05 FILLER    PIC X(2).                                     01880000
             05 (P)-MCMARQA PIC X.                                      01890000
             05 (P)-MCMARQC PIC X.                                      01900000
             05 (P)-MCMARQP PIC X.                                      01910000
             05 (P)-MCMARQH PIC X.                                      01920000
             05 (P)-MCMARQV PIC X.                                      01930000
             05 (P)-MCMARQO PIC X(5).                                   01940000
             05 FILLER    PIC X(2).                                     01950000
             05 (P)-MLMARQA PIC X.                                      01960000
             05 (P)-MLMARQC PIC X.                                      01970000
             05 (P)-MLMARQP PIC X.                                      01980000
             05 (P)-MLMARQH PIC X.                                      01990000
             05 (P)-MLMARQV PIC X.                                      02000000
             05 (P)-MLMARQO PIC X(20).                                  02010000
             05 FILLER    PIC X(2).                                     02020000
             05 (P)-MCGROUPA PIC X.                                     02030000
             05 (P)-MCGROUPC PIC X.                                     02040000
             05 (P)-MCGROUPP PIC X.                                     02050000
             05 (P)-MCGROUPH PIC X.                                     02060000
             05 (P)-MCGROUPV PIC X.                                     02070000
             05 (P)-MCGROUPO PIC X(5).                                  02080000
             05 FILLER    PIC X(2).                                     02090000
             05 (P)-MNSOCENA PIC X.                                     02100000
             05 (P)-MNSOCENC PIC X.                                     02110000
             05 (P)-MNSOCENP PIC X.                                     02120000
             05 (P)-MNSOCENH PIC X.                                     02130000
             05 (P)-MNSOCENV PIC X.                                     02140000
             05 (P)-MNSOCENO PIC X(3).                                  02150000
             05 FILLER    PIC X(2).                                     02160000
             05 (P)-MNDEPOTA PIC X.                                     02170000
             05 (P)-MNDEPOTC PIC X.                                     02180000
             05 (P)-MNDEPOTP PIC X.                                     02190000
             05 (P)-MNDEPOTH PIC X.                                     02200000
             05 (P)-MNDEPOTV PIC X.                                     02210000
             05 (P)-MNDEPOTO PIC X(3).                                  02220000
             05 FILLER    PIC X(2).                                     02230000
             05 (P)-MQTEDA PIC X.                                       02240000
             05 (P)-MQTEDC PIC X.                                       02250000
             05 (P)-MQTEDP PIC X.                                       02260000
             05 (P)-MQTEDH PIC X.                                       02270000
             05 (P)-MQTEDV PIC X.                                       02280000
             05 (P)-MQTEDO PIC X(5).                                    02290000
             05 FILLER    PIC X(2).                                     02300000
             05 (P)-MQDISPOA PIC X.                                     02310000
             05 (P)-MQDISPOC PIC X.                                     02320000
             05 (P)-MQDISPOP PIC X.                                     02330000
             05 (P)-MQDISPOH PIC X.                                     02340000
             05 (P)-MQDISPOV PIC X.                                     02350000
             05 (P)-MQDISPOO PIC X(6).                                          
             05 (P)-M8O OCCURS 12 TIMES .                               02370000
               07 FILLER       PIC X(2).                                02380000
               07 (P)-MNSOCA PIC X.                                     02390001
               07 (P)-MNSOCC PIC X.                                     02400001
               07 (P)-MNSOCP PIC X.                                     02410001
               07 (P)-MNSOCH PIC X.                                     02420001
               07 (P)-MNSOCV PIC X.                                     02430001
               07 (P)-MNSOCO PIC X(3).                                  02440001
               07 FILLER       PIC X(2).                                02450000
               07 (P)-MNLIEUA  PIC X.                                   02460001
               07 (P)-MNLIEUC PIC X.                                    02470001
               07 (P)-MNLIEUP PIC X.                                    02480001
               07 (P)-MNLIEUH PIC X.                                    02490001
               07 (P)-MNLIEUV PIC X.                                    02500001
               07 (P)-MNLIEUO  PIC X(3).                                02510001
               07 FILLER       PIC X(2).                                02520000
               07 (P)-MQTEA PIC X.                                      02530001
               07 (P)-MQTEC PIC X.                                      02540001
               07 (P)-MQTEP PIC X.                                      02550001
               07 (P)-MQTEH PIC X.                                      02560001
               07 (P)-MQTEV PIC X.                                      02570001
               07 (P)-MQTEO PIC X(5).                                   02580001
               07 FILLER       PIC X(2).                                02590000
               07 (P)-MWBLOQUEA PIC X.                                  02600001
               07 (P)-MWBLOQUEC PIC X.                                  02610001
               07 (P)-MWBLOQUEP PIC X.                                  02620001
               07 (P)-MWBLOQUEH PIC X.                                  02630001
               07 (P)-MWBLOQUEV PIC X.                                  02640001
               07 (P)-MWBLOQUEO PIC X.                                  02650001
             05 FILLER    PIC X(2).                                     02660000
             05 (P)-MZONCMDA PIC X.                                     02670000
             05 (P)-MZONCMDC PIC X.                                     02680000
             05 (P)-MZONCMDP PIC X.                                     02690000
             05 (P)-MZONCMDH PIC X.                                     02700000
             05 (P)-MZONCMDV PIC X.                                     02710000
             05 (P)-MZONCMDO PIC X(12).                                 02720000
             05 FILLER    PIC X(2).                                     02730000
             05 (P)-MLIBERRA PIC X.                                     02740000
             05 (P)-MLIBERRC PIC X.                                     02750000
             05 (P)-MLIBERRP PIC X.                                     02760000
             05 (P)-MLIBERRH PIC X.                                     02770000
             05 (P)-MLIBERRV PIC X.                                     02780000
             05 (P)-MLIBERRO PIC X(61).                                 02790000
             05 FILLER    PIC X(2).                                     02800000
             05 (P)-MCODTRAA PIC X.                                     02810000
             05 (P)-MCODTRAC PIC X.                                     02820000
             05 (P)-MCODTRAP PIC X.                                     02830000
             05 (P)-MCODTRAH PIC X.                                     02840000
             05 (P)-MCODTRAV PIC X.                                     02850000
             05 (P)-MCODTRAO PIC X(4).                                  02860000
             05 FILLER    PIC X(2).                                     02870000
             05 (P)-MCICSA PIC X.                                       02880000
             05 (P)-MCICSC PIC X.                                       02890000
             05 (P)-MCICSP PIC X.                                       02900000
             05 (P)-MCICSH PIC X.                                       02910000
             05 (P)-MCICSV PIC X.                                       02920000
             05 (P)-MCICSO PIC X(5).                                    02930000
             05 FILLER    PIC X(2).                                     02940000
             05 (P)-MNETNAMA PIC X.                                     02950000
             05 (P)-MNETNAMC PIC X.                                     02960000
             05 (P)-MNETNAMP PIC X.                                     02970000
             05 (P)-MNETNAMH PIC X.                                     02980000
             05 (P)-MNETNAMV PIC X.                                     02990000
             05 (P)-MNETNAMO PIC X(8).                                  03000000
             05 FILLER    PIC X(2).                                     03010000
             05 (P)-MSCREENA PIC X.                                     03020000
             05 (P)-MSCREENC PIC X.                                     03030000
             05 (P)-MSCREENP PIC X.                                     03040000
             05 (P)-MSCREENH PIC X.                                     03050000
             05 (P)-MSCREENV PIC X.                                     03060000
             05 (P)-MSCREENO PIC X(4).                                  03070000
           03 FILLER REDEFINES (P)-DONNEES-ECRAN.                       03074004
              05 FILLER    PIC X(12).                                   03075004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MDATJOUL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MDATJOUL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MDATJOUF PIC X.                                    03077004
              05 FILLER    PIC X(4).                                    03078004
              05 (P)-MDATJOUI PIC X(10).                                03079004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MTIMJOUL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MTIMJOUL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MTIMJOUF PIC X.                                    03079204
              05 FILLER    PIC X(4).                                    03079304
              05 (P)-MTIMJOUI PIC X(5).                                 03079404
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MNPAGEL COMP PIC S9(4).                                     
      *--                                                                       
              05 (P)-MNPAGEL COMP-5 PIC S9(4).                                  
      *}                                                                        
              05 (P)-MNPAGEF PIC X.                                     03079604
              05 FILLER    PIC X(4).                                    03079704
              05 (P)-MNPAGEI PIC X(2).                                  03079804
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MNCODICL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MNCODICL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MNCODICF PIC X.                                    03080004
              05 FILLER    PIC X(4).                                    03080104
              05 (P)-MNCODICI PIC X(7).                                 03080204
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MREFFOURNL COMP PIC S9(4).                                  
      *--                                                                       
              05 (P)-MREFFOURNL COMP-5 PIC S9(4).                               
      *}                                                                        
              05 (P)-MREFFOURNF PIC X.                                  03080404
              05 FILLER    PIC X(4).                                    03080504
              05 (P)-MREFFOURNI PIC X(20).                              03080604
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MDSIMULL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MDSIMULL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MDSIMULF PIC X.                                    03080804
              05 FILLER    PIC X(4).                                    03080904
              05 (P)-MDSIMULI PIC X(10).                                03081004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MCFAML COMP PIC S9(4).                                      
      *--                                                                       
              05 (P)-MCFAML COMP-5 PIC S9(4).                                   
      *}                                                                        
              05 (P)-MCFAMF PIC X.                                      03081204
              05 FILLER    PIC X(4).                                    03081304
              05 (P)-MCFAMI PIC X(5).                                   03081404
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MLFAML COMP PIC S9(4).                                      
      *--                                                                       
              05 (P)-MLFAML COMP-5 PIC S9(4).                                   
      *}                                                                        
              05 (P)-MLFAMF PIC X.                                      03081604
              05 FILLER    PIC X(4).                                    03081704
              05 (P)-MLFAMI PIC X(20).                                  03081804
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MNSIMULL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MNSIMULL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MNSIMULF PIC X.                                    03082004
              05 FILLER    PIC X(4).                                    03082104
              05 (P)-MNSIMULI PIC X(3).                                 03082204
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MCMARQL COMP PIC S9(4).                                     
      *--                                                                       
              05 (P)-MCMARQL COMP-5 PIC S9(4).                                  
      *}                                                                        
              05 (P)-MCMARQF PIC X.                                     03082404
              05 FILLER    PIC X(4).                                    03082504
              05 (P)-MCMARQI PIC X(5).                                  03082604
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MLMARQL COMP PIC S9(4).                                     
      *--                                                                       
              05 (P)-MLMARQL COMP-5 PIC S9(4).                                  
      *}                                                                        
              05 (P)-MLMARQF PIC X.                                     03082804
              05 FILLER    PIC X(4).                                    03082904
              05 (P)-MLMARQI PIC X(20).                                 03083004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MCGROUPL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MCGROUPL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MCGROUPF PIC X.                                    03083204
              05 FILLER    PIC X(4).                                    03083304
              05 (P)-MCGROUPI PIC X(5).                                 03083404
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MNSOCENL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MNSOCENL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MNSOCENF PIC X.                                    03083604
              05 FILLER    PIC X(4).                                    03083704
              05 (P)-MNSOCENI PIC X(3).                                 03083804
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MNDEPOTL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MNDEPOTL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MNDEPOTF PIC X.                                    03084004
              05 FILLER    PIC X(4).                                    03084104
              05 (P)-MNDEPOTI PIC X(3).                                 03084204
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MQTEDL COMP PIC S9(4).                                      
      *--                                                                       
              05 (P)-MQTEDL COMP-5 PIC S9(4).                                   
      *}                                                                        
              05 (P)-MQTEDF PIC X.                                      03084404
              05 FILLER    PIC X(4).                                    03084504
              05 (P)-MQTEDI PIC X(5).                                   03084604
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MQDISPOL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MQDISPOL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MQDISPOF PIC X.                                    03084804
              05 FILLER    PIC X(4).                                    03084904
              05 (P)-MQDISPOI PIC X(6).                                         
              05 (P)-M8I OCCURS 12 TIMES .                              03085104
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         07 TS-MNSOCL COMP PIC S9(4).                                    
      *--                                                                       
                07 (P)-MNSOCL COMP-5 PIC S9(4).                                 
      *}                                                                        
                07 (P)-MNSOCF PIC X.                                    03085304
                07 FILLER  PIC X(4).                                    03085404
                07 (P)-MNSOCI PIC X(3).                                 03085504
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         07 TS-MNLIEUL  COMP PIC S9(4).                                  
      *--                                                                       
                07 (P)-MNLIEUL COMP-5 PIC S9(4).                                
      *}                                                                        
                07 (P)-MNLIEUF  PIC X.                                  03085704
                07 FILLER  PIC X(4).                                    03085804
                07 (P)-MNLIEUI  PIC X(3).                               03085904
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         07 TS-MQTEL COMP PIC S9(4).                                     
      *--                                                                       
                07 (P)-MQTEL COMP-5 PIC S9(4).                                  
      *}                                                                        
                07 (P)-MQTEF PIC X.                                     03086104
                07 FILLER  PIC X(4).                                    03086204
                07 (P)-MQTEI PIC X(5).                                  03086304
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         07 TS-MWBLOQUEL COMP PIC S9(4).                                 
      *--                                                                       
                07 (P)-MWBLOQUEL COMP-5 PIC S9(4).                              
      *}                                                                        
                07 (P)-MWBLOQUEF PIC X.                                 03086504
                07 FILLER  PIC X(4).                                    03086604
                07 (P)-MWBLOQUEI PIC X.                                 03086704
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MZONCMDL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MZONCMDL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MZONCMDF PIC X.                                    03086904
              05 FILLER    PIC X(4).                                    03087004
              05 (P)-MZONCMDI PIC X(12).                                03087104
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MLIBERRL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MLIBERRL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MLIBERRF PIC X.                                    03087304
              05 FILLER    PIC X(4).                                    03087404
              05 (P)-MLIBERRI PIC X(61).                                03087504
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MCODTRAL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MCODTRAL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MCODTRAF PIC X.                                    03087704
              05 FILLER    PIC X(4).                                    03087804
              05 (P)-MCODTRAI PIC X(4).                                 03087904
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MCICSL COMP PIC S9(4).                                      
      *--                                                                       
              05 (P)-MCICSL COMP-5 PIC S9(4).                                   
      *}                                                                        
              05 (P)-MCICSF PIC X.                                      03088104
              05 FILLER    PIC X(4).                                    03088204
              05 (P)-MCICSI PIC X(5).                                   03088304
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MNETNAML COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MNETNAML COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MNETNAMF PIC X.                                    03088504
              05 FILLER    PIC X(4).                                    03088604
              05 (P)-MNETNAMI PIC X(8).                                 03088704
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 TS-MSCREENL COMP PIC S9(4).                                    
      *--                                                                       
              05 (P)-MSCREENL COMP-5 PIC S9(4).                                 
      *}                                                                        
              05 (P)-MSCREENF PIC X.                                    03088904
              05 FILLER    PIC X(4).                                    03089004
              05 (P)-MSCREENI PIC X(4).                                 03089104
          03 (P)-DONNEES-MUT.                                           03089200
             05  (P)-DONNEES-MUT-LIGNE    OCCURS 12.                    03090000
                 07 (P)-NMUTATION              PIC X(7).                03100000
                 07 (P)-DMUTATION              PIC X(8).                03101009
                 07 (P)-CSELART                PIC X(5).                03110000
                 07 (P)-QNBM3QUOTA             PIC S9(11) COMP-3.       03120000
                 07 (P)-M3QUOTA                PIC S9(3)V99 COMP-3.     03130000
                 07 (P)-QNBPQUOTA              PIC S9(5) COMP-3.        03140000
                 07 (P)-QSAISIE                PIC S9(5) COMP-3.        03180000
                                                                                
