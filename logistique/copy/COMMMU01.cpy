      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
000010**************************************************************    00010016
000020* COMMAREA SPECIFIQUE PRG TMU01 (TMU00 -> MENU)    TR: MU00  *    00020016
000030*               GESTION DES MUTATIONS                                   16
000040*                                                                 00040016
000050* ZONES RESERVEES APPLICATIVES ---------------------------- 7707  00050016
000060*                                                                 00060016
000070*        TRANSACTION MU01 : MAJ DES DATES DE MUTATIONS                  16
000080*                                                                 00080016
000090    02 COMM-MU01-APPLI REDEFINES COMM-OPTION1.                    00090016
000091*------------------------------ ZONE DONNEES TMU01                00100016
000092       04 COMM-MU01-DONNEES-TMU01.                                00110016
000093*------------------------------                                   00120016
000095           05  COMM-MU01-NPAGE           PIC 9(02).               00170016
000096           05  COMM-MU01-NPAGMAX         PIC 9(02).               00190016
000097           05  COMM-MU01-NLIGMAX         PIC 9(02).               00190016
000098           05  COMM-MU01-NJOURPROT       PIC 9(03).               00190016
000098           05  COMM-MU01-LISTE-VIDE      PIC X(01).               00190016
000099           05  COMM-MU01-NAAGAUCHE       PIC 9(04).               00190016
000100           05  COMM-MU01-NSEMGAUCHE      PIC 9(02).               00190016
000101           05  COMM-MU01-DDEBUT-QU       PIC 9(05).               00190016
000102           05  COMM-MU01-DDEBUT-ET       PIC X(08).               00190016
000103           05  COMM-MU01-ENTETE.                                        16
000104               06  COMM-MU01-JJMM    OCCURS 2.                            
000105                   10  COMM-MU01-ATZONE  OCCURS 21.                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000106*                      15  COMM-MU01-L   PIC S9(4) COMP.                  
      *--                                                                       
                             15  COMM-MU01-L   PIC S9(4) COMP-5.                
      *}                                                                        
000107                       15  COMM-MU01-A   PIC X(01).                       
000108                       15  COMM-MU01-C   PIC X(01).                       
000109                       15  COMM-MU01-P   PIC X(01).                       
000110                       15  COMM-MU01-H   PIC X(01).                       
000111                       15  COMM-MU01-V   PIC X(01).                       
000112                       15  COMM-MU01-JM  PIC X(02).                       
000103           05  COMM-MU01-JOURS-FERMES.                                  16
000105               06  COMM-MU01-JMF      OCCURS 21  PIC X(02).               
000113           05  COMM-MU01-PROF1.                                           
000114               06  COMM-MU01-OUV1            PIC X(01).                   
000115               06  COMM-MU01-FER1            PIC X(01).                   
000116               06  COMM-MU01-DES1            PIC X(01).                   
000116               06  COMM-MU01-CHA1            PIC X(01).                   
000117           05  COMM-MU01-PROF2.                                           
000118               06  COMM-MU01-OUV2            PIC X(01).                   
000119               06  COMM-MU01-FER2            PIC X(01).                   
000120               06  COMM-MU01-DES2            PIC X(01).                   
000120               06  COMM-MU01-CHA2            PIC X(01).                   
000121           05  COMM-MU01-PROF3.                                           
000122               06  COMM-MU01-OUV3            PIC X(01).                   
000123               06  COMM-MU01-FER3            PIC X(01).                   
000124               06  COMM-MU01-DES3            PIC X(01).                   
000124               06  COMM-MU01-CHA3            PIC X(01).                   
000125           05  COMM-MU01-PROF4.                                           
000126               06  COMM-MU01-OUV4            PIC X(01).                   
000127               06  COMM-MU01-FER4            PIC X(01).                   
000128               06  COMM-MU01-DES4            PIC X(01).                   
000128               06  COMM-MU01-CHA4            PIC X(01).                   
000129           05  COMM-MU01-ATTR            PIC X(01)  OCCURS 630.         16
                 05  COMM-MU01-TABLE               OCCURS 500.                  
                     06  COMM-MU01-NSOCIETE        PIC X(3).                    
                     06  COMM-MU01-NLIEU           PIC X(3).                    
000130           05  COMM-MU01-INDICE              PIC S9(3) COMP-3.          16
000131*------------------------------ ZONE LIBRE                        00650018
000132*                                                                 00660016
000133       04 COMM-MU01-LIBRE             PIC X(3610).                00670030
000140*                                                                 00680016
                                                                                
