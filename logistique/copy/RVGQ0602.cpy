      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RTGQ06 (REGROUPE TOUTES LES FILIALES)                
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGQ0602                         
      **********************************************************                
       01  RVGQ0602.                                                            
           02  GQ06-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GQ06-CTYPE                                                       
               PIC X(0001).                                                     
           02  GQ06-CINSEE                                                      
               PIC X(0005).                                                     
           02  GQ06-WPARCOM                                                     
               PIC X(0001).                                                     
           02  GQ06-LCOMUNE                                                     
               PIC X(0032).                                                     
           02  GQ06-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  GQ06-WPARBUR                                                     
               PIC X(0001).                                                     
           02  GQ06-LBUREAU                                                     
               PIC X(0026).                                                     
           02  GQ06-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGQ0602                                  
      **********************************************************                
       01  RVGQ0602-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06-CTYPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06-CTYPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06-WPARCOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06-WPARCOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06-LCOMUNE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06-LCOMUNE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06-WPARBUR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06-WPARBUR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06-LBUREAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06-LBUREAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GQ06-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
