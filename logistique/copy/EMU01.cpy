      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU01   EMU01                                              00000020
      ***************************************************************** 00000030
       01   EMU01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTSOCL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNENTSOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTSOCF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNENTSOCI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTLIEUL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNENTLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTLIEUF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNENTLIEUI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELARTL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCSELARTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSELARTF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCSELARTI      PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILO1L    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNPROFILO1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILO1F    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNPROFILO1I    PIC X.                                     00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILF1L    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNPROFILF1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILF1F    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNPROFILF1I    PIC X.                                     00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILD1L    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNPROFILD1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILD1F    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNPROFILD1I    PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILC1L    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNPROFILC1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILC1F    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNPROFILC1I    PIC X.                                     00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILO2L    COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNPROFILO2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILO2F    PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNPROFILO2I    PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILF2L    COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNPROFILF2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILF2F    PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNPROFILF2I    PIC X.                                     00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILD2L    COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MNPROFILD2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILD2F    PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNPROFILD2I    PIC X.                                     00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILC2L    COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNPROFILC2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILC2F    PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNPROFILC2I    PIC X.                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGSOCL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MNMAGSOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMAGSOCF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNMAGSOCI      PIC X(3).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGLIEUL     COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNMAGLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNMAGLIEUF     PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNMAGLIEUI     PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGLIBL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MNMAGLIBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMAGLIBF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNMAGLIBI      PIC X(20).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILO3L    COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MNPROFILO3L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILO3F    PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNPROFILO3I    PIC X.                                     00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILF3L    COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MNPROFILF3L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILF3F    PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNPROFILF3I    PIC X.                                     00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILD3L    COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MNPROFILD3L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILD3F    PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNPROFILD3I    PIC X.                                     00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILC3L    COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MNPROFILC3L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILC3F    PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MNPROFILC3I    PIC X.                                     00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILO4L    COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MNPROFILO4L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILO4F    PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNPROFILO4I    PIC X.                                     00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILF4L    COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MNPROFILF4L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILF4F    PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNPROFILF4I    PIC X.                                     00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILD4L    COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MNPROFILD4L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILD4F    PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MNPROFILD4I    PIC X.                                     00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPROFILC4L    COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MNPROFILC4L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPROFILC4F    PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MNPROFILC4I    PIC X.                                     00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNEE1L      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MDANNEE1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDANNEE1F      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MDANNEE1I      PIC X(4).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSEMAINE1L    COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MDSEMAINE1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDSEMAINE1F    PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MDSEMAINE1I    PIC X(2).                                  00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNEE2L      COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MDANNEE2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDANNEE2F      PIC X.                                     00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MDANNEE2I      PIC X(4).                                  00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSEMAINE2L    COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MDSEMAINE2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDSEMAINE2F    PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MDSEMAINE2I    PIC X(2).                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNEE3L      COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MDANNEE3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDANNEE3F      PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MDANNEE3I      PIC X(4).                                  00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSEMAINE3L    COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MDSEMAINE3L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDSEMAINE3F    PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MDSEMAINE3I    PIC X(2).                                  00001290
           02 MJJMMI OCCURS   2 TIMES .                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMLUN1L    COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MDJMLUN1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMLUN1F    PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MDJMLUN1I    PIC X(2).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMAR1L    COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MDJMMAR1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMAR1F    PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MDJMMAR1I    PIC X(2).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMER1L    COMP PIC S9(4).                            00001390
      *--                                                                       
             03 MDJMMER1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMER1F    PIC X.                                     00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MDJMMER1I    PIC X(2).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMJEU1L    COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MDJMJEU1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMJEU1F    PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MDJMJEU1I    PIC X(2).                                  00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMVEN1L    COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MDJMVEN1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMVEN1F    PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MDJMVEN1I    PIC X(2).                                  00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMSAM1L    COMP PIC S9(4).                            00001510
      *--                                                                       
             03 MDJMSAM1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMSAM1F    PIC X.                                     00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MDJMSAM1I    PIC X(2).                                  00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMDIM1L    COMP PIC S9(4).                            00001550
      *--                                                                       
             03 MDJMDIM1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMDIM1F    PIC X.                                     00001560
             03 FILLER  PIC X(4).                                       00001570
             03 MDJMDIM1I    PIC X(2).                                  00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMLUN2L    COMP PIC S9(4).                            00001590
      *--                                                                       
             03 MDJMLUN2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMLUN2F    PIC X.                                     00001600
             03 FILLER  PIC X(4).                                       00001610
             03 MDJMLUN2I    PIC X(2).                                  00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMAR2L    COMP PIC S9(4).                            00001630
      *--                                                                       
             03 MDJMMAR2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMAR2F    PIC X.                                     00001640
             03 FILLER  PIC X(4).                                       00001650
             03 MDJMMAR2I    PIC X(2).                                  00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMER2L    COMP PIC S9(4).                            00001670
      *--                                                                       
             03 MDJMMER2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMER2F    PIC X.                                     00001680
             03 FILLER  PIC X(4).                                       00001690
             03 MDJMMER2I    PIC X(2).                                  00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMJEU2L    COMP PIC S9(4).                            00001710
      *--                                                                       
             03 MDJMJEU2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMJEU2F    PIC X.                                     00001720
             03 FILLER  PIC X(4).                                       00001730
             03 MDJMJEU2I    PIC X(2).                                  00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMVEN2L    COMP PIC S9(4).                            00001750
      *--                                                                       
             03 MDJMVEN2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMVEN2F    PIC X.                                     00001760
             03 FILLER  PIC X(4).                                       00001770
             03 MDJMVEN2I    PIC X(2).                                  00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMSAM2L    COMP PIC S9(4).                            00001790
      *--                                                                       
             03 MDJMSAM2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMSAM2F    PIC X.                                     00001800
             03 FILLER  PIC X(4).                                       00001810
             03 MDJMSAM2I    PIC X(2).                                  00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMDIM2L    COMP PIC S9(4).                            00001830
      *--                                                                       
             03 MDJMDIM2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMDIM2F    PIC X.                                     00001840
             03 FILLER  PIC X(4).                                       00001850
             03 MDJMDIM2I    PIC X(2).                                  00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMLUN3L    COMP PIC S9(4).                            00001870
      *--                                                                       
             03 MDJMLUN3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMLUN3F    PIC X.                                     00001880
             03 FILLER  PIC X(4).                                       00001890
             03 MDJMLUN3I    PIC X(2).                                  00001900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMAR3L    COMP PIC S9(4).                            00001910
      *--                                                                       
             03 MDJMMAR3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMAR3F    PIC X.                                     00001920
             03 FILLER  PIC X(4).                                       00001930
             03 MDJMMAR3I    PIC X(2).                                  00001940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMER3L    COMP PIC S9(4).                            00001950
      *--                                                                       
             03 MDJMMER3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMER3F    PIC X.                                     00001960
             03 FILLER  PIC X(4).                                       00001970
             03 MDJMMER3I    PIC X(2).                                  00001980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMJEU3L    COMP PIC S9(4).                            00001990
      *--                                                                       
             03 MDJMJEU3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMJEU3F    PIC X.                                     00002000
             03 FILLER  PIC X(4).                                       00002010
             03 MDJMJEU3I    PIC X(2).                                  00002020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMVEN3L    COMP PIC S9(4).                            00002030
      *--                                                                       
             03 MDJMVEN3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMVEN3F    PIC X.                                     00002040
             03 FILLER  PIC X(4).                                       00002050
             03 MDJMVEN3I    PIC X(2).                                  00002060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMSAM3L    COMP PIC S9(4).                            00002070
      *--                                                                       
             03 MDJMSAM3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMSAM3F    PIC X.                                     00002080
             03 FILLER  PIC X(4).                                       00002090
             03 MDJMSAM3I    PIC X(2).                                  00002100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMDIM3L    COMP PIC S9(4).                            00002110
      *--                                                                       
             03 MDJMDIM3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMDIM3F    PIC X.                                     00002120
             03 FILLER  PIC X(4).                                       00002130
             03 MDJMDIM3I    PIC X(2).                                  00002140
           02 MCODESI OCCURS   8 TIMES .                                00002150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODLUN1L    COMP PIC S9(4).                            00002160
      *--                                                                       
             03 MCODLUN1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODLUN1F    PIC X.                                     00002170
             03 FILLER  PIC X(4).                                       00002180
             03 MCODLUN1I    PIC X(2).                                  00002190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMAR1L    COMP PIC S9(4).                            00002200
      *--                                                                       
             03 MCODMAR1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMAR1F    PIC X.                                     00002210
             03 FILLER  PIC X(4).                                       00002220
             03 MCODMAR1I    PIC X(2).                                  00002230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMER1L    COMP PIC S9(4).                            00002240
      *--                                                                       
             03 MCODMER1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMER1F    PIC X.                                     00002250
             03 FILLER  PIC X(4).                                       00002260
             03 MCODMER1I    PIC X(2).                                  00002270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODJEU1L    COMP PIC S9(4).                            00002280
      *--                                                                       
             03 MCODJEU1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODJEU1F    PIC X.                                     00002290
             03 FILLER  PIC X(4).                                       00002300
             03 MCODJEU1I    PIC X(2).                                  00002310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODVEN1L    COMP PIC S9(4).                            00002320
      *--                                                                       
             03 MCODVEN1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODVEN1F    PIC X.                                     00002330
             03 FILLER  PIC X(4).                                       00002340
             03 MCODVEN1I    PIC X(2).                                  00002350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODSAM1L    COMP PIC S9(4).                            00002360
      *--                                                                       
             03 MCODSAM1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODSAM1F    PIC X.                                     00002370
             03 FILLER  PIC X(4).                                       00002380
             03 MCODSAM1I    PIC X(2).                                  00002390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODDIM1L    COMP PIC S9(4).                            00002400
      *--                                                                       
             03 MCODDIM1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODDIM1F    PIC X.                                     00002410
             03 FILLER  PIC X(4).                                       00002420
             03 MCODDIM1I    PIC X(2).                                  00002430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODLUN2L    COMP PIC S9(4).                            00002440
      *--                                                                       
             03 MCODLUN2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODLUN2F    PIC X.                                     00002450
             03 FILLER  PIC X(4).                                       00002460
             03 MCODLUN2I    PIC X(2).                                  00002470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMAR2L    COMP PIC S9(4).                            00002480
      *--                                                                       
             03 MCODMAR2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMAR2F    PIC X.                                     00002490
             03 FILLER  PIC X(4).                                       00002500
             03 MCODMAR2I    PIC X(2).                                  00002510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMER2L    COMP PIC S9(4).                            00002520
      *--                                                                       
             03 MCODMER2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMER2F    PIC X.                                     00002530
             03 FILLER  PIC X(4).                                       00002540
             03 MCODMER2I    PIC X(2).                                  00002550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODJEU2L    COMP PIC S9(4).                            00002560
      *--                                                                       
             03 MCODJEU2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODJEU2F    PIC X.                                     00002570
             03 FILLER  PIC X(4).                                       00002580
             03 MCODJEU2I    PIC X(2).                                  00002590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODVEN2L    COMP PIC S9(4).                            00002600
      *--                                                                       
             03 MCODVEN2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODVEN2F    PIC X.                                     00002610
             03 FILLER  PIC X(4).                                       00002620
             03 MCODVEN2I    PIC X(2).                                  00002630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODSAM2L    COMP PIC S9(4).                            00002640
      *--                                                                       
             03 MCODSAM2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODSAM2F    PIC X.                                     00002650
             03 FILLER  PIC X(4).                                       00002660
             03 MCODSAM2I    PIC X(2).                                  00002670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODDIM2L    COMP PIC S9(4).                            00002680
      *--                                                                       
             03 MCODDIM2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODDIM2F    PIC X.                                     00002690
             03 FILLER  PIC X(4).                                       00002700
             03 MCODDIM2I    PIC X(2).                                  00002710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODLUN3L    COMP PIC S9(4).                            00002720
      *--                                                                       
             03 MCODLUN3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODLUN3F    PIC X.                                     00002730
             03 FILLER  PIC X(4).                                       00002740
             03 MCODLUN3I    PIC X(2).                                  00002750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMAR3L    COMP PIC S9(4).                            00002760
      *--                                                                       
             03 MCODMAR3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMAR3F    PIC X.                                     00002770
             03 FILLER  PIC X(4).                                       00002780
             03 MCODMAR3I    PIC X(2).                                  00002790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMER3L    COMP PIC S9(4).                            00002800
      *--                                                                       
             03 MCODMER3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMER3F    PIC X.                                     00002810
             03 FILLER  PIC X(4).                                       00002820
             03 MCODMER3I    PIC X(2).                                  00002830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODJEU3L    COMP PIC S9(4).                            00002840
      *--                                                                       
             03 MCODJEU3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODJEU3F    PIC X.                                     00002850
             03 FILLER  PIC X(4).                                       00002860
             03 MCODJEU3I    PIC X(2).                                  00002870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODVEN3L    COMP PIC S9(4).                            00002880
      *--                                                                       
             03 MCODVEN3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODVEN3F    PIC X.                                     00002890
             03 FILLER  PIC X(4).                                       00002900
             03 MCODVEN3I    PIC X(2).                                  00002910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODSAM3L    COMP PIC S9(4).                            00002920
      *--                                                                       
             03 MCODSAM3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODSAM3F    PIC X.                                     00002930
             03 FILLER  PIC X(4).                                       00002940
             03 MCODSAM3I    PIC X(2).                                  00002950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODDIM3L    COMP PIC S9(4).                            00002960
      *--                                                                       
             03 MCODDIM3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODDIM3F    PIC X.                                     00002970
             03 FILLER  PIC X(4).                                       00002980
             03 MCODDIM3I    PIC X(2).                                  00002990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00003000
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00003010
           02 FILLER    PIC X(4).                                       00003020
           02 MZONCMDI  PIC X(15).                                      00003030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00003040
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00003050
           02 FILLER    PIC X(4).                                       00003060
           02 MLIBERRI  PIC X(58).                                      00003070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00003080
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00003090
           02 FILLER    PIC X(4).                                       00003100
           02 MCODTRAI  PIC X(4).                                       00003110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00003120
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00003130
           02 FILLER    PIC X(4).                                       00003140
           02 MCICSI    PIC X(5).                                       00003150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00003160
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00003170
           02 FILLER    PIC X(4).                                       00003180
           02 MNETNAMI  PIC X(8).                                       00003190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00003200
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00003210
           02 FILLER    PIC X(4).                                       00003220
           02 MSCREENI  PIC X(4).                                       00003230
      ***************************************************************** 00003240
      * SDF: EMU01   EMU01                                              00003250
      ***************************************************************** 00003260
       01   EMU01O REDEFINES EMU01I.                                    00003270
           02 FILLER    PIC X(12).                                      00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MDATJOUA  PIC X.                                          00003300
           02 MDATJOUC  PIC X.                                          00003310
           02 MDATJOUP  PIC X.                                          00003320
           02 MDATJOUH  PIC X.                                          00003330
           02 MDATJOUV  PIC X.                                          00003340
           02 MDATJOUO  PIC X(10).                                      00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MTIMJOUA  PIC X.                                          00003370
           02 MTIMJOUC  PIC X.                                          00003380
           02 MTIMJOUP  PIC X.                                          00003390
           02 MTIMJOUH  PIC X.                                          00003400
           02 MTIMJOUV  PIC X.                                          00003410
           02 MTIMJOUO  PIC X(5).                                       00003420
           02 FILLER    PIC X(2).                                       00003430
           02 MNPAGEA   PIC X.                                          00003440
           02 MNPAGEC   PIC X.                                          00003450
           02 MNPAGEP   PIC X.                                          00003460
           02 MNPAGEH   PIC X.                                          00003470
           02 MNPAGEV   PIC X.                                          00003480
           02 MNPAGEO   PIC X(2).                                       00003490
           02 FILLER    PIC X(2).                                       00003500
           02 MNENTSOCA      PIC X.                                     00003510
           02 MNENTSOCC PIC X.                                          00003520
           02 MNENTSOCP PIC X.                                          00003530
           02 MNENTSOCH PIC X.                                          00003540
           02 MNENTSOCV PIC X.                                          00003550
           02 MNENTSOCO      PIC X(3).                                  00003560
           02 FILLER    PIC X(2).                                       00003570
           02 MNENTLIEUA     PIC X.                                     00003580
           02 MNENTLIEUC     PIC X.                                     00003590
           02 MNENTLIEUP     PIC X.                                     00003600
           02 MNENTLIEUH     PIC X.                                     00003610
           02 MNENTLIEUV     PIC X.                                     00003620
           02 MNENTLIEUO     PIC X(3).                                  00003630
           02 FILLER    PIC X(2).                                       00003640
           02 MCSELARTA      PIC X.                                     00003650
           02 MCSELARTC PIC X.                                          00003660
           02 MCSELARTP PIC X.                                          00003670
           02 MCSELARTH PIC X.                                          00003680
           02 MCSELARTV PIC X.                                          00003690
           02 MCSELARTO      PIC X(5).                                  00003700
           02 FILLER    PIC X(2).                                       00003710
           02 MNPROFILO1A    PIC X.                                     00003720
           02 MNPROFILO1C    PIC X.                                     00003730
           02 MNPROFILO1P    PIC X.                                     00003740
           02 MNPROFILO1H    PIC X.                                     00003750
           02 MNPROFILO1V    PIC X.                                     00003760
           02 MNPROFILO1O    PIC X.                                     00003770
           02 FILLER    PIC X(2).                                       00003780
           02 MNPROFILF1A    PIC X.                                     00003790
           02 MNPROFILF1C    PIC X.                                     00003800
           02 MNPROFILF1P    PIC X.                                     00003810
           02 MNPROFILF1H    PIC X.                                     00003820
           02 MNPROFILF1V    PIC X.                                     00003830
           02 MNPROFILF1O    PIC X.                                     00003840
           02 FILLER    PIC X(2).                                       00003850
           02 MNPROFILD1A    PIC X.                                     00003860
           02 MNPROFILD1C    PIC X.                                     00003870
           02 MNPROFILD1P    PIC X.                                     00003880
           02 MNPROFILD1H    PIC X.                                     00003890
           02 MNPROFILD1V    PIC X.                                     00003900
           02 MNPROFILD1O    PIC X.                                     00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MNPROFILC1A    PIC X.                                     00003930
           02 MNPROFILC1C    PIC X.                                     00003940
           02 MNPROFILC1P    PIC X.                                     00003950
           02 MNPROFILC1H    PIC X.                                     00003960
           02 MNPROFILC1V    PIC X.                                     00003970
           02 MNPROFILC1O    PIC X.                                     00003980
           02 FILLER    PIC X(2).                                       00003990
           02 MNPROFILO2A    PIC X.                                     00004000
           02 MNPROFILO2C    PIC X.                                     00004010
           02 MNPROFILO2P    PIC X.                                     00004020
           02 MNPROFILO2H    PIC X.                                     00004030
           02 MNPROFILO2V    PIC X.                                     00004040
           02 MNPROFILO2O    PIC X.                                     00004050
           02 FILLER    PIC X(2).                                       00004060
           02 MNPROFILF2A    PIC X.                                     00004070
           02 MNPROFILF2C    PIC X.                                     00004080
           02 MNPROFILF2P    PIC X.                                     00004090
           02 MNPROFILF2H    PIC X.                                     00004100
           02 MNPROFILF2V    PIC X.                                     00004110
           02 MNPROFILF2O    PIC X.                                     00004120
           02 FILLER    PIC X(2).                                       00004130
           02 MNPROFILD2A    PIC X.                                     00004140
           02 MNPROFILD2C    PIC X.                                     00004150
           02 MNPROFILD2P    PIC X.                                     00004160
           02 MNPROFILD2H    PIC X.                                     00004170
           02 MNPROFILD2V    PIC X.                                     00004180
           02 MNPROFILD2O    PIC X.                                     00004190
           02 FILLER    PIC X(2).                                       00004200
           02 MNPROFILC2A    PIC X.                                     00004210
           02 MNPROFILC2C    PIC X.                                     00004220
           02 MNPROFILC2P    PIC X.                                     00004230
           02 MNPROFILC2H    PIC X.                                     00004240
           02 MNPROFILC2V    PIC X.                                     00004250
           02 MNPROFILC2O    PIC X.                                     00004260
           02 FILLER    PIC X(2).                                       00004270
           02 MNMAGSOCA      PIC X.                                     00004280
           02 MNMAGSOCC PIC X.                                          00004290
           02 MNMAGSOCP PIC X.                                          00004300
           02 MNMAGSOCH PIC X.                                          00004310
           02 MNMAGSOCV PIC X.                                          00004320
           02 MNMAGSOCO      PIC X(3).                                  00004330
           02 FILLER    PIC X(2).                                       00004340
           02 MNMAGLIEUA     PIC X.                                     00004350
           02 MNMAGLIEUC     PIC X.                                     00004360
           02 MNMAGLIEUP     PIC X.                                     00004370
           02 MNMAGLIEUH     PIC X.                                     00004380
           02 MNMAGLIEUV     PIC X.                                     00004390
           02 MNMAGLIEUO     PIC X(3).                                  00004400
           02 FILLER    PIC X(2).                                       00004410
           02 MNMAGLIBA      PIC X.                                     00004420
           02 MNMAGLIBC PIC X.                                          00004430
           02 MNMAGLIBP PIC X.                                          00004440
           02 MNMAGLIBH PIC X.                                          00004450
           02 MNMAGLIBV PIC X.                                          00004460
           02 MNMAGLIBO      PIC X(20).                                 00004470
           02 FILLER    PIC X(2).                                       00004480
           02 MNPROFILO3A    PIC X.                                     00004490
           02 MNPROFILO3C    PIC X.                                     00004500
           02 MNPROFILO3P    PIC X.                                     00004510
           02 MNPROFILO3H    PIC X.                                     00004520
           02 MNPROFILO3V    PIC X.                                     00004530
           02 MNPROFILO3O    PIC X.                                     00004540
           02 FILLER    PIC X(2).                                       00004550
           02 MNPROFILF3A    PIC X.                                     00004560
           02 MNPROFILF3C    PIC X.                                     00004570
           02 MNPROFILF3P    PIC X.                                     00004580
           02 MNPROFILF3H    PIC X.                                     00004590
           02 MNPROFILF3V    PIC X.                                     00004600
           02 MNPROFILF3O    PIC X.                                     00004610
           02 FILLER    PIC X(2).                                       00004620
           02 MNPROFILD3A    PIC X.                                     00004630
           02 MNPROFILD3C    PIC X.                                     00004640
           02 MNPROFILD3P    PIC X.                                     00004650
           02 MNPROFILD3H    PIC X.                                     00004660
           02 MNPROFILD3V    PIC X.                                     00004670
           02 MNPROFILD3O    PIC X.                                     00004680
           02 FILLER    PIC X(2).                                       00004690
           02 MNPROFILC3A    PIC X.                                     00004700
           02 MNPROFILC3C    PIC X.                                     00004710
           02 MNPROFILC3P    PIC X.                                     00004720
           02 MNPROFILC3H    PIC X.                                     00004730
           02 MNPROFILC3V    PIC X.                                     00004740
           02 MNPROFILC3O    PIC X.                                     00004750
           02 FILLER    PIC X(2).                                       00004760
           02 MNPROFILO4A    PIC X.                                     00004770
           02 MNPROFILO4C    PIC X.                                     00004780
           02 MNPROFILO4P    PIC X.                                     00004790
           02 MNPROFILO4H    PIC X.                                     00004800
           02 MNPROFILO4V    PIC X.                                     00004810
           02 MNPROFILO4O    PIC X.                                     00004820
           02 FILLER    PIC X(2).                                       00004830
           02 MNPROFILF4A    PIC X.                                     00004840
           02 MNPROFILF4C    PIC X.                                     00004850
           02 MNPROFILF4P    PIC X.                                     00004860
           02 MNPROFILF4H    PIC X.                                     00004870
           02 MNPROFILF4V    PIC X.                                     00004880
           02 MNPROFILF4O    PIC X.                                     00004890
           02 FILLER    PIC X(2).                                       00004900
           02 MNPROFILD4A    PIC X.                                     00004910
           02 MNPROFILD4C    PIC X.                                     00004920
           02 MNPROFILD4P    PIC X.                                     00004930
           02 MNPROFILD4H    PIC X.                                     00004940
           02 MNPROFILD4V    PIC X.                                     00004950
           02 MNPROFILD4O    PIC X.                                     00004960
           02 FILLER    PIC X(2).                                       00004970
           02 MNPROFILC4A    PIC X.                                     00004980
           02 MNPROFILC4C    PIC X.                                     00004990
           02 MNPROFILC4P    PIC X.                                     00005000
           02 MNPROFILC4H    PIC X.                                     00005010
           02 MNPROFILC4V    PIC X.                                     00005020
           02 MNPROFILC4O    PIC X.                                     00005030
           02 FILLER    PIC X(2).                                       00005040
           02 MDANNEE1A      PIC X.                                     00005050
           02 MDANNEE1C PIC X.                                          00005060
           02 MDANNEE1P PIC X.                                          00005070
           02 MDANNEE1H PIC X.                                          00005080
           02 MDANNEE1V PIC X.                                          00005090
           02 MDANNEE1O      PIC X(4).                                  00005100
           02 FILLER    PIC X(2).                                       00005110
           02 MDSEMAINE1A    PIC X.                                     00005120
           02 MDSEMAINE1C    PIC X.                                     00005130
           02 MDSEMAINE1P    PIC X.                                     00005140
           02 MDSEMAINE1H    PIC X.                                     00005150
           02 MDSEMAINE1V    PIC X.                                     00005160
           02 MDSEMAINE1O    PIC X(2).                                  00005170
           02 FILLER    PIC X(2).                                       00005180
           02 MDANNEE2A      PIC X.                                     00005190
           02 MDANNEE2C PIC X.                                          00005200
           02 MDANNEE2P PIC X.                                          00005210
           02 MDANNEE2H PIC X.                                          00005220
           02 MDANNEE2V PIC X.                                          00005230
           02 MDANNEE2O      PIC X(4).                                  00005240
           02 FILLER    PIC X(2).                                       00005250
           02 MDSEMAINE2A    PIC X.                                     00005260
           02 MDSEMAINE2C    PIC X.                                     00005270
           02 MDSEMAINE2P    PIC X.                                     00005280
           02 MDSEMAINE2H    PIC X.                                     00005290
           02 MDSEMAINE2V    PIC X.                                     00005300
           02 MDSEMAINE2O    PIC X(2).                                  00005310
           02 FILLER    PIC X(2).                                       00005320
           02 MDANNEE3A      PIC X.                                     00005330
           02 MDANNEE3C PIC X.                                          00005340
           02 MDANNEE3P PIC X.                                          00005350
           02 MDANNEE3H PIC X.                                          00005360
           02 MDANNEE3V PIC X.                                          00005370
           02 MDANNEE3O      PIC X(4).                                  00005380
           02 FILLER    PIC X(2).                                       00005390
           02 MDSEMAINE3A    PIC X.                                     00005400
           02 MDSEMAINE3C    PIC X.                                     00005410
           02 MDSEMAINE3P    PIC X.                                     00005420
           02 MDSEMAINE3H    PIC X.                                     00005430
           02 MDSEMAINE3V    PIC X.                                     00005440
           02 MDSEMAINE3O    PIC X(2).                                  00005450
           02 MJJMMO OCCURS   2 TIMES .                                 00005460
             03 FILLER       PIC X(2).                                  00005470
             03 MDJMLUN1A    PIC X.                                     00005480
             03 MDJMLUN1C    PIC X.                                     00005490
             03 MDJMLUN1P    PIC X.                                     00005500
             03 MDJMLUN1H    PIC X.                                     00005510
             03 MDJMLUN1V    PIC X.                                     00005520
             03 MDJMLUN1O    PIC X(2).                                  00005530
             03 FILLER       PIC X(2).                                  00005540
             03 MDJMMAR1A    PIC X.                                     00005550
             03 MDJMMAR1C    PIC X.                                     00005560
             03 MDJMMAR1P    PIC X.                                     00005570
             03 MDJMMAR1H    PIC X.                                     00005580
             03 MDJMMAR1V    PIC X.                                     00005590
             03 MDJMMAR1O    PIC X(2).                                  00005600
             03 FILLER       PIC X(2).                                  00005610
             03 MDJMMER1A    PIC X.                                     00005620
             03 MDJMMER1C    PIC X.                                     00005630
             03 MDJMMER1P    PIC X.                                     00005640
             03 MDJMMER1H    PIC X.                                     00005650
             03 MDJMMER1V    PIC X.                                     00005660
             03 MDJMMER1O    PIC X(2).                                  00005670
             03 FILLER       PIC X(2).                                  00005680
             03 MDJMJEU1A    PIC X.                                     00005690
             03 MDJMJEU1C    PIC X.                                     00005700
             03 MDJMJEU1P    PIC X.                                     00005710
             03 MDJMJEU1H    PIC X.                                     00005720
             03 MDJMJEU1V    PIC X.                                     00005730
             03 MDJMJEU1O    PIC X(2).                                  00005740
             03 FILLER       PIC X(2).                                  00005750
             03 MDJMVEN1A    PIC X.                                     00005760
             03 MDJMVEN1C    PIC X.                                     00005770
             03 MDJMVEN1P    PIC X.                                     00005780
             03 MDJMVEN1H    PIC X.                                     00005790
             03 MDJMVEN1V    PIC X.                                     00005800
             03 MDJMVEN1O    PIC X(2).                                  00005810
             03 FILLER       PIC X(2).                                  00005820
             03 MDJMSAM1A    PIC X.                                     00005830
             03 MDJMSAM1C    PIC X.                                     00005840
             03 MDJMSAM1P    PIC X.                                     00005850
             03 MDJMSAM1H    PIC X.                                     00005860
             03 MDJMSAM1V    PIC X.                                     00005870
             03 MDJMSAM1O    PIC X(2).                                  00005880
             03 FILLER       PIC X(2).                                  00005890
             03 MDJMDIM1A    PIC X.                                     00005900
             03 MDJMDIM1C    PIC X.                                     00005910
             03 MDJMDIM1P    PIC X.                                     00005920
             03 MDJMDIM1H    PIC X.                                     00005930
             03 MDJMDIM1V    PIC X.                                     00005940
             03 MDJMDIM1O    PIC X(2).                                  00005950
             03 FILLER       PIC X(2).                                  00005960
             03 MDJMLUN2A    PIC X.                                     00005970
             03 MDJMLUN2C    PIC X.                                     00005980
             03 MDJMLUN2P    PIC X.                                     00005990
             03 MDJMLUN2H    PIC X.                                     00006000
             03 MDJMLUN2V    PIC X.                                     00006010
             03 MDJMLUN2O    PIC X(2).                                  00006020
             03 FILLER       PIC X(2).                                  00006030
             03 MDJMMAR2A    PIC X.                                     00006040
             03 MDJMMAR2C    PIC X.                                     00006050
             03 MDJMMAR2P    PIC X.                                     00006060
             03 MDJMMAR2H    PIC X.                                     00006070
             03 MDJMMAR2V    PIC X.                                     00006080
             03 MDJMMAR2O    PIC X(2).                                  00006090
             03 FILLER       PIC X(2).                                  00006100
             03 MDJMMER2A    PIC X.                                     00006110
             03 MDJMMER2C    PIC X.                                     00006120
             03 MDJMMER2P    PIC X.                                     00006130
             03 MDJMMER2H    PIC X.                                     00006140
             03 MDJMMER2V    PIC X.                                     00006150
             03 MDJMMER2O    PIC X(2).                                  00006160
             03 FILLER       PIC X(2).                                  00006170
             03 MDJMJEU2A    PIC X.                                     00006180
             03 MDJMJEU2C    PIC X.                                     00006190
             03 MDJMJEU2P    PIC X.                                     00006200
             03 MDJMJEU2H    PIC X.                                     00006210
             03 MDJMJEU2V    PIC X.                                     00006220
             03 MDJMJEU2O    PIC X(2).                                  00006230
             03 FILLER       PIC X(2).                                  00006240
             03 MDJMVEN2A    PIC X.                                     00006250
             03 MDJMVEN2C    PIC X.                                     00006260
             03 MDJMVEN2P    PIC X.                                     00006270
             03 MDJMVEN2H    PIC X.                                     00006280
             03 MDJMVEN2V    PIC X.                                     00006290
             03 MDJMVEN2O    PIC X(2).                                  00006300
             03 FILLER       PIC X(2).                                  00006310
             03 MDJMSAM2A    PIC X.                                     00006320
             03 MDJMSAM2C    PIC X.                                     00006330
             03 MDJMSAM2P    PIC X.                                     00006340
             03 MDJMSAM2H    PIC X.                                     00006350
             03 MDJMSAM2V    PIC X.                                     00006360
             03 MDJMSAM2O    PIC X(2).                                  00006370
             03 FILLER       PIC X(2).                                  00006380
             03 MDJMDIM2A    PIC X.                                     00006390
             03 MDJMDIM2C    PIC X.                                     00006400
             03 MDJMDIM2P    PIC X.                                     00006410
             03 MDJMDIM2H    PIC X.                                     00006420
             03 MDJMDIM2V    PIC X.                                     00006430
             03 MDJMDIM2O    PIC X(2).                                  00006440
             03 FILLER       PIC X(2).                                  00006450
             03 MDJMLUN3A    PIC X.                                     00006460
             03 MDJMLUN3C    PIC X.                                     00006470
             03 MDJMLUN3P    PIC X.                                     00006480
             03 MDJMLUN3H    PIC X.                                     00006490
             03 MDJMLUN3V    PIC X.                                     00006500
             03 MDJMLUN3O    PIC X(2).                                  00006510
             03 FILLER       PIC X(2).                                  00006520
             03 MDJMMAR3A    PIC X.                                     00006530
             03 MDJMMAR3C    PIC X.                                     00006540
             03 MDJMMAR3P    PIC X.                                     00006550
             03 MDJMMAR3H    PIC X.                                     00006560
             03 MDJMMAR3V    PIC X.                                     00006570
             03 MDJMMAR3O    PIC X(2).                                  00006580
             03 FILLER       PIC X(2).                                  00006590
             03 MDJMMER3A    PIC X.                                     00006600
             03 MDJMMER3C    PIC X.                                     00006610
             03 MDJMMER3P    PIC X.                                     00006620
             03 MDJMMER3H    PIC X.                                     00006630
             03 MDJMMER3V    PIC X.                                     00006640
             03 MDJMMER3O    PIC X(2).                                  00006650
             03 FILLER       PIC X(2).                                  00006660
             03 MDJMJEU3A    PIC X.                                     00006670
             03 MDJMJEU3C    PIC X.                                     00006680
             03 MDJMJEU3P    PIC X.                                     00006690
             03 MDJMJEU3H    PIC X.                                     00006700
             03 MDJMJEU3V    PIC X.                                     00006710
             03 MDJMJEU3O    PIC X(2).                                  00006720
             03 FILLER       PIC X(2).                                  00006730
             03 MDJMVEN3A    PIC X.                                     00006740
             03 MDJMVEN3C    PIC X.                                     00006750
             03 MDJMVEN3P    PIC X.                                     00006760
             03 MDJMVEN3H    PIC X.                                     00006770
             03 MDJMVEN3V    PIC X.                                     00006780
             03 MDJMVEN3O    PIC X(2).                                  00006790
             03 FILLER       PIC X(2).                                  00006800
             03 MDJMSAM3A    PIC X.                                     00006810
             03 MDJMSAM3C    PIC X.                                     00006820
             03 MDJMSAM3P    PIC X.                                     00006830
             03 MDJMSAM3H    PIC X.                                     00006840
             03 MDJMSAM3V    PIC X.                                     00006850
             03 MDJMSAM3O    PIC X(2).                                  00006860
             03 FILLER       PIC X(2).                                  00006870
             03 MDJMDIM3A    PIC X.                                     00006880
             03 MDJMDIM3C    PIC X.                                     00006890
             03 MDJMDIM3P    PIC X.                                     00006900
             03 MDJMDIM3H    PIC X.                                     00006910
             03 MDJMDIM3V    PIC X.                                     00006920
             03 MDJMDIM3O    PIC X(2).                                  00006930
           02 MCODESO OCCURS   8 TIMES .                                00006940
             03 FILLER       PIC X(2).                                  00006950
             03 MCODLUN1A    PIC X.                                     00006960
             03 MCODLUN1C    PIC X.                                     00006970
             03 MCODLUN1P    PIC X.                                     00006980
             03 MCODLUN1H    PIC X.                                     00006990
             03 MCODLUN1V    PIC X.                                     00007000
             03 MCODLUN1O    PIC X(2).                                  00007010
             03 FILLER       PIC X(2).                                  00007020
             03 MCODMAR1A    PIC X.                                     00007030
             03 MCODMAR1C    PIC X.                                     00007040
             03 MCODMAR1P    PIC X.                                     00007050
             03 MCODMAR1H    PIC X.                                     00007060
             03 MCODMAR1V    PIC X.                                     00007070
             03 MCODMAR1O    PIC X(2).                                  00007080
             03 FILLER       PIC X(2).                                  00007090
             03 MCODMER1A    PIC X.                                     00007100
             03 MCODMER1C    PIC X.                                     00007110
             03 MCODMER1P    PIC X.                                     00007120
             03 MCODMER1H    PIC X.                                     00007130
             03 MCODMER1V    PIC X.                                     00007140
             03 MCODMER1O    PIC X(2).                                  00007150
             03 FILLER       PIC X(2).                                  00007160
             03 MCODJEU1A    PIC X.                                     00007170
             03 MCODJEU1C    PIC X.                                     00007180
             03 MCODJEU1P    PIC X.                                     00007190
             03 MCODJEU1H    PIC X.                                     00007200
             03 MCODJEU1V    PIC X.                                     00007210
             03 MCODJEU1O    PIC X(2).                                  00007220
             03 FILLER       PIC X(2).                                  00007230
             03 MCODVEN1A    PIC X.                                     00007240
             03 MCODVEN1C    PIC X.                                     00007250
             03 MCODVEN1P    PIC X.                                     00007260
             03 MCODVEN1H    PIC X.                                     00007270
             03 MCODVEN1V    PIC X.                                     00007280
             03 MCODVEN1O    PIC X(2).                                  00007290
             03 FILLER       PIC X(2).                                  00007300
             03 MCODSAM1A    PIC X.                                     00007310
             03 MCODSAM1C    PIC X.                                     00007320
             03 MCODSAM1P    PIC X.                                     00007330
             03 MCODSAM1H    PIC X.                                     00007340
             03 MCODSAM1V    PIC X.                                     00007350
             03 MCODSAM1O    PIC X(2).                                  00007360
             03 FILLER       PIC X(2).                                  00007370
             03 MCODDIM1A    PIC X.                                     00007380
             03 MCODDIM1C    PIC X.                                     00007390
             03 MCODDIM1P    PIC X.                                     00007400
             03 MCODDIM1H    PIC X.                                     00007410
             03 MCODDIM1V    PIC X.                                     00007420
             03 MCODDIM1O    PIC X(2).                                  00007430
             03 FILLER       PIC X(2).                                  00007440
             03 MCODLUN2A    PIC X.                                     00007450
             03 MCODLUN2C    PIC X.                                     00007460
             03 MCODLUN2P    PIC X.                                     00007470
             03 MCODLUN2H    PIC X.                                     00007480
             03 MCODLUN2V    PIC X.                                     00007490
             03 MCODLUN2O    PIC X(2).                                  00007500
             03 FILLER       PIC X(2).                                  00007510
             03 MCODMAR2A    PIC X.                                     00007520
             03 MCODMAR2C    PIC X.                                     00007530
             03 MCODMAR2P    PIC X.                                     00007540
             03 MCODMAR2H    PIC X.                                     00007550
             03 MCODMAR2V    PIC X.                                     00007560
             03 MCODMAR2O    PIC X(2).                                  00007570
             03 FILLER       PIC X(2).                                  00007580
             03 MCODMER2A    PIC X.                                     00007590
             03 MCODMER2C    PIC X.                                     00007600
             03 MCODMER2P    PIC X.                                     00007610
             03 MCODMER2H    PIC X.                                     00007620
             03 MCODMER2V    PIC X.                                     00007630
             03 MCODMER2O    PIC X(2).                                  00007640
             03 FILLER       PIC X(2).                                  00007650
             03 MCODJEU2A    PIC X.                                     00007660
             03 MCODJEU2C    PIC X.                                     00007670
             03 MCODJEU2P    PIC X.                                     00007680
             03 MCODJEU2H    PIC X.                                     00007690
             03 MCODJEU2V    PIC X.                                     00007700
             03 MCODJEU2O    PIC X(2).                                  00007710
             03 FILLER       PIC X(2).                                  00007720
             03 MCODVEN2A    PIC X.                                     00007730
             03 MCODVEN2C    PIC X.                                     00007740
             03 MCODVEN2P    PIC X.                                     00007750
             03 MCODVEN2H    PIC X.                                     00007760
             03 MCODVEN2V    PIC X.                                     00007770
             03 MCODVEN2O    PIC X(2).                                  00007780
             03 FILLER       PIC X(2).                                  00007790
             03 MCODSAM2A    PIC X.                                     00007800
             03 MCODSAM2C    PIC X.                                     00007810
             03 MCODSAM2P    PIC X.                                     00007820
             03 MCODSAM2H    PIC X.                                     00007830
             03 MCODSAM2V    PIC X.                                     00007840
             03 MCODSAM2O    PIC X(2).                                  00007850
             03 FILLER       PIC X(2).                                  00007860
             03 MCODDIM2A    PIC X.                                     00007870
             03 MCODDIM2C    PIC X.                                     00007880
             03 MCODDIM2P    PIC X.                                     00007890
             03 MCODDIM2H    PIC X.                                     00007900
             03 MCODDIM2V    PIC X.                                     00007910
             03 MCODDIM2O    PIC X(2).                                  00007920
             03 FILLER       PIC X(2).                                  00007930
             03 MCODLUN3A    PIC X.                                     00007940
             03 MCODLUN3C    PIC X.                                     00007950
             03 MCODLUN3P    PIC X.                                     00007960
             03 MCODLUN3H    PIC X.                                     00007970
             03 MCODLUN3V    PIC X.                                     00007980
             03 MCODLUN3O    PIC X(2).                                  00007990
             03 FILLER       PIC X(2).                                  00008000
             03 MCODMAR3A    PIC X.                                     00008010
             03 MCODMAR3C    PIC X.                                     00008020
             03 MCODMAR3P    PIC X.                                     00008030
             03 MCODMAR3H    PIC X.                                     00008040
             03 MCODMAR3V    PIC X.                                     00008050
             03 MCODMAR3O    PIC X(2).                                  00008060
             03 FILLER       PIC X(2).                                  00008070
             03 MCODMER3A    PIC X.                                     00008080
             03 MCODMER3C    PIC X.                                     00008090
             03 MCODMER3P    PIC X.                                     00008100
             03 MCODMER3H    PIC X.                                     00008110
             03 MCODMER3V    PIC X.                                     00008120
             03 MCODMER3O    PIC X(2).                                  00008130
             03 FILLER       PIC X(2).                                  00008140
             03 MCODJEU3A    PIC X.                                     00008150
             03 MCODJEU3C    PIC X.                                     00008160
             03 MCODJEU3P    PIC X.                                     00008170
             03 MCODJEU3H    PIC X.                                     00008180
             03 MCODJEU3V    PIC X.                                     00008190
             03 MCODJEU3O    PIC X(2).                                  00008200
             03 FILLER       PIC X(2).                                  00008210
             03 MCODVEN3A    PIC X.                                     00008220
             03 MCODVEN3C    PIC X.                                     00008230
             03 MCODVEN3P    PIC X.                                     00008240
             03 MCODVEN3H    PIC X.                                     00008250
             03 MCODVEN3V    PIC X.                                     00008260
             03 MCODVEN3O    PIC X(2).                                  00008270
             03 FILLER       PIC X(2).                                  00008280
             03 MCODSAM3A    PIC X.                                     00008290
             03 MCODSAM3C    PIC X.                                     00008300
             03 MCODSAM3P    PIC X.                                     00008310
             03 MCODSAM3H    PIC X.                                     00008320
             03 MCODSAM3V    PIC X.                                     00008330
             03 MCODSAM3O    PIC X(2).                                  00008340
             03 FILLER       PIC X(2).                                  00008350
             03 MCODDIM3A    PIC X.                                     00008360
             03 MCODDIM3C    PIC X.                                     00008370
             03 MCODDIM3P    PIC X.                                     00008380
             03 MCODDIM3H    PIC X.                                     00008390
             03 MCODDIM3V    PIC X.                                     00008400
             03 MCODDIM3O    PIC X(2).                                  00008410
           02 FILLER    PIC X(2).                                       00008420
           02 MZONCMDA  PIC X.                                          00008430
           02 MZONCMDC  PIC X.                                          00008440
           02 MZONCMDP  PIC X.                                          00008450
           02 MZONCMDH  PIC X.                                          00008460
           02 MZONCMDV  PIC X.                                          00008470
           02 MZONCMDO  PIC X(15).                                      00008480
           02 FILLER    PIC X(2).                                       00008490
           02 MLIBERRA  PIC X.                                          00008500
           02 MLIBERRC  PIC X.                                          00008510
           02 MLIBERRP  PIC X.                                          00008520
           02 MLIBERRH  PIC X.                                          00008530
           02 MLIBERRV  PIC X.                                          00008540
           02 MLIBERRO  PIC X(58).                                      00008550
           02 FILLER    PIC X(2).                                       00008560
           02 MCODTRAA  PIC X.                                          00008570
           02 MCODTRAC  PIC X.                                          00008580
           02 MCODTRAP  PIC X.                                          00008590
           02 MCODTRAH  PIC X.                                          00008600
           02 MCODTRAV  PIC X.                                          00008610
           02 MCODTRAO  PIC X(4).                                       00008620
           02 FILLER    PIC X(2).                                       00008630
           02 MCICSA    PIC X.                                          00008640
           02 MCICSC    PIC X.                                          00008650
           02 MCICSP    PIC X.                                          00008660
           02 MCICSH    PIC X.                                          00008670
           02 MCICSV    PIC X.                                          00008680
           02 MCICSO    PIC X(5).                                       00008690
           02 FILLER    PIC X(2).                                       00008700
           02 MNETNAMA  PIC X.                                          00008710
           02 MNETNAMC  PIC X.                                          00008720
           02 MNETNAMP  PIC X.                                          00008730
           02 MNETNAMH  PIC X.                                          00008740
           02 MNETNAMV  PIC X.                                          00008750
           02 MNETNAMO  PIC X(8).                                       00008760
           02 FILLER    PIC X(2).                                       00008770
           02 MSCREENA  PIC X.                                          00008780
           02 MSCREENC  PIC X.                                          00008790
           02 MSCREENP  PIC X.                                          00008800
           02 MSCREENH  PIC X.                                          00008810
           02 MSCREENV  PIC X.                                          00008820
           02 MSCREENO  PIC X(4).                                       00008830
                                                                                
