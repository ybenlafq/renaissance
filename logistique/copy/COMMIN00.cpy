      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *         MAQUETTE COMMAREA STANDARD AIDA COBOL2             *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-------------------------------------------------------------            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-IN00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-IN00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 180 PIC X(1).                               
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3694          
           02 COMM-IN00-APPLI.                                                  
      *                                                                         
      *                                              TYPE DE MAGASIN            
              03 COMM-IN00-NSOCIETE       PIC X(3).                             
      *                                              TYPE DE MAGASIN            
              03 COMM-IN00-TYPE           PIC X(4).                             
      *                                              NOMBRE D'EXEMPLAIRE        
              03 COMM-IN00-NBEX           PIC 9(3).                             
      *                                              NUMERO D'IMPRIMANTE        
              03 COMM-IN00-NIMP           PIC X(4).                             
      *                                                                         
      *                                       DATE INVENTAIRE SSAAMMJJ          
              03 COMM-DATE-INVENTAIRE.                                          
                 05 COMM-DATE-INVENTAIRESA PIC X(4).                            
                 05 COMM-DATE-INVENTAIREMM PIC XX.                              
                 05 COMM-DATE-INVENTAIREJJ PIC XX.                              
      *                                       DATE INVENTAIRE JJMMSSAA          
              03 COMM-DATE-INVENTAIRB.                                          
                 05 COMM-DATE-INVENTAIRBJJ PIC XX.                              
                 05 COMM-DATE-INVENTAIRFJJ PIC X.                               
                 05 COMM-DATE-INVENTAIRBMM PIC XX.                              
                 05 COMM-DATE-INVENTAIRFMM PIC X.                               
                 05 COMM-DATE-INVENTAIRBSA PIC X(4).                            
      *                                       MESSAGE RETOUR EDITION            
              03 COMM-IN00-MESS            PIC X(80).                           
      *                                                                         
      *------------------------------ ZONE PR TRANSFERT PARAMETRES      00010000
              03 COMM-IN00-PARAM-TEX.                                   00020002
      *------------------------------ ZONE CLE                          00030000
                 04 COMM-IN00-CLE-TEX.                                  00040002
      *------------------------------ DATE                              00050000
                    05 COMM-IN00-DAT-TEX.                               00060002
      *------------------------------ DATE ANNEE                        00070000
                       06 COMM-IN00-AA-TEX  PIC X(2).                   00080002
      *------------------------------ DATE MOIS                         00090000
                       06 COMM-IN00-MM-TEX  PIC X(2).                   00100002
      *------------------------------ DATE JOUR                         00110000
                       06 COMM-IN00-JJ-TEX  PIC X(2).                   00120002
      *------------------------------ NOM DU PROGRAMME                  00130000
                    05 COMM-IN00-NOMPRG-TEX    PIC X(5).                00140002
      *------------------------------ NUMERO DE TACHE                   00150000
                    05 COMM-IN00-NUMTAC-TEX    PIC 9(2).                00160002
      *------------------------------ NUMERO DE CARTE                   00170000
                    05 COMM-IN00-NUMCAR-TEX    PIC 9(2).                00180002
      *------------------------------ CODE DE MISE A JOUR (G/I/D/R)     00190000
                 04 COMM-IN00-CODMAJ-TEX  PIC X.                        00200002
      *------------------------------ CODE RETOUR (00/99/98)            00210000
                 04 COMM-IN00-CODRET-TEX  PIC XX.                       00220002
      *------------------------------ INUTILISE                         00230000
                 04 COMM-IN00-FILLER      PIC X(15).                    00240002
      *------------------------------ ZONE PARAMETRE.                   00250000
                 04 COMM-IN00-PARAMETRE PIC X(80).                      00260002
      *------------------------------ INUTILISE                         00270003
                 04 COMM-IN00-FILLER    PIC X(07).                      00280003
      *                                              OCTETS LIBRES              
              03 COMM-IN00-FILLER         PIC X(3490).                          
                                                                                
