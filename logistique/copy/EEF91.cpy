      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GRF Reprise d'un dossier avoir                                  00000020
      ***************************************************************** 00000030
       01   EEF91I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCTRAITI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRAITF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLTRAITI  PIC X(28).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCIETEI     PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(19).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCTIERSI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLTIERSI  PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLADR1L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLADR1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLADR1F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLADR1I   PIC X(32).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNENTCDEI      PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLENTCDEI      PIC X(19).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLADR2L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLADR2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLADR2F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLADR2I   PIC X(32).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNENVOII  PIC X(7).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDENVOII  PIC X(10).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCPOSTALI      PIC X(5).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMMUNEL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MCOMMUNEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOMMUNEF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCOMMUNEI      PIC X(26).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MACCORDL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MACCORDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MACCORDF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MACCORDI  PIC X(9).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCORDL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MNACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNACCORDF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNACCORDI      PIC X(12).                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDU1L     COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MDU1L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MDU1F     PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MDU1I     PIC X(2).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACCORDL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MDACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDACCORDF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MDACCORDI      PIC X(10).                                 00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINTERLOCL     COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MINTERLOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MINTERLOCF     PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MINTERLOCI     PIC X(25).                                 00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMACCORL     COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MNOMACCORL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNOMACCORF     PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNOMACCORI     PIC X(10).                                 00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGARANTIEL     COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MGARANTIEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MGARANTIEF     PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MGARANTIEI     PIC X(8).                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGARANTIL     COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MCGARANTIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCGARANTIF     PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCGARANTII     PIC X(5).                                  00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGARANTIL     COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MLGARANTIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLGARANTIF     PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLGARANTII     PIC X(20).                                 00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBRDOSSL      COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MNBRDOSSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBRDOSSF      PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNBRDOSSI      PIC X(7).                                  00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOSSIERL      COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MDOSSIERL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDOSSIERF      PIC X.                                     00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MDOSSIERI      PIC X(11).                                 00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIGINEL      COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MORIGINEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MORIGINEF      PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MORIGINEI      PIC X(7).                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCORIGL     COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MNSOCORIGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCORIGF     PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MNSOCORIGI     PIC X(3).                                  00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUORIGL     COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MLIEUORIGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIEUORIGF     PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MLIEUORIGI     PIC X(3).                                  00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHSL      COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MHSL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MHSF      PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MHSI      PIC X(5).                                       00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORIGINEL     COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MNORIGINEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNORIGINEF     PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MNORIGINEI     PIC X(7).                                  00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MCODICI   PIC X(5).                                       00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MNCODICI  PIC X(7).                                       00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MLCODICI  PIC X(20).                                      00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERIEL   COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MSERIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSERIEF   PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MSERIEI   PIC X(7).                                       00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSERIEL  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MNSERIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSERIEF  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MNSERIEI  PIC X(16).                                      00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAMILLEL      COMP PIC S9(4).                            00001580
      *--                                                                       
           02 MFAMILLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFAMILLEF      PIC X.                                     00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MFAMILLEI      PIC X(7).                                  00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MCFAMI    PIC X(5).                                       00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MLFAMI    PIC X(20).                                      00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUANTITEL     COMP PIC S9(4).                            00001700
      *--                                                                       
           02 MQUANTITEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQUANTITEF     PIC X.                                     00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MQUANTITEI     PIC X(8).                                  00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTENVL   COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MQTENVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQTENVF   PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MQTENVI   PIC X(6).                                       00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMARQUEL  COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MMARQUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMARQUEF  PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MMARQUEI  PIC X(6).                                       00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQUEL      COMP PIC S9(4).                            00001820
      *--                                                                       
           02 MCMARQUEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMARQUEF      PIC X.                                     00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MCMARQUEI      PIC X(5).                                  00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQUEL      COMP PIC S9(4).                            00001860
      *--                                                                       
           02 MLMARQUEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMARQUEF      PIC X.                                     00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MLMARQUEI      PIC X(20).                                 00001890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIXL    COMP PIC S9(4).                                 00001900
      *--                                                                       
           02 MPRIXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRIXF    PIC X.                                          00001910
           02 FILLER    PIC X(4).                                       00001920
           02 MPRIXI    PIC X(4).                                       00001930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPBASEFACL     COMP PIC S9(4).                            00001940
      *--                                                                       
           02 MPBASEFACL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPBASEFACF     PIC X.                                     00001950
           02 FILLER    PIC X(4).                                       00001960
           02 MPBASEFACI     PIC X(12).                                 00001970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEUROSL   COMP PIC S9(4).                                 00001980
      *--                                                                       
           02 MEUROSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEUROSF   PIC X.                                          00001990
           02 FILLER    PIC X(4).                                       00002000
           02 MEUROSI   PIC X(5).                                       00002010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTALL   COMP PIC S9(4).                                 00002020
      *--                                                                       
           02 MTOTALL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTOTALF   PIC X.                                          00002030
           02 FILLER    PIC X(4).                                       00002040
           02 MTOTALI   PIC X(12).                                      00002050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRENDUL  COMP PIC S9(4).                                 00002060
      *--                                                                       
           02 MCRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRENDUF  PIC X.                                          00002070
           02 FILLER    PIC X(4).                                       00002080
           02 MCRENDUI  PIC X(5).                                       00002090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRENDUL  COMP PIC S9(4).                                 00002100
      *--                                                                       
           02 MLRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRENDUF  PIC X.                                          00002110
           02 FILLER    PIC X(4).                                       00002120
           02 MLRENDUI  PIC X(20).                                      00002130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMRESPL      COMP PIC S9(4).                            00002140
      *--                                                                       
           02 MNOMRESPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNOMRESPF      PIC X.                                     00002150
           02 FILLER    PIC X(4).                                       00002160
           02 MNOMRESPI      PIC X(20).                                 00002170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM1L   COMP PIC S9(4).                                 00002180
      *--                                                                       
           02 MCOMM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM1F   PIC X.                                          00002190
           02 FILLER    PIC X(4).                                       00002200
           02 MCOMM1I   PIC X(60).                                      00002210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM2L   COMP PIC S9(4).                                 00002220
      *--                                                                       
           02 MCOMM2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM2F   PIC X.                                          00002230
           02 FILLER    PIC X(4).                                       00002240
           02 MCOMM2I   PIC X(60).                                      00002250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM3L   COMP PIC S9(4).                                 00002260
      *--                                                                       
           02 MCOMM3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM3F   PIC X.                                          00002270
           02 FILLER    PIC X(4).                                       00002280
           02 MCOMM3I   PIC X(60).                                      00002290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002300
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002310
           02 FILLER    PIC X(4).                                       00002320
           02 MLIBERRI  PIC X(79).                                      00002330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002340
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002350
           02 FILLER    PIC X(4).                                       00002360
           02 MCODTRAI  PIC X(4).                                       00002370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00002380
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00002390
           02 FILLER    PIC X(4).                                       00002400
           02 MZONCMDI  PIC X(15).                                      00002410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002420
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002430
           02 FILLER    PIC X(4).                                       00002440
           02 MCICSI    PIC X(5).                                       00002450
      * NETNAME                                                         00002460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002470
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002480
           02 FILLER    PIC X(4).                                       00002490
           02 MNETNAMI  PIC X(8).                                       00002500
      * CODE TERMINAL                                                   00002510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002520
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002530
           02 FILLER    PIC X(4).                                       00002540
           02 MSCREENI  PIC X(4).                                       00002550
      ***************************************************************** 00002560
      * GRF Reprise d'un dossier avoir                                  00002570
      ***************************************************************** 00002580
       01   EEF91O REDEFINES EEF91I.                                    00002590
           02 FILLER    PIC X(12).                                      00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MDATJOUA  PIC X.                                          00002620
           02 MDATJOUC  PIC X.                                          00002630
           02 MDATJOUP  PIC X.                                          00002640
           02 MDATJOUH  PIC X.                                          00002650
           02 MDATJOUV  PIC X.                                          00002660
           02 MDATJOUO  PIC X(10).                                      00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MTIMJOUA  PIC X.                                          00002690
           02 MTIMJOUC  PIC X.                                          00002700
           02 MTIMJOUP  PIC X.                                          00002710
           02 MTIMJOUH  PIC X.                                          00002720
           02 MTIMJOUV  PIC X.                                          00002730
           02 MTIMJOUO  PIC X(5).                                       00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MFONCA    PIC X.                                          00002760
           02 MFONCC    PIC X.                                          00002770
           02 MFONCP    PIC X.                                          00002780
           02 MFONCH    PIC X.                                          00002790
           02 MFONCV    PIC X.                                          00002800
           02 MFONCO    PIC X(3).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCTRAITA  PIC X.                                          00002830
           02 MCTRAITC  PIC X.                                          00002840
           02 MCTRAITP  PIC X.                                          00002850
           02 MCTRAITH  PIC X.                                          00002860
           02 MCTRAITV  PIC X.                                          00002870
           02 MCTRAITO  PIC X(5).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MLTRAITA  PIC X.                                          00002900
           02 MLTRAITC  PIC X.                                          00002910
           02 MLTRAITP  PIC X.                                          00002920
           02 MLTRAITH  PIC X.                                          00002930
           02 MLTRAITV  PIC X.                                          00002940
           02 MLTRAITO  PIC X(28).                                      00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNSOCIETEA     PIC X.                                     00002970
           02 MNSOCIETEC     PIC X.                                     00002980
           02 MNSOCIETEP     PIC X.                                     00002990
           02 MNSOCIETEH     PIC X.                                     00003000
           02 MNSOCIETEV     PIC X.                                     00003010
           02 MNSOCIETEO     PIC X(3).                                  00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MLLIEUA   PIC X.                                          00003040
           02 MLLIEUC   PIC X.                                          00003050
           02 MLLIEUP   PIC X.                                          00003060
           02 MLLIEUH   PIC X.                                          00003070
           02 MLLIEUV   PIC X.                                          00003080
           02 MLLIEUO   PIC X(19).                                      00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MCTIERSA  PIC X.                                          00003110
           02 MCTIERSC  PIC X.                                          00003120
           02 MCTIERSP  PIC X.                                          00003130
           02 MCTIERSH  PIC X.                                          00003140
           02 MCTIERSV  PIC X.                                          00003150
           02 MCTIERSO  PIC X(5).                                       00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MLTIERSA  PIC X.                                          00003180
           02 MLTIERSC  PIC X.                                          00003190
           02 MLTIERSP  PIC X.                                          00003200
           02 MLTIERSH  PIC X.                                          00003210
           02 MLTIERSV  PIC X.                                          00003220
           02 MLTIERSO  PIC X(20).                                      00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MLADR1A   PIC X.                                          00003250
           02 MLADR1C   PIC X.                                          00003260
           02 MLADR1P   PIC X.                                          00003270
           02 MLADR1H   PIC X.                                          00003280
           02 MLADR1V   PIC X.                                          00003290
           02 MLADR1O   PIC X(32).                                      00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MNENTCDEA      PIC X.                                     00003320
           02 MNENTCDEC PIC X.                                          00003330
           02 MNENTCDEP PIC X.                                          00003340
           02 MNENTCDEH PIC X.                                          00003350
           02 MNENTCDEV PIC X.                                          00003360
           02 MNENTCDEO      PIC X(5).                                  00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MLENTCDEA      PIC X.                                     00003390
           02 MLENTCDEC PIC X.                                          00003400
           02 MLENTCDEP PIC X.                                          00003410
           02 MLENTCDEH PIC X.                                          00003420
           02 MLENTCDEV PIC X.                                          00003430
           02 MLENTCDEO      PIC X(19).                                 00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MLADR2A   PIC X.                                          00003460
           02 MLADR2C   PIC X.                                          00003470
           02 MLADR2P   PIC X.                                          00003480
           02 MLADR2H   PIC X.                                          00003490
           02 MLADR2V   PIC X.                                          00003500
           02 MLADR2O   PIC X(32).                                      00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MNENVOIA  PIC X.                                          00003530
           02 MNENVOIC  PIC X.                                          00003540
           02 MNENVOIP  PIC X.                                          00003550
           02 MNENVOIH  PIC X.                                          00003560
           02 MNENVOIV  PIC X.                                          00003570
           02 MNENVOIO  PIC X(7).                                       00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MDENVOIA  PIC X.                                          00003600
           02 MDENVOIC  PIC X.                                          00003610
           02 MDENVOIP  PIC X.                                          00003620
           02 MDENVOIH  PIC X.                                          00003630
           02 MDENVOIV  PIC X.                                          00003640
           02 MDENVOIO  PIC X(10).                                      00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MCPOSTALA      PIC X.                                     00003670
           02 MCPOSTALC PIC X.                                          00003680
           02 MCPOSTALP PIC X.                                          00003690
           02 MCPOSTALH PIC X.                                          00003700
           02 MCPOSTALV PIC X.                                          00003710
           02 MCPOSTALO      PIC X(5).                                  00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MCOMMUNEA      PIC X.                                     00003740
           02 MCOMMUNEC PIC X.                                          00003750
           02 MCOMMUNEP PIC X.                                          00003760
           02 MCOMMUNEH PIC X.                                          00003770
           02 MCOMMUNEV PIC X.                                          00003780
           02 MCOMMUNEO      PIC X(26).                                 00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MACCORDA  PIC X.                                          00003810
           02 MACCORDC  PIC X.                                          00003820
           02 MACCORDP  PIC X.                                          00003830
           02 MACCORDH  PIC X.                                          00003840
           02 MACCORDV  PIC X.                                          00003850
           02 MACCORDO  PIC X(9).                                       00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MNACCORDA      PIC X.                                     00003880
           02 MNACCORDC PIC X.                                          00003890
           02 MNACCORDP PIC X.                                          00003900
           02 MNACCORDH PIC X.                                          00003910
           02 MNACCORDV PIC X.                                          00003920
           02 MNACCORDO      PIC X(12).                                 00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MDU1A     PIC X.                                          00003950
           02 MDU1C     PIC X.                                          00003960
           02 MDU1P     PIC X.                                          00003970
           02 MDU1H     PIC X.                                          00003980
           02 MDU1V     PIC X.                                          00003990
           02 MDU1O     PIC X(2).                                       00004000
           02 FILLER    PIC X(2).                                       00004010
           02 MDACCORDA      PIC X.                                     00004020
           02 MDACCORDC PIC X.                                          00004030
           02 MDACCORDP PIC X.                                          00004040
           02 MDACCORDH PIC X.                                          00004050
           02 MDACCORDV PIC X.                                          00004060
           02 MDACCORDO      PIC X(10).                                 00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MINTERLOCA     PIC X.                                     00004090
           02 MINTERLOCC     PIC X.                                     00004100
           02 MINTERLOCP     PIC X.                                     00004110
           02 MINTERLOCH     PIC X.                                     00004120
           02 MINTERLOCV     PIC X.                                     00004130
           02 MINTERLOCO     PIC X(25).                                 00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MNOMACCORA     PIC X.                                     00004160
           02 MNOMACCORC     PIC X.                                     00004170
           02 MNOMACCORP     PIC X.                                     00004180
           02 MNOMACCORH     PIC X.                                     00004190
           02 MNOMACCORV     PIC X.                                     00004200
           02 MNOMACCORO     PIC X(10).                                 00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MGARANTIEA     PIC X.                                     00004230
           02 MGARANTIEC     PIC X.                                     00004240
           02 MGARANTIEP     PIC X.                                     00004250
           02 MGARANTIEH     PIC X.                                     00004260
           02 MGARANTIEV     PIC X.                                     00004270
           02 MGARANTIEO     PIC X(8).                                  00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MCGARANTIA     PIC X.                                     00004300
           02 MCGARANTIC     PIC X.                                     00004310
           02 MCGARANTIP     PIC X.                                     00004320
           02 MCGARANTIH     PIC X.                                     00004330
           02 MCGARANTIV     PIC X.                                     00004340
           02 MCGARANTIO     PIC X(5).                                  00004350
           02 FILLER    PIC X(2).                                       00004360
           02 MLGARANTIA     PIC X.                                     00004370
           02 MLGARANTIC     PIC X.                                     00004380
           02 MLGARANTIP     PIC X.                                     00004390
           02 MLGARANTIH     PIC X.                                     00004400
           02 MLGARANTIV     PIC X.                                     00004410
           02 MLGARANTIO     PIC X(20).                                 00004420
           02 FILLER    PIC X(2).                                       00004430
           02 MNBRDOSSA      PIC X.                                     00004440
           02 MNBRDOSSC PIC X.                                          00004450
           02 MNBRDOSSP PIC X.                                          00004460
           02 MNBRDOSSH PIC X.                                          00004470
           02 MNBRDOSSV PIC X.                                          00004480
           02 MNBRDOSSO      PIC X(7).                                  00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MDOSSIERA      PIC X.                                     00004510
           02 MDOSSIERC PIC X.                                          00004520
           02 MDOSSIERP PIC X.                                          00004530
           02 MDOSSIERH PIC X.                                          00004540
           02 MDOSSIERV PIC X.                                          00004550
           02 MDOSSIERO      PIC X(11).                                 00004560
           02 FILLER    PIC X(2).                                       00004570
           02 MORIGINEA      PIC X.                                     00004580
           02 MORIGINEC PIC X.                                          00004590
           02 MORIGINEP PIC X.                                          00004600
           02 MORIGINEH PIC X.                                          00004610
           02 MORIGINEV PIC X.                                          00004620
           02 MORIGINEO      PIC X(7).                                  00004630
           02 FILLER    PIC X(2).                                       00004640
           02 MNSOCORIGA     PIC X.                                     00004650
           02 MNSOCORIGC     PIC X.                                     00004660
           02 MNSOCORIGP     PIC X.                                     00004670
           02 MNSOCORIGH     PIC X.                                     00004680
           02 MNSOCORIGV     PIC X.                                     00004690
           02 MNSOCORIGO     PIC X(3).                                  00004700
           02 FILLER    PIC X(2).                                       00004710
           02 MLIEUORIGA     PIC X.                                     00004720
           02 MLIEUORIGC     PIC X.                                     00004730
           02 MLIEUORIGP     PIC X.                                     00004740
           02 MLIEUORIGH     PIC X.                                     00004750
           02 MLIEUORIGV     PIC X.                                     00004760
           02 MLIEUORIGO     PIC X(3).                                  00004770
           02 FILLER    PIC X(2).                                       00004780
           02 MHSA      PIC X.                                          00004790
           02 MHSC      PIC X.                                          00004800
           02 MHSP      PIC X.                                          00004810
           02 MHSH      PIC X.                                          00004820
           02 MHSV      PIC X.                                          00004830
           02 MHSO      PIC X(5).                                       00004840
           02 FILLER    PIC X(2).                                       00004850
           02 MNORIGINEA     PIC X.                                     00004860
           02 MNORIGINEC     PIC X.                                     00004870
           02 MNORIGINEP     PIC X.                                     00004880
           02 MNORIGINEH     PIC X.                                     00004890
           02 MNORIGINEV     PIC X.                                     00004900
           02 MNORIGINEO     PIC X(7).                                  00004910
           02 FILLER    PIC X(2).                                       00004920
           02 MCODICA   PIC X.                                          00004930
           02 MCODICC   PIC X.                                          00004940
           02 MCODICP   PIC X.                                          00004950
           02 MCODICH   PIC X.                                          00004960
           02 MCODICV   PIC X.                                          00004970
           02 MCODICO   PIC X(5).                                       00004980
           02 FILLER    PIC X(2).                                       00004990
           02 MNCODICA  PIC X.                                          00005000
           02 MNCODICC  PIC X.                                          00005010
           02 MNCODICP  PIC X.                                          00005020
           02 MNCODICH  PIC X.                                          00005030
           02 MNCODICV  PIC X.                                          00005040
           02 MNCODICO  PIC X(7).                                       00005050
           02 FILLER    PIC X(2).                                       00005060
           02 MLCODICA  PIC X.                                          00005070
           02 MLCODICC  PIC X.                                          00005080
           02 MLCODICP  PIC X.                                          00005090
           02 MLCODICH  PIC X.                                          00005100
           02 MLCODICV  PIC X.                                          00005110
           02 MLCODICO  PIC X(20).                                      00005120
           02 FILLER    PIC X(2).                                       00005130
           02 MSERIEA   PIC X.                                          00005140
           02 MSERIEC   PIC X.                                          00005150
           02 MSERIEP   PIC X.                                          00005160
           02 MSERIEH   PIC X.                                          00005170
           02 MSERIEV   PIC X.                                          00005180
           02 MSERIEO   PIC X(7).                                       00005190
           02 FILLER    PIC X(2).                                       00005200
           02 MNSERIEA  PIC X.                                          00005210
           02 MNSERIEC  PIC X.                                          00005220
           02 MNSERIEP  PIC X.                                          00005230
           02 MNSERIEH  PIC X.                                          00005240
           02 MNSERIEV  PIC X.                                          00005250
           02 MNSERIEO  PIC X(16).                                      00005260
           02 FILLER    PIC X(2).                                       00005270
           02 MFAMILLEA      PIC X.                                     00005280
           02 MFAMILLEC PIC X.                                          00005290
           02 MFAMILLEP PIC X.                                          00005300
           02 MFAMILLEH PIC X.                                          00005310
           02 MFAMILLEV PIC X.                                          00005320
           02 MFAMILLEO      PIC X(7).                                  00005330
           02 FILLER    PIC X(2).                                       00005340
           02 MCFAMA    PIC X.                                          00005350
           02 MCFAMC    PIC X.                                          00005360
           02 MCFAMP    PIC X.                                          00005370
           02 MCFAMH    PIC X.                                          00005380
           02 MCFAMV    PIC X.                                          00005390
           02 MCFAMO    PIC X(5).                                       00005400
           02 FILLER    PIC X(2).                                       00005410
           02 MLFAMA    PIC X.                                          00005420
           02 MLFAMC    PIC X.                                          00005430
           02 MLFAMP    PIC X.                                          00005440
           02 MLFAMH    PIC X.                                          00005450
           02 MLFAMV    PIC X.                                          00005460
           02 MLFAMO    PIC X(20).                                      00005470
           02 FILLER    PIC X(2).                                       00005480
           02 MQUANTITEA     PIC X.                                     00005490
           02 MQUANTITEC     PIC X.                                     00005500
           02 MQUANTITEP     PIC X.                                     00005510
           02 MQUANTITEH     PIC X.                                     00005520
           02 MQUANTITEV     PIC X.                                     00005530
           02 MQUANTITEO     PIC X(8).                                  00005540
           02 FILLER    PIC X(2).                                       00005550
           02 MQTENVA   PIC X.                                          00005560
           02 MQTENVC   PIC X.                                          00005570
           02 MQTENVP   PIC X.                                          00005580
           02 MQTENVH   PIC X.                                          00005590
           02 MQTENVV   PIC X.                                          00005600
           02 MQTENVO   PIC X(6).                                       00005610
           02 FILLER    PIC X(2).                                       00005620
           02 MMARQUEA  PIC X.                                          00005630
           02 MMARQUEC  PIC X.                                          00005640
           02 MMARQUEP  PIC X.                                          00005650
           02 MMARQUEH  PIC X.                                          00005660
           02 MMARQUEV  PIC X.                                          00005670
           02 MMARQUEO  PIC X(6).                                       00005680
           02 FILLER    PIC X(2).                                       00005690
           02 MCMARQUEA      PIC X.                                     00005700
           02 MCMARQUEC PIC X.                                          00005710
           02 MCMARQUEP PIC X.                                          00005720
           02 MCMARQUEH PIC X.                                          00005730
           02 MCMARQUEV PIC X.                                          00005740
           02 MCMARQUEO      PIC X(5).                                  00005750
           02 FILLER    PIC X(2).                                       00005760
           02 MLMARQUEA      PIC X.                                     00005770
           02 MLMARQUEC PIC X.                                          00005780
           02 MLMARQUEP PIC X.                                          00005790
           02 MLMARQUEH PIC X.                                          00005800
           02 MLMARQUEV PIC X.                                          00005810
           02 MLMARQUEO      PIC X(20).                                 00005820
           02 FILLER    PIC X(2).                                       00005830
           02 MPRIXA    PIC X.                                          00005840
           02 MPRIXC    PIC X.                                          00005850
           02 MPRIXP    PIC X.                                          00005860
           02 MPRIXH    PIC X.                                          00005870
           02 MPRIXV    PIC X.                                          00005880
           02 MPRIXO    PIC X(4).                                       00005890
           02 FILLER    PIC X(2).                                       00005900
           02 MPBASEFACA     PIC X.                                     00005910
           02 MPBASEFACC     PIC X.                                     00005920
           02 MPBASEFACP     PIC X.                                     00005930
           02 MPBASEFACH     PIC X.                                     00005940
           02 MPBASEFACV     PIC X.                                     00005950
           02 MPBASEFACO     PIC X(12).                                 00005960
           02 FILLER    PIC X(2).                                       00005970
           02 MEUROSA   PIC X.                                          00005980
           02 MEUROSC   PIC X.                                          00005990
           02 MEUROSP   PIC X.                                          00006000
           02 MEUROSH   PIC X.                                          00006010
           02 MEUROSV   PIC X.                                          00006020
           02 MEUROSO   PIC X(5).                                       00006030
           02 FILLER    PIC X(2).                                       00006040
           02 MTOTALA   PIC X.                                          00006050
           02 MTOTALC   PIC X.                                          00006060
           02 MTOTALP   PIC X.                                          00006070
           02 MTOTALH   PIC X.                                          00006080
           02 MTOTALV   PIC X.                                          00006090
           02 MTOTALO   PIC X(12).                                      00006100
           02 FILLER    PIC X(2).                                       00006110
           02 MCRENDUA  PIC X.                                          00006120
           02 MCRENDUC  PIC X.                                          00006130
           02 MCRENDUP  PIC X.                                          00006140
           02 MCRENDUH  PIC X.                                          00006150
           02 MCRENDUV  PIC X.                                          00006160
           02 MCRENDUO  PIC X(5).                                       00006170
           02 FILLER    PIC X(2).                                       00006180
           02 MLRENDUA  PIC X.                                          00006190
           02 MLRENDUC  PIC X.                                          00006200
           02 MLRENDUP  PIC X.                                          00006210
           02 MLRENDUH  PIC X.                                          00006220
           02 MLRENDUV  PIC X.                                          00006230
           02 MLRENDUO  PIC X(20).                                      00006240
           02 FILLER    PIC X(2).                                       00006250
           02 MNOMRESPA      PIC X.                                     00006260
           02 MNOMRESPC PIC X.                                          00006270
           02 MNOMRESPP PIC X.                                          00006280
           02 MNOMRESPH PIC X.                                          00006290
           02 MNOMRESPV PIC X.                                          00006300
           02 MNOMRESPO      PIC X(20).                                 00006310
           02 FILLER    PIC X(2).                                       00006320
           02 MCOMM1A   PIC X.                                          00006330
           02 MCOMM1C   PIC X.                                          00006340
           02 MCOMM1P   PIC X.                                          00006350
           02 MCOMM1H   PIC X.                                          00006360
           02 MCOMM1V   PIC X.                                          00006370
           02 MCOMM1O   PIC X(60).                                      00006380
           02 FILLER    PIC X(2).                                       00006390
           02 MCOMM2A   PIC X.                                          00006400
           02 MCOMM2C   PIC X.                                          00006410
           02 MCOMM2P   PIC X.                                          00006420
           02 MCOMM2H   PIC X.                                          00006430
           02 MCOMM2V   PIC X.                                          00006440
           02 MCOMM2O   PIC X(60).                                      00006450
           02 FILLER    PIC X(2).                                       00006460
           02 MCOMM3A   PIC X.                                          00006470
           02 MCOMM3C   PIC X.                                          00006480
           02 MCOMM3P   PIC X.                                          00006490
           02 MCOMM3H   PIC X.                                          00006500
           02 MCOMM3V   PIC X.                                          00006510
           02 MCOMM3O   PIC X(60).                                      00006520
           02 FILLER    PIC X(2).                                       00006530
           02 MLIBERRA  PIC X.                                          00006540
           02 MLIBERRC  PIC X.                                          00006550
           02 MLIBERRP  PIC X.                                          00006560
           02 MLIBERRH  PIC X.                                          00006570
           02 MLIBERRV  PIC X.                                          00006580
           02 MLIBERRO  PIC X(79).                                      00006590
           02 FILLER    PIC X(2).                                       00006600
           02 MCODTRAA  PIC X.                                          00006610
           02 MCODTRAC  PIC X.                                          00006620
           02 MCODTRAP  PIC X.                                          00006630
           02 MCODTRAH  PIC X.                                          00006640
           02 MCODTRAV  PIC X.                                          00006650
           02 MCODTRAO  PIC X(4).                                       00006660
           02 FILLER    PIC X(2).                                       00006670
           02 MZONCMDA  PIC X.                                          00006680
           02 MZONCMDC  PIC X.                                          00006690
           02 MZONCMDP  PIC X.                                          00006700
           02 MZONCMDH  PIC X.                                          00006710
           02 MZONCMDV  PIC X.                                          00006720
           02 MZONCMDO  PIC X(15).                                      00006730
           02 FILLER    PIC X(2).                                       00006740
           02 MCICSA    PIC X.                                          00006750
           02 MCICSC    PIC X.                                          00006760
           02 MCICSP    PIC X.                                          00006770
           02 MCICSH    PIC X.                                          00006780
           02 MCICSV    PIC X.                                          00006790
           02 MCICSO    PIC X(5).                                       00006800
      * NETNAME                                                         00006810
           02 FILLER    PIC X(2).                                       00006820
           02 MNETNAMA  PIC X.                                          00006830
           02 MNETNAMC  PIC X.                                          00006840
           02 MNETNAMP  PIC X.                                          00006850
           02 MNETNAMH  PIC X.                                          00006860
           02 MNETNAMV  PIC X.                                          00006870
           02 MNETNAMO  PIC X(8).                                       00006880
      * CODE TERMINAL                                                   00006890
           02 FILLER    PIC X(2).                                       00006900
           02 MSCREENA  PIC X.                                          00006910
           02 MSCREENC  PIC X.                                          00006920
           02 MSCREENP  PIC X.                                          00006930
           02 MSCREENH  PIC X.                                          00006940
           02 MSCREENV  PIC X.                                          00006950
           02 MSCREENO  PIC X(4).                                       00006960
                                                                                
