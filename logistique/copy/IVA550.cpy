      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  DSECT-IVA550.                                                        
            05 RUPTURES-IVA550.                                                 
           10 IVA550-NSOCVALO           PIC X(03).                       001 003
           10 IVA550-CSEQRAYON1         PIC X(02).                       004 002
           10 IVA550-CSEQRAYON2         PIC X(02).                       006 002
           10 IVA550-WSEQFAM            PIC S9(05)      COMP-3.          008 003
           10 IVA550-CMARQ              PIC X(05).                       011 005
           10 IVA550-NCODIC             PIC X(07).                       016 007
            05 CHAMPS-IVA550.                                                   
           10 IVA550-CFAM               PIC X(05).                       023 005
           10 IVA550-CRAYON1            PIC X(05).                       028 005
           10 IVA550-DEVISE             PIC X(03).                       033 003
           10 IVA550-LRAYON1            PIC X(20).                       036 020
           10 IVA550-LRAYON2            PIC X(20).                       056 020
           10 IVA550-LREFFOURN          PIC X(20).                       076 020
           10 IVA550-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.          096 008
           10 IVA550-PSTOCKENTREE-E     PIC S9(09)V9(6) COMP-3.          104 008
           10 IVA550-PSTOCKENTREE-M     PIC S9(09)V9(6) COMP-3.          112 008
           10 IVA550-PSTOCKENTREE-R     PIC S9(09)V9(6) COMP-3.          120 008
           10 IVA550-PSTOCKRECYCL-E     PIC S9(09)V9(6) COMP-3.          128 008
           10 IVA550-PSTOCKFINAL        PIC S9(09)V9(6) COMP-3.          136 008
           10 IVA550-PSTOCKINIT         PIC S9(09)V9(6) COMP-3.          144 008
           10 IVA550-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.          152 008
           10 IVA550-PSTOCKSORTIE-E     PIC S9(09)V9(6) COMP-3.          160 008
           10 IVA550-PSTOCKSORTIE-M     PIC S9(09)V9(6) COMP-3.          168 008
           10 IVA550-PSTOCKSORTIE-R     PIC S9(09)V9(6) COMP-3.          176 008
           10 IVA550-PSTOCKRECYCL-S     PIC S9(09)V9(6) COMP-3.          184 008
           10 IVA550-QSTOCKENTREE       PIC S9(11)      COMP-3.          192 006
           10 IVA550-QSTOCKENTREE-E     PIC S9(11)      COMP-3.          198 006
           10 IVA550-QSTOCKENTREE-M     PIC S9(11)      COMP-3.          204 006
           10 IVA550-QSTOCKENTREE-R     PIC S9(11)      COMP-3.          210 006
           10 IVA550-QSTOCKFINAL        PIC S9(11)      COMP-3.          216 006
           10 IVA550-QSTOCKINIT         PIC S9(11)      COMP-3.          222 006
           10 IVA550-QSTOCKSORTIE       PIC S9(11)      COMP-3.          228 006
           10 IVA550-QSTOCKSORTIE-E     PIC S9(11)      COMP-3.          234 006
           10 IVA550-QSTOCKSORTIE-M     PIC S9(11)      COMP-3.          240 006
           10 IVA550-QSTOCKSORTIE-R     PIC S9(11)      COMP-3.          246 006
           10 IVA550-TXEURO             PIC S9(01)V9(5) COMP-3.          252 006
             05 FILLER                  PIC X(257).                             
      *      05 FILLER                  PIC X(264).                             
                                                                                
