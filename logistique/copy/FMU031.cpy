      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00002000
      * FICHIER DE SORTIE DES MOUVEMENTS DE VIDAGE DES PI (PGM=BGS006) *00002209
      ******************************************************************        
      *   DATES    * USERID * NOMS            * FAIRE FIND                      
      ******************************************************************        
      * JJ/MM/SSAA * DSAXXX * X.XXXXXXXXXXXXX * XXXXXXXXXXXXXXXXXX              
      *-----------------------------------------------------------------        
      *                                                                         
      ******************************************************************        
       01  WS-FMU031-ENR.                                                       
           10  FMU031-NUMDOC          PIC 9(07)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-NCODIC          PIC X(07)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-COPER           PIC X(10)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-NSOCO           PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-NLIEUO          PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-NSSLIEUO        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-NSOCD           PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-NLIEUD          PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-NSSLIEUD        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-QEXP            PIC 9(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-DOPEREXP        PIC X(08)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
AA1015     10  FMU031-CACIDEXP        PIC X(10)      VALUE SPACE.               
AA1015     10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-QREC            PIC 9(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-DOPERREC        PIC X(08)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
AA1015     10  FMU031-CACIDREC        PIC X(10)      VALUE SPACE.               
AA1015     10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-QARB            PIC 9(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-DOPERARB        PIC X(08)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
AA1015     10  FMU031-CACIDARB        PIC X(10)      VALUE SPACE.               
AA1015     10  FILLER                 PIC X          VALUE ';'.                 
           10  FMU031-PRMP            PIC 9(09),9(6) VALUE ZERO.                
AA1015     10  FILLER                 PIC X(55)      VALUE ' '.                 
                                                                                
