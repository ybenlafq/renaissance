      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVVA0101                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVA0101                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVVA0101.                                                            
           02  VA01-NCODIC                                                      
               PIC X(0007).                                                     
           02  VA01-NSOCVALO                                                    
               PIC X(0003).                                                     
           02  VA01-NLIEUVALO                                                   
               PIC X(0003).                                                     
           02  VA01-CTYPLIEUVALO                                                
               PIC X(0001).                                                     
           02  VA01-LLIEUVALO                                                   
               PIC X(0020).                                                     
           02  VA01-CTYPMVTVALO                                                 
               PIC X(0001).                                                     
           02  VA01-QSTOCKENTREE                                                
               PIC S9(11) COMP-3.                                               
           02  VA01-QSTOCKSORTIE                                                
               PIC S9(11) COMP-3.                                               
           02  VA01-DOPER                                                       
               PIC X(0008).                                                     
           02  VA01-PRMP                                                        
               PIC S9(7)V9(0006) COMP-3.                                        
           02  VA01-PRA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VA01-PSTOCKENTREE                                                
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA01-PSTOCKSORTIE                                                
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA01-NORIGINE                                                    
               PIC X(0007).                                                     
           02  VA01-NSOCMVT                                                     
               PIC X(0003).                                                     
           02  VA01-NLIEUMVT                                                    
               PIC X(0003).                                                     
           02  VA01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  VA01-NSOCCOMPT                                                   
               PIC X(0003).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVVA0101                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVVA0101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-NSOCVALO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-NSOCVALO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-NLIEUVALO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-NLIEUVALO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-CTYPLIEUVALO-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-CTYPLIEUVALO-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-LLIEUVALO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-LLIEUVALO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-CTYPMVTVALO-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-CTYPMVTVALO-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-QSTOCKENTREE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-QSTOCKENTREE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-QSTOCKSORTIE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-QSTOCKSORTIE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-DOPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-DOPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-PRMP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-PRA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-PRA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-PSTOCKENTREE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-PSTOCKENTREE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-PSTOCKSORTIE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-PSTOCKSORTIE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-NORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-NORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-NSOCMVT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-NSOCMVT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-NLIEUMVT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-NLIEUMVT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA01-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA01-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
