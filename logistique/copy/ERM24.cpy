      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM24   ERM24                                              00000020
      ***************************************************************** 00000030
       01   ERM24I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAGREGL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLAGREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAGREGF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLAGREGI  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMARQI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTATL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLSTATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSTATF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLSTATI   PIC X(3).                                       00000450
           02 MLIGNEI OCCURS   13 TIMES .                               00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNSOCI  PIC X(3).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNMAGI  PIC X(3).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMAGL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MLMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLMAGF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLMAGI  PIC X(15).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXPOMAGL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCEXPOMAGL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCEXPOMAGF   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCEXPOMAGI   PIC X(10).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWASSOSTDL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MWASSOSTDL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWASSOSTDF   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MWASSOSTDI   PIC X.                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWASSORTL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MWASSORTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWASSORTF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MWASSORTI    PIC X.                                     00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPOL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MQEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQEXPOF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQEXPOI      PIC X(3).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLSL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MQLSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQLSF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQLSI   PIC X(3).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQV8SRL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQV8SRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQV8SRF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQV8SRI      PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQV8SPL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQV8SPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQV8SPF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQV8SPI      PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFLAGL  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MFLAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MFLAGF  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MFLAGI  PIC X.                                          00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOL   COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MQSOL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQSOF   PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQSOI   PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSML   COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MQSML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQSMF   PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQSMI   PIC X(3).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOPL  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MQSOPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSOPF  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQSOPI  PIC X(3).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSMPL  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MQSMPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSMPF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQSMPI  PIC X(3).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MZONCMDI  PIC X(12).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MLIBERRI  PIC X(61).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCODTRAI  PIC X(4).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCICSI    PIC X(5).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MNETNAMI  PIC X(8).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MSCREENI  PIC X(4).                                       00001300
      ***************************************************************** 00001310
      * SDF: ERM24   ERM24                                              00001320
      ***************************************************************** 00001330
       01   ERM24O REDEFINES ERM24I.                                    00001340
           02 FILLER    PIC X(12).                                      00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATJOUA  PIC X.                                          00001370
           02 MDATJOUC  PIC X.                                          00001380
           02 MDATJOUP  PIC X.                                          00001390
           02 MDATJOUH  PIC X.                                          00001400
           02 MDATJOUV  PIC X.                                          00001410
           02 MDATJOUO  PIC X(10).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MTIMJOUA  PIC X.                                          00001440
           02 MTIMJOUC  PIC X.                                          00001450
           02 MTIMJOUP  PIC X.                                          00001460
           02 MTIMJOUH  PIC X.                                          00001470
           02 MTIMJOUV  PIC X.                                          00001480
           02 MTIMJOUO  PIC X(5).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MNUMPAGEA      PIC X.                                     00001510
           02 MNUMPAGEC PIC X.                                          00001520
           02 MNUMPAGEP PIC X.                                          00001530
           02 MNUMPAGEH PIC X.                                          00001540
           02 MNUMPAGEV PIC X.                                          00001550
           02 MNUMPAGEO      PIC X(2).                                  00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MPAGEMAXA      PIC X.                                     00001580
           02 MPAGEMAXC PIC X.                                          00001590
           02 MPAGEMAXP PIC X.                                          00001600
           02 MPAGEMAXH PIC X.                                          00001610
           02 MPAGEMAXV PIC X.                                          00001620
           02 MPAGEMAXO      PIC X(2).                                  00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNCODICA  PIC X.                                          00001650
           02 MNCODICC  PIC X.                                          00001660
           02 MNCODICP  PIC X.                                          00001670
           02 MNCODICH  PIC X.                                          00001680
           02 MNCODICV  PIC X.                                          00001690
           02 MNCODICO  PIC 9(07).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MLREFA    PIC X.                                          00001720
           02 MLREFC    PIC X.                                          00001730
           02 MLREFP    PIC X.                                          00001740
           02 MLREFH    PIC X.                                          00001750
           02 MLREFV    PIC X.                                          00001760
           02 MLREFO    PIC X(20).                                      00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCFAMA    PIC X.                                          00001790
           02 MCFAMC    PIC X.                                          00001800
           02 MCFAMP    PIC X.                                          00001810
           02 MCFAMH    PIC X.                                          00001820
           02 MCFAMV    PIC X.                                          00001830
           02 MCFAMO    PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLAGREGA  PIC X.                                          00001860
           02 MLAGREGC  PIC X.                                          00001870
           02 MLAGREGP  PIC X.                                          00001880
           02 MLAGREGH  PIC X.                                          00001890
           02 MLAGREGV  PIC X.                                          00001900
           02 MLAGREGO  PIC X(20).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCMARQA   PIC X.                                          00001930
           02 MCMARQC   PIC X.                                          00001940
           02 MCMARQP   PIC X.                                          00001950
           02 MCMARQH   PIC X.                                          00001960
           02 MCMARQV   PIC X.                                          00001970
           02 MCMARQO   PIC X(5).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MLSTATA   PIC X.                                          00002000
           02 MLSTATC   PIC X.                                          00002010
           02 MLSTATP   PIC X.                                          00002020
           02 MLSTATH   PIC X.                                          00002030
           02 MLSTATV   PIC X.                                          00002040
           02 MLSTATO   PIC X(3).                                       00002050
           02 MLIGNEO OCCURS   13 TIMES .                               00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MNSOCA  PIC X.                                          00002080
             03 MNSOCC  PIC X.                                          00002090
             03 MNSOCP  PIC X.                                          00002100
             03 MNSOCH  PIC X.                                          00002110
             03 MNSOCV  PIC X.                                          00002120
             03 MNSOCO  PIC X(3).                                       00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MNMAGA  PIC X.                                          00002150
             03 MNMAGC  PIC X.                                          00002160
             03 MNMAGP  PIC X.                                          00002170
             03 MNMAGH  PIC X.                                          00002180
             03 MNMAGV  PIC X.                                          00002190
             03 MNMAGO  PIC X(3).                                       00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MLMAGA  PIC X.                                          00002220
             03 MLMAGC  PIC X.                                          00002230
             03 MLMAGP  PIC X.                                          00002240
             03 MLMAGH  PIC X.                                          00002250
             03 MLMAGV  PIC X.                                          00002260
             03 MLMAGO  PIC X(15).                                      00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MCEXPOMAGA   PIC X.                                     00002290
             03 MCEXPOMAGC   PIC X.                                     00002300
             03 MCEXPOMAGP   PIC X.                                     00002310
             03 MCEXPOMAGH   PIC X.                                     00002320
             03 MCEXPOMAGV   PIC X.                                     00002330
             03 MCEXPOMAGO   PIC X(10).                                 00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MWASSOSTDA   PIC X.                                     00002360
             03 MWASSOSTDC   PIC X.                                     00002370
             03 MWASSOSTDP   PIC X.                                     00002380
             03 MWASSOSTDH   PIC X.                                     00002390
             03 MWASSOSTDV   PIC X.                                     00002400
             03 MWASSOSTDO   PIC X.                                     00002410
             03 FILLER       PIC X(2).                                  00002420
             03 MWASSORTA    PIC X.                                     00002430
             03 MWASSORTC    PIC X.                                     00002440
             03 MWASSORTP    PIC X.                                     00002450
             03 MWASSORTH    PIC X.                                     00002460
             03 MWASSORTV    PIC X.                                     00002470
             03 MWASSORTO    PIC X.                                     00002480
             03 FILLER       PIC X(2).                                  00002490
             03 MQEXPOA      PIC X.                                     00002500
             03 MQEXPOC PIC X.                                          00002510
             03 MQEXPOP PIC X.                                          00002520
             03 MQEXPOH PIC X.                                          00002530
             03 MQEXPOV PIC X.                                          00002540
             03 MQEXPOO      PIC X(3).                                  00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MQLSA   PIC X.                                          00002570
             03 MQLSC   PIC X.                                          00002580
             03 MQLSP   PIC X.                                          00002590
             03 MQLSH   PIC X.                                          00002600
             03 MQLSV   PIC X.                                          00002610
             03 MQLSO   PIC X(3).                                       00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MQV8SRA      PIC X.                                     00002640
             03 MQV8SRC PIC X.                                          00002650
             03 MQV8SRP PIC X.                                          00002660
             03 MQV8SRH PIC X.                                          00002670
             03 MQV8SRV PIC X.                                          00002680
             03 MQV8SRO      PIC X(5).                                  00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MQV8SPA      PIC X.                                     00002710
             03 MQV8SPC PIC X.                                          00002720
             03 MQV8SPP PIC X.                                          00002730
             03 MQV8SPH PIC X.                                          00002740
             03 MQV8SPV PIC X.                                          00002750
             03 MQV8SPO      PIC X(5).                                  00002760
             03 FILLER       PIC X(2).                                  00002770
             03 MFLAGA  PIC X.                                          00002780
             03 MFLAGC  PIC X.                                          00002790
             03 MFLAGP  PIC X.                                          00002800
             03 MFLAGH  PIC X.                                          00002810
             03 MFLAGV  PIC X.                                          00002820
             03 MFLAGO  PIC X.                                          00002830
             03 FILLER       PIC X(2).                                  00002840
             03 MQSOA   PIC X.                                          00002850
             03 MQSOC   PIC X.                                          00002860
             03 MQSOP   PIC X.                                          00002870
             03 MQSOH   PIC X.                                          00002880
             03 MQSOV   PIC X.                                          00002890
             03 MQSOO   PIC X(3).                                       00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MQSMA   PIC X.                                          00002920
             03 MQSMC   PIC X.                                          00002930
             03 MQSMP   PIC X.                                          00002940
             03 MQSMH   PIC X.                                          00002950
             03 MQSMV   PIC X.                                          00002960
             03 MQSMO   PIC X(3).                                       00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MQSOPA  PIC X.                                          00002990
             03 MQSOPC  PIC X.                                          00003000
             03 MQSOPP  PIC X.                                          00003010
             03 MQSOPH  PIC X.                                          00003020
             03 MQSOPV  PIC X.                                          00003030
             03 MQSOPO  PIC X(3).                                       00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MQSMPA  PIC X.                                          00003060
             03 MQSMPC  PIC X.                                          00003070
             03 MQSMPP  PIC X.                                          00003080
             03 MQSMPH  PIC X.                                          00003090
             03 MQSMPV  PIC X.                                          00003100
             03 MQSMPO  PIC X(3).                                       00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MZONCMDA  PIC X.                                          00003130
           02 MZONCMDC  PIC X.                                          00003140
           02 MZONCMDP  PIC X.                                          00003150
           02 MZONCMDH  PIC X.                                          00003160
           02 MZONCMDV  PIC X.                                          00003170
           02 MZONCMDO  PIC X(12).                                      00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MLIBERRA  PIC X.                                          00003200
           02 MLIBERRC  PIC X.                                          00003210
           02 MLIBERRP  PIC X.                                          00003220
           02 MLIBERRH  PIC X.                                          00003230
           02 MLIBERRV  PIC X.                                          00003240
           02 MLIBERRO  PIC X(61).                                      00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MCODTRAA  PIC X.                                          00003270
           02 MCODTRAC  PIC X.                                          00003280
           02 MCODTRAP  PIC X.                                          00003290
           02 MCODTRAH  PIC X.                                          00003300
           02 MCODTRAV  PIC X.                                          00003310
           02 MCODTRAO  PIC X(4).                                       00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MCICSA    PIC X.                                          00003340
           02 MCICSC    PIC X.                                          00003350
           02 MCICSP    PIC X.                                          00003360
           02 MCICSH    PIC X.                                          00003370
           02 MCICSV    PIC X.                                          00003380
           02 MCICSO    PIC X(5).                                       00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MNETNAMA  PIC X.                                          00003410
           02 MNETNAMC  PIC X.                                          00003420
           02 MNETNAMP  PIC X.                                          00003430
           02 MNETNAMH  PIC X.                                          00003440
           02 MNETNAMV  PIC X.                                          00003450
           02 MNETNAMO  PIC X(8).                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MSCREENA  PIC X.                                          00003480
           02 MSCREENC  PIC X.                                          00003490
           02 MSCREENP  PIC X.                                          00003500
           02 MSCREENH  PIC X.                                          00003510
           02 MSCREENV  PIC X.                                          00003520
           02 MSCREENO  PIC X(4).                                       00003530
                                                                                
