      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSU2000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSU2000                         
      **********************************************************                
       01  RVSU2000.                                                            
           02  SU20-NSOC                                                        
               PIC X(0005).                                                     
           02  SU20-NCDE                                                        
               PIC X(0007).                                                     
           02  SU20-NLIGCDE                                                     
               PIC X(0004).                                                     
           02  SU20-NREC                                                        
               PIC X(0007).                                                     
           02  SU20-NCODIC                                                      
               PIC X(0007).                                                     
           02  SU20-NSEQ                                                        
               PIC S9(03) COMP-3.                                               
           02  SU20-CETAT                                                       
               PIC X(0001).                                                     
           02  SU20-NMUTATION                                                   
               PIC X(0007).                                                     
           02  SU20-DMUTATION                                                   
               PIC X(0008).                                                     
           02  SU20-QMUTEE                                                      
               PIC S9(5) COMP-3.                                                
           02  SU20-NSURCDE                                                     
               PIC X(0007).                                                     
           02  SU20-CAUSENS                                                     
               PIC X(0006).                                                     
           02  SU20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVSU2000                                  
      **********************************************************                
       01  RVSU2000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-NLIGCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-NLIGCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-DMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-DMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-QMUTEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-QMUTEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-NSURCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-NSURCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-CAUSENS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU20-CAUSENS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  SU20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
