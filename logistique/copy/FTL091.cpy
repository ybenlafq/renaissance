      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT BTL091                *        
      *                                                                *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-FTL091.                                                        
         05 CHAMPS-FTL091.                                                      
           10 FTL091-CTEQUI             PIC X(05).                      122  005
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FTL091-CLIVR              PIC X(10).                      094  006
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FTL091-DMMLIVR            PIC X(06).                      197  007
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FTL091-CRETOUR            PIC X(05).                      100  020
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FTL091-QPIECES            PIC -999999.                    127  015
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FTL091-QLIVR              PIC -999999.                    127  015
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FTL091-QJOURS             PIC -999999.                    127  015
           10 FILLER                    PIC X VALUE ';'.                007  002
         05 FILLER                      PIC X(078).                             
       01 FTL091-LIBELLES.                                                      
         05 FILLER                      PIC X(06) VALUE 'CTEQUI'.               
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(05) VALUE 'CLIVR'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'DMMLIVR'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'CRETOUR'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'QPIECES'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(13) VALUE 'QLIVR+QLIVPAR'.        
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(05) VALUE 'QJOUR'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(72) VALUE SPACES.                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FTL091-LONG           PIC S9(4)   COMP  VALUE +54.             
      *                                                                         
      *--                                                                       
       01  DSECT-FTL091-LONG           PIC S9(4) COMP-5  VALUE +54.             
                                                                                
      *}                                                                        
