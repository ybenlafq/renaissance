      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVIN0500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIN0500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVIN0500.                                                            
           02  IN05-NSOCMAG                                                     
               PIC X(0003).                                                     
           02  IN05-NMAG                                                        
               PIC X(0003).                                                     
           02  IN05-DINVENTAIRE                                                 
               PIC X(0008).                                                     
           02  IN05-DMVT                                                        
               PIC X(0008).                                                     
           02  IN05-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  IN05-CTYPMVT                                                     
               PIC X(0003).                                                     
           02  IN05-NCODIC                                                      
               PIC X(0007).                                                     
           02  IN05-CMARQ                                                       
               PIC X(0005).                                                     
           02  IN05-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  IN05-CFAM                                                        
               PIC X(0005).                                                     
           02  IN05-WSTOCKMASQ                                                  
               PIC X(0001).                                                     
           02  IN05-QSTOCK                                                      
               PIC S9(5) COMP-3.                                                
           02  IN05-QSTOCKEC1                                                   
               PIC S9(5) COMP-3.                                                
           02  IN05-CPROG                                                       
               PIC X(0005).                                                     
           02  IN05-LCOMMENT                                                    
               PIC X(0020).                                                     
           02  IN05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  IN05-CSTATUT                                                     
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVIN0500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVIN0500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-NSOCMAG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-NSOCMAG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-DINVENTAIRE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-DINVENTAIRE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-DMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-DMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-NSSLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-CTYPMVT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-CTYPMVT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-WSTOCKMASQ-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-WSTOCKMASQ-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-QSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-QSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-QSTOCKEC1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-QSTOCKEC1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-CPROG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-CPROG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN05-CSTATUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN05-CSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
