      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      *                                                                 00050000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00060000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00070000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00080000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00090000
      *                                                                 00100000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00110000
      * COMPRENANT :                                                    00120000
      * 1 - LES ZONES RESERVEES A AIDA                                  00130000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00140000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00150000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00160000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00170000
      *                                                                 00180000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00190000
      * PAR AIDA                                                        00200000
      *                                                                 00210000
      *-------------------------------------------------------------    00220000
      *                                                                 00230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-TL00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00240001
      *--                                                                       
       01  COM-TL00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3738  00730000
           02 COMM-TL00-MENU.                                           00740000
              03 COMM-TL00-FONC           PIC X(3).                     00970003
              03 COMM-TL00-NSOC           PIC X(3).                     00970004
              03 COMM-TL00-LSOC           PIC X(20).                    00970005
              03 COMM-TL00-DLIVRAISON     PIC X(8).                     00970006
              03 COMM-TL00-CPROTOUR       PIC X(5).                     00970007
              03 COMM-TL00-CTOURNEE       PIC X(3).                     00970008
              03 COMM-TL00-NLIEU          PIC X(3).                     00970009
              03 COMM-TL00-NVENTE         PIC X(7).                     00970010
              03 COMM-TL00-IMP            PIC X(4).                     00970011
              03 COMM-TL00-MESS           PIC X(80).                    00970012
              03 COMM-TL00-WRETOUR        PIC X(1).                     00970013
              03 COMM-TL00-ENCAISSEMENT   PIC X(1).                     00970013
              03 COMM-TL00-DEJA           PIC X(1).                     00970013
11/97         03 COMM-TL00-GCT            PIC X(1).                     00970013
07/98         03 COMM-TL00-CDEVREF        PIC X(3).                     00970013
07/98         03 COMM-TL00-CDEVISE        PIC X(3).                     00970013
07/98         03 COMM-TL00-CECART         PIC X(5).                     00970013
07/98         03 COMM-TL00-MAZ            PIC X(1).                     00970013
07/98         03 COMM-TL00-LIBRE          PIC X(72).                    00970013
07/98      02 COMM-TL00-APPLI.                                          00970013
05/99         03 COMM-TL00-FILLER         PIC X(3500).                  00970020
      **** ZONES APPLICATIVES TL16 GENERALES ----------------- 126      00610000
05/99         03 COMM-TL16-APPLI REDEFINES COMM-TL00-FILLER.            00620000
 !    **** PROVENANCE SI APPEL PAR TL00                                         
 !            05 COMM-TL16-PROVENANCE         PIC X(05).                00630000
 !    *-      LIEU DE SAISIE                                            00630000
 !            05 COMM-TL16-MAGASIN.                                     00640000
 !               10 COMM-TL16-NSOCIETE        PIC X(03).                00650000
 !               10 COMM-TL16-NLIEU           PIC X(03).                00660000
 !               10 COMM-TL16-LLIEU           PIC X(20).                00670000
 !               10 COMM-TL16-CTYPLIEU        PIC X(03).                00680000
 !    *-      DONNEES COMMUNES A TOUS LES PROGRAMMES                    00690000
 !            05 COMM-TL16-DONNEES.                                     00700000
 !               10 COMM-TL16-MODDELIVR        PIC X(003).              00710000
 !               10 COMM-TL16-IMPRIM           PIC X(004).              00710100
 !               10 COMM-TL16-CPROTOUR         PIC X(005).              00710200
 !               10 COMM-TL16-HEURE.                                    00711000
 !                  15 COMM-TL16-HH            PIC X(002).              00712000
 !                  15 COMM-TL16-MM            PIC X(002).              00713000
 !               10 COMM-TL16-DATE.                                     00720000
 !                  15 COMM-TL16-DEMIS-SSAA    PIC X(004).              00730000
 !                  15 COMM-TL16-DEMIS-MM      PIC X(002).              00740000
 !                  15 COMM-TL16-DEMIS-JJ      PIC X(002).              00750000
 !    *                                                                 00751000
 !               10 COMM-TL16-DATE-QT.                                  00751100
 !                  15 COMM-TL16-DATE-QT-AA    PIC 9(02).               00752000
 !                  15 COMM-TL16-DATE-QT-QQQ   PIC 9(03).               00753000
 !               10 COMM-TL16-MESSAGE.                                  00760000
 !                  15 COMM-TL16-CODRET        PIC X(01).               00770000
 !                     88 COMM-TL16-CODRET-OK               VALUE ' '.  00780000
 !                     88 COMM-TL16-CODRET-ERREUR           VALUE '1'.  00790000
 !               10 COMM-TL16-LIBERR           PIC X(58).               00800000
 !    **** PROVENANCE SI APPEL PAR TL00                                         
 !            05 COMM-TL16-FILLER             PIC X(3374).              00630000
 !    *                                                                 00810000
                                                                                
