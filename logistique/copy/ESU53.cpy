      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESU53   ESU53                                              00000020
      ***************************************************************** 00000030
       01   ESU53I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCRL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCRF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCRI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGRL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNMAGRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMAGRF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNMAGRI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMRL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAMRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMRF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMRI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICRL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNCODICRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICRF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCODICRI      PIC X(7).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETML      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MDEFFETML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEFFETMF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDEFFETMI      PIC X(10).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQRL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCMARQRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQRF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMARQRI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDEBL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MDATEDEBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEDEBF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDATEDEBI      PIC X(10).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEFINL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDATEFINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEFINF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDATEFINI      PIC X(10).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBELLEI      PIC X(31).                                 00000530
           02 LIGNEI OCCURS   14 TIMES .                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCMAGL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNSOCMAGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSOCMAGF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNSOCMAGI    PIC X(6).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNCODICI     PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCFAMI  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCMARQI      PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLREFI  PIC X(20).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOBJL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MQSOBJL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQSOBJF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQSOBJI      PIC X(6).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSFORL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQSFORL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQSFORF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQSFORI      PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSMINL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQSMINL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQSMINF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQSMINI      PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSMAXL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQSMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQSMAXF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQSMAXI      PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTENL      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQSTENL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQSTENF      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQSTENI      PIC X(6).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MZONCMDI  PIC X(12).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(61).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: ESU53   ESU53                                              00001200
      ***************************************************************** 00001210
       01   ESU53O REDEFINES ESU53I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNPAGEA   PIC X.                                          00001390
           02 MNPAGEC   PIC X.                                          00001400
           02 MNPAGEP   PIC X.                                          00001410
           02 MNPAGEH   PIC X.                                          00001420
           02 MNPAGEV   PIC X.                                          00001430
           02 MNPAGEO   PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNSOCRA   PIC X.                                          00001460
           02 MNSOCRC   PIC X.                                          00001470
           02 MNSOCRP   PIC X.                                          00001480
           02 MNSOCRH   PIC X.                                          00001490
           02 MNSOCRV   PIC X.                                          00001500
           02 MNSOCRO   PIC X(3).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MNMAGRA   PIC X.                                          00001530
           02 MNMAGRC   PIC X.                                          00001540
           02 MNMAGRP   PIC X.                                          00001550
           02 MNMAGRH   PIC X.                                          00001560
           02 MNMAGRV   PIC X.                                          00001570
           02 MNMAGRO   PIC X(3).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCFAMRA   PIC X.                                          00001600
           02 MCFAMRC   PIC X.                                          00001610
           02 MCFAMRP   PIC X.                                          00001620
           02 MCFAMRH   PIC X.                                          00001630
           02 MCFAMRV   PIC X.                                          00001640
           02 MCFAMRO   PIC X(5).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MNCODICRA      PIC X.                                     00001670
           02 MNCODICRC PIC X.                                          00001680
           02 MNCODICRP PIC X.                                          00001690
           02 MNCODICRH PIC X.                                          00001700
           02 MNCODICRV PIC X.                                          00001710
           02 MNCODICRO      PIC X(7).                                  00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MDEFFETMA      PIC X.                                     00001740
           02 MDEFFETMC PIC X.                                          00001750
           02 MDEFFETMP PIC X.                                          00001760
           02 MDEFFETMH PIC X.                                          00001770
           02 MDEFFETMV PIC X.                                          00001780
           02 MDEFFETMO      PIC X(10).                                 00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MCMARQRA  PIC X.                                          00001810
           02 MCMARQRC  PIC X.                                          00001820
           02 MCMARQRP  PIC X.                                          00001830
           02 MCMARQRH  PIC X.                                          00001840
           02 MCMARQRV  PIC X.                                          00001850
           02 MCMARQRO  PIC X(5).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MDATEDEBA      PIC X.                                     00001880
           02 MDATEDEBC PIC X.                                          00001890
           02 MDATEDEBP PIC X.                                          00001900
           02 MDATEDEBH PIC X.                                          00001910
           02 MDATEDEBV PIC X.                                          00001920
           02 MDATEDEBO      PIC X(10).                                 00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MDATEFINA      PIC X.                                     00001950
           02 MDATEFINC PIC X.                                          00001960
           02 MDATEFINP PIC X.                                          00001970
           02 MDATEFINH PIC X.                                          00001980
           02 MDATEFINV PIC X.                                          00001990
           02 MDATEFINO      PIC X(10).                                 00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLIBELLEA      PIC X.                                     00002020
           02 MLIBELLEC PIC X.                                          00002030
           02 MLIBELLEP PIC X.                                          00002040
           02 MLIBELLEH PIC X.                                          00002050
           02 MLIBELLEV PIC X.                                          00002060
           02 MLIBELLEO      PIC X(31).                                 00002070
           02 LIGNEO OCCURS   14 TIMES .                                00002080
             03 FILLER       PIC X(2).                                  00002090
             03 MNSOCMAGA    PIC X.                                     00002100
             03 MNSOCMAGC    PIC X.                                     00002110
             03 MNSOCMAGP    PIC X.                                     00002120
             03 MNSOCMAGH    PIC X.                                     00002130
             03 MNSOCMAGV    PIC X.                                     00002140
             03 MNSOCMAGO    PIC X(6).                                  00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MNCODICA     PIC X.                                     00002170
             03 MNCODICC     PIC X.                                     00002180
             03 MNCODICP     PIC X.                                     00002190
             03 MNCODICH     PIC X.                                     00002200
             03 MNCODICV     PIC X.                                     00002210
             03 MNCODICO     PIC X(7).                                  00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MCFAMA  PIC X.                                          00002240
             03 MCFAMC  PIC X.                                          00002250
             03 MCFAMP  PIC X.                                          00002260
             03 MCFAMH  PIC X.                                          00002270
             03 MCFAMV  PIC X.                                          00002280
             03 MCFAMO  PIC X(5).                                       00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MCMARQA      PIC X.                                     00002310
             03 MCMARQC PIC X.                                          00002320
             03 MCMARQP PIC X.                                          00002330
             03 MCMARQH PIC X.                                          00002340
             03 MCMARQV PIC X.                                          00002350
             03 MCMARQO      PIC X(5).                                  00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MLREFA  PIC X.                                          00002380
             03 MLREFC  PIC X.                                          00002390
             03 MLREFP  PIC X.                                          00002400
             03 MLREFH  PIC X.                                          00002410
             03 MLREFV  PIC X.                                          00002420
             03 MLREFO  PIC X(20).                                      00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MQSOBJA      PIC X.                                     00002450
             03 MQSOBJC PIC X.                                          00002460
             03 MQSOBJP PIC X.                                          00002470
             03 MQSOBJH PIC X.                                          00002480
             03 MQSOBJV PIC X.                                          00002490
             03 MQSOBJO      PIC X(6).                                  00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MQSFORA      PIC X.                                     00002520
             03 MQSFORC PIC X.                                          00002530
             03 MQSFORP PIC X.                                          00002540
             03 MQSFORH PIC X.                                          00002550
             03 MQSFORV PIC X.                                          00002560
             03 MQSFORO      PIC X(5).                                  00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MQSMINA      PIC X.                                     00002590
             03 MQSMINC PIC X.                                          00002600
             03 MQSMINP PIC X.                                          00002610
             03 MQSMINH PIC X.                                          00002620
             03 MQSMINV PIC X.                                          00002630
             03 MQSMINO      PIC X(5).                                  00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MQSMAXA      PIC X.                                     00002660
             03 MQSMAXC PIC X.                                          00002670
             03 MQSMAXP PIC X.                                          00002680
             03 MQSMAXH PIC X.                                          00002690
             03 MQSMAXV PIC X.                                          00002700
             03 MQSMAXO      PIC X(5).                                  00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MQSTENA      PIC X.                                     00002730
             03 MQSTENC PIC X.                                          00002740
             03 MQSTENP PIC X.                                          00002750
             03 MQSTENH PIC X.                                          00002760
             03 MQSTENV PIC X.                                          00002770
             03 MQSTENO      PIC X(6).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MZONCMDA  PIC X.                                          00002800
           02 MZONCMDC  PIC X.                                          00002810
           02 MZONCMDP  PIC X.                                          00002820
           02 MZONCMDH  PIC X.                                          00002830
           02 MZONCMDV  PIC X.                                          00002840
           02 MZONCMDO  PIC X(12).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(61).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
