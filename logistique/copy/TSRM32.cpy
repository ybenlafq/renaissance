      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM32                                          *  00020001
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
   BF1*01  TS-LONG              PIC S9(3) COMP-3 VALUE 113.             00080001
MH0706*01  TS-LONG              PIC S9(3) COMP-3 VALUE 118.             00080001
JA4288*01  TS-LONG              PIC S9(3) COMP-3 VALUE 120.             00080001
JA4288*01  TS-LONG              PIC S9(3) COMP-3 VALUE 126.             00080001
AD01   01  TS-LONG              PIC S9(3) COMP-3 VALUE 128.                     
       01  TS-DONNEES.                                                  00090000
MH0706*       10 TS-MNCODIC     PIC X(07).                              00100001
MH0706        10 TS-MNCODIC     PIC X(08).                              00100001
              10 TS-MCFAM       PIC X(05).                              00121001
              10 TS-MPSTDTTC    PIC S9(7)V99.                           00121001
              10 TS-MLFAM       PIC X(20).                              00121001
              10 TS-MLAGREG     PIC X(20).                              00121001
              10 TS-MCMARQ      PIC X(05).                              00121001
              10 TS-MLREF       PIC X(20).                              00122001
              10 TS-MCEXPO      PIC X(01).                                      
AD01          10 TS-COLLECTION  PIC X(02).                                      
              10 TS-MLSTATC     PIC X(03).                              00123001
              10 TS-MWASSORT    PIC X(01).                                      
MH0706*       10 TS-MQEXPO      PIC X(05).                                      
MH0706        10 TS-MQEXPO      PIC 9(03).                                      
MH0706*       10 TS-MQLS        PIC X(05).                              00130001
MH0706        10 TS-MQLS        PIC 9(03).                              00130001
              10 TS-MQSTOCK     PIC X(05).                              00140001
MH0706        10 TS-MQSTODSP    PIC X(05).                              00140001
              10 TS-WSENSAPPRO  PIC X.                                          
              10 TS-CHGT        PIC X.                                          
      *BF1    10 TS-GAMMEMANU   PIC X(05).                                      
BF1           10 TS-GAMMEMANU   PIC X(10).                                      
JA4288        10 TS-MQEXPO-TB   PIC 9(03).                                      
JA4288        10 TS-MQLS-TB     PIC 9(03).                              00130001
                                                                                
