      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVTL0300                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVTL0300                         
      **********************************************************                
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVTL0300.                                                            
           02  TL03-NSOC                                                        
               PIC X(0003).                                                     
           02  TL03-CLIVR                                                       
               PIC X(0010).                                                     
           02  TL03-LNOM                                                        
               PIC X(0025).                                                     
           02  TL03-CTYPE                                                       
               PIC X(0005).                                                     
           02  TL03-NMATRICULE                                                  
               PIC X(0007).                                                     
           02  TL03-LQUALIF                                                     
               PIC X(0020).                                                     
           02  TL03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVTL0300                                  
      **********************************************************                
       01  RVTL0300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL03-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL03-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL03-CLIVR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL03-CLIVR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL03-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL03-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL03-CTYPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL03-CTYPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL03-NMATRICULE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL03-NMATRICULE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL03-LQUALIF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL03-LQUALIF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  TL03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
EMOD                                                                            
      *}                                                                        
