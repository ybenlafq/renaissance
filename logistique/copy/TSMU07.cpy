      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *-----------------------------------------------------------------00000020
      * TSMU07   : TS DES PROGRAMMES TMU07, MMU08 ET TMU24              00000030
      * LONGUEUR : 26                                                   00000040
      *-----------------------------------------------------------------00000050
      *                                                                 00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  LONG-TS-LINK                 PIC S9(4)   COMP  VALUE 26.     00000070
      *--                                                                       
       01  LONG-TS-LINK                 PIC S9(4) COMP-5  VALUE 26.             
      *}                                                                        
      *                                                                 00000080
       01  STRUCT-TS-LINK.                                              00000090
           05  TS-NMUTATION-LINK       PIC  X(07).                      00000100
           05  TS-NSOCIETE-LINK        PIC  X(03).                      00000110
           05  TS-NLIEU-LINK           PIC  X(03).                      00000120
           05  TS-CSELART-LINK         PIC  X(05).                      00000130
           05  TS-DMUTATION-LINK       PIC  X(08).                      00000140
      *                                                                 00000150
                                                                                
