      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MGD17-LONG-COMMAREA            PIC S9(4) COMP.                  
      *--                                                                       
       01  COMM-MGD17-LONG-COMMAREA            PIC S9(4) COMP-5.                
      *}                                                                        
       01  COMM-MGD17-APPLI.                                                    
           02 COMM-MGD17-ZONES-ENTREE.                                          
              05 COMM-MGD17-NSOCDEPOT          PIC X(03).                       
              05 COMM-MGD17-NDEPOT             PIC X(03).                       
              05 COMM-MGD17-DRAFALE            PIC X(08).                       
              05 COMM-MGD17-CSELART            PIC X(05).                       
           02 COMM-MGD17-ZONES-SORTIE.                                          
              05 COMM-MGD17-CODRET             PIC S9(04).                      
              05 COMM-MGD17-LMESS              PIC X(64).                       
              05 COMM-MGD17-NRAFALE            PIC X(03) OCCURS 10.             
              05 COMM-MGD17-FILLER             PIC X(33).                       
                                                                                
