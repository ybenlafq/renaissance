      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EZQ12   EZQ12                                              00000020
      ***************************************************************** 00000030
       01   EZQ12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCIETEI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(18).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPLIVL   COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNSOCDEPLIVL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNSOCDEPLIVF   PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCDEPLIVI   PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUDEPLIVL  COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNLIEUDEPLIVL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNLIEUDEPLIVF  PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNLIEUDEPLIVI  PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPERIML  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCPERIML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPERIMF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCPERIMI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPERIML  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLPERIML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPERIMF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLPERIMI  PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORDER1L      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNORDER1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNORDER1F      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNORDER1I      PIC X.                                     00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORDER2L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MNORDER2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNORDER2F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNORDER2I      PIC X.                                     00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORDER3L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNORDER3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNORDER3F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNORDER3I      PIC X.                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORDER4L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MNORDER4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNORDER4F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNORDER4I      PIC X.                                     00000650
           02 MLIGNEI OCCURS   10 TIMES .                               00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOSTALL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCPOSTALL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCPOSTALF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCPOSTALI    PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMUNEL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLCOMUNEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLCOMUNEF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLCOMUNEI    PIC X(32).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCINSEEL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCINSEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCINSEEF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCINSEEI     PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCONTRAL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MWCONTRAL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWCONTRAF    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MWCONTRAI    PIC X.                                     00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCELEMENL    COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MCELEMENL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCELEMENF    PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCELEMENI    PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLZONELEL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MLZONELEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLZONELEF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MLZONELEI    PIC X(20).                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: EZQ12   EZQ12                                              00001160
      ***************************************************************** 00001170
       01   EZQ12O REDEFINES EZQ12I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNPAGEA   PIC X.                                          00001350
           02 MNPAGEC   PIC X.                                          00001360
           02 MNPAGEP   PIC X.                                          00001370
           02 MNPAGEH   PIC X.                                          00001380
           02 MNPAGEV   PIC X.                                          00001390
           02 MNPAGEO   PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MPAGEMAXA      PIC X.                                     00001420
           02 MPAGEMAXC PIC X.                                          00001430
           02 MPAGEMAXP PIC X.                                          00001440
           02 MPAGEMAXH PIC X.                                          00001450
           02 MPAGEMAXV PIC X.                                          00001460
           02 MPAGEMAXO      PIC X(3).                                  00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNSOCIETEA     PIC X.                                     00001490
           02 MNSOCIETEC     PIC X.                                     00001500
           02 MNSOCIETEP     PIC X.                                     00001510
           02 MNSOCIETEH     PIC X.                                     00001520
           02 MNSOCIETEV     PIC X.                                     00001530
           02 MNSOCIETEO     PIC X(3).                                  00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNLIEUA   PIC X.                                          00001560
           02 MNLIEUC   PIC X.                                          00001570
           02 MNLIEUP   PIC X.                                          00001580
           02 MNLIEUH   PIC X.                                          00001590
           02 MNLIEUV   PIC X.                                          00001600
           02 MNLIEUO   PIC X(3).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLLIEUA   PIC X.                                          00001630
           02 MLLIEUC   PIC X.                                          00001640
           02 MLLIEUP   PIC X.                                          00001650
           02 MLLIEUH   PIC X.                                          00001660
           02 MLLIEUV   PIC X.                                          00001670
           02 MLLIEUO   PIC X(18).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNSOCDEPLIVA   PIC X.                                     00001700
           02 MNSOCDEPLIVC   PIC X.                                     00001710
           02 MNSOCDEPLIVP   PIC X.                                     00001720
           02 MNSOCDEPLIVH   PIC X.                                     00001730
           02 MNSOCDEPLIVV   PIC X.                                     00001740
           02 MNSOCDEPLIVO   PIC X(3).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNLIEUDEPLIVA  PIC X.                                     00001770
           02 MNLIEUDEPLIVC  PIC X.                                     00001780
           02 MNLIEUDEPLIVP  PIC X.                                     00001790
           02 MNLIEUDEPLIVH  PIC X.                                     00001800
           02 MNLIEUDEPLIVV  PIC X.                                     00001810
           02 MNLIEUDEPLIVO  PIC X(3).                                  00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCPERIMA  PIC X.                                          00001840
           02 MCPERIMC  PIC X.                                          00001850
           02 MCPERIMP  PIC X.                                          00001860
           02 MCPERIMH  PIC X.                                          00001870
           02 MCPERIMV  PIC X.                                          00001880
           02 MCPERIMO  PIC X(5).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MLPERIMA  PIC X.                                          00001910
           02 MLPERIMC  PIC X.                                          00001920
           02 MLPERIMP  PIC X.                                          00001930
           02 MLPERIMH  PIC X.                                          00001940
           02 MLPERIMV  PIC X.                                          00001950
           02 MLPERIMO  PIC X(20).                                      00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNORDER1A      PIC X.                                     00001980
           02 MNORDER1C PIC X.                                          00001990
           02 MNORDER1P PIC X.                                          00002000
           02 MNORDER1H PIC X.                                          00002010
           02 MNORDER1V PIC X.                                          00002020
           02 MNORDER1O      PIC X.                                     00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNORDER2A      PIC X.                                     00002050
           02 MNORDER2C PIC X.                                          00002060
           02 MNORDER2P PIC X.                                          00002070
           02 MNORDER2H PIC X.                                          00002080
           02 MNORDER2V PIC X.                                          00002090
           02 MNORDER2O      PIC X.                                     00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MNORDER3A      PIC X.                                     00002120
           02 MNORDER3C PIC X.                                          00002130
           02 MNORDER3P PIC X.                                          00002140
           02 MNORDER3H PIC X.                                          00002150
           02 MNORDER3V PIC X.                                          00002160
           02 MNORDER3O      PIC X.                                     00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MNORDER4A      PIC X.                                     00002190
           02 MNORDER4C PIC X.                                          00002200
           02 MNORDER4P PIC X.                                          00002210
           02 MNORDER4H PIC X.                                          00002220
           02 MNORDER4V PIC X.                                          00002230
           02 MNORDER4O      PIC X.                                     00002240
           02 MLIGNEO OCCURS   10 TIMES .                               00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MCPOSTALA    PIC X.                                     00002270
             03 MCPOSTALC    PIC X.                                     00002280
             03 MCPOSTALP    PIC X.                                     00002290
             03 MCPOSTALH    PIC X.                                     00002300
             03 MCPOSTALV    PIC X.                                     00002310
             03 MCPOSTALO    PIC X(5).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MLCOMUNEA    PIC X.                                     00002340
             03 MLCOMUNEC    PIC X.                                     00002350
             03 MLCOMUNEP    PIC X.                                     00002360
             03 MLCOMUNEH    PIC X.                                     00002370
             03 MLCOMUNEV    PIC X.                                     00002380
             03 MLCOMUNEO    PIC X(32).                                 00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MCINSEEA     PIC X.                                     00002410
             03 MCINSEEC     PIC X.                                     00002420
             03 MCINSEEP     PIC X.                                     00002430
             03 MCINSEEH     PIC X.                                     00002440
             03 MCINSEEV     PIC X.                                     00002450
             03 MCINSEEO     PIC X(5).                                  00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MWCONTRAA    PIC X.                                     00002480
             03 MWCONTRAC    PIC X.                                     00002490
             03 MWCONTRAP    PIC X.                                     00002500
             03 MWCONTRAH    PIC X.                                     00002510
             03 MWCONTRAV    PIC X.                                     00002520
             03 MWCONTRAO    PIC X.                                     00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MCELEMENA    PIC X.                                     00002550
             03 MCELEMENC    PIC X.                                     00002560
             03 MCELEMENP    PIC X.                                     00002570
             03 MCELEMENH    PIC X.                                     00002580
             03 MCELEMENV    PIC X.                                     00002590
             03 MCELEMENO    PIC X(5).                                  00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MLZONELEA    PIC X.                                     00002620
             03 MLZONELEC    PIC X.                                     00002630
             03 MLZONELEP    PIC X.                                     00002640
             03 MLZONELEH    PIC X.                                     00002650
             03 MLZONELEV    PIC X.                                     00002660
             03 MLZONELEO    PIC X(20).                                 00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
