      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS40   EGS40                                              00000020
      ***************************************************************** 00000030
       01   EGS40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(13).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCODICI   PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLCODICI  PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAML     COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MFAML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFAMF     PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MFAMI     PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLFAMI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMARQUEL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MMARQUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMARQUEF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MMARQUEI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQUEL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLMARQUEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMARQUEF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLMARQUEI      PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDUL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDATEDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATEDUF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDATEDUI  PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEAUL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MDATEAUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATEAUF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDATEAUI  PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIGDEST1L    COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MORIGDEST1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MORIGDEST1F    PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MORIGDEST1I    PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIGDEST2L    COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MORIGDEST2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MORIGDEST2F    PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MORIGDEST2I    PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIGDEST3L    COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MORIGDEST3L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MORIGDEST3F    PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MORIGDEST3I    PIC X(3).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIGDEST4L    COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MORIGDEST4L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MORIGDEST4F    PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MORIGDEST4I    PIC X(5).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIG1L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MORIG1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MORIG1F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MORIG1I   PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIG2L   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MORIG2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MORIG2F   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MORIG2I   PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIG3L   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MORIG3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MORIG3F   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MORIG3I   PIC X(3).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIG4L   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MORIG4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MORIG4F   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MORIG4I   PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEST1L   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MDEST1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEST1F   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MDEST1I   PIC X(3).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEST2L   COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MDEST2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEST2F   PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MDEST2I   PIC X(3).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEST3L   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MDEST3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEST3F   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MDEST3I   PIC X(3).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEST4L   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MDEST4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEST4F   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MDEST4I   PIC X(5).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MUTILL    COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MUTILL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MUTILF    PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MUTILI    PIC X(5).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLUTILL   COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLUTILL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLUTILF   PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLUTILI   PIC X(20).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLIBERRI  PIC X(77).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCODTRAI  PIC X(4).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCICSI    PIC X(5).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MNETNAMI  PIC X(8).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MSCREENI  PIC X(4).                                       00001250
      ***************************************************************** 00001260
      * SDF: EGS40   EGS40                                              00001270
      ***************************************************************** 00001280
       01   EGS40O REDEFINES EGS40I.                                    00001290
           02 FILLER    PIC X(12).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MDATJOUA  PIC X.                                          00001320
           02 MDATJOUC  PIC X.                                          00001330
           02 MDATJOUP  PIC X.                                          00001340
           02 MDATJOUH  PIC X.                                          00001350
           02 MDATJOUV  PIC X.                                          00001360
           02 MDATJOUO  PIC X(10).                                      00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MTIMJOUA  PIC X.                                          00001390
           02 MTIMJOUC  PIC X.                                          00001400
           02 MTIMJOUP  PIC X.                                          00001410
           02 MTIMJOUH  PIC X.                                          00001420
           02 MTIMJOUV  PIC X.                                          00001430
           02 MTIMJOUO  PIC X(5).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MZONCMDA  PIC X.                                          00001460
           02 MZONCMDC  PIC X.                                          00001470
           02 MZONCMDP  PIC X.                                          00001480
           02 MZONCMDH  PIC X.                                          00001490
           02 MZONCMDV  PIC X.                                          00001500
           02 MZONCMDO  PIC X(13).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MCODICA   PIC X.                                          00001530
           02 MCODICC   PIC X.                                          00001540
           02 MCODICP   PIC X.                                          00001550
           02 MCODICH   PIC X.                                          00001560
           02 MCODICV   PIC X.                                          00001570
           02 MCODICO   PIC X(7).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MLCODICA  PIC X.                                          00001600
           02 MLCODICC  PIC X.                                          00001610
           02 MLCODICP  PIC X.                                          00001620
           02 MLCODICH  PIC X.                                          00001630
           02 MLCODICV  PIC X.                                          00001640
           02 MLCODICO  PIC X(20).                                      00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MFAMA     PIC X.                                          00001670
           02 MFAMC     PIC X.                                          00001680
           02 MFAMP     PIC X.                                          00001690
           02 MFAMH     PIC X.                                          00001700
           02 MFAMV     PIC X.                                          00001710
           02 MFAMO     PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLFAMA    PIC X.                                          00001740
           02 MLFAMC    PIC X.                                          00001750
           02 MLFAMP    PIC X.                                          00001760
           02 MLFAMH    PIC X.                                          00001770
           02 MLFAMV    PIC X.                                          00001780
           02 MLFAMO    PIC X(20).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MMARQUEA  PIC X.                                          00001810
           02 MMARQUEC  PIC X.                                          00001820
           02 MMARQUEP  PIC X.                                          00001830
           02 MMARQUEH  PIC X.                                          00001840
           02 MMARQUEV  PIC X.                                          00001850
           02 MMARQUEO  PIC X(5).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLMARQUEA      PIC X.                                     00001880
           02 MLMARQUEC PIC X.                                          00001890
           02 MLMARQUEP PIC X.                                          00001900
           02 MLMARQUEH PIC X.                                          00001910
           02 MLMARQUEV PIC X.                                          00001920
           02 MLMARQUEO      PIC X(20).                                 00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MDATEDUA  PIC X.                                          00001950
           02 MDATEDUC  PIC X.                                          00001960
           02 MDATEDUP  PIC X.                                          00001970
           02 MDATEDUH  PIC X.                                          00001980
           02 MDATEDUV  PIC X.                                          00001990
           02 MDATEDUO  PIC X(8).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MDATEAUA  PIC X.                                          00002020
           02 MDATEAUC  PIC X.                                          00002030
           02 MDATEAUP  PIC X.                                          00002040
           02 MDATEAUH  PIC X.                                          00002050
           02 MDATEAUV  PIC X.                                          00002060
           02 MDATEAUO  PIC X(8).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MORIGDEST1A    PIC X.                                     00002090
           02 MORIGDEST1C    PIC X.                                     00002100
           02 MORIGDEST1P    PIC X.                                     00002110
           02 MORIGDEST1H    PIC X.                                     00002120
           02 MORIGDEST1V    PIC X.                                     00002130
           02 MORIGDEST1O    PIC X(3).                                  00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MORIGDEST2A    PIC X.                                     00002160
           02 MORIGDEST2C    PIC X.                                     00002170
           02 MORIGDEST2P    PIC X.                                     00002180
           02 MORIGDEST2H    PIC X.                                     00002190
           02 MORIGDEST2V    PIC X.                                     00002200
           02 MORIGDEST2O    PIC X(3).                                  00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MORIGDEST3A    PIC X.                                     00002230
           02 MORIGDEST3C    PIC X.                                     00002240
           02 MORIGDEST3P    PIC X.                                     00002250
           02 MORIGDEST3H    PIC X.                                     00002260
           02 MORIGDEST3V    PIC X.                                     00002270
           02 MORIGDEST3O    PIC X(3).                                  00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MORIGDEST4A    PIC X.                                     00002300
           02 MORIGDEST4C    PIC X.                                     00002310
           02 MORIGDEST4P    PIC X.                                     00002320
           02 MORIGDEST4H    PIC X.                                     00002330
           02 MORIGDEST4V    PIC X.                                     00002340
           02 MORIGDEST4O    PIC X(5).                                  00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MORIG1A   PIC X.                                          00002370
           02 MORIG1C   PIC X.                                          00002380
           02 MORIG1P   PIC X.                                          00002390
           02 MORIG1H   PIC X.                                          00002400
           02 MORIG1V   PIC X.                                          00002410
           02 MORIG1O   PIC X(3).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MORIG2A   PIC X.                                          00002440
           02 MORIG2C   PIC X.                                          00002450
           02 MORIG2P   PIC X.                                          00002460
           02 MORIG2H   PIC X.                                          00002470
           02 MORIG2V   PIC X.                                          00002480
           02 MORIG2O   PIC X(3).                                       00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MORIG3A   PIC X.                                          00002510
           02 MORIG3C   PIC X.                                          00002520
           02 MORIG3P   PIC X.                                          00002530
           02 MORIG3H   PIC X.                                          00002540
           02 MORIG3V   PIC X.                                          00002550
           02 MORIG3O   PIC X(3).                                       00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MORIG4A   PIC X.                                          00002580
           02 MORIG4C   PIC X.                                          00002590
           02 MORIG4P   PIC X.                                          00002600
           02 MORIG4H   PIC X.                                          00002610
           02 MORIG4V   PIC X.                                          00002620
           02 MORIG4O   PIC X(5).                                       00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MDEST1A   PIC X.                                          00002650
           02 MDEST1C   PIC X.                                          00002660
           02 MDEST1P   PIC X.                                          00002670
           02 MDEST1H   PIC X.                                          00002680
           02 MDEST1V   PIC X.                                          00002690
           02 MDEST1O   PIC X(3).                                       00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MDEST2A   PIC X.                                          00002720
           02 MDEST2C   PIC X.                                          00002730
           02 MDEST2P   PIC X.                                          00002740
           02 MDEST2H   PIC X.                                          00002750
           02 MDEST2V   PIC X.                                          00002760
           02 MDEST2O   PIC X(3).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MDEST3A   PIC X.                                          00002790
           02 MDEST3C   PIC X.                                          00002800
           02 MDEST3P   PIC X.                                          00002810
           02 MDEST3H   PIC X.                                          00002820
           02 MDEST3V   PIC X.                                          00002830
           02 MDEST3O   PIC X(3).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MDEST4A   PIC X.                                          00002860
           02 MDEST4C   PIC X.                                          00002870
           02 MDEST4P   PIC X.                                          00002880
           02 MDEST4H   PIC X.                                          00002890
           02 MDEST4V   PIC X.                                          00002900
           02 MDEST4O   PIC X(5).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MUTILA    PIC X.                                          00002930
           02 MUTILC    PIC X.                                          00002940
           02 MUTILP    PIC X.                                          00002950
           02 MUTILH    PIC X.                                          00002960
           02 MUTILV    PIC X.                                          00002970
           02 MUTILO    PIC X(5).                                       00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MLUTILA   PIC X.                                          00003000
           02 MLUTILC   PIC X.                                          00003010
           02 MLUTILP   PIC X.                                          00003020
           02 MLUTILH   PIC X.                                          00003030
           02 MLUTILV   PIC X.                                          00003040
           02 MLUTILO   PIC X(20).                                      00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MLIBERRA  PIC X.                                          00003070
           02 MLIBERRC  PIC X.                                          00003080
           02 MLIBERRP  PIC X.                                          00003090
           02 MLIBERRH  PIC X.                                          00003100
           02 MLIBERRV  PIC X.                                          00003110
           02 MLIBERRO  PIC X(77).                                      00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MCODTRAA  PIC X.                                          00003140
           02 MCODTRAC  PIC X.                                          00003150
           02 MCODTRAP  PIC X.                                          00003160
           02 MCODTRAH  PIC X.                                          00003170
           02 MCODTRAV  PIC X.                                          00003180
           02 MCODTRAO  PIC X(4).                                       00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MCICSA    PIC X.                                          00003210
           02 MCICSC    PIC X.                                          00003220
           02 MCICSP    PIC X.                                          00003230
           02 MCICSH    PIC X.                                          00003240
           02 MCICSV    PIC X.                                          00003250
           02 MCICSO    PIC X(5).                                       00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MNETNAMA  PIC X.                                          00003280
           02 MNETNAMC  PIC X.                                          00003290
           02 MNETNAMP  PIC X.                                          00003300
           02 MNETNAMH  PIC X.                                          00003310
           02 MNETNAMV  PIC X.                                          00003320
           02 MNETNAMO  PIC X(8).                                       00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MSCREENA  PIC X.                                          00003350
           02 MSCREENC  PIC X.                                          00003360
           02 MSCREENP  PIC X.                                          00003370
           02 MSCREENH  PIC X.                                          00003380
           02 MSCREENV  PIC X.                                          00003390
           02 MSCREENO  PIC X(4).                                       00003400
                                                                                
