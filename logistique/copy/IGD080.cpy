      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -AIDA *********************************************************         
      *  DESCRIPTION DE L ETAT DE DESTOCKAGE ENLEVEMENTS MAGASIN                
      *****************************************************************         
      *                                                                         
       01  IGD080.                                                              
      *****************************************************************         
      *  ENTETE PHYSIQUE DE PAGE                                                
      *****************************************************************         
          02 W-LIG-ENTETE1.                                                     
            04 FILLER PIC X(02) VALUE SPACES.                                   
            04 FILLER PIC X(06) VALUE 'IGD080'.                                 
            04 FILLER PIC X(32) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE ' E T A B L I S S E M'.                   
            04 FILLER PIC X(20) VALUE ' E N T S    D A R T '.                   
            04 FILLER PIC X(19) VALUE 'Y                  '.                    
            04 FILLER PIC X(19) VALUE ' E D I T E    L E  '.                    
            04 FILLER PIC X(02) VALUE SPACES.                                   
            04 W-LIG-DATE PIC X(8).                                             
            04 FILLER PIC X(03) VALUE SPACES.                                   
      *****************************************************************         
          02 W-LIG-ENTETE2.                                                     
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 W-LIG-ENTETE3.                                                     
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(21) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     LISTE DE DESTOC'.                   
            04 FILLER PIC X(20) VALUE 'KAGE ENLEVEMENTS    '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(19) VALUE '                PAG'.                    
            04 FILLER PIC X(4) VALUE 'E : '.                                    
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 W-LIG-PAGE PIC ZZ9.                                              
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(2) VALUE SPACES.                                    
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 W-LIG-ENTETE4.                                                     
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '                MAGA'.                   
            04 FILLER PIC X(20) VALUE 'SINS                '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 W-LIG-ENTETE5.                                                     
            04 FILLER PIC X(15) VALUE 'ENTREPOT     : '.                        
            04 W-LIG-NSOCDEPOT PIC X(3).                                        
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 W-LIG-NDEPOT    PIC X(3).                                        
      *****************************************************************         
          02 W-LIG-ENTETE6.                                                     
            04 FILLER PIC X(15) VALUE 'JOURNEE      : '.                        
            04 W-LIG-DRAFALE   PIC X(8).                                        
      *****************************************************************         
          02 W-LIG-ENTETE7.                                                     
            04 FILLER PIC X(15) VALUE 'NO RAFALE    : '.                        
            04 W-LIG-NRAFALE   PIC X(8).                                        
      *****************************************************************         
          02 W-LIG-ENTETE8.                                                     
            04 FILLER PIC X(15) VALUE 'HEURE LANCNT : '.                        
            04 W-LIG-HSAISIE   PIC X(2).                                        
            04 FILLER PIC X(03) VALUE ' H '.                                    
            04 W-LIG-MSAISIE   PIC X(2).                                        
      *****************************************************************         
          02 W-LIG-ENTETE8BIS.                                                  
            04 FILLER PIC X(15) VALUE 'SATELLITE    : '.                        
            04 W-LIG-SATELLITE PIC X(2).                                        
            04 FILLER PIC X(03) VALUE SPACES.                                   
            04 FILLER PIC X(2)  VALUE SPACES.                                   
      *****************************************************************         
          02 W-LIG-ENTETE9.                                                     
            04 FILLER PIC X(15) VALUE '+--------------'.                        
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(16) VALUE '----------------'.                       
            04 FILLER PIC X(01) VALUE '+'.                                      
      *****************************************************************         
          02 W-LIG-ENTETE10.                                                    
            04 FILLER PIC X(13) VALUE '! SOC MAG  VE'.                          
            04 FILLER PIC X(20) VALUE 'NTE           NOM CL'.                   
            04 FILLER PIC X(20) VALUE 'IENT          !   CO'.                   
            04 FILLER PIC X(20) VALUE 'DIC   FAM    MQE    '.                   
            04 FILLER PIC X(31) VALUE '    REFERENCE         !  N BE  '.        
            04 FILLER PIC X(28) VALUE ' ! QTE ! LIEU ! DESTINATION!'.           
      *****************************************************************         
          02 W-LIG-ENTETE11.                                                    
            04 FILLER PIC X(15) VALUE '!              '.                        
            04 FILLER PIC X(20) VALUE '                    '.                   
            04 FILLER PIC X(20) VALUE '            !       '.                   
            04 FILLER PIC X(20) VALUE '                    '.                   
            04 FILLER PIC X(31) VALUE '                    !         !'.        
            04 FILLER PIC X(28) VALUE '     !      !            !'.             
      *****************************************************************         
          02 W-LIG-DETAIL.                                                      
            04 FILLER PIC X(03) VALUE '! '.                                     
            04 W-LIG-NSOCIETE PIC X(3).                                         
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 W-LIG-NMAGASIN PIC X(3).                                         
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 W-LIG-NORIGINE PIC X(7).                                         
            04 FILLER PIC X(02) VALUE SPACES.                                   
            04 W-LIG-NCLIENT  PIC X(25).                                        
            04 FILLER PIC X(02) VALUE SPACES.                                   
            04 FILLER PIC X(1) VALUE '!'.                                       
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 W-LIG-NCODIC   PIC X(7).                                         
            04 FILLER PIC X(02) VALUE SPACES.                                   
            04 W-LIG-CFAM     PIC X(5).                                         
            04 FILLER PIC X(02) VALUE SPACES.                                   
            04 W-LIG-CMARQ    PIC X(5).                                         
            04 FILLER PIC X(03) VALUE SPACES.                                   
            04 W-LIG-LREFFOURN PIC X(20).                                       
            04 FILLER PIC X(02) VALUE SPACES.                                   
            04 FILLER PIC X(02) VALUE '! '.                                     
            04 W-LIG-NBE PIC X(7).                                              
            04 FILLER PIC X(02) VALUE ' !'.                                     
            04 W-LIG-QCDEDEM   PIC ZZZ9.                                        
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(1) VALUE '!'.                                       
            04 W-LIG-NSOCBE    PIC XXX.                                         
            04 W-LIG-NLIEUBE   PIC XXX.                                         
            04 FILLER PIC X(4) VALUE '!   '.                                    
            04 W-LIG-DEST.                                                      
               05 W-LIG-NSATELLITE         PIC X(2).                            
               05 W-LIG-SLASH              PIC X(1).                            
               05 W-LIG-NCASE              PIC X(3).                            
            04 FILLER PIC X(4) VALUE '   !'.                                    
      *                                                                         
                                                                                
