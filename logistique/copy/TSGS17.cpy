      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *             TRANSACTION "GS00" / PROGRAMME TGS01              *         
      *   TS : CONSULTATION DES STOCKS ENTREPOT PAR FAMILLE           *         
      *****************************************************************         
      *                                                               *         
       01  TS-GS17-NOM.                                                         
           05  FILLER                      PIC X(4) VALUE 'GS17'.               
           05  TS-GS17-TERMINAL            PIC X(4).                            
      *---------------------------800 * 07 LONGUEUR TS                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-GS17-LONG                    PIC S9(4) COMP VALUE +5600.          
      *--                                                                       
       01  TS-GS17-LONG                    PIC S9(4) COMP-5 VALUE +5600.        
      *}                                                                        
      *----------------------------------  DONNEES  TS                          
       01  TS-GS17-DONNEES.                                                     
           02 TS-GS17-LIGNE                 OCCURS 800.                         
      *----------------------------------  CODE FAMILLE                         
              03 TS-GS17-FAM               PIC X(5).                            
      *----------------------------------  RANG-TS (TSGS01)                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 TS-GS17-RANG-TS           PIC S9(4) COMP.                      
      *                                                                         
      *--                                                                       
              03 TS-GS17-RANG-TS           PIC S9(4) COMP-5.                    
                                                                                
      *}                                                                        
