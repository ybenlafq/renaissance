      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00000010
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *00000020
      ******************************************************************00000030
      *                                                                *00000040
      *  PROJET     :                                                   00000050
      *  PROGRAMME  : MLW05                                            *00000060
      *  TITRE      : COMMAREA DES VALIDATIONS DE TRANSIT RECEPTION    *00000070
      *  LONGUEUR   : 4096C                                            *00000100
      *                                                                *00000110
      ******************************************************************00000120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-LW05-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                  
      *--                                                                       
       01  COMM-LW05-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
       01  COMM-LW05-APPLI.                                             00000130
                05 COMM-LW05-ZONES-ENTREE.                              00000140
                  10 COMM-LW05-CTRAITEMENT   PIC X.                             
                  10 COMM-LW05-CACID         PIC X(08).                         
                  10 COMM-LW05-NPROG         PIC X(08).                         
                  10 COMM-LW05-APPLID        PIC X(08).                         
                  10 COMM-LW05-TERMID        PIC X(04).                         
                  10 COMM-LW05-USERID        PIC X(08).                         
                  10 COMM-LW05-DATEJOUR      PIC X(08).                         
                  10 COMM-LW05-NSOCORIG      PIC X(03).                 00000200
                  10 COMM-LW05-NLIEUORIG     PIC X(03).                 00000200
                  10 COMM-LW05-NSOCDEST      PIC X(03).                 00000200
                  10 COMM-LW05-NLIEUDEST     PIC X(03).                 00000200
                  10 COMM-LW05-CTYPDOC       PIC X(02).                 00000200
                  10 COMM-LW05-NUMDOC        PIC X(07).                 00000200
                  10 COMM-LW05-COPAR         PIC X(05).                 00000200
                05 COMM-LW05-ZONES-SORTIE.                              00000220
                  10 COMM-LW05-CRETOUR       PIC X(02).                 00000230
                  10 COMM-LW05-LRETOUR       PIC X(80).                 00000231
AA0314*         05 COMM-LW05-FILLER          PIC X(3943).               00000240
AA0314          05 COMM-LW05-DIVERS.                                            
AA0314            10 COMM-LW05-NB-MSG-MQ     PIC 9(05).                         
AA0314            10 FILLER                  PIC X(938).                        
AA0314          05 COMM-LW05-FILLER          PIC X(3000).               00000240
                                                                                
