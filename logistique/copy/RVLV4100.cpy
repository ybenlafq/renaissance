      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RTLV41                             *        
      ******************************************************************        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLV4100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLV4100.                                                            
           10 LV41-NSOCDEPOT       PIC X(3).                                    
           10 LV41-NDEPOT          PIC X(3).                                    
           10 LV41-NCODIC          PIC X(7).                                    
           10 LV41-BLOCAGE         PIC X(3).                                    
           10 LV41-QPILE           PIC S9(9)V USAGE COMP-3.                     
           10 LV41-DEFFET          PIC X(8).                                    
           10 LV41-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLV4100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLV4100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV41-NSOCDEPOT-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 LV41-NSOCDEPOT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV41-NDEPOT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 LV41-NDEPOT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV41-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 LV41-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV41-BLOCAGE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 LV41-BLOCAGE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV41-QPILE-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 LV41-QPILE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV41-DEFFET-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 LV41-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV41-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 LV41-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
