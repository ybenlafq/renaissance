      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CTSAP TABLE DE TRANSCO POUR SAP        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCTSAP.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCTSAP.                                                             
      *}                                                                        
           05  CTSAP-CTABLEG2    PIC X(15).                                     
           05  CTSAP-CTABLEG2-REDEF REDEFINES CTSAP-CTABLEG2.                   
               10  CTSAP-CENT            PIC X(10).                             
               10  CTSAP-TYPC            PIC X(02).                             
           05  CTSAP-WTABLEG     PIC X(80).                                     
           05  CTSAP-WTABLEG-REDEF  REDEFINES CTSAP-WTABLEG.                    
               10  CTSAP-CSAP            PIC X(20).                             
               10  CTSAP-LCODE           PIC X(30).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCTSAP-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCTSAP-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTSAP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CTSAP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTSAP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CTSAP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
