      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COMMAREA SPECIFIQUES AUX PROGRAMMES TTL27 ET TTL28           00010000
      *                                                  LONGUEUR 3500  00020000
            03 COMM-TL27-DATA REDEFINES COMM-TL00-FILLER.               00030000
               05 COMM-TL27-WPAGIN.                                     00040000
      *                                      PAGE EN COURS              00050000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           07 COMM-TL27-NPAGE     PIC S9(4) COMP.                00060000
      *--                                                                       
                  07 COMM-TL27-NPAGE     PIC S9(4) COMP-5.                      
      *}                                                                        
      *                                      NOMBRE DE PAGE MAXIMUM     00070000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           07 COMM-TL27-NPAGE-MAX PIC S9(4) COMP.                00080000
      *--                                                                       
                  07 COMM-TL27-NPAGE-MAX PIC S9(4) COMP-5.                      
      *}                                                                        
      *                                      MISE A JOUR EFFECTUEE      00090000
      *                                          NON =>  ' '            00100000
      *                                          OUI =>  '1'            00110000
               05 COMM-TL27-TRAITEMENT   PIC X.                         00120000
               05 COMM-TL27-POSITION.                                   00130000
      *                                          FIN DE BASE            00140000
                  07 COMM-TL27-FIN-ACCES PIC X.                         00150000
      *                                          DERNIER NUMERO D'ORDRE 00160000
      *                                          LU                     00170000
                  07 COMM-TL27-NORDRE    PIC X(2).                      00180000
      *                                          TOTAL ENCAISSE INITIAL 00190000
               05 COMM-TL27-TOTENC0      PIC S9(6)V99.                  00200000
               05 COMM-TL27-TOTENC1      PIC S9(6)V99.                  00200000
               05 COMM-TL27-CLIVR1-I     PIC X(10).                     00210000
               05 COMM-TL27-CLIVR2-I     PIC X(10).                     00220000
               05 COMM-TL27-CLIVR3-I     PIC X(10).                     00230000
               05 COMM-TL27-CLIVR1-F     PIC X(10).                     00240000
               05 COMM-TL27-LLIVR1-F     PIC X(20).                     00250000
               05 COMM-TL27-CLIVR2-F     PIC X(10).                     00260000
               05 COMM-TL27-LLIVR2-F     PIC X(20).                     00270000
               05 COMM-TL27-CLIVR3-F     PIC X(10).                     00280000
               05 COMM-TL27-LLIVR3-F     PIC X(20).                     00290000
               05 COMM-TL27-XCTL.                                       00300000
                  07 COMM-TL27-NMAG         PIC X(3).                   00310000
                  07 COMM-TL27-NVENTE       PIC X(7).                   00320000
                  07 COMM-TL27-NLIGNE       PIC 9(2).                   00320100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           07 COMM-TLHS-NITEM        PIC S9(4) COMP.             00320110
      *--                                                                       
                  07 COMM-TLHS-NITEM        PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           07 COMM-TLH1-NITEM        PIC S9(4) COMP.             00320120
      *--                                                                       
                  07 COMM-TLH1-NITEM        PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           07 COMM-TLH2-NITEM        PIC S9(4) COMP.             00320130
      *--                                                                       
                  07 COMM-TLH2-NITEM        PIC S9(4) COMP-5.                   
      *}                                                                        
               05 COMM-TL28-XCTL.                                       00320200
                  07 COMM-TL28-CADRTOUR     PIC X.                      00330000
                  07 COMM-TL28-NITEM-MAX    PIC 9(3).                   00330100
DEBUT *//  DSA011-FC / 1992/05                                          03640103
      *--- AJOUTE EN 05/92 POUR CONTROLE PAR MODE DE REGLEMENT          03640205
      *--- (SELON CODES DEFINIS DANS LA TABLE COPAI DE GA01)            03640305
      *                                                                 03640403
               05 COMM-TL27-SOLDE           PIC X.                      03640505
               05 COMM-TL27-CODES-CUMUL.                                03640505
                  07 COMM-TL27-CUMCOD1      PIC X(3).                   03640605
                  07 COMM-TL27-CUMCOD2      PIC X(3).                   03640705
                  07 COMM-TL27-CUMCOD3      PIC X(3).                   03640805
               05 FILLER REDEFINES COMM-TL27-CODES-CUMUL.               03640905
                  07 COMM-TL27-CUMCOD       PIC X(3) OCCURS 3.          03641005
               05 COMM-TL27-DEVISES-CUMUL.                              03641205
                  07 COMM-TL27-CUMDEVA1.                                03641305
                     09 COMM-TL27-CUMDEV1      PIC  9(6)V99.            03641305
                  07 COMM-TL27-CUMDEVA2.                                03641305
                     09 COMM-TL27-CUMDEV2      PIC  9(6)V99.            03641405
               05 FILLER REDEFINES COMM-TL27-DEVISES-CUMUL.             03651005
                  07 COMM-TL27-CUMDEV       PIC  9(6)V99 OCCURS 2.      03652005
               05 COMM-TL27-MONTANTS-CUMUL.                             03641205
                  07 COMM-TL27-CUMMNTA1.                                03641305
                     09 COMM-TL27-CUMMNT1      PIC  9(6)V99.            03641305
                  07 COMM-TL27-CUMMNTA2.                                03641305
                     09 COMM-TL27-CUMMNT2      PIC  9(6)V99.            03641405
                  07 COMM-TL27-CUMMNTA3.                                03641305
                     09 COMM-TL27-CUMMNT3      PIC  9(6)V99.            03641505
               05 FILLER REDEFINES COMM-TL27-MONTANTS-CUMUL.            03651005
                  07 COMM-TL27-CUMMNT       PIC  9(6)V99 OCCURS 3.      03652005
                                                                        03652205
               05 COMM-TL27-MODPAI OCCURS 10.                           03651005
                  07 COMM-TL27-CODPAI       PIC XXX.                    03652005
                  07 COMM-TL27-MOPAI.                                   03652005
                     10 COMM-TL27-CODDEV       PIC XXX.                 03652005
                     10 COMM-TL27-CECART       PIC X.                   03652005
                                                                        03652205
FIN   *//  DSA011-FC / 1992/05                                          03640103
                                                                        03652205
               05 COMM-TL27-FILLER          PIC X(3214).                00340000
                                                                                
