      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVEF9000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEF9000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEF9000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEF9000.                                                            
      *}                                                                        
           02  EF90-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  EF90-CTRAIT                                                      
               PIC X(0005).                                                     
           02  EF90-NENVOI                                                      
               PIC X(0007).                                                     
           02  EF90-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  EF90-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  EF90-NORIGINE                                                    
               PIC X(0007).                                                     
           02  EF90-NPOSTE                                                      
               PIC X(0005).                                                     
           02  EF90-NCODIC                                                      
               PIC X(0007).                                                     
           02  EF90-CTIERS                                                      
               PIC X(0005).                                                     
           02  EF90-NENTCDE                                                     
               PIC X(0005).                                                     
           02  EF90-DENVOI                                                      
               PIC X(0008).                                                     
           02  EF90-NSERIE                                                      
               PIC X(0016).                                                     
           02  EF90-NACCORD                                                     
               PIC X(0012).                                                     
           02  EF90-DACCORD                                                     
               PIC X(0008).                                                     
           02  EF90-LNOMACCORD                                                  
               PIC X(0010).                                                     
           02  EF90-QTENV                                                       
               PIC S9(0005) COMP-3.                                             
           02  EF90-CGARANTIE                                                   
               PIC X(0005).                                                     
           02  EF90-PABASEFACT                                                  
               PIC S9(07)V9(0002) COMP-3.                                       
           02  EF90-MTPROVSAV                                                   
               PIC S9(07)V9(0002) COMP-3.                                       
           02  EF90-NSOCMVTO                                                    
               PIC X(0003).                                                     
           02  EF90-NLIEUMVTO                                                   
               PIC X(0003).                                                     
           02  EF90-NSSLIEUMVTO                                                 
               PIC X(0003).                                                     
           02  EF90-CLIEUTRTMVTO                                                
               PIC X(0005).                                                     
           02  EF90-NSOCMVTD                                                    
               PIC X(0003).                                                     
           02  EF90-NLIEUMVTD                                                   
               PIC X(0003).                                                     
           02  EF90-NSSLIEUMVTD                                                 
               PIC X(0003).                                                     
           02  EF90-CLIEUTRTMVTD                                                
               PIC X(0005).                                                     
           02  EF90-DCRE                                                        
               PIC X(0008).                                                     
           02  EF90-DMAJ                                                        
               PIC X(0008).                                                     
           02  EF90-DSAP                                                        
               PIC X(0008).                                                     
           02  EF90-DANN                                                        
               PIC X(0008).                                                     
           02  EF90-DTRT                                                        
               PIC X(0008).                                                     
           02  EF90-DCLO                                                        
               PIC X(0008).                                                     
           02  EF90-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA RVEF9000                                        
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEF9000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEF9000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-CTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-CTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NENVOI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NENVOI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NPOSTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NPOSTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-CTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-CTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-DENVOI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-DENVOI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NSERIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NSERIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-DACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-DACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-LNOMACCORD-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-LNOMACCORD-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-QTENV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-QTENV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-CGARANTIE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-CGARANTIE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-PABASEFACT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-PABASEFACT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-MTPROVSAV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-MTPROVSAV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NSOCMVTO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NSOCMVTO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NLIEUMVTO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NLIEUMVTO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NSSLIEUMVTO-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NSSLIEUMVTO-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-CLIEUTRTMVTO-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-CLIEUTRTMVTO-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NSOCMVTD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NSOCMVTD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NLIEUMVTD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NLIEUMVTD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-NSSLIEUMVTD-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-NSSLIEUMVTD-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-CLIEUTRTMVTD-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-CLIEUTRTMVTD-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-DCRE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-DCRE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-DSAP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-DSAP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-DANN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-DANN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-DTRT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-DTRT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-DCLO-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-DCLO-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF90-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF90-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
