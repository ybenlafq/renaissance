      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PRG TFL11 (TFL10 -> MENU)    TR: FL10  *            
      *          GESTION DES PARAMETRES GENERAUX LIVRAISON         *            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                                         
      *   RANSACTION FL11 : DESCRIPTION DES PROFILS                *            
      *                     D'AFFILIATION ( FAMILLE / ENTREPOTS )  *            
      *                                                                         
      *****************************************************************         
E0718 * DATE   : 19/11/2014                                           *         
      * AUTEUR : DSA004                                               *         
      * BUT    : DUPLICATION DE LA ZONE INDEXE SUR L'ENSEMBLE DES     *         
      *          FAMILLES NON DEJA RENSEIGNEES                        *         
      *****************************************************************         
          02 COMM-FL11-APPLI REDEFINES COMM-FL10-APPLI.                         
      *------------------------------ ZONE DONNEES TFL11                        
             03 COMM-FL11-DONNEES-1-TFL11.                                      
      *------------------------------ CODE FONCTION                             
                04 COMM-FL11-FONCT             PIC X(3).                        
      *------------------------------ CODE PROFIL                               
                04 COMM-FL11-CPROAFF           PIC X(5).                        
      *------------------------------ LIBELLE PROFIL                            
                04 COMM-FL11-LPROAFF           PIC X(20).                       
      *------------------------------ CODE FAMILLE                              
                04 COMM-FL11-CFAMIL            PIC X(5).                        
      *------------------------------ LIBELLE FAMILLE                           
                04 COMM-FL11-LFAMIL            PIC X(20).                       
      *------------------------------ NUMERO SEQUENCE FAMILLE                   
                04 COMM-FL11-WSEQFAM           PIC S9(5).                       
      *------------------------------ DERNIER IND-TS                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL11-PAGSUI            PIC S9(4) COMP.                  
      *--                                                                       
                04 COMM-FL11-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN IND-TS                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL11-PAGENC            PIC S9(4) COMP.                  
      *--                                                                       
                04 COMM-FL11-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                         
                04 COMM-FL11-NUMPAG            PIC 9(3).                        
      *------------------------------ NOMBRE DE PAGE TOTAL                      
                04 COMM-FL11-NBRPAG            PIC 9(3).                        
      *------------------------------ INDICATEUR FIN DE TABLE                   
                04 COMM-FL11-INDPAG            PIC 9.                           
      *------------------------------ ZONE MODIF TS                             
                04 COMM-FL11-MODIF-TS          PIC 9(1).                        
      *------------------------------ ZONE EXCEPTION                            
                04 COMM-FL11-EXCEPTION         PIC 9(1).                        
      *------------------------------ ZONE BROWSING                             
                04 COMM-FL11-BROWSING          PIC 9(1).                        
      *------------------------------ PROG XCTL                                 
                04 COMM-FL11-NOM-PROG-XCTL     PIC X(5).                        
      *------------------------------ ZONE BROWSING                             
                04 COMM-FL11-COM-PGMPRC        PIC X(5).                        
      *------------------------------ OK MAJ SUR RTGQ05 => VALID = '1'          
                04 COMM-FL11-VALID-RTGQ05   PIC  9.                             
      *------------------------------ OK MAJ SUR RTGA01 => VALID = '1'          
                04 COMM-FL11-VALID-RTGA01   PIC  9.                             
      *------------------------------    MAJ SUR RTGA01 => VALID = 'M'          
                04 COMM-FL11-CMAJ-RTGA01    PIC  X.                             
      *------------------------------ TABLE N� TS PR PAGINATION                 
                04 COMM-FL11-TABTS          OCCURS 100.                         
      *------------------------------ 1ERS IND TS DES PAGES <=> FAMILLE         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               06 COMM-FL11-NUMTS          PIC S9(4) COMP.               
      *--                                                                       
                      06 COMM-FL11-NUMTS          PIC S9(4) COMP-5.             
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS LE DEPOT              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL11-IND-NDEPOT     PIC S9(4) COMP.                     
      *--                                                                       
                04 COMM-FL11-IND-NDEPOT     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND DE SVG DE TS                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL11-TS-ENCOURS     PIC S9(4) COMP.                     
      *--                                                                       
                04 COMM-FL11-TS-ENCOURS     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL11-IND-RECH-TS    PIC S9(4) COMP.                     
      *--                                                                       
                04 COMM-FL11-IND-RECH-TS    PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND TS MAX                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL11-IND-TS-MAX     PIC S9(4) COMP.                     
      *--                                                                       
                04 COMM-FL11-IND-TS-MAX     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ ZONE DONNEES 2 TFL11                      
             03 COMM-FL11-DONNEES-2-TFL11.                                      
      *------------------------------ CODES D'AFFILIATION PAR DEFAUT            
E0718 *         04 COMM-FL11-ENTAFF-DF      OCCURS 5.                           
E0718           04 COMM-FL11-ENTAFF-DF      OCCURS 100.                         
      *------------------------------ CODE SOCIETE D'AFFILIATION DEFAUT         
                   05 COMM-FL11-CSOCAFF-DF  PIC X(03).                          
      *------------------------------ CODE ENTREPOT D'AFFILIATION DEFAUT        
                   05 COMM-FL11-CDEPAFF-DF  PIC X(03).                          
      *------------------------------ ZONE FAMILLES PAGE                        
                04 COMM-FL11-TABFAM.                                            
      *------------------------------ ZONE LIGNES FAMILLES                      
                   05 COMM-FL11-LIGFAM  OCCURS 15.                              
      *------------------------------ CODE EXCEPTION                            
                      06 COMM-FL11-CEXC           PIC X(1).                     
      *------------------------------ CODE FAMILLE                              
                      06 COMM-FL11-CFAM           PIC X(5).                     
      *------------------------------ LIBELLE FAMILLE                           
                      06 COMM-FL11-LFAM           PIC X(20).                    
      *------------------------------ CODES D'AFFILIATION                       
                      06 COMM-FL11-ENTAFF         OCCURS 5.                     
      *------------------------------ CODE SOCIETE D'AFFILIATION                
                         07 COMM-FL11-CSOCAFF     PIC X(03).                    
      *------------------------------ CODE ENTREPOT D'AFFILIATION               
                         07 COMM-FL11-CDEPAFF     PIC X(03).                    
      *------------------------------ ZONE DONNEES 3 TFL11                      
             03 COMM-FL11-DONNEES-3-TFL11.                                      
      *------------------------------ ZONES SWAP                                
                   05 COMM-FL11-ATTR           PIC X OCCURS 500.                
      *------------------------------ ZONE LIBRE                                
E0718 *      03 COMM-FL11-LIBRE          PIC X(2061).                           
E0718        03 COMM-FL11-LIBRE          PIC X(1491).                           
      *****************************************************************         
                                                                                
