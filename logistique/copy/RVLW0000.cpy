      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLW0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLW0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW0000.                                                            
           02  LW00-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  LW00-NDEPOT                                                      
               PIC X(0003).                                                     
           02  LW00-NCODIC                                                      
               PIC X(0007).                                                     
           02  LW00-DMAJ                                                        
               PIC X(0008).                                                     
           02  LW00-CFAM                                                        
               PIC X(0005).                                                     
           02  LW00-CFAMSTK                                                     
               PIC X(0005).                                                     
           02  LW00-CMARQ                                                       
               PIC X(0005).                                                     
           02  LW00-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  LW00-CAPPRO                                                      
               PIC X(0005).                                                     
           02  LW00-LSTATCOMP                                                   
               PIC X(0003).                                                     
           02  LW00-WDACEM                                                      
               PIC X(0001).                                                     
           02  LW00-CQUOTA                                                      
               PIC X(0005).                                                     
           02  LW00-D1RECEPT                                                    
               PIC X(0008).                                                     
           02  LW00-NEAN                                                        
               PIC X(0013).                                                     
           02  LW00-NBEAN                                                       
               PIC S9(5) COMP-3.                                                
           02  LW00-NENTCDE                                                     
               PIC X(0005).                                                     
           02  LW00-CMODSTOCK                                                   
               PIC X(0001).                                                     
           02  LW00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLW0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-CFAMSTK-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-CFAMSTK-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-CAPPRO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-CAPPRO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-LSTATCOMP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-LSTATCOMP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-WDACEM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-WDACEM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-CQUOTA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-CQUOTA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-D1RECEPT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-D1RECEPT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-NEAN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-NEAN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-NBEAN-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-NBEAN-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
