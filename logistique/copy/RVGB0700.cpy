      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVGB0700                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB0700                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGB0700.                                                    00000090
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGB0700.                                                            
      *}                                                                        
           02  GB07-NMUTATION      PIC X(0007).                         00000100
           02  GB07-DMUTATION      PIC X(0008).                         00000120
           02  GB07-HDACEM         PIC X(0004).                         00000160
           02  GB07-FILIERE        PIC X(0002).                         00000180
           02  GB07-WMULTI         PIC X(0001).                         00000200
           02  GB07-QMUTEE         PIC S9(0005) COMP-3.                 00000220
           02  GB07-PGMMAJ         PIC X(0006).                         00000240
           02  GB07-DSYST          PIC S9(13) COMP-3.                   00000260
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000600
                                                                                
