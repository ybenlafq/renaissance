      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVIE9000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIE9000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVIE9000.                                                            
           02  IE90-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  IE90-NDEPOT                                                      
               PIC X(0003).                                                     
           02  IE90-CREGROUP                                                    
               PIC X(0001).                                                     
           02  IE90-WTOPHS                                                      
               PIC X(0001).                                                     
           02  IE90-WTOPPRET                                                    
               PIC X(0001).                                                     
           02  IE90-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVIE9000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVIE9000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE90-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE90-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE90-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE90-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE90-CREGROUP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE90-CREGROUP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE90-WTOPHS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE90-WTOPHS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE90-WTOPPRET-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE90-WTOPPRET-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE90-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE90-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
