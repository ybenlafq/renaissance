      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PRG TMU54                     TR: GA00              
      *        PG: TMU54 CREATION MAJ DES TABLES GENERALISEES                   
      **************************************************************            
      * ZONES RESERVEES APPLICATIVES ----------------------- 3724               
      * PROGRAMME TMU54 : CREATION MAJ DES TABLES GENERALISEES                  
      **************************************************************            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-MU54-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-MU54-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *------------------------------ CODE FONCTION                             
      *      03 COMM-MU54-FONCT          PIC X(3).                              
      *------------------------------ SELECTION CLE                             
             03 COMM-MU54-SELECT         PIC X(15).                             
             03 COMM-CICS-APPLID     PIC X(8).                                  
             03 COMM-CICS-NETNAM     PIC X(8).                                  
             03 COMM-DATE-JJ-MM-SSAA PIC X(8).                                  
      *------------------------------ LIBELLE FONCTION                          
      *      03 COMM-MU54-LFONCT         PIC X(14).                             
      *----- TABLE 71                                                           
      *      03  COMM-MU54-T71.                                                 
      *       04  COMM-MU54-CTABLE                                              
      *         PIC X(0006).                                                    
      *     04  COMM-MU54-CSTABLE                                               
      *         PIC X(0005).                                                    
      *     04  COMM-MU54-LTABLE                                                
      *         PIC X(0030).                                                    
      *     04  COMM-MU54-CRESP                                                 
      *         PIC X(0003).                                                    
      *     04  COMM-MU54-DDEBUT                                                
      *         PIC X(0008).                                                    
      *     04  COMM-MU54-DFIN                                                  
      *         PIC X(0008).                                                    
      *     04  COMM-MU54-DMAJ                                                  
      *         PIC X(0008).                                                    
      *     04  COMM-MU54-6-CHAMP OCCURS 6 TIMES.                               
      *      05  COMM-MU54-CHAMP PIC  X(008).                                   
      *      05  COMM-MU54-LCHAMP PIC X(030).                                   
      *      05  COMM-MU54-QCHAMP PIC S9(003) COMP-3.                           
      *      05  COMM-MU54-WCHAMP PIC X(001).                                   
      *      05  COMM-MU54-QDEC  PIC S9(003) COMP-3.                            
      *      05  COMM-MU54-WCTLDIR PIC X(0001).                                 
      *      05  COMM-MU54-WCTLIND PIC X(0001).                                 
      *      05  COMM-MU54-CCTLIND1 PIC X(0003).                                
      *      05  COMM-MU54-CCTLIND2 PIC X(0003).                                
      *      05  COMM-MU54-CCTLIND3 PIC X(0003).                                
      *      05  COMM-MU54-CCTLIND4 PIC X(0003).                                
      *      05  COMM-MU54-CCTLIND5 PIC X(0003).                                
      *      05  COMM-MU54-CTABASS PIC X(0006).                                 
      *      05  COMM-MU54-CSTABASS PIC X(0005).                                
      *      05  COMM-MU54-WPOSCLE PIC S9(001) COMP-3.                          
      *-----                                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 COMM-MU54-PAGE-COURANTE  PIC S9(004) COMP.                        
      *--                                                                       
           03 COMM-MU54-PAGE-COURANTE  PIC S9(004) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 COMM-MU54-NBRE-PAGE      PIC S9(004) COMP.                        
      *--                                                                       
           03 COMM-MU54-NBRE-PAGE      PIC S9(004) COMP-5.                      
      *}                                                                        
      *----- 168 C ETENDU                                                       
      *    03 COMM-MU54-MEMO-POS-ATTR.                                          
      *      05  FILLER OCCURS 14 TIMES.                                        
      *       07 FILLER OCCURS  6 TIMES.                                        
      *          09 COMM-MU54-POS-ATTR PIC 9(0002).                             
      *-----                                                                    
      *      03  FILLER OCCURS 100 TIMES.                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MU54-NBRE-LIGNE     PIC S9(004) COMP.                     
      *--                                                                       
              03 COMM-MU54-NBRE-LIGNE     PIC S9(004) COMP-5.                   
      *}                                                                        
              03 COMM-PF6 PIC X.                                                
      *-----                                                                    
      *      03 COMM-MU54-LIBRE1         PIC X(2820).                           
      *                                                                         
                                                                                
