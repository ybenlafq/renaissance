      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------------------------*               
      *    FICHIER CSV EXTRACTION DE L'ETAT JHE601              *               
      *    LONGUEUR : 243                                       *               
      *---------------------------------------------------------*               
       01  HE60-ENREG.                                                          
           05  HE60-DONNEES.                                             000008 
               10  HE60-NOMETAT          PIC X(06).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-DATED            PIC X(08).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-DATEF            PIC X(08).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-CENTRE           PIC X(05).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-NDEM             PIC 9(07).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-CTIERS           PIC X(05).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-FAM              PIC X(05).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-CODIC            PIC X(07).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-MARQ             PIC X(05).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-REF              PIC X(19).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-NDEP             PIC X(03).                      000008 
               10  HE60-NHS              PIC X(07).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-NTRAI            PIC X(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-DVENTE.                                                 
                   15  HE60-DVEN-SSAA.                                          
                       20  HE60-DVEN-SS  PIC X(02).                             
                       20  HE60-DVEN-AA  PIC X(02).                             
                   15  HE60-DVEN-MOIS    PIC X(02).                             
                   15  HE60-DVEN-JOUR    PIC X(02).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-DDEP.                                                   
                   15  HE60-DDEP-SSAA.                                          
                       20  HE60-DDEP-SS  PIC X(02).                             
                       20  HE60-DDEP-AA  PIC X(02).                             
                   15  HE60-DDEP-MOIS    PIC X(02).                             
                   15  HE60-DDEP-JOUR    PIC X(02).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-SERIE            PIC X(16).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-NACCORD          PIC X(12).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-LPAN             PIC X(20).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-PVTTC            PIC -9(7)V99.                          
               10  HE60-PVTTC-R          REDEFINES  HE60-PVTTC                  
                                         PIC X(10).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-ORI              PIC X(03).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-PRMP             PIC -9(7)V9(6).                        
               10  HE60-PRMP-R           REDEFINES  HE60-PRMP                   
                                         PIC X(14).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-ECOP             PIC -9(5)V99.                          
               10  HE60-ECOP-R           REDEFINES  HE60-ECOP                   
                                         PIC X(08).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  HE60-PDOSNASC         PIC X(03).                             
               10  HE60-SEP              PIC X(01)  VALUE '/'.                  
               10  HE60-DOSNASC          PIC X(09).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  HE60-NEAN             PIC X(13).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
      *---  VARIABLE A METTRE A BLANC LORS DE L ENVOI PAR MAIL                  
      *---  SERT AU PROGRAMME DE SUPPRESSION DES DOUBLONS                       
               10  HE60-NSEQ             PIC X(05).                             
      *                                                                         
      *---------------------------------------------------------*               
                                                                                
