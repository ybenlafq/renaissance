      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGL05                             *            
      *       TR : GL00  GESTION DES LIVRAISON                     *            
      *       PG : TGL05 CONSULTATION D'UNE LIVRAISON              *            
      **************************************************************            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-GL05-LONG-COMMAREA PIC S9(4) COMP VALUE +116.                    
      *--                                                                       
       01 COMM-GL05-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +116.                  
      *}                                                                        
       01 COMM-GL05-APPLI.                                                      
      *                                                                         
          03 COMM-GL05-NSOCIETE       PIC X(3).                                 
      *-------                                                                  
          03 COMM-GL05-NDEPOT         PIC X(3).                                 
      *-------                                                                  
          03 COMM-GL05-CREC           PIC X(5).                                 
      *-------                                                                  
          03 COMM-GL05-LREC           PIC X(20).                                
      *-------                                                                  
          03 COMM-GL05-JOURNEE        PIC X(8).                                 
      *-------                                                                  
          03 COMM-GL05-SEMAIN         PIC X(6).                                 
      *-------                                                                  
          03 COMM-GL05-MSG            PIC X(70).                                
      *---------------------------------------CODE RETOUR                       
          03 COMM-GL05-CODRET    PIC    X VALUE '0'.                            
             88 COMM-GL05-OK     VALUE '0'.                                     
             88 COMM-GL05-KO     VALUE '1'.                                     
      *                                                                         
                                                                                
