      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *                COMMAREA PROGRAMME TFL12                    *            
      **************************************************************            
      *                                                                         
      * COM-RM40-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      *                                                                         
           02  COMM-FL12-APPLI REDEFINES COMM-FL10-APPLI.                       
               05 COMM-FL12-CREATE          PIC X.                              
               05 COMM-FL12-SUPP            PIC X.                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 COMM-FL12-PAGE            PIC S9(4) COMP.                     
      *--                                                                       
               05 COMM-FL12-PAGE            PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 COMM-FL12-PAGE-MAX        PIC S9(4) COMP.                     
      *--                                                                       
               05 COMM-FL12-PAGE-MAX        PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 COMM-FL12-ITEMAX          PIC S9(4) COMP.                     
      *--                                                                       
               05 COMM-FL12-ITEMAX          PIC S9(4) COMP-5.                   
      *}                                                                        
               05 COMM-FL12-CPROAFF         PIC X(05).                          
               05 COMM-FL12-NSOCIETE        PIC X(03).                          
               05 COMM-FL12-NLIEU           PIC X(03).                          
               05 COMM-FL12-CTYPTRAIT       PIC X(05).                          
               05 COMM-FL12-FILLER          PIC X(3700).                        
                                                                                
