      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *  PROJET     : QUOTAS DECENTRALISES                             *        
      *  TITRE      : TS DE MISE A JOUR DE SOUS-TABLE                  *        
      *  APPELER PAR: TZQ11 - CODIFICATION DES ZONES ELEMENTAIRES      *        
      *             : TZQ13 - CODIFICATION DES ZONES DE LIVRAISON      *        
      *             : TZQ14 - LIENS ZONES LIVRAISON / ZONES ELEMENTAIRE*        
      *             : TZQ15 - CODIFICATION DES PROFILS DE LIVRAISON    *        
      *             : TZQ16 - REPARTITION DES QUOTAS DANS UN PROFIL    *        
      *                                                                *        
      *  CONTENU    : 1 ITEM PAR PAGE                                  *00000050
      *               13 LIGNES PAR ITEM                               *00000051
      *              (1 LIGNE CORRESPOND A UNE LIGNE DE L'ECRAN)       *        
      *  LONGUEUR   : 250 C * 13 LG = 3250 C                           *        
      ******************************************************************        
      *                                                                         
       01  TS-DONNEES.                                                          
           05 TS-LIGNE               OCCURS 13.                                 
              10 TS-ACTION                 PIC X(01).                           
                 88 TS-INACTION                VALUE ' '.                       
                 88 TS-CREATION                VALUE 'C'.                       
                 88 TS-MODIFICATION            VALUE 'M'.                       
                 88 TS-SUPPRESSION             VALUE 'S'.                       
              10 TS-EXISTE                 PIC X(01).                           
                 88 TS-INEXISTENCE             VALUE ' '.                       
                 88 TS-EXISTENCE               VALUE 'E'.                       
              10 TS-RVGA0100-ECRAN.                                             
                 15 TS-CTABLEG1-ECRAN      PIC X(05).                           
                 15 TS-RVGA01XX-ECRAN      PIC X(95).                           
              10 TS-RVGA0100-FICHIER.                                           
                 15 TS-CTABLEG1-FICHIER    PIC X(05).                           
                 15 TS-RVGA01XX-FICHIER    PIC X(95).                           
              10 TS-FILLER                 PIC X(48).                           
                                                                                
