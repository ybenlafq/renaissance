      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE DEPCL COULEUR DEPOT                    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVDEPCL.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVDEPCL.                                                             
      *}                                                                        
           05  DEPCL-CTABLEG2    PIC X(15).                                     
           05  DEPCL-CTABLEG2-REDEF REDEFINES DEPCL-CTABLEG2.                   
               10  DEPCL-NSOCIETE        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  DEPCL-NSOCIETE-N     REDEFINES DEPCL-NSOCIETE                
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  DEPCL-NDEPOT          PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  DEPCL-NDEPOT-N       REDEFINES DEPCL-NDEPOT                  
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  DEPCL-WTABLEG     PIC X(80).                                     
           05  DEPCL-WTABLEG-REDEF  REDEFINES DEPCL-WTABLEG.                    
               10  DEPCL-CCOLOR          PIC X(01).                             
               10  DEPCL-CBASE           PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVDEPCL-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVDEPCL-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05  DEPCL-CTABLEG2-F  PIC S9(4)  COMP.                                
      *--                                                                       
          05  DEPCL-CTABLEG2-F  PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05  DEPCL-WTABLEG-F   PIC S9(4)  COMP.                                
      *                                                                         
      *--                                                                       
          05  DEPCL-WTABLEG-F   PIC S9(4) COMP-5.                               
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
