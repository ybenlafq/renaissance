      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
000010*                                                                 00010000
000020**************************************************************    00020000
000030*        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
000040**************************************************************    00040000
000050*                                                                 00050000
000060* XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00060000
000070*      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00070000
000080*      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00080000
000090*      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00090000
000091*                                                                 00100000
000092* COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00110000
000093* COMPRENANT :                                                    00120000
000094* 1 - LES ZONES RESERVEES A AIDA                                  00130000
000095* 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00140000
000096* 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00150000
000097* 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00160000
000098* 5 - LES ZONES RESERVEES APPLICATIVES                            00170000
000099*                                                                 00180000
000100* COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00190000
000110* PAR AIDA                                                        00200000
000120*                                                                 00210000
000130*-------------------------------------------------------------    00220000
000160*                                                                 00250000
000170 01  Z-COMMAREA.                                                  00260000
000180*                                                                 00270000
000190* ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
000191     02 FILLER-COM-AIDA      PIC X(100).                          00290000
000192*                                                                 00300000
000193* ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
000194     02 COMM-CICS-APPLID     PIC X(8).                            00320000
000195     02 COMM-CICS-NETNAM     PIC X(8).                            00330000
000196     02 COMM-CICS-TRANSA     PIC X(4).                            00340000
000197*                                                                 00350000
000198* ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
000199     02 COMM-DATE-SIECLE     PIC XX.                              00370000
000200     02 COMM-DATE-ANNEE      PIC XX.                              00380000
000210     02 COMM-DATE-MOIS       PIC XX.                              00390000
000220     02 COMM-DATE-JOUR       PIC 99.                              00400000
000230*   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
000240     02 COMM-DATE-QNTA       PIC 999.                             00420000
000250     02 COMM-DATE-QNT0       PIC 99999.                           00430000
000260*   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
000270     02 COMM-DATE-BISX       PIC 9.                               00450000
000280*    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
000290     02 COMM-DATE-JSM        PIC 9.                               00470000
000291*   LIBELLES DU JOUR COURT - LONG                                 00480000
000292     02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
000293     02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
000294*   LIBELLES DU MOIS COURT - LONG                                 00510000
000295     02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
000296     02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
000297*   DIFFERENTES FORMES DE DATE                                    00540000
000298     02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
000299     02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
000300     02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
000310     02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
000320     02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
000330     02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
000340*   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
000350     02 COMM-DATE-WEEK.                                           00620000
000360        05 COMM-DATE-SEMSS   PIC 99.                              00630000
000370        05 COMM-DATE-SEMAA   PIC 99.                              00640000
000380        05 COMM-DATE-SEMNU   PIC 99.                              00650000
000390*                                                                 00660000
000391     02 COMM-DATE-FILLER     PIC X(08).                           00670000
000392*                                                                 00680000
000393* ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000394*    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
000395     02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
000396*                                                                 00720000
000397* ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00730000
                                                                                
