      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVSU9000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSU9000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSU9000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSU9000.                                                            
      *}                                                                        
           02  SU90-NCODIC                                                      
               PIC X(0007).                                                     
           02  SU90-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  SU90-NLIEU                                                       
               PIC X(0003).                                                     
           02  SU90-QV8SR                                                       
               PIC S9(7) COMP-3.                                                
           02  SU90-QSTOCKOBJR                                                  
               PIC S9(7) COMP-3.                                                
           02  SU90-QV8SD                                                       
               PIC S9(7) COMP-3.                                                
           02  SU90-QSTOCKOBJD                                                  
               PIC S9(7) COMP-3.                                                
           02  SU90-QV8SC                                                       
               PIC S9(7) COMP-3.                                                
           02  SU90-QSTOCKOBJC                                                  
               PIC S9(7) COMP-3.                                                
           02  SU90-QV8SP                                                       
               PIC S9(7) COMP-3.                                                
           02  SU90-QSTOCKOBJP                                                  
               PIC S9(7) COMP-3.                                                
           02  SU90-QSO                                                         
               PIC S9(7) COMP-3.                                                
           02  SU90-QSA                                                         
               PIC S9(7) COMP-3.                                                
           02  SU90-QFREQUENCE                                                  
               PIC S9(1)V9(0002) COMP-3.                                        
           02  SU90-QINDICER                                                    
               PIC S9(1)V9(0006) COMP-3.                                        
           02  SU90-QINDICEP                                                    
               PIC S9(1)V9(0006) COMP-3.                                        
           02  SU90-QPOIDSMAG                                                   
               PIC S9(1)V9(0006) COMP-3.                                        
           02  SU90-QTE                                                         
               PIC S9(1)V9(0006) COMP-3.                                        
           02  SU90-NLOI                                                        
               PIC X(0002).                                                     
           02  SU90-NLOIP                                                       
               PIC X(0002).                                                     
           02  SU90-DMAJ                                                        
               PIC X(0008).                                                     
           02  SU90-QSOS                                                        
               PIC S9(7) COMP-3.                                                
           02  SU90-QSAS                                                        
               PIC S9(7) COMP-3.                                                
           02  SU90-DSAISIE                                                     
               PIC X(0008).                                                     
           02  SU90-QSOAS                                                       
               PIC S9(7) COMP-3.                                                
           02  SU90-QSAAS                                                       
               PIC S9(7) COMP-3.                                                
           02  SU90-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSU9000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSU9000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSU9000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QV8SR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QV8SR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QSTOCKOBJR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QSTOCKOBJR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QV8SD-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QV8SD-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QSTOCKOBJD-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QSTOCKOBJD-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QV8SC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QV8SC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QSTOCKOBJC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QSTOCKOBJC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QV8SP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QV8SP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QSTOCKOBJP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QSTOCKOBJP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QSO-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QSO-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QSA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QSA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QFREQUENCE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QFREQUENCE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QINDICER-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QINDICER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QINDICEP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QINDICEP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QPOIDSMAG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QPOIDSMAG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QTE-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QTE-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-NLOI-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-NLOI-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-NLOIP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-NLOIP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QSOS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QSOS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QSAS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QSAS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QSOAS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QSOAS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-QSAAS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-QSAAS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU90-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU90-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
