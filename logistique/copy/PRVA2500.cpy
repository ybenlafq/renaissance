      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE VA2500                       
      ******************************************************************        
      *                                                                         
       CLEF-VA2500             SECTION.                                         
      *                                                                         
           MOVE 'RVVA2500          '       TO   TABLE-NAME.                     
           MOVE 'VA2500'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-VA2500. EXIT.                                                   
                EJECT                                                           
                                                                                
