      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ GENERATION  LOCAL                                    
      *                                                                         
      *                                                                         
      *****************************************************************         
      *  POUR MGL20 AVEC PUT                                                    
      *                                                                         
           10  WS-MESSAGE REDEFINES COMM-MQ21-MESSAGE.                          
            15  MES-ENTETE.                                                     
              20   MES-TYPE      PIC    X(3).                                   
              20   MES-NSOCMSG   PIC    X(3).                                   
              20   MES-NLIEUMSG  PIC    X(3).                                   
              20   MES-NSOCDST   PIC    X(3).                                   
              20   MES-NLIEUDST  PIC    X(3).                                   
              20   MES-NORD      PIC    9(8).                                   
              20   MES-LPROG     PIC    X(10).                                  
              20   MES-DJOUR     PIC    X(8).                                   
              20   MES-WSID      PIC    X(10).                                  
              20   MES-USER      PIC    X(10).                                  
              20   MES-CHRONO    PIC    9(7).                                   
              20   MES-NBRMSG    PIC    9(7).                                   
              20   MES-NBRENR    PIC    9(5).                                   
              20   MES-TAILLE    PIC    9(5).                                   
              20   MES-FILLER    PIC    X(20).                                  
      *---  43860 A DEFINIR SELON LA COMMAREA COMMGL20                          
            15  MES-ENR          PIC    X(43860).                               
      *---                                                                      
                                                                                
