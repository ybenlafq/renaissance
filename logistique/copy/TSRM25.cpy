      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS-RM25 SPECIFIQUE TRM25                                     *  00020007
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-RM25-LONG            PIC S9(3) COMP-3 VALUE 91.           00060009
       01  TS-RM25-DONNEES.                                             00070007
              05 TS-RM25-NCODIC    PIC X(07).                           00080008
              05 TS-RM25-LREF      PIC X(20).                           00090008
              05 TS-RM25-CMARQ     PIC X(05).                           00100008
              05 TS-RM25-CEXPO     PIC X(01).                           00110008
              05 TS-RM25-WASSORT   PIC X(01).                           00120008
              05 TS-RM25-WASSOSTD  PIC X(01).                           00130008
              05 TS-RM25-QEXPO     PIC X(03).                           00140008
              05 TS-RM25-QLS       PIC X(03).                           00150008
              05 TS-RM25-QV8SP     PIC X(05).                           00160008
              05 TS-RM25-QV8SR     PIC X(05).                           00170008
              05 TS-RM25-QSO       PIC X(03).                           00180008
              05 TS-RM25-QSM       PIC X(03).                           00190008
              05 TS-RM25-QSOP      PIC X(03).                           00200008
              05 TS-RM25-QSMP      PIC X(03).                           00210008
              05 TS-RM25-MODIF     PIC X(01).                           00220008
              05 TS-RM25-QSO-SAVE  PIC X(03).                           00230008
              05 TS-RM25-QSM-SAVE  PIC X(03).                           00240008
              05 TS-RM25-QSOTROUVE PIC X.                               00250007
              05 TS-RM25-QSATROUVE PIC X.                               00260007
              05 TS-RM25-QSOMIN    PIC X(03).                           00270008
              05 TS-RM25-QSMMIN    PIC X(03).                           00280008
              05 TS-RM25-QSOMAX    PIC X(03).                           00290008
              05 TS-RM25-QSMMAX    PIC X(03).                           00300008
              05 TS-RM25-W80       PIC X.                               00310008
              05 TS-RM25-FLAG      PIC X.                               00320009
                                                                                
