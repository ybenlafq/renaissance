      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : PARAMETRAGE DES RETOURS INVENDUS                       *         
      *****************************************************************         
      *                                                               *         
       01  TS-SR10.                                                             
           02 TS-SR10-GESTION.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TS-SR10-POS                 PIC S9(4) COMP.                   
      *--                                                                       
               05 TS-SR10-POS                 PIC S9(4) COMP-5.                 
      *}                                                                        
      *--------------------------------  STRUCTURE TS                           
           02 TS-SR10-DONNEES.                                                  
             03 TS-SR10-ENR.                                                    
               05 TS-SR10-ACT                 PIC X(1).                         
               05 TS-SR10-NLIEU               PIC X(3).                         
               05 TS-SR10-NCODIC              PIC X(7).                         
               05 TS-SR10-DDEBUT-SL17         PIC X(8).                         
               05 TS-SR10-DFIN-SL17           PIC X(8).                         
               05 TS-SR10-DATEJJDEB           PIC X(2).                         
               05 TS-SR10-DATEMMDEB           PIC X(2).                         
               05 TS-SR10-DATEAADEB           PIC X(4).                         
               05 TS-SR10-DATEJJFIN           PIC X(2).                         
               05 TS-SR10-DATEMMFIN           PIC X(2).                         
               05 TS-SR10-DATEAAFIN           PIC X(4).                         
               05 TS-SR10-CLIEUGHE            PIC X(5).                         
               05 TS-SR10-CTRAIT              PIC X(5).                         
               05 TS-SR10-CTYPHS              PIC X(5).                         
               05 TS-SR10-LIBRE               PIC X(10).                        
                                                                                
