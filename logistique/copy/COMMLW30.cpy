      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *-------------------------------------------------------------    00000040
      *-------------------------------------------------------------    00000040
      *       PROJET WMS - LM7 - ASIS                                   00000040
      *       COPIE POUR LE MODULE MLW30                                00000040
      *       MODULE DE TRANSFERT MQ DES COMMANDES                      00000070
      *  LE 08/02/2012                                                  00000100
      *  ARNAUD ANGER - DSA033                                          00000110
      *-------------------------------------------------------------    00000130
      *-------------------------------------------------------------    00000150
       01 COMM-LW30-APPLI.                                              00260000
          02 COMM-LW30-ENTREE.                                          00260000
             03 COMM-LW30-NPROG       PIC X(08).                        00320000
             03 COMM-LW30-APPLID      PIC X(08).                        00330000
             03 COMM-LW30-TERMID      PIC X(04).                        00340000
             03 COMM-LW30-USERID      PIC X(08).                        00340000
             03 COMM-LW30-DATEJOUR    PIC X(08).                                
             03 COMM-LW30-NSOCDEPOT   PIC X(03).                                
             03 COMM-LW30-NDEPOT      PIC X(03).                                
             03 COMM-LW30-NCDE        PIC X(07).                                
             03 COMM-LW30-CTRAITEMENT PIC X(07).                                
         02 COMM-MLW30-SORTIE.                                          00260000
             03 COMM-LW30-CRETOUR     PIC X(02).                        00340000
             03 COMM-LW30-LRETOUR     PIC X(60).                                
AA1212*  02 FILLER                    PIC X(500).                       00260000
AA1212   02 COMM-MLW30-DIVERS.                                          00260000
AA1212       03 COMM-LW30-NB-MSG-MQ   PIC 9(05).                        00340000
AA1212       03 FILLER                PIC X(495).                       00340000
                                                                                
