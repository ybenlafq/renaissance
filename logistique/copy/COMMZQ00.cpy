      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : QUOTAS DECENTRALISES                             *        
      *  TRANSACTION: ZQ00                                             *        
      *  TITRE      : COMMAREA DE LA TRANSACTION ZQ00                  *        
      *  LONGUEUR   : 4096                                             *        
      *                                                                *        
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :            *        
      * 1 - LES ZONES RESERVEES A AIDA                           100 C *        
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS             20 C *        
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT          100 C *        
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP               152 C *        
      * 5 - LES ZONES RESERVEES APPLICATIVES                    3724 C *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
       01  Z-COMMAREA.                                                          
      **** ZONES RESERVEES A AIDA ********************************** 100        
           02 FILLER-COM-AIDA           PIC X(100).                             
      *                                                                         
      **** ZONES RESERVEES EN PROVENANCE DE CICS ******************* 020        
           02 COMM-CICS-APPLID          PIC X(08).                              
           02 COMM-CICS-NETNAM          PIC X(08).                              
           02 COMM-CICS-TRANSA          PIC X(04).                              
      *                                                                         
      **** ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ***** 100        
           02 COMM-DATE-SIECLE          PIC X(02).                              
           02 COMM-DATE-ANNEE           PIC X(02).                              
           02 COMM-DATE-MOIS            PIC X(02).                              
           02 COMM-DATE-JOUR            PIC 9(02).                              
           02 COMM-DATE-QNTA            PIC 9(03).                              
           02 COMM-DATE-QNT0            PIC 9(05).                              
           02 COMM-DATE-BISX            PIC 9(01).                              
           02 COMM-DATE-JSM             PIC 9(01).                              
           02 COMM-DATE-JSM-LC          PIC X(03).                              
           02 COMM-DATE-JSM-LL          PIC X(08).                              
           02 COMM-DATE-MOIS-LC         PIC X(03).                              
           02 COMM-DATE-MOIS-LL         PIC X(09).                              
           02 COMM-DATE-SSAAMMJJ        PIC X(08).                              
           02 COMM-DATE-AAMMJJ          PIC X(06).                              
           02 COMM-DATE-JJMMSSAA        PIC X(08).                              
           02 COMM-DATE-JJMMAA          PIC X(06).                              
           02 COMM-DATE-JJ-MM-AA        PIC X(08).                              
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                              
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS        PIC 9(02).                              
              05 COMM-DATE-SEMAA        PIC 9(02).                              
              05 COMM-DATE-SEMNU        PIC 9(02).                              
           02 COMM-DATE-FILLER          PIC X(07).                              
      *                                                                         
      **** ZONES RESERVEES TRAITEMENT DU SWAP ********************** 152        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.                
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).                    
      *                                                                         
      *                                                                         
      **** ZONES APPLICATIVES ZQ00 ******************************** 3724        
           02 COMM-ZQ00-APPLI.                                                  
      ******* ZONES APPLICATIVES ZQ00 : NIVEAU 1 (GESTION PLTFRM) ** 724        
      *------ ZONES RESERVEES AUX COMMAREAS DE TGA01 ET DE TGQ04 -- 1000        
              05 COMM-ZQ00-AUTRE-APPLI     PIC X(1000).                         
      ******* ZONES APPLICATIVES MU68 - OPTION 3 - ENCOURS CLIENT *** 58        
              05 COMM-MU68-APPLI REDEFINES COMM-ZQ00-AUTRE-APPLI.               
                 10 COMM-MU10-NSOCIETE        PIC X(03).                00742002
                 10 COMM-MU10-NLIEU           PIC X(03).                00743002
                 10 COMM-MU10-LLIEU           PIC X(20).                00744002
                 10 COMM-MU10-NSOCENTR        PIC X(03).                00745002
                 10 COMM-MU10-NDEPOT          PIC X(03).                00748002
                 10 COMM-MU68-PAGE            PIC 9(04).                        
                 10 COMM-MU68-NBPAGES         PIC 9(04).                        
                 10 COMM-MU68-VALIDE-TRT      PIC X(01).                        
                 10 COMM-MU68-VALIDE-SUP      PIC 9(01).                        
                 10 COMM-MU68-TRACE-TRAIT     PIC X(03).                        
                 10 COMM-MU68-EXNSOCIETE      PIC X(03).                        
                 10 COMM-MU68-EXNLIEU         PIC X(03).                        
                 10 COMM-MU68-EXNSOCDEPLIV    PIC X(03).                        
                 10 COMM-MU68-EXNLIEUDEPLIV   PIC X(03).                        
      *--------- INDICATEUR SI TS TROP LONGUE                                   
                 10 COMM-MU68-TSLONG          PIC 9(01).                        
                    88 TS-PAS-TROP-LONGUE                       VALUE 0.        
                    88 TS-TROP-LONGUE                           VALUE 1.        
      *                                                                         
      *------ LIEU DE SAISIE (LIE A L'IDENTIFIANT) --------------------*        
              05 COMM-ZQ00-LIEU-USER.                                           
                 10 COMM-ZQ00-LIEU.                                             
                    15 COMM-ZQ00-NSOCIETE        PIC X(03).                     
                    15 COMM-ZQ00-NLIEU           PIC X(03).                     
                 10 COMM-ZQ00-CTYPLIEU        PIC X(01).                        
      *{ remove-comma-in-dde 1.5                                                
      *             88 COMM-ZQ00-CTYPLIEU-PLTFRM         VALUE '2', '3'.        
      *--                                                                       
                    88 COMM-ZQ00-CTYPLIEU-PLTFRM         VALUE '2'  '3'.        
      *}                                                                        
                 10 COMM-ZQ00-LLIEU           PIC X(20).                        
STKAV *       05 COMM-ZQ00-FILLER          PIC X(197).                          
                 10 COMM-ZQ00-LIEUSTKAV.                                        
                    15 COMM-ZQ00-NATLIEU         PIC X(01).                     
                       88 COMM-ZQ00-PTF          VALUE 'O'.                     
                    15 COMM-ZQ00-NSOCSTKAV       PIC X(03).                     
                    15 COMM-ZQ00-NLIEUSTKAV      PIC X(03).                     
                    15 COMM-ZQ00-EMPORTE         PIC X(01).                     
                    15 COMM-ZQ00-LIVRAISON       PIC X(01).                     
                 15 COMM-ZQ00-PILOTAGE           PIC X(01) VALUE 'N'.           
                    88 COMM-ZQ00-RAZ             VALUE 'N'.                     
                    88 COMM-ZQ00-AFFICHAGE       VALUE 'A'.                     
                    88 COMM-ZQ00-VALIDREQUEST    VALUE 'V'.                     
                    88 COMM-ZQ00-VALIDATION      VALUE 'O'.                     
              05 COMM-ZQ00-FILLER          PIC X(187).                          
      *                                                                         
      *                                                                         
      ******* ZONES APPLICATIVES ZQ00 : NIVEAU 2 ******************* 500        
              05 COMM-ZQ00-REDEFINES       PIC X(500).                          
      ******* ZONES APPLICATIVES ZQ10 - GESTION ZONES GEO LIVR ****** 69        
              05 COMM-ZQ10-APPLI REDEFINES COMM-ZQ00-REDEFINES.                 
      *--------- PERIMETRE --------------------------------------------*        
                 10 COMM-ZQ10-PERIMETRE.                                        
                    15 COMM-ZQ10-CPERIM          PIC X(05).                     
                    15 COMM-ZQ10-LPERIM          PIC X(20).                     
                    15 COMM-ZQ10-LDEPLIV.                                       
                       20 COMM-ZQ10-NSOCDEPLIV      PIC X(03).                  
                       20 COMM-ZQ10-NLIEUDEPLIV     PIC X(03).                  
                    15 COMM-ZQ10-NBQUOTA         PIC 9(02).                     
      *--------- PARAMETRES DU PERIMETRES, OCCURS 1=LIVRE,OCCURS 2=SAV *        
                    15 COMM-ZQ10-PARAMETRES      OCCURS 2.                      
                       20 COMM-ZQ10-CTQUOTA         PIC X(01).                  
                       20 COMM-ZQ10-COUPLE.                                     
                          25 COMM-ZQ10-CEQUIP          PIC X(05).               
                          25 COMM-ZQ10-CMODDEL         PIC X(05).               
                       20 COMM-ZQ10-WZONE           PIC X(01).                  
                          88 COMM-ZQ10-ZE-ZL-OUI              VALUE 'O'.        
                 10 COMM-ZQ10-DJOUR           PIC X(08).                        
      *--------- PAGINATION -------------------------------------------*        
                 10 COMM-ZQ10-PAGIN.                                            
                    15 COMM-ZQ10-NPAGE           PIC S9(05) COMP-3.             
                    15 COMM-ZQ10-PAGEMAX         PIC S9(05) COMP-3.             
                 10 COMM-ZQ10-FILLER          PIC X(429).                       
      *                                                                         
      *                                                                         
      ******* ZONES APPLICATIVES ZQ10 : NIVEAU 3 ******************* 500        
              05 COMM-ZQ10-REDEFINES       PIC X(500).                          
      ******* ZONES APPLICATIVES ZQ12 ******************************** 6        
              05 COMM-ZQ12-APPLI REDEFINES COMM-ZQ10-REDEFINES.                 
      *--------- N�ORDRE DANS LE TRI PAR CRITERES (4) -----------------*        
                 10 COMM-ZQ12-CRITERES.                                         
                    15 COMM-ZQ12-NORDER       PIC X OCCURS 4.                   
      *--------- ORDER BY (N� DE COLONNE) -----------------------------*        
                 10 COMM-ZQ12-ORDERBY         PIC X(02).                        
      *                                                                         
      ******* ZONES APPLICATIVES ZQ16 ******************************* 25        
              05 COMM-ZQ16-APPLI REDEFINES COMM-ZQ10-REDEFINES.                 
                 10 COMM-ZQ16-CPRODEL         PIC X(05).                        
                 10 COMM-ZQ16-LPRODEL         PIC X(20).                        
                 10 COMM-ZQ16-FILLER          PIC X(475).                       
      *                                                                         
      ******* ZONES APPLICATIVES ZQ18 ******************************* 28        
              05 COMM-ZQ18-APPLI REDEFINES COMM-ZQ10-REDEFINES.                 
                 10 COMM-ZQ18-ZONES-TZQ20.                                      
                    15 COMM-ZQ18-NOM-PROG        PIC X(05).                     
                    15 COMM-ZQ18-ITYP-SEL        PIC 9.                         
                    15 COMM-ZQ18-DJOUR-SEL       PIC X(08).                     
                 10 COMM-ZQ18-DJOUR-ENCOURS   PIC X(08).                        
      *                                                                         
      ******* ZONES APPLICATIVES ZQ19 ****************************** 142        
              05 COMM-ZQ19-APPLI REDEFINES COMM-ZQ10-REDEFINES.                 
                 10 COMM-ZQ19-ZONES-TZQ20     PIC X(14).                        
      *--------- ZONES TOTAL JOURNEE -------------------------- 7*17=119        
                 10 COMM-ZQ19-DONNEES-TOTAL.                                    
                    15 COMM-ZQ19-TOTAL-JOUR      OCCURS 7.                      
                       20 COMM-ZQ19-DJOUR           PIC X(08).                  
                       20 COMM-ZQ19-ACTION          PIC X(01).                  
                          88 COMM-ZQ19-INACTION               VALUE ' '.        
                          88 COMM-ZQ19-MODIFICATION           VALUE 'M'.        
                       20 COMM-ZQ19-EXISTE          PIC X(01).                  
                          88 COMM-ZQ19-INEXISTENCE            VALUE ' '.        
                          88 COMM-ZQ19-EXISTENCE              VALUE 'E'.        
                       20 COMM-ZQ19-WILLIM          PIC X(01).                  
                          88 COMM-ZQ19-WILLIM-OUI             VALUE 'O'.        
                          88 COMM-ZQ19-WILLIM-NON             VALUE 'N'.        
                       20 COMM-ZQ19-QQUOTA          PIC S9(05) COMP-3.          
                       20 COMM-ZQ19-QPRIS           PIC S9(05) COMP-3.          
                 10 COMM-ZQ19-DJOUR-ENCOURS   PIC X(08).                        
                 10 COMM-ZQ19-ITYP            PIC 9.                            
      *                                                                         
      *                                                                         
      ******* ZONES APPLICATIVES ZQ20 : NIVEAU 4 ****************** 1500        
              05 COMM-ZQ20-REDEFINES          PIC X(1500).                      
      *------ ZONES APPLICATIVES ZQ20 ----------------------- 20+800=808        
              05 COMM-ZQ20-APPLI REDEFINES COMM-ZQ20-REDEFINES.                 
      *--------- PAGINATION -------------------------------------------*        
                 10 COMM-ZQ20-PAGIN.                                            
                    15 COMM-ZQ20-NPAGE           PIC S9(05) COMP-3.             
                    15 COMM-ZQ20-NPAGEMAX        PIC S9(05) COMP-3.             
                    15 COMM-ZQ20-NPAGE-HORIZ     PIC S9(05) COMP-3.             
                    15 COMM-ZQ20-NPAGEMAX-HORIZ  PIC S9(05) COMP-3.             
                 10 COMM-ZQ20-ITYP            PIC 9.                            
                 10 COMM-ZQ20-ZONES-QUOTA.                                      
                    15 COMM-ZQ20-QUOTA-JOUR.                                    
                       20 COMM-ZQ20-WILLIM          PIC X(01).                  
                          88 COMM-ZQ20-WILLIM-OUI             VALUE 'O'.        
                          88 COMM-ZQ20-WILLIM-NON             VALUE 'N'.        
                       20 COMM-ZQ20-QQUOTAJ         PIC S9(05) COMP-3.          
                       20 COMM-ZQ20-QPRISJ          PIC S9(05) COMP-3.          
                    15 COMM-ZQ20-POSTE-PLAGE     OCCURS 50.                     
                       20 COMM-ZQ20-CPLAGE          PIC X(02).                  
                       20 COMM-ZQ20-LPLAGE          PIC X(08).                  
                       20 COMM-ZQ20-QQUOTA-PL       PIC S9(05) COMP-3.          
                       20 COMM-ZQ20-QPRIS-PL        PIC S9(05) COMP-3.          
      *                                                                         
      **** ZONES APPLICATIVES GA00 ******************************** 3724        
           02 COMM-GA00-APPLI REDEFINES COMM-ZQ00-APPLI PIC X(3724).            
      *                                                                         
      **** ZONES APPLICATIVES GQ00 ******************************** 3724        
           02 COMM-GQ00-APPLI REDEFINES COMM-ZQ00-APPLI PIC X(3724).            
                                                                                
