      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFL1400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFL1400                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL1400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL1400.                                                            
      *}                                                                        
           02  FL14-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  FL14-NDEPOT                                                      
               PIC X(0003).                                                     
           02  FL14-CFAM                                                        
               PIC X(0005).                                                     
           02  FL14-QDELAIAPPRO                                                 
               PIC X(0003).                                                     
           02  FL14-QCOLICDE                                                    
               PIC S9(3) COMP-3.                                                
           02  FL14-QCOLIRECEPT                                                 
               PIC S9(5) COMP-3.                                                
           02  FL14-QCOLIDSTOCK                                                 
               PIC S9(5) COMP-3.                                                
           02  FL14-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  FL14-QNBRANMAIL                                                  
               PIC S9(3) COMP-3.                                                
           02  FL14-QNBNIVGERB                                                  
               PIC S9(3) COMP-3.                                                
           02  FL14-CZONEACCES                                                  
               PIC X(0001).                                                     
           02  FL14-CCONTENEUR                                                  
               PIC X(0001).                                                     
           02  FL14-CSPECIFSTK                                                  
               PIC X(0001).                                                     
           02  FL14-CCOTEHOLD                                                   
               PIC X(0001).                                                     
           02  FL14-QNBPVSOL                                                    
               PIC S9(3) COMP-3.                                                
           02  FL14-QNBPRACK                                                    
               PIC S9(5) COMP-3.                                                
           02  FL14-CTPSUP                                                      
               PIC X(0001).                                                     
           02  FL14-CQUOTA                                                      
               PIC X(0005).                                                     
           02  FL14-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFL1400                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL1400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL1400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-QDELAIAPPRO-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-QDELAIAPPRO-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-QCOLICDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-QCOLICDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-QCOLIRECEPT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-QCOLIRECEPT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-QCOLIDSTOCK-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-QCOLIDSTOCK-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-QNBRANMAIL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-QNBRANMAIL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-QNBNIVGERB-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-QNBNIVGERB-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-CZONEACCES-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-CZONEACCES-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-CCONTENEUR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-CCONTENEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-CSPECIFSTK-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-CSPECIFSTK-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-CCOTEHOLD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-CCOTEHOLD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-QNBPVSOL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-QNBPVSOL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-QNBPRACK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-QNBPRACK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-CTPSUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-CTPSUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-CQUOTA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-CQUOTA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL14-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL14-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
