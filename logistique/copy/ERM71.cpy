      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM54   ERM54                                              00000020
      ***************************************************************** 00000030
       01   ERM71I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCODICI  PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREFFOURNL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MREFFOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MREFFOURNF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MREFFOURNI     PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSIMULL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDSIMULL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDSIMULF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDSIMULI  PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSIMULL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNSIMULL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSIMULF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNSIMULI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMARQI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLMARQI   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRMGROUPL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCRMGROUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCRMGROUPF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCRMGROUPI     PIC X(5).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNSOCENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCENF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNSOCENI  PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNDEPOTI  PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQSODL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MQSODL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQSODF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQSODI    PIC X(4).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDEFFETI  PIC X(10).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINEFFL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MDFINEFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDFINEFFF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDFINEFFI      PIC X(10).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQSADL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MQSADL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQSADF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MQSADI    PIC X(3).                                       00000770
           02 M8I OCCURS   12 TIMES .                                   00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNSOCI  PIC X(3).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNLIEUI      PIC X(3).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOL   COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MQSOL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQSOF   PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQSOI   PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSAL   COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MQSAL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQSAF   PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQSAI   PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWBLOQUEL    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MWBLOQUEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWBLOQUEF    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MWBLOQUEI    PIC X.                                     00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MZONCMDI  PIC X(12).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLIBERRI  PIC X(61).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCICSI    PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MNETNAMI  PIC X(8).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MSCREENI  PIC X(4).                                       00001220
      ***************************************************************** 00001230
      * SDF: ERM54   ERM54                                              00001240
      ***************************************************************** 00001250
       01   ERM71O REDEFINES ERM71I.                                    00001260
           02 FILLER    PIC X(12).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTIMJOUA  PIC X.                                          00001360
           02 MTIMJOUC  PIC X.                                          00001370
           02 MTIMJOUP  PIC X.                                          00001380
           02 MTIMJOUH  PIC X.                                          00001390
           02 MTIMJOUV  PIC X.                                          00001400
           02 MTIMJOUO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNPAGEA   PIC X.                                          00001430
           02 MNPAGEC   PIC X.                                          00001440
           02 MNPAGEP   PIC X.                                          00001450
           02 MNPAGEH   PIC X.                                          00001460
           02 MNPAGEV   PIC X.                                          00001470
           02 MNPAGEO   PIC X(2).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNCODICA  PIC X.                                          00001500
           02 MNCODICC  PIC X.                                          00001510
           02 MNCODICP  PIC X.                                          00001520
           02 MNCODICH  PIC X.                                          00001530
           02 MNCODICV  PIC X.                                          00001540
           02 MNCODICO  PIC X(7).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MREFFOURNA     PIC X.                                     00001570
           02 MREFFOURNC     PIC X.                                     00001580
           02 MREFFOURNP     PIC X.                                     00001590
           02 MREFFOURNH     PIC X.                                     00001600
           02 MREFFOURNV     PIC X.                                     00001610
           02 MREFFOURNO     PIC X(20).                                 00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MDSIMULA  PIC X.                                          00001640
           02 MDSIMULC  PIC X.                                          00001650
           02 MDSIMULP  PIC X.                                          00001660
           02 MDSIMULH  PIC X.                                          00001670
           02 MDSIMULV  PIC X.                                          00001680
           02 MDSIMULO  PIC X(10).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCFAMA    PIC X.                                          00001710
           02 MCFAMC    PIC X.                                          00001720
           02 MCFAMP    PIC X.                                          00001730
           02 MCFAMH    PIC X.                                          00001740
           02 MCFAMV    PIC X.                                          00001750
           02 MCFAMO    PIC X(5).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MLFAMA    PIC X.                                          00001780
           02 MLFAMC    PIC X.                                          00001790
           02 MLFAMP    PIC X.                                          00001800
           02 MLFAMH    PIC X.                                          00001810
           02 MLFAMV    PIC X.                                          00001820
           02 MLFAMO    PIC X(20).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNSIMULA  PIC X.                                          00001850
           02 MNSIMULC  PIC X.                                          00001860
           02 MNSIMULP  PIC X.                                          00001870
           02 MNSIMULH  PIC X.                                          00001880
           02 MNSIMULV  PIC X.                                          00001890
           02 MNSIMULO  PIC X(3).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCMARQA   PIC X.                                          00001920
           02 MCMARQC   PIC X.                                          00001930
           02 MCMARQP   PIC X.                                          00001940
           02 MCMARQH   PIC X.                                          00001950
           02 MCMARQV   PIC X.                                          00001960
           02 MCMARQO   PIC X(5).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLMARQA   PIC X.                                          00001990
           02 MLMARQC   PIC X.                                          00002000
           02 MLMARQP   PIC X.                                          00002010
           02 MLMARQH   PIC X.                                          00002020
           02 MLMARQV   PIC X.                                          00002030
           02 MLMARQO   PIC X(20).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCRMGROUPA     PIC X.                                     00002060
           02 MCRMGROUPC     PIC X.                                     00002070
           02 MCRMGROUPP     PIC X.                                     00002080
           02 MCRMGROUPH     PIC X.                                     00002090
           02 MCRMGROUPV     PIC X.                                     00002100
           02 MCRMGROUPO     PIC X(5).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNSOCENA  PIC X.                                          00002130
           02 MNSOCENC  PIC X.                                          00002140
           02 MNSOCENP  PIC X.                                          00002150
           02 MNSOCENH  PIC X.                                          00002160
           02 MNSOCENV  PIC X.                                          00002170
           02 MNSOCENO  PIC X(3).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNDEPOTA  PIC X.                                          00002200
           02 MNDEPOTC  PIC X.                                          00002210
           02 MNDEPOTP  PIC X.                                          00002220
           02 MNDEPOTH  PIC X.                                          00002230
           02 MNDEPOTV  PIC X.                                          00002240
           02 MNDEPOTO  PIC X(3).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MQSODA    PIC X.                                          00002270
           02 MQSODC    PIC X.                                          00002280
           02 MQSODP    PIC X.                                          00002290
           02 MQSODH    PIC X.                                          00002300
           02 MQSODV    PIC X.                                          00002310
           02 MQSODO    PIC X(4).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MDEFFETA  PIC X.                                          00002340
           02 MDEFFETC  PIC X.                                          00002350
           02 MDEFFETP  PIC X.                                          00002360
           02 MDEFFETH  PIC X.                                          00002370
           02 MDEFFETV  PIC X.                                          00002380
           02 MDEFFETO  PIC X(10).                                      00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MDFINEFFA      PIC X.                                     00002410
           02 MDFINEFFC PIC X.                                          00002420
           02 MDFINEFFP PIC X.                                          00002430
           02 MDFINEFFH PIC X.                                          00002440
           02 MDFINEFFV PIC X.                                          00002450
           02 MDFINEFFO      PIC X(10).                                 00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MQSADA    PIC X.                                          00002480
           02 MQSADC    PIC X.                                          00002490
           02 MQSADP    PIC X.                                          00002500
           02 MQSADH    PIC X.                                          00002510
           02 MQSADV    PIC X.                                          00002520
           02 MQSADO    PIC X(3).                                       00002530
           02 M8O OCCURS   12 TIMES .                                   00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MNSOCA  PIC X.                                          00002560
             03 MNSOCC  PIC X.                                          00002570
             03 MNSOCP  PIC X.                                          00002580
             03 MNSOCH  PIC X.                                          00002590
             03 MNSOCV  PIC X.                                          00002600
             03 MNSOCO  PIC X(3).                                       00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MNLIEUA      PIC X.                                     00002630
             03 MNLIEUC PIC X.                                          00002640
             03 MNLIEUP PIC X.                                          00002650
             03 MNLIEUH PIC X.                                          00002660
             03 MNLIEUV PIC X.                                          00002670
             03 MNLIEUO      PIC X(3).                                  00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MQSOA   PIC X.                                          00002700
             03 MQSOC   PIC X.                                          00002710
             03 MQSOP   PIC X.                                          00002720
             03 MQSOH   PIC X.                                          00002730
             03 MQSOV   PIC X.                                          00002740
             03 MQSOO   PIC X(4).                                       00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MQSAA   PIC X.                                          00002770
             03 MQSAC   PIC X.                                          00002780
             03 MQSAP   PIC X.                                          00002790
             03 MQSAH   PIC X.                                          00002800
             03 MQSAV   PIC X.                                          00002810
             03 MQSAO   PIC X(3).                                       00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MWBLOQUEA    PIC X.                                     00002840
             03 MWBLOQUEC    PIC X.                                     00002850
             03 MWBLOQUEP    PIC X.                                     00002860
             03 MWBLOQUEH    PIC X.                                     00002870
             03 MWBLOQUEV    PIC X.                                     00002880
             03 MWBLOQUEO    PIC X.                                     00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MZONCMDA  PIC X.                                          00002910
           02 MZONCMDC  PIC X.                                          00002920
           02 MZONCMDP  PIC X.                                          00002930
           02 MZONCMDH  PIC X.                                          00002940
           02 MZONCMDV  PIC X.                                          00002950
           02 MZONCMDO  PIC X(12).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLIBERRA  PIC X.                                          00002980
           02 MLIBERRC  PIC X.                                          00002990
           02 MLIBERRP  PIC X.                                          00003000
           02 MLIBERRH  PIC X.                                          00003010
           02 MLIBERRV  PIC X.                                          00003020
           02 MLIBERRO  PIC X(61).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSO    PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNETNAMA  PIC X.                                          00003190
           02 MNETNAMC  PIC X.                                          00003200
           02 MNETNAMP  PIC X.                                          00003210
           02 MNETNAMH  PIC X.                                          00003220
           02 MNETNAMV  PIC X.                                          00003230
           02 MNETNAMO  PIC X(8).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSCREENA  PIC X.                                          00003260
           02 MSCREENC  PIC X.                                          00003270
           02 MSCREENP  PIC X.                                          00003280
           02 MSCREENH  PIC X.                                          00003290
           02 MSCREENV  PIC X.                                          00003300
           02 MSCREENO  PIC X(4).                                       00003310
                                                                                
