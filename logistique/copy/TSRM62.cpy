      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
       01  TS-RM62-LONG             PIC S9(4) COMP-3 VALUE +565.                
       01  TS-RM62-RECORD.                                                      
           05 TS-RM62-LIGNE                OCCURS 5.                            
              10 TS-RM62-LAGREG     PIC X(20).                                  
              10 TS-RM62-QHV4       PIC 9(6).                                   
              10 TS-RM62-QHV3       PIC 9(6).                                   
              10 TS-RM62-QHV2       PIC 9(6).                                   
              10 TS-RM62-QHV1       PIC 9(6).                                   
              10 TS-RM62-QPM3       PIC 9(6).                                   
              10 TS-RM62-QPM3-F     PIC X.                                      
              10 TS-RM62-QPM2       PIC 9(6).                                   
              10 TS-RM62-QPM2-F     PIC X.                                      
              10 TS-RM62-QPM1       PIC 9(6).                                   
              10 TS-RM62-QPM1-F     PIC X.                                      
              10 TS-RM62-QSV4       PIC 9(6).                                   
              10 TS-RM62-QSV3       PIC 9(6).                                   
              10 TS-RM62-QSV2       PIC 9(6).                                   
              10 TS-RM62-QSV1       PIC 9(6).                                   
              10 TS-RM62-QPV0       PIC X(6).                                   
              10 TS-RM62-QPVF3      PIC X(6).                                   
              10 TS-RM62-QPVF2      PIC X(6).                                   
              10 TS-RM62-QPVF1      PIC X(6).                                   
                                                                                
