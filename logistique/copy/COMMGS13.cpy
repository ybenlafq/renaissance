      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010001
      * COMMAREA SPECIFIQUE PRG TGS13 (TGS10 -> MENU)    TR: GS10  *    00020001
      *               GESTION DES STOCKS MAGASIN                   *    00030001
      *                                                                 00040001
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3573  00050001
      *                                                            *    00060001
      *        TRANSACTION GS13 : CONSULTATION DES STOCKS          *    00070001
      *                                                                 00080001
          02 COMM-GS13-APPLI REDEFINES COMM-GS10-APPLI.                 00090001
      *------------------------------ ZONE DONNEES TGS13                00100001
             03 COMM-GS13-DONNEES-TGS13.                                00110001
      *------------------------------ LIBELLE STATUT ASSORTIMENT        00120001
                04 COMM-GS13-LSTATASSOR        PIC X(20).               00130001
      *------------------------------ LIBELLE STATUT APPROVISIONNEMENT  00140001
                04 COMM-GS13-LSTATAPPRO        PIC X(20).               00150001
      *------------------------------ LIBELLE STATUT EXPOSITION         00160001
                04 COMM-GS13-LSTATEXPO         PIC X(20).               00170001
      *-------------------------- ZONE CODICS PAGE                      00180001
                04 COMM-GS13-LIGCODICS         OCCURS 11.               00190001
                   05 COMM-GS13-NCODIC         PIC X(07).               00200001
                   05 COMM-GS13-CMARQ          PIC X(05).               00210001
                   05 COMM-GS13-LREFFOURN      PIC X(20).               00220001
                   05 COMM-GS13-LSTATCOMP      PIC X(03).               00230001
                   05 COMM-GS13-ZONSTOCK.                               00240001
                      06 COMM-GS13-QSTOCK1     PIC 9(04) OCCURS 4.      00250001
                      06 COMM-GS13-QSTOCK2     PIC 9(03) OCCURS 4.      00260001
      *------------------------------ TABLE N� TS PR PAGINATION         00270001
                04 COMM-GS13-TABART         OCCURS 100.                 00280001
      *------------------------------ 1ERS IND TS DES PAGES <=> CODIC   00290001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-GS13-NUMTS          PIC S9(4) COMP.          00300001
      *--                                                                       
                   05 COMM-GS13-NUMTS          PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ INDICATEUR FIN DE TABLE           00310001
                04 COMM-GS13-INDPAG            PIC 9.                   00320001
      *------------------------------ DERNIER IND-TS                    00330001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS13-PAGSUI            PIC S9(4) COMP.          00340001
      *--                                                                       
                04 COMM-GS13-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN  IND-TS                    00350001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS13-PAGENC            PIC S9(4) COMP.          00360001
      *--                                                                       
                04 COMM-GS13-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00370001
                04 COMM-GS13-NUMPAG            PIC 9(3).                00380001
      *------------------------------ IND POUR RECH DS TS               00390001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS13-IND-RECH-TS       PIC S9(4) COMP.          00400001
      *--                                                                       
                04 COMM-GS13-IND-RECH-TS       PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ IND TS MAX                        00410001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS13-IND-TS-MAX        PIC S9(4) COMP.          00420001
      *--                                                                       
                04 COMM-GS13-IND-TS-MAX        PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ZONES ATTRIBUTS POUR SWAP         00430001
             03 COMM-GS13-ZONSWAP.                                      00440001
                04 COMM-GS13-ATTR              PIC 9 OCCURS 500.        00450001
      *------------------------------ ZONE LIBRE                        00460001
  SM  *      03 COMM-GS13-LIBRE                PIC X(2039).             00470002
  SM  *      03 COMM-GS13-LIBRE                PIC X(1974).             00470003
  NT         03 COMM-GS13-LIBRE                PIC X(1434).             00470003
      ***************************************************************** 00480001
                                                                                
