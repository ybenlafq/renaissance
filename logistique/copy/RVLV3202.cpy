      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLV3202                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLV3202                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVLV3202.                                                            
           02  LV32-NSOCDEPOT      PIC X(0003).                                 
           02  LV32-NDEPOT         PIC X(0003).                                 
           02  LV32-NRECQUAI       PIC X(0007).                                 
           02  LV32-NCODIC         PIC X(0007).                                 
           02  LV32-NENTCDE        PIC X(0005).                                 
           02  LV32-DJRECQUAI      PIC X(0008).                                 
           02  LV32-HRECQUAI       PIC X(0006).                                 
           02  LV32-QRECQUAI       PIC S9(05) COMP-3.                           
           02  LV32-NDOSLM6        PIC X(0015).                                 
           02  LV32-DSYST          PIC S9(13) COMP-3.                           
           02  LV32-NRECQORIG      PIC X(0007).                                 
           02  LV32-CMVT           PIC X(0002).                                 
           02  LV32-QRANGEE        PIC S9(05) COMP-3.                           
           02  LV32-WFLAG          PIC X(0001).                                 
           02  LV32-CSTATUT        PIC X(0002).                                 
           02  LV32-LCOMMENT       PIC X(0020).                                 
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLV3202                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLV3202-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-NRECQUAI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-NRECQUAI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-DJRECQUAI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-DJRECQUAI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-HRECQUAI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-HRECQUAI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-QRECQUAI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-QRECQUAI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-NDOSLM6-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-NDOSLM6-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-NRECQORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-NRECQORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-CMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-CMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-QRANGEE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-QRANGEE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-WFLAG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-WFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-CSTATUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-CSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV32-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV32-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
