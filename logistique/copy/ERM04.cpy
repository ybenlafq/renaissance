      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM04   ERM04                                              00000020
      ***************************************************************** 00000030
       01   ERM04I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCGROUPI  PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELA1L  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCSELA1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCSELA1F  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCSELA1I  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELA2L  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCSELA2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCSELA2F  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCSELA2I  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELA3L  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCSELA3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCSELA3F  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCSELA3I  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELA4L  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCSELA4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCSELA4F  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCSELA4I  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELA5L  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCSELA5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCSELA5F  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCSELA5I  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELA6L  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCSELA6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCSELA6F  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCSELA6I  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTRESERVL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MQTRESERVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQTRESERVF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MQTRESERVI     PIC X(4).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPOIDSJGL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNPOIDSJGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNPOIDSJGF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNPOIDSJGI     PIC X(2).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQFREQG1L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MQFREQG1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQFREQG1F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MQFREQG1I      PIC X(4).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQFREQG2L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MQFREQG2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQFREQG2F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQFREQG2I      PIC X(4).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQFREQG3L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MQFREQG3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQFREQG3F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQFREQG3I      PIC X(4).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQFREQG4L      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MQFREQG4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQFREQG4F      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MQFREQG4I      PIC X(4).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQFREQG5L      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MQFREQG5L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQFREQG5F      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MQFREQG5I      PIC X(4).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQFREQG6L      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MQFREQG6L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQFREQG6F      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MQFREQG6I      PIC X(4).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWEXCFGL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MWEXCFGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWEXCFGF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MWEXCFGI  PIC X.                                          00000810
           02 LIGNEI OCCURS   13 TIMES .                                00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNSOCI  PIC X(3).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MNMAGI  PIC X(3).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMAGL  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MLMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLMAGF  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MLMAGI  PIC X(20).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTRESERL    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MQTRESERL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQTRESERF    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQTRESERI    PIC X(4).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNPOIDSJL    COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MNPOIDSJL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNPOIDSJF    PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MNPOIDSJI    PIC X(2).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQFREQ1L     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MQFREQ1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQFREQ1F     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQFREQ1I     PIC X(4).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQFREQ2L     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MQFREQ2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQFREQ2F     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MQFREQ2I     PIC X(4).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQFREQ3L     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MQFREQ3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQFREQ3F     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MQFREQ3I     PIC X(4).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQFREQ4L     COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MQFREQ4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQFREQ4F     PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MQFREQ4I     PIC X(4).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQFREQ5L     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MQFREQ5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQFREQ5F     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MQFREQ5I     PIC X(4).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQFREQ6L     COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MQFREQ6L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQFREQ6F     PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MQFREQ6I     PIC X(4).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWEXCFML     COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MWEXCFML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWEXCFMF     PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MWEXCFMI     PIC X.                                     00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPF11L   COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLPF11L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPF11F   PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLPF11I   PIC X(13).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MZONCMDI  PIC X(12).                                      00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MLIBERRI  PIC X(61).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MCODTRAI  PIC X(4).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCICSI    PIC X(5).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MNETNAMI  PIC X(8).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MSCREENI  PIC X(4).                                       00001580
      ***************************************************************** 00001590
      * SDF: ERM04   ERM04                                              00001600
      ***************************************************************** 00001610
       01   ERM04O REDEFINES ERM04I.                                    00001620
           02 FILLER    PIC X(12).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDATJOUA  PIC X.                                          00001650
           02 MDATJOUC  PIC X.                                          00001660
           02 MDATJOUP  PIC X.                                          00001670
           02 MDATJOUH  PIC X.                                          00001680
           02 MDATJOUV  PIC X.                                          00001690
           02 MDATJOUO  PIC X(10).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MTIMJOUA  PIC X.                                          00001720
           02 MTIMJOUC  PIC X.                                          00001730
           02 MTIMJOUP  PIC X.                                          00001740
           02 MTIMJOUH  PIC X.                                          00001750
           02 MTIMJOUV  PIC X.                                          00001760
           02 MTIMJOUO  PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCGROUPA  PIC X.                                          00001790
           02 MCGROUPC  PIC X.                                          00001800
           02 MCGROUPP  PIC X.                                          00001810
           02 MCGROUPH  PIC X.                                          00001820
           02 MCGROUPV  PIC X.                                          00001830
           02 MCGROUPO  PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNPAGEA   PIC X.                                          00001860
           02 MNPAGEC   PIC X.                                          00001870
           02 MNPAGEP   PIC X.                                          00001880
           02 MNPAGEH   PIC X.                                          00001890
           02 MNPAGEV   PIC X.                                          00001900
           02 MNPAGEO   PIC X(3).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCSELA1A  PIC X.                                          00001930
           02 MCSELA1C  PIC X.                                          00001940
           02 MCSELA1P  PIC X.                                          00001950
           02 MCSELA1H  PIC X.                                          00001960
           02 MCSELA1V  PIC X.                                          00001970
           02 MCSELA1O  PIC X(5).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MCSELA2A  PIC X.                                          00002000
           02 MCSELA2C  PIC X.                                          00002010
           02 MCSELA2P  PIC X.                                          00002020
           02 MCSELA2H  PIC X.                                          00002030
           02 MCSELA2V  PIC X.                                          00002040
           02 MCSELA2O  PIC X(5).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MCSELA3A  PIC X.                                          00002070
           02 MCSELA3C  PIC X.                                          00002080
           02 MCSELA3P  PIC X.                                          00002090
           02 MCSELA3H  PIC X.                                          00002100
           02 MCSELA3V  PIC X.                                          00002110
           02 MCSELA3O  PIC X(5).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCSELA4A  PIC X.                                          00002140
           02 MCSELA4C  PIC X.                                          00002150
           02 MCSELA4P  PIC X.                                          00002160
           02 MCSELA4H  PIC X.                                          00002170
           02 MCSELA4V  PIC X.                                          00002180
           02 MCSELA4O  PIC X(5).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCSELA5A  PIC X.                                          00002210
           02 MCSELA5C  PIC X.                                          00002220
           02 MCSELA5P  PIC X.                                          00002230
           02 MCSELA5H  PIC X.                                          00002240
           02 MCSELA5V  PIC X.                                          00002250
           02 MCSELA5O  PIC X(5).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCSELA6A  PIC X.                                          00002280
           02 MCSELA6C  PIC X.                                          00002290
           02 MCSELA6P  PIC X.                                          00002300
           02 MCSELA6H  PIC X.                                          00002310
           02 MCSELA6V  PIC X.                                          00002320
           02 MCSELA6O  PIC X(5).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MQTRESERVA     PIC X.                                     00002350
           02 MQTRESERVC     PIC X.                                     00002360
           02 MQTRESERVP     PIC X.                                     00002370
           02 MQTRESERVH     PIC X.                                     00002380
           02 MQTRESERVV     PIC X.                                     00002390
           02 MQTRESERVO     PIC X(4).                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNPOIDSJGA     PIC X.                                     00002420
           02 MNPOIDSJGC     PIC X.                                     00002430
           02 MNPOIDSJGP     PIC X.                                     00002440
           02 MNPOIDSJGH     PIC X.                                     00002450
           02 MNPOIDSJGV     PIC X.                                     00002460
           02 MNPOIDSJGO     PIC X(2).                                  00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MQFREQG1A      PIC X.                                     00002490
           02 MQFREQG1C PIC X.                                          00002500
           02 MQFREQG1P PIC X.                                          00002510
           02 MQFREQG1H PIC X.                                          00002520
           02 MQFREQG1V PIC X.                                          00002530
           02 MQFREQG1O      PIC X(4).                                  00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MQFREQG2A      PIC X.                                     00002560
           02 MQFREQG2C PIC X.                                          00002570
           02 MQFREQG2P PIC X.                                          00002580
           02 MQFREQG2H PIC X.                                          00002590
           02 MQFREQG2V PIC X.                                          00002600
           02 MQFREQG2O      PIC X(4).                                  00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MQFREQG3A      PIC X.                                     00002630
           02 MQFREQG3C PIC X.                                          00002640
           02 MQFREQG3P PIC X.                                          00002650
           02 MQFREQG3H PIC X.                                          00002660
           02 MQFREQG3V PIC X.                                          00002670
           02 MQFREQG3O      PIC X(4).                                  00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MQFREQG4A      PIC X.                                     00002700
           02 MQFREQG4C PIC X.                                          00002710
           02 MQFREQG4P PIC X.                                          00002720
           02 MQFREQG4H PIC X.                                          00002730
           02 MQFREQG4V PIC X.                                          00002740
           02 MQFREQG4O      PIC X(4).                                  00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MQFREQG5A      PIC X.                                     00002770
           02 MQFREQG5C PIC X.                                          00002780
           02 MQFREQG5P PIC X.                                          00002790
           02 MQFREQG5H PIC X.                                          00002800
           02 MQFREQG5V PIC X.                                          00002810
           02 MQFREQG5O      PIC X(4).                                  00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MQFREQG6A      PIC X.                                     00002840
           02 MQFREQG6C PIC X.                                          00002850
           02 MQFREQG6P PIC X.                                          00002860
           02 MQFREQG6H PIC X.                                          00002870
           02 MQFREQG6V PIC X.                                          00002880
           02 MQFREQG6O      PIC X(4).                                  00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MWEXCFGA  PIC X.                                          00002910
           02 MWEXCFGC  PIC X.                                          00002920
           02 MWEXCFGP  PIC X.                                          00002930
           02 MWEXCFGH  PIC X.                                          00002940
           02 MWEXCFGV  PIC X.                                          00002950
           02 MWEXCFGO  PIC X.                                          00002960
           02 LIGNEO OCCURS   13 TIMES .                                00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MNSOCA  PIC X.                                          00002990
             03 MNSOCC  PIC X.                                          00003000
             03 MNSOCP  PIC X.                                          00003010
             03 MNSOCH  PIC X.                                          00003020
             03 MNSOCV  PIC X.                                          00003030
             03 MNSOCO  PIC X(3).                                       00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MNMAGA  PIC X.                                          00003060
             03 MNMAGC  PIC X.                                          00003070
             03 MNMAGP  PIC X.                                          00003080
             03 MNMAGH  PIC X.                                          00003090
             03 MNMAGV  PIC X.                                          00003100
             03 MNMAGO  PIC X(3).                                       00003110
             03 FILLER       PIC X(2).                                  00003120
             03 MLMAGA  PIC X.                                          00003130
             03 MLMAGC  PIC X.                                          00003140
             03 MLMAGP  PIC X.                                          00003150
             03 MLMAGH  PIC X.                                          00003160
             03 MLMAGV  PIC X.                                          00003170
             03 MLMAGO  PIC X(20).                                      00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MQTRESERA    PIC X.                                     00003200
             03 MQTRESERC    PIC X.                                     00003210
             03 MQTRESERP    PIC X.                                     00003220
             03 MQTRESERH    PIC X.                                     00003230
             03 MQTRESERV    PIC X.                                     00003240
             03 MQTRESERO    PIC X(4).                                  00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MNPOIDSJA    PIC X.                                     00003270
             03 MNPOIDSJC    PIC X.                                     00003280
             03 MNPOIDSJP    PIC X.                                     00003290
             03 MNPOIDSJH    PIC X.                                     00003300
             03 MNPOIDSJV    PIC X.                                     00003310
             03 MNPOIDSJO    PIC X(2).                                  00003320
             03 FILLER       PIC X(2).                                  00003330
             03 MQFREQ1A     PIC X.                                     00003340
             03 MQFREQ1C     PIC X.                                     00003350
             03 MQFREQ1P     PIC X.                                     00003360
             03 MQFREQ1H     PIC X.                                     00003370
             03 MQFREQ1V     PIC X.                                     00003380
             03 MQFREQ1O     PIC X(4).                                  00003390
             03 FILLER       PIC X(2).                                  00003400
             03 MQFREQ2A     PIC X.                                     00003410
             03 MQFREQ2C     PIC X.                                     00003420
             03 MQFREQ2P     PIC X.                                     00003430
             03 MQFREQ2H     PIC X.                                     00003440
             03 MQFREQ2V     PIC X.                                     00003450
             03 MQFREQ2O     PIC X(4).                                  00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MQFREQ3A     PIC X.                                     00003480
             03 MQFREQ3C     PIC X.                                     00003490
             03 MQFREQ3P     PIC X.                                     00003500
             03 MQFREQ3H     PIC X.                                     00003510
             03 MQFREQ3V     PIC X.                                     00003520
             03 MQFREQ3O     PIC X(4).                                  00003530
             03 FILLER       PIC X(2).                                  00003540
             03 MQFREQ4A     PIC X.                                     00003550
             03 MQFREQ4C     PIC X.                                     00003560
             03 MQFREQ4P     PIC X.                                     00003570
             03 MQFREQ4H     PIC X.                                     00003580
             03 MQFREQ4V     PIC X.                                     00003590
             03 MQFREQ4O     PIC X(4).                                  00003600
             03 FILLER       PIC X(2).                                  00003610
             03 MQFREQ5A     PIC X.                                     00003620
             03 MQFREQ5C     PIC X.                                     00003630
             03 MQFREQ5P     PIC X.                                     00003640
             03 MQFREQ5H     PIC X.                                     00003650
             03 MQFREQ5V     PIC X.                                     00003660
             03 MQFREQ5O     PIC X(4).                                  00003670
             03 FILLER       PIC X(2).                                  00003680
             03 MQFREQ6A     PIC X.                                     00003690
             03 MQFREQ6C     PIC X.                                     00003700
             03 MQFREQ6P     PIC X.                                     00003710
             03 MQFREQ6H     PIC X.                                     00003720
             03 MQFREQ6V     PIC X.                                     00003730
             03 MQFREQ6O     PIC X(4).                                  00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MWEXCFMA     PIC X.                                     00003760
             03 MWEXCFMC     PIC X.                                     00003770
             03 MWEXCFMP     PIC X.                                     00003780
             03 MWEXCFMH     PIC X.                                     00003790
             03 MWEXCFMV     PIC X.                                     00003800
             03 MWEXCFMO     PIC X.                                     00003810
           02 FILLER    PIC X(2).                                       00003820
           02 MLPF11A   PIC X.                                          00003830
           02 MLPF11C   PIC X.                                          00003840
           02 MLPF11P   PIC X.                                          00003850
           02 MLPF11H   PIC X.                                          00003860
           02 MLPF11V   PIC X.                                          00003870
           02 MLPF11O   PIC X(13).                                      00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MZONCMDA  PIC X.                                          00003900
           02 MZONCMDC  PIC X.                                          00003910
           02 MZONCMDP  PIC X.                                          00003920
           02 MZONCMDH  PIC X.                                          00003930
           02 MZONCMDV  PIC X.                                          00003940
           02 MZONCMDO  PIC X(12).                                      00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MLIBERRA  PIC X.                                          00003970
           02 MLIBERRC  PIC X.                                          00003980
           02 MLIBERRP  PIC X.                                          00003990
           02 MLIBERRH  PIC X.                                          00004000
           02 MLIBERRV  PIC X.                                          00004010
           02 MLIBERRO  PIC X(61).                                      00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MCODTRAA  PIC X.                                          00004040
           02 MCODTRAC  PIC X.                                          00004050
           02 MCODTRAP  PIC X.                                          00004060
           02 MCODTRAH  PIC X.                                          00004070
           02 MCODTRAV  PIC X.                                          00004080
           02 MCODTRAO  PIC X(4).                                       00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MCICSA    PIC X.                                          00004110
           02 MCICSC    PIC X.                                          00004120
           02 MCICSP    PIC X.                                          00004130
           02 MCICSH    PIC X.                                          00004140
           02 MCICSV    PIC X.                                          00004150
           02 MCICSO    PIC X(5).                                       00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MNETNAMA  PIC X.                                          00004180
           02 MNETNAMC  PIC X.                                          00004190
           02 MNETNAMP  PIC X.                                          00004200
           02 MNETNAMH  PIC X.                                          00004210
           02 MNETNAMV  PIC X.                                          00004220
           02 MNETNAMO  PIC X(8).                                       00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MSCREENA  PIC X.                                          00004250
           02 MSCREENC  PIC X.                                          00004260
           02 MSCREENP  PIC X.                                          00004270
           02 MSCREENH  PIC X.                                          00004280
           02 MSCREENV  PIC X.                                          00004290
           02 MSCREENO  PIC X(4).                                       00004300
                                                                                
