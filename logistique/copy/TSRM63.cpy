      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM63                                          *  00020001
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-RM63-LONG            PIC S9(3) COMP-3 VALUE 124.          00060002
       01  TS-RM63-DONNEES.                                             00070001
              10 TS-RM63-NCODIC    PIC X(7).                            00080001
              10 TS-RM63-QRANG     PIC X(3).                            00090001
              10 TS-RM63-MCMARQ    PIC X(5).                            00100001
              10 TS-RM63-SENSAPPRO PIC X.                               00110001
              10 TS-RM63-MQV4S     PIC S9(5).                           00120001
              10 TS-RM63-MQV3S     PIC S9(5).                           00130001
              10 TS-RM63-MQV2S     PIC S9(5).                           00140001
              10 TS-RM63-MQV1S     PIC S9(5).                           00150001
              10 TS-RM63-MQR1S     PIC S9(5).                           00160001
              10 TS-RM63-MQR2S     PIC S9(5).                           00170001
              10 TS-RM63-MQR3S     PIC S9(5).                           00180001
              10 TS-RM63-MQR1S-T   PIC X.                               00190001
              10 TS-RM63-MQR2S-T   PIC X.                               00200001
              10 TS-RM63-MQR3S-T   PIC X.                               00210001
              10 TS-RM63-REF       PIC X(20).                           00220001
              10 TS-RM63-MQSV4D    PIC S9(5).                           00230001
              10 TS-RM63-MQSV3D    PIC S9(5).                           00240001
              10 TS-RM63-MQSV2D    PIC S9(5).                           00250001
              10 TS-RM63-MQSV1D    PIC S9(5).                           00260001
              10 TS-RM63-MQPV0D    PIC S9(5).                           00270001
              10 TS-RM63-MQPV0D-OK PIC X.                               00280001
              10 TS-RM63-QPV1F     PIC X(5).                            00290001
              10 TS-RM63-INSERT1   PIC X.                               00300001
              10 TS-RM63-MODIF1    PIC X.                               00310001
              10 TS-RM63-QPV2F     PIC X(5).                            00320001
              10 TS-RM63-INSERT2   PIC X.                               00330001
              10 TS-RM63-MODIF2    PIC X.                               00340001
              10 TS-RM63-QPV3F     PIC X(5).                            00350001
              10 TS-RM63-INSERT3   PIC X.                               00360001
              10 TS-RM63-MODIF3    PIC X.                               00370001
              10 TS-RM63-QV4STRI   PIC S9(5) COMP-3.                    00380002
                                                                                
