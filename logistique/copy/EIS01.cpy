      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EIE00   EIE00                                              00000020
      ***************************************************************** 00000030
       01   EIS01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE JOUR TRAITEMENT                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE TRAITEMENT                                                00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
           02 MLIGNEI OCCURS   15 TIMES .                               00000240
      * SOCIETE DU DEPOT                                                00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCDEPL     COMP PIC S9(4).                            00000260
      *--                                                                       
             03 MSOCDEPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCDEPF     PIC X.                                     00000270
             03 FILLER  PIC X(4).                                       00000280
             03 MSOCDEPI     PIC X(3).                                  00000290
      * NUM DEPOT                                                       00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEPOTL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MDEPOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDEPOTF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MDEPOTI      PIC X(3).                                  00000340
      * SOUS LIEU                                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSSLIEUL     COMP PIC S9(4).                            00000360
      *--                                                                       
             03 MSSLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSSLIEUF     PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 MSSLIEUI     PIC X.                                     00000390
      * LIEU TRAITEMENT                                                 00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUTRL     COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MLIEUTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIEUTRF     PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MLIEUTRI     PIC X(5).                                  00000440
      * ZONE VRAC                                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONEL  COMP PIC S9(4).                                 00000460
      *--                                                                       
             03 MZONEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MZONEF  PIC X.                                          00000470
             03 FILLER  PIC X(4).                                       00000480
             03 MZONEI  PIC X(2).                                       00000490
      * SECTEUR                                                         00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSECTL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MSECTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSECTF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MSECTI  PIC X(2).                                       00000540
      * LIBELLE ZONE FICTIVE                                            00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELL      COMP PIC S9(4).                            00000560
      *--                                                                       
             03 MLIBELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIBELF      PIC X.                                     00000570
             03 FILLER  PIC X(4).                                       00000580
             03 MLIBELI      PIC X(20).                                 00000590
      * FLAG STOCK AV=AP                                                00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAVAPL  COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 MAVAPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MAVAPF  PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MAVAPI  PIC X.                                          00000640
      * TYPE DE COMPTAGE                                                00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPCPTL     COMP PIC S9(4).                            00000660
      *--                                                                       
             03 MTYPCPTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPCPTF     PIC X.                                     00000670
             03 FILLER  PIC X(4).                                       00000680
             03 MTYPCPTI     PIC X(2).                                  00000690
      * QUANTITE MULT/UNI                                               00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQTEI   PIC X.                                          00000740
      * PREMIERE FICHE                                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFICHE1L     COMP PIC S9(4).                            00000760
      *--                                                                       
             03 MFICHE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MFICHE1F     PIC X.                                     00000770
             03 FILLER  PIC X(4).                                       00000780
             03 MFICHE1I     PIC X(5).                                  00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFICHE2L     COMP PIC S9(4).                            00000800
      *--                                                                       
             03 MFICHE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MFICHE2F     PIC X.                                     00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MFICHE2I     PIC X(5).                                  00000830
      * LIBELLE ERREUR                                                  00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MLIBERRI  PIC X(78).                                      00000880
      * CODE TRANSACTION                                                00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCODTRAI  PIC X(4).                                       00000930
      * NOM DU CICS                                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      * NOM LIGNE VTAM                                                  00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MNETNAMI  PIC X(8).                                       00001030
      * CODE TERMINAL                                                   00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MSCREENI  PIC X(4).                                       00001080
      ***************************************************************** 00001090
      * SDF: EIE00   EIE00                                              00001100
      ***************************************************************** 00001110
       01   EIS01O REDEFINES EIS01I.                                    00001120
           02 FILLER    PIC X(12).                                      00001130
      * DATE JOUR TRAITEMENT                                            00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MDATJOUA  PIC X.                                          00001160
           02 MDATJOUC  PIC X.                                          00001170
           02 MDATJOUP  PIC X.                                          00001180
           02 MDATJOUH  PIC X.                                          00001190
           02 MDATJOUV  PIC X.                                          00001200
           02 MDATJOUO  PIC X(10).                                      00001210
      * HEURE TRAITEMENT                                                00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MPAGEA    PIC X.                                          00001310
           02 MPAGEC    PIC X.                                          00001320
           02 MPAGEP    PIC X.                                          00001330
           02 MPAGEH    PIC X.                                          00001340
           02 MPAGEV    PIC X.                                          00001350
           02 MPAGEO    PIC X(2).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MNBPA     PIC X.                                          00001380
           02 MNBPC     PIC X.                                          00001390
           02 MNBPP     PIC X.                                          00001400
           02 MNBPH     PIC X.                                          00001410
           02 MNBPV     PIC X.                                          00001420
           02 MNBPO     PIC X(2).                                       00001430
           02 MLIGNEO OCCURS   15 TIMES .                               00001440
      * SOCIETE DU DEPOT                                                00001450
             03 FILLER       PIC X(2).                                  00001460
             03 MSOCDEPA     PIC X.                                     00001470
             03 MSOCDEPC     PIC X.                                     00001480
             03 MSOCDEPP     PIC X.                                     00001490
             03 MSOCDEPH     PIC X.                                     00001500
             03 MSOCDEPV     PIC X.                                     00001510
             03 MSOCDEPO     PIC X(3).                                  00001520
      * NUM DEPOT                                                       00001530
             03 FILLER       PIC X(2).                                  00001540
             03 MDEPOTA      PIC X.                                     00001550
             03 MDEPOTC PIC X.                                          00001560
             03 MDEPOTP PIC X.                                          00001570
             03 MDEPOTH PIC X.                                          00001580
             03 MDEPOTV PIC X.                                          00001590
             03 MDEPOTO      PIC X(3).                                  00001600
      * SOUS LIEU                                                       00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MSSLIEUA     PIC X.                                     00001630
             03 MSSLIEUC     PIC X.                                     00001640
             03 MSSLIEUP     PIC X.                                     00001650
             03 MSSLIEUH     PIC X.                                     00001660
             03 MSSLIEUV     PIC X.                                     00001670
             03 MSSLIEUO     PIC X.                                     00001680
      * LIEU TRAITEMENT                                                 00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MLIEUTRA     PIC X.                                     00001710
             03 MLIEUTRC     PIC X.                                     00001720
             03 MLIEUTRP     PIC X.                                     00001730
             03 MLIEUTRH     PIC X.                                     00001740
             03 MLIEUTRV     PIC X.                                     00001750
             03 MLIEUTRO     PIC X(5).                                  00001760
      * ZONE VRAC                                                       00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MZONEA  PIC X.                                          00001790
             03 MZONEC  PIC X.                                          00001800
             03 MZONEP  PIC X.                                          00001810
             03 MZONEH  PIC X.                                          00001820
             03 MZONEV  PIC X.                                          00001830
             03 MZONEO  PIC X(2).                                       00001840
      * SECTEUR                                                         00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MSECTA  PIC X.                                          00001870
             03 MSECTC  PIC X.                                          00001880
             03 MSECTP  PIC X.                                          00001890
             03 MSECTH  PIC X.                                          00001900
             03 MSECTV  PIC X.                                          00001910
             03 MSECTO  PIC X(2).                                       00001920
      * LIBELLE ZONE FICTIVE                                            00001930
             03 FILLER       PIC X(2).                                  00001940
             03 MLIBELA      PIC X.                                     00001950
             03 MLIBELC PIC X.                                          00001960
             03 MLIBELP PIC X.                                          00001970
             03 MLIBELH PIC X.                                          00001980
             03 MLIBELV PIC X.                                          00001990
             03 MLIBELO      PIC X(20).                                 00002000
      * FLAG STOCK AV=AP                                                00002010
             03 FILLER       PIC X(2).                                  00002020
             03 MAVAPA  PIC X.                                          00002030
             03 MAVAPC  PIC X.                                          00002040
             03 MAVAPP  PIC X.                                          00002050
             03 MAVAPH  PIC X.                                          00002060
             03 MAVAPV  PIC X.                                          00002070
             03 MAVAPO  PIC X.                                          00002080
      * TYPE DE COMPTAGE                                                00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MTYPCPTA     PIC X.                                     00002110
             03 MTYPCPTC     PIC X.                                     00002120
             03 MTYPCPTP     PIC X.                                     00002130
             03 MTYPCPTH     PIC X.                                     00002140
             03 MTYPCPTV     PIC X.                                     00002150
             03 MTYPCPTO     PIC X(2).                                  00002160
      * QUANTITE MULT/UNI                                               00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MQTEA   PIC X.                                          00002190
             03 MQTEC   PIC X.                                          00002200
             03 MQTEP   PIC X.                                          00002210
             03 MQTEH   PIC X.                                          00002220
             03 MQTEV   PIC X.                                          00002230
             03 MQTEO   PIC X.                                          00002240
      * PREMIERE FICHE                                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MFICHE1A     PIC X.                                     00002270
             03 MFICHE1C     PIC X.                                     00002280
             03 MFICHE1P     PIC X.                                     00002290
             03 MFICHE1H     PIC X.                                     00002300
             03 MFICHE1V     PIC X.                                     00002310
             03 MFICHE1O     PIC X(5).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MFICHE2A     PIC X.                                     00002340
             03 MFICHE2C     PIC X.                                     00002350
             03 MFICHE2P     PIC X.                                     00002360
             03 MFICHE2H     PIC X.                                     00002370
             03 MFICHE2V     PIC X.                                     00002380
             03 MFICHE2O     PIC X(5).                                  00002390
      * LIBELLE ERREUR                                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MLIBERRA  PIC X.                                          00002420
           02 MLIBERRC  PIC X.                                          00002430
           02 MLIBERRP  PIC X.                                          00002440
           02 MLIBERRH  PIC X.                                          00002450
           02 MLIBERRV  PIC X.                                          00002460
           02 MLIBERRO  PIC X(78).                                      00002470
      * CODE TRANSACTION                                                00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
      * NOM DU CICS                                                     00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MCICSA    PIC X.                                          00002580
           02 MCICSC    PIC X.                                          00002590
           02 MCICSP    PIC X.                                          00002600
           02 MCICSH    PIC X.                                          00002610
           02 MCICSV    PIC X.                                          00002620
           02 MCICSO    PIC X(5).                                       00002630
      * NOM LIGNE VTAM                                                  00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MNETNAMA  PIC X.                                          00002660
           02 MNETNAMC  PIC X.                                          00002670
           02 MNETNAMP  PIC X.                                          00002680
           02 MNETNAMH  PIC X.                                          00002690
           02 MNETNAMV  PIC X.                                          00002700
           02 MNETNAMO  PIC X(8).                                       00002710
      * CODE TERMINAL                                                   00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MSCREENA  PIC X.                                          00002740
           02 MSCREENC  PIC X.                                          00002750
           02 MSCREENP  PIC X.                                          00002760
           02 MSCREENH  PIC X.                                          00002770
           02 MSCREENV  PIC X.                                          00002780
           02 MSCREENO  PIC X(4).                                       00002790
                                                                                
