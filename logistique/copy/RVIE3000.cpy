      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COPY DE LA VUE RVIE3000 DE LA TABLE RTIE30                              
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIE3000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIE3000.                                                            
      *}                                                                        
           03 IE30-NSOCDEPOT            PIC X(3).                               
           03 IE30-NDEPOT               PIC X(3).                               
           03 IE30-NSSLIEU              PIC X(3).                               
           03 IE30-CLIEUTRT             PIC X(5).                               
           03 IE30-ZONE                 PIC X(2).                               
           03 IE30-SECTEUR              PIC X(2).                               
           03 IE30-LZONE                PIC X(20).                              
           03 IE30-WSTOCKAVAP           PIC X(1).                               
           03 IE30-CTYPECPT             PIC X(2).                               
           03 IE30-WQTEMULTIPLE         PIC X(1).                               
           03 IE30-NUMFICHDEB           PIC X(5).                               
           03 IE30-NUMFICHFIN           PIC X(5).                               
           03 IE30-Q2COMPTAGEG          PIC S9(5)  COMP-3.                      
           03 IE30-W2GLOBALOK           PIC X(1).                               
           03 IE30-DSYST                PIC S9(13) COMP-3.                      
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------         
      * LISTE DE FLAGS DE LA VUE RVIE3000                                       
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIE3000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIE3000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-NSOCDEPOT-F          PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-NSOCDEPOT-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-NDEPOT-F             PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-NDEPOT-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-NSSLIEU-F            PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-NSSLIEU-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-CLIEUTRT-F           PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-CLIEUTRT-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-ZONE-F               PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-ZONE-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-SECTEUR-F            PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-SECTEUR-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-LZONE-F              PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-LZONE-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-WSTOCKAVAP-F         PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-WSTOCKAVAP-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-CTYPECPT-F           PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-CTYPECPT-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-WQTEMULTIPLE-F       PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-WQTEMULTIPLE-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-NUMFICHDEB-F         PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-NUMFICHDEB-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-NUMFICHFIN-F         PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-NUMFICHFIN-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-Q2COMPTAGEG-F        PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-Q2COMPTAGEG-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-W2GLOBALOK-F         PIC S9(4) COMP.                         
      *--                                                                       
           03 IE30-W2GLOBALOK-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 IE30-DSYST-F              PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           03 IE30-DSYST-F              PIC S9(4) COMP-5.                       
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
