      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IQ028A AU 23/04/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,20,BI,A,                          *        
      *                           30,05,BI,A,                          *        
      *                           35,05,BI,A,                          *        
      *                           40,05,BI,A,                          *        
      *                           45,03,BI,A,                          *        
      *                           48,07,BI,A,                          *        
      *                           55,08,BI,A,                          *        
      *                           63,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IQ028A.                                                        
            05 NOMETAT-IQ028A           PIC X(6) VALUE 'IQ028A'.                
            05 RUPTURES-IQ028A.                                                 
           10 IQ028A-NSOCIETE           PIC X(03).                      007  003
           10 IQ028A-LVPARAM            PIC X(20).                      010  020
           10 IQ028A-WSEQFAM            PIC X(05).                      030  005
           10 IQ028A-CMARQ              PIC X(05).                      035  005
           10 IQ028A-CAPPRO             PIC X(05).                      040  005
           10 IQ028A-STATCOMP           PIC X(03).                      045  003
           10 IQ028A-NCODIC             PIC X(07).                      048  007
           10 IQ028A-DCDE               PIC X(08).                      055  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IQ028A-SEQUENCE           PIC S9(04) COMP.                063  002
      *--                                                                       
           10 IQ028A-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IQ028A.                                                   
           10 IQ028A-CDEVISE            PIC X(03).                      065  003
           10 IQ028A-CDEVREF            PIC X(03).                      068  003
           10 IQ028A-CFAM               PIC X(05).                      071  005
           10 IQ028A-DTRECEP            PIC X(08).                      076  008
           10 IQ028A-ETOILE             PIC X(01).                      084  001
           10 IQ028A-JOURNEE            PIC X(10).                      085  010
           10 IQ028A-LIB1               PIC X(02).                      095  002
           10 IQ028A-LIB2               PIC X(02).                      097  002
           10 IQ028A-LREFFOURN          PIC X(20).                      099  020
           10 IQ028A-WSENSNF            PIC X(01).                      119  001
           10 IQ028A-DISPO-COMM         PIC S9(06)      COMP-3.         120  004
           10 IQ028A-INTER              PIC S9(04)V9(1) COMP-3.         124  003
           10 IQ028A-NBREART            PIC S9(07)      COMP-3.         127  004
           10 IQ028A-PSTDTTCEU          PIC S9(07)V9(2) COMP-3.         131  005
           10 IQ028A-PSTDTTCZ1          PIC S9(07)V9(2) COMP-3.         136  005
           10 IQ028A-QSTOCKDEP          PIC S9(07)      COMP-3.         141  004
           10 IQ028A-QSTOCKMAG          PIC S9(07)      COMP-3.         145  004
           10 IQ028A-TOTAL-COMM         PIC S9(06)      COMP-3.         149  004
            05 FILLER                      PIC X(360).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IQ028A-LONG           PIC S9(4)   COMP  VALUE +152.           
      *                                                                         
      *--                                                                       
        01  DSECT-IQ028A-LONG           PIC S9(4) COMP-5  VALUE +152.           
                                                                                
      *}                                                                        
