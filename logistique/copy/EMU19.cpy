      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU19   EMU19                                              00000020
      ***************************************************************** 00000030
       01   EMU19I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEMAXL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNPAGEMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNPAGEMAXF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEMAXI     PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTSOCL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNENTSOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTSOCF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNENTSOCI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTLIEUL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNENTLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTLIEUF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNENTLIEUI     PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGSOCL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNMAGSOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMAGSOCF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNMAGSOCI      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGLIEUL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNMAGLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNMAGLIEUF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNMAGLIEUI     PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEBUTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDDEBUTI  PIC X(8).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJOURL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDJOURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDJOURF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDJOURI   PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVAR1L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLVAR1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLVAR1F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLVAR1I   PIC X(9).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVAR2L   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLVAR2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLVAR2F   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLVAR2I   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVAR3L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLVAR3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLVAR3F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLVAR3I   PIC X(9).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVAR4L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLVAR4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLVAR4F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLVAR4I   PIC X(5).                                       00000610
           02 MDETAILMUT1I OCCURS   13 TIMES .                          00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MNMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMUTF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNMUTI  PIC X(7).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEOL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MDATEOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEOF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MDATEOI      PIC X(4).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEFL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDATEFL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEFF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDATEFI      PIC X(4).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEDL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDATEDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEDF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDATEDI      PIC X(4).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATECL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MDATECL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATECF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MDATECI      PIC X(4).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHDATECL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MHDATECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MHDATECF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MHDATECI     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEML      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDATEML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEMF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDATEMI      PIC X(4).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDHHMUTL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MDHHMUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDHHMUTF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MDHHMUTI     PIC X(2).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMMMUTL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MDMMMUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDMMMUTF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MDMMMUTI     PIC X(2).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTSOCMUTL      COMP PIC S9(4).                       00000990
      *--                                                                       
             03 MNENTSOCMUTL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MNENTSOCMUTF      PIC X.                                00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MNENTSOCMUTI      PIC X(3).                             00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTLIEUMUTL     COMP PIC S9(4).                       00001030
      *--                                                                       
             03 MNENTLIEUMUTL COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 MNENTLIEUMUTF     PIC X.                                00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNENTLIEUMUTI     PIC X(3).                             00001060
           02 MDETAILMUT2I OCCURS   13 TIMES .                          00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVAR1L  COMP PIC S9(4).                                 00001080
      *--                                                                       
             03 MVAR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MVAR1F  PIC X.                                          00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MVAR1I  PIC X(9).                                       00001110
           02 MDETAILMUT3I OCCURS   13 TIMES .                          00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTAM3L    COMP PIC S9(4).                            00001130
      *--                                                                       
             03 MQUOTAM3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQUOTAM3F    PIC X.                                     00001140
             03 FILLER  PIC X(4).                                       00001150
             03 MQUOTAM3I    PIC X(4).                                  00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTAPROL   COMP PIC S9(4).                            00001170
      *--                                                                       
             03 MQUOTAPROL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQUOTAPROF   PIC X.                                     00001180
             03 FILLER  PIC X(4).                                       00001190
             03 MQUOTAPROI   PIC X(4).                                  00001200
           02 MDETAILMUT4I OCCURS   13 TIMES .                          00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVAR2L  COMP PIC S9(4).                                 00001220
      *--                                                                       
             03 MVAR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MVAR2F  PIC X.                                          00001230
             03 FILLER  PIC X(4).                                       00001240
             03 MVAR2I  PIC X(5).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MZONCMDI  PIC X(15).                                      00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MLIBERRI  PIC X(58).                                      00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MCODTRAI  PIC X(4).                                       00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MCICSI    PIC X(5).                                       00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MNETNAMI  PIC X(8).                                       00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MSCREENI  PIC X(4).                                       00001490
      ***************************************************************** 00001500
      * SDF: EMU19   EMU19                                              00001510
      ***************************************************************** 00001520
       01   EMU19O REDEFINES EMU19I.                                    00001530
           02 FILLER    PIC X(12).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MDATJOUA  PIC X.                                          00001560
           02 MDATJOUC  PIC X.                                          00001570
           02 MDATJOUP  PIC X.                                          00001580
           02 MDATJOUH  PIC X.                                          00001590
           02 MDATJOUV  PIC X.                                          00001600
           02 MDATJOUO  PIC X(10).                                      00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MTIMJOUA  PIC X.                                          00001630
           02 MTIMJOUC  PIC X.                                          00001640
           02 MTIMJOUP  PIC X.                                          00001650
           02 MTIMJOUH  PIC X.                                          00001660
           02 MTIMJOUV  PIC X.                                          00001670
           02 MTIMJOUO  PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNPAGEA   PIC X.                                          00001700
           02 MNPAGEC   PIC X.                                          00001710
           02 MNPAGEP   PIC X.                                          00001720
           02 MNPAGEH   PIC X.                                          00001730
           02 MNPAGEV   PIC X.                                          00001740
           02 MNPAGEO   PIC X(2).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNPAGEMAXA     PIC X.                                     00001770
           02 MNPAGEMAXC     PIC X.                                     00001780
           02 MNPAGEMAXP     PIC X.                                     00001790
           02 MNPAGEMAXH     PIC X.                                     00001800
           02 MNPAGEMAXV     PIC X.                                     00001810
           02 MNPAGEMAXO     PIC X(2).                                  00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MNENTSOCA      PIC X.                                     00001840
           02 MNENTSOCC PIC X.                                          00001850
           02 MNENTSOCP PIC X.                                          00001860
           02 MNENTSOCH PIC X.                                          00001870
           02 MNENTSOCV PIC X.                                          00001880
           02 MNENTSOCO      PIC X(3).                                  00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MNENTLIEUA     PIC X.                                     00001910
           02 MNENTLIEUC     PIC X.                                     00001920
           02 MNENTLIEUP     PIC X.                                     00001930
           02 MNENTLIEUH     PIC X.                                     00001940
           02 MNENTLIEUV     PIC X.                                     00001950
           02 MNENTLIEUO     PIC X(3).                                  00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNMAGSOCA      PIC X.                                     00001980
           02 MNMAGSOCC PIC X.                                          00001990
           02 MNMAGSOCP PIC X.                                          00002000
           02 MNMAGSOCH PIC X.                                          00002010
           02 MNMAGSOCV PIC X.                                          00002020
           02 MNMAGSOCO      PIC X(3).                                  00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNMAGLIEUA     PIC X.                                     00002050
           02 MNMAGLIEUC     PIC X.                                     00002060
           02 MNMAGLIEUP     PIC X.                                     00002070
           02 MNMAGLIEUH     PIC X.                                     00002080
           02 MNMAGLIEUV     PIC X.                                     00002090
           02 MNMAGLIEUO     PIC X(3).                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MDDEBUTA  PIC X.                                          00002120
           02 MDDEBUTC  PIC X.                                          00002130
           02 MDDEBUTP  PIC X.                                          00002140
           02 MDDEBUTH  PIC X.                                          00002150
           02 MDDEBUTV  PIC X.                                          00002160
           02 MDDEBUTO  PIC X(8).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MDJOURA   PIC X.                                          00002190
           02 MDJOURC   PIC X.                                          00002200
           02 MDJOURP   PIC X.                                          00002210
           02 MDJOURH   PIC X.                                          00002220
           02 MDJOURV   PIC X.                                          00002230
           02 MDJOURO   PIC X(8).                                       00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MLVAR1A   PIC X.                                          00002260
           02 MLVAR1C   PIC X.                                          00002270
           02 MLVAR1P   PIC X.                                          00002280
           02 MLVAR1H   PIC X.                                          00002290
           02 MLVAR1V   PIC X.                                          00002300
           02 MLVAR1O   PIC X(9).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLVAR2A   PIC X.                                          00002330
           02 MLVAR2C   PIC X.                                          00002340
           02 MLVAR2P   PIC X.                                          00002350
           02 MLVAR2H   PIC X.                                          00002360
           02 MLVAR2V   PIC X.                                          00002370
           02 MLVAR2O   PIC X(5).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MLVAR3A   PIC X.                                          00002400
           02 MLVAR3C   PIC X.                                          00002410
           02 MLVAR3P   PIC X.                                          00002420
           02 MLVAR3H   PIC X.                                          00002430
           02 MLVAR3V   PIC X.                                          00002440
           02 MLVAR3O   PIC X(9).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MLVAR4A   PIC X.                                          00002470
           02 MLVAR4C   PIC X.                                          00002480
           02 MLVAR4P   PIC X.                                          00002490
           02 MLVAR4H   PIC X.                                          00002500
           02 MLVAR4V   PIC X.                                          00002510
           02 MLVAR4O   PIC X(5).                                       00002520
           02 MDETAILMUT1O OCCURS   13 TIMES .                          00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MNMUTA  PIC X.                                          00002550
             03 MNMUTC  PIC X.                                          00002560
             03 MNMUTP  PIC X.                                          00002570
             03 MNMUTH  PIC X.                                          00002580
             03 MNMUTV  PIC X.                                          00002590
             03 MNMUTO  PIC X(7).                                       00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MDATEOA      PIC X.                                     00002620
             03 MDATEOC PIC X.                                          00002630
             03 MDATEOP PIC X.                                          00002640
             03 MDATEOH PIC X.                                          00002650
             03 MDATEOV PIC X.                                          00002660
             03 MDATEOO      PIC X(4).                                  00002670
             03 FILLER       PIC X(2).                                  00002680
             03 MDATEFA      PIC X.                                     00002690
             03 MDATEFC PIC X.                                          00002700
             03 MDATEFP PIC X.                                          00002710
             03 MDATEFH PIC X.                                          00002720
             03 MDATEFV PIC X.                                          00002730
             03 MDATEFO      PIC X(4).                                  00002740
             03 FILLER       PIC X(2).                                  00002750
             03 MDATEDA      PIC X.                                     00002760
             03 MDATEDC PIC X.                                          00002770
             03 MDATEDP PIC X.                                          00002780
             03 MDATEDH PIC X.                                          00002790
             03 MDATEDV PIC X.                                          00002800
             03 MDATEDO      PIC X(4).                                  00002810
             03 FILLER       PIC X(2).                                  00002820
             03 MDATECA      PIC X.                                     00002830
             03 MDATECC PIC X.                                          00002840
             03 MDATECP PIC X.                                          00002850
             03 MDATECH PIC X.                                          00002860
             03 MDATECV PIC X.                                          00002870
             03 MDATECO      PIC X(4).                                  00002880
             03 FILLER       PIC X(2).                                  00002890
             03 MHDATECA     PIC X.                                     00002900
             03 MHDATECC     PIC X.                                     00002910
             03 MHDATECP     PIC X.                                     00002920
             03 MHDATECH     PIC X.                                     00002930
             03 MHDATECV     PIC X.                                     00002940
             03 MHDATECO     PIC X(5).                                  00002950
             03 FILLER       PIC X(2).                                  00002960
             03 MDATEMA      PIC X.                                     00002970
             03 MDATEMC PIC X.                                          00002980
             03 MDATEMP PIC X.                                          00002990
             03 MDATEMH PIC X.                                          00003000
             03 MDATEMV PIC X.                                          00003010
             03 MDATEMO      PIC X(4).                                  00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MDHHMUTA     PIC X.                                     00003040
             03 MDHHMUTC     PIC X.                                     00003050
             03 MDHHMUTP     PIC X.                                     00003060
             03 MDHHMUTH     PIC X.                                     00003070
             03 MDHHMUTV     PIC X.                                     00003080
             03 MDHHMUTO     PIC X(2).                                  00003090
             03 FILLER       PIC X(2).                                  00003100
             03 MDMMMUTA     PIC X.                                     00003110
             03 MDMMMUTC     PIC X.                                     00003120
             03 MDMMMUTP     PIC X.                                     00003130
             03 MDMMMUTH     PIC X.                                     00003140
             03 MDMMMUTV     PIC X.                                     00003150
             03 MDMMMUTO     PIC X(2).                                  00003160
             03 FILLER       PIC X(2).                                  00003170
             03 MNENTSOCMUTA      PIC X.                                00003180
             03 MNENTSOCMUTC PIC X.                                     00003190
             03 MNENTSOCMUTP PIC X.                                     00003200
             03 MNENTSOCMUTH PIC X.                                     00003210
             03 MNENTSOCMUTV PIC X.                                     00003220
             03 MNENTSOCMUTO      PIC X(3).                             00003230
             03 FILLER       PIC X(2).                                  00003240
             03 MNENTLIEUMUTA     PIC X.                                00003250
             03 MNENTLIEUMUTC     PIC X.                                00003260
             03 MNENTLIEUMUTP     PIC X.                                00003270
             03 MNENTLIEUMUTH     PIC X.                                00003280
             03 MNENTLIEUMUTV     PIC X.                                00003290
             03 MNENTLIEUMUTO     PIC X(3).                             00003300
           02 MDETAILMUT2O OCCURS   13 TIMES .                          00003310
             03 FILLER       PIC X(2).                                  00003320
             03 MVAR1A  PIC X.                                          00003330
             03 MVAR1C  PIC X.                                          00003340
             03 MVAR1P  PIC X.                                          00003350
             03 MVAR1H  PIC X.                                          00003360
             03 MVAR1V  PIC X.                                          00003370
             03 MVAR1O  PIC X(9).                                       00003380
           02 MDETAILMUT3O OCCURS   13 TIMES .                          00003390
             03 FILLER       PIC X(2).                                  00003400
             03 MQUOTAM3A    PIC X.                                     00003410
             03 MQUOTAM3C    PIC X.                                     00003420
             03 MQUOTAM3P    PIC X.                                     00003430
             03 MQUOTAM3H    PIC X.                                     00003440
             03 MQUOTAM3V    PIC X.                                     00003450
             03 MQUOTAM3O    PIC X(4).                                  00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MQUOTAPROA   PIC X.                                     00003480
             03 MQUOTAPROC   PIC X.                                     00003490
             03 MQUOTAPROP   PIC X.                                     00003500
             03 MQUOTAPROH   PIC X.                                     00003510
             03 MQUOTAPROV   PIC X.                                     00003520
             03 MQUOTAPROO   PIC X(4).                                  00003530
           02 MDETAILMUT4O OCCURS   13 TIMES .                          00003540
             03 FILLER       PIC X(2).                                  00003550
             03 MVAR2A  PIC X.                                          00003560
             03 MVAR2C  PIC X.                                          00003570
             03 MVAR2P  PIC X.                                          00003580
             03 MVAR2H  PIC X.                                          00003590
             03 MVAR2V  PIC X.                                          00003600
             03 MVAR2O  PIC X(5).                                       00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MZONCMDA  PIC X.                                          00003630
           02 MZONCMDC  PIC X.                                          00003640
           02 MZONCMDP  PIC X.                                          00003650
           02 MZONCMDH  PIC X.                                          00003660
           02 MZONCMDV  PIC X.                                          00003670
           02 MZONCMDO  PIC X(15).                                      00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MLIBERRA  PIC X.                                          00003700
           02 MLIBERRC  PIC X.                                          00003710
           02 MLIBERRP  PIC X.                                          00003720
           02 MLIBERRH  PIC X.                                          00003730
           02 MLIBERRV  PIC X.                                          00003740
           02 MLIBERRO  PIC X(58).                                      00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MCODTRAA  PIC X.                                          00003770
           02 MCODTRAC  PIC X.                                          00003780
           02 MCODTRAP  PIC X.                                          00003790
           02 MCODTRAH  PIC X.                                          00003800
           02 MCODTRAV  PIC X.                                          00003810
           02 MCODTRAO  PIC X(4).                                       00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MCICSA    PIC X.                                          00003840
           02 MCICSC    PIC X.                                          00003850
           02 MCICSP    PIC X.                                          00003860
           02 MCICSH    PIC X.                                          00003870
           02 MCICSV    PIC X.                                          00003880
           02 MCICSO    PIC X(5).                                       00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MNETNAMA  PIC X.                                          00003910
           02 MNETNAMC  PIC X.                                          00003920
           02 MNETNAMP  PIC X.                                          00003930
           02 MNETNAMH  PIC X.                                          00003940
           02 MNETNAMV  PIC X.                                          00003950
           02 MNETNAMO  PIC X(8).                                       00003960
           02 FILLER    PIC X(2).                                       00003970
           02 MSCREENA  PIC X.                                          00003980
           02 MSCREENC  PIC X.                                          00003990
           02 MSCREENP  PIC X.                                          00004000
           02 MSCREENH  PIC X.                                          00004010
           02 MSCREENV  PIC X.                                          00004020
           02 MSCREENO  PIC X(4).                                       00004030
                                                                                
