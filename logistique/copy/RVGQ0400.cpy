      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGQ0400                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGQ0400                         
      **********************************************************                
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGQ0400.                                                            
           02  GQ04-CMODDEL                                                     
               PIC X(0003).                                                     
           02  GQ04-CPERIMETRE                                                  
               PIC X(0005).                                                     
           02  GQ04-DEFFET                                                      
               PIC X(0008).                                                     
           02  GQ04-CFAM                                                        
               PIC X(0005).                                                     
           02  GQ04-CEQUIP1                                                     
               PIC X(0005).                                                     
           02  GQ04-CEQUIP2                                                     
               PIC X(0005).                                                     
           02  GQ04-CEQUIP3                                                     
               PIC X(0005).                                                     
           02  GQ04-CEQUIP4                                                     
               PIC X(0005).                                                     
           02  GQ04-CEQUIP5                                                     
               PIC X(0005).                                                     
           02  GQ04-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGQ0400                                  
      **********************************************************                
       01  RVGQ0400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ04-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ04-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ04-CPERIMETRE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ04-CPERIMETRE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ04-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ04-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ04-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ04-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ04-CEQUIP1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ04-CEQUIP1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ04-CEQUIP2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ04-CEQUIP2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ04-CEQUIP3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ04-CEQUIP3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ04-CEQUIP4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ04-CEQUIP4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ04-CEQUIP5-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ04-CEQUIP5-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ04-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GQ04-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
