      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGQ10   EGQ10                                              00000020
      ***************************************************************** 00000030
       01   EGQ10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MFONCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODEDEL1L    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCMODEDEL1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCMODEDEL1F    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCMODEDEL1I    PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFDELL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCPROFDELL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCPROFDELF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCPROFDELI     PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEQUILVR1L    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCEQUILVR1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCEQUILVR1F    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCEQUILVR1I    PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE1L   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDATE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE1F   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDATE1I   PIC X(6).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODEDEL2L    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCMODEDEL2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCMODEDEL2F    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMODEDEL2I    PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSOCL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCSOCF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCSOCI    PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIEUF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCLIEUI   PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEQUILVR2L    COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCEQUILVR2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCEQUILVR2F    PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCEQUILVR2I    PIC X(5).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE2L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDATE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE2F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDATE2I   PIC X(6).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIBERRI  PIC X(78).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCODTRAI  PIC X(4).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCICSI    PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNETNAMI  PIC X(8).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MSCREENI  PIC X(4).                                       00000770
      ***************************************************************** 00000780
      * SDF: EGQ10   EGQ10                                              00000790
      ***************************************************************** 00000800
       01   EGQ10O REDEFINES EGQ10I.                                    00000810
           02 FILLER    PIC X(12).                                      00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MDATJOUA  PIC X.                                          00000840
           02 MDATJOUC  PIC X.                                          00000850
           02 MDATJOUP  PIC X.                                          00000860
           02 MDATJOUH  PIC X.                                          00000870
           02 MDATJOUV  PIC X.                                          00000880
           02 MDATJOUO  PIC X(10).                                      00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MTIMJOUA  PIC X.                                          00000910
           02 MTIMJOUC  PIC X.                                          00000920
           02 MTIMJOUP  PIC X.                                          00000930
           02 MTIMJOUH  PIC X.                                          00000940
           02 MTIMJOUV  PIC X.                                          00000950
           02 MTIMJOUO  PIC X(5).                                       00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MZONCMDA  PIC X.                                          00000980
           02 MZONCMDC  PIC X.                                          00000990
           02 MZONCMDP  PIC X.                                          00001000
           02 MZONCMDH  PIC X.                                          00001010
           02 MZONCMDV  PIC X.                                          00001020
           02 MZONCMDO  PIC X(15).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MFONCA    PIC X.                                          00001050
           02 MFONCC    PIC X.                                          00001060
           02 MFONCP    PIC X.                                          00001070
           02 MFONCH    PIC X.                                          00001080
           02 MFONCV    PIC X.                                          00001090
           02 MFONCO    PIC X(3).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MCMODEDEL1A    PIC X.                                     00001120
           02 MCMODEDEL1C    PIC X.                                     00001130
           02 MCMODEDEL1P    PIC X.                                     00001140
           02 MCMODEDEL1H    PIC X.                                     00001150
           02 MCMODEDEL1V    PIC X.                                     00001160
           02 MCMODEDEL1O    PIC X(3).                                  00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MCPROFDELA     PIC X.                                     00001190
           02 MCPROFDELC     PIC X.                                     00001200
           02 MCPROFDELP     PIC X.                                     00001210
           02 MCPROFDELH     PIC X.                                     00001220
           02 MCPROFDELV     PIC X.                                     00001230
           02 MCPROFDELO     PIC X(5).                                  00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MCEQUILVR1A    PIC X.                                     00001260
           02 MCEQUILVR1C    PIC X.                                     00001270
           02 MCEQUILVR1P    PIC X.                                     00001280
           02 MCEQUILVR1H    PIC X.                                     00001290
           02 MCEQUILVR1V    PIC X.                                     00001300
           02 MCEQUILVR1O    PIC X(5).                                  00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATE1A   PIC X.                                          00001330
           02 MDATE1C   PIC X.                                          00001340
           02 MDATE1P   PIC X.                                          00001350
           02 MDATE1H   PIC X.                                          00001360
           02 MDATE1V   PIC X.                                          00001370
           02 MDATE1O   PIC X(6).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCMODEDEL2A    PIC X.                                     00001400
           02 MCMODEDEL2C    PIC X.                                     00001410
           02 MCMODEDEL2P    PIC X.                                     00001420
           02 MCMODEDEL2H    PIC X.                                     00001430
           02 MCMODEDEL2V    PIC X.                                     00001440
           02 MCMODEDEL2O    PIC X(3).                                  00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCSOCA    PIC X.                                          00001470
           02 MCSOCC    PIC X.                                          00001480
           02 MCSOCP    PIC X.                                          00001490
           02 MCSOCH    PIC X.                                          00001500
           02 MCSOCV    PIC X.                                          00001510
           02 MCSOCO    PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MCLIEUA   PIC X.                                          00001540
           02 MCLIEUC   PIC X.                                          00001550
           02 MCLIEUP   PIC X.                                          00001560
           02 MCLIEUH   PIC X.                                          00001570
           02 MCLIEUV   PIC X.                                          00001580
           02 MCLIEUO   PIC X(3).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MCEQUILVR2A    PIC X.                                     00001610
           02 MCEQUILVR2C    PIC X.                                     00001620
           02 MCEQUILVR2P    PIC X.                                     00001630
           02 MCEQUILVR2H    PIC X.                                     00001640
           02 MCEQUILVR2V    PIC X.                                     00001650
           02 MCEQUILVR2O    PIC X(5).                                  00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MDATE2A   PIC X.                                          00001680
           02 MDATE2C   PIC X.                                          00001690
           02 MDATE2P   PIC X.                                          00001700
           02 MDATE2H   PIC X.                                          00001710
           02 MDATE2V   PIC X.                                          00001720
           02 MDATE2O   PIC X(6).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MLIBERRA  PIC X.                                          00001750
           02 MLIBERRC  PIC X.                                          00001760
           02 MLIBERRP  PIC X.                                          00001770
           02 MLIBERRH  PIC X.                                          00001780
           02 MLIBERRV  PIC X.                                          00001790
           02 MLIBERRO  PIC X(78).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCODTRAA  PIC X.                                          00001820
           02 MCODTRAC  PIC X.                                          00001830
           02 MCODTRAP  PIC X.                                          00001840
           02 MCODTRAH  PIC X.                                          00001850
           02 MCODTRAV  PIC X.                                          00001860
           02 MCODTRAO  PIC X(4).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MCICSA    PIC X.                                          00001890
           02 MCICSC    PIC X.                                          00001900
           02 MCICSP    PIC X.                                          00001910
           02 MCICSH    PIC X.                                          00001920
           02 MCICSV    PIC X.                                          00001930
           02 MCICSO    PIC X(5).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MNETNAMA  PIC X.                                          00001960
           02 MNETNAMC  PIC X.                                          00001970
           02 MNETNAMP  PIC X.                                          00001980
           02 MNETNAMH  PIC X.                                          00001990
           02 MNETNAMV  PIC X.                                          00002000
           02 MNETNAMO  PIC X(8).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MSCREENA  PIC X.                                          00002030
           02 MSCREENC  PIC X.                                          00002040
           02 MSCREENP  PIC X.                                          00002050
           02 MSCREENH  PIC X.                                          00002060
           02 MSCREENV  PIC X.                                          00002070
           02 MSCREENO  PIC X(4).                                       00002080
                                                                                
