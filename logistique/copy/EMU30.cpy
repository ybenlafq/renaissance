      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU30   EMU30                                              00000020
      ***************************************************************** 00000030
       01   EMU30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCIETEI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENTRL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNSOCENTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCENTRF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNSOCENTRI     PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNDEPOTI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLDEPOTI  PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTATIONL    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNMUTATIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNMUTATIONF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNMUTATIONI    PIC X(7).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMUTATIONL    COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDMUTATIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDMUTATIONF    PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDMUTATIONI    PIC X(10).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELARTL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCSELARTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSELARTF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCSELARTI      PIC X(5).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAMIL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MFAMIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFAMIF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MFAMII    PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCODICI   PIC X(7).                                       00000610
           02 M5I OCCURS   11 TIMES .                                   00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCFAMI  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCMARQI      PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOURNL  COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLREFFOURNL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLREFFOURNF  PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLREFFOURNI  PIC X(20).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNCODICI     PIC X(7).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQDEMANDEEL  COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQDEMANDEEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQDEMANDEEF  PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQDEMANDEEI  PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMUTEEL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQMUTEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQMUTEEF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQMUTEEI     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLIEEL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQLIEEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQLIEEF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQLIEEI      PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MGESTPENUL   COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MGESTPENUL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MGESTPENUF   PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MGESTPENUI   PIC X.                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRESERVL    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MQRESERVL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQRESERVF    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQRESERVI    PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MZONCMDI  PIC X(12).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLIBERRI  PIC X(61).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCICSI    PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MNETNAMI  PIC X(8).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MSCREENI  PIC X(4).                                       00001220
      ***************************************************************** 00001230
      * SDF: EMU30   EMU30                                              00001240
      ***************************************************************** 00001250
       01   EMU30O REDEFINES EMU30I.                                    00001260
           02 FILLER    PIC X(12).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTIMJOUA  PIC X.                                          00001360
           02 MTIMJOUC  PIC X.                                          00001370
           02 MTIMJOUP  PIC X.                                          00001380
           02 MTIMJOUH  PIC X.                                          00001390
           02 MTIMJOUV  PIC X.                                          00001400
           02 MTIMJOUO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MPAGEA    PIC X.                                          00001430
           02 MPAGEC    PIC X.                                          00001440
           02 MPAGEP    PIC X.                                          00001450
           02 MPAGEH    PIC X.                                          00001460
           02 MPAGEV    PIC X.                                          00001470
           02 MPAGEO    PIC X(3).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNSOCIETEA     PIC X.                                     00001500
           02 MNSOCIETEC     PIC X.                                     00001510
           02 MNSOCIETEP     PIC X.                                     00001520
           02 MNSOCIETEH     PIC X.                                     00001530
           02 MNSOCIETEV     PIC X.                                     00001540
           02 MNSOCIETEO     PIC X(3).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNLIEUA   PIC X.                                          00001570
           02 MNLIEUC   PIC X.                                          00001580
           02 MNLIEUP   PIC X.                                          00001590
           02 MNLIEUH   PIC X.                                          00001600
           02 MNLIEUV   PIC X.                                          00001610
           02 MNLIEUO   PIC X(3).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLLIEUA   PIC X.                                          00001640
           02 MLLIEUC   PIC X.                                          00001650
           02 MLLIEUP   PIC X.                                          00001660
           02 MLLIEUH   PIC X.                                          00001670
           02 MLLIEUV   PIC X.                                          00001680
           02 MLLIEUO   PIC X(20).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNSOCENTRA     PIC X.                                     00001710
           02 MNSOCENTRC     PIC X.                                     00001720
           02 MNSOCENTRP     PIC X.                                     00001730
           02 MNSOCENTRH     PIC X.                                     00001740
           02 MNSOCENTRV     PIC X.                                     00001750
           02 MNSOCENTRO     PIC X(3).                                  00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNDEPOTA  PIC X.                                          00001780
           02 MNDEPOTC  PIC X.                                          00001790
           02 MNDEPOTP  PIC X.                                          00001800
           02 MNDEPOTH  PIC X.                                          00001810
           02 MNDEPOTV  PIC X.                                          00001820
           02 MNDEPOTO  PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLDEPOTA  PIC X.                                          00001850
           02 MLDEPOTC  PIC X.                                          00001860
           02 MLDEPOTP  PIC X.                                          00001870
           02 MLDEPOTH  PIC X.                                          00001880
           02 MLDEPOTV  PIC X.                                          00001890
           02 MLDEPOTO  PIC X(20).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MNMUTATIONA    PIC X.                                     00001920
           02 MNMUTATIONC    PIC X.                                     00001930
           02 MNMUTATIONP    PIC X.                                     00001940
           02 MNMUTATIONH    PIC X.                                     00001950
           02 MNMUTATIONV    PIC X.                                     00001960
           02 MNMUTATIONO    PIC X(7).                                  00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MDMUTATIONA    PIC X.                                     00001990
           02 MDMUTATIONC    PIC X.                                     00002000
           02 MDMUTATIONP    PIC X.                                     00002010
           02 MDMUTATIONH    PIC X.                                     00002020
           02 MDMUTATIONV    PIC X.                                     00002030
           02 MDMUTATIONO    PIC X(10).                                 00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCSELARTA      PIC X.                                     00002060
           02 MCSELARTC PIC X.                                          00002070
           02 MCSELARTP PIC X.                                          00002080
           02 MCSELARTH PIC X.                                          00002090
           02 MCSELARTV PIC X.                                          00002100
           02 MCSELARTO      PIC X(5).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MFAMIA    PIC X.                                          00002130
           02 MFAMIC    PIC X.                                          00002140
           02 MFAMIP    PIC X.                                          00002150
           02 MFAMIH    PIC X.                                          00002160
           02 MFAMIV    PIC X.                                          00002170
           02 MFAMIO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCODICA   PIC X.                                          00002200
           02 MCODICC   PIC X.                                          00002210
           02 MCODICP   PIC X.                                          00002220
           02 MCODICH   PIC X.                                          00002230
           02 MCODICV   PIC X.                                          00002240
           02 MCODICO   PIC X(7).                                       00002250
           02 M5O OCCURS   11 TIMES .                                   00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MCFAMA  PIC X.                                          00002280
             03 MCFAMC  PIC X.                                          00002290
             03 MCFAMP  PIC X.                                          00002300
             03 MCFAMH  PIC X.                                          00002310
             03 MCFAMV  PIC X.                                          00002320
             03 MCFAMO  PIC X(5).                                       00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MCMARQA      PIC X.                                     00002350
             03 MCMARQC PIC X.                                          00002360
             03 MCMARQP PIC X.                                          00002370
             03 MCMARQH PIC X.                                          00002380
             03 MCMARQV PIC X.                                          00002390
             03 MCMARQO      PIC X(5).                                  00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MLREFFOURNA  PIC X.                                     00002420
             03 MLREFFOURNC  PIC X.                                     00002430
             03 MLREFFOURNP  PIC X.                                     00002440
             03 MLREFFOURNH  PIC X.                                     00002450
             03 MLREFFOURNV  PIC X.                                     00002460
             03 MLREFFOURNO  PIC X(20).                                 00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MNCODICA     PIC X.                                     00002490
             03 MNCODICC     PIC X.                                     00002500
             03 MNCODICP     PIC X.                                     00002510
             03 MNCODICH     PIC X.                                     00002520
             03 MNCODICV     PIC X.                                     00002530
             03 MNCODICO     PIC X(7).                                  00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MQDEMANDEEA  PIC X.                                     00002560
             03 MQDEMANDEEC  PIC X.                                     00002570
             03 MQDEMANDEEP  PIC X.                                     00002580
             03 MQDEMANDEEH  PIC X.                                     00002590
             03 MQDEMANDEEV  PIC X.                                     00002600
             03 MQDEMANDEEO  PIC X(5).                                  00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MQMUTEEA     PIC X.                                     00002630
             03 MQMUTEEC     PIC X.                                     00002640
             03 MQMUTEEP     PIC X.                                     00002650
             03 MQMUTEEH     PIC X.                                     00002660
             03 MQMUTEEV     PIC X.                                     00002670
             03 MQMUTEEO     PIC X(5).                                  00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MQLIEEA      PIC X.                                     00002700
             03 MQLIEEC PIC X.                                          00002710
             03 MQLIEEP PIC X.                                          00002720
             03 MQLIEEH PIC X.                                          00002730
             03 MQLIEEV PIC X.                                          00002740
             03 MQLIEEO      PIC X(5).                                  00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MGESTPENUA   PIC X.                                     00002770
             03 MGESTPENUC   PIC X.                                     00002780
             03 MGESTPENUP   PIC X.                                     00002790
             03 MGESTPENUH   PIC X.                                     00002800
             03 MGESTPENUV   PIC X.                                     00002810
             03 MGESTPENUO   PIC X.                                     00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MQRESERVA    PIC X.                                     00002840
             03 MQRESERVC    PIC X.                                     00002850
             03 MQRESERVP    PIC X.                                     00002860
             03 MQRESERVH    PIC X.                                     00002870
             03 MQRESERVV    PIC X.                                     00002880
             03 MQRESERVO    PIC X(5).                                  00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MZONCMDA  PIC X.                                          00002910
           02 MZONCMDC  PIC X.                                          00002920
           02 MZONCMDP  PIC X.                                          00002930
           02 MZONCMDH  PIC X.                                          00002940
           02 MZONCMDV  PIC X.                                          00002950
           02 MZONCMDO  PIC X(12).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLIBERRA  PIC X.                                          00002980
           02 MLIBERRC  PIC X.                                          00002990
           02 MLIBERRP  PIC X.                                          00003000
           02 MLIBERRH  PIC X.                                          00003010
           02 MLIBERRV  PIC X.                                          00003020
           02 MLIBERRO  PIC X(61).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSO    PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNETNAMA  PIC X.                                          00003190
           02 MNETNAMC  PIC X.                                          00003200
           02 MNETNAMP  PIC X.                                          00003210
           02 MNETNAMH  PIC X.                                          00003220
           02 MNETNAMV  PIC X.                                          00003230
           02 MNETNAMO  PIC X(8).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSCREENA  PIC X.                                          00003260
           02 MSCREENC  PIC X.                                          00003270
           02 MSCREENP  PIC X.                                          00003280
           02 MSCREENH  PIC X.                                          00003290
           02 MSCREENV  PIC X.                                          00003300
           02 MSCREENO  PIC X(4).                                       00003310
                                                                                
