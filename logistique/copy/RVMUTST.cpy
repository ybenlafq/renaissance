      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MUTST FDSFDS                           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMUTST.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMUTST.                                                             
      *}                                                                        
           05  MUTST-CTABLEG2    PIC X(15).                                     
           05  MUTST-CTABLEG2-REDEF REDEFINES MUTST-CTABLEG2.                   
               10  MUTST-NSOCIETE        PIC X(03).                             
               10  MUTST-NLIEU           PIC X(03).                             
           05  MUTST-WTABLEG     PIC X(80).                                     
           05  MUTST-WTABLEG-REDEF  REDEFINES MUTST-WTABLEG.                    
               10  MUTST-ACTIF           PIC X(01).                             
               10  MUTST-NLIEUO          PIC X(03).                             
               10  MUTST-COPER           PIC X(10).                             
               10  MUTST-COMMENT         PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMUTST-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMUTST-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MUTST-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MUTST-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MUTST-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MUTST-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
