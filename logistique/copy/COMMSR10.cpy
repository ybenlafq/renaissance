      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00310000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-SR10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00320000
      *--                                                                       
       01  COM-SR10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00330000
       01  Z-COMMAREA.                                                  00340000
      *                                                                 00350000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00360000
          02 FILLER-COM-AIDA      PIC X(100).                           00370000
      *                                                                 00380000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00390000
          02 COMM-CICS-APPLID     PIC X(8).                             00400000
          02 COMM-CICS-NETNAM     PIC X(8).                             00410000
          02 COMM-CICS-TRANSA     PIC X(4).                             00420000
      *                                                                 00430000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00440000
          02 COMM-DATE-SIECLE     PIC XX.                               00450000
          02 COMM-DATE-ANNEE      PIC XX.                               00460000
          02 COMM-DATE-MOIS       PIC XX.                               00470000
          02 COMM-DATE-JOUR       PIC XX.                               00480000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00490000
          02 COMM-DATE-QNTA       PIC 999.                              00500000
          02 COMM-DATE-QNT0       PIC 99999.                            00510000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00520000
          02 COMM-DATE-BISX       PIC 9.                                00530000
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00540000
          02 COMM-DATE-JSM        PIC 9.                                00550000
      *   LIBELLES DU JOUR COURT - LONG                                 00560000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00570000
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00580000
      *   LIBELLES DU MOIS COURT - LONG                                 00590000
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00600000
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00610000
      *   DIFFERENTES FORMES DE DATE                                    00620000
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00630000
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00640000
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00650000
          02 COMM-DATE-JJMMAA     PIC X(6).                             00660000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00670000
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00680000
      *   DIFFERENTES FORMES DE DATE                                    00690000
          02 COMM-DATE-SEMSS      PIC X(02).                            00700000
          02 COMM-DATE-SEMAA      PIC X(02).                            00710000
          02 COMM-DATE-SEMNU      PIC X(02).                            00720000
          02 COMM-DATE-FILLER     PIC X(08).                            00730000
      *                                                                 00740000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00750000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00760000
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00770000
      *                                                                 00780000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00790000
      *                                                                 00800000
      *            TRANSACTION SR10 : ADMINISTRATION DES DONNEES      * 00810000
      *                                                                 00820000
          02 COMM-SR10-APPLI.                                           00820600
      *------------------------------ ZONE COMMUNE                      00820700
      *      03 COMM-SR10-FILLER         PIC X(3724).                   00820800
      *------------------------------ PAGINATION                        00822100
             03 COMM-SR10-ZONE-PAGINATION.                              00822200
                05 COMM-SR10-NPAGE       PIC 9(3) COMP-3.               00822300
                05 COMM-SR10-PAGE-MAX    PIC 9(3) COMP-3.               00822400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-SR10-NB-COMM     PIC S9(4) COMP.                00822500
      *--                                                                       
                05 COMM-SR10-NB-COMM     PIC S9(4) COMP-5.                      
      *}                                                                        
      *                                                                 00822600
      *------------------------------ GESTION TS                        00822700
             03 COMM-SR10-TS.                                           00822800
                05 CS-SR10-IDENT         PIC X(8).                      00822900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 CS-SR10-NB            PIC S9(4) COMP.                00823000
      *--                                                                       
                05 CS-SR10-NB            PIC S9(4) COMP-5.                      
      *}                                                                        
      *------------------------------ GESTION DES CONTROLES             00823100
             03 COMM-SR10-ZONE-CONTROLE PIC X.                          00823200
                88 COMM-SR10-NO-MAJ-ENCOURS  VALUE 'N'.                 00823300
                88 COMM-SR10-MAJ-ENCOURS     VALUE 'O'.                 00823400
      *------------------------------ GESTION MAP                       00823500
             03 COMM-SR10-ZONE-FILTRE.                                  00823600
                05 COMM-SR10-NCODIC-FILTRE     PIC X(7).                00823700
                05 COMM-SR10-LIEU-FILTRE       PIC X(3).                00823800
      *                                                                 00823900
             03 COMM-SR10-ZONE-MAP-NEW.                                 00824000
                05 COMM-SR10-LIEU              PIC X(3).                00824100
                05 COMM-SR10-NCODIC            PIC X(7).                00824200
                05 COMM-SR10-DATEJJDEB         PIC X(2).                00824300
                05 COMM-SR10-DATEMMDEB         PIC X(2).                00824400
                05 COMM-SR10-DATEAADEB         PIC X(4).                00824500
                05 COMM-SR10-DATEJJFIN         PIC X(2).                00824600
                05 COMM-SR10-DATEMMFIN         PIC X(2).                00824700
                05 COMM-SR10-DATEAAFIN         PIC X(4).                00824800
                05 COMM-SR10-LIEUGHE           PIC X(5).                00824900
                05 COMM-SR10-TRAIT             PIC X(5).                00825000
                05 COMM-SR10-TYPHS             PIC X(5).                00825100
                05 COMM-SR10-LIBRE             PIC X(10).               00825200
      *                                                                 00825300
             03 COMM-SR10-ZONE-MAP-TAB.                                 00825400
               04 COMM-SR10-LIGNES OCCURS 10.                           00825500
                05 COMM-SR10-DJJDEB            PIC X(2).                00825600
                05 COMM-SR10-DMMDEB            PIC X(2).                00825700
                05 COMM-SR10-DAADEB            PIC X(4).                00825800
                05 COMM-SR10-DJJFIN            PIC X(2).                00825900
                05 COMM-SR10-DMMFIN            PIC X(2).                00826000
                05 COMM-SR10-DAAFIN            PIC X(4).                00826100
                05 COMM-SR10-CLIEUGHE          PIC X(5).                00826200
                05 COMM-SR10-CTRAIT            PIC X(5).                00826300
                05 COMM-SR10-CTYPHS            PIC X(5).                00826400
                05 COMM-SR10-SLIBRE            PIC X(10).               00827400
      *                                                                 00827500
             03 COMM-SR10-NSOCIETE       PIC X(3).                      00827602
             03 COMM-SR10-FILLER         PIC X(3231).                   00827702
      ***************************************************************** 00828000
                                                                                
