      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA511 AU 31/05/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,02,BI,A,                          *        
      *                           09,03,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,07,BI,A,                          *        
      *                           27,03,BI,A,                          *        
      *                           30,03,PD,A,                          *        
      *                           33,05,BI,A,                          *        
      *                           38,07,BI,A,                          *        
      *                           45,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA511.                                                        
            05 NOMETAT-IVA511           PIC X(6) VALUE 'IVA511'.                
            05 RUPTURES-IVA511.                                                 
           10 IVA511-CSEQRAYON          PIC X(02).                      007  002
           10 IVA511-NSOCVALO           PIC X(03).                      009  003
           10 IVA511-NLIEUVALO          PIC X(03).                      012  003
           10 IVA511-NENTCDE            PIC X(05).                      015  005
           10 IVA511-NORIGINE           PIC X(07).                      020  007
           10 IVA511-CNATURE            PIC X(03).                      027  003
           10 IVA511-WSEQFAM            PIC S9(05)      COMP-3.         030  003
           10 IVA511-CMARQ              PIC X(05).                      033  005
           10 IVA511-NCODIC             PIC X(07).                      038  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA511-SEQUENCE           PIC S9(04) COMP.                045  002
      *--                                                                       
           10 IVA511-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA511.                                                   
           10 IVA511-CFAM               PIC X(05).                      047  005
           10 IVA511-CRAYON1            PIC X(05).                      052  005
           10 IVA511-DEVISE             PIC X(03).                      057  003
           10 IVA511-DOPER              PIC X(06).                      060  006
           10 IVA511-LENTCDE            PIC X(20).                      066  020
           10 IVA511-LIB1               PIC X(01).                      086  001
           10 IVA511-LIB11              PIC X(01).                      087  001
           10 IVA511-LIB2               PIC X(01).                      088  001
           10 IVA511-LIB21              PIC X(01).                      089  001
           10 IVA511-LIB3               PIC X(01).                      090  001
           10 IVA511-LIB31              PIC X(01).                      091  001
           10 IVA511-LLIEU              PIC X(20).                      092  020
           10 IVA511-LREFFOURN          PIC X(20).                      112  020
           10 IVA511-NSOCLIEUVALODEST   PIC X(06).                      132  006
           10 IVA511-NSOCLIEUVALOORIG   PIC X(06).                      138  006
           10 IVA511-PRA                PIC S9(07)V9(2) COMP-3.         144  005
           10 IVA511-PRMP               PIC S9(07)V9(6) COMP-3.         149  007
           10 IVA511-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         156  008
           10 IVA511-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         164  008
           10 IVA511-QSTOCKENTREE       PIC S9(11)      COMP-3.         172  006
           10 IVA511-QSTOCKSORTIE       PIC S9(11)      COMP-3.         178  006
           10 IVA511-TXEURO             PIC S9(01)V9(5) COMP-3.         184  004
            05 FILLER                      PIC X(325).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IVA511-LONG           PIC S9(4)   COMP  VALUE +187.           
      *                                                                         
      *--                                                                       
        01  DSECT-IVA511-LONG           PIC S9(4) COMP-5  VALUE +187.           
                                                                                
      *}                                                                        
