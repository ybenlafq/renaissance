      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-TLH2.                                                     00010000
           02 TSH2-NOM.                                                 00020000
               03 TSH2-NOM1       PIC X(04) VALUE 'TLH2'.               00030000
               03 TSH2-TERM       PIC X(04).                            00040000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     02 TSH2-LONG       PIC S9(04) COMP VALUE +62.               00050000
      *--                                                                       
            02 TSH2-LONG       PIC S9(04) COMP-5 VALUE +62.                     
      *}                                                                        
            02 TSH2-DONNEES.                                            00060000
               03 TSH2-NLIEUENT            PIC X(03).                   00070000
               03 TSH2-NHS                 PIC X(07).                   00070000
               03 TSH2-NCODIC              PIC X(07).                   00070100
               03 TSH2-NSOCDEST            PIC X(3).                    00180000
               03 TSH2-NLIEUDEST           PIC X(3).                    00190000
               03 TSH2-NSSLIEUDEST         PIC X(3).                    00200000
               03 TSH2-LCOMMENT.                                        00200010
                  04 TSH2-NSOCIETE         PIC X(3).                    00200020
                  04 TSH2-NLIEU            PIC X(3).                    00200030
                  04 TSH2-NVENTE           PIC X(7).                    00200040
                  04 TSH2-NSEQ             PIC X(2).                    00200050
                  04 TSH2-FIL1             PIC X(5).                    00200060
               03 TSH2-QHS                 PIC 9(5).                    00200100
               03 TSH2-CMVT                PIC X(1).                    00200100
               03 TSH2-CMOTIFENT           PIC X(10).                   00200100
                                                                                
                                                                        00260000
