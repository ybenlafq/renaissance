      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGQ01   EGQ01                                              00000020
      ***************************************************************** 00000030
       01   EFL06I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPMAXL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPMAXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPMAXF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPMAXI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFAMI    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLFAMI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODSTOCK1L   COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCMODSTOCK1L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCMODSTOCK1F   PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMODSTOCK1I   PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPRACK1L    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MQNBPRACK1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQNBPRACK1F    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MQNBPRACK1I    PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCONTENEUR1L  COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCCONTENEUR1L COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MCCONTENEUR1F  PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCCONTENEUR1I  PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSPECIFSTK1L  COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCSPECIFSTK1L COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MCSPECIFSTK1F  PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCSPECIFSTK1I  PIC X.                                     00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPVSOL1L    COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MQNBPVSOL1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQNBPVSOL1F    PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MQNBPVSOL1I    PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBRANMAIL1L  COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MQNBRANMAIL1L COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MQNBRANMAIL1F  PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MQNBRANMAIL1I  PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBNIVGERB1L  COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MQNBNIVGERB1L COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MQNBNIVGERB1F  PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MQNBNIVGERB1I  PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTPSUP1L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCTPSUP1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTPSUP1F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCTPSUP1I      PIC X.                                     00000610
           02 MTABLEI OCCURS   13 TIMES .                               00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCDEPOTL  COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNSOCDEPOTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNSOCDEPOTF  PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNSOCDEPOTI  PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEPOTL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNDEPOTF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNDEPOTI     PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIEUL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLLIEUF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLLIEUI      PIC X(20).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODSTOCKL  COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCMODSTOCKL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCMODSTOCKF  PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCMODSTOCKI  PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBPRACKL   COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQNBPRACKL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQNBPRACKF   PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQNBPRACKI   PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCONTENEURL      COMP PIC S9(4).                       00000830
      *--                                                                       
             03 MCCONTENEURL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MCCONTENEURF      PIC X.                                00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCCONTENEURI      PIC X.                                00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSPECIFSTKL      COMP PIC S9(4).                       00000870
      *--                                                                       
             03 MCSPECIFSTKL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MCSPECIFSTKF      PIC X.                                00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCSPECIFSTKI      PIC X.                                00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBPVSOLL   COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQNBPVSOLL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQNBPVSOLF   PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQNBPVSOLI   PIC X(3).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBRANMAILL      COMP PIC S9(4).                       00000950
      *--                                                                       
             03 MQNBRANMAILL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MQNBRANMAILF      PIC X.                                00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQNBRANMAILI      PIC X(3).                             00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBNIVGERBL      COMP PIC S9(4).                       00000990
      *--                                                                       
             03 MQNBNIVGERBL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MQNBNIVGERBF      PIC X.                                00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQNBNIVGERBI      PIC X(3).                             00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTPSUPL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MCTPSUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCTPSUPF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MCTPSUPI     PIC X.                                     00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(78).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * SDF: EGQ01   EGQ01                                              00001280
      ***************************************************************** 00001290
       01   EFL06O REDEFINES EFL06I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNPAGEA   PIC X.                                          00001470
           02 MNPAGEC   PIC X.                                          00001480
           02 MNPAGEP   PIC X.                                          00001490
           02 MNPAGEH   PIC X.                                          00001500
           02 MNPAGEV   PIC X.                                          00001510
           02 MNPAGEO   PIC ZZZ.                                        00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNPMAXA   PIC X.                                          00001540
           02 MNPMAXC   PIC X.                                          00001550
           02 MNPMAXP   PIC X.                                          00001560
           02 MNPMAXH   PIC X.                                          00001570
           02 MNPMAXV   PIC X.                                          00001580
           02 MNPMAXO   PIC ZZZ.                                        00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MCFAMA    PIC X.                                          00001610
           02 MCFAMC    PIC X.                                          00001620
           02 MCFAMP    PIC X.                                          00001630
           02 MCFAMH    PIC X.                                          00001640
           02 MCFAMV    PIC X.                                          00001650
           02 MCFAMO    PIC X(5).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLFAMA    PIC X.                                          00001680
           02 MLFAMC    PIC X.                                          00001690
           02 MLFAMP    PIC X.                                          00001700
           02 MLFAMH    PIC X.                                          00001710
           02 MLFAMV    PIC X.                                          00001720
           02 MLFAMO    PIC X(20).                                      00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCMODSTOCK1A   PIC X.                                     00001750
           02 MCMODSTOCK1C   PIC X.                                     00001760
           02 MCMODSTOCK1P   PIC X.                                     00001770
           02 MCMODSTOCK1H   PIC X.                                     00001780
           02 MCMODSTOCK1V   PIC X.                                     00001790
           02 MCMODSTOCK1O   PIC X(5).                                  00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MQNBPRACK1A    PIC X.                                     00001820
           02 MQNBPRACK1C    PIC X.                                     00001830
           02 MQNBPRACK1P    PIC X.                                     00001840
           02 MQNBPRACK1H    PIC X.                                     00001850
           02 MQNBPRACK1V    PIC X.                                     00001860
           02 MQNBPRACK1O    PIC X(5).                                  00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MCCONTENEUR1A  PIC X.                                     00001890
           02 MCCONTENEUR1C  PIC X.                                     00001900
           02 MCCONTENEUR1P  PIC X.                                     00001910
           02 MCCONTENEUR1H  PIC X.                                     00001920
           02 MCCONTENEUR1V  PIC X.                                     00001930
           02 MCCONTENEUR1O  PIC X.                                     00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MCSPECIFSTK1A  PIC X.                                     00001960
           02 MCSPECIFSTK1C  PIC X.                                     00001970
           02 MCSPECIFSTK1P  PIC X.                                     00001980
           02 MCSPECIFSTK1H  PIC X.                                     00001990
           02 MCSPECIFSTK1V  PIC X.                                     00002000
           02 MCSPECIFSTK1O  PIC X.                                     00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MQNBPVSOL1A    PIC X.                                     00002030
           02 MQNBPVSOL1C    PIC X.                                     00002040
           02 MQNBPVSOL1P    PIC X.                                     00002050
           02 MQNBPVSOL1H    PIC X.                                     00002060
           02 MQNBPVSOL1V    PIC X.                                     00002070
           02 MQNBPVSOL1O    PIC X(3).                                  00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MQNBRANMAIL1A  PIC X.                                     00002100
           02 MQNBRANMAIL1C  PIC X.                                     00002110
           02 MQNBRANMAIL1P  PIC X.                                     00002120
           02 MQNBRANMAIL1H  PIC X.                                     00002130
           02 MQNBRANMAIL1V  PIC X.                                     00002140
           02 MQNBRANMAIL1O  PIC X(3).                                  00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MQNBNIVGERB1A  PIC X.                                     00002170
           02 MQNBNIVGERB1C  PIC X.                                     00002180
           02 MQNBNIVGERB1P  PIC X.                                     00002190
           02 MQNBNIVGERB1H  PIC X.                                     00002200
           02 MQNBNIVGERB1V  PIC X.                                     00002210
           02 MQNBNIVGERB1O  PIC X(3).                                  00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCTPSUP1A      PIC X.                                     00002240
           02 MCTPSUP1C PIC X.                                          00002250
           02 MCTPSUP1P PIC X.                                          00002260
           02 MCTPSUP1H PIC X.                                          00002270
           02 MCTPSUP1V PIC X.                                          00002280
           02 MCTPSUP1O      PIC X.                                     00002290
           02 MTABLEO OCCURS   13 TIMES .                               00002300
             03 FILLER       PIC X(2).                                  00002310
             03 MNSOCDEPOTA  PIC X.                                     00002320
             03 MNSOCDEPOTC  PIC X.                                     00002330
             03 MNSOCDEPOTP  PIC X.                                     00002340
             03 MNSOCDEPOTH  PIC X.                                     00002350
             03 MNSOCDEPOTV  PIC X.                                     00002360
             03 MNSOCDEPOTO  PIC X(3).                                  00002370
             03 FILLER       PIC X(2).                                  00002380
             03 MNDEPOTA     PIC X.                                     00002390
             03 MNDEPOTC     PIC X.                                     00002400
             03 MNDEPOTP     PIC X.                                     00002410
             03 MNDEPOTH     PIC X.                                     00002420
             03 MNDEPOTV     PIC X.                                     00002430
             03 MNDEPOTO     PIC X(3).                                  00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MLLIEUA      PIC X.                                     00002460
             03 MLLIEUC PIC X.                                          00002470
             03 MLLIEUP PIC X.                                          00002480
             03 MLLIEUH PIC X.                                          00002490
             03 MLLIEUV PIC X.                                          00002500
             03 MLLIEUO      PIC X(20).                                 00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MCMODSTOCKA  PIC X.                                     00002530
             03 MCMODSTOCKC  PIC X.                                     00002540
             03 MCMODSTOCKP  PIC X.                                     00002550
             03 MCMODSTOCKH  PIC X.                                     00002560
             03 MCMODSTOCKV  PIC X.                                     00002570
             03 MCMODSTOCKO  PIC X(5).                                  00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MQNBPRACKA   PIC X.                                     00002600
             03 MQNBPRACKC   PIC X.                                     00002610
             03 MQNBPRACKP   PIC X.                                     00002620
             03 MQNBPRACKH   PIC X.                                     00002630
             03 MQNBPRACKV   PIC X.                                     00002640
             03 MQNBPRACKO   PIC ZZZZZ.                                 00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MCCONTENEURA      PIC X.                                00002670
             03 MCCONTENEURC PIC X.                                     00002680
             03 MCCONTENEURP PIC X.                                     00002690
             03 MCCONTENEURH PIC X.                                     00002700
             03 MCCONTENEURV PIC X.                                     00002710
             03 MCCONTENEURO      PIC X.                                00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MCSPECIFSTKA      PIC X.                                00002740
             03 MCSPECIFSTKC PIC X.                                     00002750
             03 MCSPECIFSTKP PIC X.                                     00002760
             03 MCSPECIFSTKH PIC X.                                     00002770
             03 MCSPECIFSTKV PIC X.                                     00002780
             03 MCSPECIFSTKO      PIC X.                                00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MQNBPVSOLA   PIC X.                                     00002810
             03 MQNBPVSOLC   PIC X.                                     00002820
             03 MQNBPVSOLP   PIC X.                                     00002830
             03 MQNBPVSOLH   PIC X.                                     00002840
             03 MQNBPVSOLV   PIC X.                                     00002850
             03 MQNBPVSOLO   PIC X(3).                                  00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MQNBRANMAILA      PIC X.                                00002880
             03 MQNBRANMAILC PIC X.                                     00002890
             03 MQNBRANMAILP PIC X.                                     00002900
             03 MQNBRANMAILH PIC X.                                     00002910
             03 MQNBRANMAILV PIC X.                                     00002920
             03 MQNBRANMAILO      PIC X(3).                             00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MQNBNIVGERBA      PIC X.                                00002950
             03 MQNBNIVGERBC PIC X.                                     00002960
             03 MQNBNIVGERBP PIC X.                                     00002970
             03 MQNBNIVGERBH PIC X.                                     00002980
             03 MQNBNIVGERBV PIC X.                                     00002990
             03 MQNBNIVGERBO      PIC X(3).                             00003000
             03 FILLER       PIC X(2).                                  00003010
             03 MCTPSUPA     PIC X.                                     00003020
             03 MCTPSUPC     PIC X.                                     00003030
             03 MCTPSUPP     PIC X.                                     00003040
             03 MCTPSUPH     PIC X.                                     00003050
             03 MCTPSUPV     PIC X.                                     00003060
             03 MCTPSUPO     PIC X.                                     00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(78).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
