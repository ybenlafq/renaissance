      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS04   EGS04                                              00000020
      ***************************************************************** 00000030
       01   EGS67I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCODICI   PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLCODICI  PIC X(20).                                      00000250
           02 MLIGNEI OCCURS   18 TIMES .                               00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREPGROUPL   COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MREPGROUPL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MREPGROUPF   PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MREPGROUPI   PIC X.                                     00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGROUPL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCGROUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCGROUPF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCGROUPI     PIC X(5).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLGROUPL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLGROUPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLGROUPF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLGROUPI     PIC X(20).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAML     COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MFAML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFAMF     PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MFAMI     PIC X(5).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MLFAMI    PIC X(20).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMARQUEL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MMARQUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMARQUEF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MMARQUEI  PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQUEL      COMP PIC S9(4).                            00000510
      *--                                                                       
           02 MLMARQUEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMARQUEF      PIC X.                                     00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MLMARQUEI      PIC X(20).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCODICL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MWCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWCODICF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MWCODICI  PIC X(7).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MSTATUTI  PIC X(3).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSAPPL      COMP PIC S9(4).                            00000630
      *--                                                                       
           02 MSENSAPPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSENSAPPF      PIC X.                                     00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MSENSAPPI      PIC X.                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSVTEL      COMP PIC S9(4).                            00000670
      *--                                                                       
           02 MSENSVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSENSVTEF      PIC X.                                     00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MSENSVTEI      PIC X.                                     00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCROSSDOCKL    COMP PIC S9(4).                            00000710
      *--                                                                       
           02 MCROSSDOCKL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCROSSDOCKF    PIC X.                                     00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCROSSDOCKI    PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(78).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: EGS04   EGS04                                              00000960
      ***************************************************************** 00000970
       01   EGS67O REDEFINES EGS67I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEA    PIC X.                                          00001150
           02 MPAGEC    PIC X.                                          00001160
           02 MPAGEP    PIC X.                                          00001170
           02 MPAGEH    PIC X.                                          00001180
           02 MPAGEV    PIC X.                                          00001190
           02 MPAGEO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MCODICA   PIC X.                                          00001220
           02 MCODICC   PIC X.                                          00001230
           02 MCODICP   PIC X.                                          00001240
           02 MCODICH   PIC X.                                          00001250
           02 MCODICV   PIC X.                                          00001260
           02 MCODICO   PIC X(7).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MLCODICA  PIC X.                                          00001290
           02 MLCODICC  PIC X.                                          00001300
           02 MLCODICP  PIC X.                                          00001310
           02 MLCODICH  PIC X.                                          00001320
           02 MLCODICV  PIC X.                                          00001330
           02 MLCODICO  PIC X(20).                                      00001340
           02 MLIGNEO OCCURS   18 TIMES .                               00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MREPGROUPA   PIC X.                                     00001370
             03 MREPGROUPC   PIC X.                                     00001380
             03 MREPGROUPP   PIC X.                                     00001390
             03 MREPGROUPH   PIC X.                                     00001400
             03 MREPGROUPV   PIC X.                                     00001410
             03 MREPGROUPO   PIC X.                                     00001420
             03 FILLER       PIC X(2).                                  00001430
             03 MCGROUPA     PIC X.                                     00001440
             03 MCGROUPC     PIC X.                                     00001450
             03 MCGROUPP     PIC X.                                     00001460
             03 MCGROUPH     PIC X.                                     00001470
             03 MCGROUPV     PIC X.                                     00001480
             03 MCGROUPO     PIC X(5).                                  00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MLGROUPA     PIC X.                                     00001510
             03 MLGROUPC     PIC X.                                     00001520
             03 MLGROUPP     PIC X.                                     00001530
             03 MLGROUPH     PIC X.                                     00001540
             03 MLGROUPV     PIC X.                                     00001550
             03 MLGROUPO     PIC X(20).                                 00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MFAMA     PIC X.                                          00001580
           02 MFAMC     PIC X.                                          00001590
           02 MFAMP     PIC X.                                          00001600
           02 MFAMH     PIC X.                                          00001610
           02 MFAMV     PIC X.                                          00001620
           02 MFAMO     PIC X(5).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLFAMA    PIC X.                                          00001650
           02 MLFAMC    PIC X.                                          00001660
           02 MLFAMP    PIC X.                                          00001670
           02 MLFAMH    PIC X.                                          00001680
           02 MLFAMV    PIC X.                                          00001690
           02 MLFAMO    PIC X(20).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MMARQUEA  PIC X.                                          00001720
           02 MMARQUEC  PIC X.                                          00001730
           02 MMARQUEP  PIC X.                                          00001740
           02 MMARQUEH  PIC X.                                          00001750
           02 MMARQUEV  PIC X.                                          00001760
           02 MMARQUEO  PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MLMARQUEA      PIC X.                                     00001790
           02 MLMARQUEC PIC X.                                          00001800
           02 MLMARQUEP PIC X.                                          00001810
           02 MLMARQUEH PIC X.                                          00001820
           02 MLMARQUEV PIC X.                                          00001830
           02 MLMARQUEO      PIC X(20).                                 00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MWCODICA  PIC X.                                          00001860
           02 MWCODICC  PIC X.                                          00001870
           02 MWCODICP  PIC X.                                          00001880
           02 MWCODICH  PIC X.                                          00001890
           02 MWCODICV  PIC X.                                          00001900
           02 MWCODICO  PIC X(7).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MSTATUTA  PIC X.                                          00001930
           02 MSTATUTC  PIC X.                                          00001940
           02 MSTATUTP  PIC X.                                          00001950
           02 MSTATUTH  PIC X.                                          00001960
           02 MSTATUTV  PIC X.                                          00001970
           02 MSTATUTO  PIC X(3).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MSENSAPPA      PIC X.                                     00002000
           02 MSENSAPPC PIC X.                                          00002010
           02 MSENSAPPP PIC X.                                          00002020
           02 MSENSAPPH PIC X.                                          00002030
           02 MSENSAPPV PIC X.                                          00002040
           02 MSENSAPPO      PIC X.                                     00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MSENSVTEA      PIC X.                                     00002070
           02 MSENSVTEC PIC X.                                          00002080
           02 MSENSVTEP PIC X.                                          00002090
           02 MSENSVTEH PIC X.                                          00002100
           02 MSENSVTEV PIC X.                                          00002110
           02 MSENSVTEO      PIC X.                                     00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCROSSDOCKA    PIC X.                                     00002140
           02 MCROSSDOCKC    PIC X.                                     00002150
           02 MCROSSDOCKP    PIC X.                                     00002160
           02 MCROSSDOCKH    PIC X.                                     00002170
           02 MCROSSDOCKV    PIC X.                                     00002180
           02 MCROSSDOCKO    PIC X.                                     00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(78).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
