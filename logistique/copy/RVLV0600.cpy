      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLV0600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLV0600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLV0600.                                                            
           10  LV06-NOP            PIC X(9).                                    
           10  LV06-NMUTATION      PIC X(7).                                    
           10  LV06-DSYST          PIC S9(13)V USAGE COMP-3.                    
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLV0600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLV0600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV06-NOP-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV06-NOP-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV06-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV06-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV06-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV06-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
