      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGB9500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB9500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB9500.                                                            
           02  GB95-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GB95-NCODIC                                                      
               PIC X(0007).                                                     
           02  GB95-QMUTEE                                                      
               PIC S9(8) COMP-3.                                                
           02  GB95-QNBREF                                                      
               PIC S9(8) COMP-3.                                                
           02  GB95-PUHT                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GB95-CTYPFACT                                                    
               PIC X(0005).                                                     
           02  GB95-POIDSGR                                                     
               PIC S9(9) COMP-3.                                                
           02  GB95-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGB9500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB9500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB95-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB95-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB95-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB95-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB95-QMUTEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB95-QMUTEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB95-QNBREF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB95-QNBREF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB95-PUHT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB95-PUHT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB95-CTYPFACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB95-CTYPFACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB95-POIDSGR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB95-POIDSGR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB95-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB95-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
