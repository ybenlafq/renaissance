      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFL5000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFL5000                         
      *---------------------------------------------------------                
      *                       NSOCDEPOT                                         
       01  RVFL5011.                                                            
      *                       NSOCDEPOT                                         
           10 FL50-NSOCDEPOT       PIC X(3).                                    
      *                       NDEPOT                                            
           10 FL50-NDEPOT          PIC X(3).                                    
      *                       NCODIC                                            
           10 FL50-NCODIC          PIC X(7).                                    
      *                       D1RECEPT                                          
           10 FL50-D1RECEPT        PIC X(8).                                    
      *                       CAPPRO                                            
           10 FL50-CAPPRO          PIC X(5).                                    
      *                       WSENSAPPRO                                        
           10 FL50-WSENSAPPRO      PIC X(1).                                    
      *                       QDELAIAPPRO                                       
           10 FL50-QDELAIAPPRO     PIC X(3).                                    
      *                       QCOLICDE                                          
           10 FL50-QCOLICDE        PIC S9(3)V USAGE COMP-3.                     
      *                       LEMBALLAGE                                        
           10 FL50-LEMBALLAGE      PIC X(50).                                   
      *                       QCOLIRECEPT                                       
           10 FL50-QCOLIRECEPT     PIC S9(5)V USAGE COMP-3.                     
      *                       QCOLIDSTOCK                                       
           10 FL50-QCOLIDSTOCK     PIC S9(5)V USAGE COMP-3.                     
      *                       CMODSTOCK1                                        
           10 FL50-CMODSTOCK1      PIC X(5).                                    
      *                       WMODSTOCK1                                        
           10 FL50-WMODSTOCK1      PIC X(1).                                    
      *                       CMODSTOCK2                                        
           10 FL50-CMODSTOCK2      PIC X(5).                                    
      *                       WMODSTOCK2                                        
           10 FL50-WMODSTOCK2      PIC X(1).                                    
      *                       CMODSTOCK3                                        
           10 FL50-CMODSTOCK3      PIC X(5).                                    
      *                       WMODSTOCK3                                        
           10 FL50-WMODSTOCK3      PIC X(1).                                    
      *                       CFETEMPL                                          
           10 FL50-CFETEMPL        PIC X(1).                                    
      *                       QNBRANMAIL                                        
           10 FL50-QNBRANMAIL      PIC S9(3)V USAGE COMP-3.                     
      *                       QNBNIVGERB                                        
           10 FL50-QNBNIVGERB      PIC S9(3)V USAGE COMP-3.                     
      *                       CZONEACCES                                        
           10 FL50-CZONEACCES      PIC X(1).                                    
      *                       CCONTENEUR                                        
           10 FL50-CCONTENEUR      PIC X(1).                                    
      *                       CSPECIFSTK                                        
           10 FL50-CSPECIFSTK      PIC X(1).                                    
      *                       CCOTEHOLD                                         
           10 FL50-CCOTEHOLD       PIC X(1).                                    
      *                       QNBPVSOL                                          
           10 FL50-QNBPVSOL        PIC S9(3)V USAGE COMP-3.                     
      *                       QNBPRACK                                          
           10 FL50-QNBPRACK        PIC S9(5)V USAGE COMP-3.                     
      *                       CTPSUP                                            
           10 FL50-CTPSUP          PIC X(1).                                    
      *                       WRESFOURN                                         
           10 FL50-WRESFOURN       PIC X(1).                                    
      *                       CQUOTA                                            
           10 FL50-CQUOTA          PIC X(5).                                    
      *                       CUSINE                                            
           10 FL50-CUSINE          PIC X(5).                                    
      *                       DMAJ                                              
           10 FL50-DMAJ            PIC X(8).                                    
      *                       CEXPO                                             
           10 FL50-CEXPO           PIC X(5).                                    
      *                       LSTATCOMP                                         
           10 FL50-LSTATCOMP       PIC X(3).                                    
      *                       WSTOCKAVANCE                                      
           10 FL50-WSTOCKAVANCE    PIC X(1).                                    
      *                       DSYST                                             
           10 FL50-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       CLASSE                                            
           10 FL50-CLASSE          PIC X(1).                                    
      *                       QCOUCHPAL                                         
           10 FL50-QCOUCHPAL       PIC S9(3)V USAGE COMP-3.                     
      *                       QCARTCOUCH                                        
           10 FL50-QCARTCOUCH      PIC S9(3)V USAGE COMP-3.                     
      *                       QBOXCART                                          
           10 FL50-QBOXCART        PIC S9(5)V USAGE COMP-3.                     
      *                       QPRODBOX                                          
           10 FL50-QPRODBOX        PIC S9(5)V USAGE COMP-3.                     
      *                       CUNITRECEPT                                       
           10 FL50-CUNITRECEPT     PIC X(3).                                    
      *                       QPRODCAM                                          
           10 FL50-QPRODCAM        PIC S9(5)V USAGE COMP-3.                     
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVFL5011-FLAGS.                                                      
      *                       NSOCDEPOT                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-NSOCDEPOT-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-NSOCDEPOT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       NDEPOT                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-NDEPOT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-NDEPOT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       NCODIC                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       D1RECEPT                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-D1RECEPT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-D1RECEPT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CAPPRO                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CAPPRO-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CAPPRO-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       WSENSAPPRO                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-WSENSAPPRO-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-WSENSAPPRO-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QDELAIAPPRO                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QDELAIAPPRO-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QDELAIAPPRO-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QCOLICDE                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QCOLICDE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QCOLICDE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       LEMBALLAGE                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-LEMBALLAGE-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-LEMBALLAGE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QCOLIRECEPT                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QCOLIRECEPT-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QCOLIRECEPT-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QCOLIDSTOCK                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QCOLIDSTOCK-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QCOLIDSTOCK-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CMODSTOCK1                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CMODSTOCK1-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CMODSTOCK1-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       WMODSTOCK1                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-WMODSTOCK1-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-WMODSTOCK1-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CMODSTOCK2                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CMODSTOCK2-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CMODSTOCK2-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       WMODSTOCK2                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-WMODSTOCK2-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-WMODSTOCK2-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CMODSTOCK3                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CMODSTOCK3-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CMODSTOCK3-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       WMODSTOCK3                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-WMODSTOCK3-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-WMODSTOCK3-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CFETEMPL                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CFETEMPL-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CFETEMPL-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QNBRANMAIL                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QNBRANMAIL-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QNBRANMAIL-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QNBNIVGERB                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QNBNIVGERB-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QNBNIVGERB-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CZONEACCES                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CZONEACCES-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CZONEACCES-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CCONTENEUR                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CCONTENEUR-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CCONTENEUR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CSPECIFSTK                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CSPECIFSTK-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CSPECIFSTK-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CCOTEHOLD                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CCOTEHOLD-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CCOTEHOLD-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QNBPVSOL                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QNBPVSOL-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QNBPVSOL-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QNBPRACK                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QNBPRACK-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QNBPRACK-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CTPSUP                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CTPSUP-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CTPSUP-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       WRESFOURN                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-WRESFOURN-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-WRESFOURN-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CQUOTA                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CQUOTA-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CQUOTA-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CUSINE                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CUSINE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CUSINE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       DMAJ                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-DMAJ-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-DMAJ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CEXPO                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CEXPO-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CEXPO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       LSTATCOMP                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-LSTATCOMP-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-LSTATCOMP-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       WSTOCKAVANCE                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-WSTOCKAVANCE-F  PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-WSTOCKAVANCE-F  PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       DSYST                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CLASSE                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CLASSE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CLASSE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QCOUCHPAL                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QCOUCHPAL-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QCOUCHPAL-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QCARTCOUCH                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QCARTCOUCH-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QCARTCOUCH-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QBOXCART                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QBOXCART-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QBOXCART-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QPRODBOX                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QPRODBOX-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QPRODBOX-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CUNITRECEPT                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-CUNITRECEPT-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-CUNITRECEPT-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QPRODBOX                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL50-QPRODCAM-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL50-QPRODCAM-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 44      *        
      ******************************************************************        
                                                                                
