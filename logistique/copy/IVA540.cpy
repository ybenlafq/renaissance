      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA540 AU 26/11/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,02,BI,A,                          *        
      *                           18,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA540.                                                        
            05 NOMETAT-IVA540           PIC X(6) VALUE 'IVA540'.                
            05 RUPTURES-IVA540.                                                 
           10 IVA540-NSOCVALO           PIC X(03).                      007  003
           10 IVA540-TLMELA             PIC X(03).                      010  003
           10 IVA540-NLIEUVALO          PIC X(03).                      013  003
           10 IVA540-BL-BR              PIC X(02).                      016  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA540-SEQUENCE           PIC S9(04) COMP.                018  002
      *--                                                                       
           10 IVA540-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA540.                                                   
           10 IVA540-CRAYON1            PIC X(05).                      020  005
           10 IVA540-DEVISE             PIC X(03).                      025  003
           10 IVA540-LIB1               PIC X(01).                      028  001
           10 IVA540-LIB11              PIC X(01).                      029  001
           10 IVA540-LIB2               PIC X(01).                      030  001
           10 IVA540-LIB21              PIC X(01).                      031  001
           10 IVA540-LIB4               PIC X(01).                      032  001
           10 IVA540-LIB41              PIC X(01).                      033  001
           10 IVA540-LLIEU              PIC X(20).                      034  020
           10 IVA540-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         054  008
           10 IVA540-PSTOCKFINAL        PIC S9(09)V9(6) COMP-3.         062  008
           10 IVA540-PSTOCKINIT         PIC S9(09)V9(6) COMP-3.         070  008
           10 IVA540-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         078  008
           10 IVA540-QSTOCKENTREE       PIC S9(11)      COMP-3.         086  006
           10 IVA540-QSTOCKFINAL        PIC S9(11)      COMP-3.         092  006
           10 IVA540-QSTOCKINIT         PIC S9(11)      COMP-3.         098  006
           10 IVA540-QSTOCKSORTIE       PIC S9(11)      COMP-3.         104  006
           10 IVA540-TXEURO             PIC S9(01)V9(5) COMP-3.         110  004
            05 FILLER                      PIC X(399).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IVA540-LONG           PIC S9(4)   COMP  VALUE +113.           
      *                                                                         
      *--                                                                       
        01  DSECT-IVA540-LONG           PIC S9(4) COMP-5  VALUE +113.           
                                                                                
      *}                                                                        
