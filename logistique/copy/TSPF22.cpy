      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *                   TS SPECIFIQUE  TPF22                       *          
      *   LONG : 369                                                 *          
      ****************************************************************          
       01 TS-PF22-DATA.                                                         
          05 TS-PF22-LIGNE   OCCURS  9.                                         
             10 TS-PF22-DCHARGT    PIC X(8).                                    
             10 TS-PF22-HCHARGT    PIC X(5).                                    
             10 TS-PF22-NMUT       PIC X(7).                                    
             10 TS-PF22-QVOLINIT   PIC S9(3)V9(6) COMP-3.                       
             10 TS-PF22-QVOLPRIS   PIC S9(3)V9(6) COMP-3.                       
             10 TS-PF22-QPINIT     PIC S9(5)      COMP-3.                       
             10 TS-PF22-QPPRIS     PIC S9(5)      COMP-3.                       
             10 TS-PF22-SELART     PIC X(5).                                    
                                                                                
