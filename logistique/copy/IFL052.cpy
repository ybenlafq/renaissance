      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IFL052 AU 26/11/2002  *        
      *                                                                *        
      *          CRITERES DE TRI  07,10,BI,A,                          *        
      *                           17,07,BI,A,                          *        
      *                           24,02,BI,A,                          *        
      *                           26,05,BI,A,                          *        
      *                           31,05,BI,A,                          *        
      *                           36,05,BI,A,                          *        
      *                           41,07,BI,A,                          *        
      *                           48,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IFL052.                                                        
            05 NOMETAT-IFL052           PIC X(6) VALUE 'IFL052'.                
            05 RUPTURES-IFL052.                                                 
           10 IFL052-CAPPRO-ASSOCIES    PIC X(10).                      007  010
           10 IFL052-RAYON              PIC X(07).                      017  007
           10 IFL052-WBLBR              PIC X(02).                      024  002
           10 IFL052-WSEQFAM            PIC 9(05).                      026  005
           10 IFL052-CFAM               PIC X(05).                      031  005
           10 IFL052-CMARQ              PIC X(05).                      036  005
           10 IFL052-NCODIC             PIC X(07).                      041  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IFL052-SEQUENCE           PIC S9(04) COMP.                048  002
      *--                                                                       
           10 IFL052-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IFL052.                                                   
           10 IFL052-CAPPRO1            PIC X(05).                      050  005
           10 IFL052-CAPPRO2            PIC X(05).                      055  005
           10 IFL052-CEXPO              PIC X(05).                      060  005
           10 IFL052-ENTREPOT1          PIC X(06).                      065  006
           10 IFL052-ENTREPOT2          PIC X(06).                      071  006
           10 IFL052-ENTREPOT3          PIC X(06).                      077  006
           10 IFL052-ENTREPOT4          PIC X(06).                      083  006
           10 IFL052-ENTREPOT5          PIC X(06).                      089  006
           10 IFL052-ENTREPOT6          PIC X(06).                      095  006
           10 IFL052-ENTREPOT7          PIC X(06).                      101  006
           10 IFL052-LFAM               PIC X(20).                      107  020
           10 IFL052-LREFFOURN          PIC X(20).                      127  020
           10 IFL052-LSTATCOMP          PIC X(03).                      147  003
           10 IFL052-NSOCIETE1          PIC X(03).                      150  003
           10 IFL052-NSOCIETE2          PIC X(03).                      153  003
           10 IFL052-WELATLM            PIC X(03).                      156  003
           10 IFL052-NB-NCODICS         PIC S9(01)      COMP-3.         159  001
            05 FILLER                      PIC X(353).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IFL052-LONG           PIC S9(4)   COMP  VALUE +159.           
      *                                                                         
      *--                                                                       
        01  DSECT-IFL052-LONG           PIC S9(4) COMP-5  VALUE +159.           
                                                                                
      *}                                                                        
