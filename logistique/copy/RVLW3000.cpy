      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLW3000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLW3000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW3000.                                                            
           02  LW30-NSOCDEPOT      PIC X(0003).                                 
           02  LW30-NDEPOT         PIC X(0003).                                 
           02  LW30-NCDE           PIC X(0007).                                 
           02  LW30-NCODIC         PIC X(0007).                                 
           02  LW30-DLIVRAISON     PIC X(0008).                                 
           02  LW30-NSEQ           PIC X(0003).                                 
           02  LW30-CACTION        PIC X(0001).                                 
           02  LW30-TYPCDE         PIC X(0002).                                 
           02  LW30-QCDE           PIC S9(09) COMP-3.                           
           02  LW30-DTRANSFERT     PIC X(0008).                                 
           02  LW30-CTRANSFERT     PIC X(0008).                                 
           02  LW30-QREC           PIC S9(09) COMP-3.                           
           02  LW30-DRECEPTION     PIC X(0008).                                 
           02  LW30-NENTCDE        PIC X(0007).                                 
           02  LW30-DSYST          PIC S9(13) COMP-3.                           
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLW3000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW3000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-DLIVRAISON-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-DLIVRAISON-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-CACTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-CACTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-TYPCDE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-TYPCDE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-QCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-QCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-DTRANSFERT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-DTRANSFERT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-CTRANSFERT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-CTRANSFERT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-QREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-QREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-DRECEPTION-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-DRECEPTION-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
