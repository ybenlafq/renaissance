      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TMU60                    TR: MU10  *    00020001
      *                         TMU61                              *    00050001
      *                         TMU62                              *    00050001
      *    SAISIE DES CONTREMARQUE POUR MUTATION MGC MGI           *    00060001
      *                                                            *    00070001
      * ZONES RESERVEES APPLICATIVES --------------------- 3519    *            
      *                                                            *    00080001
      **************************************************************            
      *   TRANSACTION MU61 : CONSULTATION ET SUPPRESSION DES       *            
      *                      CODIC CONTRE-MARQUE A MUTER           *            
      **************************************************************            
      *                                                                 00790001
          02 COMM-MU61-APPLI    REDEFINES COMM-MU10-FILLER.             00800001
      *                                                                 00810001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-MU61-NB-CODIC          PIC S9(04) COMP.           00880001
      *--                                                                       
              05 COMM-MU61-NB-CODIC          PIC S9(04) COMP-5.                 
      *}                                                                        
              05 COMM-MU61-NB-PAGE           PIC 9(03).                 00880001
              05 COMM-MU61-NO-PAGE           PIC 9(03).                 00880001
              05 COMM-MU61-TRAIT             PIC X(01).                 00880001
              05 COMM-MU61-LDEPOT            PIC X(20).                 00870001
              05 COMM-MU61-NZONPRIX          PIC X(02).                 00870001
              05 COMM-MU61-TABLE.                                       00180001
                 07 COMM-MU61-LIGNE          OCCURS 12.                         
                    09 COMM-MU61-CFAM        PIC X(05).                         
                    09 COMM-MU61-CMARQ       PIC X(05).                         
                    09 COMM-MU61-REF         PIC X(20).                         
                    09 COMM-MU61-NCODIC      PIC X(07).                         
                    09 COMM-MU61-QTE         PIC 9(04).                         
                    09 COMM-MU61-NDOC        PIC X(07).                         
                    09 COMM-MU61-DMUT.                                          
                       11 COMM-MU61-DMUTJJ   PIC X(02).                         
                       11 COMM-MU61-DMUTMM   PIC X(02).                         
                       11 COMM-MU61-DMUTAA   PIC X(02).                         
                    09 COMM-MU61-PVUN        PIC S9(06)V99 COMP-3.              
              05 COMM-MU61-TSUP.                                        00180001
                 07 COMM-MU61-LSUP           OCCURS 12.                         
                    09 COMM-MU61-SUP         PIC X.                             
              05 COMM-MU61-T-RNG.                                       00180001
                 07 COMM-MU61-TRNG           OCCURS 100.                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             09 COMM-MU61-RNG         PIC S9(4) COMP.                    
      *--                                                                       
                    09 COMM-MU61-RNG         PIC S9(4) COMP-5.                  
      *}                                                                        
      *                                                                 00970000
             05 COMM-MU61-FILLER         PIC X(2365).                   01000001
      *                                                                 01010000
      *****************************************************************         
                                                                                
