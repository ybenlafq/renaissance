      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PRG TGD50 (TGD00 -> MENU)    TR: GD50  *            
      *              VALIDATION D'UNE RAFALE                       *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3580          
      *                                                                         
      *        TRANSACTION GD50 : VALIDATION D'UNE RAFALE          *            
      *                                                                         
          03 COMM-GD50-APPLI REDEFINES COMM-GD00-FILLER.                        
      *------------------------------ ZONE DONNEES TGD50                        
             04 COMM-GD50-DONNEES-TGD50.                                        
                05  COMM-GD50-ATTR               PIC X(01) OCCURS 400.          
                05  COMM-GD50-PAGE-SUP           PIC 9(3).                      
                05  COMM-GD50-PAGE               PIC 9(3).                      
                05  COMM-GD50-QUEL-PASSAGE       PIC X.                         
                    88  PREMIER-PASSAGE          VALUE '0'.                     
                    88  PAS-PREMIER-PASSAGE      VALUE '1'.                     
                05  COMM-GD50-DJOUR              PIC X(8).                      
                05  COMM-GD50-JOUR1              PIC X(8).                      
                05  COMM-GD50-JOUR2              PIC X(8).                      
                05  COMM-GD50-JOUR3              PIC X(8).                      
                05  COMM-GD50-JOUR4              PIC X(8).                      
                05  COMM-GD50-JOUR5              PIC X(8).                      
                05  COMM-GD50-JOUR6              PIC X(8).                      
                05  COMM-GD50-RAF1               PIC X(3).                      
                05  COMM-GD50-RAF2               PIC X(3).                      
                05  COMM-GD50-RAF3               PIC X(3).                      
                05  COMM-GD50-RAF4               PIC X(3).                      
                05  COMM-GD50-RAF5               PIC X(3).                      
                05  COMM-GD50-RAF6               PIC X(3).                      
                05  COMM-GD50-TABLE.                                            
                    10  COMM-GD50-CNTL-SAISIE        OCCURS 100.                
                        15  COMM-GD50-CNTL-CODIC        PIC X(7).               
                        15  COMM-GD50-CNTL-EMP1         PIC X(2).               
                        15  COMM-GD50-CNTL-EMP2         PIC X(2).               
                        15  COMM-GD50-CNTL-EMP3         PIC X(3).               
                05  COMM-GD50-GEMPLACT           PIC X(1).                      
                05  COMM-GD50-FILLER             PIC X(43).                     
                                                                                
