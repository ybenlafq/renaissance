      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ETL80   ETL80                                              00000020
      ***************************************************************** 00000030
       01   ETL80I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBSOCL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLIBSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBSOCF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIBSOCI  PIC X(20).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLIVJJL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDLIVJJL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDLIVJJF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDLIVJJI  PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLIVMML  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDLIVMML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDLIVMMF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDLIVMMI  PIC X(2).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLIVAAL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDLIVAAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDLIVAAF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDLIVAAI  PIC X(2).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOPBLIVL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MTOPBLIVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOPBLIVF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MTOPBLIVI      PIC X.                                     00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOPBREPL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MTOPBREPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOPBREPF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTOPBREPI      PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDIMPRL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCDIMPRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCDIMPRF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCDIMPRI  PIC X(4).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNMAGI    PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNVENTEI  PIC X(7).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLIBERRI  PIC X(78).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCODTRAI  PIC X(4).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCICSI    PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNETNAMI  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSCREENI  PIC X(4).                                       00000730
      ***************************************************************** 00000740
      * SDF: ETL80   ETL80                                              00000750
      ***************************************************************** 00000760
       01   ETL80O REDEFINES ETL80I.                                    00000770
           02 FILLER    PIC X(12).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MDATJOUA  PIC X.                                          00000800
           02 MDATJOUC  PIC X.                                          00000810
           02 MDATJOUP  PIC X.                                          00000820
           02 MDATJOUH  PIC X.                                          00000830
           02 MDATJOUV  PIC X.                                          00000840
           02 MDATJOUO  PIC X(10).                                      00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MTIMJOUA  PIC X.                                          00000870
           02 MTIMJOUC  PIC X.                                          00000880
           02 MTIMJOUP  PIC X.                                          00000890
           02 MTIMJOUH  PIC X.                                          00000900
           02 MTIMJOUV  PIC X.                                          00000910
           02 MTIMJOUO  PIC X(5).                                       00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MNSOCA    PIC X.                                          00000940
           02 MNSOCC    PIC X.                                          00000950
           02 MNSOCP    PIC X.                                          00000960
           02 MNSOCH    PIC X.                                          00000970
           02 MNSOCV    PIC X.                                          00000980
           02 MNSOCO    PIC X(3).                                       00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MLIBSOCA  PIC X.                                          00001010
           02 MLIBSOCC  PIC X.                                          00001020
           02 MLIBSOCP  PIC X.                                          00001030
           02 MLIBSOCH  PIC X.                                          00001040
           02 MLIBSOCV  PIC X.                                          00001050
           02 MLIBSOCO  PIC X(20).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MDLIVJJA  PIC X.                                          00001080
           02 MDLIVJJC  PIC X.                                          00001090
           02 MDLIVJJP  PIC X.                                          00001100
           02 MDLIVJJH  PIC X.                                          00001110
           02 MDLIVJJV  PIC X.                                          00001120
           02 MDLIVJJO  PIC X(2).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MDLIVMMA  PIC X.                                          00001150
           02 MDLIVMMC  PIC X.                                          00001160
           02 MDLIVMMP  PIC X.                                          00001170
           02 MDLIVMMH  PIC X.                                          00001180
           02 MDLIVMMV  PIC X.                                          00001190
           02 MDLIVMMO  PIC X(2).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MDLIVAAA  PIC X.                                          00001220
           02 MDLIVAAC  PIC X.                                          00001230
           02 MDLIVAAP  PIC X.                                          00001240
           02 MDLIVAAH  PIC X.                                          00001250
           02 MDLIVAAV  PIC X.                                          00001260
           02 MDLIVAAO  PIC X(2).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MTOPBLIVA      PIC X.                                     00001290
           02 MTOPBLIVC PIC X.                                          00001300
           02 MTOPBLIVP PIC X.                                          00001310
           02 MTOPBLIVH PIC X.                                          00001320
           02 MTOPBLIVV PIC X.                                          00001330
           02 MTOPBLIVO      PIC X.                                     00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTOPBREPA      PIC X.                                     00001360
           02 MTOPBREPC PIC X.                                          00001370
           02 MTOPBREPP PIC X.                                          00001380
           02 MTOPBREPH PIC X.                                          00001390
           02 MTOPBREPV PIC X.                                          00001400
           02 MTOPBREPO      PIC X.                                     00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCDIMPRA  PIC X.                                          00001430
           02 MCDIMPRC  PIC X.                                          00001440
           02 MCDIMPRP  PIC X.                                          00001450
           02 MCDIMPRH  PIC X.                                          00001460
           02 MCDIMPRV  PIC X.                                          00001470
           02 MCDIMPRO  PIC X(4).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNMAGA    PIC X.                                          00001500
           02 MNMAGC    PIC X.                                          00001510
           02 MNMAGP    PIC X.                                          00001520
           02 MNMAGH    PIC X.                                          00001530
           02 MNMAGV    PIC X.                                          00001540
           02 MNMAGO    PIC X(3).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNVENTEA  PIC X.                                          00001570
           02 MNVENTEC  PIC X.                                          00001580
           02 MNVENTEP  PIC X.                                          00001590
           02 MNVENTEH  PIC X.                                          00001600
           02 MNVENTEV  PIC X.                                          00001610
           02 MNVENTEO  PIC X(7).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLIBERRA  PIC X.                                          00001640
           02 MLIBERRC  PIC X.                                          00001650
           02 MLIBERRP  PIC X.                                          00001660
           02 MLIBERRH  PIC X.                                          00001670
           02 MLIBERRV  PIC X.                                          00001680
           02 MLIBERRO  PIC X(78).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCODTRAA  PIC X.                                          00001710
           02 MCODTRAC  PIC X.                                          00001720
           02 MCODTRAP  PIC X.                                          00001730
           02 MCODTRAH  PIC X.                                          00001740
           02 MCODTRAV  PIC X.                                          00001750
           02 MCODTRAO  PIC X(4).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCICSA    PIC X.                                          00001780
           02 MCICSC    PIC X.                                          00001790
           02 MCICSP    PIC X.                                          00001800
           02 MCICSH    PIC X.                                          00001810
           02 MCICSV    PIC X.                                          00001820
           02 MCICSO    PIC X(5).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNETNAMA  PIC X.                                          00001850
           02 MNETNAMC  PIC X.                                          00001860
           02 MNETNAMP  PIC X.                                          00001870
           02 MNETNAMH  PIC X.                                          00001880
           02 MNETNAMV  PIC X.                                          00001890
           02 MNETNAMO  PIC X(8).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MSCREENA  PIC X.                                          00001920
           02 MSCREENC  PIC X.                                          00001930
           02 MSCREENP  PIC X.                                          00001940
           02 MSCREENH  PIC X.                                          00001950
           02 MSCREENV  PIC X.                                          00001960
           02 MSCREENO  PIC X(4).                                       00001970
                                                                                
