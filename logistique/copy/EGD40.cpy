      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD40   EGD40                                              00000020
      ***************************************************************** 00000030
       01   EGD40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDPL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCDPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCDPF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCDPI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNDEPOTI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODSTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCMODSTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMODSTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCMODSTI  PIC X(5).                                       00000290
           02 MLIGNEI OCCURS   13 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MSELI   PIC X(2).                                       00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCZONEDL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCZONEDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCZONEDF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCZONEDI     PIC X(2).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNIVD1L     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCNIVD1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCNIVD1F     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCNIVD1I     PIC X(2).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNIVF1L     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCNIVF1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCNIVF1F     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCNIVF1I     PIC X(2).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNPOSD1L     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNPOSD1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNPOSD1F     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNPOSD1I     PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNPOSF1L     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNPOSF1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNPOSF1F     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNPOSF1I     PIC X(3).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCZONEFL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCZONEFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCZONEFF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCZONEFI     PIC X(2).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNIVD2L     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCNIVD2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCNIVD2F     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCNIVD2I     PIC X(2).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNIVF2L     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCNIVF2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCNIVF2F     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCNIVF2I     PIC X(2).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNPOSD2L     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNPOSD2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNPOSD2F     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNPOSD2I     PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNPOSF2L     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNPOSF2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNPOSF2F     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNPOSF2I     PIC X(3).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRUPTL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCRUPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCRUPTF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCRUPTI      PIC X.                                     00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(15).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(58).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: EGD40   EGD40                                              00001040
      ***************************************************************** 00001050
       01   EGD40O REDEFINES EGD40I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MPAGEA    PIC X.                                          00001230
           02 MPAGEC    PIC X.                                          00001240
           02 MPAGEP    PIC X.                                          00001250
           02 MPAGEH    PIC X.                                          00001260
           02 MPAGEV    PIC X.                                          00001270
           02 MPAGEO    PIC X(3).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNSOCDPA  PIC X.                                          00001300
           02 MNSOCDPC  PIC X.                                          00001310
           02 MNSOCDPP  PIC X.                                          00001320
           02 MNSOCDPH  PIC X.                                          00001330
           02 MNSOCDPV  PIC X.                                          00001340
           02 MNSOCDPO  PIC X(3).                                       00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MNDEPOTA  PIC X.                                          00001370
           02 MNDEPOTC  PIC X.                                          00001380
           02 MNDEPOTP  PIC X.                                          00001390
           02 MNDEPOTH  PIC X.                                          00001400
           02 MNDEPOTV  PIC X.                                          00001410
           02 MNDEPOTO  PIC X(3).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MCMODSTA  PIC X.                                          00001440
           02 MCMODSTC  PIC X.                                          00001450
           02 MCMODSTP  PIC X.                                          00001460
           02 MCMODSTH  PIC X.                                          00001470
           02 MCMODSTV  PIC X.                                          00001480
           02 MCMODSTO  PIC X(5).                                       00001490
           02 MLIGNEO OCCURS   13 TIMES .                               00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MSELA   PIC X.                                          00001520
             03 MSELC   PIC X.                                          00001530
             03 MSELP   PIC X.                                          00001540
             03 MSELH   PIC X.                                          00001550
             03 MSELV   PIC X.                                          00001560
             03 MSELO   PIC X(2).                                       00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MCZONEDA     PIC X.                                     00001590
             03 MCZONEDC     PIC X.                                     00001600
             03 MCZONEDP     PIC X.                                     00001610
             03 MCZONEDH     PIC X.                                     00001620
             03 MCZONEDV     PIC X.                                     00001630
             03 MCZONEDO     PIC X(2).                                  00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MCNIVD1A     PIC X.                                     00001660
             03 MCNIVD1C     PIC X.                                     00001670
             03 MCNIVD1P     PIC X.                                     00001680
             03 MCNIVD1H     PIC X.                                     00001690
             03 MCNIVD1V     PIC X.                                     00001700
             03 MCNIVD1O     PIC X(2).                                  00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MCNIVF1A     PIC X.                                     00001730
             03 MCNIVF1C     PIC X.                                     00001740
             03 MCNIVF1P     PIC X.                                     00001750
             03 MCNIVF1H     PIC X.                                     00001760
             03 MCNIVF1V     PIC X.                                     00001770
             03 MCNIVF1O     PIC X(2).                                  00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MNPOSD1A     PIC X.                                     00001800
             03 MNPOSD1C     PIC X.                                     00001810
             03 MNPOSD1P     PIC X.                                     00001820
             03 MNPOSD1H     PIC X.                                     00001830
             03 MNPOSD1V     PIC X.                                     00001840
             03 MNPOSD1O     PIC X(3).                                  00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MNPOSF1A     PIC X.                                     00001870
             03 MNPOSF1C     PIC X.                                     00001880
             03 MNPOSF1P     PIC X.                                     00001890
             03 MNPOSF1H     PIC X.                                     00001900
             03 MNPOSF1V     PIC X.                                     00001910
             03 MNPOSF1O     PIC X(3).                                  00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MCZONEFA     PIC X.                                     00001940
             03 MCZONEFC     PIC X.                                     00001950
             03 MCZONEFP     PIC X.                                     00001960
             03 MCZONEFH     PIC X.                                     00001970
             03 MCZONEFV     PIC X.                                     00001980
             03 MCZONEFO     PIC X(2).                                  00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MCNIVD2A     PIC X.                                     00002010
             03 MCNIVD2C     PIC X.                                     00002020
             03 MCNIVD2P     PIC X.                                     00002030
             03 MCNIVD2H     PIC X.                                     00002040
             03 MCNIVD2V     PIC X.                                     00002050
             03 MCNIVD2O     PIC X(2).                                  00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MCNIVF2A     PIC X.                                     00002080
             03 MCNIVF2C     PIC X.                                     00002090
             03 MCNIVF2P     PIC X.                                     00002100
             03 MCNIVF2H     PIC X.                                     00002110
             03 MCNIVF2V     PIC X.                                     00002120
             03 MCNIVF2O     PIC X(2).                                  00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MNPOSD2A     PIC X.                                     00002150
             03 MNPOSD2C     PIC X.                                     00002160
             03 MNPOSD2P     PIC X.                                     00002170
             03 MNPOSD2H     PIC X.                                     00002180
             03 MNPOSD2V     PIC X.                                     00002190
             03 MNPOSD2O     PIC X(3).                                  00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MNPOSF2A     PIC X.                                     00002220
             03 MNPOSF2C     PIC X.                                     00002230
             03 MNPOSF2P     PIC X.                                     00002240
             03 MNPOSF2H     PIC X.                                     00002250
             03 MNPOSF2V     PIC X.                                     00002260
             03 MNPOSF2O     PIC X(3).                                  00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MCRUPTA      PIC X.                                     00002290
             03 MCRUPTC PIC X.                                          00002300
             03 MCRUPTP PIC X.                                          00002310
             03 MCRUPTH PIC X.                                          00002320
             03 MCRUPTV PIC X.                                          00002330
             03 MCRUPTO      PIC X.                                     00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MZONCMDA  PIC X.                                          00002360
           02 MZONCMDC  PIC X.                                          00002370
           02 MZONCMDP  PIC X.                                          00002380
           02 MZONCMDH  PIC X.                                          00002390
           02 MZONCMDV  PIC X.                                          00002400
           02 MZONCMDO  PIC X(15).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(58).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
