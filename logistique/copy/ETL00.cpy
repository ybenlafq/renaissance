      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ETL00   ETL00                                              00000020
      ***************************************************************** 00000030
       01   ETL00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR1L   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MJOUR1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR1F   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MJOUR1I   PIC X(6).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR2L   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MJOUR2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR2F   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MJOUR2I   PIC X(6).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR3L   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MJOUR3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR3F   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MJOUR3I   PIC X(6).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPT40L   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MOPT40L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MOPT40F   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MOPT40I   PIC X(35).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPROFILL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPROFILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPROFILF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPROFILI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOURNEEL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MTOURNEEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOURNEEF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTOURNEEI      PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDATEI    PIC X(6).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNLIEUI   PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNVENTEI  PIC X(7).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMP1L    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MIMP1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MIMP1F    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MIMP1I    PIC X(4).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR4L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MJOUR4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR4F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MJOUR4I   PIC X(6).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMP2L    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MIMP2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MIMP2F    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MIMP2I    PIC X(4).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAGL     COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMAGF     PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MMAGI     PIC X(3).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTE2L      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MNVENTE2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNVENTE2F      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNVENTE2I      PIC X(7).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR5L   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MJOUR5L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR5F   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MJOUR5I   PIC X(6).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDELIVL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MDDELIVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDELIVF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MDDELIVI  PIC X(10).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTPRESTL      COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MCTPRESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTPRESTF      PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCTPRESTI      PIC X(5).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCPOSTALI      PIC X(5).                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLIBERRI  PIC X(78).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCODTRAI  PIC X(4).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCICSI    PIC X(5).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNETNAMI  PIC X(8).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MSCREENI  PIC X(4).                                       00001170
      ***************************************************************** 00001180
      * SDF: ETL00   ETL00                                              00001190
      ***************************************************************** 00001200
       01   ETL00O REDEFINES ETL00I.                                    00001210
           02 FILLER    PIC X(12).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MDATJOUA  PIC X.                                          00001240
           02 MDATJOUC  PIC X.                                          00001250
           02 MDATJOUP  PIC X.                                          00001260
           02 MDATJOUH  PIC X.                                          00001270
           02 MDATJOUV  PIC X.                                          00001280
           02 MDATJOUO  PIC X(10).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MTIMJOUA  PIC X.                                          00001310
           02 MTIMJOUC  PIC X.                                          00001320
           02 MTIMJOUP  PIC X.                                          00001330
           02 MTIMJOUH  PIC X.                                          00001340
           02 MTIMJOUV  PIC X.                                          00001350
           02 MTIMJOUO  PIC X(5).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MZONCMDA  PIC X.                                          00001380
           02 MZONCMDC  PIC X.                                          00001390
           02 MZONCMDP  PIC X.                                          00001400
           02 MZONCMDH  PIC X.                                          00001410
           02 MZONCMDV  PIC X.                                          00001420
           02 MZONCMDO  PIC X(15).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MWFONCA   PIC X.                                          00001450
           02 MWFONCC   PIC X.                                          00001460
           02 MWFONCP   PIC X.                                          00001470
           02 MWFONCH   PIC X.                                          00001480
           02 MWFONCV   PIC X.                                          00001490
           02 MWFONCO   PIC X(3).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNSOCA    PIC X.                                          00001520
           02 MNSOCC    PIC X.                                          00001530
           02 MNSOCP    PIC X.                                          00001540
           02 MNSOCH    PIC X.                                          00001550
           02 MNSOCV    PIC X.                                          00001560
           02 MNSOCO    PIC X(3).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MJOUR1A   PIC X.                                          00001590
           02 MJOUR1C   PIC X.                                          00001600
           02 MJOUR1P   PIC X.                                          00001610
           02 MJOUR1H   PIC X.                                          00001620
           02 MJOUR1V   PIC X.                                          00001630
           02 MJOUR1O   PIC X(6).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MJOUR2A   PIC X.                                          00001660
           02 MJOUR2C   PIC X.                                          00001670
           02 MJOUR2P   PIC X.                                          00001680
           02 MJOUR2H   PIC X.                                          00001690
           02 MJOUR2V   PIC X.                                          00001700
           02 MJOUR2O   PIC X(6).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MJOUR3A   PIC X.                                          00001730
           02 MJOUR3C   PIC X.                                          00001740
           02 MJOUR3P   PIC X.                                          00001750
           02 MJOUR3H   PIC X.                                          00001760
           02 MJOUR3V   PIC X.                                          00001770
           02 MJOUR3O   PIC X(6).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MOPT40A   PIC X.                                          00001800
           02 MOPT40C   PIC X.                                          00001810
           02 MOPT40P   PIC X.                                          00001820
           02 MOPT40H   PIC X.                                          00001830
           02 MOPT40V   PIC X.                                          00001840
           02 MOPT40O   PIC X(35).                                      00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MPROFILA  PIC X.                                          00001870
           02 MPROFILC  PIC X.                                          00001880
           02 MPROFILP  PIC X.                                          00001890
           02 MPROFILH  PIC X.                                          00001900
           02 MPROFILV  PIC X.                                          00001910
           02 MPROFILO  PIC X(5).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MTOURNEEA      PIC X.                                     00001940
           02 MTOURNEEC PIC X.                                          00001950
           02 MTOURNEEP PIC X.                                          00001960
           02 MTOURNEEH PIC X.                                          00001970
           02 MTOURNEEV PIC X.                                          00001980
           02 MTOURNEEO      PIC X(3).                                  00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MDATEA    PIC X.                                          00002010
           02 MDATEC    PIC X.                                          00002020
           02 MDATEP    PIC X.                                          00002030
           02 MDATEH    PIC X.                                          00002040
           02 MDATEV    PIC X.                                          00002050
           02 MDATEO    PIC X(6).                                       00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MNLIEUA   PIC X.                                          00002080
           02 MNLIEUC   PIC X.                                          00002090
           02 MNLIEUP   PIC X.                                          00002100
           02 MNLIEUH   PIC X.                                          00002110
           02 MNLIEUV   PIC X.                                          00002120
           02 MNLIEUO   PIC X(3).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MNVENTEA  PIC X.                                          00002150
           02 MNVENTEC  PIC X.                                          00002160
           02 MNVENTEP  PIC X.                                          00002170
           02 MNVENTEH  PIC X.                                          00002180
           02 MNVENTEV  PIC X.                                          00002190
           02 MNVENTEO  PIC X(7).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MIMP1A    PIC X.                                          00002220
           02 MIMP1C    PIC X.                                          00002230
           02 MIMP1P    PIC X.                                          00002240
           02 MIMP1H    PIC X.                                          00002250
           02 MIMP1V    PIC X.                                          00002260
           02 MIMP1O    PIC X(4).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MJOUR4A   PIC X.                                          00002290
           02 MJOUR4C   PIC X.                                          00002300
           02 MJOUR4P   PIC X.                                          00002310
           02 MJOUR4H   PIC X.                                          00002320
           02 MJOUR4V   PIC X.                                          00002330
           02 MJOUR4O   PIC X(6).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MIMP2A    PIC X.                                          00002360
           02 MIMP2C    PIC X.                                          00002370
           02 MIMP2P    PIC X.                                          00002380
           02 MIMP2H    PIC X.                                          00002390
           02 MIMP2V    PIC X.                                          00002400
           02 MIMP2O    PIC X(4).                                       00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MMAGA     PIC X.                                          00002430
           02 MMAGC     PIC X.                                          00002440
           02 MMAGP     PIC X.                                          00002450
           02 MMAGH     PIC X.                                          00002460
           02 MMAGV     PIC X.                                          00002470
           02 MMAGO     PIC X(3).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MNVENTE2A      PIC X.                                     00002500
           02 MNVENTE2C PIC X.                                          00002510
           02 MNVENTE2P PIC X.                                          00002520
           02 MNVENTE2H PIC X.                                          00002530
           02 MNVENTE2V PIC X.                                          00002540
           02 MNVENTE2O      PIC X(7).                                  00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MJOUR5A   PIC X.                                          00002570
           02 MJOUR5C   PIC X.                                          00002580
           02 MJOUR5P   PIC X.                                          00002590
           02 MJOUR5H   PIC X.                                          00002600
           02 MJOUR5V   PIC X.                                          00002610
           02 MJOUR5O   PIC X(6).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MDDELIVA  PIC X.                                          00002640
           02 MDDELIVC  PIC X.                                          00002650
           02 MDDELIVP  PIC X.                                          00002660
           02 MDDELIVH  PIC X.                                          00002670
           02 MDDELIVV  PIC X.                                          00002680
           02 MDDELIVO  PIC X(10).                                      00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MCTPRESTA      PIC X.                                     00002710
           02 MCTPRESTC PIC X.                                          00002720
           02 MCTPRESTP PIC X.                                          00002730
           02 MCTPRESTH PIC X.                                          00002740
           02 MCTPRESTV PIC X.                                          00002750
           02 MCTPRESTO      PIC X(5).                                  00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MCPOSTALA      PIC X.                                     00002780
           02 MCPOSTALC PIC X.                                          00002790
           02 MCPOSTALP PIC X.                                          00002800
           02 MCPOSTALH PIC X.                                          00002810
           02 MCPOSTALV PIC X.                                          00002820
           02 MCPOSTALO      PIC X(5).                                  00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MLIBERRA  PIC X.                                          00002850
           02 MLIBERRC  PIC X.                                          00002860
           02 MLIBERRP  PIC X.                                          00002870
           02 MLIBERRH  PIC X.                                          00002880
           02 MLIBERRV  PIC X.                                          00002890
           02 MLIBERRO  PIC X(78).                                      00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MCODTRAA  PIC X.                                          00002920
           02 MCODTRAC  PIC X.                                          00002930
           02 MCODTRAP  PIC X.                                          00002940
           02 MCODTRAH  PIC X.                                          00002950
           02 MCODTRAV  PIC X.                                          00002960
           02 MCODTRAO  PIC X(4).                                       00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MCICSA    PIC X.                                          00002990
           02 MCICSC    PIC X.                                          00003000
           02 MCICSP    PIC X.                                          00003010
           02 MCICSH    PIC X.                                          00003020
           02 MCICSV    PIC X.                                          00003030
           02 MCICSO    PIC X(5).                                       00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MNETNAMA  PIC X.                                          00003060
           02 MNETNAMC  PIC X.                                          00003070
           02 MNETNAMP  PIC X.                                          00003080
           02 MNETNAMH  PIC X.                                          00003090
           02 MNETNAMV  PIC X.                                          00003100
           02 MNETNAMO  PIC X(8).                                       00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MSCREENA  PIC X.                                          00003130
           02 MSCREENC  PIC X.                                          00003140
           02 MSCREENP  PIC X.                                          00003150
           02 MSCREENH  PIC X.                                          00003160
           02 MSCREENV  PIC X.                                          00003170
           02 MSCREENO  PIC X(4).                                       00003180
                                                                                
