      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      *                                                                 00050000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-MU70-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00240001
      *--                                                                       
       01  COM-MU70-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ------------------------------220  00730000
           02 COMM-MU70-APPLI.                                          00740000
              03 COMM-MU70-CTYPSOC        PIC X(3).                     00741002
              03 COMM-MU70-CTYPLIEU       PIC X(1).                     00741002
              03 COMM-MU70-FILIALE        PIC X(3).                     00741002
              03 COMM-MU70-AUTOR          PIC X(1).                     00741002
              03 COMM-MU70-NSOC           PIC X(3).                     00741002
              03 COMM-MU70-NLIEU          PIC X(3).                     00741002
              03 COMM-MU70-NSOCENT        PIC X(3).                     00741002
              03 COMM-MU70-NDEPENT        PIC X(3).                     00741002
              03 COMM-MU70-LIBENT         PIC X(20).                    00741002
              03 COMM-MU70-NSOCMAG        PIC X(3).                     00741002
              03 COMM-MU70-NMAG           PIC X(3).                     00741002
              03 COMM-MU70-NBDEP          PIC S9(3) COMP-3.             00741002
              03 COMM-MU70-MATRICE      OCCURS 20.                              
                 04 COMM-MU70-AUTOR-NSOC  PIC X(3).                             
                 04 COMM-MU70-AUTOR-NDEP  PIC X(3).                             
              03 COMM-MU70-VOLUME         PIC S9(3)V99  COMP-3.         00741002
              03 COMM-MU70-VOLMINI        PIC S9(3)     COMP-3.         00741002
              03 COMM-MU70-NBPMINI        PIC S9(3)     COMP-3.         00741002
              03 COMM-MU70-DDEB           PIC X(10).                    00741002
              03 COMM-MU70-DFIN           PIC X(10).                    00741002
              03 COMM-MU70-NVENTE         PIC X(7).                     00741002
              03 COMM-MU70-NMUT           PIC X(7).                     00741002
              03 COMM-MU70-DDELIV         PIC X(10).                    00741002
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES -----------------------------95            
          02 COMM-MU71-APPLI.                                                   
             03 COMM-MU71-EN-TETE.                                              
                04 COMM-MU71-PAGE               PIC 9(4).                       
                04 COMM-MU71-NBPAGE             PIC 9(4).                       
                04 COMM-MU71-NSOCENT            PIC X(3).                       
                04 COMM-MU71-NDEPENT            PIC X(3).                       
             03 COMM-MU71-NSOCMAG            PIC X(3).                          
             03 COMM-MU71-NMAG               PIC X(3).                          
             03 COMM-MU71-NSOCPFT            PIC X(3).                          
             03 COMM-MU71-NPTF               PIC X(3).                          
             03 COMM-MU71-NOMCLI             PIC X(20).                         
             03 COMM-MU71-DATE-LENDEMAIN-TRT PIC X(8).                          
             03 COMM-MU71-DMUTATION          PIC X(8).                          
             03 COMM-MU71-DDELIV             PIC X(8).                          
             03 COMM-MU71-CSELART            PIC X(5).                          
             03 COMM-MU71-IND-MAX            PIC S9(5)       COMP-3.            
             03 COMM-MU71-IND                PIC S9(5)       COMP-3.            
             03 COMM-MU71-TOTPIECE           PIC 9(3).                          
             03 COMM-MU71-TOTVOLUM           PIC S9(3)V99    COMP-3.            
             03 COMM-MU71-QVOLUME            PIC S9(3)V99    COMP-3.            
             03 COMM-MU71-QPIECES            PIC 9(3).                          
      * ZONES RESERVEES APPLICATIVES ----------------------------- 62   00730000
          02 COMM-MU72-APPLI.                                           00800001
      *                                                                 00810001
              03 COMM-MU72-PROGPREC          PIC X(05).                 00890001
              03 COMM-MU72-DMUTATION         PIC X(08).                 00890001
              03 COMM-MU72-NMUTATION         PIC X(07).                 00890001
              03 COMM-MU72-SELART            PIC X(05).                 00890001
              03 COMM-MU72-QNBM3QUOTA        PIC S9(3)V99    COMP-3.    00890001
              03 COMM-MU72-QNBM3DISPO        PIC S9(3)V99    COMP-3.    00890001
              03 COMM-MU72-QNBM3PRIS         PIC S9(3)V99    COMP-3.    00890001
              03 COMM-MU72-QNBPQUOTA         PIC S9(5)       COMP-3.    00890001
              03 COMM-MU72-QNBPDISPO         PIC S9(5)       COMP-3.    00830001
              03 COMM-MU72-QNBPPRIS          PIC S9(5)       COMP-3.    00830001
              03 COMM-MU72-PAGE              PIC 999    COMP-3.         00830001
              03 COMM-MU72-NBPAGES           PIC 999    COMP-3.         00830001
              03 COMM-MU72-IND-PAGE          PIC 999    COMP-3.         00830001
              03 COMM-MU72-IND               PIC 999    COMP-3.         00830001
              03 COMM-MU72-IND-MAX           PIC 999    COMP-3.         00830001
              03 COMM-MU72-MAJOK             PIC X.                             
           02 COMM-MU70-FILLER            PIC X(3347).                  00970003
                                                                                
