      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGB201 AU 17/12/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,03,BI,A,                          *        
      *                           24,08,BI,A,                          *        
      *                           32,07,BI,A,                          *        
      *                           39,03,PD,A,                          *        
      *                           42,05,BI,A,                          *        
      *                           47,07,BI,A,                          *        
      *                           54,01,BI,A,                          *        
      *                           55,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGB201.                                                        
            05 NOMETAT-IGB201           PIC X(6) VALUE 'IGB201'.                
            05 RUPTURES-IGB201.                                                 
           10 IGB201-NSOCGRP            PIC X(03).                      007  003
           10 IGB201-NSOCIETE           PIC X(03).                      010  003
           10 IGB201-NLIEU              PIC X(03).                      013  003
           10 IGB201-CSELART            PIC X(05).                      016  005
           10 IGB201-NDEPOT             PIC X(03).                      021  003
           10 IGB201-DFINSAIS           PIC X(08).                      024  008
           10 IGB201-NMUTATION          PIC X(07).                      032  007
           10 IGB201-WSEQFAM            PIC S9(05)      COMP-3.         039  003
           10 IGB201-CMARQ              PIC X(05).                      042  005
           10 IGB201-NCODIC             PIC X(07).                      047  007
           10 IGB201-WORIGINE           PIC X(01).                      054  001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGB201-SEQUENCE           PIC S9(04) COMP.                055  002
      *--                                                                       
           10 IGB201-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGB201.                                                   
           10 IGB201-CFAM               PIC X(05).                      057  005
           10 IGB201-INTDATES           PIC X(24).                      062  024
           10 IGB201-LREFFOURN          PIC X(20).                      086  020
           10 IGB201-NSOCENTR           PIC X(03).                      106  003
           10 IGB201-QTENONRM           PIC S9(05)      COMP-3.         109  003
            05 FILLER                      PIC X(401).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGB201-LONG           PIC S9(4)   COMP  VALUE +111.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGB201-LONG           PIC S9(4) COMP-5  VALUE +111.           
                                                                                
      *}                                                                        
