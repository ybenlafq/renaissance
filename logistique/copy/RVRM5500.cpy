      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM5500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM5500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM5500.                                                            
           02  RM55-CLEREPART                                                   
               PIC X(0005).                                                     
           02  RM55-CGROUP                                                      
               PIC X(0005).                                                     
           02  RM55-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RM55-NLIEU                                                       
               PIC X(0003).                                                     
           02  RM55-QTE                                                         
               PIC S9(5) COMP-3.                                                
           02  RM55-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM5500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM5500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM55-CLEREPART-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM55-CLEREPART-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM55-CGROUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM55-CGROUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM55-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM55-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM55-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM55-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM55-QTE-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM55-QTE-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM55-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM55-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
