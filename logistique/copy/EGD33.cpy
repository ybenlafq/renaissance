      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD33   EGD33                                              00000020
      ***************************************************************** 00000030
       01   EGD33I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MSOCIETEI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDEPOTI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF1L    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDEF1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF1F    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDEF1I    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF2L    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDEF2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF2F    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDEF2I    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF3L    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDEF3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF3F    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDEF3I    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF4L    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDEF4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF4F    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDEF4I    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF5L    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDEF5L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF5F    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEF5I    PIC X(5).                                       00000410
           02 MTABLEI OCCURS   12 TIMES .                               00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MFAMI   PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMARQL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MMARQL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MMARQF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MMARQI  PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MCODL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCODF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCODI   PIC X(7).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MREFI   PIC X(20).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCK1L     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MSTOCK1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCK1F     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MSTOCK1I     PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCK2L     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MSTOCK2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCK2F     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MSTOCK2I     PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCK3L     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MSTOCK3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCK3F     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MSTOCK3I     PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCK4L     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MSTOCK4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCK4F     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSTOCK4I     PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCK5L     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MSTOCK5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCK5F     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MSTOCK5I     PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(78).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MZONCMDI  PIC X(11).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: EGD33   EGD33                                              00001040
      ***************************************************************** 00001050
       01   EGD33O REDEFINES EGD33I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MSOCIETEA      PIC X.                                     00001230
           02 MSOCIETEC PIC X.                                          00001240
           02 MSOCIETEP PIC X.                                          00001250
           02 MSOCIETEH PIC X.                                          00001260
           02 MSOCIETEV PIC X.                                          00001270
           02 MSOCIETEO      PIC X(3).                                  00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MDEPOTA   PIC X.                                          00001300
           02 MDEPOTC   PIC X.                                          00001310
           02 MDEPOTP   PIC X.                                          00001320
           02 MDEPOTH   PIC X.                                          00001330
           02 MDEPOTV   PIC X.                                          00001340
           02 MDEPOTO   PIC X(3).                                       00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDEF1A    PIC X.                                          00001370
           02 MDEF1C    PIC X.                                          00001380
           02 MDEF1P    PIC X.                                          00001390
           02 MDEF1H    PIC X.                                          00001400
           02 MDEF1V    PIC X.                                          00001410
           02 MDEF1O    PIC X(5).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MDEF2A    PIC X.                                          00001440
           02 MDEF2C    PIC X.                                          00001450
           02 MDEF2P    PIC X.                                          00001460
           02 MDEF2H    PIC X.                                          00001470
           02 MDEF2V    PIC X.                                          00001480
           02 MDEF2O    PIC X(5).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MDEF3A    PIC X.                                          00001510
           02 MDEF3C    PIC X.                                          00001520
           02 MDEF3P    PIC X.                                          00001530
           02 MDEF3H    PIC X.                                          00001540
           02 MDEF3V    PIC X.                                          00001550
           02 MDEF3O    PIC X(5).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MDEF4A    PIC X.                                          00001580
           02 MDEF4C    PIC X.                                          00001590
           02 MDEF4P    PIC X.                                          00001600
           02 MDEF4H    PIC X.                                          00001610
           02 MDEF4V    PIC X.                                          00001620
           02 MDEF4O    PIC X(5).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDEF5A    PIC X.                                          00001650
           02 MDEF5C    PIC X.                                          00001660
           02 MDEF5P    PIC X.                                          00001670
           02 MDEF5H    PIC X.                                          00001680
           02 MDEF5V    PIC X.                                          00001690
           02 MDEF5O    PIC X(5).                                       00001700
           02 MTABLEO OCCURS   12 TIMES .                               00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MFAMA   PIC X.                                          00001730
             03 MFAMC   PIC X.                                          00001740
             03 MFAMP   PIC X.                                          00001750
             03 MFAMH   PIC X.                                          00001760
             03 MFAMV   PIC X.                                          00001770
             03 MFAMO   PIC X(5).                                       00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MMARQA  PIC X.                                          00001800
             03 MMARQC  PIC X.                                          00001810
             03 MMARQP  PIC X.                                          00001820
             03 MMARQH  PIC X.                                          00001830
             03 MMARQV  PIC X.                                          00001840
             03 MMARQO  PIC X(5).                                       00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MCODA   PIC X.                                          00001870
             03 MCODC   PIC X.                                          00001880
             03 MCODP   PIC X.                                          00001890
             03 MCODH   PIC X.                                          00001900
             03 MCODV   PIC X.                                          00001910
             03 MCODO   PIC X(7).                                       00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MREFA   PIC X.                                          00001940
             03 MREFC   PIC X.                                          00001950
             03 MREFP   PIC X.                                          00001960
             03 MREFH   PIC X.                                          00001970
             03 MREFV   PIC X.                                          00001980
             03 MREFO   PIC X(20).                                      00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MSTOCK1A     PIC X.                                     00002010
             03 MSTOCK1C     PIC X.                                     00002020
             03 MSTOCK1P     PIC X.                                     00002030
             03 MSTOCK1H     PIC X.                                     00002040
             03 MSTOCK1V     PIC X.                                     00002050
             03 MSTOCK1O     PIC X(5).                                  00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MSTOCK2A     PIC X.                                     00002080
             03 MSTOCK2C     PIC X.                                     00002090
             03 MSTOCK2P     PIC X.                                     00002100
             03 MSTOCK2H     PIC X.                                     00002110
             03 MSTOCK2V     PIC X.                                     00002120
             03 MSTOCK2O     PIC X(5).                                  00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MSTOCK3A     PIC X.                                     00002150
             03 MSTOCK3C     PIC X.                                     00002160
             03 MSTOCK3P     PIC X.                                     00002170
             03 MSTOCK3H     PIC X.                                     00002180
             03 MSTOCK3V     PIC X.                                     00002190
             03 MSTOCK3O     PIC X(5).                                  00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MSTOCK4A     PIC X.                                     00002220
             03 MSTOCK4C     PIC X.                                     00002230
             03 MSTOCK4P     PIC X.                                     00002240
             03 MSTOCK4H     PIC X.                                     00002250
             03 MSTOCK4V     PIC X.                                     00002260
             03 MSTOCK4O     PIC X(5).                                  00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MSTOCK5A     PIC X.                                     00002290
             03 MSTOCK5C     PIC X.                                     00002300
             03 MSTOCK5P     PIC X.                                     00002310
             03 MSTOCK5H     PIC X.                                     00002320
             03 MSTOCK5V     PIC X.                                     00002330
             03 MSTOCK5O     PIC X(5).                                  00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MLIBERRA  PIC X.                                          00002360
           02 MLIBERRC  PIC X.                                          00002370
           02 MLIBERRP  PIC X.                                          00002380
           02 MLIBERRH  PIC X.                                          00002390
           02 MLIBERRV  PIC X.                                          00002400
           02 MLIBERRO  PIC X(78).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MCODTRAA  PIC X.                                          00002430
           02 MCODTRAC  PIC X.                                          00002440
           02 MCODTRAP  PIC X.                                          00002450
           02 MCODTRAH  PIC X.                                          00002460
           02 MCODTRAV  PIC X.                                          00002470
           02 MCODTRAO  PIC X(4).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MZONCMDA  PIC X.                                          00002500
           02 MZONCMDC  PIC X.                                          00002510
           02 MZONCMDP  PIC X.                                          00002520
           02 MZONCMDH  PIC X.                                          00002530
           02 MZONCMDV  PIC X.                                          00002540
           02 MZONCMDO  PIC X(11).                                      00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
