      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESU54   ESU54                                              00000020
      ***************************************************************** 00000030
       01   ESU54I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLSOCI    PIC X(20).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDJJL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MDATEDJJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEDJJF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDATEDJJI      PIC X(2).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDMML      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MDATEDMML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEDMMF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDATEDMMI      PIC X(2).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDAAL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MDATEDAAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEDAAF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDATEDAAI      PIC X(4).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEFJJL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MDATEFJJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEFJJF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDATEFJJI      PIC X(2).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEFMML      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDATEFMML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEFMMF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDATEFMMI      PIC X(2).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEFAAL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MDATEFAAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEFAAF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDATEFAAI      PIC X(4).                                  00000530
           02 M8I OCCURS   12 TIMES .                                   00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCODICL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCODICF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLCODICI     PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNCODICI     PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQTEI   PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MZONCMDI  PIC X(12).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(61).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: ESU54   ESU54                                              00000920
      ***************************************************************** 00000930
       01   ESU54O REDEFINES ESU54I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MNSOCA    PIC X.                                          00001110
           02 MNSOCC    PIC X.                                          00001120
           02 MNSOCP    PIC X.                                          00001130
           02 MNSOCH    PIC X.                                          00001140
           02 MNSOCV    PIC X.                                          00001150
           02 MNSOCO    PIC X(3).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MLSOCA    PIC X.                                          00001180
           02 MLSOCC    PIC X.                                          00001190
           02 MLSOCP    PIC X.                                          00001200
           02 MLSOCH    PIC X.                                          00001210
           02 MLSOCV    PIC X.                                          00001220
           02 MLSOCO    PIC X(20).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNLIEUA   PIC X.                                          00001250
           02 MNLIEUC   PIC X.                                          00001260
           02 MNLIEUP   PIC X.                                          00001270
           02 MNLIEUH   PIC X.                                          00001280
           02 MNLIEUV   PIC X.                                          00001290
           02 MNLIEUO   PIC X(3).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MLLIEUA   PIC X.                                          00001320
           02 MLLIEUC   PIC X.                                          00001330
           02 MLLIEUP   PIC X.                                          00001340
           02 MLLIEUH   PIC X.                                          00001350
           02 MLLIEUV   PIC X.                                          00001360
           02 MLLIEUO   PIC X(20).                                      00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MDATEDJJA      PIC X.                                     00001390
           02 MDATEDJJC PIC X.                                          00001400
           02 MDATEDJJP PIC X.                                          00001410
           02 MDATEDJJH PIC X.                                          00001420
           02 MDATEDJJV PIC X.                                          00001430
           02 MDATEDJJO      PIC X(2).                                  00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MDATEDMMA      PIC X.                                     00001460
           02 MDATEDMMC PIC X.                                          00001470
           02 MDATEDMMP PIC X.                                          00001480
           02 MDATEDMMH PIC X.                                          00001490
           02 MDATEDMMV PIC X.                                          00001500
           02 MDATEDMMO      PIC X(2).                                  00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MDATEDAAA      PIC X.                                     00001530
           02 MDATEDAAC PIC X.                                          00001540
           02 MDATEDAAP PIC X.                                          00001550
           02 MDATEDAAH PIC X.                                          00001560
           02 MDATEDAAV PIC X.                                          00001570
           02 MDATEDAAO      PIC X(4).                                  00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MDATEFJJA      PIC X.                                     00001600
           02 MDATEFJJC PIC X.                                          00001610
           02 MDATEFJJP PIC X.                                          00001620
           02 MDATEFJJH PIC X.                                          00001630
           02 MDATEFJJV PIC X.                                          00001640
           02 MDATEFJJO      PIC X(2).                                  00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MDATEFMMA      PIC X.                                     00001670
           02 MDATEFMMC PIC X.                                          00001680
           02 MDATEFMMP PIC X.                                          00001690
           02 MDATEFMMH PIC X.                                          00001700
           02 MDATEFMMV PIC X.                                          00001710
           02 MDATEFMMO      PIC X(2).                                  00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MDATEFAAA      PIC X.                                     00001740
           02 MDATEFAAC PIC X.                                          00001750
           02 MDATEFAAP PIC X.                                          00001760
           02 MDATEFAAH PIC X.                                          00001770
           02 MDATEFAAV PIC X.                                          00001780
           02 MDATEFAAO      PIC X(4).                                  00001790
           02 M8O OCCURS   12 TIMES .                                   00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MLCODICA     PIC X.                                     00001820
             03 MLCODICC     PIC X.                                     00001830
             03 MLCODICP     PIC X.                                     00001840
             03 MLCODICH     PIC X.                                     00001850
             03 MLCODICV     PIC X.                                     00001860
             03 MLCODICO     PIC X(20).                                 00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MNCODICA     PIC X.                                     00001890
             03 MNCODICC     PIC X.                                     00001900
             03 MNCODICP     PIC X.                                     00001910
             03 MNCODICH     PIC X.                                     00001920
             03 MNCODICV     PIC X.                                     00001930
             03 MNCODICO     PIC X(7).                                  00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MQTEA   PIC X.                                          00001960
             03 MQTEC   PIC X.                                          00001970
             03 MQTEP   PIC X.                                          00001980
             03 MQTEH   PIC X.                                          00001990
             03 MQTEV   PIC X.                                          00002000
             03 MQTEO   PIC X(5).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MZONCMDA  PIC X.                                          00002030
           02 MZONCMDC  PIC X.                                          00002040
           02 MZONCMDP  PIC X.                                          00002050
           02 MZONCMDH  PIC X.                                          00002060
           02 MZONCMDV  PIC X.                                          00002070
           02 MZONCMDO  PIC X(12).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(61).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
