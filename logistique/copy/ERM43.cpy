      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM31   ERM31                                              00000020
      ***************************************************************** 00000030
       01   ERM43I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEMAXL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNPAGEMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNPAGEMAXF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEMAXI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICIL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCODICIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODICIF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCODICII  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROIL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCAPPROIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCAPPROIF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCAPPROII      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCGROUPI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMIL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAMIL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMIF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMII   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAGREGIL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLAGREGIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLAGREGIF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLAGREGII      PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAGREGL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLAGREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAGREGF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLAGREGI  PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREG1L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MTYPREG1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREG1F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MTYPREG1I      PIC X(2).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREG2L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MTYPREG2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREG2F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MTYPREG2I      PIC X(2).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREG3L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MTYPREG3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREG3F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MTYPREG3I      PIC X(2).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREG4L      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MTYPREG4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREG4F      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MTYPREG4I      PIC X(2).                                  00000690
           02 LIGNEI OCCURS   13 TIMES .                                00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREMPLL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MREMPLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MREMPLF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MREMPLI      PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNCODICI     PIC X(7).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCMARQI      PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MLREFI  PIC X(20).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAPPROL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCAPPROL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCAPPROF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCAPPROI     PIC X(3).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXPOL      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MCEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCEXPOF      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCEXPOI      PIC X.                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPO1L     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MQEXPO1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQEXPO1F     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQEXPO1I     PIC X(3).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLS1L  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MQLS1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQLS1F  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQLS1I  PIC X(3).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPO2L     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MQEXPO2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQEXPO2F     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQEXPO2I     PIC X(3).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLS2L  COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MQLS2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQLS2F  PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MQLS2I  PIC X(3).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPO3L     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MQEXPO3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQEXPO3F     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MQEXPO3I     PIC X(3).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLS3L  COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MQLS3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQLS3F  PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MQLS3I  PIC X(3).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPO4L     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MQEXPO4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQEXPO4F     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MQEXPO4I     PIC X(3).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLS4L  COMP PIC S9(4).                                 00001230
      *--                                                                       
             03 MQLS4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQLS4F  PIC X.                                          00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MQLS4I  PIC X(3).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPF11L   COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MLPF11L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPF11F   PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLPF11I   PIC X(13).                                      00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLIBERRI  PIC X(80).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCODTRAI  PIC X(4).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCICSI    PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MNETNAMI  PIC X(8).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MSCREENI  PIC X(4).                                       00001500
      ***************************************************************** 00001510
      * SDF: ERM31   ERM31                                              00001520
      ***************************************************************** 00001530
       01   ERM43O REDEFINES ERM43I.                                    00001540
           02 FILLER    PIC X(12).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDATJOUA  PIC X.                                          00001570
           02 MDATJOUC  PIC X.                                          00001580
           02 MDATJOUP  PIC X.                                          00001590
           02 MDATJOUH  PIC X.                                          00001600
           02 MDATJOUV  PIC X.                                          00001610
           02 MDATJOUO  PIC X(10).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MTIMJOUA  PIC X.                                          00001640
           02 MTIMJOUC  PIC X.                                          00001650
           02 MTIMJOUP  PIC X.                                          00001660
           02 MTIMJOUH  PIC X.                                          00001670
           02 MTIMJOUV  PIC X.                                          00001680
           02 MTIMJOUO  PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNPAGEA   PIC X.                                          00001710
           02 MNPAGEC   PIC X.                                          00001720
           02 MNPAGEP   PIC X.                                          00001730
           02 MNPAGEH   PIC X.                                          00001740
           02 MNPAGEV   PIC X.                                          00001750
           02 MNPAGEO   PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNPAGEMAXA     PIC X.                                     00001780
           02 MNPAGEMAXC     PIC X.                                     00001790
           02 MNPAGEMAXP     PIC X.                                     00001800
           02 MNPAGEMAXH     PIC X.                                     00001810
           02 MNPAGEMAXV     PIC X.                                     00001820
           02 MNPAGEMAXO     PIC X(3).                                  00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MCODICIA  PIC X.                                          00001850
           02 MCODICIC  PIC X.                                          00001860
           02 MCODICIP  PIC X.                                          00001870
           02 MCODICIH  PIC X.                                          00001880
           02 MCODICIV  PIC X.                                          00001890
           02 MCODICIO  PIC X(7).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCAPPROIA      PIC X.                                     00001920
           02 MCAPPROIC PIC X.                                          00001930
           02 MCAPPROIP PIC X.                                          00001940
           02 MCAPPROIH PIC X.                                          00001950
           02 MCAPPROIV PIC X.                                          00001960
           02 MCAPPROIO      PIC X(3).                                  00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCGROUPA  PIC X.                                          00001990
           02 MCGROUPC  PIC X.                                          00002000
           02 MCGROUPP  PIC X.                                          00002010
           02 MCGROUPH  PIC X.                                          00002020
           02 MCGROUPV  PIC X.                                          00002030
           02 MCGROUPO  PIC X(5).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCFAMIA   PIC X.                                          00002060
           02 MCFAMIC   PIC X.                                          00002070
           02 MCFAMIP   PIC X.                                          00002080
           02 MCFAMIH   PIC X.                                          00002090
           02 MCFAMIV   PIC X.                                          00002100
           02 MCFAMIO   PIC X(5).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MLAGREGIA      PIC X.                                     00002130
           02 MLAGREGIC PIC X.                                          00002140
           02 MLAGREGIP PIC X.                                          00002150
           02 MLAGREGIH PIC X.                                          00002160
           02 MLAGREGIV PIC X.                                          00002170
           02 MLAGREGIO      PIC X(20).                                 00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCFAMA    PIC X.                                          00002200
           02 MCFAMC    PIC X.                                          00002210
           02 MCFAMP    PIC X.                                          00002220
           02 MCFAMH    PIC X.                                          00002230
           02 MCFAMV    PIC X.                                          00002240
           02 MCFAMO    PIC X(5).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLFAMA    PIC X.                                          00002270
           02 MLFAMC    PIC X.                                          00002280
           02 MLFAMP    PIC X.                                          00002290
           02 MLFAMH    PIC X.                                          00002300
           02 MLFAMV    PIC X.                                          00002310
           02 MLFAMO    PIC X(20).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MLAGREGA  PIC X.                                          00002340
           02 MLAGREGC  PIC X.                                          00002350
           02 MLAGREGP  PIC X.                                          00002360
           02 MLAGREGH  PIC X.                                          00002370
           02 MLAGREGV  PIC X.                                          00002380
           02 MLAGREGO  PIC X(20).                                      00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MTYPREG1A      PIC X.                                     00002410
           02 MTYPREG1C PIC X.                                          00002420
           02 MTYPREG1P PIC X.                                          00002430
           02 MTYPREG1H PIC X.                                          00002440
           02 MTYPREG1V PIC X.                                          00002450
           02 MTYPREG1O      PIC X(2).                                  00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MTYPREG2A      PIC X.                                     00002480
           02 MTYPREG2C PIC X.                                          00002490
           02 MTYPREG2P PIC X.                                          00002500
           02 MTYPREG2H PIC X.                                          00002510
           02 MTYPREG2V PIC X.                                          00002520
           02 MTYPREG2O      PIC X(2).                                  00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MTYPREG3A      PIC X.                                     00002550
           02 MTYPREG3C PIC X.                                          00002560
           02 MTYPREG3P PIC X.                                          00002570
           02 MTYPREG3H PIC X.                                          00002580
           02 MTYPREG3V PIC X.                                          00002590
           02 MTYPREG3O      PIC X(2).                                  00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MTYPREG4A      PIC X.                                     00002620
           02 MTYPREG4C PIC X.                                          00002630
           02 MTYPREG4P PIC X.                                          00002640
           02 MTYPREG4H PIC X.                                          00002650
           02 MTYPREG4V PIC X.                                          00002660
           02 MTYPREG4O      PIC X(2).                                  00002670
           02 LIGNEO OCCURS   13 TIMES .                                00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MREMPLA      PIC X.                                     00002700
             03 MREMPLC PIC X.                                          00002710
             03 MREMPLP PIC X.                                          00002720
             03 MREMPLH PIC X.                                          00002730
             03 MREMPLV PIC X.                                          00002740
             03 MREMPLO      PIC X.                                     00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MNCODICA     PIC X.                                     00002770
             03 MNCODICC     PIC X.                                     00002780
             03 MNCODICP     PIC X.                                     00002790
             03 MNCODICH     PIC X.                                     00002800
             03 MNCODICV     PIC X.                                     00002810
             03 MNCODICO     PIC X(7).                                  00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MCMARQA      PIC X.                                     00002840
             03 MCMARQC PIC X.                                          00002850
             03 MCMARQP PIC X.                                          00002860
             03 MCMARQH PIC X.                                          00002870
             03 MCMARQV PIC X.                                          00002880
             03 MCMARQO      PIC X(5).                                  00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MLREFA  PIC X.                                          00002910
             03 MLREFC  PIC X.                                          00002920
             03 MLREFP  PIC X.                                          00002930
             03 MLREFH  PIC X.                                          00002940
             03 MLREFV  PIC X.                                          00002950
             03 MLREFO  PIC X(20).                                      00002960
             03 FILLER       PIC X(2).                                  00002970
             03 MCAPPROA     PIC X.                                     00002980
             03 MCAPPROC     PIC X.                                     00002990
             03 MCAPPROP     PIC X.                                     00003000
             03 MCAPPROH     PIC X.                                     00003010
             03 MCAPPROV     PIC X.                                     00003020
             03 MCAPPROO     PIC X(3).                                  00003030
             03 FILLER       PIC X(2).                                  00003040
             03 MCEXPOA      PIC X.                                     00003050
             03 MCEXPOC PIC X.                                          00003060
             03 MCEXPOP PIC X.                                          00003070
             03 MCEXPOH PIC X.                                          00003080
             03 MCEXPOV PIC X.                                          00003090
             03 MCEXPOO      PIC X.                                     00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MQEXPO1A     PIC X.                                     00003120
             03 MQEXPO1C     PIC X.                                     00003130
             03 MQEXPO1P     PIC X.                                     00003140
             03 MQEXPO1H     PIC X.                                     00003150
             03 MQEXPO1V     PIC X.                                     00003160
             03 MQEXPO1O     PIC X(3).                                  00003170
             03 FILLER       PIC X(2).                                  00003180
             03 MQLS1A  PIC X.                                          00003190
             03 MQLS1C  PIC X.                                          00003200
             03 MQLS1P  PIC X.                                          00003210
             03 MQLS1H  PIC X.                                          00003220
             03 MQLS1V  PIC X.                                          00003230
             03 MQLS1O  PIC X(3).                                       00003240
             03 FILLER       PIC X(2).                                  00003250
             03 MQEXPO2A     PIC X.                                     00003260
             03 MQEXPO2C     PIC X.                                     00003270
             03 MQEXPO2P     PIC X.                                     00003280
             03 MQEXPO2H     PIC X.                                     00003290
             03 MQEXPO2V     PIC X.                                     00003300
             03 MQEXPO2O     PIC X(3).                                  00003310
             03 FILLER       PIC X(2).                                  00003320
             03 MQLS2A  PIC X.                                          00003330
             03 MQLS2C  PIC X.                                          00003340
             03 MQLS2P  PIC X.                                          00003350
             03 MQLS2H  PIC X.                                          00003360
             03 MQLS2V  PIC X.                                          00003370
             03 MQLS2O  PIC X(3).                                       00003380
             03 FILLER       PIC X(2).                                  00003390
             03 MQEXPO3A     PIC X.                                     00003400
             03 MQEXPO3C     PIC X.                                     00003410
             03 MQEXPO3P     PIC X.                                     00003420
             03 MQEXPO3H     PIC X.                                     00003430
             03 MQEXPO3V     PIC X.                                     00003440
             03 MQEXPO3O     PIC X(3).                                  00003450
             03 FILLER       PIC X(2).                                  00003460
             03 MQLS3A  PIC X.                                          00003470
             03 MQLS3C  PIC X.                                          00003480
             03 MQLS3P  PIC X.                                          00003490
             03 MQLS3H  PIC X.                                          00003500
             03 MQLS3V  PIC X.                                          00003510
             03 MQLS3O  PIC X(3).                                       00003520
             03 FILLER       PIC X(2).                                  00003530
             03 MQEXPO4A     PIC X.                                     00003540
             03 MQEXPO4C     PIC X.                                     00003550
             03 MQEXPO4P     PIC X.                                     00003560
             03 MQEXPO4H     PIC X.                                     00003570
             03 MQEXPO4V     PIC X.                                     00003580
             03 MQEXPO4O     PIC X(3).                                  00003590
             03 FILLER       PIC X(2).                                  00003600
             03 MQLS4A  PIC X.                                          00003610
             03 MQLS4C  PIC X.                                          00003620
             03 MQLS4P  PIC X.                                          00003630
             03 MQLS4H  PIC X.                                          00003640
             03 MQLS4V  PIC X.                                          00003650
             03 MQLS4O  PIC X(3).                                       00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MLPF11A   PIC X.                                          00003680
           02 MLPF11C   PIC X.                                          00003690
           02 MLPF11P   PIC X.                                          00003700
           02 MLPF11H   PIC X.                                          00003710
           02 MLPF11V   PIC X.                                          00003720
           02 MLPF11O   PIC X(13).                                      00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MLIBERRA  PIC X.                                          00003750
           02 MLIBERRC  PIC X.                                          00003760
           02 MLIBERRP  PIC X.                                          00003770
           02 MLIBERRH  PIC X.                                          00003780
           02 MLIBERRV  PIC X.                                          00003790
           02 MLIBERRO  PIC X(80).                                      00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MCODTRAA  PIC X.                                          00003820
           02 MCODTRAC  PIC X.                                          00003830
           02 MCODTRAP  PIC X.                                          00003840
           02 MCODTRAH  PIC X.                                          00003850
           02 MCODTRAV  PIC X.                                          00003860
           02 MCODTRAO  PIC X(4).                                       00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MCICSA    PIC X.                                          00003890
           02 MCICSC    PIC X.                                          00003900
           02 MCICSP    PIC X.                                          00003910
           02 MCICSH    PIC X.                                          00003920
           02 MCICSV    PIC X.                                          00003930
           02 MCICSO    PIC X(5).                                       00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MNETNAMA  PIC X.                                          00003960
           02 MNETNAMC  PIC X.                                          00003970
           02 MNETNAMP  PIC X.                                          00003980
           02 MNETNAMH  PIC X.                                          00003990
           02 MNETNAMV  PIC X.                                          00004000
           02 MNETNAMO  PIC X(8).                                       00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MSCREENA  PIC X.                                          00004030
           02 MSCREENC  PIC X.                                          00004040
           02 MSCREENP  PIC X.                                          00004050
           02 MSCREENH  PIC X.                                          00004060
           02 MSCREENV  PIC X.                                          00004070
           02 MSCREENO  PIC X(4).                                       00004080
                                                                                
