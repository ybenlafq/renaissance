      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TFL32                                          *  00020001
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG              PIC S9(3) COMP-3 VALUE 637.             00080001
       01  TS-RECORD.                                                   00090001
         05  TS-LIGNE         OCCURS 13.                                00100001
           10 TS-NSOCIETE     PIC X(3).                                 00120301
           10 TS-NLIEU        PIC X(3).                                 00120401
           10 TS-CTYPTRAIT    PIC X(5).                                 00120501
           10 TS-CPROAFF      PIC X(5).                                 00120601
           10 TS-LPROAFF      PIC X(20).                                00121001
           10 TS-DEFFET       PIC X(10).                                00121101
           10 TS-SUPP         PIC X.                                    00121201
           10 TS-MAJ-FL05     PIC X.                                    00121301
           10 TS-SUP-FL05     PIC X.                                    00121401
      *    10 TS-FILLER       PIC X(02).                                00126001
                                                                                
