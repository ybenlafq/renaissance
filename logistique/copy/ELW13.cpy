      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SUIVI DES COMMANDES LM7                                         00000020
      ***************************************************************** 00000030
       01   ELW13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDEPF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCDEPI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDRECEPTL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MSDRECEPTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSDRECEPTF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSDRECEPTI     PIC X(4).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNDOSLM7L     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MSNDOSLM7L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSNDOSLM7F     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSNDOSLM7I     PIC X(8).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNRECQORIGL   COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MSNRECQORIGL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MSNRECQORIGF   PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSNRECQORIGI   PIC X(7).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNRECQUAIL    COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MSNRECQUAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MSNRECQUAIF    PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSNRECQUAII    PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNRECL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MSNRECL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSNRECF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSNRECI   PIC X(7).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNCDEL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSNCDEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSNCDEF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSNCDEI   PIC X(7).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNCODICL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MSNCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSNCODICF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSNCODICI      PIC X(7).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDLIVRL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSDLIVRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSDLIVRF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSDLIVRI  PIC X(4).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNBLL    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MSNBLL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSNBLF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSNBLI    PIC X(10).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSETATL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSETATF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSETATI   PIC X(2).                                       00000730
           02 MDRECEPTD OCCURS   6 TIMES .                              00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRECEPTL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDRECEPTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDRECEPTF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDRECEPTI    PIC X(4).                                  00000780
           02 MNDOSLM7D OCCURS   6 TIMES .                              00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDOSLM7L    COMP PIC S9(4).                            00000800
      *--                                                                       
             03 MNDOSLM7L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNDOSLM7F    PIC X.                                     00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MNDOSLM7I    PIC X(8).                                  00000830
           02 MNRECQORIGD OCCURS   6 TIMES .                            00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRECQORIGL  COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MNRECQORIGL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNRECQORIGF  PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MNRECQORIGI  PIC X(7).                                  00000880
           02 MNRECQUAID OCCURS   6 TIMES .                             00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRECQUAIL   COMP PIC S9(4).                            00000900
      *--                                                                       
             03 MNRECQUAIL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNRECQUAIF   PIC X.                                     00000910
             03 FILLER  PIC X(4).                                       00000920
             03 MNRECQUAII   PIC X(7).                                  00000930
           02 MNRECD OCCURS   6 TIMES .                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRECL  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MNRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNRECF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MNRECI  PIC X(7).                                       00000980
           02 MNCDED OCCURS   6 TIMES .                                 00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEL  COMP PIC S9(4).                                 00001000
      *--                                                                       
             03 MNCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCDEF  PIC X.                                          00001010
             03 FILLER  PIC X(4).                                       00001020
             03 MNCDEI  PIC X(7).                                       00001030
           02 MNCODICD OCCURS   6 TIMES .                               00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00001050
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00001060
             03 FILLER  PIC X(4).                                       00001070
             03 MNCODICI     PIC X(7).                                  00001080
           02 MDLIVRD OCCURS   6 TIMES .                                00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDLIVRL      COMP PIC S9(4).                            00001100
      *--                                                                       
             03 MDLIVRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDLIVRF      PIC X.                                     00001110
             03 FILLER  PIC X(4).                                       00001120
             03 MDLIVRI      PIC X(4).                                  00001130
           02 MQRECQUAID OCCURS   6 TIMES .                             00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRECQUAIL   COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MQRECQUAIL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQRECQUAIF   PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MQRECQUAII   PIC X(5).                                  00001180
           02 MQRANGEED OCCURS   6 TIMES .                              00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRANGEEL    COMP PIC S9(4).                            00001200
      *--                                                                       
             03 MQRANGEEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQRANGEEF    PIC X.                                     00001210
             03 FILLER  PIC X(4).                                       00001220
             03 MQRANGEEI    PIC X(5).                                  00001230
           02 METATD OCCURS   6 TIMES .                                 00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 METATL  COMP PIC S9(4).                                 00001250
      *--                                                                       
             03 METATL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 METATF  PIC X.                                          00001260
             03 FILLER  PIC X(4).                                       00001270
             03 METATI  PIC X(2).                                       00001280
           02 MNENTCDED OCCURS   6 TIMES .                              00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTCDEL    COMP PIC S9(4).                            00001300
      *--                                                                       
             03 MNENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTCDEF    PIC X.                                     00001310
             03 FILLER  PIC X(4).                                       00001320
             03 MNENTCDEI    PIC X(5).                                  00001330
           02 MLENTCDED OCCURS   6 TIMES .                              00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTCDEL    COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MLENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLENTCDEF    PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MLENTCDEI    PIC X(20).                                 00001380
           02 MNBLD OCCURS   6 TIMES .                                  00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBLL   COMP PIC S9(4).                                 00001400
      *--                                                                       
             03 MNBLL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNBLF   PIC X.                                          00001410
             03 FILLER  PIC X(4).                                       00001420
             03 MNBLI   PIC X(10).                                      00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNENTCDEL     COMP PIC S9(4).                            00001440
      *--                                                                       
           02 MSNENTCDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSNENTCDEF     PIC X.                                     00001450
           02 FILLER    PIC X(4).                                       00001460
           02 MSNENTCDEI     PIC X(5).                                  00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLENTCDEL     COMP PIC S9(4).                            00001480
      *--                                                                       
           02 MSLENTCDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSLENTCDEF     PIC X.                                     00001490
           02 FILLER    PIC X(4).                                       00001500
           02 MSLENTCDEI     PIC X(20).                                 00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001520
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001530
           02 FILLER    PIC X(4).                                       00001540
           02 MLIBERRI  PIC X(78).                                      00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001560
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MCODTRAI  PIC X(4).                                       00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001600
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001610
           02 FILLER    PIC X(4).                                       00001620
           02 MCICSI    PIC X(5).                                       00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001640
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MNETNAMI  PIC X(8).                                       00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001680
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001690
           02 FILLER    PIC X(4).                                       00001700
           02 MSCREENI  PIC X(4).                                       00001710
      ***************************************************************** 00001720
      * SUIVI DES COMMANDES LM7                                         00001730
      ***************************************************************** 00001740
       01   ELW13O REDEFINES ELW13I.                                    00001750
           02 FILLER    PIC X(12).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MDATJOUA  PIC X.                                          00001780
           02 MDATJOUC  PIC X.                                          00001790
           02 MDATJOUP  PIC X.                                          00001800
           02 MDATJOUH  PIC X.                                          00001810
           02 MDATJOUV  PIC X.                                          00001820
           02 MDATJOUO  PIC X(10).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MTIMJOUA  PIC X.                                          00001850
           02 MTIMJOUC  PIC X.                                          00001860
           02 MTIMJOUP  PIC X.                                          00001870
           02 MTIMJOUH  PIC X.                                          00001880
           02 MTIMJOUV  PIC X.                                          00001890
           02 MTIMJOUO  PIC X(5).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MPAGEA    PIC X.                                          00001920
           02 MPAGEC    PIC X.                                          00001930
           02 MPAGEP    PIC X.                                          00001940
           02 MPAGEH    PIC X.                                          00001950
           02 MPAGEV    PIC X.                                          00001960
           02 MPAGEO    PIC X(4).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MNPAGEA   PIC X.                                          00001990
           02 MNPAGEC   PIC X.                                          00002000
           02 MNPAGEP   PIC X.                                          00002010
           02 MNPAGEH   PIC X.                                          00002020
           02 MNPAGEV   PIC X.                                          00002030
           02 MNPAGEO   PIC X(4).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MNSOCDEPA      PIC X.                                     00002060
           02 MNSOCDEPC PIC X.                                          00002070
           02 MNSOCDEPP PIC X.                                          00002080
           02 MNSOCDEPH PIC X.                                          00002090
           02 MNSOCDEPV PIC X.                                          00002100
           02 MNSOCDEPO      PIC X(3).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNDEPOTA  PIC X.                                          00002130
           02 MNDEPOTC  PIC X.                                          00002140
           02 MNDEPOTP  PIC X.                                          00002150
           02 MNDEPOTH  PIC X.                                          00002160
           02 MNDEPOTV  PIC X.                                          00002170
           02 MNDEPOTO  PIC X(3).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MLLIEUA   PIC X.                                          00002200
           02 MLLIEUC   PIC X.                                          00002210
           02 MLLIEUP   PIC X.                                          00002220
           02 MLLIEUH   PIC X.                                          00002230
           02 MLLIEUV   PIC X.                                          00002240
           02 MLLIEUO   PIC X(20).                                      00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSDRECEPTA     PIC X.                                     00002270
           02 MSDRECEPTC     PIC X.                                     00002280
           02 MSDRECEPTP     PIC X.                                     00002290
           02 MSDRECEPTH     PIC X.                                     00002300
           02 MSDRECEPTV     PIC X.                                     00002310
           02 MSDRECEPTO     PIC X(4).                                  00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MSNDOSLM7A     PIC X.                                     00002340
           02 MSNDOSLM7C     PIC X.                                     00002350
           02 MSNDOSLM7P     PIC X.                                     00002360
           02 MSNDOSLM7H     PIC X.                                     00002370
           02 MSNDOSLM7V     PIC X.                                     00002380
           02 MSNDOSLM7O     PIC X(8).                                  00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MSNRECQORIGA   PIC X.                                     00002410
           02 MSNRECQORIGC   PIC X.                                     00002420
           02 MSNRECQORIGP   PIC X.                                     00002430
           02 MSNRECQORIGH   PIC X.                                     00002440
           02 MSNRECQORIGV   PIC X.                                     00002450
           02 MSNRECQORIGO   PIC X(7).                                  00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MSNRECQUAIA    PIC X.                                     00002480
           02 MSNRECQUAIC    PIC X.                                     00002490
           02 MSNRECQUAIP    PIC X.                                     00002500
           02 MSNRECQUAIH    PIC X.                                     00002510
           02 MSNRECQUAIV    PIC X.                                     00002520
           02 MSNRECQUAIO    PIC X(7).                                  00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MSNRECA   PIC X.                                          00002550
           02 MSNRECC   PIC X.                                          00002560
           02 MSNRECP   PIC X.                                          00002570
           02 MSNRECH   PIC X.                                          00002580
           02 MSNRECV   PIC X.                                          00002590
           02 MSNRECO   PIC X(7).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MSNCDEA   PIC X.                                          00002620
           02 MSNCDEC   PIC X.                                          00002630
           02 MSNCDEP   PIC X.                                          00002640
           02 MSNCDEH   PIC X.                                          00002650
           02 MSNCDEV   PIC X.                                          00002660
           02 MSNCDEO   PIC X(7).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MSNCODICA      PIC X.                                     00002690
           02 MSNCODICC PIC X.                                          00002700
           02 MSNCODICP PIC X.                                          00002710
           02 MSNCODICH PIC X.                                          00002720
           02 MSNCODICV PIC X.                                          00002730
           02 MSNCODICO      PIC X(7).                                  00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MSDLIVRA  PIC X.                                          00002760
           02 MSDLIVRC  PIC X.                                          00002770
           02 MSDLIVRP  PIC X.                                          00002780
           02 MSDLIVRH  PIC X.                                          00002790
           02 MSDLIVRV  PIC X.                                          00002800
           02 MSDLIVRO  PIC X(4).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MSNBLA    PIC X.                                          00002830
           02 MSNBLC    PIC X.                                          00002840
           02 MSNBLP    PIC X.                                          00002850
           02 MSNBLH    PIC X.                                          00002860
           02 MSNBLV    PIC X.                                          00002870
           02 MSNBLO    PIC X(10).                                      00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MSETATA   PIC X.                                          00002900
           02 MSETATC   PIC X.                                          00002910
           02 MSETATP   PIC X.                                          00002920
           02 MSETATH   PIC X.                                          00002930
           02 MSETATV   PIC X.                                          00002940
           02 MSETATO   PIC X(2).                                       00002950
           02 DFHMS1 OCCURS   6 TIMES .                                 00002960
             03 FILLER       PIC X(2).                                  00002970
             03 MDRECEPTA    PIC X.                                     00002980
             03 MDRECEPTC    PIC X.                                     00002990
             03 MDRECEPTP    PIC X.                                     00003000
             03 MDRECEPTH    PIC X.                                     00003010
             03 MDRECEPTV    PIC X.                                     00003020
             03 MDRECEPTO    PIC X(4).                                  00003030
           02 DFHMS2 OCCURS   6 TIMES .                                 00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MNDOSLM7A    PIC X.                                     00003060
             03 MNDOSLM7C    PIC X.                                     00003070
             03 MNDOSLM7P    PIC X.                                     00003080
             03 MNDOSLM7H    PIC X.                                     00003090
             03 MNDOSLM7V    PIC X.                                     00003100
             03 MNDOSLM7O    PIC X(8).                                  00003110
           02 DFHMS3 OCCURS   6 TIMES .                                 00003120
             03 FILLER       PIC X(2).                                  00003130
             03 MNRECQORIGA  PIC X.                                     00003140
             03 MNRECQORIGC  PIC X.                                     00003150
             03 MNRECQORIGP  PIC X.                                     00003160
             03 MNRECQORIGH  PIC X.                                     00003170
             03 MNRECQORIGV  PIC X.                                     00003180
             03 MNRECQORIGO  PIC X(7).                                  00003190
           02 DFHMS4 OCCURS   6 TIMES .                                 00003200
             03 FILLER       PIC X(2).                                  00003210
             03 MNRECQUAIA   PIC X.                                     00003220
             03 MNRECQUAIC   PIC X.                                     00003230
             03 MNRECQUAIP   PIC X.                                     00003240
             03 MNRECQUAIH   PIC X.                                     00003250
             03 MNRECQUAIV   PIC X.                                     00003260
             03 MNRECQUAIO   PIC X(7).                                  00003270
           02 DFHMS5 OCCURS   6 TIMES .                                 00003280
             03 FILLER       PIC X(2).                                  00003290
             03 MNRECA  PIC X.                                          00003300
             03 MNRECC  PIC X.                                          00003310
             03 MNRECP  PIC X.                                          00003320
             03 MNRECH  PIC X.                                          00003330
             03 MNRECV  PIC X.                                          00003340
             03 MNRECO  PIC X(7).                                       00003350
           02 DFHMS6 OCCURS   6 TIMES .                                 00003360
             03 FILLER       PIC X(2).                                  00003370
             03 MNCDEA  PIC X.                                          00003380
             03 MNCDEC  PIC X.                                          00003390
             03 MNCDEP  PIC X.                                          00003400
             03 MNCDEH  PIC X.                                          00003410
             03 MNCDEV  PIC X.                                          00003420
             03 MNCDEO  PIC X(7).                                       00003430
           02 DFHMS7 OCCURS   6 TIMES .                                 00003440
             03 FILLER       PIC X(2).                                  00003450
             03 MNCODICA     PIC X.                                     00003460
             03 MNCODICC     PIC X.                                     00003470
             03 MNCODICP     PIC X.                                     00003480
             03 MNCODICH     PIC X.                                     00003490
             03 MNCODICV     PIC X.                                     00003500
             03 MNCODICO     PIC X(7).                                  00003510
           02 DFHMS8 OCCURS   6 TIMES .                                 00003520
             03 FILLER       PIC X(2).                                  00003530
             03 MDLIVRA      PIC X.                                     00003540
             03 MDLIVRC PIC X.                                          00003550
             03 MDLIVRP PIC X.                                          00003560
             03 MDLIVRH PIC X.                                          00003570
             03 MDLIVRV PIC X.                                          00003580
             03 MDLIVRO      PIC X(4).                                  00003590
           02 DFHMS9 OCCURS   6 TIMES .                                 00003600
             03 FILLER       PIC X(2).                                  00003610
             03 MQRECQUAIA   PIC X.                                     00003620
             03 MQRECQUAIC   PIC X.                                     00003630
             03 MQRECQUAIP   PIC X.                                     00003640
             03 MQRECQUAIH   PIC X.                                     00003650
             03 MQRECQUAIV   PIC X.                                     00003660
             03 MQRECQUAIO   PIC X(5).                                  00003670
           02 DFHMS10 OCCURS   6 TIMES .                                00003680
             03 FILLER       PIC X(2).                                  00003690
             03 MQRANGEEA    PIC X.                                     00003700
             03 MQRANGEEC    PIC X.                                     00003710
             03 MQRANGEEP    PIC X.                                     00003720
             03 MQRANGEEH    PIC X.                                     00003730
             03 MQRANGEEV    PIC X.                                     00003740
             03 MQRANGEEO    PIC X(5).                                  00003750
           02 DFHMS11 OCCURS   6 TIMES .                                00003760
             03 FILLER       PIC X(2).                                  00003770
             03 METATA  PIC X.                                          00003780
             03 METATC  PIC X.                                          00003790
             03 METATP  PIC X.                                          00003800
             03 METATH  PIC X.                                          00003810
             03 METATV  PIC X.                                          00003820
             03 METATO  PIC X(2).                                       00003830
           02 DFHMS12 OCCURS   6 TIMES .                                00003840
             03 FILLER       PIC X(2).                                  00003850
             03 MNENTCDEA    PIC X.                                     00003860
             03 MNENTCDEC    PIC X.                                     00003870
             03 MNENTCDEP    PIC X.                                     00003880
             03 MNENTCDEH    PIC X.                                     00003890
             03 MNENTCDEV    PIC X.                                     00003900
             03 MNENTCDEO    PIC X(5).                                  00003910
           02 DFHMS13 OCCURS   6 TIMES .                                00003920
             03 FILLER       PIC X(2).                                  00003930
             03 MLENTCDEA    PIC X.                                     00003940
             03 MLENTCDEC    PIC X.                                     00003950
             03 MLENTCDEP    PIC X.                                     00003960
             03 MLENTCDEH    PIC X.                                     00003970
             03 MLENTCDEV    PIC X.                                     00003980
             03 MLENTCDEO    PIC X(20).                                 00003990
           02 DFHMS14 OCCURS   6 TIMES .                                00004000
             03 FILLER       PIC X(2).                                  00004010
             03 MNBLA   PIC X.                                          00004020
             03 MNBLC   PIC X.                                          00004030
             03 MNBLP   PIC X.                                          00004040
             03 MNBLH   PIC X.                                          00004050
             03 MNBLV   PIC X.                                          00004060
             03 MNBLO   PIC X(10).                                      00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MSNENTCDEA     PIC X.                                     00004090
           02 MSNENTCDEC     PIC X.                                     00004100
           02 MSNENTCDEP     PIC X.                                     00004110
           02 MSNENTCDEH     PIC X.                                     00004120
           02 MSNENTCDEV     PIC X.                                     00004130
           02 MSNENTCDEO     PIC X(5).                                  00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MSLENTCDEA     PIC X.                                     00004160
           02 MSLENTCDEC     PIC X.                                     00004170
           02 MSLENTCDEP     PIC X.                                     00004180
           02 MSLENTCDEH     PIC X.                                     00004190
           02 MSLENTCDEV     PIC X.                                     00004200
           02 MSLENTCDEO     PIC X(20).                                 00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MLIBERRA  PIC X.                                          00004230
           02 MLIBERRC  PIC X.                                          00004240
           02 MLIBERRP  PIC X.                                          00004250
           02 MLIBERRH  PIC X.                                          00004260
           02 MLIBERRV  PIC X.                                          00004270
           02 MLIBERRO  PIC X(78).                                      00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MCODTRAA  PIC X.                                          00004300
           02 MCODTRAC  PIC X.                                          00004310
           02 MCODTRAP  PIC X.                                          00004320
           02 MCODTRAH  PIC X.                                          00004330
           02 MCODTRAV  PIC X.                                          00004340
           02 MCODTRAO  PIC X(4).                                       00004350
           02 FILLER    PIC X(2).                                       00004360
           02 MCICSA    PIC X.                                          00004370
           02 MCICSC    PIC X.                                          00004380
           02 MCICSP    PIC X.                                          00004390
           02 MCICSH    PIC X.                                          00004400
           02 MCICSV    PIC X.                                          00004410
           02 MCICSO    PIC X(5).                                       00004420
           02 FILLER    PIC X(2).                                       00004430
           02 MNETNAMA  PIC X.                                          00004440
           02 MNETNAMC  PIC X.                                          00004450
           02 MNETNAMP  PIC X.                                          00004460
           02 MNETNAMH  PIC X.                                          00004470
           02 MNETNAMV  PIC X.                                          00004480
           02 MNETNAMO  PIC X(8).                                       00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MSCREENA  PIC X.                                          00004510
           02 MSCREENC  PIC X.                                          00004520
           02 MSCREENP  PIC X.                                          00004530
           02 MSCREENH  PIC X.                                          00004540
           02 MSCREENV  PIC X.                                          00004550
           02 MSCREENO  PIC X(4).                                       00004560
                                                                                
