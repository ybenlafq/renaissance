      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGL02   EGL02                                              00000020
      ***************************************************************** 00000030
       01   EGL02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRECL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCRECF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCRECI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRECL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLRECF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLRECI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNDEPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNDEPF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNDEPI    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSEML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDSEML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDSEMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDSEMI    PIC X(6).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDATEI    PIC X(10).                                      00000490
           02 M184I OCCURS   10 TIMES .                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIVRL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNLIVRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIVRF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNLIVRI      PIC X(7).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIVRL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCLIVRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCLIVRF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCLIVRI      PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLIVRL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MQLIVRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQLIVRF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MQLIVRI      PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBUOL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MQNBUOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQNBUOF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQNBUOI      PIC X(4).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTRANSL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCTRANSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCTRANSF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCTRANSI     PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDHHL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MDHHL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MDHHF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDHHI   PIC X(2).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMNL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MDMNL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MDMNF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDMNI   PIC X(2).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMML      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MLCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCOMMF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLCOMMI      PIC X(20).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MZONCMDI  PIC X(15).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(58).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: EGL02   EGL02                                              00001080
      ***************************************************************** 00001090
       01   EGL02O REDEFINES EGL02I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MPAGEA    PIC X.                                          00001270
           02 MPAGEC    PIC X.                                          00001280
           02 MPAGEP    PIC X.                                          00001290
           02 MPAGEH    PIC X.                                          00001300
           02 MPAGEV    PIC X.                                          00001310
           02 MPAGEO    PIC ZZ.                                         00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MPAGETOTA      PIC X.                                     00001340
           02 MPAGETOTC PIC X.                                          00001350
           02 MPAGETOTP PIC X.                                          00001360
           02 MPAGETOTH PIC X.                                          00001370
           02 MPAGETOTV PIC X.                                          00001380
           02 MPAGETOTO      PIC ZZ.                                    00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MWFONCA   PIC X.                                          00001410
           02 MWFONCC   PIC X.                                          00001420
           02 MWFONCP   PIC X.                                          00001430
           02 MWFONCH   PIC X.                                          00001440
           02 MWFONCV   PIC X.                                          00001450
           02 MWFONCO   PIC X(3).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MNSOCA    PIC X.                                          00001480
           02 MNSOCC    PIC X.                                          00001490
           02 MNSOCP    PIC X.                                          00001500
           02 MNSOCH    PIC X.                                          00001510
           02 MNSOCV    PIC X.                                          00001520
           02 MNSOCO    PIC X(3).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MCRECA    PIC X.                                          00001550
           02 MCRECC    PIC X.                                          00001560
           02 MCRECP    PIC X.                                          00001570
           02 MCRECH    PIC X.                                          00001580
           02 MCRECV    PIC X.                                          00001590
           02 MCRECO    PIC X(5).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MLRECA    PIC X.                                          00001620
           02 MLRECC    PIC X.                                          00001630
           02 MLRECP    PIC X.                                          00001640
           02 MLRECH    PIC X.                                          00001650
           02 MLRECV    PIC X.                                          00001660
           02 MLRECO    PIC X(20).                                      00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MNDEPA    PIC X.                                          00001690
           02 MNDEPC    PIC X.                                          00001700
           02 MNDEPP    PIC X.                                          00001710
           02 MNDEPH    PIC X.                                          00001720
           02 MNDEPV    PIC X.                                          00001730
           02 MNDEPO    PIC X(3).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MDSEMA    PIC X.                                          00001760
           02 MDSEMC    PIC X.                                          00001770
           02 MDSEMP    PIC X.                                          00001780
           02 MDSEMH    PIC X.                                          00001790
           02 MDSEMV    PIC X.                                          00001800
           02 MDSEMO    PIC X(6).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MDATEA    PIC X.                                          00001830
           02 MDATEC    PIC X.                                          00001840
           02 MDATEP    PIC X.                                          00001850
           02 MDATEH    PIC X.                                          00001860
           02 MDATEV    PIC X.                                          00001870
           02 MDATEO    PIC X(10).                                      00001880
           02 M184O OCCURS   10 TIMES .                                 00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MNLIVRA      PIC X.                                     00001910
             03 MNLIVRC PIC X.                                          00001920
             03 MNLIVRP PIC X.                                          00001930
             03 MNLIVRH PIC X.                                          00001940
             03 MNLIVRV PIC X.                                          00001950
             03 MNLIVRO      PIC X(7).                                  00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MCLIVRA      PIC X.                                     00001980
             03 MCLIVRC PIC X.                                          00001990
             03 MCLIVRP PIC X.                                          00002000
             03 MCLIVRH PIC X.                                          00002010
             03 MCLIVRV PIC X.                                          00002020
             03 MCLIVRO      PIC X(5).                                  00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MQLIVRA      PIC X.                                     00002050
             03 MQLIVRC PIC X.                                          00002060
             03 MQLIVRP PIC X.                                          00002070
             03 MQLIVRH PIC X.                                          00002080
             03 MQLIVRV PIC X.                                          00002090
             03 MQLIVRO      PIC ZZZZZ.                                 00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MQNBUOA      PIC X.                                     00002120
             03 MQNBUOC PIC X.                                          00002130
             03 MQNBUOP PIC X.                                          00002140
             03 MQNBUOH PIC X.                                          00002150
             03 MQNBUOV PIC X.                                          00002160
             03 MQNBUOO      PIC ZZZZ.                                  00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MCTRANSA     PIC X.                                     00002190
             03 MCTRANSC     PIC X.                                     00002200
             03 MCTRANSP     PIC X.                                     00002210
             03 MCTRANSH     PIC X.                                     00002220
             03 MCTRANSV     PIC X.                                     00002230
             03 MCTRANSO     PIC X(5).                                  00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MDHHA   PIC X.                                          00002260
             03 MDHHC   PIC X.                                          00002270
             03 MDHHP   PIC X.                                          00002280
             03 MDHHH   PIC X.                                          00002290
             03 MDHHV   PIC X.                                          00002300
             03 MDHHO   PIC X(2).                                       00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MDMNA   PIC X.                                          00002330
             03 MDMNC   PIC X.                                          00002340
             03 MDMNP   PIC X.                                          00002350
             03 MDMNH   PIC X.                                          00002360
             03 MDMNV   PIC X.                                          00002370
             03 MDMNO   PIC X(2).                                       00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MLCOMMA      PIC X.                                     00002400
             03 MLCOMMC PIC X.                                          00002410
             03 MLCOMMP PIC X.                                          00002420
             03 MLCOMMH PIC X.                                          00002430
             03 MLCOMMV PIC X.                                          00002440
             03 MLCOMMO      PIC X(20).                                 00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MZONCMDA  PIC X.                                          00002470
           02 MZONCMDC  PIC X.                                          00002480
           02 MZONCMDP  PIC X.                                          00002490
           02 MZONCMDH  PIC X.                                          00002500
           02 MZONCMDV  PIC X.                                          00002510
           02 MZONCMDO  PIC X(15).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(58).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
