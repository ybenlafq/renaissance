      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      * -AIDA ********************************************************* 00000100
      *  ZONES DE COMMAREA                                              00000200
      ***************************************************************** 00000300
      **************************************************************    00000400
      * COMMAREA SPECIFIQUE PRG TMU00 (MENU)             TR: MU00  *    00000500
      *                          TMU01 TMU02                       *    00000600
      *                           TMU03 TMU04                      *    00000700
      *                                                            *    00000800
      *                                                            *    00001200
      *           POUR L'ADMINISTATION DES DONNEES                 *    00001300
      **************************************************************    00001400
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00001500
      **************************************************************    00001600
      *                                                                 00001700
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00001800
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00001900
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00002000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00002100
      *                                                                 00002200
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00002300
      * COMPRENANT :                                                    00002400
      * 1 - LES ZONES RESERVEES A AIDA                                  00002500
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00002600
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00002700
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00002800
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00002900
      *                                                                 00003000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00003100
      * PAR AIDA                                                        00003200
      *                                                                 00003300
      *-------------------------------------------------------------    00003400
      *                                                                 00003500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-MU00-LONG-COMMAREA PIC S9(4) COMP VALUE +8192.           00003600
      *--                                                                       
       01  COM-MU00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +8192.                 
      *}                                                                        
      *                                                                 00003700
       01  Z-COMMAREA.                                                  00003800
      *                                                                 00003900
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00004000
          02 FILLER-COM-AIDA      PIC X(100).                           00004100
      *                                                                 00004200
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00004300
          02 COMM-CICS-APPLID     PIC X(8).                             00004400
          02 COMM-CICS-NETNAM     PIC X(8).                             00004500
          02 COMM-CICS-TRANSA     PIC X(4).                             00004600
      *                                                                 00004700
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00004800
          02 COMM-DATE-SIECLE     PIC XX.                               00004900
          02 COMM-DATE-ANNEE      PIC XX.                               00005000
          02 COMM-DATE-MOIS       PIC XX.                               00005100
          02 COMM-DATE-JOUR       PIC XX.                               00005200
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00005300
          02 COMM-DATE-QNTA       PIC 999.                              00005400
          02 COMM-DATE-QNT0       PIC 99999.                            00005500
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00005600
          02 COMM-DATE-BISX       PIC 9.                                00005700
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00005800
          02 COMM-DATE-JSM        PIC 9.                                00005900
      *   LIBELLES DU JOUR COURT - LONG                                 00006000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00006100
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00006200
      *   LIBELLES DU MOIS COURT - LONG                                 00006300
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00006400
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00006500
      *   DIFFERENTES FORMES DE DATE                                    00006600
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00006700
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00006800
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00006900
          02 COMM-DATE-JJMMAA     PIC X(6).                             00007000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00007100
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00007200
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00007300
          02 COMM-DATE-WEEK.                                            00007400
             05  COMM-DATE-SEMSS  PIC 99.                               00007500
             05  COMM-DATE-SEMAA  PIC 99.                               00007600
             05  COMM-DATE-SEMNU  PIC 99.                               00007700
          02 COMM-DATE-FILLER     PIC X(08).                            00007800
      *                                                                 00007900
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00008000
      *                                                                 00008100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00008200
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00008300
      *                                                                 00008400
      * ZONES DE COMMAREA CHARGEES PAR LE PROG TMU00 ------------- 113  00008500
      *                                                                 00008600
          02 COMM-MU00-ANCRAGE.                                         00008700
      *                                                                 00008800
              03 COMM-MU00-SOC1              PIC X(3).                  00008900
              03 COMM-MU00-LIEU1             PIC X(3).                  00009000
              03 COMM-MU00-SOC2              PIC X(3).                  00009100
              03 COMM-MU00-LIEU2             PIC X(3).                  00009200
              03 COMM-MU00-SELART            PIC X(5).                  00009300
              03 COMM-MU00-DEBGRP            PIC X(5).                  00009310
              03 COMM-MU00-DESMUT            PIC X(1).                  00009310
              03 COMM-MU00-DEBSEM.                                      00009400
                 05  COMM-MU00-DEBSEM-SS         PIC XX.                00009500
                 05  COMM-MU00-DEBSEM-AA         PIC XX.                00009600
                 05  COMM-MU00-DEBSEM-WW         PIC XX.                00009700
              03 COMM-MU00-JOUR.                                        00009800
                 05  COMM-MU00-JOUR-JJ           PIC XX.                00009900
                 05  COMM-MU00-JOUR-MM           PIC XX.                00010000
                 05  COMM-MU00-JOUR-AA           PIC XX.                00010100
              03 COMM-MU00-JOUR-SAMJ.                                   00010200
                 05  COMM-MU00-JOUR-SAMJ-S       PIC XX.                00010300
                 05  COMM-MU00-JOUR-SAMJ-A       PIC XX.                00010400
                 05  COMM-MU00-JOUR-SAMJ-M       PIC XX.                00010500
                 05  COMM-MU00-JOUR-SAMJ-J       PIC XX.                00010600
              03 COMM-MU00-JOUR-OUT.                                    00010700
                 05  COMM-MU00-JOUR-OUT-J       PIC XX.                 00010800
                 05  FILLER                     PIC X VALUE '/'.        00010900
                 05  COMM-MU00-JOUR-OUT-M       PIC XX.                 00011000
                 05  FILLER                     PIC X VALUE '/'.        00011100
                 05  COMM-MU00-JOUR-OUT-A       PIC XX.                 00011200
              03 COMM-MU00-DDEBUT.                                      00011300
                 05  COMM-MU00-DDEBUT-JJ         PIC XX.                00011400
                 05  COMM-MU00-DDEBUT-MM         PIC XX.                00011500
                 05  COMM-MU00-DDEBUT-AA         PIC XX.                00011600
              03 COMM-MU00-DDEBUT-SAMJ.                                 00011700
                 05  COMM-MU00-DDEBUT-SAMJ-S       PIC XX.              00011800
                 05  COMM-MU00-DDEBUT-SAMJ-A       PIC XX.              00011900
                 05  COMM-MU00-DDEBUT-SAMJ-M       PIC XX.              00012000
                 05  COMM-MU00-DDEBUT-SAMJ-J       PIC XX.              00012100
              03 COMM-MU00-DDEBUT-OUT.                                  00012200
                 05  COMM-MU00-DDEBUT-OUT-J       PIC XX.               00012300
                 05  FILLER                       PIC X VALUE '/'.      00012400
                 05  COMM-MU00-DDEBUT-OUT-M       PIC XX.               00012500
                 05  FILLER                       PIC X VALUE '/'.      00012600
                 05  COMM-MU00-DDEBUT-OUT-A       PIC XX.               00012700
      *                                                                 00012800
          02 COMM-MU00-LIBELLES.                                        00012900
      *                                                                 00013000
              03 COMM-MU00-LENTREPOT         PIC X(20).                 00013100
              03 COMM-MU00-LLIEU             PIC X(20).                 00013200
      *                                                                 01000005
      *-----------------------------------------------------------------01000007
      *   ZONES DE COMMAREA CHARGEES POUR LE PROG TMU03 ----------- 166501000008
      *                                                                 01000009
          02 COMM-OPTION1.                                              01000010
      *                                                                 01000011
              03 COMM-MU00-PAGE           PIC 99.                       01000012
              03 COMM-MU00-SEM.                                         01000013
                 05  COMM-MU00-SEM-1          PIC  X(4).                01000014
                 05  COMM-MU00-SEM-2          PIC  X(2).                01000015
              03 COMM-MU00-QUANT-JOUR1    PIC  9(5).                    01000016
              03 COMM-MU00-SEM-PREC.                                    01000017
                 05  COMM-MU00-SEM-PREC-1     PIC  X(4).                01000018
                 05  COMM-MU00-SEM-PREC-2     PIC  X(2).                01000019
              03 COMM-MU00-SEM-SUIV.                                    01000020
                 05  COMM-MU00-SEM-SUIV-1     PIC  X(4).                01000021
                 05  COMM-MU00-SEM-SUIV-2     PIC  X(2).                01000022
              03 COMM-MU00-INFOS-TRAIT-NORMAL OCCURS 12.                01000023
                 05  COMM-MU00-NATURE-MAJ     PIC  X.                   01000024
                 05  COMM-MU00-NBRE-PRO       PIC  9.                   01000025
              03 COMM-MU00-MUTATION OCCURS 84.                          01000026
                 05  COMM-MU00-MUT-JOUR       PIC  X(8).                01000027
                 05  COMM-MU00-MUT-CODE       PIC  X.                   01000028
              03 COMM-MU00-DESTOCK  OCCURS 84.                          01000029
                 05  COMM-MU00-DES-JOUR       PIC  X(8).                01000030
                 05  COMM-MU00-DES-CODE       PIC  X.                   01000031
              03 COMM-MU00-SOCIETE            PIC X(3).                 01000032
              03 COMM-MU00-LIEU               PIC X(3).                 01000033
              03 COMM-MU00-LIGNE-TESTEE       PIC 99.                   01000034
HA                                                                              
              03 COMM-MU00-TYPUSER        PIC X(08).                            
                 88 COMM-MU00-88-MONODEP  VALUE 'MONODEP '.                     
                 88 COMM-MU00-88-MULTIDEP VALUE 'MULTIDEP'.                     
                 88 COMM-MU00-88-MULTISOC VALUE 'MULTISOC'.                     
                 88 COMM-MU00-88-INTEGRAL VALUE 'INTEGRAL'.                     
AD01  *       03 COMM-MU00-AUTOR-DIXDEPOT.                                      
      *          05 COMM-MU00-AUTOR-UN-DEPOT OCCURS 10                          
      *                                      INDEXED BY IND1-MU00.              
      *             07 COMM-MU00-AUTOR-NSOCENTRE PIC X(3).                      
      *             07 COMM-MU00-AUTOR-NDEPOT    PIC X(3).                      
      *                                                                 01000062
AD01          03 COMM-MU00-AUTOR-VINGTDEPOTS.                                   
                 05 COMM-MU00-AUTOR-UN-DEPOT OCCURS 20                          
                                             INDEXED BY IND1-MU00.              
                    07 COMM-MU00-AUTOR-NSOCENTRE PIC X(3).                      
                    07 COMM-MU00-AUTOR-NDEPOT    PIC X(3).                      
      *                                                                 01000062
      * ZONES RESERVEES APPLICATIVES -----------------------6070- 6010  01000063
      *                                                                 01000064
             03 COMM-MU00-FILLER         PIC X(6070).                   01000070
             03 COMM-OPTION4 REDEFINES COMM-MU00-FILLER.                01000074
                04 COMM-MU00-TRICSELART             PIC X.                      
                04 COMM-MU00-TRICHARG               PIC X.                      
AD01  *         04 COMM-OPTION4-FILLER              PIC X(6068).                
AD01            04 COMM-OPTION4-FILLER              PIC X(6008).                
             03 COMM-TMU09-DONNEES   REDEFINES COMM-MU00-FILLER.        01000074
                04 COMM-TMU09.                                                  
                   05 COMM-MU09-SOC1                PIC X(3).                000
                   05 COMM-MU09-LIEU1               PIC X(3).                000
                   05 COMM-MU09-SOC2                PIC X(3).                000
                   05 COMM-MU09-LIEU2               PIC X(3).                000
                   05 COMM-MU09-LENTREPOT           PIC X(20).          00013100
                   05 COMM-MU09-LLIEU               PIC X(20).          00013200
                   05 COMM-MU09-NUMPAG              PIC 9(3).           00013200
                   05 COMM-MU09-PAGE-MAX            PIC 9(3).           00013200
                   05 COMM-MU09-MAJ-TS              PIC X(1).           00013200
AD01  *         04 COMM-TMU09-FILLER                PIC X(6011).                
AD01            04 COMM-TMU09-FILLER                PIC X(5951).                
      *                                                                 01000062
      * ZONES RESERVEES APPLICATIVES ---------------------------- 6138  01000063
      *                                                                 01000064
      *      03 COMM-MU00-FILLER         PIC X(6138).                   01000070
      *                                                                 01000071
      * ZONES APPLICATIVES TL60 =================================  173  01000072
      * ZONES ISSUE DE LA SAISIE DE L'ECRAN ETL60                       01000073
             03 COMM-OPTION6 REDEFINES COMM-MU00-FILLER.                01000074
                                                                        01000075
                05 COMM-TL60-PAGINATION.                                01000076
                   10 XX               PIC 9(2).                        01000077
                   10 YY               PIC 9(2).                        01000078
                   10 TOTXX            PIC 9(2).                        01000079
                   10 TOTYY            PIC 9(2).                        01000080
                05 COMM-TL60-DEBSEM.                                    01000081
                   10 COMM-TL60-DEBSSAA.                                01000082
                      15 COMM-TL60-DEBSS PIC X(2).                      01000083
                      15 COMM-TL60-DEBAA PIC X(2).                      01000084
                   10 COMM-TL60-DEBNU  PIC X(2).                        01000085
                05 COMM-TL60-NSOC      PIC X(3).                        01000086
                05 COMM-TL60-LIEU      PIC X(3).                        01000087
                05 COMM-TL60-PLAN      PIC X(5).                        01000088
                05 COMM-TL60-LIGNE-F   PIC X     OCCURS 8.              01000089
                05 COMM-TL60-COLONNE             OCCURS 20.             01000090
                   10 COMM-TL60-COLONNE-F PIC X  OCCURS 7.              01000091
      *                                                                 01000100
      *                                                                 01000200
      * ZONES APPLICATIVES TMU00 POUR DUPLICATION CALENDRIER            01000300
      * MODIF HA 01/98 OPTION 5 DE TMU00                                01000400
      *                                                                 01000410
      *                                                                 01000420
             03 COMM-CALENDRIER REDEFINES COMM-MU00-FILLER.             01000500
                                                                        01000600
                05 COMM-MU00-SELARR    PIC X(5).                        01000700
                05 COMM-MU00-SEMDER.                                    01000710
                   10  COMM-MU00-SEMDER-SS         PIC XX.              01000711
                   10  COMM-MU00-SEMDER-AA         PIC XX.              01000712
                   10  COMM-MU00-SEMDER-WW         PIC XX.              01000713
                05 COMM-MU00-SEMFIR.                                    01000720
                   10  COMM-MU00-SEMFIR-SS         PIC XX.              01000721
                   10  COMM-MU00-SEMFIR-AA         PIC XX.              01000722
                   10  COMM-MU00-SEMFIR-WW         PIC XX.              01000723
                05 COMM-MU00-NBSEMR    PIC 9(2).                        01000730
                05 COMM-MU00-DUPQUR    PIC X(1).                        01000740
      *                                                                 01002300
                                                                                
