      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGD3500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGD3500                         
      **********************************************************                
       01  RVGD3500.                                                            
           02  GD35-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GD35-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GD35-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  GD35-WSEQ                                                        
               PIC S9(5) COMP-3.                                                
           02  GD35-CZONED                                                      
               PIC X(0002).                                                     
           02  GD35-CNIVEAUD1                                                   
               PIC X(0002).                                                     
           02  GD35-CNIVEAUF1                                                   
               PIC X(0002).                                                     
           02  GD35-NPOSITIOND1                                                 
               PIC X(0003).                                                     
           02  GD35-NPOSITIONF1                                                 
               PIC X(0003).                                                     
           02  GD35-CZONEF                                                      
               PIC X(0002).                                                     
           02  GD35-CNIVEAUD2                                                   
               PIC X(0002).                                                     
           02  GD35-CNIVEAUF2                                                   
               PIC X(0002).                                                     
           02  GD35-NPOSITIOND2                                                 
               PIC X(0003).                                                     
           02  GD35-NPOSITIONF2                                                 
               PIC X(0003).                                                     
           02  GD35-CRUPTURE                                                    
               PIC X(0001).                                                     
           02  GD35-WTYPEMPL                                                    
               PIC X(0001).                                                     
           02  GD35-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGD3500                                  
      **********************************************************                
       01  RVGD3500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-WSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-WSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-CZONED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-CZONED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-CNIVEAUD1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-CNIVEAUD1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-CNIVEAUF1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-CNIVEAUF1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-NPOSITIOND1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-NPOSITIOND1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-NPOSITIONF1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-NPOSITIONF1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-CZONEF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-CZONEF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-CNIVEAUD2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-CNIVEAUD2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-CNIVEAUF2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-CNIVEAUF2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-NPOSITIOND2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-NPOSITIOND2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-NPOSITIONF2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-NPOSITIONF2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-CRUPTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-CRUPTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-WTYPEMPL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD35-WTYPEMPL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD35-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GD35-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
