      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU22   EMU22                                              00000020
      ***************************************************************** 00000030
       01   EMU22I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGESI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENTL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCENTF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCENTI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTATIL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNMUTATIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMUTATIF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNMUTATII      PIC X(7).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNSOCIETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCIETF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCIETI      PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNLIEUI   PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLLIEUI   PIC X(20).                                      00000450
           02 M8I OCCURS   11 TIMES .                                   00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNCODICI     PIC X(7).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCFAMI  PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCMARQI      PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOUL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLREFFOUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLREFFOUF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLREFFOUI    PIC X(20).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEDEML     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MQTEDEML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTEDEMF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQTEDEMI     PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEDESTL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MQTEDESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQTEDESTF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQTEDESTI    PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRAFALEL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNRAFALEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNRAFALEF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNRAFALEI    PIC X(3).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRAFALEL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDRAFALEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDRAFALEF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDRAFALEI    PIC X(8).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODIC1L      COMP PIC S9(4).                            00000790
      *--                                                                       
           02 MNCODIC1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODIC1F      PIC X.                                     00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNCODIC1I      PIC X(7).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCFAM1I   PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQ1L  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQ1F  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCMARQ1I  PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFF1L  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLREFF1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLREFF1F  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLREFF1I  PIC X(20).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEDEM1L      COMP PIC S9(4).                            00000950
      *--                                                                       
           02 MQTEDEM1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQTEDEM1F      PIC X.                                     00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MQTEDEM1I      PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEDES1L      COMP PIC S9(4).                            00000990
      *--                                                                       
           02 MQTEDES1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQTEDES1F      PIC X.                                     00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MQTEDES1I      PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRAFAL1L      COMP PIC S9(4).                            00001030
      *--                                                                       
           02 MNRAFAL1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNRAFAL1F      PIC X.                                     00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MNRAFAL1I      PIC X(2).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRAFAL1L      COMP PIC S9(4).                            00001070
      *--                                                                       
           02 MDRAFAL1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRAFAL1F      PIC X.                                     00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MDRAFAL1I      PIC X(8).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODIC2L      COMP PIC S9(4).                            00001110
      *--                                                                       
           02 MNCODIC2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODIC2F      PIC X.                                     00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNCODIC2I      PIC X(7).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM2L   COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCFAM2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM2F   PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCFAM2I   PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQ2L  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCMARQ2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQ2F  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCMARQ2I  PIC X(5).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFF2L  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MLREFF2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLREFF2F  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MLREFF2I  PIC X(20).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEDEM2L      COMP PIC S9(4).                            00001270
      *--                                                                       
           02 MQTEDEM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQTEDEM2F      PIC X.                                     00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MQTEDEM2I      PIC X(5).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEDES2L      COMP PIC S9(4).                            00001310
      *--                                                                       
           02 MQTEDES2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQTEDES2F      PIC X.                                     00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MQTEDES2I      PIC X(5).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRAFAL2L      COMP PIC S9(4).                            00001350
      *--                                                                       
           02 MNRAFAL2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNRAFAL2F      PIC X.                                     00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MNRAFAL2I      PIC X(2).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRAFAL2L      COMP PIC S9(4).                            00001390
      *--                                                                       
           02 MDRAFAL2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRAFAL2F      PIC X.                                     00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MDRAFAL2I      PIC X(8).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MZONCMDI  PIC X(12).                                      00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MLIBERRI  PIC X(61).                                      00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MCODTRAI  PIC X(4).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MCICSI    PIC X(5).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MNETNAMI  PIC X(8).                                       00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MSCREENI  PIC X(4).                                       00001660
      ***************************************************************** 00001670
      * SDF: EMU22   EMU22                                              00001680
      ***************************************************************** 00001690
       01   EMU22O REDEFINES EMU22I.                                    00001700
           02 FILLER    PIC X(12).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MDATJOUA  PIC X.                                          00001730
           02 MDATJOUC  PIC X.                                          00001740
           02 MDATJOUP  PIC X.                                          00001750
           02 MDATJOUH  PIC X.                                          00001760
           02 MDATJOUV  PIC X.                                          00001770
           02 MDATJOUO  PIC X(10).                                      00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MTIMJOUA  PIC X.                                          00001800
           02 MTIMJOUC  PIC X.                                          00001810
           02 MTIMJOUP  PIC X.                                          00001820
           02 MTIMJOUH  PIC X.                                          00001830
           02 MTIMJOUV  PIC X.                                          00001840
           02 MTIMJOUO  PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MPAGEA    PIC X.                                          00001870
           02 MPAGEC    PIC X.                                          00001880
           02 MPAGEP    PIC X.                                          00001890
           02 MPAGEH    PIC X.                                          00001900
           02 MPAGEV    PIC X.                                          00001910
           02 MPAGEO    PIC X(3).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MNBPAGESA      PIC X.                                     00001940
           02 MNBPAGESC PIC X.                                          00001950
           02 MNBPAGESP PIC X.                                          00001960
           02 MNBPAGESH PIC X.                                          00001970
           02 MNBPAGESV PIC X.                                          00001980
           02 MNBPAGESO      PIC X(3).                                  00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MNSOCENTA      PIC X.                                     00002010
           02 MNSOCENTC PIC X.                                          00002020
           02 MNSOCENTP PIC X.                                          00002030
           02 MNSOCENTH PIC X.                                          00002040
           02 MNSOCENTV PIC X.                                          00002050
           02 MNSOCENTO      PIC X(3).                                  00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MNDEPOTA  PIC X.                                          00002080
           02 MNDEPOTC  PIC X.                                          00002090
           02 MNDEPOTP  PIC X.                                          00002100
           02 MNDEPOTH  PIC X.                                          00002110
           02 MNDEPOTV  PIC X.                                          00002120
           02 MNDEPOTO  PIC X(3).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MNMUTATIA      PIC X.                                     00002150
           02 MNMUTATIC PIC X.                                          00002160
           02 MNMUTATIP PIC X.                                          00002170
           02 MNMUTATIH PIC X.                                          00002180
           02 MNMUTATIV PIC X.                                          00002190
           02 MNMUTATIO      PIC X(7).                                  00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MNSOCIETA      PIC X.                                     00002220
           02 MNSOCIETC PIC X.                                          00002230
           02 MNSOCIETP PIC X.                                          00002240
           02 MNSOCIETH PIC X.                                          00002250
           02 MNSOCIETV PIC X.                                          00002260
           02 MNSOCIETO      PIC X(3).                                  00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MNLIEUA   PIC X.                                          00002290
           02 MNLIEUC   PIC X.                                          00002300
           02 MNLIEUP   PIC X.                                          00002310
           02 MNLIEUH   PIC X.                                          00002320
           02 MNLIEUV   PIC X.                                          00002330
           02 MNLIEUO   PIC X(3).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MLLIEUA   PIC X.                                          00002360
           02 MLLIEUC   PIC X.                                          00002370
           02 MLLIEUP   PIC X.                                          00002380
           02 MLLIEUH   PIC X.                                          00002390
           02 MLLIEUV   PIC X.                                          00002400
           02 MLLIEUO   PIC X(20).                                      00002410
           02 M8O OCCURS   11 TIMES .                                   00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MNCODICA     PIC X.                                     00002440
             03 MNCODICC     PIC X.                                     00002450
             03 MNCODICP     PIC X.                                     00002460
             03 MNCODICH     PIC X.                                     00002470
             03 MNCODICV     PIC X.                                     00002480
             03 MNCODICO     PIC X(7).                                  00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MCFAMA  PIC X.                                          00002510
             03 MCFAMC  PIC X.                                          00002520
             03 MCFAMP  PIC X.                                          00002530
             03 MCFAMH  PIC X.                                          00002540
             03 MCFAMV  PIC X.                                          00002550
             03 MCFAMO  PIC X(5).                                       00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MCMARQA      PIC X.                                     00002580
             03 MCMARQC PIC X.                                          00002590
             03 MCMARQP PIC X.                                          00002600
             03 MCMARQH PIC X.                                          00002610
             03 MCMARQV PIC X.                                          00002620
             03 MCMARQO      PIC X(5).                                  00002630
             03 FILLER       PIC X(2).                                  00002640
             03 MLREFFOUA    PIC X.                                     00002650
             03 MLREFFOUC    PIC X.                                     00002660
             03 MLREFFOUP    PIC X.                                     00002670
             03 MLREFFOUH    PIC X.                                     00002680
             03 MLREFFOUV    PIC X.                                     00002690
             03 MLREFFOUO    PIC X(20).                                 00002700
             03 FILLER       PIC X(2).                                  00002710
             03 MQTEDEMA     PIC X.                                     00002720
             03 MQTEDEMC     PIC X.                                     00002730
             03 MQTEDEMP     PIC X.                                     00002740
             03 MQTEDEMH     PIC X.                                     00002750
             03 MQTEDEMV     PIC X.                                     00002760
             03 MQTEDEMO     PIC X(5).                                  00002770
             03 FILLER       PIC X(2).                                  00002780
             03 MQTEDESTA    PIC X.                                     00002790
             03 MQTEDESTC    PIC X.                                     00002800
             03 MQTEDESTP    PIC X.                                     00002810
             03 MQTEDESTH    PIC X.                                     00002820
             03 MQTEDESTV    PIC X.                                     00002830
             03 MQTEDESTO    PIC X(5).                                  00002840
             03 FILLER       PIC X(2).                                  00002850
             03 MNRAFALEA    PIC X.                                     00002860
             03 MNRAFALEC    PIC X.                                     00002870
             03 MNRAFALEP    PIC X.                                     00002880
             03 MNRAFALEH    PIC X.                                     00002890
             03 MNRAFALEV    PIC X.                                     00002900
             03 MNRAFALEO    PIC X(3).                                  00002910
             03 FILLER       PIC X(2).                                  00002920
             03 MDRAFALEA    PIC X.                                     00002930
             03 MDRAFALEC    PIC X.                                     00002940
             03 MDRAFALEP    PIC X.                                     00002950
             03 MDRAFALEH    PIC X.                                     00002960
             03 MDRAFALEV    PIC X.                                     00002970
             03 MDRAFALEO    PIC X(8).                                  00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MNCODIC1A      PIC X.                                     00003000
           02 MNCODIC1C PIC X.                                          00003010
           02 MNCODIC1P PIC X.                                          00003020
           02 MNCODIC1H PIC X.                                          00003030
           02 MNCODIC1V PIC X.                                          00003040
           02 MNCODIC1O      PIC X(7).                                  00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MCFAM1A   PIC X.                                          00003070
           02 MCFAM1C   PIC X.                                          00003080
           02 MCFAM1P   PIC X.                                          00003090
           02 MCFAM1H   PIC X.                                          00003100
           02 MCFAM1V   PIC X.                                          00003110
           02 MCFAM1O   PIC X(5).                                       00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MCMARQ1A  PIC X.                                          00003140
           02 MCMARQ1C  PIC X.                                          00003150
           02 MCMARQ1P  PIC X.                                          00003160
           02 MCMARQ1H  PIC X.                                          00003170
           02 MCMARQ1V  PIC X.                                          00003180
           02 MCMARQ1O  PIC X(5).                                       00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MLREFF1A  PIC X.                                          00003210
           02 MLREFF1C  PIC X.                                          00003220
           02 MLREFF1P  PIC X.                                          00003230
           02 MLREFF1H  PIC X.                                          00003240
           02 MLREFF1V  PIC X.                                          00003250
           02 MLREFF1O  PIC X(20).                                      00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MQTEDEM1A      PIC X.                                     00003280
           02 MQTEDEM1C PIC X.                                          00003290
           02 MQTEDEM1P PIC X.                                          00003300
           02 MQTEDEM1H PIC X.                                          00003310
           02 MQTEDEM1V PIC X.                                          00003320
           02 MQTEDEM1O      PIC X(5).                                  00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MQTEDES1A      PIC X.                                     00003350
           02 MQTEDES1C PIC X.                                          00003360
           02 MQTEDES1P PIC X.                                          00003370
           02 MQTEDES1H PIC X.                                          00003380
           02 MQTEDES1V PIC X.                                          00003390
           02 MQTEDES1O      PIC X(5).                                  00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MNRAFAL1A      PIC X.                                     00003420
           02 MNRAFAL1C PIC X.                                          00003430
           02 MNRAFAL1P PIC X.                                          00003440
           02 MNRAFAL1H PIC X.                                          00003450
           02 MNRAFAL1V PIC X.                                          00003460
           02 MNRAFAL1O      PIC X(2).                                  00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MDRAFAL1A      PIC X.                                     00003490
           02 MDRAFAL1C PIC X.                                          00003500
           02 MDRAFAL1P PIC X.                                          00003510
           02 MDRAFAL1H PIC X.                                          00003520
           02 MDRAFAL1V PIC X.                                          00003530
           02 MDRAFAL1O      PIC X(8).                                  00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MNCODIC2A      PIC X.                                     00003560
           02 MNCODIC2C PIC X.                                          00003570
           02 MNCODIC2P PIC X.                                          00003580
           02 MNCODIC2H PIC X.                                          00003590
           02 MNCODIC2V PIC X.                                          00003600
           02 MNCODIC2O      PIC X(7).                                  00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MCFAM2A   PIC X.                                          00003630
           02 MCFAM2C   PIC X.                                          00003640
           02 MCFAM2P   PIC X.                                          00003650
           02 MCFAM2H   PIC X.                                          00003660
           02 MCFAM2V   PIC X.                                          00003670
           02 MCFAM2O   PIC X(5).                                       00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MCMARQ2A  PIC X.                                          00003700
           02 MCMARQ2C  PIC X.                                          00003710
           02 MCMARQ2P  PIC X.                                          00003720
           02 MCMARQ2H  PIC X.                                          00003730
           02 MCMARQ2V  PIC X.                                          00003740
           02 MCMARQ2O  PIC X(5).                                       00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MLREFF2A  PIC X.                                          00003770
           02 MLREFF2C  PIC X.                                          00003780
           02 MLREFF2P  PIC X.                                          00003790
           02 MLREFF2H  PIC X.                                          00003800
           02 MLREFF2V  PIC X.                                          00003810
           02 MLREFF2O  PIC X(20).                                      00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MQTEDEM2A      PIC X.                                     00003840
           02 MQTEDEM2C PIC X.                                          00003850
           02 MQTEDEM2P PIC X.                                          00003860
           02 MQTEDEM2H PIC X.                                          00003870
           02 MQTEDEM2V PIC X.                                          00003880
           02 MQTEDEM2O      PIC X(5).                                  00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MQTEDES2A      PIC X.                                     00003910
           02 MQTEDES2C PIC X.                                          00003920
           02 MQTEDES2P PIC X.                                          00003930
           02 MQTEDES2H PIC X.                                          00003940
           02 MQTEDES2V PIC X.                                          00003950
           02 MQTEDES2O      PIC X(5).                                  00003960
           02 FILLER    PIC X(2).                                       00003970
           02 MNRAFAL2A      PIC X.                                     00003980
           02 MNRAFAL2C PIC X.                                          00003990
           02 MNRAFAL2P PIC X.                                          00004000
           02 MNRAFAL2H PIC X.                                          00004010
           02 MNRAFAL2V PIC X.                                          00004020
           02 MNRAFAL2O      PIC X(2).                                  00004030
           02 FILLER    PIC X(2).                                       00004040
           02 MDRAFAL2A      PIC X.                                     00004050
           02 MDRAFAL2C PIC X.                                          00004060
           02 MDRAFAL2P PIC X.                                          00004070
           02 MDRAFAL2H PIC X.                                          00004080
           02 MDRAFAL2V PIC X.                                          00004090
           02 MDRAFAL2O      PIC X(8).                                  00004100
           02 FILLER    PIC X(2).                                       00004110
           02 MZONCMDA  PIC X.                                          00004120
           02 MZONCMDC  PIC X.                                          00004130
           02 MZONCMDP  PIC X.                                          00004140
           02 MZONCMDH  PIC X.                                          00004150
           02 MZONCMDV  PIC X.                                          00004160
           02 MZONCMDO  PIC X(12).                                      00004170
           02 FILLER    PIC X(2).                                       00004180
           02 MLIBERRA  PIC X.                                          00004190
           02 MLIBERRC  PIC X.                                          00004200
           02 MLIBERRP  PIC X.                                          00004210
           02 MLIBERRH  PIC X.                                          00004220
           02 MLIBERRV  PIC X.                                          00004230
           02 MLIBERRO  PIC X(61).                                      00004240
           02 FILLER    PIC X(2).                                       00004250
           02 MCODTRAA  PIC X.                                          00004260
           02 MCODTRAC  PIC X.                                          00004270
           02 MCODTRAP  PIC X.                                          00004280
           02 MCODTRAH  PIC X.                                          00004290
           02 MCODTRAV  PIC X.                                          00004300
           02 MCODTRAO  PIC X(4).                                       00004310
           02 FILLER    PIC X(2).                                       00004320
           02 MCICSA    PIC X.                                          00004330
           02 MCICSC    PIC X.                                          00004340
           02 MCICSP    PIC X.                                          00004350
           02 MCICSH    PIC X.                                          00004360
           02 MCICSV    PIC X.                                          00004370
           02 MCICSO    PIC X(5).                                       00004380
           02 FILLER    PIC X(2).                                       00004390
           02 MNETNAMA  PIC X.                                          00004400
           02 MNETNAMC  PIC X.                                          00004410
           02 MNETNAMP  PIC X.                                          00004420
           02 MNETNAMH  PIC X.                                          00004430
           02 MNETNAMV  PIC X.                                          00004440
           02 MNETNAMO  PIC X(8).                                       00004450
           02 FILLER    PIC X(2).                                       00004460
           02 MSCREENA  PIC X.                                          00004470
           02 MSCREENC  PIC X.                                          00004480
           02 MSCREENP  PIC X.                                          00004490
           02 MSCREENH  PIC X.                                          00004500
           02 MSCREENV  PIC X.                                          00004510
           02 MSCREENO  PIC X(4).                                       00004520
                                                                                
