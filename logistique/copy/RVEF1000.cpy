      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVEF1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEF1000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVEF1000.                                                            
           02  EF10-CTRAIT                                                      
               PIC X(0005).                                                     
           02  EF10-CTIERS                                                      
               PIC X(0005).                                                     
           02  EF10-NRENDU                                                      
               PIC X(0020).                                                     
           02  EF10-DRENDU                                                      
               PIC X(0008).                                                     
           02  EF10-CRENDU                                                      
               PIC X(0005).                                                     
           02  EF10-CIMPUT                                                      
               PIC X(0001).                                                     
           02  EF10-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  EF10-DANNUL                                                      
               PIC X(0008).                                                     
           02  EF10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  EF10-NBIMPUT                                                     
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEF1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVEF1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF10-CTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF10-CTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF10-CTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF10-CTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF10-NRENDU-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF10-NRENDU-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF10-DRENDU-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF10-DRENDU-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF10-CRENDU-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF10-CRENDU-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF10-CIMPUT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF10-CIMPUT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF10-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF10-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF10-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF10-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF10-NBIMPUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF10-NBIMPUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
