      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *             TRANSACTION "GS00" / PROGRAMME TGS17              *         
      *   TS : CONSULTATION DES STOCKS ENTREPOT (ORIENTEE PLATEFORME) *         
      *****************************************************************         
      *                                                               *         
       01  TS-GS17.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GS17-LONG                 PIC S9(3) COMP-3                     
                                           VALUE +61.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GS17-DONNEES.                                                  
      *----------------------------------  CODE DEBUT PAGE                      
              03 TS-GS17-DEBPAGE           PIC X(1).                            
      *----------------------------------  CODE ARTICLE                         
              03 TS-GS17-NCODIC            PIC X(7).                            
      *----------------------------------  CODE FAMILLE                         
              03 TS-GS17-CFAM              PIC X(5).                            
      *----------------------------------  CODE MARQUE                          
              03 TS-GS17-CMARQ             PIC X(5).                            
      *----------------------------------  LIBELLE REFERENCE                    
              03 TS-GS17-LREFFOURN         PIC X(20).                           
      *----------------------------------  CODE STATUT COMPACTE                 
              03 TS-GS17-LSTATCOMP         PIC X(3).                            
      *----------------------------------  CODE SENSIBILITE APPRO               
              03 TS-GS17-WSENSAPPRO        PIC X(1).                            
      *----------------------------------  CODE SENSIBILITE VENTE               
              03 TS-GS17-WSENSVTE          PIC X(1).                            
      *----------------------------------  ZONE STOCKS                          
              03 TS-GS17-ZONSTOCK.                                              
                 04 TS-GS17-QSTOCK1        PIC S9(5) COMP-3 OCCURS 3.           
                                                                                
