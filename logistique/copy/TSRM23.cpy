      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM23                                          *  00020002
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-LONG              PIC S9(3) COMP-3 VALUE 120.             00060008
       01  TS-DONNEES.                                                  00070000
              10 TS-RANG         PIC 9(3).                              00080001
              10 TS-NCODIC       PIC X(7).                              00090000
              10 TS-MCMARQ       PIC X(5).                              00100000
              10 TS-SENSAPPRO    PIC X.                                 00110000
              10 TS-MQV4S        PIC S9(5).                             00120006
              10 TS-MQV3S        PIC S9(5).                             00130006
              10 TS-MQV2S        PIC S9(5).                             00140006
              10 TS-MQV1S        PIC S9(5).                             00150006
              10 TS-MQR1S        PIC S9(5).                             00160006
              10 TS-MQR2S        PIC S9(5).                             00170006
              10 TS-MQR3S        PIC S9(5).                             00180006
              10 TS-STOCK        PIC X.                                 00190000
              10 TS-MODIF-GA00   PIC X.                                 00200000
              10 TS-REF          PIC X(20).                             00210005
              10 TS-MQSV4D       PIC S9(5).                             00220007
              10 TS-MQSV3D       PIC S9(5).                             00230007
              10 TS-MQSV2D       PIC S9(5).                             00240007
              10 TS-MQSV1D       PIC S9(5).                             00250007
              10 TS-MQPV0D       PIC S9(5).                             00260006
              10 TS-MQPV0D-OK    PIC X.                                 00270008
              10 TS-QPV1F        PIC X(5).                              00280004
              10 TS-INSERT1      PIC X.                                 00290000
              10 TS-MODIF1       PIC X.                                 00300000
              10 TS-QPV2F        PIC X(5).                              00310004
              10 TS-INSERT2      PIC X.                                 00320000
              10 TS-MODIF2       PIC X.                                 00330000
              10 TS-QPV3F        PIC X(5).                              00340004
              10 TS-INSERT3      PIC X.                                 00350000
              10 TS-MODIF3       PIC X.                                 00360000
                                                                                
