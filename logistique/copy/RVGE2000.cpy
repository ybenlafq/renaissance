      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGE2000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGE2000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGE2000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGE2000.                                                            
      *}                                                                        
           02  GE20-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GE20-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GE20-CAIRE                                                       
               PIC X(0002).                                                     
           02  GE20-QLGMAILLE                                                   
               PIC S9(3) COMP-3.                                                
           02  GE20-QPOSITION                                                   
               PIC S9(3) COMP-3.                                                
           02  GE20-QLGCRAN                                                     
               PIC S9(3) COMP-3.                                                
           02  GE20-QCRAN                                                       
               PIC S9(3) COMP-3.                                                
           02  GE20-CCOTE1                                                      
               PIC X(0002).                                                     
           02  GE20-QFONDEMP1                                                   
               PIC S9(3) COMP-3.                                                
           02  GE20-CCOTE2                                                      
               PIC X(0002).                                                     
           02  GE20-QFONDEMP2                                                   
               PIC S9(3) COMP-3.                                                
           02  GE20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGE2000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGE2000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGE2000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE20-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE20-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-CAIRE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE20-CAIRE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-QLGMAILLE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE20-QLGMAILLE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-QPOSITION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE20-QPOSITION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-QLGCRAN-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE20-QLGCRAN-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-QCRAN-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE20-QCRAN-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-CCOTE1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE20-CCOTE1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-QFONDEMP1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE20-QFONDEMP1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-CCOTE2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE20-CCOTE2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-QFONDEMP2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE20-QFONDEMP2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GE20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
