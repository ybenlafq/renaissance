      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EZQ30   EZQ30                                              00000020
      ***************************************************************** 00000030
       01   EZQ30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(9).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CPOSTALL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 CPOSTALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CPOSTALF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 CPOSTALI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CELEMENL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 CELEMENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CELEMENF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 CELEMENI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNMAGI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPLSOCL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNPLSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNPLSOCF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNPLSOCI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPLMAGL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNPLMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNPLMAGF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNPLMAGI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDELL     COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDELL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MDELF     PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDELI     PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CPLAGEL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 CPLAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 CPLAGEF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 CPLAGEI   PIC X(2).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDATDEBI  PIC X(8).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTOURNL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCTOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTOURNF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCTOURNI  PIC X(8).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNCODICI  PIC X(7).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSTATUTI  PIC X.                                          00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDATFINI  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPVTEL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MTYPVTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPVTEF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MTYPVTEI  PIC X.                                          00000730
           02 MDDELIVD OCCURS   6 TIMES .                               00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDELIVL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDDELIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDDELIVF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDDELIVI     PIC X(6).                                  00000780
           02 MPTFD OCCURS   6 TIMES .                                  00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPTFL   COMP PIC S9(4).                                 00000800
      *--                                                                       
             03 MPTFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPTFF   PIC X.                                          00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MPTFI   PIC X(3).                                       00000830
           02 MNVENTED OCCURS   6 TIMES .                               00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MNVENTEI     PIC X(7).                                  00000880
           02 MNCODICLD OCCURS   6 TIMES .                              00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICLL    COMP PIC S9(4).                            00000900
      *--                                                                       
             03 MNCODICLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNCODICLF    PIC X.                                     00000910
             03 FILLER  PIC X(4).                                       00000920
             03 MNCODICLI    PIC X(7).                                  00000930
           02 MCFAMD OCCURS   6 TIMES .                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MCFAMI  PIC X(5).                                       00000980
           02 MMDELD OCCURS   6 TIMES .                                 00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMDELL  COMP PIC S9(4).                                 00001000
      *--                                                                       
             03 MMDELL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MMDELF  PIC X.                                          00001010
             03 FILLER  PIC X(4).                                       00001020
             03 MMDELI  PIC X(3).                                       00001030
           02 MNQTED OCCURS   6 TIMES .                                 00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNQTEL  COMP PIC S9(4).                                 00001050
      *--                                                                       
             03 MNQTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNQTEF  PIC X.                                          00001060
             03 FILLER  PIC X(4).                                       00001070
             03 MNQTEI  PIC X(5).                                       00001080
           02 MNMUTD OCCURS   6 TIMES .                                 00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTL  COMP PIC S9(4).                                 00001100
      *--                                                                       
             03 MNMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMUTF  PIC X.                                          00001110
             03 FILLER  PIC X(4).                                       00001120
             03 MNMUTI  PIC X(7).                                       00001130
           02 MNTOURND OCCURS   6 TIMES .                               00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNTOURNL     COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MNTOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNTOURNF     PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MNTOURNI     PIC X(8).                                  00001180
           02 MPTFLIEUD OCCURS   6 TIMES .                              00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPTFLIEUL    COMP PIC S9(4).                            00001200
      *--                                                                       
             03 MPTFLIEUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPTFLIEUF    PIC X.                                     00001210
             03 FILLER  PIC X(4).                                       00001220
             03 MPTFLIEUI    PIC X(3).                                  00001230
           02 M-MAGAD OCCURS   6 TIMES .                                00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M-MAGAL      COMP PIC S9(4).                            00001250
      *--                                                                       
             03 M-MAGAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 M-MAGAF      PIC X.                                     00001260
             03 FILLER  PIC X(4).                                       00001270
             03 M-MAGAI      PIC X(3).                                  00001280
           02 MCMARQD OCCURS   6 TIMES .                                00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00001300
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00001310
             03 FILLER  PIC X(4).                                       00001320
             03 MCMARQI      PIC X(5).                                  00001330
           02 MREFD OCCURS   6 TIMES .                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00001350
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MREFI   PIC X(20).                                      00001380
           02 MCSTATUTD OCCURS   6 TIMES .                              00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSTATUTL    COMP PIC S9(4).                            00001400
      *--                                                                       
             03 MCSTATUTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCSTATUTF    PIC X.                                     00001410
             03 FILLER  PIC X(4).                                       00001420
             03 MCSTATUTI    PIC X.                                     00001430
           02 MNSOCDED OCCURS   6 TIMES .                               00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCDEL     COMP PIC S9(4).                            00001450
      *--                                                                       
             03 MNSOCDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNSOCDEF     PIC X.                                     00001460
             03 FILLER  PIC X(4).                                       00001470
             03 MNSOCDEI     PIC X(3).                                  00001480
           02 MNMAGDED OCCURS   6 TIMES .                               00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGDEL     COMP PIC S9(4).                            00001500
      *--                                                                       
             03 MNMAGDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNMAGDEF     PIC X.                                     00001510
             03 FILLER  PIC X(4).                                       00001520
             03 MNMAGDEI     PIC X(3).                                  00001530
           02 MNCLIENTD OCCURS   6 TIMES .                              00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCLIENTL    COMP PIC S9(4).                            00001550
      *--                                                                       
             03 MNCLIENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNCLIENTF    PIC X.                                     00001560
             03 FILLER  PIC X(4).                                       00001570
             03 MNCLIENTI    PIC X(9).                                  00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MZONCMDI  PIC X(15).                                      00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MLIBERRI  PIC X(58).                                      00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCODTRAI  PIC X(4).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MCICSI    PIC X(5).                                       00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MNETNAMI  PIC X(8).                                       00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001800
           02 FILLER    PIC X(4).                                       00001810
           02 MSCREENI  PIC X(4).                                       00001820
      ***************************************************************** 00001830
      * SDF: EZQ30   EZQ30                                              00001840
      ***************************************************************** 00001850
       01   EZQ30O REDEFINES EZQ30I.                                    00001860
           02 FILLER    PIC X(12).                                      00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MDATJOUA  PIC X.                                          00001890
           02 MDATJOUC  PIC X.                                          00001900
           02 MDATJOUP  PIC X.                                          00001910
           02 MDATJOUH  PIC X.                                          00001920
           02 MDATJOUV  PIC X.                                          00001930
           02 MDATJOUO  PIC X(10).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MTIMJOUA  PIC X.                                          00001960
           02 MTIMJOUC  PIC X.                                          00001970
           02 MTIMJOUP  PIC X.                                          00001980
           02 MTIMJOUH  PIC X.                                          00001990
           02 MTIMJOUV  PIC X.                                          00002000
           02 MTIMJOUO  PIC X(5).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MNPAGEA   PIC X.                                          00002030
           02 MNPAGEC   PIC X.                                          00002040
           02 MNPAGEP   PIC X.                                          00002050
           02 MNPAGEH   PIC X.                                          00002060
           02 MNPAGEV   PIC X.                                          00002070
           02 MNPAGEO   PIC X(9).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 CPOSTALA  PIC X.                                          00002100
           02 CPOSTALC  PIC X.                                          00002110
           02 CPOSTALP  PIC X.                                          00002120
           02 CPOSTALH  PIC X.                                          00002130
           02 CPOSTALV  PIC X.                                          00002140
           02 CPOSTALO  PIC X(5).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 CELEMENA  PIC X.                                          00002170
           02 CELEMENC  PIC X.                                          00002180
           02 CELEMENP  PIC X.                                          00002190
           02 CELEMENH  PIC X.                                          00002200
           02 CELEMENV  PIC X.                                          00002210
           02 CELEMENO  PIC X(5).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MNSOCA    PIC X.                                          00002240
           02 MNSOCC    PIC X.                                          00002250
           02 MNSOCP    PIC X.                                          00002260
           02 MNSOCH    PIC X.                                          00002270
           02 MNSOCV    PIC X.                                          00002280
           02 MNSOCO    PIC X(3).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNMAGA    PIC X.                                          00002310
           02 MNMAGC    PIC X.                                          00002320
           02 MNMAGP    PIC X.                                          00002330
           02 MNMAGH    PIC X.                                          00002340
           02 MNMAGV    PIC X.                                          00002350
           02 MNMAGO    PIC X(3).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MNPLSOCA  PIC X.                                          00002380
           02 MNPLSOCC  PIC X.                                          00002390
           02 MNPLSOCP  PIC X.                                          00002400
           02 MNPLSOCH  PIC X.                                          00002410
           02 MNPLSOCV  PIC X.                                          00002420
           02 MNPLSOCO  PIC X(3).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MNPLMAGA  PIC X.                                          00002450
           02 MNPLMAGC  PIC X.                                          00002460
           02 MNPLMAGP  PIC X.                                          00002470
           02 MNPLMAGH  PIC X.                                          00002480
           02 MNPLMAGV  PIC X.                                          00002490
           02 MNPLMAGO  PIC X(3).                                       00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MDELA     PIC X.                                          00002520
           02 MDELC     PIC X.                                          00002530
           02 MDELP     PIC X.                                          00002540
           02 MDELH     PIC X.                                          00002550
           02 MDELV     PIC X.                                          00002560
           02 MDELO     PIC X(3).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 CPLAGEA   PIC X.                                          00002590
           02 CPLAGEC   PIC X.                                          00002600
           02 CPLAGEP   PIC X.                                          00002610
           02 CPLAGEH   PIC X.                                          00002620
           02 CPLAGEV   PIC X.                                          00002630
           02 CPLAGEO   PIC X(2).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MDATDEBA  PIC X.                                          00002660
           02 MDATDEBC  PIC X.                                          00002670
           02 MDATDEBP  PIC X.                                          00002680
           02 MDATDEBH  PIC X.                                          00002690
           02 MDATDEBV  PIC X.                                          00002700
           02 MDATDEBO  PIC X(8).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCTOURNA  PIC X.                                          00002730
           02 MCTOURNC  PIC X.                                          00002740
           02 MCTOURNP  PIC X.                                          00002750
           02 MCTOURNH  PIC X.                                          00002760
           02 MCTOURNV  PIC X.                                          00002770
           02 MCTOURNO  PIC X(8).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MNCODICA  PIC X.                                          00002800
           02 MNCODICC  PIC X.                                          00002810
           02 MNCODICP  PIC X.                                          00002820
           02 MNCODICH  PIC X.                                          00002830
           02 MNCODICV  PIC X.                                          00002840
           02 MNCODICO  PIC X(7).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MSTATUTA  PIC X.                                          00002870
           02 MSTATUTC  PIC X.                                          00002880
           02 MSTATUTP  PIC X.                                          00002890
           02 MSTATUTH  PIC X.                                          00002900
           02 MSTATUTV  PIC X.                                          00002910
           02 MSTATUTO  PIC X.                                          00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MDATFINA  PIC X.                                          00002940
           02 MDATFINC  PIC X.                                          00002950
           02 MDATFINP  PIC X.                                          00002960
           02 MDATFINH  PIC X.                                          00002970
           02 MDATFINV  PIC X.                                          00002980
           02 MDATFINO  PIC X(8).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MTYPVTEA  PIC X.                                          00003010
           02 MTYPVTEC  PIC X.                                          00003020
           02 MTYPVTEP  PIC X.                                          00003030
           02 MTYPVTEH  PIC X.                                          00003040
           02 MTYPVTEV  PIC X.                                          00003050
           02 MTYPVTEO  PIC X.                                          00003060
           02 DFHMS1 OCCURS   6 TIMES .                                 00003070
             03 FILLER       PIC X(2).                                  00003080
             03 MDDELIVA     PIC X.                                     00003090
             03 MDDELIVC     PIC X.                                     00003100
             03 MDDELIVP     PIC X.                                     00003110
             03 MDDELIVH     PIC X.                                     00003120
             03 MDDELIVV     PIC X.                                     00003130
             03 MDDELIVO     PIC X(6).                                  00003140
           02 DFHMS2 OCCURS   6 TIMES .                                 00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MPTFA   PIC X.                                          00003170
             03 MPTFC   PIC X.                                          00003180
             03 MPTFP   PIC X.                                          00003190
             03 MPTFH   PIC X.                                          00003200
             03 MPTFV   PIC X.                                          00003210
             03 MPTFO   PIC X(3).                                       00003220
           02 DFHMS3 OCCURS   6 TIMES .                                 00003230
             03 FILLER       PIC X(2).                                  00003240
             03 MNVENTEA     PIC X.                                     00003250
             03 MNVENTEC     PIC X.                                     00003260
             03 MNVENTEP     PIC X.                                     00003270
             03 MNVENTEH     PIC X.                                     00003280
             03 MNVENTEV     PIC X.                                     00003290
             03 MNVENTEO     PIC X(7).                                  00003300
           02 DFHMS4 OCCURS   6 TIMES .                                 00003310
             03 FILLER       PIC X(2).                                  00003320
             03 MNCODICLA    PIC X.                                     00003330
             03 MNCODICLC    PIC X.                                     00003340
             03 MNCODICLP    PIC X.                                     00003350
             03 MNCODICLH    PIC X.                                     00003360
             03 MNCODICLV    PIC X.                                     00003370
             03 MNCODICLO    PIC X(7).                                  00003380
           02 DFHMS5 OCCURS   6 TIMES .                                 00003390
             03 FILLER       PIC X(2).                                  00003400
             03 MCFAMA  PIC X.                                          00003410
             03 MCFAMC  PIC X.                                          00003420
             03 MCFAMP  PIC X.                                          00003430
             03 MCFAMH  PIC X.                                          00003440
             03 MCFAMV  PIC X.                                          00003450
             03 MCFAMO  PIC X(5).                                       00003460
           02 DFHMS6 OCCURS   6 TIMES .                                 00003470
             03 FILLER       PIC X(2).                                  00003480
             03 MMDELA  PIC X.                                          00003490
             03 MMDELC  PIC X.                                          00003500
             03 MMDELP  PIC X.                                          00003510
             03 MMDELH  PIC X.                                          00003520
             03 MMDELV  PIC X.                                          00003530
             03 MMDELO  PIC X(3).                                       00003540
           02 DFHMS7 OCCURS   6 TIMES .                                 00003550
             03 FILLER       PIC X(2).                                  00003560
             03 MNQTEA  PIC X.                                          00003570
             03 MNQTEC  PIC X.                                          00003580
             03 MNQTEP  PIC X.                                          00003590
             03 MNQTEH  PIC X.                                          00003600
             03 MNQTEV  PIC X.                                          00003610
             03 MNQTEO  PIC X(5).                                       00003620
           02 DFHMS8 OCCURS   6 TIMES .                                 00003630
             03 FILLER       PIC X(2).                                  00003640
             03 MNMUTA  PIC X.                                          00003650
             03 MNMUTC  PIC X.                                          00003660
             03 MNMUTP  PIC X.                                          00003670
             03 MNMUTH  PIC X.                                          00003680
             03 MNMUTV  PIC X.                                          00003690
             03 MNMUTO  PIC X(7).                                       00003700
           02 DFHMS9 OCCURS   6 TIMES .                                 00003710
             03 FILLER       PIC X(2).                                  00003720
             03 MNTOURNA     PIC X.                                     00003730
             03 MNTOURNC     PIC X.                                     00003740
             03 MNTOURNP     PIC X.                                     00003750
             03 MNTOURNH     PIC X.                                     00003760
             03 MNTOURNV     PIC X.                                     00003770
             03 MNTOURNO     PIC X(8).                                  00003780
           02 DFHMS10 OCCURS   6 TIMES .                                00003790
             03 FILLER       PIC X(2).                                  00003800
             03 MPTFLIEUA    PIC X.                                     00003810
             03 MPTFLIEUC    PIC X.                                     00003820
             03 MPTFLIEUP    PIC X.                                     00003830
             03 MPTFLIEUH    PIC X.                                     00003840
             03 MPTFLIEUV    PIC X.                                     00003850
             03 MPTFLIEUO    PIC X(3).                                  00003860
           02 DFHMS11 OCCURS   6 TIMES .                                00003870
             03 FILLER       PIC X(2).                                  00003880
             03 M-MAGAA      PIC X.                                     00003890
             03 M-MAGAC PIC X.                                          00003900
             03 M-MAGAP PIC X.                                          00003910
             03 M-MAGAH PIC X.                                          00003920
             03 M-MAGAV PIC X.                                          00003930
             03 M-MAGAO      PIC X(3).                                  00003940
           02 DFHMS12 OCCURS   6 TIMES .                                00003950
             03 FILLER       PIC X(2).                                  00003960
             03 MCMARQA      PIC X.                                     00003970
             03 MCMARQC PIC X.                                          00003980
             03 MCMARQP PIC X.                                          00003990
             03 MCMARQH PIC X.                                          00004000
             03 MCMARQV PIC X.                                          00004010
             03 MCMARQO      PIC X(5).                                  00004020
           02 DFHMS13 OCCURS   6 TIMES .                                00004030
             03 FILLER       PIC X(2).                                  00004040
             03 MREFA   PIC X.                                          00004050
             03 MREFC   PIC X.                                          00004060
             03 MREFP   PIC X.                                          00004070
             03 MREFH   PIC X.                                          00004080
             03 MREFV   PIC X.                                          00004090
             03 MREFO   PIC X(20).                                      00004100
           02 DFHMS14 OCCURS   6 TIMES .                                00004110
             03 FILLER       PIC X(2).                                  00004120
             03 MCSTATUTA    PIC X.                                     00004130
             03 MCSTATUTC    PIC X.                                     00004140
             03 MCSTATUTP    PIC X.                                     00004150
             03 MCSTATUTH    PIC X.                                     00004160
             03 MCSTATUTV    PIC X.                                     00004170
             03 MCSTATUTO    PIC X.                                     00004180
           02 DFHMS15 OCCURS   6 TIMES .                                00004190
             03 FILLER       PIC X(2).                                  00004200
             03 MNSOCDEA     PIC X.                                     00004210
             03 MNSOCDEC     PIC X.                                     00004220
             03 MNSOCDEP     PIC X.                                     00004230
             03 MNSOCDEH     PIC X.                                     00004240
             03 MNSOCDEV     PIC X.                                     00004250
             03 MNSOCDEO     PIC X(3).                                  00004260
           02 DFHMS16 OCCURS   6 TIMES .                                00004270
             03 FILLER       PIC X(2).                                  00004280
             03 MNMAGDEA     PIC X.                                     00004290
             03 MNMAGDEC     PIC X.                                     00004300
             03 MNMAGDEP     PIC X.                                     00004310
             03 MNMAGDEH     PIC X.                                     00004320
             03 MNMAGDEV     PIC X.                                     00004330
             03 MNMAGDEO     PIC X(3).                                  00004340
           02 DFHMS17 OCCURS   6 TIMES .                                00004350
             03 FILLER       PIC X(2).                                  00004360
             03 MNCLIENTA    PIC X.                                     00004370
             03 MNCLIENTC    PIC X.                                     00004380
             03 MNCLIENTP    PIC X.                                     00004390
             03 MNCLIENTH    PIC X.                                     00004400
             03 MNCLIENTV    PIC X.                                     00004410
             03 MNCLIENTO    PIC X(9).                                  00004420
           02 FILLER    PIC X(2).                                       00004430
           02 MZONCMDA  PIC X.                                          00004440
           02 MZONCMDC  PIC X.                                          00004450
           02 MZONCMDP  PIC X.                                          00004460
           02 MZONCMDH  PIC X.                                          00004470
           02 MZONCMDV  PIC X.                                          00004480
           02 MZONCMDO  PIC X(15).                                      00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MLIBERRA  PIC X.                                          00004510
           02 MLIBERRC  PIC X.                                          00004520
           02 MLIBERRP  PIC X.                                          00004530
           02 MLIBERRH  PIC X.                                          00004540
           02 MLIBERRV  PIC X.                                          00004550
           02 MLIBERRO  PIC X(58).                                      00004560
           02 FILLER    PIC X(2).                                       00004570
           02 MCODTRAA  PIC X.                                          00004580
           02 MCODTRAC  PIC X.                                          00004590
           02 MCODTRAP  PIC X.                                          00004600
           02 MCODTRAH  PIC X.                                          00004610
           02 MCODTRAV  PIC X.                                          00004620
           02 MCODTRAO  PIC X(4).                                       00004630
           02 FILLER    PIC X(2).                                       00004640
           02 MCICSA    PIC X.                                          00004650
           02 MCICSC    PIC X.                                          00004660
           02 MCICSP    PIC X.                                          00004670
           02 MCICSH    PIC X.                                          00004680
           02 MCICSV    PIC X.                                          00004690
           02 MCICSO    PIC X(5).                                       00004700
           02 FILLER    PIC X(2).                                       00004710
           02 MNETNAMA  PIC X.                                          00004720
           02 MNETNAMC  PIC X.                                          00004730
           02 MNETNAMP  PIC X.                                          00004740
           02 MNETNAMH  PIC X.                                          00004750
           02 MNETNAMV  PIC X.                                          00004760
           02 MNETNAMO  PIC X(8).                                       00004770
           02 FILLER    PIC X(2).                                       00004780
           02 MSCREENA  PIC X.                                          00004790
           02 MSCREENC  PIC X.                                          00004800
           02 MSCREENP  PIC X.                                          00004810
           02 MSCREENH  PIC X.                                          00004820
           02 MSCREENV  PIC X.                                          00004830
           02 MSCREENO  PIC X(4).                                       00004840
                                                                                
