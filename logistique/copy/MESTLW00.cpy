      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * - MESTLW00 COPY GENERALISES  50000 OCTETS                      *      13
      *   = 105 DE DEFINITION D ENTETE                                 *        
      *   + 49895 DE MESSAGE REEL                                      *        
      * COPY POUR LA SERIE DE TRANSACTION TLW00 <-> MMQ21              *      13
      ******************************************************************        
              05 MESTLW00-ENTETE.                                               
                 10 MESTLW00-TYPE        PIC X(03).                             
                 10 MESTLW00-NSOCMSG     PIC X(03).                             
                 10 MESTLW00-NLIEUMSG    PIC X(03).                             
                 10 MESTLW00-NSOCDST     PIC X(03).                             
                 10 MESTLW00-NLIEUDST    PIC X(03).                             
                 10 MESTLW00-NORD        PIC 9(08).                             
                 10 MESTLW00-LPROG       PIC X(10).                             
                 10 MESTLW00-DJOUR       PIC X(08).                             
                 10 MESTLW00-WSID        PIC X(10).                             
                 10 MESTLW00-USER        PIC X(10).                             
                 10 MESTLW00-CHRONO      PIC 9(07).                             
                 10 MESTLW00-NBRMSG      PIC 9(07).                             
                 10 MESTLW00-NBRENR      PIC 9(5).                              
                 10 MESTLW00-TAILLE      PIC 9(5).                              
                 10 MESTLW00-VERSION     PIC X(2).                              
                 10 MESTLW00-FILLER      PIC X(18).                             
              05 MESTLW00-DATA           PIC X(49895).                          
                                                                                
