      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : 360 SCHEDULING                                           
      *  COPY        : FLG360                                          *        
      *  CREATION    : 01/04/2010                                      *        
      *  FONCTION    : DSECT DU FICHIER GENERE PAR LE BLG100 QUI       *        
      *                GENERE LE FICHIER DES VENTES POUR 360           *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                *        
      * LONGUEUR DU FICHIER : 500 / 362 POUR LES DONNEES               *        
      *                             138 POUR LE FILLER                 *        
      ******************************************************************        
      ******************************************************************        
      *                                                                         
       01  FLG360-RECORD.                                                       
1          03 FLG360-CPOSTAL       PIC X(05) VALUE SPACES.                      
6          03 FLG360-LNOM          PIC X(25) VALUE SPACES.                      
31         03 FLG360-LBATIMENT     PIC X(03) VALUE SPACES.                      
34         03 FLG360-LESCALIER     PIC X(03) VALUE SPACES.                      
37         03 FLG360-LETAGE        PIC X(03) VALUE SPACES.                      
40         03 FLG360-LPORTE        PIC X(03) VALUE SPACES.                      
43         03 FLG360-LCMPAD1       PIC X(32) VALUE SPACES.                      
75         03 FLG360-LCMPAD2       PIC X(32) VALUE SPACES.                      
107        03 FLG360-CVOIE         PIC X(05) VALUE SPACES.                      
112        03 FLG360-CTVOIE        PIC X(04) VALUE SPACES.                      
116        03 FLG360-LNOMVOIE      PIC X(21) VALUE SPACES.                      
137        03 FLG360-LCOMMUNE      PIC X(32) VALUE SPACES.                      
169        03 FLG360-LBUREAU       PIC X(26) VALUE SPACES.                      
195        03 FLG360-CPAYS         PIC X(03) VALUE SPACES.                      
198        03 FLG360-NSOCIETE      PIC X(03) VALUE SPACES.                      
201        03 FLG360-NLIEU         PIC X(03) VALUE SPACES.                      
204        03 FLG360-NVENTE        PIC X(07) VALUE SPACES.                      
           03 FLG360-PTF.                                                       
211           05 FLG360-SOC-PTF    PIC X(03) VALUE SPACES.                      
214           05 FLG360-LIEU-PTF   PIC X(03) VALUE SPACES.                      
217        03 FLG360-CPROTOUR      PIC X(05) VALUE SPACES.                      
222        03 FLG360-DDELIV        PIC X(08) VALUE SPACES.                      
230        03 FLG360-QTE-VENDUE    PIC 9(05) VALUE 0.                           
235        03 FLG360-QPOIDS        PIC 9(07) VALUE 0.                           
242        03 FLG360-VOLUME        PIC 9(09) VALUE 0.                           
251        03 FLG360-QREPRIS       PIC 9(05) VALUE 0.                           
256        03 FLG360-QPOIDSR       PIC 9(07) VALUE 0.                           
263        03 FLG360-VOL-REPRIS    PIC 9(09) VALUE 0.                           
272        03 FLG360-CEQUI         PIC X(03) VALUE SPACES.                      
275        03 FLG360-QTEMPS        PIC 9(03) VALUE 0.                           
278        03 FLG360-LISTE-FAM     PIC X(50) VALUE SPACES.                      
328        03 FLG360-LCOMVTE1      PIC X(30) VALUE SPACES.                      
358        03 FLG360-CADRTOUR      PIC X(01) VALUE SPACES.                      
359        03 FLG360-CPLAGE        PIC X(02) VALUE SPACES.                      
361        03 FLG360-CTYPE         PIC X(01) VALUE SPACES.                      
362        03 FLG360-WFLAG         PIC X(10) VALUE SPACES.                      
372        03 FILLER               PIC X(128) VALUE SPACES.                     
      *                                                                         
                                                                                
