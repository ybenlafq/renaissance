      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPF35   EPF35                                              00000020
      ***************************************************************** 00000030
       01   EPF35I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEPARL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MSEPARL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSEPARF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSEPARI   PIC X.                                          00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNPAGEI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPARAML  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCPARAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPARAMF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCPARAMI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPARAML  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLPARAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPARAMF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLPARAMI  PIC X(20).                                      00000330
           02 MLIGNEI OCCURS   15 TIMES .                               00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCOL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNSOCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSOCOF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNSOCOI      PIC 999.                                   00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUOL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNLIEUOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIEUOF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNLIEUOI     PIC X(3).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSSLIEOL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MSSLIEOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSSLIEOF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MSSLIEOI     PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLVPARAML    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLVPARAML COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLVPARAMF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLVPARAMI    PIC X(20).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MLIBERRI  PIC X(74).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCODTRAI  PIC X(4).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCICSI    PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MNETNAMI  PIC X(8).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MSCREENI  PIC X(4).                                       00000700
      ***************************************************************** 00000710
      * SDF: EPF35   EPF35                                              00000720
      ***************************************************************** 00000730
       01   EPF35O REDEFINES EPF35I.                                    00000740
           02 FILLER    PIC X(12).                                      00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MDATJOUA  PIC X.                                          00000770
           02 MDATJOUC  PIC X.                                          00000780
           02 MDATJOUP  PIC X.                                          00000790
           02 MDATJOUH  PIC X.                                          00000800
           02 MDATJOUV  PIC X.                                          00000810
           02 MDATJOUO  PIC X(10).                                      00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MTIMJOUA  PIC X.                                          00000840
           02 MTIMJOUC  PIC X.                                          00000850
           02 MTIMJOUP  PIC X.                                          00000860
           02 MTIMJOUH  PIC X.                                          00000870
           02 MTIMJOUV  PIC X.                                          00000880
           02 MTIMJOUO  PIC X(5).                                       00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MPAGEA    PIC X.                                          00000910
           02 MPAGEC    PIC X.                                          00000920
           02 MPAGEP    PIC X.                                          00000930
           02 MPAGEH    PIC X.                                          00000940
           02 MPAGEV    PIC X.                                          00000950
           02 MPAGEO    PIC X(3).                                       00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MSEPARA   PIC X.                                          00000980
           02 MSEPARC   PIC X.                                          00000990
           02 MSEPARP   PIC X.                                          00001000
           02 MSEPARH   PIC X.                                          00001010
           02 MSEPARV   PIC X.                                          00001020
           02 MSEPARO   PIC X.                                          00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MNPAGEA   PIC X.                                          00001050
           02 MNPAGEC   PIC X.                                          00001060
           02 MNPAGEP   PIC X.                                          00001070
           02 MNPAGEH   PIC X.                                          00001080
           02 MNPAGEV   PIC X.                                          00001090
           02 MNPAGEO   PIC X(3).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MCPARAMA  PIC X.                                          00001120
           02 MCPARAMC  PIC X.                                          00001130
           02 MCPARAMP  PIC X.                                          00001140
           02 MCPARAMH  PIC X.                                          00001150
           02 MCPARAMV  PIC X.                                          00001160
           02 MCPARAMO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MLPARAMA  PIC X.                                          00001190
           02 MLPARAMC  PIC X.                                          00001200
           02 MLPARAMP  PIC X.                                          00001210
           02 MLPARAMH  PIC X.                                          00001220
           02 MLPARAMV  PIC X.                                          00001230
           02 MLPARAMO  PIC X(20).                                      00001240
           02 MLIGNEO OCCURS   15 TIMES .                               00001250
             03 FILLER       PIC X(2).                                  00001260
             03 MNSOCOA      PIC X.                                     00001270
             03 MNSOCOC PIC X.                                          00001280
             03 MNSOCOP PIC X.                                          00001290
             03 MNSOCOH PIC X.                                          00001300
             03 MNSOCOV PIC X.                                          00001310
             03 MNSOCOO      PIC ZZZ.                                   00001320
             03 FILLER       PIC X(2).                                  00001330
             03 MNLIEUOA     PIC X.                                     00001340
             03 MNLIEUOC     PIC X.                                     00001350
             03 MNLIEUOP     PIC X.                                     00001360
             03 MNLIEUOH     PIC X.                                     00001370
             03 MNLIEUOV     PIC X.                                     00001380
             03 MNLIEUOO     PIC X(3).                                  00001390
             03 FILLER       PIC X(2).                                  00001400
             03 MSSLIEOA     PIC X.                                     00001410
             03 MSSLIEOC     PIC X.                                     00001420
             03 MSSLIEOP     PIC X.                                     00001430
             03 MSSLIEOH     PIC X.                                     00001440
             03 MSSLIEOV     PIC X.                                     00001450
             03 MSSLIEOO     PIC X(5).                                  00001460
             03 FILLER       PIC X(2).                                  00001470
             03 MLVPARAMA    PIC X.                                     00001480
             03 MLVPARAMC    PIC X.                                     00001490
             03 MLVPARAMP    PIC X.                                     00001500
             03 MLVPARAMH    PIC X.                                     00001510
             03 MLVPARAMV    PIC X.                                     00001520
             03 MLVPARAMO    PIC X(20).                                 00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MLIBERRA  PIC X.                                          00001550
           02 MLIBERRC  PIC X.                                          00001560
           02 MLIBERRP  PIC X.                                          00001570
           02 MLIBERRH  PIC X.                                          00001580
           02 MLIBERRV  PIC X.                                          00001590
           02 MLIBERRO  PIC X(74).                                      00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MCODTRAA  PIC X.                                          00001620
           02 MCODTRAC  PIC X.                                          00001630
           02 MCODTRAP  PIC X.                                          00001640
           02 MCODTRAH  PIC X.                                          00001650
           02 MCODTRAV  PIC X.                                          00001660
           02 MCODTRAO  PIC X(4).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MCICSA    PIC X.                                          00001690
           02 MCICSC    PIC X.                                          00001700
           02 MCICSP    PIC X.                                          00001710
           02 MCICSH    PIC X.                                          00001720
           02 MCICSV    PIC X.                                          00001730
           02 MCICSO    PIC X(5).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNETNAMA  PIC X.                                          00001760
           02 MNETNAMC  PIC X.                                          00001770
           02 MNETNAMP  PIC X.                                          00001780
           02 MNETNAMH  PIC X.                                          00001790
           02 MNETNAMV  PIC X.                                          00001800
           02 MNETNAMO  PIC X(8).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MSCREENA  PIC X.                                          00001830
           02 MSCREENC  PIC X.                                          00001840
           02 MSCREENP  PIC X.                                          00001850
           02 MSCREENH  PIC X.                                          00001860
           02 MSCREENV  PIC X.                                          00001870
           02 MSCREENO  PIC X(4).                                       00001880
                                                                                
