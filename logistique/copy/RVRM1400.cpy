      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM1400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM1400                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1400.                                                            
           02  RM14-CGROUP       PIC X(005).                                    
           02  RM14-CFAM         PIC X(005).                                    
           02  RM14-LAGREGATED   PIC X(020).                                    
           02  RM14-DEFFET       PIC X(008).                                    
           02  RM14-CEXPOA       PIC X(010).                                    
           02  RM14-WQLS         PIC X(001).                                    
           02  RM14-WCEXPO       PIC X(001).                                    
           02  RM14-CACID        PIC X(010).                                    
           02  RM14-DSYST        PIC S9(13) COMP-3.                             
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM1400                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM14-CGROUP-F     PIC S9(4) COMP.                                
      *--                                                                       
           02  RM14-CGROUP-F     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM14-CFAM-F       PIC S9(4) COMP.                                
      *--                                                                       
           02  RM14-CFAM-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM14-LAGREGATED-F PIC S9(4) COMP.                                
      *--                                                                       
           02  RM14-LAGREGATED-F PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM14-DEFFET-F     PIC S9(4) COMP.                                
      *--                                                                       
           02  RM14-DEFFET-F     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM14-CEXPOA-F     PIC S9(4) COMP.                                
      *--                                                                       
           02  RM14-CEXPOA-F     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM14-WQLS-F       PIC S9(4) COMP.                                
      *--                                                                       
           02  RM14-WQLS-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM14-WCEXPO-F     PIC S9(4) COMP.                                
      *--                                                                       
           02  RM14-WCEXPO-F     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM14-CACID-F      PIC S9(4) COMP.                                
      *--                                                                       
           02  RM14-CACID-F      PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM14-DSYST-F      PIC S9(4) COMP.                                
      *--                                                                       
           02  RM14-DSYST-F      PIC S9(4) COMP-5.                              
      *}                                                                        
      *                                                                         
                                                                                
