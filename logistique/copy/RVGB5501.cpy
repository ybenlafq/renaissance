      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVGB5501                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB5501                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGB5501.                                                    00000090
           02  GB55-NSOCENTR                                            00000100
               PIC X(0003).                                             00000110
           02  GB55-NDEPOT                                              00000120
               PIC X(0003).                                             00000130
           02  GB55-NSOCIETE                                            00000140
               PIC X(0003).                                             00000150
           02  GB55-NLIEU                                               00000160
               PIC X(0003).                                             00000170
           02  GB55-NMUTATION                                           00000180
               PIC X(0007).                                             00000190
           02  GB55-DDEBSAIS                                            00000200
               PIC X(0008).                                             00000210
           02  GB55-DFINSAIS                                            00000220
               PIC X(0008).                                             00000230
           02  GB55-DDESTOCK                                            00000240
               PIC X(0008).                                             00000250
           02  GB55-DMUTATION                                           00000260
               PIC X(0008).                                             00000270
           02  GB55-CSELART                                             00000280
               PIC X(0005).                                             00000290
           02  GB55-DHEURMUT                                            00000300
               PIC X(0002).                                             00000310
           02  GB55-DMINUMUT                                            00000320
               PIC X(0002).                                             00000330
           02  GB55-QNBCAMIONS                                          00000340
               PIC S9(2) COMP-3.                                        00000350
           02  GB55-QNBM3QUOTA                                          00000360
               PIC S9(3)V9(0002) COMP-3.                                00000370
           02  GB55-QNBPQUOTA                                           00000380
               PIC S9(5) COMP-3.                                        00000390
           02  GB55-QVOLUME                                             00000400
               PIC S9(11) COMP-3.                                       00000410
           02  GB55-QNBPIECES                                           00000420
               PIC S9(5) COMP-3.                                        00000430
           02  GB55-QNBLIGNES                                           00000440
               PIC S9(5) COMP-3.                                        00000450
           02  GB55-QNBPLANCE                                           00000460
               PIC S9(5) COMP-3.                                        00000470
           02  GB55-QNBLLANCE                                           00000480
               PIC S9(5) COMP-3.                                        00000490
           02  GB55-WPROPERMIS                                          00000500
               PIC X(0001).                                             00000510
           02  GB55-WVAL                                                00000520
               PIC X(0001).                                             00000530
           02  GB55-LHEURLIMIT                                          00000540
               PIC X(0010).                                             00000550
           02  GB55-DSYST                                               00000560
               PIC S9(13) COMP-3.                                       00000570
           02  GB55-DVALID                                              00000580
               PIC X(0008).                                             00000590
           02  GB55-QNBM3PROPOS                                         00000600
               PIC S9(3)V9(0002) COMP-3.                                00000610
           02  GB55-QNBPPROPOS                                          00000620
               PIC S9(5) COMP-3.                                        00000630
      *                                                                 00000640
      *---------------------------------------------------------        00000650
      *   LISTE DES FLAGS DE LA TABLE RVGB5501                          00000660
      *---------------------------------------------------------        00000670
      *                                                                 00000680
       01  RVGB5501-FLAGS.                                              00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NSOCENTR-F                                          00000700
      *        PIC S9(4) COMP.                                          00000710
      *--                                                                       
           02  GB55-NSOCENTR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NDEPOT-F                                            00000720
      *        PIC S9(4) COMP.                                          00000730
      *--                                                                       
           02  GB55-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NSOCIETE-F                                          00000740
      *        PIC S9(4) COMP.                                          00000750
      *--                                                                       
           02  GB55-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NLIEU-F                                             00000760
      *        PIC S9(4) COMP.                                          00000770
      *--                                                                       
           02  GB55-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NMUTATION-F                                         00000780
      *        PIC S9(4) COMP.                                          00000790
      *--                                                                       
           02  GB55-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DDEBSAIS-F                                          00000800
      *        PIC S9(4) COMP.                                          00000810
      *--                                                                       
           02  GB55-DDEBSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DFINSAIS-F                                          00000820
      *        PIC S9(4) COMP.                                          00000830
      *--                                                                       
           02  GB55-DFINSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DDESTOCK-F                                          00000840
      *        PIC S9(4) COMP.                                          00000850
      *--                                                                       
           02  GB55-DDESTOCK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DMUTATION-F                                         00000860
      *        PIC S9(4) COMP.                                          00000870
      *--                                                                       
           02  GB55-DMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-CSELART-F                                           00000880
      *        PIC S9(4) COMP.                                          00000890
      *--                                                                       
           02  GB55-CSELART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DHEURMUT-F                                          00000900
      *        PIC S9(4) COMP.                                          00000910
      *--                                                                       
           02  GB55-DHEURMUT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DMINUMUT-F                                          00000920
      *        PIC S9(4) COMP.                                          00000930
      *--                                                                       
           02  GB55-DMINUMUT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBCAMIONS-F                                        00000940
      *        PIC S9(4) COMP.                                          00000950
      *--                                                                       
           02  GB55-QNBCAMIONS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBM3QUOTA-F                                        00000960
      *        PIC S9(4) COMP.                                          00000970
      *--                                                                       
           02  GB55-QNBM3QUOTA-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBPQUOTA-F                                         00000980
      *        PIC S9(4) COMP.                                          00000990
      *--                                                                       
           02  GB55-QNBPQUOTA-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QVOLUME-F                                           00001000
      *        PIC S9(4) COMP.                                          00001010
      *--                                                                       
           02  GB55-QVOLUME-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBPIECES-F                                         00001020
      *        PIC S9(4) COMP.                                          00001030
      *--                                                                       
           02  GB55-QNBPIECES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBLIGNES-F                                         00001040
      *        PIC S9(4) COMP.                                          00001050
      *--                                                                       
           02  GB55-QNBLIGNES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBPLANCE-F                                         00001060
      *        PIC S9(4) COMP.                                          00001070
      *--                                                                       
           02  GB55-QNBPLANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBLLANCE-F                                         00001080
      *        PIC S9(4) COMP.                                          00001090
      *--                                                                       
           02  GB55-QNBLLANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-WPROPERMIS-F                                        00001100
      *        PIC S9(4) COMP.                                          00001110
      *--                                                                       
           02  GB55-WPROPERMIS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-WVAL-F                                              00001120
      *        PIC S9(4) COMP.                                          00001130
      *--                                                                       
           02  GB55-WVAL-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-LHEURLIMIT-F                                        00001140
      *        PIC S9(4) COMP.                                          00001150
      *--                                                                       
           02  GB55-LHEURLIMIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DSYST-F                                             00001160
      *        PIC S9(4) COMP.                                          00001170
      *--                                                                       
           02  GB55-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DVALID-F                                            00001180
      *        PIC S9(4) COMP.                                          00001190
      *--                                                                       
           02  GB55-DVALID-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBM3PROPOS-F                                       00001200
      *        PIC S9(4) COMP.                                          00001210
      *--                                                                       
           02  GB55-QNBM3PROPOS-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBPPROPOS-F                                        00001220
      *        PIC S9(4) COMP.                                          00001230
      *--                                                                       
           02  GB55-QNBPPROPOS-F                                                
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                            00001240
                                                                                
