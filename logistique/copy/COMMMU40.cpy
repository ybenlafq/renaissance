      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PRG TMU40 (MENU)             TR: MU40  *            
      *                          TMU42                             *            
      *                                                            *            
      *           POUR L'ADMINISTATION DES DONNEES                 *            
      **************************************************************            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +8192            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-------------------------------------------------------------            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-MU40-LONG-COMMAREA PIC S9(4) COMP VALUE +8192.                   
      *--                                                                       
       01  COM-MU40-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +8192.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA      PIC X(100).                                   
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02 COMM-CICS-APPLID     PIC X(8).                                     
          02 COMM-CICS-NETNAM     PIC X(8).                                     
          02 COMM-CICS-TRANSA     PIC X(4).                                     
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE     PIC XX.                                       
          02 COMM-DATE-ANNEE      PIC XX.                                       
          02 COMM-DATE-MOIS       PIC XX.                                       
          02 COMM-DATE-JOUR       PIC XX.                                       
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC 999.                                      
          02 COMM-DATE-QNT0       PIC 99999.                                    
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC 9.                                        
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC 9.                                        
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC XXX.                                      
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                 
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC XXX.                                      
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                 
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                                     
          02 COMM-DATE-AAMMJJ     PIC X(6).                                     
          02 COMM-DATE-JJMMSSAA   PIC X(8).                                     
          02 COMM-DATE-JJMMAA     PIC X(6).                                     
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                                     
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                    
      * DIFFERENTES FORMES DE DATE                                              
          02 COMM-DATE-SEMSS      PIC 9(02).                                    
          02 COMM-DATE-SEMAA      PIC 9(02).                                    
          02 COMM-DATE-SEMNU      PIC 9(02).                                    
          02 COMM-DATE-FILLER     PIC X(08).                                    
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                                
      *                                                                         
      * ZONES DE COMMAREA CHARGEES PAR LE PROG TMU40 ------------ 1150          
      *                                                                         
          02 COMM-MU40-APPLI.                                                   
      *                                                                         
              03 COMM-MU40-NMUTATION         PIC X(07).                         
              03 COMM-MU40-DMUTATION.                                           
                 05 COMM-MU40-DMUTAA         PIC X(04).                         
                 05 COMM-MU40-DMUTMM         PIC X(02).                         
                 05 COMM-MU40-DMUTJJ         PIC X(02).                         
              03 COMM-MU40-DMUT-DEF          PIC X(08).                         
              03 COMM-MU40-DVALID            PIC X(08).                         
              03 COMM-MU40-NSOCENTR          PIC X(03).                         
              03 COMM-MU40-NDEPOT            PIC X(03).                         
              03 COMM-MU40-LLIEUDEPOT        PIC X(20).                         
              03 COMM-MU40-FLAG-SAISIE       PIC X(01).                         
              03 COMM-MU40-TABLE-DEPOTS-CSELART.                                
                 05 COMM-MU40-DEPOTS OCCURS 12.                                 
                    07 COMM-MU40-WSOCENTR    PIC X(03).                         
                    07 COMM-MU40-WDEPOT      PIC X(03).                         
                    07 COMM-MU40-WLDEPOT     PIC X(03).                         
                    07 COMM-MU40-WSELART     PIC X(05).                         
              03 COMM-MU40-CSELART           PIC X(05).                         
              03 COMM-MU40-NSOCIETE          PIC X(03).                         
              03 COMM-MU40-NLIEU             PIC X(03).                         
              03 COMM-MU40-LLIEUDEM          PIC X(20).                         
              03 COMM-MU40-COPCO             PIC X(03).                         
              03 COMM-MU40-WVAL              PIC X(01).                         
              03 COMM-MU40-CACID             PIC X(08).                         
              03 COMM-MU40-FLAG-BYPASS       PIC X.                             
                 88 COMM-MU40-BYPASS-GP      VALUE 'O'.                         
                 88 COMM-MU40-PAS-BYPASS-GP  VALUE 'N'.                         
              03 FILLER                      PIC X(448).                        
          02  COMM-MU40-OPTION               PIC X(3000).                       
      *                                                                         
          02  COMM-MU40-OPTION1      REDEFINES COMM-MU40-OPTION.                
              03  COMM-MU41-NPAGE       PIC 9(03).                              
              03  COMM-MU41-TABLE-PAGE.                                         
                  05  COMM-MU41-CLE OCCURS 100.                                 
      *                                                  100 * 15 =1500         
                      10  COMM-MU41-NLASTMUT    PIC X(07).                      
                      10  COMM-MU41-DLASTMUT    PIC X(08).                      
              03  COMM-MU41-LIGNE  OCCURS  11.                                  
      *                                                   11 * 59 = 649         
                  05  COMM-MU41-NMUTATION       PIC X(07).                      
                  05  COMM-MU41-DDEBSAIS        PIC X(08).                      
                  05  COMM-MU41-DFINSAIS        PIC X(08).                      
                  05  COMM-MU41-DMUTATION       PIC X(08).                      
                  05  COMM-MU41-NSOCENTR        PIC X(03).                      
                  05  COMM-MU41-NDEPOT          PIC X(03).                      
                  05  COMM-MU41-CSELART         PIC X(05).                      
                  05  COMM-MU41-DHEURMUT        PIC X(02).                      
                  05  COMM-MU41-DMINUMUT        PIC X(02).                      
                  05  COMM-MU41-QNBCAMIONS      PIC 9(02).                      
                  05  COMM-MU41-QNBM3QUOTA      PIC 9(03)V9.                    
                  05  COMM-MU41-QNBPQUOTA       PIC 9(05).                      
                  05  COMM-MU41-PROPERMIS       PIC X(01).                      
                  05  COMM-MU41-WMULTI          PIC X(01).                      
              03  COMM-MU41-FILLER              PIC X(848).                     
          02  COMM-MU40-OPTION2      REDEFINES COMM-MU40-OPTION.                
              03 COMM-MU42-TS-VIDE       PIC X.                                 
              03 COMM-MU42-MAJ-EN-COURS  PIC X.                                 
              03 COMM-MU42-CONFIRM-PF3   PIC X.                                 
              03 COMM-MU42-RECH.                                                
                 05 COMM-MU42-RECH-FLAG  PIC X.                                 
                 05 COMM-MU42-RECH-COD   PIC X(07).                             
              03 COMM-MU42-PLAFD         PIC 9(2).                              
              03 COMM-MU42-ITEM          PIC 9(02).                             
              03 COMM-MU42-PAGE          PIC 9(02).                             
              03 COMM-MU42-PAGEMAX       PIC 9(02).                             
              03 COMM-MU42-NLIGNE        PIC 9(02).                             
              03 COMM-MU42-QVOLUME       PIC S9(11) COMP-3.                     
              03 COMM-MU42-QNBPIECES     PIC S9(05) COMP-3.                     
              03 COMM-MU42-QNBM3QUOTA    PIC  9(5)V9.                           
              03 COMM-MU42-QNBPQUOTA     PIC  9(5).                             
              03 COMM-MU42-DDEBSAIS      PIC X(08).                             
              03 COMM-MU42-DFINSAIS      PIC X(08).                             
              03 COMM-MU42-DDESTOCK.                                            
                 05 COMM-MU42-DSTKAA         PIC X(04).                         
                 05 COMM-MU42-DSTKMM         PIC X(02).                         
                 05 COMM-MU42-DSTKJJ         PIC X(02).                         
              03 COMM-MU42-PCF           PIC S9(7)V9(6) USAGE COMP-3.           
      *****************************************************************         
                                                                                
