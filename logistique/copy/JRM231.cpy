      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JRM231 AU 19/10/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,20,BI,A,                          *        
      *                           27,05,BI,A,                          *        
      *                           32,03,PD,A,                          *        
      *                           35,20,BI,A,                          *        
      *                           55,02,PD,A,                          *        
      *                           57,01,BI,A,                          *        
      *                           58,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JRM231.                                                        
            05 NOMETAT-JRM231           PIC X(6) VALUE 'JRM231'.                
            05 RUPTURES-JRM231.                                                 
           10 JRM231-LCHEFPROD          PIC X(20).                      007  020
           10 JRM231-CGROUP             PIC X(05).                      027  005
           10 JRM231-WSEQFAM            PIC S9(05)      COMP-3.         032  003
           10 JRM231-LAGREGATED         PIC X(20).                      035  020
           10 JRM231-QRANG              PIC S9(03)      COMP-3.         055  002
           10 JRM231-REMPLACE           PIC X(01).                      057  001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JRM231-SEQUENCE           PIC S9(04) COMP.                058  002
      *--                                                                       
           10 JRM231-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JRM231.                                                   
           10 JRM231-CFAM               PIC X(05).                      060  005
           10 JRM231-CHEFPROD           PIC X(05).                      065  005
           10 JRM231-CMARQ              PIC X(05).                      070  005
           10 JRM231-DPV1               PIC X(04).                      075  004
           10 JRM231-DPV2               PIC X(04).                      079  004
           10 JRM231-DPV3               PIC X(04).                      083  004
           10 JRM231-DV1SD              PIC X(04).                      087  004
           10 JRM231-DV1SF              PIC X(04).                      091  004
           10 JRM231-DV2SD              PIC X(04).                      095  004
           10 JRM231-DV2SF              PIC X(04).                      099  004
           10 JRM231-DV3SD              PIC X(04).                      103  004
           10 JRM231-DV3SF              PIC X(04).                      107  004
           10 JRM231-DV4SD              PIC X(04).                      111  004
           10 JRM231-DV4SF              PIC X(04).                      115  004
           10 JRM231-INDLOI             PIC X(01).                      119  001
           10 JRM231-INDLOIS            PIC X(01).                      120  001
           10 JRM231-LFAM               PIC X(20).                      121  020
           10 JRM231-LIBELLE1           PIC X(07).                      141  007
           10 JRM231-LIBELLE2           PIC X(05).                      148  005
           10 JRM231-LREFFOURN          PIC X(20).                      153  020
           10 JRM231-LSTATCOMP          PIC X(03).                      173  003
           10 JRM231-NCODIC1            PIC X(08).                      176  008
           10 JRM231-NSOCIETE           PIC X(03).                      184  003
           10 JRM231-PLURIEL            PIC X(01).                      187  001
           10 JRM231-QRANGAFF           PIC X(03).                      188  003
           10 JRM231-QSTOCKDEP          PIC X(05).                      191  005
           10 JRM231-WEDIT              PIC X(01).                      196  001
           10 JRM231-WNAT               PIC X(01).                      197  001
           10 JRM231-WPV1               PIC X(01).                      198  001
           10 JRM231-WPV2               PIC X(01).                      199  001
           10 JRM231-WPV3               PIC X(01).                      200  001
           10 JRM231-WPV4               PIC X(01).                      201  001
           10 JRM231-WSAVANCE           PIC X(01).                      202  001
           10 JRM231-WSENSAPPRO         PIC X(01).                      203  001
           10 JRM231-W8020              PIC X(01).                      204  001
           10 JRM231-ZDCDE              PIC X(08).                      205  008
           10 JRM231-ZDEPOT1            PIC X(03).                      213  003
           10 JRM231-ZDEPOT2            PIC X(03).                      216  003
           10 JRM231-ZDEPOT3            PIC X(03).                      219  003
           10 JRM231-ZDEPOT4            PIC X(03).                      222  003
           10 JRM231-ZDEPOT5            PIC X(03).                      225  003
           10 JRM231-ZNCODIC            PIC X(07).                      228  007
           10 JRM231-ZSOCDEPOT1         PIC X(03).                      235  003
           10 JRM231-ZSOCDEPOT2         PIC X(03).                      238  003
           10 JRM231-ZSOCDEPOT3         PIC X(03).                      241  003
           10 JRM231-ZSOCDEPOT4         PIC X(03).                      244  003
           10 JRM231-ZSOCDEPOT5         PIC X(03).                      247  003
           10 JRM231-DISPO              PIC S9(03)      COMP-3.         250  002
           10 JRM231-QCDE               PIC S9(05)      COMP-3.         252  003
           10 JRM231-QCOLD              PIC S9(03)      COMP-3.         255  002
           10 JRM231-QNBMAG             PIC S9(03)      COMP-3.         257  002
           10 JRM231-QPV1               PIC S9(07)      COMP-3.         259  004
           10 JRM231-QPV2               PIC S9(07)      COMP-3.         263  004
           10 JRM231-QPV3               PIC S9(07)      COMP-3.         267  004
           10 JRM231-QSTOCKMAG          PIC S9(06)      COMP-3.         271  004
           10 JRM231-QTE1               PIC S9(05)      COMP-3.         275  003
           10 JRM231-QTE2               PIC S9(05)      COMP-3.         278  003
           10 JRM231-QTE3               PIC S9(05)      COMP-3.         281  003
           10 JRM231-QTE4               PIC S9(05)      COMP-3.         284  003
           10 JRM231-QTE5               PIC S9(05)      COMP-3.         287  003
           10 JRM231-QTOTMAG            PIC S9(02)      COMP-3.         290  002
           10 JRM231-QVS-1              PIC S9(07)      COMP-3.         292  004
           10 JRM231-QVS-2              PIC S9(07)      COMP-3.         296  004
           10 JRM231-QVS-3              PIC S9(07)      COMP-3.         300  004
           10 JRM231-QVS-4              PIC S9(07)      COMP-3.         304  004
           10 JRM231-QV1S               PIC S9(07)      COMP-3.         308  004
           10 JRM231-QV2S               PIC S9(07)      COMP-3.         312  004
           10 JRM231-QV3S               PIC S9(07)      COMP-3.         316  004
           10 JRM231-QV4S               PIC S9(07)      COMP-3.         320  004
           10 JRM231-STOCKAV            PIC S9(02)V9(2) COMP-3.         324  003
           10 JRM231-ZTRICODIC          PIC S9(07)      COMP-3.         327  004
           10 JRM231-ZZCDE              PIC S9(08)      COMP-3.         331  005
           10 JRM231-ZZNDEPOT           PIC S9(03)      COMP-3.         336  002
           10 JRM231-ZZNSOCDEPOT        PIC S9(03)      COMP-3.         338  002
           10 JRM231-ZZQCDE             PIC S9(05)      COMP-3.         340  003
            05 FILLER                      PIC X(170).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JRM231-LONG           PIC S9(4)   COMP  VALUE +342.           
      *                                                                         
      *--                                                                       
        01  DSECT-JRM231-LONG           PIC S9(4) COMP-5  VALUE +342.           
                                                                                
      *}                                                                        
