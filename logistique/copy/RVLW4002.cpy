      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RTLW40                             *        
      ******************************************************************        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLW4002                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW4002.                                                            
           10 LW40-NSOCDEPOT       PIC X(3).                                    
           10 LW40-NDEPOT          PIC X(3).                                    
           10 LW40-NMVT            PIC X(9).                                    
           10 LW40-CMVT            PIC X(2).                                    
           10 LW40-BLOCAGEI        PIC X(3).                                    
           10 LW40-BLOCAGEF        PIC X(3).                                    
           10 LW40-DMVT            PIC X(8).                                    
           10 LW40-HMVT            PIC X(6).                                    
           10 LW40-NCODIC          PIC X(7).                                    
           10 LW40-DTRANSFERT      PIC X(8).                                    
           10 LW40-QMVT            PIC S9(9)V USAGE COMP-3.                     
           10 LW40-NCDE            PIC X(15).                                   
           10 LW40-DLIVRAISON      PIC X(8).                                    
           10 LW40-NDOSLM7         PIC X(15).                                   
           10 LW40-NINV            PIC X(15).                                   
           10 LW40-CSTATUT         PIC X(2).                                    
           10 LW40-LCOMMENT        PIC X(20).                                   
           10 LW40-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 LW40-NSUPPORT        PIC X(18).                                   
           10 LW40-NBL             PIC X(10).                                   
           10 LW40-QPILE           PIC S9(9)V USAGE COMP-3.                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLW4002                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW4002-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-NSOCDEPOT-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-NSOCDEPOT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-NDEPOT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-NDEPOT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-NMVT-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-NMVT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-CMVT-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-CMVT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-BLOCAGEI-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-BLOCAGEI-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-BLOCAGEF-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-BLOCAGEF-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-DMVT-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-DMVT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-HMVT-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-HMVT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-DTRANSFERT-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-DTRANSFERT-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-QMVT-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-QMVT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-NCDE-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-NCDE-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-DLIVRAISON-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-DLIVRAISON-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-NDOSLM7-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-NDOSLM7-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-NINV-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-NINV-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-CSTATUT-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-CSTATUT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-LCOMMENT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-LCOMMENT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-NSUPPORT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-NSUPPORT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-NBL-F           PIC S9(4) COMP.                              
      *--                                                                       
           10 LW40-NBL-F           PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW40-QPILE-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 LW40-QPILE-F         PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
