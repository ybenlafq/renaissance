      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *-------------------------------------------------------------    00000040
      *-------------------------------------------------------------    00000040
      *       PROJET WMS - LM7 - ASIS                                   00000040
      *       COPIE POUR LE MODULE MLW15                                00000040
      *       MODULE DE TRANSFERT MQ DES ARTCILES                       00000070
      *  LE 19/07/2012                                                  00000100
      *  ARNAUD ANGER - DSA033                                          00000110
      *-------------------------------------------------------------    00000130
      *-------------------------------------------------------------    00000150
       01 COMM-LW15-APPLI.                                              00260000
          02 COMM-LW15-ENTREE.                                          00260000
             03 COMM-LW15-NPROG       PIC X(08).                        00320000
             03 COMM-LW15-APPLID      PIC X(08).                        00330000
             03 COMM-LW15-TERMID      PIC X(04).                        00340000
             03 COMM-LW15-USERID      PIC X(08).                        00340000
             03 COMM-LW15-DATEJOUR    PIC X(08).                                
             03 COMM-LW15-NSOCDEPOT   PIC X(03).                                
             03 COMM-LW15-NDEPOT      PIC X(03).                                
             03 COMM-LW15-NCODIC      PIC X(07).                                
             03 COMM-LW15-CODLANG     PIC X(02).                                
         02 COMM-MLW15-SORTIE.                                          00260000
             03 COMM-LW15-CRETOUR     PIC X(02).                        00340000
             03 COMM-LW15-LRETOUR     PIC X(60).                                
         02  FILLER                    PIC X(500).                      00340000
                                                                                
