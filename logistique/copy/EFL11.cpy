      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFL11   EFL11                                              00000020
      ***************************************************************** 00000030
       01   EFL11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEYL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEYL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEYF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEYI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MFONCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROAFFL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCPROAFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPROAFFF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCPROAFFI      PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROAFFL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLPROAFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPROAFFF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLPROAFFI      PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXCEPL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCEXCEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCEXCEPF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCEXCEPI  PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMILL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCFAMILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCFAMILF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCFAMILI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT1L      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNDEPOT1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT1F      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNDEPOT1I      PIC X(3).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT2L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNDEPOT2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT2F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNDEPOT2I      PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT3L      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNDEPOT3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT3F      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNDEPOT3I      PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT4L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MNDEPOT4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT4F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNDEPOT4I      PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT5L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNDEPOT5L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT5F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNDEPOT5I      PIC X(3).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOC1DFL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSOC1DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOC1DFF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSOC1DFI  PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENT1DFL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MENT1DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENT1DFF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MENT1DFI  PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOC2DFL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSOC2DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOC2DFF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSOC2DFI  PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENT2DFL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MENT2DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENT2DFF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MENT2DFI  PIC X(3).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOC3DFL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MSOC3DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOC3DFF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSOC3DFI  PIC X(3).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENT3DFL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MENT3DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENT3DFF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MENT3DFI  PIC X(3).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOC4DFL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSOC4DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOC4DFF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSOC4DFI  PIC X(3).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENT4DFL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MENT4DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENT4DFF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MENT4DFI  PIC X(3).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOC5DFL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSOC5DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOC5DFF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSOC5DFI  PIC X(3).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENT5DFL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MENT5DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENT5DFF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MENT5DFI  PIC X(3).                                       00001010
           02 MTABLEI OCCURS   15 TIMES .                               00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXCL  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MCEXCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCEXCF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MCEXCI  PIC X.                                          00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MCFAMI  PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MLFAMI  PIC X(20).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC1L  COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MSOC1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC1F  PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MSOC1I  PIC X(3).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT1L  COMP PIC S9(4).                                 00001190
      *--                                                                       
             03 MENT1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT1F  PIC X.                                          00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MENT1I  PIC X(3).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC2L  COMP PIC S9(4).                                 00001230
      *--                                                                       
             03 MSOC2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC2F  PIC X.                                          00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MSOC2I  PIC X(3).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT2L  COMP PIC S9(4).                                 00001270
      *--                                                                       
             03 MENT2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT2F  PIC X.                                          00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MENT2I  PIC X(3).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC3L  COMP PIC S9(4).                                 00001310
      *--                                                                       
             03 MSOC3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC3F  PIC X.                                          00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MSOC3I  PIC X(3).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT3L  COMP PIC S9(4).                                 00001350
      *--                                                                       
             03 MENT3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT3F  PIC X.                                          00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MENT3I  PIC X(3).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC4L  COMP PIC S9(4).                                 00001390
      *--                                                                       
             03 MSOC4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC4F  PIC X.                                          00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MSOC4I  PIC X(3).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT4L  COMP PIC S9(4).                                 00001430
      *--                                                                       
             03 MENT4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT4F  PIC X.                                          00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MENT4I  PIC X(3).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC5L  COMP PIC S9(4).                                 00001470
      *--                                                                       
             03 MSOC5L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC5F  PIC X.                                          00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MSOC5I  PIC X(3).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT5L  COMP PIC S9(4).                                 00001510
      *--                                                                       
             03 MENT5L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT5F  PIC X.                                          00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MENT5I  PIC X(3).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MZONCMDI  PIC X(15).                                      00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MLIBERRI  PIC X(58).                                      00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MCODTRAI  PIC X(4).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCICSI    PIC X(5).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MNETNAMI  PIC X(8).                                       00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MSCREENI  PIC X(4).                                       00001780
      ***************************************************************** 00001790
      * SDF: EFL11   EFL11                                              00001800
      ***************************************************************** 00001810
       01   EFL11O REDEFINES EFL11I.                                    00001820
           02 FILLER    PIC X(12).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDATJOUA  PIC X.                                          00001850
           02 MDATJOUC  PIC X.                                          00001860
           02 MDATJOUP  PIC X.                                          00001870
           02 MDATJOUH  PIC X.                                          00001880
           02 MDATJOUV  PIC X.                                          00001890
           02 MDATJOUO  PIC X(10).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MTIMJOUA  PIC X.                                          00001920
           02 MTIMJOUC  PIC X.                                          00001930
           02 MTIMJOUP  PIC X.                                          00001940
           02 MTIMJOUH  PIC X.                                          00001950
           02 MTIMJOUV  PIC X.                                          00001960
           02 MTIMJOUO  PIC X(5).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MPAGEA    PIC X.                                          00001990
           02 MPAGEC    PIC X.                                          00002000
           02 MPAGEP    PIC X.                                          00002010
           02 MPAGEH    PIC X.                                          00002020
           02 MPAGEV    PIC X.                                          00002030
           02 MPAGEO    PIC X(3).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MPAGEYA   PIC X.                                          00002060
           02 MPAGEYC   PIC X.                                          00002070
           02 MPAGEYP   PIC X.                                          00002080
           02 MPAGEYH   PIC X.                                          00002090
           02 MPAGEYV   PIC X.                                          00002100
           02 MPAGEYO   PIC X(3).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MFONCA    PIC X.                                          00002130
           02 MFONCC    PIC X.                                          00002140
           02 MFONCP    PIC X.                                          00002150
           02 MFONCH    PIC X.                                          00002160
           02 MFONCV    PIC X.                                          00002170
           02 MFONCO    PIC X(3).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCPROAFFA      PIC X.                                     00002200
           02 MCPROAFFC PIC X.                                          00002210
           02 MCPROAFFP PIC X.                                          00002220
           02 MCPROAFFH PIC X.                                          00002230
           02 MCPROAFFV PIC X.                                          00002240
           02 MCPROAFFO      PIC X(5).                                  00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLPROAFFA      PIC X.                                     00002270
           02 MLPROAFFC PIC X.                                          00002280
           02 MLPROAFFP PIC X.                                          00002290
           02 MLPROAFFH PIC X.                                          00002300
           02 MLPROAFFV PIC X.                                          00002310
           02 MLPROAFFO      PIC X(20).                                 00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCEXCEPA  PIC X.                                          00002340
           02 MCEXCEPC  PIC X.                                          00002350
           02 MCEXCEPP  PIC X.                                          00002360
           02 MCEXCEPH  PIC X.                                          00002370
           02 MCEXCEPV  PIC X.                                          00002380
           02 MCEXCEPO  PIC X.                                          00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MCFAMILA  PIC X.                                          00002410
           02 MCFAMILC  PIC X.                                          00002420
           02 MCFAMILP  PIC X.                                          00002430
           02 MCFAMILH  PIC X.                                          00002440
           02 MCFAMILV  PIC X.                                          00002450
           02 MCFAMILO  PIC X(5).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MNDEPOT1A      PIC X.                                     00002480
           02 MNDEPOT1C PIC X.                                          00002490
           02 MNDEPOT1P PIC X.                                          00002500
           02 MNDEPOT1H PIC X.                                          00002510
           02 MNDEPOT1V PIC X.                                          00002520
           02 MNDEPOT1O      PIC X(3).                                  00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MNDEPOT2A      PIC X.                                     00002550
           02 MNDEPOT2C PIC X.                                          00002560
           02 MNDEPOT2P PIC X.                                          00002570
           02 MNDEPOT2H PIC X.                                          00002580
           02 MNDEPOT2V PIC X.                                          00002590
           02 MNDEPOT2O      PIC X(3).                                  00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MNDEPOT3A      PIC X.                                     00002620
           02 MNDEPOT3C PIC X.                                          00002630
           02 MNDEPOT3P PIC X.                                          00002640
           02 MNDEPOT3H PIC X.                                          00002650
           02 MNDEPOT3V PIC X.                                          00002660
           02 MNDEPOT3O      PIC X(3).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MNDEPOT4A      PIC X.                                     00002690
           02 MNDEPOT4C PIC X.                                          00002700
           02 MNDEPOT4P PIC X.                                          00002710
           02 MNDEPOT4H PIC X.                                          00002720
           02 MNDEPOT4V PIC X.                                          00002730
           02 MNDEPOT4O      PIC X(3).                                  00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNDEPOT5A      PIC X.                                     00002760
           02 MNDEPOT5C PIC X.                                          00002770
           02 MNDEPOT5P PIC X.                                          00002780
           02 MNDEPOT5H PIC X.                                          00002790
           02 MNDEPOT5V PIC X.                                          00002800
           02 MNDEPOT5O      PIC X(3).                                  00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MSOC1DFA  PIC X.                                          00002830
           02 MSOC1DFC  PIC X.                                          00002840
           02 MSOC1DFP  PIC X.                                          00002850
           02 MSOC1DFH  PIC X.                                          00002860
           02 MSOC1DFV  PIC X.                                          00002870
           02 MSOC1DFO  PIC X(3).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MENT1DFA  PIC X.                                          00002900
           02 MENT1DFC  PIC X.                                          00002910
           02 MENT1DFP  PIC X.                                          00002920
           02 MENT1DFH  PIC X.                                          00002930
           02 MENT1DFV  PIC X.                                          00002940
           02 MENT1DFO  PIC X(3).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MSOC2DFA  PIC X.                                          00002970
           02 MSOC2DFC  PIC X.                                          00002980
           02 MSOC2DFP  PIC X.                                          00002990
           02 MSOC2DFH  PIC X.                                          00003000
           02 MSOC2DFV  PIC X.                                          00003010
           02 MSOC2DFO  PIC X(3).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MENT2DFA  PIC X.                                          00003040
           02 MENT2DFC  PIC X.                                          00003050
           02 MENT2DFP  PIC X.                                          00003060
           02 MENT2DFH  PIC X.                                          00003070
           02 MENT2DFV  PIC X.                                          00003080
           02 MENT2DFO  PIC X(3).                                       00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MSOC3DFA  PIC X.                                          00003110
           02 MSOC3DFC  PIC X.                                          00003120
           02 MSOC3DFP  PIC X.                                          00003130
           02 MSOC3DFH  PIC X.                                          00003140
           02 MSOC3DFV  PIC X.                                          00003150
           02 MSOC3DFO  PIC X(3).                                       00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MENT3DFA  PIC X.                                          00003180
           02 MENT3DFC  PIC X.                                          00003190
           02 MENT3DFP  PIC X.                                          00003200
           02 MENT3DFH  PIC X.                                          00003210
           02 MENT3DFV  PIC X.                                          00003220
           02 MENT3DFO  PIC X(3).                                       00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MSOC4DFA  PIC X.                                          00003250
           02 MSOC4DFC  PIC X.                                          00003260
           02 MSOC4DFP  PIC X.                                          00003270
           02 MSOC4DFH  PIC X.                                          00003280
           02 MSOC4DFV  PIC X.                                          00003290
           02 MSOC4DFO  PIC X(3).                                       00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MENT4DFA  PIC X.                                          00003320
           02 MENT4DFC  PIC X.                                          00003330
           02 MENT4DFP  PIC X.                                          00003340
           02 MENT4DFH  PIC X.                                          00003350
           02 MENT4DFV  PIC X.                                          00003360
           02 MENT4DFO  PIC X(3).                                       00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MSOC5DFA  PIC X.                                          00003390
           02 MSOC5DFC  PIC X.                                          00003400
           02 MSOC5DFP  PIC X.                                          00003410
           02 MSOC5DFH  PIC X.                                          00003420
           02 MSOC5DFV  PIC X.                                          00003430
           02 MSOC5DFO  PIC X(3).                                       00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MENT5DFA  PIC X.                                          00003460
           02 MENT5DFC  PIC X.                                          00003470
           02 MENT5DFP  PIC X.                                          00003480
           02 MENT5DFH  PIC X.                                          00003490
           02 MENT5DFV  PIC X.                                          00003500
           02 MENT5DFO  PIC X(3).                                       00003510
           02 MTABLEO OCCURS   15 TIMES .                               00003520
             03 FILLER       PIC X(2).                                  00003530
             03 MCEXCA  PIC X.                                          00003540
             03 MCEXCC  PIC X.                                          00003550
             03 MCEXCP  PIC X.                                          00003560
             03 MCEXCH  PIC X.                                          00003570
             03 MCEXCV  PIC X.                                          00003580
             03 MCEXCO  PIC X.                                          00003590
             03 FILLER       PIC X(2).                                  00003600
             03 MCFAMA  PIC X.                                          00003610
             03 MCFAMC  PIC X.                                          00003620
             03 MCFAMP  PIC X.                                          00003630
             03 MCFAMH  PIC X.                                          00003640
             03 MCFAMV  PIC X.                                          00003650
             03 MCFAMO  PIC X(5).                                       00003660
             03 FILLER       PIC X(2).                                  00003670
             03 MLFAMA  PIC X.                                          00003680
             03 MLFAMC  PIC X.                                          00003690
             03 MLFAMP  PIC X.                                          00003700
             03 MLFAMH  PIC X.                                          00003710
             03 MLFAMV  PIC X.                                          00003720
             03 MLFAMO  PIC X(20).                                      00003730
             03 FILLER       PIC X(2).                                  00003740
             03 MSOC1A  PIC X.                                          00003750
             03 MSOC1C  PIC X.                                          00003760
             03 MSOC1P  PIC X.                                          00003770
             03 MSOC1H  PIC X.                                          00003780
             03 MSOC1V  PIC X.                                          00003790
             03 MSOC1O  PIC X(3).                                       00003800
             03 FILLER       PIC X(2).                                  00003810
             03 MENT1A  PIC X.                                          00003820
             03 MENT1C  PIC X.                                          00003830
             03 MENT1P  PIC X.                                          00003840
             03 MENT1H  PIC X.                                          00003850
             03 MENT1V  PIC X.                                          00003860
             03 MENT1O  PIC X(3).                                       00003870
             03 FILLER       PIC X(2).                                  00003880
             03 MSOC2A  PIC X.                                          00003890
             03 MSOC2C  PIC X.                                          00003900
             03 MSOC2P  PIC X.                                          00003910
             03 MSOC2H  PIC X.                                          00003920
             03 MSOC2V  PIC X.                                          00003930
             03 MSOC2O  PIC X(3).                                       00003940
             03 FILLER       PIC X(2).                                  00003950
             03 MENT2A  PIC X.                                          00003960
             03 MENT2C  PIC X.                                          00003970
             03 MENT2P  PIC X.                                          00003980
             03 MENT2H  PIC X.                                          00003990
             03 MENT2V  PIC X.                                          00004000
             03 MENT2O  PIC X(3).                                       00004010
             03 FILLER       PIC X(2).                                  00004020
             03 MSOC3A  PIC X.                                          00004030
             03 MSOC3C  PIC X.                                          00004040
             03 MSOC3P  PIC X.                                          00004050
             03 MSOC3H  PIC X.                                          00004060
             03 MSOC3V  PIC X.                                          00004070
             03 MSOC3O  PIC X(3).                                       00004080
             03 FILLER       PIC X(2).                                  00004090
             03 MENT3A  PIC X.                                          00004100
             03 MENT3C  PIC X.                                          00004110
             03 MENT3P  PIC X.                                          00004120
             03 MENT3H  PIC X.                                          00004130
             03 MENT3V  PIC X.                                          00004140
             03 MENT3O  PIC X(3).                                       00004150
             03 FILLER       PIC X(2).                                  00004160
             03 MSOC4A  PIC X.                                          00004170
             03 MSOC4C  PIC X.                                          00004180
             03 MSOC4P  PIC X.                                          00004190
             03 MSOC4H  PIC X.                                          00004200
             03 MSOC4V  PIC X.                                          00004210
             03 MSOC4O  PIC X(3).                                       00004220
             03 FILLER       PIC X(2).                                  00004230
             03 MENT4A  PIC X.                                          00004240
             03 MENT4C  PIC X.                                          00004250
             03 MENT4P  PIC X.                                          00004260
             03 MENT4H  PIC X.                                          00004270
             03 MENT4V  PIC X.                                          00004280
             03 MENT4O  PIC X(3).                                       00004290
             03 FILLER       PIC X(2).                                  00004300
             03 MSOC5A  PIC X.                                          00004310
             03 MSOC5C  PIC X.                                          00004320
             03 MSOC5P  PIC X.                                          00004330
             03 MSOC5H  PIC X.                                          00004340
             03 MSOC5V  PIC X.                                          00004350
             03 MSOC5O  PIC X(3).                                       00004360
             03 FILLER       PIC X(2).                                  00004370
             03 MENT5A  PIC X.                                          00004380
             03 MENT5C  PIC X.                                          00004390
             03 MENT5P  PIC X.                                          00004400
             03 MENT5H  PIC X.                                          00004410
             03 MENT5V  PIC X.                                          00004420
             03 MENT5O  PIC X(3).                                       00004430
           02 FILLER    PIC X(2).                                       00004440
           02 MZONCMDA  PIC X.                                          00004450
           02 MZONCMDC  PIC X.                                          00004460
           02 MZONCMDP  PIC X.                                          00004470
           02 MZONCMDH  PIC X.                                          00004480
           02 MZONCMDV  PIC X.                                          00004490
           02 MZONCMDO  PIC X(15).                                      00004500
           02 FILLER    PIC X(2).                                       00004510
           02 MLIBERRA  PIC X.                                          00004520
           02 MLIBERRC  PIC X.                                          00004530
           02 MLIBERRP  PIC X.                                          00004540
           02 MLIBERRH  PIC X.                                          00004550
           02 MLIBERRV  PIC X.                                          00004560
           02 MLIBERRO  PIC X(58).                                      00004570
           02 FILLER    PIC X(2).                                       00004580
           02 MCODTRAA  PIC X.                                          00004590
           02 MCODTRAC  PIC X.                                          00004600
           02 MCODTRAP  PIC X.                                          00004610
           02 MCODTRAH  PIC X.                                          00004620
           02 MCODTRAV  PIC X.                                          00004630
           02 MCODTRAO  PIC X(4).                                       00004640
           02 FILLER    PIC X(2).                                       00004650
           02 MCICSA    PIC X.                                          00004660
           02 MCICSC    PIC X.                                          00004670
           02 MCICSP    PIC X.                                          00004680
           02 MCICSH    PIC X.                                          00004690
           02 MCICSV    PIC X.                                          00004700
           02 MCICSO    PIC X(5).                                       00004710
           02 FILLER    PIC X(2).                                       00004720
           02 MNETNAMA  PIC X.                                          00004730
           02 MNETNAMC  PIC X.                                          00004740
           02 MNETNAMP  PIC X.                                          00004750
           02 MNETNAMH  PIC X.                                          00004760
           02 MNETNAMV  PIC X.                                          00004770
           02 MNETNAMO  PIC X(8).                                       00004780
           02 FILLER    PIC X(2).                                       00004790
           02 MSCREENA  PIC X.                                          00004800
           02 MSCREENC  PIC X.                                          00004810
           02 MSCREENP  PIC X.                                          00004820
           02 MSCREENH  PIC X.                                          00004830
           02 MSCREENV  PIC X.                                          00004840
           02 MSCREENO  PIC X(4).                                       00004850
                                                                                
