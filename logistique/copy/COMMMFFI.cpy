      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      * TABLEAU DES ECS A COMPTABILISER PAR FACTURE                             
      * TOUTES LES ZONES "EN ENTREE" DOIVENT ETRE RENSEIGNEES                   
      *****************************************************************         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MFFI-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                  
      *--                                                                       
       01  COMM-MFFI-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
       01  Z-COMMAREA-MFFI.                                                     
      *******  ZONES EN ENTREE                                                  
           02  COMM-MFFI-PREFT1        PIC  X(01).                              
           02  COMM-MFFI-DEXCPT        PIC  X(04).                              
           02  COMM-MFFI-NPIECE        PIC  X(06).                              
           02  COMM-MFFI-NSOC          PIC  X(03).                              
      *******  CODE RETOUR ET MESSAGE                                           
      * SIGNIFICATION DU CODE RETOUR:                                           
      *  OK  - COMPTABILISATION PARFAITE, COMM-MFFI-MSG VIDE                    
      *  KO  - ERREUR DECRITE DANS COMM-MFFI-MSG                                
      *******                                                                   
           02  COMM-MFFI-RCODE         PIC  X(01).                              
               88  COMM-MFFI-OK  VALUE '0'.                                     
               88  COMM-MFFI-KO  VALUE '1'.                                     
           02  COMM-MFFI-MSG           PIC  X(60).                              
      *******  ZONES EN SORTIE                                                  
      *******  NOMBRE DE LIGNES ECS GENEREES                                    
           02  COMM-MFFI-NL            PIC S9(3) PACKED-DECIMAL.                
      *******  ECS GENERES                                                      
           02  COMM-MFFI-TAB-ECS.                                               
               03  COMM-MFFI-TAB-ECS-FILLER OCCURS 80.                          
                   04  COMM-MFFI-COMPTE   PIC  X(06).                           
                   04  COMM-MFFI-SECTION  PIC  X(06).                           
                   04  COMM-MFFI-RUBRIQUE PIC  X(06).                           
                   04  COMM-MFFI-APPARENT PIC  X(05).                           
                   04  COMM-MFFI-SENS     PIC  X(01).                           
                   04  COMM-MFFI-MONTANT  PIC S9(13)V99 PACKED-DECIMAL.         
                   04  COMM-MFFI-NUECS    PIC  X(05).                           
                   04  COMM-MFFI-CRT1     PIC  X(05).                           
                   04  COMM-MFFI-CTVA     PIC  X(01).                           
               03  COMM-MFFI-LITIGE       PIC  X(06)  OCCURS 80.                
           02  FILLER                     PIC  X(99).                           
                                                                                
