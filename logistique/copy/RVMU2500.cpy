      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVMU2500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVMU2500                         
      **********************************************************                
       01  RVMU2500.                                                            
           02  MU25-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  MU25-QPRCCONTR                                                   
               PIC S9(3) COMP-3.                                                
           02  MU25-QMAXART                                                     
               PIC S9(3) COMP-3.                                                
           02  MU25-QCSTLISSAGE                                                 
               PIC S9(1)V9(0002) COMP-3.                                        
           02  MU25-QNBJRUPT                                                    
               PIC S9(3) COMP-3.                                                
           02  MU25-QARTROT1                                                    
               PIC S9(3) COMP-3.                                                
           02  MU25-QARTROT2                                                    
               PIC S9(3) COMP-3.                                                
           02  MU25-QARTROT3                                                    
               PIC S9(3) COMP-3.                                                
           02  MU25-QARTROT4                                                    
               PIC S9(3) COMP-3.                                                
           02  MU25-QARTROT5                                                    
               PIC S9(3) COMP-3.                                                
           02  MU25-QARTROTF                                                    
               PIC S9(3) COMP-3.                                                
           02  MU25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVMU2500                                  
      **********************************************************                
       01  RVMU2500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU25-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-QPRCCONTR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU25-QPRCCONTR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-QMAXART-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU25-QMAXART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-QCSTLISSAGE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU25-QCSTLISSAGE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-QNBJRUPT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU25-QNBJRUPT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-QARTROT1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU25-QARTROT1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-QARTROT2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU25-QARTROT2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-QARTROT3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU25-QARTROT3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-QARTROT4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU25-QARTROT4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-QARTROT5-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU25-QARTROT5-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-QARTROTF-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU25-QARTROTF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  MU25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
