      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00002036
      * COMMAREA SPECIFIQUE PRG TGQ02 (TGQ00 -> MENU)    TR: GQ00     * 00002236
      *          GESTION DES PARAMETRES GENERAUX LIVRAISON            * 00002336
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      * TRANSACTION GQ02 : AFFECTATION DES PROFILS D'AFFILIATION      * 00411036
      *                    ( MAGASIN / PROFIL / MODES DE DELIVRANCE ) * 00411136
      *                                                                 00412000
          02 COMM-GQ02-APPLI REDEFINES COMM-GQ00-APPLI.                 00420036
      *------------------------------ ZONE DONNEES TGQ02                00510036
             03 COMM-GQ02-DONNEES-1-TGQ02.                              00520036
      *------------------------------ CODE SOCIETE                      00560136
                04 COMM-GQ02-FONCT             PIC X(3).                00560241
      *------------------------------ CODE SOCIETE                      00560341
                04 COMM-GQ02-NSOCIETE          PIC X(3).                00560441
      *------------------------------ CODE MAGASIN                      00560536
                04 COMM-GQ02-NMAGASIN          PIC X(3).                00560636
      *------------------------------ LIBELLE MAGASIN                   00560736
                04 COMM-GQ02-LMAGASIN          PIC X(20).               00560836
      *------------------------------ ZONE CONFIRMATION PF3             00743235
                04 COMM-GQ02-CONF-PF3          PIC 9(1).                00743336
      *------------------------------ ZONE CONFIRMATION PF4             00743435
                04 COMM-GQ02-CONF-PF4          PIC 9(1).                00743536
      *------------------------------ DERNIER IND-COMM                  00743639
                04 COMM-GQ02-PAGSUI            PIC 9(02).               00743739
      *------------------------------ ANCIEN IND-COMM                   00743839
                04 COMM-GQ02-PAGENC            PIC 9(02).               00743939
      *------------------------------ NUMERO PAGE ECRAN                 00744038
                04 COMM-GQ02-NUMPAG            PIC 9(3).                00745038
      *------------------------------ INDICATEUR FIN DE TABLE           00746038
                04 COMM-GQ02-INDPAG            PIC 9.                   00747038
      *------------------------------ OK MAJ SUR RTGA01 => VALID = '1'  00752038
                04 COMM-GQ02-VALID-RTGA01      PIC 9.                   00753039
      *------------------------------ TABLE N� COMM PR PAGINATION       00754039
                04 COMM-GQ02-TABCOMM        OCCURS 10.                  00755039
      *------------------------------ 1ERS IND DES PAGES <=> MODDEL     00756039
                      06 COMM-GQ02-NUMCOMM        PIC 9(02).            00757039
      *------------------------------ IND POUR RECH DS COMM             00758039
                04 COMM-GQ02-IND-RECH-COMM  PIC 9(02).                  00759039
      *------------------------------ IND COMM MAX                      00759139
                04 COMM-GQ02-IND-COMM-MAX   PIC 9(02).                  00759239
      *------------------------------ ZONE DONNEES 2 TGQ02              00760036
             03 COMM-GQ02-DONNEES-2-TGQ02.                              00761036
      *------------------------------ CODE PROFIL REAPPRO MAGASIN       00762043
                04 COMM-GQ02-CPROREA           PIC X(5).                00763043
      *------------------------------ LIBELLE PROFIL MAGASIN            00764043
                04 COMM-GQ02-LPROREA           PIC X(20).               00765043
      *------------------------------ CODE MAJ PROFIL MAGASIN           00766043
                04 COMM-GQ02-CMAJ-REA          PIC X(01).               00767043
      *------------------------------ ZONE MODES DELIVRANCES PAGE       00770036
                04 COMM-GQ02-TABMODDEL.                                 00780036
      *------------------------------ ZONE LIGNES MODES DELIVRANCES     00790036
                   05 COMM-GQ02-LIGMODDEL OCCURS 14.                    00800037
      *------------------------------ CODE MODE DELIVRANCE              00810036
                      06 COMM-GQ02-CMODDEL        PIC X(3).             00820036
      *------------------------------ CODE PROFIL D'AFFILIATION         00830036
                      06 COMM-GQ02-CPROAFF        PIC X(5).             00840036
      *------------------------------ LIBELLE PROFIL D'AFFILIATION      00850036
                      06 COMM-GQ02-LPROAFF        PIC X(20).            00860036
      *------------------------------ ZONE DONNEES 3 TGQ02              00870039
             03 COMM-GQ02-DONNEES-3-TGQ02.                              00880039
      *------------------------------ ZONE MODES DELIVRANCES PAGE       00890039
                04 COMM-GQ02-TABMODDEL2.                                00900039
      *------------------------------ ZONE LIGNES MODES DELIVRANCES     00910039
                   05 COMM-GQ02-LIGMODDEL2 OCCURS 99.                   00920042
      *------------------------------ CODE MODE DELIVRANCE              00930039
                      06 COMM-GQ02-CMAJ           PIC X(1).             00940040
      *------------------------------ CODE MODE DELIVRANCE              00941040
                      06 COMM-GQ02-CMODDEL2       PIC X(3).             00942040
      *------------------------------ ZONE PROFIL D'AFFILIATION         00950040
                      06 COMM-GQ02-PROAFF2.                             00960040
      *------------------------------ CODE PROFIL D'AFFILIATION         00961040
                         07 COMM-GQ02-CPROAFF2       PIC X(5).          00962040
      *------------------------------ LIBELLE PROFIL D'AFFILIATION      00970039
                         07 COMM-GQ02-LPROAFF2       PIC X(20).         00980040
      *------------------------------ ZONE DONNEES 4 TGQ02              01650039
             03 COMM-GQ02-DONNEES-4-TGQ02.                              01651039
      *------------------------------ ZONES SWAP                        01670035
                04 COMM-GQ02-ATTR           PIC X OCCURS 200.           01680037
      *------------------------------ ZONE LIBRE                        01710035
             03 COMM-GQ02-LIBRE          PIC X(0171).                   01720043
      ***************************************************************** 02170035
                                                                                
