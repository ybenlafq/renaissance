      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IMU211 AU 10/05/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IMU211.                                                        
            05 NOMETAT-IMU211           PIC X(6) VALUE 'IMU211'.                
            05 RUPTURES-IMU211.                                                 
           10 IMU211-NSOCENTR           PIC X(03).                      007  003
           10 IMU211-NDEPOT             PIC X(03).                      010  003
           10 IMU211-CSELART            PIC X(05).                      013  005
           10 IMU211-NLIEU              PIC X(03).                      018  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IMU211-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IMU211-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IMU211.                                                   
           10 IMU211-LIBELIEU           PIC X(20).                      023  020
           10 IMU211-LLIEU              PIC X(20).                      043  020
           10 IMU211-QNBM3QUOTA-1       PIC S9(04)V9(1) COMP-3.         063  003
           10 IMU211-QNBM3QUOTA-2       PIC S9(04)V9(1) COMP-3.         066  003
           10 IMU211-QNBM3QUOTA-3       PIC S9(04)V9(1) COMP-3.         069  003
           10 IMU211-QNBM3QUOTA-4       PIC S9(04)V9(1) COMP-3.         072  003
           10 IMU211-QNBM3QUOTAJ        PIC S9(04)V9(1) COMP-3.         075  003
           10 IMU211-QNBM3QUOTA1        PIC S9(04)V9(1) COMP-3.         078  003
           10 IMU211-QNBM3QUOTA2        PIC S9(04)V9(1) COMP-3.         081  003
           10 IMU211-QNBM3QUOTA3        PIC S9(04)V9(1) COMP-3.         084  003
           10 IMU211-QNBPIECES-1        PIC S9(05)      COMP-3.         087  003
           10 IMU211-QNBPIECES-2        PIC S9(05)      COMP-3.         090  003
           10 IMU211-QNBPIECES-3        PIC S9(05)      COMP-3.         093  003
           10 IMU211-QNBPIECES-4        PIC S9(05)      COMP-3.         096  003
           10 IMU211-QNBPIECESJ         PIC S9(05)      COMP-3.         099  003
           10 IMU211-QNBPIECES1         PIC S9(05)      COMP-3.         102  003
           10 IMU211-QNBPIECES2         PIC S9(05)      COMP-3.         105  003
           10 IMU211-QNBPIECES3         PIC S9(05)      COMP-3.         108  003
           10 IMU211-QNBPQUOTA-1        PIC S9(05)      COMP-3.         111  003
           10 IMU211-QNBPQUOTA-2        PIC S9(05)      COMP-3.         114  003
           10 IMU211-QNBPQUOTA-3        PIC S9(05)      COMP-3.         117  003
           10 IMU211-QNBPQUOTA-4        PIC S9(05)      COMP-3.         120  003
           10 IMU211-QNBPQUOTAJ         PIC S9(05)      COMP-3.         123  003
           10 IMU211-QNBPQUOTA1         PIC S9(05)      COMP-3.         126  003
           10 IMU211-QNBPQUOTA2         PIC S9(05)      COMP-3.         129  003
           10 IMU211-QNBPQUOTA3         PIC S9(05)      COMP-3.         132  003
           10 IMU211-QVOLUME-1          PIC S9(04)V9(1) COMP-3.         135  003
           10 IMU211-QVOLUME-2          PIC S9(04)V9(1) COMP-3.         138  003
           10 IMU211-QVOLUME-3          PIC S9(04)V9(1) COMP-3.         141  003
           10 IMU211-QVOLUME-4          PIC S9(04)V9(1) COMP-3.         144  003
           10 IMU211-QVOLUMEJ           PIC S9(04)V9(1) COMP-3.         147  003
           10 IMU211-QVOLUME1           PIC S9(04)V9(1) COMP-3.         150  003
           10 IMU211-QVOLUME2           PIC S9(04)V9(1) COMP-3.         153  003
           10 IMU211-QVOLUME3           PIC S9(04)V9(1) COMP-3.         156  003
           10 IMU211-DATEJ              PIC X(08).                      159  008
           10 IMU211-DDEBUT             PIC X(08).                      167  008
            05 FILLER                      PIC X(338).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IMU211-LONG           PIC S9(4)   COMP  VALUE +174.           
      *                                                                         
      *--                                                                       
        01  DSECT-IMU211-LONG           PIC S9(4) COMP-5  VALUE +174.           
                                                                                
      *}                                                                        
