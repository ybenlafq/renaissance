      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IFL050 AU 10/01/2002  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,07,BI,A,                          *        
      *                           34,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IFL050.                                                        
            05 NOMETAT-IFL050           PIC X(6) VALUE 'IFL050'.                
            05 RUPTURES-IFL050.                                                 
           10 IFL050-NSOCIETE           PIC X(03).                      007  003
           10 IFL050-WBLBR              PIC X(02).                      010  002
           10 IFL050-WSEQFAM            PIC 9(05).                      012  005
           10 IFL050-CFAM               PIC X(05).                      017  005
           10 IFL050-CMARQ              PIC X(05).                      022  005
           10 IFL050-NCODIC             PIC X(07).                      027  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IFL050-SEQUENCE           PIC S9(04) COMP.                034  002
      *--                                                                       
           10 IFL050-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IFL050.                                                   
           10 IFL050-ENTREPOT1          PIC X(06).                      036  006
           10 IFL050-ENTREPOT2          PIC X(06).                      042  006
           10 IFL050-ENTREPOT3          PIC X(06).                      048  006
           10 IFL050-ENTREPOT4          PIC X(06).                      054  006
           10 IFL050-ENTREPOT5          PIC X(06).                      060  006
           10 IFL050-LLIEU              PIC X(20).                      066  020
           10 IFL050-LREFFOURN          PIC X(20).                      086  020
           10 IFL050-WELATLM            PIC X(03).                      106  003
           10 IFL050-QHAUTEUR           PIC S9(03)      COMP-3.         109  002
           10 IFL050-QLARGEUR           PIC S9(03)      COMP-3.         111  002
           10 IFL050-QNBPRACK           PIC S9(05)      COMP-3.         113  003
           10 IFL050-QNBPRACK1          PIC S9(05)      COMP-3.         116  003
           10 IFL050-QNBPRACK2          PIC S9(05)      COMP-3.         119  003
           10 IFL050-QNBPRACK3          PIC S9(05)      COMP-3.         122  003
           10 IFL050-QNBPRACK4          PIC S9(05)      COMP-3.         125  003
           10 IFL050-QNBPRACK5          PIC S9(05)      COMP-3.         128  003
           10 IFL050-QPOIDS             PIC S9(05)V9(3) COMP-3.         131  005
           10 IFL050-QPROFONDEUR        PIC S9(03)      COMP-3.         136  002
           10 IFL050-VOLUME             PIC S9(01)V9(6) COMP-3.         138  004
            05 FILLER                      PIC X(371).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IFL050-LONG           PIC S9(4)   COMP  VALUE +141.           
      *                                                                         
      *--                                                                       
        01  DSECT-IFL050-LONG           PIC S9(4) COMP-5  VALUE +141.           
                                                                                
      *}                                                                        
