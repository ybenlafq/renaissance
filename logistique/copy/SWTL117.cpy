      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*00000010
      *                                                                *00000020
      *   Gestion des statistiques de livraison                        *00000030
      *                                                                *00000040
      *   D�finition de la structure du fichier FTL117                 *00000050
      *     (Les modes de d�livrance  pour �dition)                    *00000060
      *                                                                *00000070
      *   Extraction da la table GS40.                                 *00000080
      *   S�quentiel ; Longueur 80                                     *00000090
      *                                                                *00000100
      *----------------------------------------------------------------*00000110
      *                                                                *00000120
      * FTL117-NDEPOT     NO DE DEPOT                          001/003 *00000130
      *       -NMAG       no du magasin                        004/006 *00000140
      *       -CFAM       code famille                         007/011 *00000150
      *       -WSEQFAM    no de sequuence                      012/016 *00000160
      *       -LFAM       libelle famille                      017/036 *00000170
      *       -CBB        code brun/blanc                      037/037 *00000180
      *       -CGRP       code group                           038/039 *00000190
      *       -QPED       qmvt si cmoddel =   'ed '            040/043 *00000200
      *       -QPEMX      qmvt si cmoddel =   'em*'            044/047 *00000210
      *       -QPLDM      qmvt si cmoddel =   'ldm'            048/051 *00000220
      *       -QPLDX      qmvt si cmoddel =   'ld*' non ldm    052/055 *00000230
      *       -QPRDX      qmvt si cmoddel =   'rd*'            056/059 *00000240
      *       -QPRM       qmvt si cmoddel =   'rm*'            060/063 *00000250
      *       -QPTOT      qmvt si cmoddel =/= 'r**'            064/067 *00000260
      *       -LIVR       flag si famille livrable(l) ou non(p)068/067 *00000260
      *                      Disponible                         69/80  *00000270
      *----------------------------------------------------------------*00000280
      *                                                                 00000290
       01      :FTL117:-DSECT.                                          00000300
           05  :FTL117:-NDEPOT         PIC  X(03).                      00000310
           05  :FTL117:-NMAG           PIC  X(03).                      00000320
           05  :FTL117:-CFAM           PIC  X(05).                      00000330
           05  :FTL117:-WSEQFAM        PIC  X(05).                      00000340
           05  :FTL117:-LFAM           PIC  X(20).                      00000350
           05  :FTL117:-CBB            PIC  X(01).                      00000360
           05  :FTL117:-CGRP           PIC  X(02).                      00000370
           05  :FTL117:-QPED           PIC  S9(07)      COMP-3.         00000380
           05  :FTL117:-QPEMX          PIC  S9(07)      COMP-3.         00000390
           05  :FTL117:-QPLDM          PIC  S9(07)      COMP-3.         00000400
           05  :FTL117:-QPLDX          PIC  S9(07)      COMP-3.         00000410
           05  :FTL117:-QPRDX          PIC  S9(07)      COMP-3.         00000420
           05  :FTL117:-QPRM           PIC  S9(07)      COMP-3.         00000430
           05  :FTL117:-QPTOT          PIC  S9(07)      COMP-3.         00000440
           05  :FTL117:-LIVR           PIC  X.                          00000440
           05  FILLER                  PIC  X(12).                      00000450
      *                                                                 00000460
                                                                                
