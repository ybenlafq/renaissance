      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS03   EGS03                                              00000020
      ***************************************************************** 00000030
       01   EGS03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCL     COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MSOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSOCF     PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MSOCI     PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDEPOTI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCROSSDOCKL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCROSSDOCKL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCROSSDOCKF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCROSSDOCKI    PIC X.                                     00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCODICI   PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCODICI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSTATUTI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAML     COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MFAML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFAMF     PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MFAMI     PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLFAMI    PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSAPPL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MSENSAPPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSENSAPPF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSENSAPPI      PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMARQUEL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MMARQUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMARQUEF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MMARQUEI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQUEL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLMARQUEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMARQUEF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLMARQUEI      PIC X(20).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSVTEL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MSENSVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSENSVTEF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSENSVTEI      PIC X.                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDISP1L   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDISP1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDISP1F   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDISP1I   PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDISP2L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDISP2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDISP2F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDISP2I   PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM1L   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCOMM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM1F   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCOMM1I   PIC X(20).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRESL     COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MRESL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MRESF     PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MRESI     PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM2L   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCOMM2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM2F   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCOMM2I   PIC X(20).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOTE1L   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCOTE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOTE1F   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCOTE1I   PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOTE2L   COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCOTE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOTE2F   PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCOTE2I   PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM3L   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCOMM3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM3F   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCOMM3I   PIC X(20).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRANSL   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MTRANSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTRANSF   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MTRANSI   PIC X(5).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM4L   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MCOMM4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM4F   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MCOMM4I   PIC X(20).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRETSL   COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MPRETSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPRETSF   PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MPRETSI   PIC X(5).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM5L   COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCOMM5L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM5F   PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCOMM5I   PIC X(20).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHSL      COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MHSL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MHSF      PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MHSI      PIC X(5).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM6L   COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCOMM6L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM6F   PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCOMM6I   PIC X(20).                                      00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM7L   COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCOMM7L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM7F   PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCOMM7I   PIC X(20).                                      00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTALL   COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MTOTALL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTOTALF   PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MTOTALI   PIC X(5).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM8L   COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MCOMM8L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM8F   PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MCOMM8I   PIC X(20).                                      00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MZONCMDI  PIC X(12).                                      00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MLIBERRI  PIC X(61).                                      00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MCODTRAI  PIC X(4).                                       00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MCICSI    PIC X(5).                                       00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MNETNAMI  PIC X(8).                                       00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MSCREENI  PIC X(4).                                       00001530
      ***************************************************************** 00001540
      * SDF: EGS03   EGS03                                              00001550
      ***************************************************************** 00001560
       01   EGS03O REDEFINES EGS03I.                                    00001570
           02 FILLER    PIC X(12).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MDATJOUA  PIC X.                                          00001600
           02 MDATJOUC  PIC X.                                          00001610
           02 MDATJOUP  PIC X.                                          00001620
           02 MDATJOUH  PIC X.                                          00001630
           02 MDATJOUV  PIC X.                                          00001640
           02 MDATJOUO  PIC X(10).                                      00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MTIMJOUA  PIC X.                                          00001670
           02 MTIMJOUC  PIC X.                                          00001680
           02 MTIMJOUP  PIC X.                                          00001690
           02 MTIMJOUH  PIC X.                                          00001700
           02 MTIMJOUV  PIC X.                                          00001710
           02 MTIMJOUO  PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MSOCA     PIC X.                                          00001740
           02 MSOCC     PIC X.                                          00001750
           02 MSOCP     PIC X.                                          00001760
           02 MSOCH     PIC X.                                          00001770
           02 MSOCV     PIC X.                                          00001780
           02 MSOCO     PIC X(3).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MDEPOTA   PIC X.                                          00001810
           02 MDEPOTC   PIC X.                                          00001820
           02 MDEPOTP   PIC X.                                          00001830
           02 MDEPOTH   PIC X.                                          00001840
           02 MDEPOTV   PIC X.                                          00001850
           02 MDEPOTO   PIC X(3).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCROSSDOCKA    PIC X.                                     00001880
           02 MCROSSDOCKC    PIC X.                                     00001890
           02 MCROSSDOCKP    PIC X.                                     00001900
           02 MCROSSDOCKH    PIC X.                                     00001910
           02 MCROSSDOCKV    PIC X.                                     00001920
           02 MCROSSDOCKO    PIC X.                                     00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODICA   PIC X.                                          00001950
           02 MCODICC   PIC X.                                          00001960
           02 MCODICP   PIC X.                                          00001970
           02 MCODICH   PIC X.                                          00001980
           02 MCODICV   PIC X.                                          00001990
           02 MCODICO   PIC X(7).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLCODICA  PIC X.                                          00002020
           02 MLCODICC  PIC X.                                          00002030
           02 MLCODICP  PIC X.                                          00002040
           02 MLCODICH  PIC X.                                          00002050
           02 MLCODICV  PIC X.                                          00002060
           02 MLCODICO  PIC X(20).                                      00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MSTATUTA  PIC X.                                          00002090
           02 MSTATUTC  PIC X.                                          00002100
           02 MSTATUTP  PIC X.                                          00002110
           02 MSTATUTH  PIC X.                                          00002120
           02 MSTATUTV  PIC X.                                          00002130
           02 MSTATUTO  PIC X(3).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MFAMA     PIC X.                                          00002160
           02 MFAMC     PIC X.                                          00002170
           02 MFAMP     PIC X.                                          00002180
           02 MFAMH     PIC X.                                          00002190
           02 MFAMV     PIC X.                                          00002200
           02 MFAMO     PIC X(5).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MLFAMA    PIC X.                                          00002230
           02 MLFAMC    PIC X.                                          00002240
           02 MLFAMP    PIC X.                                          00002250
           02 MLFAMH    PIC X.                                          00002260
           02 MLFAMV    PIC X.                                          00002270
           02 MLFAMO    PIC X(20).                                      00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MSENSAPPA      PIC X.                                     00002300
           02 MSENSAPPC PIC X.                                          00002310
           02 MSENSAPPP PIC X.                                          00002320
           02 MSENSAPPH PIC X.                                          00002330
           02 MSENSAPPV PIC X.                                          00002340
           02 MSENSAPPO      PIC X.                                     00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MMARQUEA  PIC X.                                          00002370
           02 MMARQUEC  PIC X.                                          00002380
           02 MMARQUEP  PIC X.                                          00002390
           02 MMARQUEH  PIC X.                                          00002400
           02 MMARQUEV  PIC X.                                          00002410
           02 MMARQUEO  PIC X(5).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MLMARQUEA      PIC X.                                     00002440
           02 MLMARQUEC PIC X.                                          00002450
           02 MLMARQUEP PIC X.                                          00002460
           02 MLMARQUEH PIC X.                                          00002470
           02 MLMARQUEV PIC X.                                          00002480
           02 MLMARQUEO      PIC X(20).                                 00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MSENSVTEA      PIC X.                                     00002510
           02 MSENSVTEC PIC X.                                          00002520
           02 MSENSVTEP PIC X.                                          00002530
           02 MSENSVTEH PIC X.                                          00002540
           02 MSENSVTEV PIC X.                                          00002550
           02 MSENSVTEO      PIC X.                                     00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MDISP1A   PIC X.                                          00002580
           02 MDISP1C   PIC X.                                          00002590
           02 MDISP1P   PIC X.                                          00002600
           02 MDISP1H   PIC X.                                          00002610
           02 MDISP1V   PIC X.                                          00002620
           02 MDISP1O   PIC X(5).                                       00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MDISP2A   PIC X.                                          00002650
           02 MDISP2C   PIC X.                                          00002660
           02 MDISP2P   PIC X.                                          00002670
           02 MDISP2H   PIC X.                                          00002680
           02 MDISP2V   PIC X.                                          00002690
           02 MDISP2O   PIC X(5).                                       00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCOMM1A   PIC X.                                          00002720
           02 MCOMM1C   PIC X.                                          00002730
           02 MCOMM1P   PIC X.                                          00002740
           02 MCOMM1H   PIC X.                                          00002750
           02 MCOMM1V   PIC X.                                          00002760
           02 MCOMM1O   PIC X(20).                                      00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MRESA     PIC X.                                          00002790
           02 MRESC     PIC X.                                          00002800
           02 MRESP     PIC X.                                          00002810
           02 MRESH     PIC X.                                          00002820
           02 MRESV     PIC X.                                          00002830
           02 MRESO     PIC X(5).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MCOMM2A   PIC X.                                          00002860
           02 MCOMM2C   PIC X.                                          00002870
           02 MCOMM2P   PIC X.                                          00002880
           02 MCOMM2H   PIC X.                                          00002890
           02 MCOMM2V   PIC X.                                          00002900
           02 MCOMM2O   PIC X(20).                                      00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MCOTE1A   PIC X.                                          00002930
           02 MCOTE1C   PIC X.                                          00002940
           02 MCOTE1P   PIC X.                                          00002950
           02 MCOTE1H   PIC X.                                          00002960
           02 MCOTE1V   PIC X.                                          00002970
           02 MCOTE1O   PIC X(5).                                       00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MCOTE2A   PIC X.                                          00003000
           02 MCOTE2C   PIC X.                                          00003010
           02 MCOTE2P   PIC X.                                          00003020
           02 MCOTE2H   PIC X.                                          00003030
           02 MCOTE2V   PIC X.                                          00003040
           02 MCOTE2O   PIC X(5).                                       00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MCOMM3A   PIC X.                                          00003070
           02 MCOMM3C   PIC X.                                          00003080
           02 MCOMM3P   PIC X.                                          00003090
           02 MCOMM3H   PIC X.                                          00003100
           02 MCOMM3V   PIC X.                                          00003110
           02 MCOMM3O   PIC X(20).                                      00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MTRANSA   PIC X.                                          00003140
           02 MTRANSC   PIC X.                                          00003150
           02 MTRANSP   PIC X.                                          00003160
           02 MTRANSH   PIC X.                                          00003170
           02 MTRANSV   PIC X.                                          00003180
           02 MTRANSO   PIC X(5).                                       00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MCOMM4A   PIC X.                                          00003210
           02 MCOMM4C   PIC X.                                          00003220
           02 MCOMM4P   PIC X.                                          00003230
           02 MCOMM4H   PIC X.                                          00003240
           02 MCOMM4V   PIC X.                                          00003250
           02 MCOMM4O   PIC X(20).                                      00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MPRETSA   PIC X.                                          00003280
           02 MPRETSC   PIC X.                                          00003290
           02 MPRETSP   PIC X.                                          00003300
           02 MPRETSH   PIC X.                                          00003310
           02 MPRETSV   PIC X.                                          00003320
           02 MPRETSO   PIC X(5).                                       00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MCOMM5A   PIC X.                                          00003350
           02 MCOMM5C   PIC X.                                          00003360
           02 MCOMM5P   PIC X.                                          00003370
           02 MCOMM5H   PIC X.                                          00003380
           02 MCOMM5V   PIC X.                                          00003390
           02 MCOMM5O   PIC X(20).                                      00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MHSA      PIC X.                                          00003420
           02 MHSC      PIC X.                                          00003430
           02 MHSP      PIC X.                                          00003440
           02 MHSH      PIC X.                                          00003450
           02 MHSV      PIC X.                                          00003460
           02 MHSO      PIC X(5).                                       00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MCOMM6A   PIC X.                                          00003490
           02 MCOMM6C   PIC X.                                          00003500
           02 MCOMM6P   PIC X.                                          00003510
           02 MCOMM6H   PIC X.                                          00003520
           02 MCOMM6V   PIC X.                                          00003530
           02 MCOMM6O   PIC X(20).                                      00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MCOMM7A   PIC X.                                          00003560
           02 MCOMM7C   PIC X.                                          00003570
           02 MCOMM7P   PIC X.                                          00003580
           02 MCOMM7H   PIC X.                                          00003590
           02 MCOMM7V   PIC X.                                          00003600
           02 MCOMM7O   PIC X(20).                                      00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MTOTALA   PIC X.                                          00003630
           02 MTOTALC   PIC X.                                          00003640
           02 MTOTALP   PIC X.                                          00003650
           02 MTOTALH   PIC X.                                          00003660
           02 MTOTALV   PIC X.                                          00003670
           02 MTOTALO   PIC X(5).                                       00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MCOMM8A   PIC X.                                          00003700
           02 MCOMM8C   PIC X.                                          00003710
           02 MCOMM8P   PIC X.                                          00003720
           02 MCOMM8H   PIC X.                                          00003730
           02 MCOMM8V   PIC X.                                          00003740
           02 MCOMM8O   PIC X(20).                                      00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MZONCMDA  PIC X.                                          00003770
           02 MZONCMDC  PIC X.                                          00003780
           02 MZONCMDP  PIC X.                                          00003790
           02 MZONCMDH  PIC X.                                          00003800
           02 MZONCMDV  PIC X.                                          00003810
           02 MZONCMDO  PIC X(12).                                      00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MLIBERRA  PIC X.                                          00003840
           02 MLIBERRC  PIC X.                                          00003850
           02 MLIBERRP  PIC X.                                          00003860
           02 MLIBERRH  PIC X.                                          00003870
           02 MLIBERRV  PIC X.                                          00003880
           02 MLIBERRO  PIC X(61).                                      00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MCODTRAA  PIC X.                                          00003910
           02 MCODTRAC  PIC X.                                          00003920
           02 MCODTRAP  PIC X.                                          00003930
           02 MCODTRAH  PIC X.                                          00003940
           02 MCODTRAV  PIC X.                                          00003950
           02 MCODTRAO  PIC X(4).                                       00003960
           02 FILLER    PIC X(2).                                       00003970
           02 MCICSA    PIC X.                                          00003980
           02 MCICSC    PIC X.                                          00003990
           02 MCICSP    PIC X.                                          00004000
           02 MCICSH    PIC X.                                          00004010
           02 MCICSV    PIC X.                                          00004020
           02 MCICSO    PIC X(5).                                       00004030
           02 FILLER    PIC X(2).                                       00004040
           02 MNETNAMA  PIC X.                                          00004050
           02 MNETNAMC  PIC X.                                          00004060
           02 MNETNAMP  PIC X.                                          00004070
           02 MNETNAMH  PIC X.                                          00004080
           02 MNETNAMV  PIC X.                                          00004090
           02 MNETNAMO  PIC X(8).                                       00004100
           02 FILLER    PIC X(2).                                       00004110
           02 MSCREENA  PIC X.                                          00004120
           02 MSCREENC  PIC X.                                          00004130
           02 MSCREENP  PIC X.                                          00004140
           02 MSCREENH  PIC X.                                          00004150
           02 MSCREENV  PIC X.                                          00004160
           02 MSCREENO  PIC X(4).                                       00004170
                                                                                
