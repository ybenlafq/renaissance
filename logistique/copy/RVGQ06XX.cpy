      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RTGQ06  COPY                                         
      **********************************************************                
      *   LISTE DES HOST VARIABLES DES TABLES RVGQ0699 ET RTGQ06                
      **********************************************************                
       01  RVGQ06XX.                                                            
           02  GQ06X-CTYPE                                                      
               PIC X(0001).                                                     
           02  GQ06X-CINSEE                                                     
               PIC X(0005).                                                     
           02  GQ06X-WPARCOM                                                    
               PIC X(0001).                                                     
           02  GQ06X-LCOMUNE                                                    
               PIC X(0032).                                                     
           02  GQ06X-CPOSTAL                                                    
               PIC X(0005).                                                     
           02  GQ06X-WPARBUR                                                    
               PIC X(0001).                                                     
           02  GQ06X-LBUREAU                                                    
               PIC X(0026).                                                     
           02  GQ06X-DSYST                                                      
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGQ06XX                                  
      **********************************************************                
       01  RVGQ06XX-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06X-CTYPE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06X-CTYPE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06X-CINSEE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06X-CINSEE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06X-WPARCOM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06X-WPARCOM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06X-LCOMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06X-LCOMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06X-CPOSTAL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06X-CPOSTAL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06X-WPARBUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06X-WPARBUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06X-LBUREAU-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ06X-LBUREAU-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ06X-DSYST-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GQ06X-DSYST-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
