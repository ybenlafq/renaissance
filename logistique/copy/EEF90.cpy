      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * GRF S�lection d'un dossier avoir                                00000020
      ***************************************************************** 00000030
       01   EEF90I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEI    PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPI     PIC X(4).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTRAITI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRAITF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLTRAITI  PIC X(27).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCIETEI     PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLLIEUI   PIC X(19).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCTIERSI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLTIERSI  PIC X(25).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNENVOII  PIC X(7).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDENVOII  PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNENTCDEI      PIC X(5).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLENTCDEI      PIC X(20).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCORDL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNACCORDF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNACCORDI      PIC X(12).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACCORDL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MDACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDACCORDF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDACCORDI      PIC X(10).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCORIGL     COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MNSOCORIGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCORIGF     PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNSOCORIGI     PIC X(3).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUORIGL     COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MLIEUORIGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIEUORIGF     PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIEUORIGI     PIC X(3).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORIGINEL     COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MNORIGINEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNORIGINEF     PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNORIGINEI     PIC X(7).                                  00000850
           02 MLIGNEI OCCURS   9 TIMES .                                00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MACTI   PIC X.                                          00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENREGL      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MENREGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MENREGF      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MENREGI      PIC X(77).                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(79).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(15).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      * NETNAME                                                         00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MNETNAMI  PIC X(8).                                       00001150
      * CODE TERMINAL                                                   00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MSCREENI  PIC X(4).                                       00001200
      ***************************************************************** 00001210
      * GRF S�lection d'un dossier avoir                                00001220
      ***************************************************************** 00001230
       01   EEF90O REDEFINES EEF90I.                                    00001240
           02 FILLER    PIC X(12).                                      00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MDATJOUA  PIC X.                                          00001270
           02 MDATJOUC  PIC X.                                          00001280
           02 MDATJOUP  PIC X.                                          00001290
           02 MDATJOUH  PIC X.                                          00001300
           02 MDATJOUV  PIC X.                                          00001310
           02 MDATJOUO  PIC X(10).                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MTIMJOUA  PIC X.                                          00001340
           02 MTIMJOUC  PIC X.                                          00001350
           02 MTIMJOUP  PIC X.                                          00001360
           02 MTIMJOUH  PIC X.                                          00001370
           02 MTIMJOUV  PIC X.                                          00001380
           02 MTIMJOUO  PIC X(5).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MFONCA    PIC X.                                          00001410
           02 MFONCC    PIC X.                                          00001420
           02 MFONCP    PIC X.                                          00001430
           02 MFONCH    PIC X.                                          00001440
           02 MFONCV    PIC X.                                          00001450
           02 MFONCO    PIC X(3).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MPAGEA    PIC X.                                          00001480
           02 MPAGEC    PIC X.                                          00001490
           02 MPAGEP    PIC X.                                          00001500
           02 MPAGEH    PIC X.                                          00001510
           02 MPAGEV    PIC X.                                          00001520
           02 MPAGEO    PIC ZZZ9.                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNBPA     PIC X.                                          00001550
           02 MNBPC     PIC X.                                          00001560
           02 MNBPP     PIC X.                                          00001570
           02 MNBPH     PIC X.                                          00001580
           02 MNBPV     PIC X.                                          00001590
           02 MNBPO     PIC ZZZ9.                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MCTRAITA  PIC X.                                          00001620
           02 MCTRAITC  PIC X.                                          00001630
           02 MCTRAITP  PIC X.                                          00001640
           02 MCTRAITH  PIC X.                                          00001650
           02 MCTRAITV  PIC X.                                          00001660
           02 MCTRAITO  PIC X(5).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MLTRAITA  PIC X.                                          00001690
           02 MLTRAITC  PIC X.                                          00001700
           02 MLTRAITP  PIC X.                                          00001710
           02 MLTRAITH  PIC X.                                          00001720
           02 MLTRAITV  PIC X.                                          00001730
           02 MLTRAITO  PIC X(27).                                      00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNSOCIETEA     PIC X.                                     00001760
           02 MNSOCIETEC     PIC X.                                     00001770
           02 MNSOCIETEP     PIC X.                                     00001780
           02 MNSOCIETEH     PIC X.                                     00001790
           02 MNSOCIETEV     PIC X.                                     00001800
           02 MNSOCIETEO     PIC X(3).                                  00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MLLIEUA   PIC X.                                          00001830
           02 MLLIEUC   PIC X.                                          00001840
           02 MLLIEUP   PIC X.                                          00001850
           02 MLLIEUH   PIC X.                                          00001860
           02 MLLIEUV   PIC X.                                          00001870
           02 MLLIEUO   PIC X(19).                                      00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MCTIERSA  PIC X.                                          00001900
           02 MCTIERSC  PIC X.                                          00001910
           02 MCTIERSP  PIC X.                                          00001920
           02 MCTIERSH  PIC X.                                          00001930
           02 MCTIERSV  PIC X.                                          00001940
           02 MCTIERSO  PIC X(5).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MLTIERSA  PIC X.                                          00001970
           02 MLTIERSC  PIC X.                                          00001980
           02 MLTIERSP  PIC X.                                          00001990
           02 MLTIERSH  PIC X.                                          00002000
           02 MLTIERSV  PIC X.                                          00002010
           02 MLTIERSO  PIC X(25).                                      00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MNENVOIA  PIC X.                                          00002040
           02 MNENVOIC  PIC X.                                          00002050
           02 MNENVOIP  PIC X.                                          00002060
           02 MNENVOIH  PIC X.                                          00002070
           02 MNENVOIV  PIC X.                                          00002080
           02 MNENVOIO  PIC X(7).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MDENVOIA  PIC X.                                          00002110
           02 MDENVOIC  PIC X.                                          00002120
           02 MDENVOIP  PIC X.                                          00002130
           02 MDENVOIH  PIC X.                                          00002140
           02 MDENVOIV  PIC X.                                          00002150
           02 MDENVOIO  PIC X(10).                                      00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MNENTCDEA      PIC X.                                     00002180
           02 MNENTCDEC PIC X.                                          00002190
           02 MNENTCDEP PIC X.                                          00002200
           02 MNENTCDEH PIC X.                                          00002210
           02 MNENTCDEV PIC X.                                          00002220
           02 MNENTCDEO      PIC X(5).                                  00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MLENTCDEA      PIC X.                                     00002250
           02 MLENTCDEC PIC X.                                          00002260
           02 MLENTCDEP PIC X.                                          00002270
           02 MLENTCDEH PIC X.                                          00002280
           02 MLENTCDEV PIC X.                                          00002290
           02 MLENTCDEO      PIC X(20).                                 00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MNACCORDA      PIC X.                                     00002320
           02 MNACCORDC PIC X.                                          00002330
           02 MNACCORDP PIC X.                                          00002340
           02 MNACCORDH PIC X.                                          00002350
           02 MNACCORDV PIC X.                                          00002360
           02 MNACCORDO      PIC X(12).                                 00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MDACCORDA      PIC X.                                     00002390
           02 MDACCORDC PIC X.                                          00002400
           02 MDACCORDP PIC X.                                          00002410
           02 MDACCORDH PIC X.                                          00002420
           02 MDACCORDV PIC X.                                          00002430
           02 MDACCORDO      PIC X(10).                                 00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MNSOCORIGA     PIC X.                                     00002460
           02 MNSOCORIGC     PIC X.                                     00002470
           02 MNSOCORIGP     PIC X.                                     00002480
           02 MNSOCORIGH     PIC X.                                     00002490
           02 MNSOCORIGV     PIC X.                                     00002500
           02 MNSOCORIGO     PIC X(3).                                  00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MLIEUORIGA     PIC X.                                     00002530
           02 MLIEUORIGC     PIC X.                                     00002540
           02 MLIEUORIGP     PIC X.                                     00002550
           02 MLIEUORIGH     PIC X.                                     00002560
           02 MLIEUORIGV     PIC X.                                     00002570
           02 MLIEUORIGO     PIC X(3).                                  00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MNORIGINEA     PIC X.                                     00002600
           02 MNORIGINEC     PIC X.                                     00002610
           02 MNORIGINEP     PIC X.                                     00002620
           02 MNORIGINEH     PIC X.                                     00002630
           02 MNORIGINEV     PIC X.                                     00002640
           02 MNORIGINEO     PIC X(7).                                  00002650
           02 MLIGNEO OCCURS   9 TIMES .                                00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MACTA   PIC X.                                          00002680
             03 MACTC   PIC X.                                          00002690
             03 MACTP   PIC X.                                          00002700
             03 MACTH   PIC X.                                          00002710
             03 MACTV   PIC X.                                          00002720
             03 MACTO   PIC X.                                          00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MENREGA      PIC X.                                     00002750
             03 MENREGC PIC X.                                          00002760
             03 MENREGP PIC X.                                          00002770
             03 MENREGH PIC X.                                          00002780
             03 MENREGV PIC X.                                          00002790
             03 MENREGO      PIC X(77).                                 00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MLIBERRA  PIC X.                                          00002820
           02 MLIBERRC  PIC X.                                          00002830
           02 MLIBERRP  PIC X.                                          00002840
           02 MLIBERRH  PIC X.                                          00002850
           02 MLIBERRV  PIC X.                                          00002860
           02 MLIBERRO  PIC X(79).                                      00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MCODTRAA  PIC X.                                          00002890
           02 MCODTRAC  PIC X.                                          00002900
           02 MCODTRAP  PIC X.                                          00002910
           02 MCODTRAH  PIC X.                                          00002920
           02 MCODTRAV  PIC X.                                          00002930
           02 MCODTRAO  PIC X(4).                                       00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MZONCMDA  PIC X.                                          00002960
           02 MZONCMDC  PIC X.                                          00002970
           02 MZONCMDP  PIC X.                                          00002980
           02 MZONCMDH  PIC X.                                          00002990
           02 MZONCMDV  PIC X.                                          00003000
           02 MZONCMDO  PIC X(15).                                      00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MCICSA    PIC X.                                          00003030
           02 MCICSC    PIC X.                                          00003040
           02 MCICSP    PIC X.                                          00003050
           02 MCICSH    PIC X.                                          00003060
           02 MCICSV    PIC X.                                          00003070
           02 MCICSO    PIC X(5).                                       00003080
      * NETNAME                                                         00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MNETNAMA  PIC X.                                          00003110
           02 MNETNAMC  PIC X.                                          00003120
           02 MNETNAMP  PIC X.                                          00003130
           02 MNETNAMH  PIC X.                                          00003140
           02 MNETNAMV  PIC X.                                          00003150
           02 MNETNAMO  PIC X(8).                                       00003160
      * CODE TERMINAL                                                   00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MSCREENA  PIC X.                                          00003190
           02 MSCREENC  PIC X.                                          00003200
           02 MSCREENP  PIC X.                                          00003210
           02 MSCREENH  PIC X.                                          00003220
           02 MSCREENV  PIC X.                                          00003230
           02 MSCREENO  PIC X(4).                                       00003240
                                                                                
