      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *         MAQUETTE COMMAREA STANDARD AIDA COBOL2             *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-----------------------------------------------------------------        
      *                                                                         
YS1194*01  COM-CD10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
YS1194*01  COM-CD10-LONG-COMMAREA PIC S9(4) COMP VALUE +8192.                   
      *--                                                                       
       01  COM-CD10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +8192.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA -----------------------------------------        
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS --------------------------        
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------------        
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------------        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 180 PIC X(1).                               
      *                                                                         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>> ZONES RESERVEES APPLICATIVES <<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *                                                                         
      *----------------------------------------------------------------*        
      * MENU GENERAL DE L'APPLICATION (PROGRAMME TCD10)                *        
      *----------------------------------------------------------------*        
           02 COMM-CD10-MENU.                                                   
      *....................................PARAMETRE POUR CD10                  
              03 COMM-CD10-CD10.                                                
      *...........................................SOC ASSOCIEE AU CICS          
                 04 COMM-CD10-NSOC             PIC X(3).                        
      *..............................................NUM LIEU                   
                 04 COMM-CD10-NMAG             PIC X(3).                        
      *..............................................LIBELLE DU MAG             
                 04 COMM-CD10-LMAG             PIC X(20).                       
      *..............................................MAG DE L'ACID              
                 04 COMM-CD10-NMAG-ACID        PIC X(3).                        
      *...............................................................          
                 04 FILLER                     PIC X(97).                       
      *............................PARAMETRES TRANSMIS PAR TCD10                
              03 COMM-CD10-TRANS.                                               
      *..............................................CODIC                      
                 04 COMM-CD10-NCODIC           PIC X(7).                        
      *.....................................LIB REF FOURNISSEUR                 
                 04 COMM-CD10-LREFFOURN        PIC X(20).                       
      *..............................................CODE FAMILLE               
                 04 COMM-CD10-CFAM             PIC X(5).                        
      *..............................................MARQUE                     
                 04 COMM-CD10-CMARQ            PIC X(5).                        
      *..............................................STATUS                     
                 04 COMM-CD10-LSTATCOMP        PIC X(3).                        
      *..............................................QUANTITE                   
                 04 COMM-CD10-QCOLIDSTOCK      PIC S9(5) COMP-3.                
      *......................................LIB DE LA FAMILLE                  
                 04 COMM-CD10-LFAM             PIC X(20).                       
      *......................................GROUPE DE MAGASIN                  
                 04 COMM-CD10-CGRPMAG          PIC X(2).                        
      *..............................................NUM ZONE DE PRIX           
                 04 COMM-CD10-NZONPRIX         PIC X(2).                        
      *..............................................................           
                 04 COMM-CD10-STAPPR           PIC X(5).                        
      *..............................................................           
                 04 COMM-CD10-STEXPO OCCURS 5  PIC X(1).                        
      *..............................................................           
                 04 COMM-CD10-CRAYON           PIC X(5).                        
      *..............................................................           
                 04 COMM-CD10-LRAYON           PIC X(20).                       
      *..............................................................           
                 04 COMM-CD10-STKRAY           PIC X(3).                        
                 04 COMM-CD10-STKRAY9                                           
                    REDEFINES COMM-CD10-STKRAY PIC 9(3).                        
      *..............................................................           
                 04 COMM-CD10-SIGNE            PIC X.                           
      *..............................................................           
                 04 FILLER                     PIC X(61).                       
      *..............................................................           
PM0305*YS1194 03 COMM-APPLID-CD10              PIC X(2800).                     
YS1194        03 COMM-APPLID-CD10              PIC X(7497).                     
PM0305*       03 COMM-APPLID-CD10              PIC X(800).                      
      *..............................................................           
      *..............................................................           
      *..............................................................           
              03 COMM-CD11-CD11     REDEFINES  COMM-APPLID-CD10.                
                 04 COMM-CD11-GA11.                                             
CL0410*             05 COMM-CD11-WEEKPLUS1        PIC X(6).                     
CL0410              05 COMM-CD11-WEEKPLUS2        PIC X(6).                     
CL0410              05 COMM-CD11-WEEKMOINS1       PIC X(6).                     
CL0410              05 COMM-CD11-WEEKMOINS12      PIC X(6).                     
PM0305*YS1194       05 COMM-CD11-CRAYONFAM OCCURS 500 PIC X(5).                 
YS1194              05 COMM-CD11-CRAYONFAM OCCURS 1000 PIC X(5).                
                 04 COMM-CD11-AUTRES.                                           
                    05 COMM-CD11-PAGE             PIC 9(2).                     
                    05 COMM-CD11-PAGET            PIC 9(2).                     
                    05 COMM-CD11-QSTOCKD            PIC X(5).                   
                    05 COMM-CD11-QSTOCKD9                                       
                       REDEFINES COMM-CD11-QSTOCKD  PIC S9(5).                  
MH0706              05 COMM-CD11-QSTOCK             PIC X(4).                   
                    05 COMM-CD11-QSTOCK9                                        
MH0706                 REDEFINES COMM-CD11-QSTOCK   PIC S9(4).                  
                    05 COMM-CD11-QSTOCKOBJ          PIC X(4).                   
                    05 COMM-CD11-QSTOCKOBJ9                                     
                       REDEFINES COMM-CD11-QSTOCKOBJ PIC 9(4).                  
                    05 COMM-CD11-QSTOCKAV           PIC X(4).                   
                    05 COMM-CD11-QSTOCKAV9                                      
                       REDEFINES COMM-CD11-QSTOCKAV PIC 9(4).                   
                    05 COMM-CD11-QCDENEXP           PIC X(3).                   
                    05 COMM-CD11-QCDENEXP9                                      
                       REDEFINES COMM-CD11-QCDENEXP PIC 9(3).                   
                    05 COMM-CD11-QMANUATT           PIC X(3).                   
                    05 COMM-CD11-QMANUATT9                                      
                       REDEFINES COMM-CD11-QMANUATT PIC 9(3).                   
                    05 COMM-CD11-QENCLIV            PIC X(3).                   
                    05 COMM-CD11-QENCLIV9                                       
                       REDEFINES COMM-CD11-QENCLIV PIC 9(3).                    
                    05 COMM-CD11-PSTDTTC            PIC X(6).                   
                    05 COMM-CD11-PSTDTTC9                                       
                       REDEFINES COMM-CD11-PSTDTTC  PIC 9(4)V99.                
PM0305              05 COMM-CD11-QCOEFF             PIC 9(3).                   
PM0305*             05 COMM-CD11-QCOEFF             PIC 99V9.                   
                    05 COMM-CD11-VTES1              PIC X(4).                   
                    05 COMM-CD11-VTES19                                         
                       REDEFINES COMM-CD11-VTES1    PIC 9(4).                   
                    05 COMM-CD11-VTES2              PIC X(4).                   
                    05 COMM-CD11-VTES29                                         
                       REDEFINES COMM-CD11-VTES2    PIC 9(4).                   
                    05 COMM-CD11-VTES3              PIC X(4).                   
                    05 COMM-CD11-VTES39                                         
                       REDEFINES COMM-CD11-VTES3    PIC 9(4).                   
CP... *             05 COMM-CD11-CEXPFAM            PIC X(05).                  
AA0213              05 COMM-CD11-CEXPFAM            PIC X(10).                  
                    05 COMM-CD11-CAPPRO             PIC X(05).                  
...CP *             05 COMM-CD11-CEXPO              PIC X(05).                  
AA0213              05 COMM-CD11-CEXPO              PIC X(10).                  
CL1110*             05 COMM-CD11-QSTOCKMAX          PIC X(3).                   
     |*             05 COMM-CD11-QSTOCKMAX9                                     
     |*                REDEFINES COMM-CD11-QSTOCKMAX PIC 9(3).                  
     |              05 COMM-CD11-QSTOCKMAX          PIC X(4).                   
     |              05 COMM-CD11-QSTOCKMAX9                                     
CL1110                 REDEFINES COMM-CD11-QSTOCKMAX PIC 9(4).                  
AL0802              05 COMM-CD11-QPLAFD             PIC X(5).                   
  "                 05 COMM-CD11-QPLAFD9                                        
AL0802                 REDEFINES COMM-CD11-QPLAFD   PIC 9(5).                   
CL0410              05 COMM-CD11-WOA                PIC X.                      
     |              05 COMM-CD11-WSENSAPPRO         PIC X.                      
     |              05 COMM-CD11-QRANG              PIC 9(3)  COMP-3.           
     |              05 COMM-CD11-QNBRANG            PIC 9(4)  COMP-3.           
     |              05 COMM-CD11-QNBREF             PIC 9(3)  COMP-3.           
     |              05 COMM-CD11-DCDE               PIC X(8).                   
     |              05 COMM-CD11-NSOCDEPOT          PIC X(3).                   
CL0410              05 COMM-CD11-NDEPOT             PIC X(3).                   
AA1215              05 COMM-CD11-WASSORTMAG         PIC X(1).                   
AA1215           04 COMM-CD11-FILLER                PIC X(2370).                
AA0213*          04 COMM-CD11-FILLER                PIC X(2371).                
YS1194*          04 COMM-CD11-FILLER                PIC X(2376).                
AA0213*YS1194       05 COMM-CD11-FILLER             PIC X(180).                 
CL1110*             05 COMM-CD11-FILLER             PIC X(185).                 
CL1110*             05 COMM-CD11-FILLER             PIC X(186).                 
CL0410*AL0802       05 COMM-CD11-FILLER             PIC X(220).                 
AL0802* SUPRESSION DES APPELS AU PROGRAMMES TCD15 TDC17 TCD18 TCD19             
      *       03 COMM-CD17-CD17     REDEFINES  COMM-APPLID-CD10.                
      *       03 COMM-CD15          REDEFINES  COMM-APPLID-CD10.                
      *       03 COMM-CD18-CD18     REDEFINES  COMM-APPLID-CD10.                
      *       03 COMM-CD19-CD19     REDEFINES  COMM-APPLID-CD10.                
      *                                                                 01000059
              03 COMM-CD20-CD20     REDEFINES  COMM-APPLID-CD10.                
                 04 COMM-CD20-ORIGINE               PIC X(05).                  
                 04 COMM-CD20-TB-ACID.                                          
                    05 COMM-CD20-CCLIENT            PIC X(06).                  
                    05 COMM-CD20-PASS-OBLIGATOIRE   PIC X(01).                  
                    05 COMM-CD20-PASS               PIC X(04).                  
                    05 COMM-CD20-CRITERE            PIC X(01).                  
                    05 COMM-CD20-LCRITERE           PIC X(05).                  
                 04 COMM-CD20-NCODIC                PIC X(07).                  
                 04 COMM-CD20-NPAGE                 PIC 9.                      
                 04 COMM-CD20-TOUCHE                PIC X.                      
                 04 COMM-CD20-BT                    PIC 9.                      
                    88 COMM-CD20-BOUTIQUE       VALUE 1.                        
                    88 COMM-CD20-PAS-BOUTIQUE   VALUE 0.                        
                 04 COMM-CD20-ETAT-CODIC            PIC X OCCURS 8.             
                 04 COMM-CD20-PRIX-AFFICHABLE       PIC X(01).                  
                 04 COMM-CD20-QTE-MAXX.                                         
                    05 COMM-CD20-QTE-MAX            PIC 9(03).                  
YS1194           04 COMM-CD11-FILLER                PIC X(7453).                
                                                                                
