      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IIE505 AU 18/02/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IIE505.                                                        
            05 NOMETAT-IIE505           PIC X(6) VALUE 'IIE505'.                
            05 RUPTURES-IIE505.                                                 
           10 IIE505-NSOCDEPOT          PIC X(03).                      007  003
           10 IIE505-NDEPOT             PIC X(03).                      010  003
           10 IIE505-NSSLIEU            PIC X(03).                      013  003
           10 IIE505-CLIEUTRT           PIC X(05).                      016  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IIE505-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IIE505-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IIE505.                                                   
           10 IIE505-CSTABLE            PIC X(05).                      023  005
           10 IIE505-LIB-ANOMALIE       PIC X(15).                      028  015
            05 FILLER                      PIC X(470).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IIE505-LONG           PIC S9(4)   COMP  VALUE +042.           
      *                                                                         
      *--                                                                       
        01  DSECT-IIE505-LONG           PIC S9(4) COMP-5  VALUE +042.           
                                                                                
      *}                                                                        
