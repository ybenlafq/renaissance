      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IMU212 AU 10/05/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IMU212.                                                        
            05 NOMETAT-IMU212           PIC X(6) VALUE 'IMU212'.                
            05 RUPTURES-IMU212.                                                 
           10 IMU212-NSOCENTR           PIC X(03).                      007  003
           10 IMU212-NDEPOT             PIC X(03).                      010  003
           10 IMU212-CSELART            PIC X(05).                      013  005
           10 IMU212-NLIEU              PIC X(03).                      018  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IMU212-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IMU212-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IMU212.                                                   
           10 IMU212-LIBELIEU           PIC X(20).                      023  020
           10 IMU212-LLIEU              PIC X(20).                      043  020
           10 IMU212-QNBM3QUOTA-1       PIC S9(04)V9(1) COMP-3.         063  003
           10 IMU212-QNBM3QUOTA-2       PIC S9(04)V9(1) COMP-3.         066  003
           10 IMU212-QNBM3QUOTA-3       PIC S9(04)V9(1) COMP-3.         069  003
           10 IMU212-QNBM3QUOTA-4       PIC S9(04)V9(1) COMP-3.         072  003
           10 IMU212-QNBM3QUOTAJ        PIC S9(04)V9(1) COMP-3.         075  003
           10 IMU212-QNBM3QUOTA1        PIC S9(04)V9(1) COMP-3.         078  003
           10 IMU212-QNBM3QUOTA2        PIC S9(04)V9(1) COMP-3.         081  003
           10 IMU212-QNBPIECES-1        PIC S9(05)      COMP-3.         084  003
           10 IMU212-QNBPIECES-2        PIC S9(05)      COMP-3.         087  003
           10 IMU212-QNBPIECES-3        PIC S9(05)      COMP-3.         090  003
           10 IMU212-QNBPIECES-4        PIC S9(05)      COMP-3.         093  003
           10 IMU212-QNBPIECESJ         PIC S9(05)      COMP-3.         096  003
           10 IMU212-QNBPIECES1         PIC S9(05)      COMP-3.         099  003
           10 IMU212-QNBPIECES2         PIC S9(05)      COMP-3.         102  003
           10 IMU212-QVOLUME-1          PIC S9(04)V9(1) COMP-3.         105  003
           10 IMU212-QVOLUME-2          PIC S9(04)V9(1) COMP-3.         108  003
           10 IMU212-QVOLUME-3          PIC S9(04)V9(1) COMP-3.         111  003
           10 IMU212-QVOLUME-4          PIC S9(04)V9(1) COMP-3.         114  003
           10 IMU212-QVOLUMEJ           PIC S9(04)V9(1) COMP-3.         117  003
           10 IMU212-QVOLUME1           PIC S9(04)V9(1) COMP-3.         120  003
           10 IMU212-QVOLUME2           PIC S9(04)V9(1) COMP-3.         123  003
           10 IMU212-DATEJ              PIC X(08).                      126  008
           10 IMU212-DDEBUT             PIC X(08).                      134  008
            05 FILLER                      PIC X(371).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IMU212-LONG           PIC S9(4)   COMP  VALUE +141.           
      *                                                                         
      *--                                                                       
        01  DSECT-IMU212-LONG           PIC S9(4) COMP-5  VALUE +141.           
                                                                                
      *}                                                                        
