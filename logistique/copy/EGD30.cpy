      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD30   EGD30                                              00000020
      ***************************************************************** 00000030
       01   EGD30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(14).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDEPF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCDEPI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJOURL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDJOURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDJOURF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDJOURI   PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRAFL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNRAFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNRAFF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNRAFI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELARTL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCSELARTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSELARTF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCSELARTI      PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEPOTL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCDEPOTF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCDEPOTI  PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNSOCI    PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBERRI  PIC X(78).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCODTRAI  PIC X(4).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCICSI    PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNETNAMI  PIC X(8).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSCREENI  PIC X(4).                                       00000690
      ***************************************************************** 00000700
      * SDF: EGD30   EGD30                                              00000710
      ***************************************************************** 00000720
       01   EGD30O REDEFINES EGD30I.                                    00000730
           02 FILLER    PIC X(12).                                      00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MDATJOUA  PIC X.                                          00000760
           02 MDATJOUC  PIC X.                                          00000770
           02 MDATJOUP  PIC X.                                          00000780
           02 MDATJOUH  PIC X.                                          00000790
           02 MDATJOUV  PIC X.                                          00000800
           02 MDATJOUO  PIC X(10).                                      00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MTIMJOUA  PIC X.                                          00000830
           02 MTIMJOUC  PIC X.                                          00000840
           02 MTIMJOUP  PIC X.                                          00000850
           02 MTIMJOUH  PIC X.                                          00000860
           02 MTIMJOUV  PIC X.                                          00000870
           02 MTIMJOUO  PIC X(5).                                       00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MZONCMDA  PIC X.                                          00000900
           02 MZONCMDC  PIC X.                                          00000910
           02 MZONCMDP  PIC X.                                          00000920
           02 MZONCMDH  PIC X.                                          00000930
           02 MZONCMDV  PIC X.                                          00000940
           02 MZONCMDO  PIC X(14).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MWFONCA   PIC X.                                          00000970
           02 MWFONCC   PIC X.                                          00000980
           02 MWFONCP   PIC X.                                          00000990
           02 MWFONCH   PIC X.                                          00001000
           02 MWFONCV   PIC X.                                          00001010
           02 MWFONCO   PIC X(3).                                       00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MNSOCDEPA      PIC X.                                     00001040
           02 MNSOCDEPC PIC X.                                          00001050
           02 MNSOCDEPP PIC X.                                          00001060
           02 MNSOCDEPH PIC X.                                          00001070
           02 MNSOCDEPV PIC X.                                          00001080
           02 MNSOCDEPO      PIC X(3).                                  00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MNDEPOTA  PIC X.                                          00001110
           02 MNDEPOTC  PIC X.                                          00001120
           02 MNDEPOTP  PIC X.                                          00001130
           02 MNDEPOTH  PIC X.                                          00001140
           02 MNDEPOTV  PIC X.                                          00001150
           02 MNDEPOTO  PIC X(3).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MDJOURA   PIC X.                                          00001180
           02 MDJOURC   PIC X.                                          00001190
           02 MDJOURP   PIC X.                                          00001200
           02 MDJOURH   PIC X.                                          00001210
           02 MDJOURV   PIC X.                                          00001220
           02 MDJOURO   PIC X(8).                                       00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNRAFA    PIC X.                                          00001250
           02 MNRAFC    PIC X.                                          00001260
           02 MNRAFP    PIC X.                                          00001270
           02 MNRAFH    PIC X.                                          00001280
           02 MNRAFV    PIC X.                                          00001290
           02 MNRAFO    PIC X(3).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MCSELARTA      PIC X.                                     00001320
           02 MCSELARTC PIC X.                                          00001330
           02 MCSELARTP PIC X.                                          00001340
           02 MCSELARTH PIC X.                                          00001350
           02 MCSELARTV PIC X.                                          00001360
           02 MCSELARTO      PIC X(5).                                  00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MCDEPOTA  PIC X.                                          00001390
           02 MCDEPOTC  PIC X.                                          00001400
           02 MCDEPOTP  PIC X.                                          00001410
           02 MCDEPOTH  PIC X.                                          00001420
           02 MCDEPOTV  PIC X.                                          00001430
           02 MCDEPOTO  PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNSOCA    PIC X.                                          00001460
           02 MNSOCC    PIC X.                                          00001470
           02 MNSOCP    PIC X.                                          00001480
           02 MNSOCH    PIC X.                                          00001490
           02 MNSOCV    PIC X.                                          00001500
           02 MNSOCO    PIC X(3).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MLIBERRA  PIC X.                                          00001530
           02 MLIBERRC  PIC X.                                          00001540
           02 MLIBERRP  PIC X.                                          00001550
           02 MLIBERRH  PIC X.                                          00001560
           02 MLIBERRV  PIC X.                                          00001570
           02 MLIBERRO  PIC X(78).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCODTRAA  PIC X.                                          00001600
           02 MCODTRAC  PIC X.                                          00001610
           02 MCODTRAP  PIC X.                                          00001620
           02 MCODTRAH  PIC X.                                          00001630
           02 MCODTRAV  PIC X.                                          00001640
           02 MCODTRAO  PIC X(4).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCICSA    PIC X.                                          00001670
           02 MCICSC    PIC X.                                          00001680
           02 MCICSP    PIC X.                                          00001690
           02 MCICSH    PIC X.                                          00001700
           02 MCICSV    PIC X.                                          00001710
           02 MCICSO    PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MNETNAMA  PIC X.                                          00001740
           02 MNETNAMC  PIC X.                                          00001750
           02 MNETNAMP  PIC X.                                          00001760
           02 MNETNAMH  PIC X.                                          00001770
           02 MNETNAMV  PIC X.                                          00001780
           02 MNETNAMO  PIC X(8).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MSCREENA  PIC X.                                          00001810
           02 MSCREENC  PIC X.                                          00001820
           02 MSCREENP  PIC X.                                          00001830
           02 MSCREENH  PIC X.                                          00001840
           02 MSCREENV  PIC X.                                          00001850
           02 MSCREENO  PIC X(4).                                       00001860
                                                                                
