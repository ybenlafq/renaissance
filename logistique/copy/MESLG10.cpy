      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:03 >
      
      *****************************************************************
      *   COPY MESSAGE DETAIL MQ RECU PAR LE HOST EN PROVENANCE DE 360
      *   PAR LE PROGRAMME MLG10
      *****************************************************************
      *     M O D I F I C A T I O N S  /  M A I N T E N A N C E S      *
      ******************************************************************
      *  SSAAMMJJ / AUTEUR / REPAIRE / DESCRIPTIF MAINTENANCE          *
      *  -------- / ------ / ------- / ------------------------------- *
      ******************************************************************
      *
      * DETAIL DU MESSAGE                                           500
        01 MESLG10-DETAIL.
           03 MESLG10-D-SOC-PTF    PIC X(03) VALUE SPACES.
           03 MESLG10-D-LIEU-PTF   PIC X(03) VALUE SPACES.
           03 MESLG10-D-DDELIV     PIC X(08) VALUE SPACES.
           03 MESLG10-D-CTOURNEE   PIC X(03) VALUE SPACES.
           03 MESLG10-D-NORDRE     PIC X(02) VALUE SPACES.
           03 MESLG10-D-CPROTOUR   PIC X(05) VALUE SPACES.
           03 MESLG10-D-NSOCIETE   PIC X(03) VALUE SPACES.
           03 MESLG10-D-NLIEU      PIC X(03) VALUE SPACES.
           03 MESLG10-D-NVENTE     PIC X(07) VALUE SPACES.
           03 MESLG10-D-CADRTOUR   PIC X(01) VALUE SPACES.
           03 MESLG10-D-LPLAGE     PIC X(04) VALUE SPACES.
           03 MESLG10-D-CTYPE      PIC X(01) VALUE SPACES.
           03 MESLG10-D-HORAIRE    PIC X(06) VALUE SPACES.
           03 MESLG10-D-LIVR1      PIC X(10) VALUE SPACES.
           03 MESLG10-D-LIVR2      PIC X(10) VALUE SPACES.
           03 MESLG10-D-LIVR3      PIC X(10) VALUE SPACES.
      *    LA ZONE CRENEAU PASSE DE 5 A 7
           03 MESLG10-D-CRENEAU    PIC X(07) VALUE SPACES.
           03 MESLG10-D-CTYPEASS   PIC X(02) VALUE SPACES.
           03 MESLG10-D-LNOM       PIC X(25) VALUE SPACES.
           03 MESLG10-D-CPOSTAL    PIC X(05) VALUE SPACES.
           03 MESLG10-D-LCOMMUNE   PIC X(32) VALUE SPACES.
      *    03 FILLER               PIC X(352) VALUE SPACES.
           03 FILLER               PIC X(350) VALUE SPACES.
      *
      
