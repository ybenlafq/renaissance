      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * DCLGEN TABLE(DEVNCG00.RVFL4010)                                *        
      *        LIBRARY(DSA.PH1C.DCLGEN(RVFL4010))                      *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(FL40-)                                            *        
      *        STRUCTURE(RVFL4010)                                     *        
      *        APOST                                                   *        
      *        COLSUFFIX(YES)                                          *        
      *        INDVAR(YES)                                             *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVFL4010                  *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL4010.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL4010.                                                            
      *}                                                                        
      *                       NSOCIETE                                          
           10 FL40-NSOCIETE        PIC X(3).                                    
      *                       NCODIC                                            
           10 FL40-NCODIC          PIC X(7).                                    
      *                       CFAM                                              
           10 FL40-CFAM            PIC X(5).                                    
      *                       WSEQFAM                                           
           10 FL40-WSEQFAM         PIC S9(5)V USAGE COMP-3.                     
      *                       CMARQ                                             
           10 FL40-CMARQ           PIC X(5).                                    
      *                       LREFFOURN                                         
           10 FL40-LREFFOURN       PIC X(20).                                   
      *                       WDACEM                                            
           10 FL40-WDACEM          PIC X(1).                                    
      *                       CASSORT                                           
           10 FL40-CASSORT         PIC X(5).                                    
      *                       LSTATCOMP                                         
           10 FL40-LSTATCOMP       PIC X(3).                                    
      *                       CEXPO                                             
           10 FL40-CEXPO           PIC X(5).                                    
      *                       WSENSVTE                                          
           10 FL40-WSENSVTE        PIC X(1).                                    
      *                       CHEFPROD                                          
           10 FL40-CHEFPROD        PIC X(5).                                    
      *                       CHEFPRM                                           
           10 FL40-CHEFPRM         PIC X(5).                                    
      *                       QCOLIVTE                                          
           10 FL40-QCOLIVTE        PIC S9(5)V USAGE COMP-3.                     
      *                       WSTOCKAVANCE                                      
           10 FL40-WSTOCKAVANCE    PIC X(1).                                    
      *                       WTLMELA                                           
           10 FL40-WTLMELA         PIC X(3).                                    
      *                       DSYST                                             
           10 FL40-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       CUNITVTE                                          
           10 FL40-CUNITVTE        PIC X(3).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL4010-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL4010-FLAGS.                                                      
      *}                                                                        
      *                       NSOCIETE                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-NSOCIETE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       NCODIC                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CFAM                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-CFAM-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-CFAM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       WSEQFAM                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-WSEQFAM-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-WSEQFAM-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CMARQ                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-CMARQ-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-CMARQ-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       LREFFOURN                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-LREFFOURN-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-LREFFOURN-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       WDACEM                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-WDACEM-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-WDACEM-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CASSORT                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-CASSORT-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-CASSORT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       LSTATCOMP                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-LSTATCOMP-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-LSTATCOMP-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CEXPO                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-CEXPO-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-CEXPO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       WSENSVTE                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-WSENSVTE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-WSENSVTE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CHEFPROD                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-CHEFPROD-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-CHEFPROD-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CHEFPRM                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-CHEFPRM-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-CHEFPRM-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       QCOLIVTE                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-QCOLIVTE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-QCOLIVTE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       WSTOCKAVANCE                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-WSTOCKAVANCE-F  PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-WSTOCKAVANCE-F  PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       WTLMELA                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-WTLMELA-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-WTLMELA-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       DSYST                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *                       CUNITVTE                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FL40-CUNITVTE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 FL40-CUNITVTE-F      PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 18      *        
      ******************************************************************        
                                                                                
