      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGB3000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB3000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB3000.                                                            
           02  GB30-NLIEUENT                                                    
               PIC X(0003).                                                     
           02  GB30-NHS                                                         
               PIC X(0007).                                                     
           02  GB30-NCODIC                                                      
               PIC X(0007).                                                     
           02  GB30-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GB30-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GB30-NLIEU                                                       
               PIC X(0003).                                                     
           02  GB30-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  GB30-CLIEUTRT                                                    
               PIC X(0005).                                                     
           02  GB30-CSTATUTTRT                                                  
               PIC X(0005).                                                     
           02  GB30-LEMPLACT                                                    
               PIC X(0005).                                                     
           02  GB30-LCOMMENTTRT                                                 
               PIC X(0020).                                                     
           02  GB30-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGB3000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB3000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-NLIEUENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-NLIEUENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-NHS-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-NHS-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-NSSLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-CLIEUTRT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-CLIEUTRT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-CSTATUTTRT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-CSTATUTTRT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-LEMPLACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-LEMPLACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-LCOMMENTTRT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-LCOMMENTTRT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
