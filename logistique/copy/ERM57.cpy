      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM57   ERM57                                              00000020
      ***************************************************************** 00000030
       01   ERM57I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGESL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGESL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNPAGESF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGESI  PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCGROUPI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGROUPL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLGROUPF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLGROUPI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLERL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCLERL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCLERF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCLERI    PIC X(5).                                       00000330
           02 LIGNEI OCCURS   17 TIMES .                                00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSMG1L      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNSMG1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSMG1F      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNSMG1I      PIC X(6).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMAG1L      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MLMAG1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLMAG1F      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MLMAG1I      PIC X(14).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE1L  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MQTE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE1F  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MQTE1I  PIC X(3).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSMG2L      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNSMG2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSMG2F      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNSMG2I      PIC X(6).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMAG2L      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLMAG2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLMAG2F      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLMAG2I      PIC X(14).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE2L  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MQTE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE2F  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MQTE2I  PIC X(3).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSMG3L      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNSMG3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSMG3F      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNSMG3I      PIC X(6).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMAG3L      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLMAG3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLMAG3F      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLMAG3I      PIC X(14).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE3L  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MQTE3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE3F  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQTE3I  PIC X(3).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MZONCMDI  PIC X(12).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(61).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: ERM57   ERM57                                              00000960
      ***************************************************************** 00000970
       01   ERM57O REDEFINES ERM57I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNPAGEA   PIC X.                                          00001150
           02 MNPAGEC   PIC X.                                          00001160
           02 MNPAGEP   PIC X.                                          00001170
           02 MNPAGEH   PIC X.                                          00001180
           02 MNPAGEV   PIC X.                                          00001190
           02 MNPAGEO   PIC X(2).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MNPAGESA  PIC X.                                          00001220
           02 MNPAGESC  PIC X.                                          00001230
           02 MNPAGESP  PIC X.                                          00001240
           02 MNPAGESH  PIC X.                                          00001250
           02 MNPAGESV  PIC X.                                          00001260
           02 MNPAGESO  PIC X(2).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MCGROUPA  PIC X.                                          00001290
           02 MCGROUPC  PIC X.                                          00001300
           02 MCGROUPP  PIC X.                                          00001310
           02 MCGROUPH  PIC X.                                          00001320
           02 MCGROUPV  PIC X.                                          00001330
           02 MCGROUPO  PIC X(5).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLGROUPA  PIC X.                                          00001360
           02 MLGROUPC  PIC X.                                          00001370
           02 MLGROUPP  PIC X.                                          00001380
           02 MLGROUPH  PIC X.                                          00001390
           02 MLGROUPV  PIC X.                                          00001400
           02 MLGROUPO  PIC X(20).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCLERA    PIC X.                                          00001430
           02 MCLERC    PIC X.                                          00001440
           02 MCLERP    PIC X.                                          00001450
           02 MCLERH    PIC X.                                          00001460
           02 MCLERV    PIC X.                                          00001470
           02 MCLERO    PIC X(5).                                       00001480
           02 LIGNEO OCCURS   17 TIMES .                                00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MNSMG1A      PIC X.                                     00001510
             03 MNSMG1C PIC X.                                          00001520
             03 MNSMG1P PIC X.                                          00001530
             03 MNSMG1H PIC X.                                          00001540
             03 MNSMG1V PIC X.                                          00001550
             03 MNSMG1O      PIC X(6).                                  00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MLMAG1A      PIC X.                                     00001580
             03 MLMAG1C PIC X.                                          00001590
             03 MLMAG1P PIC X.                                          00001600
             03 MLMAG1H PIC X.                                          00001610
             03 MLMAG1V PIC X.                                          00001620
             03 MLMAG1O      PIC X(14).                                 00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MQTE1A  PIC X.                                          00001650
             03 MQTE1C  PIC X.                                          00001660
             03 MQTE1P  PIC X.                                          00001670
             03 MQTE1H  PIC X.                                          00001680
             03 MQTE1V  PIC X.                                          00001690
             03 MQTE1O  PIC X(3).                                       00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MNSMG2A      PIC X.                                     00001720
             03 MNSMG2C PIC X.                                          00001730
             03 MNSMG2P PIC X.                                          00001740
             03 MNSMG2H PIC X.                                          00001750
             03 MNSMG2V PIC X.                                          00001760
             03 MNSMG2O      PIC X(6).                                  00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MLMAG2A      PIC X.                                     00001790
             03 MLMAG2C PIC X.                                          00001800
             03 MLMAG2P PIC X.                                          00001810
             03 MLMAG2H PIC X.                                          00001820
             03 MLMAG2V PIC X.                                          00001830
             03 MLMAG2O      PIC X(14).                                 00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MQTE2A  PIC X.                                          00001860
             03 MQTE2C  PIC X.                                          00001870
             03 MQTE2P  PIC X.                                          00001880
             03 MQTE2H  PIC X.                                          00001890
             03 MQTE2V  PIC X.                                          00001900
             03 MQTE2O  PIC X(3).                                       00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MNSMG3A      PIC X.                                     00001930
             03 MNSMG3C PIC X.                                          00001940
             03 MNSMG3P PIC X.                                          00001950
             03 MNSMG3H PIC X.                                          00001960
             03 MNSMG3V PIC X.                                          00001970
             03 MNSMG3O      PIC X(6).                                  00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MLMAG3A      PIC X.                                     00002000
             03 MLMAG3C PIC X.                                          00002010
             03 MLMAG3P PIC X.                                          00002020
             03 MLMAG3H PIC X.                                          00002030
             03 MLMAG3V PIC X.                                          00002040
             03 MLMAG3O      PIC X(14).                                 00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MQTE3A  PIC X.                                          00002070
             03 MQTE3C  PIC X.                                          00002080
             03 MQTE3P  PIC X.                                          00002090
             03 MQTE3H  PIC X.                                          00002100
             03 MQTE3V  PIC X.                                          00002110
             03 MQTE3O  PIC X(3).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MZONCMDA  PIC X.                                          00002140
           02 MZONCMDC  PIC X.                                          00002150
           02 MZONCMDP  PIC X.                                          00002160
           02 MZONCMDH  PIC X.                                          00002170
           02 MZONCMDV  PIC X.                                          00002180
           02 MZONCMDO  PIC X(12).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(61).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
