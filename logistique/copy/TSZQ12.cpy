      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *  PROJET     : QUOTAS DECENTRALISES                             *        
      *  TITRE      : TS DE MISE A JOUR DE SOUS-TABLE                  *        
      *  APPELER PAR: TZQ12 - LIENS CODES POSTAUX / ZONES ELEMENTAIRES *        
      *                                                                *        
      *  CONTENU    : 1 ITEM PAR PAGE                                  *00000050
      *               13 LIGNES PAR ITEM                               *00000051
      *              (1 LIGNE CORRESPOND A UNE LIGNE DE L'ECRAN)       *        
      *  LONGUEUR   : 100 C * 10 LG = 1000 C                           *        
      ******************************************************************        
      *                                                                         
       01  TS-DONNEES.                                                          
           05 TS-LIGNE               OCCURS 10.                                 
              10 TS-RVGQ0100-FICHIER.                                           
                 15 TS-GQ06-CPOSTAL        PIC X(05).                           
                 15 TS-GQ06-LCOMUNE        PIC X(32).                           
                 15 TS-GQ01-CINSEE         PIC X(05).                           
                 15 TS-GQ01-WCONTRA        PIC X(01).                           
                 15 TS-GQ01-CELEMEN        PIC X(05).                           
                 15 TS-ELEMG-LZONELE       PIC X(20).                           
                 15 TS-WS-CADRE            PIC X(05).                           
              10 TS-FILLER                 PIC X(32).                           
                                                                                
