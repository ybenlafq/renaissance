      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGQ01   EGQ01                                              00000020
      ***************************************************************** 00000030
       01   EFL21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEYL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEYL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEYF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEYI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MFONCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROAFFL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCPROAFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPROAFFF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCPROAFFI      PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROAFFL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLPROAFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPROAFFF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLPROAFFI      PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOC1DFL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSOC1DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOC1DFF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSOC1DFI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENT1DFL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MENT1DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENT1DFF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MENT1DFI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOC2DFL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSOC2DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOC2DFF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSOC2DFI  PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENT2DFL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MENT2DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENT2DFF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MENT2DFI  PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOC3DFL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MSOC3DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOC3DFF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSOC3DFI  PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENT3DFL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MENT3DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENT3DFF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MENT3DFI  PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOC4DFL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MSOC4DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOC4DFF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSOC4DFI  PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENT4DFL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MENT4DFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENT4DFF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MENT4DFI  PIC X(3).                                       00000650
           02 MTABLEI OCCURS   15 TIMES .                               00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDACL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MCDACL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCDACF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCDACI  PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCARTL  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MCARTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCARTF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCARTI  PIC X(7).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLARTL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MLARTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLARTF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MLARTI  PIC X(20).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MCMARL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCMARF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCMARI  PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC1L  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MSOC1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC1F  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MSOC1I  PIC X(3).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT1L  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MENT1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT1F  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MENT1I  PIC X(3).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC2L  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MSOC2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC2F  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MSOC2I  PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT2L  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MENT2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT2F  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MENT2I  PIC X(3).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC3L  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MSOC3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC3F  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MSOC3I  PIC X(3).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT3L  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MENT3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT3F  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MENT3I  PIC X(3).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC4L  COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MSOC4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC4F  PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MSOC4I  PIC X(3).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT4L  COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MENT4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT4F  PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MENT4I  PIC X(3).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MZONCMDI  PIC X(15).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MLIBERRI  PIC X(58).                                      00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCODTRAI  PIC X(4).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCICSI    PIC X(5).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MNETNAMI  PIC X(8).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MSCREENI  PIC X(4).                                       00001380
      ***************************************************************** 00001390
      * SDF: EGQ01   EGQ01                                              00001400
      ***************************************************************** 00001410
       01   EFL21O REDEFINES EFL21I.                                    00001420
           02 FILLER    PIC X(12).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MDATJOUA  PIC X.                                          00001450
           02 MDATJOUC  PIC X.                                          00001460
           02 MDATJOUP  PIC X.                                          00001470
           02 MDATJOUH  PIC X.                                          00001480
           02 MDATJOUV  PIC X.                                          00001490
           02 MDATJOUO  PIC X(10).                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MTIMJOUA  PIC X.                                          00001520
           02 MTIMJOUC  PIC X.                                          00001530
           02 MTIMJOUP  PIC X.                                          00001540
           02 MTIMJOUH  PIC X.                                          00001550
           02 MTIMJOUV  PIC X.                                          00001560
           02 MTIMJOUO  PIC X(5).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MPAGEA    PIC X.                                          00001590
           02 MPAGEC    PIC X.                                          00001600
           02 MPAGEP    PIC X.                                          00001610
           02 MPAGEH    PIC X.                                          00001620
           02 MPAGEV    PIC X.                                          00001630
           02 MPAGEO    PIC X(3).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MPAGEYA   PIC X.                                          00001660
           02 MPAGEYC   PIC X.                                          00001670
           02 MPAGEYP   PIC X.                                          00001680
           02 MPAGEYH   PIC X.                                          00001690
           02 MPAGEYV   PIC X.                                          00001700
           02 MPAGEYO   PIC X(3).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MFONCA    PIC X.                                          00001730
           02 MFONCC    PIC X.                                          00001740
           02 MFONCP    PIC X.                                          00001750
           02 MFONCH    PIC X.                                          00001760
           02 MFONCV    PIC X.                                          00001770
           02 MFONCO    PIC X(3).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCPROAFFA      PIC X.                                     00001800
           02 MCPROAFFC PIC X.                                          00001810
           02 MCPROAFFP PIC X.                                          00001820
           02 MCPROAFFH PIC X.                                          00001830
           02 MCPROAFFV PIC X.                                          00001840
           02 MCPROAFFO      PIC X(5).                                  00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MLPROAFFA      PIC X.                                     00001870
           02 MLPROAFFC PIC X.                                          00001880
           02 MLPROAFFP PIC X.                                          00001890
           02 MLPROAFFH PIC X.                                          00001900
           02 MLPROAFFV PIC X.                                          00001910
           02 MLPROAFFO      PIC X(20).                                 00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSOC1DFA  PIC X.                                          00001940
           02 MSOC1DFC  PIC X.                                          00001950
           02 MSOC1DFP  PIC X.                                          00001960
           02 MSOC1DFH  PIC X.                                          00001970
           02 MSOC1DFV  PIC X.                                          00001980
           02 MSOC1DFO  PIC X(3).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MENT1DFA  PIC X.                                          00002010
           02 MENT1DFC  PIC X.                                          00002020
           02 MENT1DFP  PIC X.                                          00002030
           02 MENT1DFH  PIC X.                                          00002040
           02 MENT1DFV  PIC X.                                          00002050
           02 MENT1DFO  PIC X(3).                                       00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MSOC2DFA  PIC X.                                          00002080
           02 MSOC2DFC  PIC X.                                          00002090
           02 MSOC2DFP  PIC X.                                          00002100
           02 MSOC2DFH  PIC X.                                          00002110
           02 MSOC2DFV  PIC X.                                          00002120
           02 MSOC2DFO  PIC X(3).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MENT2DFA  PIC X.                                          00002150
           02 MENT2DFC  PIC X.                                          00002160
           02 MENT2DFP  PIC X.                                          00002170
           02 MENT2DFH  PIC X.                                          00002180
           02 MENT2DFV  PIC X.                                          00002190
           02 MENT2DFO  PIC X(3).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MSOC3DFA  PIC X.                                          00002220
           02 MSOC3DFC  PIC X.                                          00002230
           02 MSOC3DFP  PIC X.                                          00002240
           02 MSOC3DFH  PIC X.                                          00002250
           02 MSOC3DFV  PIC X.                                          00002260
           02 MSOC3DFO  PIC X(3).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MENT3DFA  PIC X.                                          00002290
           02 MENT3DFC  PIC X.                                          00002300
           02 MENT3DFP  PIC X.                                          00002310
           02 MENT3DFH  PIC X.                                          00002320
           02 MENT3DFV  PIC X.                                          00002330
           02 MENT3DFO  PIC X(3).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MSOC4DFA  PIC X.                                          00002360
           02 MSOC4DFC  PIC X.                                          00002370
           02 MSOC4DFP  PIC X.                                          00002380
           02 MSOC4DFH  PIC X.                                          00002390
           02 MSOC4DFV  PIC X.                                          00002400
           02 MSOC4DFO  PIC X(3).                                       00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MENT4DFA  PIC X.                                          00002430
           02 MENT4DFC  PIC X.                                          00002440
           02 MENT4DFP  PIC X.                                          00002450
           02 MENT4DFH  PIC X.                                          00002460
           02 MENT4DFV  PIC X.                                          00002470
           02 MENT4DFO  PIC X(3).                                       00002480
           02 MTABLEO OCCURS   15 TIMES .                               00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MCDACA  PIC X.                                          00002510
             03 MCDACC  PIC X.                                          00002520
             03 MCDACP  PIC X.                                          00002530
             03 MCDACH  PIC X.                                          00002540
             03 MCDACV  PIC X.                                          00002550
             03 MCDACO  PIC X.                                          00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MCARTA  PIC X.                                          00002580
             03 MCARTC  PIC X.                                          00002590
             03 MCARTP  PIC X.                                          00002600
             03 MCARTH  PIC X.                                          00002610
             03 MCARTV  PIC X.                                          00002620
             03 MCARTO  PIC X(7).                                       00002630
             03 FILLER       PIC X(2).                                  00002640
             03 MLARTA  PIC X.                                          00002650
             03 MLARTC  PIC X.                                          00002660
             03 MLARTP  PIC X.                                          00002670
             03 MLARTH  PIC X.                                          00002680
             03 MLARTV  PIC X.                                          00002690
             03 MLARTO  PIC X(20).                                      00002700
             03 FILLER       PIC X(2).                                  00002710
             03 MCMARA  PIC X.                                          00002720
             03 MCMARC  PIC X.                                          00002730
             03 MCMARP  PIC X.                                          00002740
             03 MCMARH  PIC X.                                          00002750
             03 MCMARV  PIC X.                                          00002760
             03 MCMARO  PIC X(5).                                       00002770
             03 FILLER       PIC X(2).                                  00002780
             03 MSOC1A  PIC X.                                          00002790
             03 MSOC1C  PIC X.                                          00002800
             03 MSOC1P  PIC X.                                          00002810
             03 MSOC1H  PIC X.                                          00002820
             03 MSOC1V  PIC X.                                          00002830
             03 MSOC1O  PIC X(3).                                       00002840
             03 FILLER       PIC X(2).                                  00002850
             03 MENT1A  PIC X.                                          00002860
             03 MENT1C  PIC X.                                          00002870
             03 MENT1P  PIC X.                                          00002880
             03 MENT1H  PIC X.                                          00002890
             03 MENT1V  PIC X.                                          00002900
             03 MENT1O  PIC X(3).                                       00002910
             03 FILLER       PIC X(2).                                  00002920
             03 MSOC2A  PIC X.                                          00002930
             03 MSOC2C  PIC X.                                          00002940
             03 MSOC2P  PIC X.                                          00002950
             03 MSOC2H  PIC X.                                          00002960
             03 MSOC2V  PIC X.                                          00002970
             03 MSOC2O  PIC X(3).                                       00002980
             03 FILLER       PIC X(2).                                  00002990
             03 MENT2A  PIC X.                                          00003000
             03 MENT2C  PIC X.                                          00003010
             03 MENT2P  PIC X.                                          00003020
             03 MENT2H  PIC X.                                          00003030
             03 MENT2V  PIC X.                                          00003040
             03 MENT2O  PIC X(3).                                       00003050
             03 FILLER       PIC X(2).                                  00003060
             03 MSOC3A  PIC X.                                          00003070
             03 MSOC3C  PIC X.                                          00003080
             03 MSOC3P  PIC X.                                          00003090
             03 MSOC3H  PIC X.                                          00003100
             03 MSOC3V  PIC X.                                          00003110
             03 MSOC3O  PIC X(3).                                       00003120
             03 FILLER       PIC X(2).                                  00003130
             03 MENT3A  PIC X.                                          00003140
             03 MENT3C  PIC X.                                          00003150
             03 MENT3P  PIC X.                                          00003160
             03 MENT3H  PIC X.                                          00003170
             03 MENT3V  PIC X.                                          00003180
             03 MENT3O  PIC X(3).                                       00003190
             03 FILLER       PIC X(2).                                  00003200
             03 MSOC4A  PIC X.                                          00003210
             03 MSOC4C  PIC X.                                          00003220
             03 MSOC4P  PIC X.                                          00003230
             03 MSOC4H  PIC X.                                          00003240
             03 MSOC4V  PIC X.                                          00003250
             03 MSOC4O  PIC X(3).                                       00003260
             03 FILLER       PIC X(2).                                  00003270
             03 MENT4A  PIC X.                                          00003280
             03 MENT4C  PIC X.                                          00003290
             03 MENT4P  PIC X.                                          00003300
             03 MENT4H  PIC X.                                          00003310
             03 MENT4V  PIC X.                                          00003320
             03 MENT4O  PIC X(3).                                       00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MZONCMDA  PIC X.                                          00003350
           02 MZONCMDC  PIC X.                                          00003360
           02 MZONCMDP  PIC X.                                          00003370
           02 MZONCMDH  PIC X.                                          00003380
           02 MZONCMDV  PIC X.                                          00003390
           02 MZONCMDO  PIC X(15).                                      00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MLIBERRA  PIC X.                                          00003420
           02 MLIBERRC  PIC X.                                          00003430
           02 MLIBERRP  PIC X.                                          00003440
           02 MLIBERRH  PIC X.                                          00003450
           02 MLIBERRV  PIC X.                                          00003460
           02 MLIBERRO  PIC X(58).                                      00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MCODTRAA  PIC X.                                          00003490
           02 MCODTRAC  PIC X.                                          00003500
           02 MCODTRAP  PIC X.                                          00003510
           02 MCODTRAH  PIC X.                                          00003520
           02 MCODTRAV  PIC X.                                          00003530
           02 MCODTRAO  PIC X(4).                                       00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MCICSA    PIC X.                                          00003560
           02 MCICSC    PIC X.                                          00003570
           02 MCICSP    PIC X.                                          00003580
           02 MCICSH    PIC X.                                          00003590
           02 MCICSV    PIC X.                                          00003600
           02 MCICSO    PIC X(5).                                       00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MNETNAMA  PIC X.                                          00003630
           02 MNETNAMC  PIC X.                                          00003640
           02 MNETNAMP  PIC X.                                          00003650
           02 MNETNAMH  PIC X.                                          00003660
           02 MNETNAMV  PIC X.                                          00003670
           02 MNETNAMO  PIC X(8).                                       00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MSCREENA  PIC X.                                          00003700
           02 MSCREENC  PIC X.                                          00003710
           02 MSCREENP  PIC X.                                          00003720
           02 MSCREENH  PIC X.                                          00003730
           02 MSCREENV  PIC X.                                          00003740
           02 MSCREENO  PIC X(4).                                       00003750
                                                                                
