      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *                   TS SPECIFIQUE  TPF21                       *          
      *   LONG : 880                                                 *          
      ****************************************************************          
       01 TS-PF21-DATA.                                                         
          05 TS-PF21-LIGNE   OCCURS  16.                                        
             10 TS-PF21-NSOC       PIC X(3).                                    
             10 TS-PF21-PTF        PIC X(3).                                    
             10 TS-PF21-NMUT       PIC X(7).                                    
             10 TS-PF21-SELART     PIC X(5).                                    
             10 TS-PF21-DDESTOCK   PIC X(8).                                    
             10 TS-PF21-DCHARGT    PIC X(8).                                    
             10 TS-PF21-HCHARGT    PIC X(5).                                    
             10 TS-PF21-DMUT       PIC X(8).                                    
             10 TS-PF21-QVOLUME    PIC S9(3)V9(6)  COMP-3.                      
             10 TS-PF21-QTE        PIC S9(5)       COMP-3.                      
                                                                                
