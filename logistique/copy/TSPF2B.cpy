      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *                   TS SPECIFIQUE  TPF22                       *          
      *   LONG : 33                                                  *          
      ****************************************************************          
       01 TS-PF2B-DATA.                                                         
          05 TS-PF2B-TRI        PIC X(01).                                      
          05 TS-PF2B-NVENTE     PIC X(07).                                      
          05 TS-PF2B-NLIEU      PIC X(03).                                      
          05 TS-PF2B-NCODIC     PIC X(07).                                      
          05 TS-PF2B-NCODICGRP  PIC X(07).                                      
          05 TS-PF2B-QVENDUE    PIC S9(5)      COMP-3.                          
          05 TS-PF2B-QVOLUME    PIC S9(3)V9(6) COMP-3.                          
                                                                                
