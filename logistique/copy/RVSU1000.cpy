      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVSU1000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSU1000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSU1000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSU1000.                                                            
      *}                                                                        
           02  SU10-NSOC                                                        
               PIC X(0003).                                                     
           02  SU10-NCDE                                                        
               PIC X(0007).                                                     
           02  SU10-NLIGCDE                                                     
               PIC X(0004).                                                     
           02  SU10-NREC                                                        
               PIC X(0007).                                                     
           02  SU10-DREC                                                        
               PIC X(0008).                                                     
           02  SU10-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  SU10-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  SU10-NSOCDEST                                                    
               PIC X(0003).                                                     
           02  SU10-NLIEUDEST                                                   
               PIC X(0003).                                                     
           02  SU10-NCODIC                                                      
               PIC X(0007).                                                     
           02  SU10-TYPCDE                                                      
               PIC X(0001).                                                     
           02  SU10-QTEDEM                                                      
               PIC S9(5) COMP-3.                                                
           02  SU10-QTEASERV                                                    
               PIC S9(5) COMP-3.                                                
           02  SU10-DTOPE                                                       
               PIC X(0008).                                                     
           02  SU10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVSU1000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSU1000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSU1000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-NLIGCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-NLIGCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-DREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-DREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-NSOCDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-NSOCDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-NLIEUDEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-NLIEUDEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-TYPCDE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-TYPCDE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-QTEDEM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-QTEDEM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-QTEASERV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-QTEASERV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-DTOPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SU10-DTOPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SU10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  SU10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
