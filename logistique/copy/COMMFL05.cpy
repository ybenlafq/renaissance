      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TFL05                    TR: FL05  *    00020000
      *            DEFINITION DES DONNEES FAMILLES/DEPOT           *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00050000
      *                                                                 00060000
          02 COMM-FL05-APPLI REDEFINES COMM-GA00-APPLI.                 00070005
      *------------------------------ CODE FAMILLE                      00080000
             03 COMM-FL05-CFAM           PIC X(5).                      00090000
      *------------------------------ LIBELLE FAMILLE                   00100002
             03 COMM-FL05-LFAM           PIC X(20).                     00110004
      *------------------------------ CODE SOCIETE                      00121002
             03 COMM-FL05-NSOC           PIC X(3).                      00122002
      *------------------------------ CODE DEPOT                        00123002
             03 COMM-FL05-NDEPOT         PIC X(3).                      00130000
      *------------------------------ PAGE                              00131001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FL05-PAGE           PIC S9(04) COMP.               00132001
      *--                                                                       
             03 COMM-FL05-PAGE           PIC S9(04) COMP-5.                     
      *}                                                                        
      *------------------------------ PAGE MAXI                         00133001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FL05-PAGE-MAX       PIC S9(04) COMP.               00134001
      *--                                                                       
             03 COMM-FL05-PAGE-MAX       PIC S9(04) COMP-5.                     
      *}                                                                        
      *------------------------------ FLAG DE MAJ                       00135001
             03 COMM-FL05-MAJ            PIC X.                         00136001
      *------------------------------ NB ITEM MAX DANS TS               00137001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FL05-ITEMAX         PIC S9(04) COMP.               00138001
      *--                                                                       
             03 COMM-FL05-ITEMAX         PIC S9(04) COMP-5.                     
      *}                                                                        
      *------------------------------ DEPOT GESTION PALETTE                     
      *      03 COMM-FL05-DEPOTPAL       PIC X(1).                              
      ***************************************************************** 00140000
                                                                                
