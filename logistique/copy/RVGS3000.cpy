      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGS3000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS3000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS3000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS3000.                                                            
      *}                                                                        
           02  GS30-NSOCMAG                                                     
               PIC X(0003).                                                     
           02  GS30-NMAG                                                        
               PIC X(0003).                                                     
           02  GS30-NCODIC                                                      
               PIC X(0007).                                                     
           02  GS30-WSTOCKMASQ                                                  
               PIC X(0001).                                                     
           02  GS30-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  GS30-QSTOCK                                                      
               PIC S9(5) COMP-3.                                                
           02  GS30-QSTOCKRES                                                   
               PIC S9(5) COMP-3.                                                
           02  GS30-DMAJSTOCK                                                   
               PIC X(0008).                                                     
           02  GS30-WARTINC                                                     
               PIC X(0001).                                                     
           02  GS30-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGS3000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS3000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS3000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS30-NSOCMAG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS30-NSOCMAG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS30-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS30-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS30-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS30-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS30-WSTOCKMASQ-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS30-WSTOCKMASQ-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS30-NSSLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS30-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS30-QSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS30-QSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS30-QSTOCKRES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS30-QSTOCKRES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS30-DMAJSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS30-DMAJSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS30-WARTINC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS30-WARTINC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GS30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
