      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVTL0200                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVTL0200                         
      **********************************************************                
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVTL0200.                                                            
           02  TL02-DDELIV                                                      
               PIC X(0008).                                                     
           02  TL02-NSOC                                                        
               PIC X(0003).                                                     
           02  TL02-CPROTOUR                                                    
               PIC X(0005).                                                     
           02  TL02-CTOURNEE                                                    
               PIC X(0003).                                                     
           02  TL02-NORDRE                                                      
               PIC X(0002).                                                     
           02  TL02-NMAG                                                        
               PIC X(0003).                                                     
           02  TL02-NVENTE                                                      
               PIC X(0007).                                                     
           02  TL02-CADRTOUR                                                    
               PIC X(0001).                                                     
           02  TL02-CTYPE                                                       
               PIC X(0001).                                                     
           02  TL02-CTYPEASS                                                    
               PIC X(0002).                                                     
           02  TL02-WVAL                                                        
               PIC X(0001).                                                     
           02  TL02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVTL0200                                  
      **********************************************************                
       01  RVTL0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL02-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL02-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-CPROTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL02-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-CTOURNEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL02-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL02-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL02-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL02-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-CADRTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL02-CADRTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-CTYPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL02-CTYPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-CTYPEASS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL02-CTYPEASS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-WVAL-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL02-WVAL-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  TL02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
EMOD                                                                            
      *}                                                                        
