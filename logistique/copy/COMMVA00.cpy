      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * LONGUEUR 120                                                            
       01  COMM-VA00-APPLI.                                                     
           05  MVA000-ENTREE.                                                   
              10  COMM-VA00-NSOC           PIC  X(3).                           
              10  COMM-VA00-NLIEU          PIC  X(3).                           
              10  COMM-VA00-NSSLIEU        PIC  X(3).                           
              10  COMM-VA00-NLIEUTRT       PIC  X(5).                           
           05  MVA000-SORTIE.                                                   
              10  COMM-VA00-NSOCVALO       PIC  X(3).                           
              10  COMM-VA00-NLIEUVALO      PIC  X(3).                           
              10  COMM-VA00-LLIEUVALO      PIC  X(20).                          
              10  COMM-VA00-TYPEVALO       PIC  X(1).                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-VA00-CODRET            PIC  S9(4) COMP.                     
      *--                                                                       
           05  COMM-VA00-CODRET            PIC  S9(4) COMP-5.                   
      *}                                                                        
           05  COMM-VA00-MESS              PIC  X(50).                          
           05  COMM-VA00-NSOCCOMPT         PIC  X(3).                           
           05  COMM-VA00-FILLER            PIC  X(24).                          
                                                                                
