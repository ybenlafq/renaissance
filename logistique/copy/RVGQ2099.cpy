      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVGQ2099                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGQ2099.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGQ2099.                                                            
      *}                                                                        
      *                       CINSEE                                            
           10 GQ20-CINSEE          PIC X(5).                                    
      *                       NSOC                                              
           10 GQ20-NSOC            PIC X(3).                                    
      *                       CCADRE                                            
           10 GQ20-CCADRE          PIC X(5).                                    
      *                       WCONTRA                                           
           10 GQ20-WCONTRA         PIC X(1).                                    
      *                       CELEMEN                                           
           10 GQ20-CELEMEN         PIC X(5).                                    
      *                       DSYST                                             
           10 GQ20-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
                                                                                
