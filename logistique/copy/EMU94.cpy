      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU94   EMU94                                              00000020
      ***************************************************************** 00000030
       01   EMU94I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCQLL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCQLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCQLF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCQLI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUQL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUQL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEUQF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUQI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUQL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLLIEUQL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEUQF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUQI  PIC X(13).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDAMUTQL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDAMUTQL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDAMUTQF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDAMUTQI  PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELARQL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSELARQL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSELARQF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSELARQI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIGTQL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNLIGTQL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIGTQF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNLIGTQI  PIC X(6).                                       00000410
           02 DEFLITRI OCCURS   7 TIMES .                               00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIGTRL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNLIGTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIGTRF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNLIGTRI     PIC X(6).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCLTL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNSOCLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNSOCLTF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNSOCLTI     PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUTL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNLIEUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIEUTF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNLIEUTI     PIC X(3).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIEUTL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLLIEUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLLIEUTF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLLIEUTI     PIC X(15).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDAMUTRL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDAMUTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDAMUTRF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDAMUTRI     PIC X(8).                                  00000620
             03 DSELARTI .                                              00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELAR1L   COMP PIC S9(4).                            00000640
      *--                                                                       
               04 MSELAR1L COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSELAR1F   PIC X.                                     00000650
               04 FILLER     PIC X(4).                                  00000660
               04 MSELAR1I   PIC X(5).                                  00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELAR2L     COMP PIC S9(4).                            00000680
      *--                                                                       
             03 MSELAR2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELAR2F     PIC X.                                     00000690
             03 FILLER  PIC X(4).                                       00000700
             03 MSELAR2I     PIC X(5).                                  00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELAR3L     COMP PIC S9(4).                            00000720
      *--                                                                       
             03 MSELAR3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELAR3F     PIC X.                                     00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MSELAR3I     PIC X(5).                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELAR4L     COMP PIC S9(4).                            00000760
      *--                                                                       
             03 MSELAR4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELAR4F     PIC X.                                     00000770
             03 FILLER  PIC X(4).                                       00000780
             03 MSELAR4I     PIC X(5).                                  00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELAR5L     COMP PIC S9(4).                            00000800
      *--                                                                       
             03 MSELAR5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELAR5F     PIC X.                                     00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MSELAR5I     PIC X(5).                                  00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELAR6L     COMP PIC S9(4).                            00000840
      *--                                                                       
             03 MSELAR6L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELAR6F     PIC X.                                     00000850
             03 FILLER  PIC X(4).                                       00000860
             03 MSELAR6I     PIC X(5).                                  00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MLIBERRI  PIC X(78).                                      00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCODTRAI  PIC X(4).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MCICSI    PIC X(5).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MNETNAMI  PIC X(8).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MSCREENI  PIC X(4).                                       00001070
      ***************************************************************** 00001080
      * SDF: EMU94   EMU94                                              00001090
      ***************************************************************** 00001100
       01   EMU94O REDEFINES EMU94I.                                    00001110
           02 FILLER    PIC X(12).                                      00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MDATJOUA  PIC X.                                          00001140
           02 MDATJOUC  PIC X.                                          00001150
           02 MDATJOUP  PIC X.                                          00001160
           02 MDATJOUH  PIC X.                                          00001170
           02 MDATJOUV  PIC X.                                          00001180
           02 MDATJOUO  PIC X(10).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MTIMJOUA  PIC X.                                          00001210
           02 MTIMJOUC  PIC X.                                          00001220
           02 MTIMJOUP  PIC X.                                          00001230
           02 MTIMJOUH  PIC X.                                          00001240
           02 MTIMJOUV  PIC X.                                          00001250
           02 MTIMJOUO  PIC X(5).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MPAGEA    PIC X.                                          00001280
           02 MPAGEC    PIC X.                                          00001290
           02 MPAGEP    PIC X.                                          00001300
           02 MPAGEH    PIC X.                                          00001310
           02 MPAGEV    PIC X.                                          00001320
           02 MPAGEO    PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNSOCQLA  PIC X.                                          00001350
           02 MNSOCQLC  PIC X.                                          00001360
           02 MNSOCQLP  PIC X.                                          00001370
           02 MNSOCQLH  PIC X.                                          00001380
           02 MNSOCQLV  PIC X.                                          00001390
           02 MNSOCQLO  PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNLIEUQA  PIC X.                                          00001420
           02 MNLIEUQC  PIC X.                                          00001430
           02 MNLIEUQP  PIC X.                                          00001440
           02 MNLIEUQH  PIC X.                                          00001450
           02 MNLIEUQV  PIC X.                                          00001460
           02 MNLIEUQO  PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLLIEUQA  PIC X.                                          00001490
           02 MLLIEUQC  PIC X.                                          00001500
           02 MLLIEUQP  PIC X.                                          00001510
           02 MLLIEUQH  PIC X.                                          00001520
           02 MLLIEUQV  PIC X.                                          00001530
           02 MLLIEUQO  PIC X(13).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MDAMUTQA  PIC X.                                          00001560
           02 MDAMUTQC  PIC X.                                          00001570
           02 MDAMUTQP  PIC X.                                          00001580
           02 MDAMUTQH  PIC X.                                          00001590
           02 MDAMUTQV  PIC X.                                          00001600
           02 MDAMUTQO  PIC X(8).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MSELARQA  PIC X.                                          00001630
           02 MSELARQC  PIC X.                                          00001640
           02 MSELARQP  PIC X.                                          00001650
           02 MSELARQH  PIC X.                                          00001660
           02 MSELARQV  PIC X.                                          00001670
           02 MSELARQO  PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNLIGTQA  PIC X.                                          00001700
           02 MNLIGTQC  PIC X.                                          00001710
           02 MNLIGTQP  PIC X.                                          00001720
           02 MNLIGTQH  PIC X.                                          00001730
           02 MNLIGTQV  PIC X.                                          00001740
           02 MNLIGTQO  PIC X(6).                                       00001750
           02 DEFLITRO OCCURS   7 TIMES .                               00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MNLIGTRA     PIC X.                                     00001780
             03 MNLIGTRC     PIC X.                                     00001790
             03 MNLIGTRP     PIC X.                                     00001800
             03 MNLIGTRH     PIC X.                                     00001810
             03 MNLIGTRV     PIC X.                                     00001820
             03 MNLIGTRO     PIC X(6).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MNSOCLTA     PIC X.                                     00001850
             03 MNSOCLTC     PIC X.                                     00001860
             03 MNSOCLTP     PIC X.                                     00001870
             03 MNSOCLTH     PIC X.                                     00001880
             03 MNSOCLTV     PIC X.                                     00001890
             03 MNSOCLTO     PIC X(3).                                  00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MNLIEUTA     PIC X.                                     00001920
             03 MNLIEUTC     PIC X.                                     00001930
             03 MNLIEUTP     PIC X.                                     00001940
             03 MNLIEUTH     PIC X.                                     00001950
             03 MNLIEUTV     PIC X.                                     00001960
             03 MNLIEUTO     PIC X(3).                                  00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MLLIEUTA     PIC X.                                     00001990
             03 MLLIEUTC     PIC X.                                     00002000
             03 MLLIEUTP     PIC X.                                     00002010
             03 MLLIEUTH     PIC X.                                     00002020
             03 MLLIEUTV     PIC X.                                     00002030
             03 MLLIEUTO     PIC X(15).                                 00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MDAMUTRA     PIC X.                                     00002060
             03 MDAMUTRC     PIC X.                                     00002070
             03 MDAMUTRP     PIC X.                                     00002080
             03 MDAMUTRH     PIC X.                                     00002090
             03 MDAMUTRV     PIC X.                                     00002100
             03 MDAMUTRO     PIC X(8).                                  00002110
             03 DSELARTO .                                              00002120
               04 FILLER     PIC X(2).                                  00002130
               04 MSELAR1A   PIC X.                                     00002140
               04 MSELAR1C   PIC X.                                     00002150
               04 MSELAR1P   PIC X.                                     00002160
               04 MSELAR1H   PIC X.                                     00002170
               04 MSELAR1V   PIC X.                                     00002180
               04 MSELAR1O   PIC X(5).                                  00002190
             03 FILLER       PIC X(2).                                  00002200
             03 MSELAR2A     PIC X.                                     00002210
             03 MSELAR2C     PIC X.                                     00002220
             03 MSELAR2P     PIC X.                                     00002230
             03 MSELAR2H     PIC X.                                     00002240
             03 MSELAR2V     PIC X.                                     00002250
             03 MSELAR2O     PIC X(5).                                  00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MSELAR3A     PIC X.                                     00002280
             03 MSELAR3C     PIC X.                                     00002290
             03 MSELAR3P     PIC X.                                     00002300
             03 MSELAR3H     PIC X.                                     00002310
             03 MSELAR3V     PIC X.                                     00002320
             03 MSELAR3O     PIC X(5).                                  00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MSELAR4A     PIC X.                                     00002350
             03 MSELAR4C     PIC X.                                     00002360
             03 MSELAR4P     PIC X.                                     00002370
             03 MSELAR4H     PIC X.                                     00002380
             03 MSELAR4V     PIC X.                                     00002390
             03 MSELAR4O     PIC X(5).                                  00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MSELAR5A     PIC X.                                     00002420
             03 MSELAR5C     PIC X.                                     00002430
             03 MSELAR5P     PIC X.                                     00002440
             03 MSELAR5H     PIC X.                                     00002450
             03 MSELAR5V     PIC X.                                     00002460
             03 MSELAR5O     PIC X(5).                                  00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MSELAR6A     PIC X.                                     00002490
             03 MSELAR6C     PIC X.                                     00002500
             03 MSELAR6P     PIC X.                                     00002510
             03 MSELAR6H     PIC X.                                     00002520
             03 MSELAR6V     PIC X.                                     00002530
             03 MSELAR6O     PIC X(5).                                  00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MLIBERRA  PIC X.                                          00002560
           02 MLIBERRC  PIC X.                                          00002570
           02 MLIBERRP  PIC X.                                          00002580
           02 MLIBERRH  PIC X.                                          00002590
           02 MLIBERRV  PIC X.                                          00002600
           02 MLIBERRO  PIC X(78).                                      00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MCODTRAA  PIC X.                                          00002630
           02 MCODTRAC  PIC X.                                          00002640
           02 MCODTRAP  PIC X.                                          00002650
           02 MCODTRAH  PIC X.                                          00002660
           02 MCODTRAV  PIC X.                                          00002670
           02 MCODTRAO  PIC X(4).                                       00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MCICSA    PIC X.                                          00002700
           02 MCICSC    PIC X.                                          00002710
           02 MCICSP    PIC X.                                          00002720
           02 MCICSH    PIC X.                                          00002730
           02 MCICSV    PIC X.                                          00002740
           02 MCICSO    PIC X(5).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MNETNAMA  PIC X.                                          00002770
           02 MNETNAMC  PIC X.                                          00002780
           02 MNETNAMP  PIC X.                                          00002790
           02 MNETNAMH  PIC X.                                          00002800
           02 MNETNAMV  PIC X.                                          00002810
           02 MNETNAMO  PIC X(8).                                       00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MSCREENA  PIC X.                                          00002840
           02 MSCREENC  PIC X.                                          00002850
           02 MSCREENP  PIC X.                                          00002860
           02 MSCREENH  PIC X.                                          00002870
           02 MSCREENV  PIC X.                                          00002880
           02 MSCREENO  PIC X(4).                                       00002890
                                                                                
