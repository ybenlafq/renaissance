      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE POUR LE MODULE MFL16                           *  00020001
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-FL16-LONG         PIC S9(2) COMP-3 VALUE 31.              00080001
       01  TS-FL16-DATA.                                                00090000
           05 TS-FL16-NCODIC      PIC X(07).                            00120001
           05 TS-FL16-QLIVR       PIC S9(7) COMP-3.                             
           05 TS-FL16-PRA         PIC S9(7)V99 COMP-3.                  00122001
           05 TS-FL16-SRP         PIC S9(7)V9(6) COMP-3.                00122001
           05 TS-FL16-DMUTATION   PIC X(08).                                    
                                                                                
