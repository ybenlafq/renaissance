      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRM055 AU 25/09/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,06,BI,A,                          *        
      *                           21,07,BI,A,                          *        
      *                           28,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRM055.                                                        
            05 NOMETAT-IRM055           PIC X(6) VALUE 'IRM055'.                
            05 RUPTURES-IRM055.                                                 
           10 IRM055-NFILIALE           PIC X(03).                      007  003
           10 IRM055-CSELART            PIC X(05).                      010  005
           10 IRM055-NLIEU              PIC X(06).                      015  006
           10 IRM055-NMUTATION          PIC X(07).                      021  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRM055-SEQUENCE           PIC S9(04) COMP.                028  002
      *--                                                                       
           10 IRM055-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRM055.                                                   
           10 IRM055-INDLOI             PIC X(02).                      030  002
           10 IRM055-RANG               PIC X(05).                      032  005
           10 IRM055-CNBCOLIS           PIC S9(04)      COMP-3.         037  003
           10 IRM055-CNMUT              PIC S9(01)      COMP-3.         040  001
           10 IRM055-CQUOTAKOMAX        PIC S9(01)      COMP-3.         041  001
           10 IRM055-CQUOTAKOOBJ        PIC S9(01)      COMP-3.         042  001
           10 IRM055-CQUOTAOK           PIC S9(01)      COMP-3.         043  001
           10 IRM055-DISTDEP            PIC S9(05)      COMP-3.         044  003
           10 IRM055-DISTMAX            PIC S9(05)      COMP-3.         047  003
           10 IRM055-DISTMIN            PIC S9(05)      COMP-3.         050  003
           10 IRM055-DISTOBJ            PIC S9(05)      COMP-3.         053  003
           10 IRM055-PDSETI             PIC S9(02)V9(2) COMP-3.         056  003
           10 IRM055-QTEMAX             PIC S9(05)      COMP-3.         059  003
           10 IRM055-QTEMUT             PIC S9(05)      COMP-3.         062  003
           10 IRM055-QTENON             PIC S9(05)      COMP-3.         065  003
           10 IRM055-QTEOBJ             PIC S9(05)      COMP-3.         068  003
           10 IRM055-QUOTAP             PIC S9(05)      COMP-3.         071  003
           10 IRM055-QUOTAV             PIC S9(05)V9(2) COMP-3.         074  004
           10 IRM055-VOLMAX             PIC S9(05)V9(2) COMP-3.         078  004
           10 IRM055-VOLMUT             PIC S9(05)V9(2) COMP-3.         082  004
           10 IRM055-VOLOBJ             PIC S9(05)V9(2) COMP-3.         086  004
            05 FILLER                      PIC X(423).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRM055-LONG           PIC S9(4)   COMP  VALUE +089.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRM055-LONG           PIC S9(4) COMP-5  VALUE +089.           
                                                                                
      *}                                                                        
