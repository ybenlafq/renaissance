      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    TS PERMETTANT LE PASSAGE DES DIFFERENCES ENTRE LES VENTES ET 00000100
      *    LES TOURNEES ENTRE LE PROGRAMME TTL10 ET TTL11               00000101
      *                                                                 00000102
       01   TS-TL11.                                                    00000110
      *                                                                 00000111
           02 TS-TL11-LONG                PIC S9(3) COMP-3 VALUE 20.    00000200
      *                                        DONNEES                  00000210
           02 TS-TL11-DONNEES.                                          00000300
      *                                        MAGASIN                  00000310
              03 TS-TL11-NMAG             PIC X(3).                     00000400
      *                                        VENTE                    00000410
              03 TS-TL11-NVENTE           PIC X(7).                     00000500
      *                                        TOP DIFFERENCE :         00000501
      *                                            'S' SUPPRESSION      00000502
      *                                            'A' AJOUT            00000503
              03 TS-TL11-WMAJ             PIC X.                        00000504
      *                                        NUMERO DE TOURNEE        00000505
              03 TS-TL11-CTOURNEE         PIC X(3).                     00000510
      *                                        SATELLITE                00000520
              03 TS-TL11-NSAT             PIC X(2).                     00000600
      *                                        CASE                     00000601
              03 TS-TL11-NCASE            PIC X(3).                     00000610
      *                                        CODE ADRESSE TOURNEE     00000620
              03 TS-TL11-CADRTOUR         PIC X(1).                     00000630
                                                                                
