      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGL0001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGL0001                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGL0001.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGL0001.                                                            
      *}                                                                        
           02  GL00-NLIVRAISON                                                  
               PIC X(0007).                                                     
           02  GL00-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GL00-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GL00-CREC                                                        
               PIC X(0005).                                                     
           02  GL00-DLIVRAISON                                                  
               PIC X(0008).                                                     
           02  GL00-CLIVR                                                       
               PIC X(0005).                                                     
           02  GL00-CMODETRANS                                                  
               PIC X(0005).                                                     
           02  GL00-DHLIVR                                                      
               PIC X(0002).                                                     
           02  GL00-DMLIVR                                                      
               PIC X(0002).                                                     
           02  GL00-LLIVRAISON                                                  
               PIC X(0020).                                                     
           02  GL00-WPALETTIS                                                   
               PIC X(0001).                                                     
           02  GL00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GL00-FPLANDATE                                                   
               PIC X(0008).                                                     
           02  GL00-FPLANFLAG                                                   
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGL0001                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGL0001-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGL0001-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-NLIVRAISON-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-NLIVRAISON-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-CREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-CREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-DLIVRAISON-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-DLIVRAISON-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-CLIVR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-CLIVR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-CMODETRANS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-CMODETRANS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-DHLIVR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-DHLIVR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-DMLIVR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-DMLIVR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-LLIVRAISON-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-LLIVRAISON-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-WPALETTIS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-WPALETTIS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-FPLANDATE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-FPLANDATE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GL00-FPLANFLAG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GL00-FPLANFLAG-F                                                 
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
