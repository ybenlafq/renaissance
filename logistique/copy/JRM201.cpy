      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JRM201 AU 10/05/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,20,BI,A,                          *        
      *                           37,03,PD,A,                          *        
      *                           40,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JRM201.                                                        
            05 NOMETAT-JRM201           PIC X(6) VALUE 'JRM201'.                
            05 RUPTURES-JRM201.                                                 
           10 JRM201-CGROUP             PIC X(05).                      007  005
           10 JRM201-CFAM               PIC X(05).                      012  005
           10 JRM201-LAGREGATED         PIC X(20).                      017  020
           10 JRM201-QRANG              PIC S9(05)      COMP-3.         037  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JRM201-SEQUENCE           PIC S9(04) COMP.                040  002
      *--                                                                       
           10 JRM201-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JRM201.                                                   
           10 JRM201-CMARQ              PIC X(05).                      042  005
           10 JRM201-LFAM               PIC X(20).                      047  020
           10 JRM201-LLIEU1             PIC X(20).                      067  020
           10 JRM201-LLIEU2             PIC X(20).                      087  020
           10 JRM201-LLIEU3             PIC X(20).                      107  020
           10 JRM201-LREFFOURN          PIC X(20).                      127  020
           10 JRM201-LSTATCOMP          PIC X(03).                      147  003
           10 JRM201-NCODIC             PIC X(07).                      150  007
           10 JRM201-NDEPOT1            PIC X(03).                      157  003
           10 JRM201-NDEPOT2            PIC X(03).                      160  003
           10 JRM201-NDEPOT3            PIC X(03).                      163  003
           10 JRM201-NSOCIETE1          PIC X(03).                      166  003
           10 JRM201-NSOCIETE2          PIC X(03).                      169  003
           10 JRM201-NSOCIETE3          PIC X(03).                      172  003
           10 JRM201-WE9                PIC X(01).                      175  001
           10 JRM201-WRM80              PIC X(01).                      176  001
           10 JRM201-W8020-RUP          PIC X(01).                      177  001
           10 JRM201-PRIX-VENTE         PIC S9(07)V9(2) COMP-3.         178  005
           10 JRM201-QMUTATT            PIC S9(05)      COMP-3.         183  003
           10 JRM201-QNBMAG             PIC S9(03)      COMP-3.         186  002
           10 JRM201-QPV                PIC S9(07)      COMP-3.         188  004
           10 JRM201-QPV-SIGNE          PIC S9(07)      COMP-3.         192  004
           10 JRM201-QSA                PIC S9(07)      COMP-3.         196  004
           10 JRM201-QSASIMU            PIC S9(07)      COMP-3.         200  004
           10 JRM201-QSO                PIC S9(07)      COMP-3.         204  004
           10 JRM201-QSOSIMU            PIC S9(07)      COMP-3.         208  004
           10 JRM201-QSTOCKDEP          PIC S9(05)      COMP-3.         212  003
           10 JRM201-QSTOCKMAG          PIC S9(05)      COMP-3.         215  003
           10 JRM201-S-1                PIC S9(05)      COMP-3.         218  003
           10 JRM201-S-2                PIC S9(05)      COMP-3.         221  003
           10 JRM201-S-3                PIC S9(05)      COMP-3.         224  003
           10 JRM201-S-4                PIC S9(05)      COMP-3.         227  003
            05 FILLER                      PIC X(283).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JRM201-LONG           PIC S9(4)   COMP  VALUE +229.           
      *                                                                         
      *--                                                                       
        01  DSECT-JRM201-LONG           PIC S9(4) COMP-5  VALUE +229.           
                                                                                
      *}                                                                        
