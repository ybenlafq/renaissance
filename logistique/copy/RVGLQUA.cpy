      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GLQUA GESTION LIENS : QUALIFICATION    *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGLQUA.                                                             
           05  GLQUA-CTABLEG2    PIC X(15).                                     
           05  GLQUA-CTABLEG2-REDEF REDEFINES GLQUA-CTABLEG2.                   
               10  GLQUA-CQUALIF         PIC X(05).                             
           05  GLQUA-WTABLEG     PIC X(80).                                     
           05  GLQUA-WTABLEG-REDEF  REDEFINES GLQUA-WTABLEG.                    
               10  GLQUA-WACTIF          PIC X(01).                             
               10  GLQUA-LIBELLE         PIC X(20).                             
               10  GLQUA-WDATA           PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGLQUA-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GLQUA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GLQUA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GLQUA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GLQUA-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
