      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : GESTION DE PENURIE                              *        
      *  COPY        : FGS256                                          *        
      *  CREATION    : 24/05/2011                                      *        
      *  FONCTION    : DSECT DU FICHIER CSV GESTION DE PENURIE         *        
      *                PAR GROUPE DE REAPPROVISIONNEMENT               *        
      *  UTLISATION  : CETTE DSECT EST UTILISEE PAR LE BGS256          *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                *        
      * LONGUEUR DU FICHIER : 185                                      *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                         
       01  :FGS256:-ENREGISTREMENT.                                             
      *    CODE FAMILLE                                                         
           03 :FGS256:-CFAM          PIC X(05).                                 
           03 FILL01                 PIC X.                                     
      *    CODE MARQUE                                                          
           03 :FGS256:-CMARQ         PIC X(05).                                 
           03 FILL02                 PIC X.                                     
      *    LIBELLE REFERENCE FOURNISSEUR                                        
           03 :FGS256:-LREFFOURN     PIC X(20).                                 
           03 FILL03                 PIC X.                                     
      *    STATUT DU PRODUIT                                                    
           03 :FGS256:-STT           PIC X(03).                                 
           03 FILL04                 PIC X.                                     
      *    CODIC DE L'ARTICLE                                                   
           03 :FGS256:-NCODIC        PIC X(07).                                 
           03 FILL05                 PIC X.                                     
      *    TOP COMMANDE FOURNISSEUR SUR ENTREPOT NORD                           
           03 :FGS256:-CDEN-FOU      PIC X(09).                                 
           03 FILL06                 PIC X.                                     
      *    TOP COMMANDE FOURNISSEUR SUR ENTREPOT SUD                            
           03 :FGS256:-CDES-FOU      PIC X(09).                                 
           03 FILL07                 PIC X.                                     
      *    TABLEAU GROUPES DE REAPPROVISIONNEMENT                               
           03 :FGS256:-T-CGROUP.                                                
              05 :FGS256:-TAB-CGROUP OCCURS 20.                                 
                 10 :FGS256:-CGROUP  PIC X(05).                                 
                 10 FILL08           PIC X.                                     
      *                                                                         
                                                                                
