      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00000100
      * COMMAREA SPECIFIQUE PRG TGD40 (TGD00 -> MENU)    TR: GD40  *    00000200
      *               CIRCUIT DE DESTOCKAGE                        *    00000300
      *                                                                 00000400
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3580  00000500
      *                                                                 00000600
      *        TRANSACTION GD40 : CIRCUIT DE  DESTOCKAGE           *    00000700
      *                                                                 00000710
           03 COMM-GD40-APPLI REDEFINES COMM-GD00-FILLER.               00000720
      *------------------------------ ZONE DONNEES TGD40                00000730
             04 COMM-GD40-DONNEES-TGD40.                                00000740
                05  COMM-GD40-FICHIER            PIC X(6).              00000741
                05  COMM-GD40-PAGE               PIC 9(3).              00000745
                05  COMM-GD40-PAGE-MAX           PIC 9(3).              00000746
                05  COMM-GD40-FIN-FICHIER        PIC X.                 00000753
                05  COMM-GD40-APPEL              PIC X.                 00000754
                05  COMM-GD40-DECALAGE           PIC X.                 00000755
                05  COMM-GD40-MAJ                PIC X.                 00000756
                05  COMM-GD40-INS                PIC X.                 00000757
                05  COMM-GD40-DEL                PIC X.                 00000758
                05  COMM-GD40-PAS                PIC 9(3).              00000759
                05  COMM-GD40-CONSTANTE          PIC 9(3).              00000759
                05  COMM-GD40-LIGNE          OCCURS 14.                 00000760
                    10  COMM-GD40-SEL               PIC X(2).           00000761
                    10  COMM-GD40-CZONED            PIC X(2).           00000762
                    10  COMM-GD40-CNIVD1            PIC X(2).           00000763
                    10  COMM-GD40-CNIVF1            PIC X(2).           00000764
                    10  COMM-GD40-NPOSD1            PIC X(3).           00000765
                    10  COMM-GD40-NPOSF1            PIC X(3).           00000766
                    10  COMM-GD40-CZONEF            PIC X(2).           00000767
                    10  COMM-GD40-CNIVD2            PIC X(2).           00000768
                    10  COMM-GD40-CNIVF2            PIC X(2).           00000769
                    10  COMM-GD40-NPOSD2            PIC X(3).           00000770
                    10  COMM-GD40-NPOSF2            PIC X(3).           00000771
                    10  COMM-GD40-CRUPTURE          PIC X.              00000772
                05  COMM-GD40-WSEQ-VAL       OCCURS 14.                 00000773
                    10  COMM-GD40-WSEQ              PIC S9(5) COMP-3.   00000774
                05  COMM-GD40-VALEUR         OCCURS 500.                00000775
                    10  COMM-GD40-WSEQFIN           PIC S9(5) COMP-3.   00000776
                05  COMM-GD40-WSEQMAX               PIC S9(5) COMP-3.   00000776
                05  COMM-GD40-SWAP-ATTR          PIC X(01) OCCURS 300.  00000780
                05  COMM-GD40-FILLER             PIC X(500).            00000780
                                                                                
