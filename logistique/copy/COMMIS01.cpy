      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      * COMMAREA SPECIFIQUE PRG TIS01 (TIS00 -> MENU)    TR: IS01  *    00030000
      *                                                            *    00031000
      *                       INVENTAIRE                           *    00040000
      *                                                            *    00050000
      * ZONES RESERVEES APPLICATIVES ------------------------------*    00060003
      *                                                            *    00070000
      *        TRANSACTION IS01                                    *    00080000
      *                                                            *    00090000
      *------------------------------ ZONE DONNEES TIS01---2597----*    00110000
      *                                                                 00720200
           02 COMM-IS01-APPLI   REDEFINES COMM-IS00-APPLI.              00721000
              03 COMM-IS01-MODIFIE    PIC X.                            00743010
              03 COMM-IS01-NBP        PIC 999.                                  
              03 COMM-IS01-PAGE       PIC 999.                                  
              03 COMM-IS01-ITEM       PIC 9999.                                 
              03 SAV-MAP OCCURS 14.                                             
                 05 COMM-IS01-SOCDEP     PIC X(3).                         00743
                 05 COMM-IS01-DEPOT      PIC X(3).                         00743
                 05 COMM-IS01-SSLIEU     PIC X.                            00743
                 05 COMM-IS01-CLIEUTRT   PIC X(5).                         00743
                 05 COMM-IS01-ZONE       PIC XX.                           00743
                 05 COMM-IS01-SECTEUR    PIC XX.                           00743
                 05 COMM-IS01-LIB-ZONE   PIC X(20).                        00743
                 05 COMM-IS01-STOCKAVAP  PIC X.                            00743
                 05 COMM-IS01-TYPCPTE    PIC XX.                           00743
                 05 COMM-IS01-QTE        PIC X.                            00743
                 05 COMM-IS01-FICHEDEB   PIC X(5).                         00743
                 05 COMM-IS01-FICHEFIN   PIC X(5).                         00743
              03 COMM-IS01-FILLER     PIC X(1886).                      00743030
                                                                                
                                                                        00750000
