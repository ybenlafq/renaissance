      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVGS4300                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS4300                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS4300.                                                    00000090
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS4300.                                                            
      *}                                                                        
           02  GS43-NSOCORIG                                            00000100
               PIC X(0003).                                             00000110
           02  GS43-NLIEUORIG                                           00000120
               PIC X(0003).                                             00000130
           02  GS43-NSSLIEUORIG                                         00000140
               PIC X(0003).                                             00000150
           02  GS43-NSOCDEST                                            00000160
               PIC X(0003).                                             00000170
           02  GS43-NLIEUDEST                                           00000180
               PIC X(0003).                                             00000190
           02  GS43-NSSLIEUDEST                                         00000200
               PIC X(0003).                                             00000210
           02  GS43-NCODIC                                              00000220
               PIC X(0007).                                             00000230
           02  GS43-NORIGINE                                            00000240
               PIC X(0007).                                             00000250
           02  GS43-CPROG                                               00000260
               PIC X(0005).                                             00000270
           02  GS43-COPER                                               00000280
               PIC X(0010).                                             00000290
           02  GS43-NSEQ                                                00000300
               PIC S9(13) COMP-3.                                       00000310
           02  GS43-WTOPE                                               00000320
               PIC X(0001).                                             00000330
           02  GS43-WMUTATION                                           00000340
               PIC X(0001).                                             00000350
           02  GS43-QMVT                                                00000360
               PIC S9(5) COMP-3.                                        00000370
           02  GS43-DMVT                                                00000380
               PIC X(0008).                                             00000390
           02  GS43-DSYST                                               00000400
               PIC S9(13) COMP-3.                                       00000410
           02  GS43-NSOCMAJ                                             00000420
               PIC X(0003).                                             00000430
           02  GS43-NLIEUMAJ                                            00000440
               PIC X(0003).                                             00000450
           02  GS43-NSSLIEUMAJ                                          00000460
               PIC X(0003).                                             00000470
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000480
      *---------------------------------------------------------        00000490
      *   LISTE DES FLAGS DE LA TABLE RVGS4300                          00000500
      *---------------------------------------------------------        00000510
      *                                                                 00000520
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS4300-FLAGS.                                              00000530
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS4300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSOCORIG-F                                          00000540
      *        PIC S9(4) COMP.                                          00000550
      *--                                                                       
           02  GS43-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NLIEUORIG-F                                         00000560
      *        PIC S9(4) COMP.                                          00000570
      *--                                                                       
           02  GS43-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSSLIEUORIG-F                                       00000580
      *        PIC S9(4) COMP.                                          00000590
      *--                                                                       
           02  GS43-NSSLIEUORIG-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSOCDEST-F                                          00000600
      *        PIC S9(4) COMP.                                          00000610
      *--                                                                       
           02  GS43-NSOCDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NLIEUDEST-F                                         00000620
      *        PIC S9(4) COMP.                                          00000630
      *--                                                                       
           02  GS43-NLIEUDEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSSLIEUDEST-F                                       00000640
      *        PIC S9(4) COMP.                                          00000650
      *--                                                                       
           02  GS43-NSSLIEUDEST-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NCODIC-F                                            00000660
      *        PIC S9(4) COMP.                                          00000670
      *--                                                                       
           02  GS43-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NORIGINE-F                                          00000680
      *        PIC S9(4) COMP.                                          00000690
      *--                                                                       
           02  GS43-NORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-CPROG-F                                             00000700
      *        PIC S9(4) COMP.                                          00000710
      *--                                                                       
           02  GS43-CPROG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-COPER-F                                             00000720
      *        PIC S9(4) COMP.                                          00000730
      *--                                                                       
           02  GS43-COPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSEQ-F                                              00000740
      *        PIC S9(4) COMP.                                          00000750
      *--                                                                       
           02  GS43-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-WTOPE-F                                             00000760
      *        PIC S9(4) COMP.                                          00000770
      *--                                                                       
           02  GS43-WTOPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-WMUTATION-F                                         00000780
      *        PIC S9(4) COMP.                                          00000790
      *--                                                                       
           02  GS43-WMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-QMVT-F                                              00000800
      *        PIC S9(4) COMP.                                          00000810
      *--                                                                       
           02  GS43-QMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-DMVT-F                                              00000820
      *        PIC S9(4) COMP.                                          00000830
      *--                                                                       
           02  GS43-DMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-DSYST-F                                             00000840
      *        PIC S9(4) COMP.                                          00000850
      *--                                                                       
           02  GS43-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSOCMAJ-F                                           00000860
      *        PIC S9(4) COMP.                                          00000870
      *--                                                                       
           02  GS43-NSOCMAJ-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NLIEUMAJ-F                                          00000880
      *        PIC S9(4) COMP.                                          00000890
      *--                                                                       
           02  GS43-NLIEUMAJ-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSSLIEUMAJ-F                                        00000900
      *        PIC S9(4) COMP.                                          00000910
      *--                                                                       
           02  GS43-NSSLIEUMAJ-F                                                
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00000920
                                                                                
