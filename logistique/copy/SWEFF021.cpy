      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------------------------*               
      *    FICHIER CSV EXTRACTION DE L'ETAT JEF021              *               
      *    LONGUEUR : 197                                       *               
      *---------------------------------------------------------*               
       01  F021-CSV-ENREG.                                                      
           05  F021-CSV-DONNEES.                                                
               10  F021-CSV-NOMETAT      PIC X(06).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-DATED        PIC X(08).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-DATEF        PIC X(08).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-NRENDU       PIC X(20).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-DRENDU       PIC X(08).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-MRENDU       PIC X(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-LTIERS       PIC X(15).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-CTIERS       PIC X(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-CODE         PIC X(01).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-QTRDU        PIC 9(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-NCODIC       PIC X(07).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-CFAM         PIC X(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-CMARQ        PIC X(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-LREF         PIC X(20).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-NLIEUHS      PIC X(03).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-NHS          PIC X(07).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-NENVOI       PIC X(07).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-DENVOI       PIC X(08).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-QTENV        PIC 9(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-PRMP         PIC -9(6)V99.                          
               10  F021-CSV-PRMP-R       REDEFINES  F021-CSV-PRMP               
                                         PIC X(09).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-PVTTC        PIC -9(7)V99.                          
               10  F021-CSV-PVTTC-R      REDEFINES  F021-CSV-PVTTC              
                                         PIC X(10).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CSV-ECOPA        PIC -9(5)V99.                          
               10  F021-CSV-ECOPA-R      REDEFINES  F021-CSV-ECOPA              
                                         PIC X(08).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
      *                                                                         
      *-----------------------------------------------------------*             
                                                                                
