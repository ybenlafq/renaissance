      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU15   EMU15                                              00000020
      ***************************************************************** 00000030
       01   EMU15I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSOCENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCENTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCENTI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNDEPOTI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLDEPOTI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCROSSDOCKL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCROSSDOCKL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCROSSDOCKF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCROSSDOCKI    PIC X.                                     00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCODICI  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCFAMI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMARQI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLCODICI  PIC X(20).                                      00000490
           02 M11I OCCURS   13 TIMES .                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLIBELLEI    PIC X(11).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MQTEI   PIC X(7).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPRESL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MTYPRESL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPRESF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MTYPRESI     PIC X.                                     00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCLIEUL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MSOCLIEUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSOCLIEUF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MSOCLIEUI    PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNLIEUI      PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNORDREL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNORDREL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNORDREF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNORDREI     PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNVENTEI     PIC X(7).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTATIL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MNMUTATIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNMUTATIF    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNMUTATII    PIC X(7).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMODELL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MMODELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMODELF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MMODELI      PIC X(3).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDELIVRL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDDELIVRL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDDELIVRF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDDELIVRI    PIC X(8).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVENTEL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQVENTEF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQVENTEI     PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRESL  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MQRESL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQRESF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQRESI  PIC X(6).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODSUIVL     COMP PIC S9(4).                            00000990
      *--                                                                       
           02 MNCODSUIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNCODSUIVF     PIC X.                                     00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNCODSUIVI     PIC X(7).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(12).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(61).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * SDF: EMU15   EMU15                                              00001280
      ***************************************************************** 00001290
       01   EMU15O REDEFINES EMU15I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MPAGEA    PIC X.                                          00001470
           02 MPAGEC    PIC X.                                          00001480
           02 MPAGEP    PIC X.                                          00001490
           02 MPAGEH    PIC X.                                          00001500
           02 MPAGEV    PIC X.                                          00001510
           02 MPAGEO    PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNSOCENTA      PIC X.                                     00001540
           02 MNSOCENTC PIC X.                                          00001550
           02 MNSOCENTP PIC X.                                          00001560
           02 MNSOCENTH PIC X.                                          00001570
           02 MNSOCENTV PIC X.                                          00001580
           02 MNSOCENTO      PIC X(3).                                  00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNDEPOTA  PIC X.                                          00001610
           02 MNDEPOTC  PIC X.                                          00001620
           02 MNDEPOTP  PIC X.                                          00001630
           02 MNDEPOTH  PIC X.                                          00001640
           02 MNDEPOTV  PIC X.                                          00001650
           02 MNDEPOTO  PIC X(3).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLDEPOTA  PIC X.                                          00001680
           02 MLDEPOTC  PIC X.                                          00001690
           02 MLDEPOTP  PIC X.                                          00001700
           02 MLDEPOTH  PIC X.                                          00001710
           02 MLDEPOTV  PIC X.                                          00001720
           02 MLDEPOTO  PIC X(20).                                      00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCROSSDOCKA    PIC X.                                     00001750
           02 MCROSSDOCKC    PIC X.                                     00001760
           02 MCROSSDOCKP    PIC X.                                     00001770
           02 MCROSSDOCKH    PIC X.                                     00001780
           02 MCROSSDOCKV    PIC X.                                     00001790
           02 MCROSSDOCKO    PIC X.                                     00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNCODICA  PIC X.                                          00001820
           02 MNCODICC  PIC X.                                          00001830
           02 MNCODICP  PIC X.                                          00001840
           02 MNCODICH  PIC X.                                          00001850
           02 MNCODICV  PIC X.                                          00001860
           02 MNCODICO  PIC X(7).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MCFAMA    PIC X.                                          00001890
           02 MCFAMC    PIC X.                                          00001900
           02 MCFAMP    PIC X.                                          00001910
           02 MCFAMH    PIC X.                                          00001920
           02 MCFAMV    PIC X.                                          00001930
           02 MCFAMO    PIC X(5).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MCMARQA   PIC X.                                          00001960
           02 MCMARQC   PIC X.                                          00001970
           02 MCMARQP   PIC X.                                          00001980
           02 MCMARQH   PIC X.                                          00001990
           02 MCMARQV   PIC X.                                          00002000
           02 MCMARQO   PIC X(5).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MLCODICA  PIC X.                                          00002030
           02 MLCODICC  PIC X.                                          00002040
           02 MLCODICP  PIC X.                                          00002050
           02 MLCODICH  PIC X.                                          00002060
           02 MLCODICV  PIC X.                                          00002070
           02 MLCODICO  PIC X(20).                                      00002080
           02 M11O OCCURS   13 TIMES .                                  00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MLIBELLEA    PIC X.                                     00002110
             03 MLIBELLEC    PIC X.                                     00002120
             03 MLIBELLEP    PIC X.                                     00002130
             03 MLIBELLEH    PIC X.                                     00002140
             03 MLIBELLEV    PIC X.                                     00002150
             03 MLIBELLEO    PIC X(11).                                 00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MQTEA   PIC X.                                          00002180
             03 MQTEC   PIC X.                                          00002190
             03 MQTEP   PIC X.                                          00002200
             03 MQTEH   PIC X.                                          00002210
             03 MQTEV   PIC X.                                          00002220
             03 MQTEO   PIC X(7).                                       00002230
             03 FILLER       PIC X(2).                                  00002240
             03 MTYPRESA     PIC X.                                     00002250
             03 MTYPRESC     PIC X.                                     00002260
             03 MTYPRESP     PIC X.                                     00002270
             03 MTYPRESH     PIC X.                                     00002280
             03 MTYPRESV     PIC X.                                     00002290
             03 MTYPRESO     PIC X.                                     00002300
             03 FILLER       PIC X(2).                                  00002310
             03 MSOCLIEUA    PIC X.                                     00002320
             03 MSOCLIEUC    PIC X.                                     00002330
             03 MSOCLIEUP    PIC X.                                     00002340
             03 MSOCLIEUH    PIC X.                                     00002350
             03 MSOCLIEUV    PIC X.                                     00002360
             03 MSOCLIEUO    PIC X(3).                                  00002370
             03 FILLER       PIC X(2).                                  00002380
             03 MNLIEUA      PIC X.                                     00002390
             03 MNLIEUC PIC X.                                          00002400
             03 MNLIEUP PIC X.                                          00002410
             03 MNLIEUH PIC X.                                          00002420
             03 MNLIEUV PIC X.                                          00002430
             03 MNLIEUO      PIC X(3).                                  00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MNORDREA     PIC X.                                     00002460
             03 MNORDREC     PIC X.                                     00002470
             03 MNORDREP     PIC X.                                     00002480
             03 MNORDREH     PIC X.                                     00002490
             03 MNORDREV     PIC X.                                     00002500
             03 MNORDREO     PIC X(5).                                  00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MNVENTEA     PIC X.                                     00002530
             03 MNVENTEC     PIC X.                                     00002540
             03 MNVENTEP     PIC X.                                     00002550
             03 MNVENTEH     PIC X.                                     00002560
             03 MNVENTEV     PIC X.                                     00002570
             03 MNVENTEO     PIC X(7).                                  00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MNMUTATIA    PIC X.                                     00002600
             03 MNMUTATIC    PIC X.                                     00002610
             03 MNMUTATIP    PIC X.                                     00002620
             03 MNMUTATIH    PIC X.                                     00002630
             03 MNMUTATIV    PIC X.                                     00002640
             03 MNMUTATIO    PIC X(7).                                  00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MMODELA      PIC X.                                     00002670
             03 MMODELC PIC X.                                          00002680
             03 MMODELP PIC X.                                          00002690
             03 MMODELH PIC X.                                          00002700
             03 MMODELV PIC X.                                          00002710
             03 MMODELO      PIC X(3).                                  00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MDDELIVRA    PIC X.                                     00002740
             03 MDDELIVRC    PIC X.                                     00002750
             03 MDDELIVRP    PIC X.                                     00002760
             03 MDDELIVRH    PIC X.                                     00002770
             03 MDDELIVRV    PIC X.                                     00002780
             03 MDDELIVRO    PIC X(8).                                  00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MQVENTEA     PIC X.                                     00002810
             03 MQVENTEC     PIC X.                                     00002820
             03 MQVENTEP     PIC X.                                     00002830
             03 MQVENTEH     PIC X.                                     00002840
             03 MQVENTEV     PIC X.                                     00002850
             03 MQVENTEO     PIC X(5).                                  00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MQRESA  PIC X.                                          00002880
             03 MQRESC  PIC X.                                          00002890
             03 MQRESP  PIC X.                                          00002900
             03 MQRESH  PIC X.                                          00002910
             03 MQRESV  PIC X.                                          00002920
             03 MQRESO  PIC X(6).                                       00002930
           02 FILLER    PIC X(2).                                       00002940
           02 MNCODSUIVA     PIC X.                                     00002950
           02 MNCODSUIVC     PIC X.                                     00002960
           02 MNCODSUIVP     PIC X.                                     00002970
           02 MNCODSUIVH     PIC X.                                     00002980
           02 MNCODSUIVV     PIC X.                                     00002990
           02 MNCODSUIVO     PIC X(7).                                  00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MZONCMDA  PIC X.                                          00003020
           02 MZONCMDC  PIC X.                                          00003030
           02 MZONCMDP  PIC X.                                          00003040
           02 MZONCMDH  PIC X.                                          00003050
           02 MZONCMDV  PIC X.                                          00003060
           02 MZONCMDO  PIC X(12).                                      00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(61).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
