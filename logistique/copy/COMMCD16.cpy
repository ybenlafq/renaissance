      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-CD16-LONG-COMMAREA PIC S9(4) COMP VALUE +96.                    
      *--                                                                       
       01  COMM-CD16-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +96.                  
      *}                                                                        
       01  Z-COMMAREA-CD16.                                                     
           05  COMM-CD16-SSAAMMJJ   PIC X(8).                                   
           05  COMM-CD16-NSOCORIG   PIC X(3).                                   
           05  COMM-CD16-NSOCDEST   PIC X(3).                                   
           05  COMM-CD16-NLIEUDEST  PIC X(3).                                   
           05  COMM-CD16-NOMTS      PIC X(8).                                   
           05  COMM-CD16-NORIGINE   PIC X(7).                                   
           05  COMM-CD16-ERR        PIC X(60).                                  
           05  COMM-CD16-CDRET      PIC X(4).                                   
                                                                                
