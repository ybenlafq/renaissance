      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * COM-RM60-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +8192            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-RM60-LONG-COMMAREA PIC S9(4) COMP VALUE +8192.                   
      *--                                                                       
       01  COM-RM60-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +8192.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ---------------------------------- 100           
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS ------------------- 020           
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ----- 100           
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC XX.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS PIC 99.                                        
              05 COMM-DATE-SEMAA PIC 99.                                        
              05 COMM-DATE-SEMNU PIC 99.                                        
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ------------------------- 7972             
      *                                                                         
           02  COMM-RM60-APPLI.                                                 
      *=> ZONES DE SAUVEGARDE COMMUNES AU MENU PRINCIPAL                        
      *=> ET AUX TRANSACTIONS DE NIVEAU INFERIEUR                               
      *--------------------------------------------------------- 402            
              03  COMM-RM60.                                                    
                  05  COMM-RM60-MZONCMD          PIC X(15).                     
                  05  COMM-RM60-MWFONC           PIC X(03).                     
                  05  COMM-RM60-CRMGROUP         PIC X(5).                      
                  05  COMM-RM60-LRMGROUP         PIC X(20).                     
                  05  COMM-RM60-MCHEFG           PIC X(5).                      
                  05  COMM-RM60-MLCHEFG          PIC X(20).                     
                  05  COMM-RM60-MCFAM            PIC X(5).                      
                  05  COMM-RM60-MLFAM            PIC X(20).                     
                  05  COMM-RM60-MLAGREG          PIC X(20).                     
                  05  COMM-RM60-MNCODIC          PIC 9(7).                      
                  05  COMM-RM60-MNLIEU           PIC X(3).                      
                  05  COMM-RM60-NSOCIETE         PIC X(3).                      
                  05  COMM-RM60-MNSOC            PIC X(3).                      
                  05  COMM-RM60-TRAIT-ORI        PIC X(5).                      
                  05  COMM-RM60-PAGE-ORI         PIC S9(2) COMP-3.              
                  05  COMM-RM60-TYPE-SIMU        PIC X.                         
                  05  COMM-RM60-C-SIMU           PIC X(7).                      
                  05  COMM-RM60-S-SIMU           PIC X(20).                     
                  05  COMM-RM60-F-SIMU           PIC X(5).                      
                  05  COMM-RM60-RMGROUPE         PIC X(5).                      
                  05  COMM-RM60-GROUPE.                                         
                      06 COMM-RM60-DETAIL-GROUPE OCCURS 10.                     
                         10 COMM-RM60-MCGROUP    PIC X(5).                      
                         10 COMM-RM60-MFILIALE   PIC X(3).                      
                      06 COMM-RM60-FILIALE       PIC X(3) OCCURS 10.            
                  05  COMM-RM60-NBGROUP          PIC S9(3).                     
                  05  COMM-RM60-NBFIL            PIC S9(3).                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           05  COMM-SWAP-CURS             PIC S9(4) COMP.                
      *--                                                                       
                  05  COMM-SWAP-CURS             PIC S9(4) COMP-5.              
      *}                                                                        
                  05  COMM-RM60-GRP-SIMU         PIC X(05).                     
                  05  COMM-RM60-DSIMULATION      PIC X(08).                     
                  05  COMM-RM60-NSIMULATION      PIC X(03).                     
                  05  COMM-RM60-QTE              PIC X(04).                     
                  05  COMM-RM60-RANG-QTE         PIC 9(01).                     
                  05  COMM-RM60-QSA              PIC X(03).                     
                  05  COMM-RM60-RANG-QSA         PIC 9(01).                     
                  05  COMM-RM60-FLAG             PIC X(01).                     
                  05  COMM-RM60-QSTOCK         PIC S9(5) COMP-3 VALUE 0.        
                  05  COMM-RM60-MESSAGE          PIC X(78).                     
                  05  COMM-RM60-FILLER           PIC X(001).                    
      * AJOUT NLG ------------------------------------------ 346                
           02 COMM-RM60-NLG.                                                    
GD            03  COMM-RM60-MNSOCENT         PIC X(03).                         
              03  COMM-RM60-MNDEPOT          PIC X(03).                         
              03 COMM-RM60-TYPUSER           PIC X(8).                          
              03 COMM-RM60-TAB-SOC.                                             
                04 COMM-RM60-AUTOR-DEPOT        OCCURS 10.                      
                  05 COMM-RM60-AUTOR-NSOC    PIC X(3).                          
                  05 COMM-RM60-AUTOR-NDEPOT  PIC X(3).                          
              03 COMM-RM60-NSOCCICS          PIC X(8).                          
              03 COMM-RM60-CFLGMAG           PIC X(5).                          
              03 COMM-RM60-NSOC              PIC X(3).                          
NT            03 COMM-RM60-TCGROUP              OCCURS 50.                      
                 04 COMM-RM60-CGROUP         PIC X(5).                          
              03 COMM-RM60-NSOCIETE2         PIC X(3).                          
              03 COMM-RM60-NLIEU             PIC X(3).                          
      *=> DE NIVEAU INFERIEUR ---------------------------------- 1412           
             02  COMM-RM62.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05  COMM-RM62-PAGE              PIC S9(2) COMP.                
      *--                                                                       
                 05  COMM-RM62-PAGE              PIC S9(2) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05  COMM-RM62-PAGE-TOT          PIC S9(2) COMP.                
      *--                                                                       
                 05  COMM-RM62-PAGE-TOT          PIC S9(2) COMP-5.              
      *}                                                                        
                 05  COMM-RM62-SEM               PIC X(06).                     
                 05  COMM-RM62-SEM1              PIC X(06).                     
                 05  COMM-RM62-SEM2              PIC X(06).                     
                 05  COMM-RM62-SEM3              PIC X(06).                     
                 05  COMM-RM62-SEM4              PIC X(06).                     
                 05  COMM-RM62-SEM5              PIC X(06).                     
                 05  COMM-RM62-SEM6              PIC X(06).                     
                 05  COMM-RM62-SEM7              PIC X(06).                     
                 05  COMM-RM62-SEM8              PIC X(06).                     
                 05  COMM-RM62-SEM-1             PIC X(06).                     
                 05  COMM-RM62-SEM-2             PIC X(06).                     
                 05  COMM-RM62-SEM-3             PIC X(06).                     
                 05  COMM-RM62-SEM-4             PIC X(06).                     
                 05  COMM-RM62-SEM-5             PIC X(06).                     
                 05  COMM-RM62-SEM-6             PIC X(06).                     
                 05  COMM-RM62-SEM-7             PIC X(06).                     
                 05  COMM-RM62-SEM-8             PIC X(06).                     
                 05  COMM-RM62-DDEB1             PIC X(8).                      
                 05  COMM-RM62-DDEB1-2           PIC X(10).                     
                 05  COMM-RM62-DDEB2             PIC X(8).                      
                 05  COMM-RM62-DDEB2-2           PIC X(10).                     
                 05  COMM-RM62-DDEB3             PIC X(8).                      
                 05  COMM-RM62-DDEB3-2           PIC X(10).                     
                 05  COMM-RM62-DDEB4             PIC X(8).                      
                 05  COMM-RM62-DDEB4-2           PIC X(10).                     
                 05  COMM-RM62-DFIN1             PIC X(8).                      
                 05  COMM-RM62-DFIN1-2           PIC X(10).                     
                 05  COMM-RM62-DFIN2             PIC X(8).                      
                 05  COMM-RM62-DFIN2-2           PIC X(10).                     
                 05  COMM-RM62-DFIN3             PIC X(8).                      
                 05  COMM-RM62-DFIN3-2           PIC X(10).                     
                 05  COMM-RM62-DFIN4             PIC X(8).                      
                 05  COMM-RM62-DFIN4-2           PIC X(10).                     
                 05  COMM-RM62-DEB1              PIC X(8).                      
                 05  COMM-RM62-DEB1-2            PIC X(10).                     
                 05  COMM-RM62-DEB2              PIC X(8).                      
                 05  COMM-RM62-DEB2-2            PIC X(10).                     
                 05  COMM-RM62-DEB3              PIC X(8).                      
                 05  COMM-RM62-DEB3-2            PIC X(10).                     
                 05  COMM-RM62-FIN1              PIC X(8).                      
                 05  COMM-RM62-FIN1-2            PIC X(10).                     
                 05  COMM-RM62-FIN2              PIC X(8).                      
                 05  COMM-RM62-FIN2-2            PIC X(10).                     
                 05  COMM-RM62-FIN3              PIC X(8).                      
                 05  COMM-RM62-FIN3-2            PIC X(10).                     
                 05  COMM-RM62-GROUPE            PIC X.                         
                 05  COMM-RM62-ACCES-CHEFG       PIC X.                         
                 05  COMM-RM62-DONNEES-FAM.                                     
                     10 COMM-RM62-TAB-FAM OCCURS 200.                           
                        15 COMM-RM62-CFAM        PIC X(5).                      
                 05  COMM-RM62-NFAM-OK           PIC S9(3) COMP-3.              
                 05  COMM-RM62-NFAM-MAX          PIC S9(3) COMP-3.              
                 05  COMM-RM62-FILLER            PIC X(48).                     
      *------------------------------------------------------- 190              
             02  COMM-RM63.                                                     
                 05  COMM-RM63-MODIF-DONNEE      PIC X.                         
                 05  COMM-RM63-PAGE              PIC  9(3).                     
                 05  COMM-RM63-PAGE-MAX          PIC  9(3).                     
                 05  COMM-RM63-ITEMAX            PIC S9(5) COMP-3.              
                 05  COMM-RM63-LIGNE             PIC 9.                         
                 05  COMM-RM63-CHOIX             PIC 9.                         
                 05  COMM-RM63-NUMSEM            OCCURS 3.                      
                    10  COMM-RM63-SEMAINE        PIC X(6).                      
                 05  COMM-RM63-QUANTITE-SEG      OCCURS 3.                      
                    10  COMM-RM63-QPV            PIC S9(6).                     
                    10  COMM-RM63-QPV-T          PIC X.                         
                 05  COMM-RM63-QUANTITE-ART      OCCURS 3.                      
                    10  COMM-RM63-QART           PIC 9(6).                      
AL0607*          05  COMM-RM63-FILLER            PIC X(118).                    
                 05  COMM-RM63-FILLER            PIC X(68).                     
      *------------------------------------------------------- 5                
             02  COMM-RM67.                                                     
                 05  COMM-RM67-MODIF-DONNEE      PIC X.                         
AL0607           05  COMM-RM67-PAGE              PIC  9(3).                     
AL0607           05  COMM-RM67-PAGE-MAX          PIC  9(3).                     
AL0607           05  COMM-RM67-FILLER            PIC  X(48).                    
      * ----------------------------------------------- RESTE 5617              
      *                                                                         
             02  COMM-RMXX                       PIC X(5617).                   
      *                                                                         
      **************************************************************            
             02  COMM-RM61 REDEFINES COMM-RMXX.                                 
      * ---------------------------------------------------284                  
                 05  COMM-RM61-DETAIL-GROUPE     OCCURS 10.                     
                     10 COMM-RM61-CGROUP         PIC X(5).                      
                     10 COMM-RM61-LGROUP         PIC X(20).                     
                     10 COMM-RM61-SUP            PIC X.                         
                     10 COMM-RM61-UPGR           PIC X.                         
                     10 COMM-RM61-MAJ            PIC X.                         
                 05  COMM-RM61-CREATE            PIC X(1).                      
                 05  COMM-RM61-SUPP              PIC X(1).                      
                 05  COMM-RM61-UPGRADE           PIC X(1).                      
                 05  COMM-RM61-SUP-GROUP         PIC  X.                        
                 05  COMM-RM61-FILLER            PIC X(5333).                   
      **************************************************************            
             02  COMM-RM64 REDEFINES COMM-RMXX.                                 
      **-----------------------------------------------------230                
                 05 COMM-RM64-PAGE              PIC  9(2).                      
                 05 COMM-RM64-PAGE-MAX          PIC  9(2).                      
                 05 COMM-RM64-ITEMAX            PIC S9(5) COMP-3.               
                 05 COMM-RM64-DEBUT-TS1         PIC S9(5) COMP-3.               
                 05 COMM-RM64-FIN-TS1           PIC S9(5) COMP-3.               
                 05 COMM-RM64-NCODIC            PIC X(7).                       
                 05 COMM-RM64-LREF              PIC X(20).                      
                 05 COMM-RM64-CMARQ             PIC X(5).                       
                 05 COMM-RM64-CFAM              PIC X(5).                       
                 05 COMM-RM64-MLAGREG           PIC X(20).                      
                 05 COMM-RM64-GROUPES.                                          
                    10 COMM-RM64-DETAIL-GROUPE  OCCURS 10.                      
                       15 COMM-RM64-CGROUP      PIC X(5).                       
                       15 COMM-RM64-W20         PIC X.                          
                       15 COMM-RM64-W80         PIC X.                          
                       15 COMM-RM64-NTABLE      PIC X(2).                       
                       15 COMM-RM64-NTABLESA    PIC X(2).                       
                 05 COMM-RM64-EIBTRNID          PIC X(4).                       
                 05 COMM-RM64-STOCK-FORCE       PIC X.                          
                 05 COMM-RM64-MRM6-ITEM         PIC S9(5) COMP-3.               
AA0814           05 COMM-RM64-DEPOT.                                            
  |                 10 COMM-RM64-DETAIL-DEPOT   OCCURS 3.                       
  |                    15 COMM-RM64-NSOCDEPOT   PIC X(3).                       
  |                    15 COMM-RM64-NDEPOT      PIC X(3).                       
  |                    15 COMM-RM64-QSTOCKDEPOT PIC S9(6) COMP-3.               
  |                    15 COMM-RM64-QNRMDEPOT   PIC S9(6) COMP-3.               
AA0814           05 COMM-RM64-MTQSTKMAG         PIC S9(6) COMP-3.               
CL1209           05 COMM-RM64-TQVTEP            PIC S9(6) COMP-3.               
     |           05 COMM-RM64-TQVTER            PIC S9(6) COMP-3.               
     |           05 COMM-RM64-TQSMP             PIC S9(5) COMP-3.               
     |           05 COMM-RM64-TQSM              PIC S9(5) COMP-3.               
CL1209           05 COMM-RM64-IMPACTR           PIC S9(5) COMP-3.               
AA0814           05 COMM-RM64-IMP-POS           PIC S9(5) COMP-3.               
AA0814           05 COMM-RM64-IMP-NEG           PIC S9(5) COMP-3.               
AA0814           05 COMM-RM64-QNRM              PIC S9(5) COMP-3.               
CL0310           05 COMM-RM64-NCODICLIE         PIC X(7).                       
CL0310           05 COMM-RM64-LREFLIE           PIC X(20).                      
AA0814           05 COMM-RM64-WSENSAPPRO        PIC X(1).                       
AA0814           05 COMM-RM64-EXP               PIC X(9).                       
AA0815           05 COMM-RM64-FILLER            PIC X(5320).                    
CL0310*          05 COMM-RM64-FILLER            PIC X(5385).                    
CL0310*          05 COMM-RM64-FILLER            PIC X(5412).                    
CL1209*          05 COMM-RM64-FILLER            PIC X(5429).                    
      **************************************************************            
             02  COMM-RM65 REDEFINES COMM-RMXX.                                 
      * ------------------------------------------------------108               
                 05 COMM-RM65-PAGE              PIC  9(3).                      
                 05 COMM-RM65-PAGE-MAX          PIC  9(3).                      
                 05 COMM-RM65-ITEMAX            PIC S9(5) COMP-3.               
                 05 COMM-RM65-NSOC              PIC X(3).                       
                 05 COMM-RM65-NMAG              PIC X(3).                       
                 05 COMM-RM65-LMAG              PIC X(20).                      
                 05 COMM-RM65-NZONPRIX          PIC X(2).                       
AA0213*          05 COMM-RM65-CEXPO             PIC X(5).                       
AA0213           05 COMM-RM65-CEXPO             PIC X(10).                      
                 05 COMM-RM65-CFAM              PIC X(5).                       
                 05 COMM-RM65-LFAM              PIC X(20).                      
                 05 COMM-RM65-MLAGREG           PIC X(20).                      
                 05 COMM-RM65-W20               PIC X.                          
                 05 COMM-RM65-W80               PIC X.                          
                 05 COMM-RM65-NTABLE            PIC X(2).                       
                 05 COMM-RM65-NTABLESA          PIC X(2).                       
                 05 COMM-RM65-STOCK-FORCE       PIC X.                          
                 05 COMM-RM65-GROUPE            PIC X.                          
                 05 COMM-RM65-NSOCGRP           PIC X(3).                       
                 05 COMM-RM65-MCGROUP           PIC X(5).                       
AA0213           05 COMM-RM65-FILLER            PIC X(5509).                    
AA0213*          05 COMM-RM65-FILLER            PIC X(5514).                    
      **************************************************************            
             02  COMM-RM66 REDEFINES COMM-RMXX.                                 
      * ------------------------------------------------------ 70               
                 05  COMM-RM66-MODIF-DONNEE      PIC X.                         
                 05  COMM-RM66-PAGE              PIC  9(2).                     
                 05  COMM-RM66-PAGE-MAX          PIC  9(2).                     
                 05  COMM-RM66-FLAG-ETAT         PIC  X.                        
                 05  COMM-RM66-LIGNE-DEPOT       OCCURS 10.                     
                     10 COMM-RM66-NSOCDEPOT      PIC X(3).                      
                     10 COMM-RM66-NDEPOT         PIC X(3).                      
                 05  COMM-RM66-NBDEP             PIC S9(3).                     
                 05  COMM-RM66-FILLER            PIC X(5547).                   
      **************************************************************            
      *=> ZONES PROPRES A TRM68          = 936 + 133 + 4500 = 5569              
             02  COMM-RM68 REDEFINES COMM-RMXX.                                 
                 03  COMM-RM68-TYPE-SIMU       PIC X(01).                       
                 03  COMM-RM68-DEFFET-SIMU     PIC X(10).                       
                 03  COMM-RM68-DEFFET-SAMJ     PIC X(8).                        
                 03  COMM-RM68-DFINEFFET-SIMU  PIC X(10).                       
                 03  COMM-RM68-DFINEFFET-SAMJ  PIC X(8).                        
                 03  COMM-RM68-CLE-REPAR       PIC X(01).                       
                 03  COMM-RM68-AFFICH-MESS     PIC X(01).                       
                 03  COMM-RM68-CREATE-NSIMU    PIC X(01).                       
                 03  COMM-RM68-DSYST           PIC S9(13) COMP-3.               
                 03  COMM-RM68-CODRET          PIC S9(4)  COMP-3.               
                 03  COMM-RM68-NUMERR          PIC X(04).                       
                 03  COMM-RM68-TRIEDIT         PIC X.                           
                 03  COMM-RM68-MESS            PIC X(78).                       
      *           TABLE DE CONTROLE DES DOUBLONS     12 X 78 = -936             
                 03  COMM-RM68-TABLE.                                           
                     05  COMM-RM68-T-CODE OCCURS 12.                            
                         07   COMM-RM68-T-NCODIC PIC X(07).                     
                         07   COMM-RM68-T-CFAM  PIC X(05).                      
                         07   COMM-RM68-T-LREF  PIC X(20).                      
                         07   COMM-RM68-T-CLEREP PIC X(01).                     
                         07   COMM-RM68-T-GROUPE PIC X(01).                     
                         07   COMM-RM68-T-NCODICM PIC X(07).                    
                         07   COMM-RM68-E-NCODICM PIC X(07).                    
                         07   COMM-RM68-E-NCODFLG PIC X(01).                    
                         07   COMM-RM68-T-QSP   PIC X(02).                      
                         07   COMM-RM68-T-QTE1  PIC X(04).                      
                         07   COMM-RM68-T-QSA1  PIC X(03).                      
                         07   COMM-RM68-T-QTE2  PIC X(04).                      
                         07   COMM-RM68-T-QSA2  PIC X(03).                      
                         07   COMM-RM68-T-QTE3  PIC X(04).                      
                         07   COMM-RM68-T-QSA3  PIC X(03).                      
                 03  COMM-RM68-TABLE2.                                          
                     05  COMM-RM68-T-CODE2 OCCURS 12.                           
                         07   COMM-RM68-T-NSOCDEPOT PIC X(3).                   
                         07   COMM-RM68-T-NDEPOT    PIC X(3).                   
      *  TABLE SOCIETE/LIEU DU GROUPE DE GROUPES =     500 X 9 = -4500          
                 03  COMM-RM68-TABMAG.                                          
                     05  COMM-RM68-T-MAG OCCURS 500.                            
                         07   COMM-RM68-T-NMAG    PIC X(06).                    
                 03  COMM-RM68-TABSOC.                                          
                     05  COMM-RM68-T-SOC2  OCCURS 500.                          
                         07   COMM-RM68-T-NSOC    PIC X(03).                    
                 03  COMM-RM68-FILLER           PIC X(48).                      
      *                                                                         
      **************************************************************            
      *----------------------------------------------------- 3209               
             02  COMM-RM69 REDEFINES COMM-RMXX.                                 
                 05  COMM-69-MENU-20              PIC X(01).                    
                 05  COMM-69-TYPE-CODIC           PIC X(01).                    
                 05  COMM-69-LMARQ                PIC X(20).                    
                 05  COMM-69-LFAM                 PIC X(20).                    
                 05  COMM-69-LREFFOURN            PIC X(20).                    
AA0613           05  COMM-69-LSTATCOMP            PIC X(03).                    
                 05  COMM-69-DISENT               PIC S9(05) COMP-3.            
                 05  COMM-69-ERR                  PIC 9(02).                    
                 05  COMM-69-ERR-LAST             PIC 9(02).                    
                 05  COMM-69-QCOLIDSTOCK          PIC S9(5) COMP-3.             
      *-----> TABLE DES CODES SELART ASSOCIES                                   
                 05  COMM-69-TABLE-SEL-ART.                                     
                     10  COMM-69-POSTE-SEL-ART    OCCURS 50                     
                       INDEXED BY COMM-69-IND1.                                 
                         15  ITEM-SEL-ART              PIC X(05).               
      *-----> TABLE DES CODICS (MAXIMUM 40)                                     
      *-----> LE CODIC DE RANG 1 EST SYSTEMATIQUEMENT RENSEIGNE                 
      *-----> LES AUTRES POSTES EN TABLE NE SONT UTILISES QUE DANS              
      *-----> LE CAS D'UN CODIC GROUPE (CODICS ELEMENTS DU GROUPE)              
                  05  COMM-69-TABLE-CODIC.                                      
                      10  COMM-69-POSTE            OCCURS 40.                   
                          15  COMM-69-NCODIC          PIC X(7).                 
                          15  COMM-69-QTYPLIEN        PIC S9(3) COMP-3.         
                          15  COMM-69-WSEQFAM         PIC S9(5) COMP-3.         
                          15  COMM-69-QNBPIECE        PIC S9(5) COMP-3.         
                          15  COMM-69-WMULTIFAM       PIC X.                    
                          15  COMM-69-CMARQ           PIC X(5).                 
                          15  COMM-69-CFAM            PIC X(5).                 
                          15  COMM-69-CRAYONFAM       PIC X(5).                 
                          15  COMM-69-WRAYONFAM       PIC X.                    
                          15  COMM-69-WSEQED          PIC S9(5) COMP-3.         
                          15  COMM-69-QPOIDS          PIC S9(7) COMP-3.         
                          15  COMM-69-QVOLUME         PIC S9(11) COMP-3.        
                          15  COMM-69-CMODSTOCK       PIC X(5).                 
                          15  COMM-69-CTYPCONDT       PIC X(5).                 
                          15  COMM-69-QCONDT          PIC S9(5) COMP-3.         
      *-----> IMAGE DES INFOS ECRAN  (12 LIGNES MAX)                            
                  05  COMM-69-ECRAN.                                            
                    10  COMM-69-LIGNE            OCCURS 12.                     
                      15  COMM-69-NMUTATION         PIC X(7).                   
                      15  COMM-69-DMUTATION         PIC X(8).                   
                      15  COMM-69-CSELART           PIC X(5).                   
                      15  COMM-69-QNBM3QUOTA        PIC S9(11) COMP-3.          
                      15  COMM-69-M3QUOTA           PIC S9(3)V99 COMP-3.        
                      15  COMM-69-QNBPQUOTA         PIC S9(5) COMP-3.           
                      15  COMM-69-QVOLUME-TRANS     PIC S9(11) COMP-3.          
                      15  COMM-69-QNBPIECES-TRANS   PIC S9(5) COMP-3.           
                      15  COMM-69-QNBLIGNES         PIC S9(5) COMP-3.           
                      15  COMM-69-QSAISIE           PIC S9(5) COMP-3.           
ADBRID****        AJOUT DES ZONES NECESSAIRE AU BRIDAGE DE LA SASIE             
                  05  COMM-69-FLAG-USER             PIC X.                      
                      88  COMM-69-USER-BRIDE        VALUE 'O'.                  
                      88  COMM-69-USER-PAS-BRIDE    VALUE 'N'.                  
ADBRID*           05  COMM-69-FILLER                PIC X(2411).                
ADBRID*           05  COMM-69-FILLER                PIC X(2410).                
AA0613            05  COMM-69-FILLER                PIC X(2407).                
      **************************************************************            
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM70 --2659              
           02  COMM-RM70     REDEFINES COMM-RMXX.                               
               05  COMM-70-FLAG-NEW          PIC X(01).                         
               05  COMM-70-NEW-PAGE          PIC X(01).                         
               05  COMM-70-NB-PAGE           PIC 9(02).                         
               05  COMM-70-NB-COLS           PIC 9(02).                         
               05  COMM-70-CURRENT-PAGE      PIC 9(02).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05  COMM-70-NB-TS-ITEM        PIC 9(18) COMP.                    
      *--                                                                       
               05  COMM-70-NB-TS-ITEM        PIC 9(18) COMP-5.                  
      *}                                                                        
               05  COMM-70-TYPE-CODIC           PIC X(01).                      
               05  COMM-70-LMARQ                PIC X(20).                      
               05  COMM-70-LFAM                 PIC X(20).                      
               05  COMM-70-LREFFOURN            PIC X(20).                      
               05  COMM-70-DISENT               PIC S9(05) COMP-3.              
               05  COMM-70-ERR                  PIC 9(02).                      
               05  COMM-70-ERR-PAGE             PIC 9(02).                      
               05  COMM-70-ERR-LAST             PIC 9(02).                      
               05  COMM-70-QCOLIDSTOCK          PIC S9(5) COMP-3.               
      *-----> TABLE DES CODES SELART ASSOCIES                                   
               05  COMM-70-TABLE-SEL-ART.                                       
                   10  COMM-70-POSTE-SEL-ART    OCCURS 50                       
                       INDEXED BY COMM-70-IND1.                                 
                       15  FILLER                    PIC X(05).                 
      *-----> TABLE DES CODICS (MAXIMUM 40)                                     
      *-----> LE CODIC DE RANG 1 EST SYSTEMATIQUEMENT RENSEIGNE                 
      *-----> LES AUTRES POSTES EN TABLE NE SONT UTILIS�S QUE DANS              
      *-----> LE CAS D'UN CODIC GROUPE (CODICS ELEMENTS DU GROUPE)              
               05  COMM-70-TABLE-CODIC.                                         
                   10  COMM-70-POSTE            OCCURS 40.                      
                       15  COMM-70-NCODIC            PIC X(7).                  
                       15  COMM-70-QTYPLIEN          PIC S9(3) COMP-3.          
                       15  COMM-70-WSEQFAM           PIC S9(5) COMP-3.          
                       15  COMM-70-QNBPIECE          PIC S9(5) COMP-3.          
                       15  COMM-70-WMULTIFAM         PIC X.                     
                       15  COMM-70-CMARQ             PIC X(5).                  
                       15  COMM-70-CFAM              PIC X(5).                  
                       15  COMM-70-CRAYONFAM         PIC X(5).                  
                       15  COMM-70-WRAYONFAM         PIC X.                     
                       15  COMM-70-WSEQED            PIC S9(5) COMP-3.          
                       15  COMM-70-QPOIDS            PIC S9(7) COMP-3.          
                       15  COMM-70-QVOLUME           PIC S9(11) COMP-3.         
                       15  COMM-70-CMODSTOCK         PIC X(5).                  
                       15  COMM-70-CTYPCONDT         PIC X(5).                  
                       15  COMM-70-QCONDT            PIC S9(5) COMP-3.          
                05  COMM-70-FILLER                   PIC X(2958).               
      **************************************************************            
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION RM71        -140         
           02  COMM-71 REDEFINES COMM-RMXX.                                     
               03  COMM-71-PAGE              PIC 9(2) COMP-3.                   
               03  COMM-71-PAGE-MAX          PIC 9(2) COMP-3.                   
               03  COMM-71-MNCODIC           PIC X(07).                         
               03  COMM-71-MREFFOURN         PIC X(20).                         
               03  COMM-71-MDSIMUL           PIC X(10).                         
               03  COMM-71-MCFAM             PIC X(05).                         
               03  COMM-71-MLFAM             PIC X(20).                         
               03  COMM-71-MNSIMUL           PIC X(03).                         
               03  COMM-71-MCMARQ            PIC X(05).                         
               03  COMM-71-MLMARQ            PIC X(20).                         
               03  COMM-71-MCRMGROUP         PIC X(05).                         
               03  COMM-71-MNSOCEN           PIC X(03).                         
               03  COMM-71-MNDEPOT           PIC X(03).                         
               03  COMM-71-MDEFFET           PIC X(10).                         
               03  COMM-71-MDFINEFF          PIC X(10).                         
               03  COMM-71-MQSOD             PIC X(04).                         
               03  COMM-71-MQSAD             PIC X(03).                         
               03  COMM-71-TOT-QSO           PIC 9(04).                         
               03  COMM-71-TOT-QSA           PIC 9(03).                         
               03  COMM-71-FLAG              PIC X(01).                         
               03  FILLER-COMM-71            PIC X(5477).                       
                                                                                
