      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM15                                          *  00020000
      ****************************************************************  00050000
      *                                                                 00060000
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 79.              00080000
       01  TS-DONNEES.                                                  00090000
              10 TS-LAGREG       PIC X(20).                             00120100
              10 TS-W20          PIC X(1).                              00121000
              10 TS-W80          PIC X(1).                              00121100
              10 TS-WTXEMP       PIC X(1).                              00121200
              10 TS-QSEUILEX     PIC X(6).                              00121300
              10 TS-DEFFET       PIC X(10).                             00121500
              10 TS-DFINEFFET    PIC X(10).                             00121600
              10 TS-INDISP       PIC X(5).                              00121700
              10 TS-POND         PIC X(4).                              00121800
              10 TS-INDICE       PIC X.                                 00122000
              10 TS-QSEUIL8S     PIC X(6).                              00122100
              10 TS-PVM          PIC X(4).                              00122200
              10 TS-QEXPMAX      PIC X(5).                              00122600
              10 TS-QLSMAX       PIC X(5).                              00122700
                                                                                
