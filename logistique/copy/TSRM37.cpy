      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM37                                          *  00020000
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
JA4288*01  TS-LONG              PIC S9(2) COMP-3 VALUE 24.              00060001
JA4288 01  TS-LONG              PIC S9(2) COMP-3 VALUE 30.              00061001
       01  TS-DONNEES.                                                  00070000
              10 TS-SOC          PIC 9(3).                              00080001
              10 TS-MAG          PIC 9(3).                              00090001
              10 TS-WASS         PIC X.                                 00100001
              10 TS-WASSM        PIC X.                                 00110000
              10 TS-QEXPO        PIC X(3).                              00120001
              10 TS-QLS          PIC X(3).                              00130001
              10 TS-QSTOCK       PIC X(5).                              00140001
              10 TS-INSERT       PIC X.                                 00150000
              10 TS-PM           PIC X.                                 00160001
              10 TS-MODIF        PIC X(3).                              00170001
JA4288        10 TS-QEXPO-TB     PIC X(3).                              00180001
JA4288        10 TS-QLS-TB       PIC X(3).                              00190001
                                                                                
