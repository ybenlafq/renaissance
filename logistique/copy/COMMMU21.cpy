      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      * COMMAREA SPECIFIQUE PRG TMU21 (TGS50 -> MENU)    TR: GS50  *    00030000
      *                                                            *    00031000
      *                       GESTION DES MUTATIONS                *    00040000
      *                                                            *    00050000
      * ZONES RESERVEES APPLICATIVES ------------------------------*    00060003
      *                                                            *    00070000
      *        TRANSACTION MU21                                    *    00080000
      *                                                            *    00090000
      *------------------------------ ZONE DONNEES TMU21---2734----*    00110000
      *                                                                 00720200
           02 COMM-MU21-APPLI   REDEFINES COMM-GS50-APPLI.              00721000
      *                                                                 00721100
      *------------------------------ ZONE DONNEES TMU21                00741200
      *                                                                 00741300
AA1115        03 COMM-MU21-CROSSDOCK  PIC X(1).                         00743004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MU21-ITEM       PIC S9(4)    COMP.                00743004
      *--                                                                       
              03 COMM-MU21-ITEM       PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-MU21-POS-MAX    PIC S9(4)    COMP.                00743005
      *--                                                                       
              03 COMM-MU21-POS-MAX    PIC S9(4) COMP-5.                         
      *}                                                                        
              03 COMM-MU21-VENDABLE   PIC S9(7)    COMP-3.              00743006
              03 COMM-MU21-COTE       PIC S9(7)    COMP-3.              00743007
              03 COMM-MU21-TRANSIT    PIC S9(7)    COMP-3.              00743008
              03 COMM-MU21-PRET       PIC S9(7)    COMP-3.              00743009
              03 COMM-MU21-HS         PIC S9(7)    COMP-3.              00743010
              03 COMM-MU21-TOTAL      PIC S9(7)    COMP-3.              00743011
              03 COMM-MU21-LITIGE     PIC S9(7)    COMP-3.              00743012
              03 COMM-MU21-DISPONIBLE PIC S9(7)    COMP-3.              00743013
              03 COMM-MU21-RESDISPO   PIC S9(7)    COMP-3.              00743014
              03 COMM-MU21-RESFOUR    PIC S9(7)    COMP-3.              00743015
              03 COMM-MU21-RESCDE     PIC S9(7)    COMP-3.              00743016
AA1115        03 FILLER               PIC X(2682).                      00743020
AA1115*       03 FILLER               PIC X(2683).                      00743020
                                                                                
                                                                        00750000
