      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00414100
      * COMMAREA SPECIFIQUE PRG TGS06 (TGS00 -> MENU)    TR: GS00  *    00414201
      *               GESTION DES STOCKS ENTREPOT                  *    00414301
      *                                                                 00414400
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3544  00414502
      *                                                            *    00414600
      *        TRANSACTION GS06 : CONSULTATION DES STOCKS          *    00414701
      *                           MIS DE COTE                      *    00414801
      *                                                                 00414900
          02 COMM-GS06-APPLI REDEFINES COMM-GS00-APPLI.                 00415001
      *------------------------------ ZONE DONNEES TGS06                00415101
             03 COMM-GS06-DONNEES-TGS06.                                00416001
      *------------------------------ LIBELLE STATUT ASSORTIMENT        00417000
                04 COMM-GS06-LSTATASSOR        PIC X(20).               00418001
      *------------------------------ LIBELLE STATUT APPROVISIONNEMENT  00419000
                04 COMM-GS06-LSTATAPPRO        PIC X(20).               00420001
      *------------------------------ LIBELLE STATUT EXPOSITION         00430000
                04 COMM-GS06-LSTATEXPO         PIC X(20).               00440001
      *-------------------------- ZONE CODICS PAGE                      00734000
                04 COMM-GS06-LIGCODICS         OCCURS 11.               00735001
                   05 COMM-GS06-NCODIC         PIC X(07).               00736001
                   05 COMM-GS06-CMARQ          PIC X(05).               00736101
                   05 COMM-GS06-LREFFOURN      PIC X(20).               00737001
                   05 COMM-GS06-LSTATCOMP      PIC X(03).               00737101
                   05 COMM-GS06-ZONSTOCK.                               00738001
                      06 COMM-GS06-QSTOCK1     PIC 9(04) OCCURS 4.      00738101
                      06 COMM-GS06-QSTOCK2     PIC 9(03) OCCURS 4.      00738201
      *------------------------------ TABLE N� TS PR PAGINATION         00739000
                04 COMM-GS06-TABART         OCCURS 100.                 00740001
      *------------------------------ 1ERS IND TS DES PAGES <=> CODIC   00740100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-GS06-NUMTS          PIC S9(4) COMP.          00740201
      *--                                                                       
                   05 COMM-GS06-NUMTS          PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ INDICATEUR FIN DE TABLE           00740300
                04 COMM-GS06-INDPAG            PIC 9.                   00740401
      *------------------------------ DERNIER IND-TS                    00740500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS06-PAGSUI            PIC S9(4) COMP.          00740601
      *--                                                                       
                04 COMM-GS06-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN  IND-TS                    00740700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS06-PAGENC            PIC S9(4) COMP.          00740801
      *--                                                                       
                04 COMM-GS06-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00740900
                04 COMM-GS06-NUMPAG            PIC 9(3).                00741001
      *------------------------------ IND POUR RECH DS TS               00741100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS06-IND-RECH-TS       PIC S9(4) COMP.          00741201
      *--                                                                       
                04 COMM-GS06-IND-RECH-TS       PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ IND TS MAX                        00741300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS06-IND-TS-MAX        PIC S9(4) COMP.          00741401
      *--                                                                       
                04 COMM-GS06-IND-TS-MAX        PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ZONES ATTRIBUTS POUR SWAP         00741500
             03 COMM-GS06-ZONSWAP.                                      00741701
                04 COMM-GS06-ATTR              PIC 9 OCCURS 500.        00741801
      ***************************************************************** 02180000
                                                                                
