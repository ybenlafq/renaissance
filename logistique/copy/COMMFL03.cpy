      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TFL03                    TR: FL03  *    00020000
      *            DEFINITION DES DONNEES FAMILLES/DEPOT           *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00050000
      *                                                                 00060000
          02 COMM-FL03-APPLI REDEFINES COMM-GA00-APPLI.                 00070015
      *------------------------------ CODE FAMILLE                      00080003
             03 COMM-FL03-CFAM           PIC X(5).                      00090015
      *------------------------------ CODE SOCIETE                      00100000
             03 COMM-FL03-NSOC           PIC X(3).                      00110015
      *------------------------------ CODE DEPOT                        00120000
             03 COMM-FL03-NDEPOT         PIC X(3).                      00130015
      *------------------------------ LIBELLE DEPOT                     00140009
             03 COMM-FL03-LDEPOT         PIC X(20).                     00150015
      *------------------------------ N� PAGE COURANTE                  00160019
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FL03-PAGE           PIC S9(4) COMP.                00170024
      *--                                                                       
             03 COMM-FL03-PAGE           PIC S9(4) COMP-5.                      
      *}                                                                        
      *------------------------------ N� PAGE MAXIMUM                   00180007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-FL03-PAGE-MAX       PIC S9(4) COMP.                00190024
      *--                                                                       
             03 COMM-FL03-PAGE-MAX       PIC S9(4) COMP-5.                      
      *}                                                                        
      *------------------------------ FLAG DE MAJ OU CRE                00200005
             03 COMM-FL03-FLAG           PIC X(3).                      00210015
      *------------------------------ INDICE N� FAMILLE AYANT UN PB     00220016
             03 COMM-FL03-IND-FAM        PIC 9(2).                      00230016
      *------------------------------ INDICE N� COLONNE AYANT UN PB     00240016
             03 COMM-FL03-IND-COL        PIC 9(2).                      00250016
      *------------------------------ TOUCHE PF                         00260010
             03 COMM-FL03-TOUCHE-PF      PIC X(5).                      00270015
      *------------------------------ LIBELLE D'ERREUR                  00280013
             03 COMM-FL03-LIBERR         PIC X(78).                     00290015
      *------------------------------ DEPOT GESTION PALETTE                     
             03 COMM-FL03-DEPOTPAL       PIC X(1).                              
      ***************************************************************** 00300000
                                                                                
