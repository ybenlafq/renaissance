      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGE3500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGE3500                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGE3500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGE3500.                                                            
      *}                                                                        
           02  GE35-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GE35-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GE35-CALLEE                                                      
               PIC X(0002).                                                     
           02  GE35-CNIVEAU                                                     
               PIC X(0002).                                                     
           02  GE35-NPOSITION                                                   
               PIC X(0003).                                                     
           02  GE35-NCODIC                                                      
               PIC X(0007).                                                     
           02  GE35-QSTOCK                                                      
               PIC S9(5) COMP-3.                                                
           02  GE35-DATTR                                                       
               PIC X(0008).                                                     
           02  GE35-QSTOCKRES                                                   
               PIC S9(5) COMP-3.                                                
           02  GE35-NORDRE                                                      
               PIC S9(7) COMP-3.                                                
           02  GE35-NPRIORITE                                                   
               PIC X(0001).                                                     
           02  GE35-NRUPT                                                       
               PIC S9(5) COMP-3.                                                
           02  GE35-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGE3500                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGE3500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGE3500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-CALLEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-CALLEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-CNIVEAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-CNIVEAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-NPOSITION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-NPOSITION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-QSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-QSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-DATTR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-DATTR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-QSTOCKRES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-QSTOCKRES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-NPRIORITE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-NPRIORITE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-NRUPT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE35-NRUPT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE35-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GE35-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
