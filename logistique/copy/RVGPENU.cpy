      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GPENU GROUPE DE NRM POUR IGS256        *        
      *----------------------------------------------------------------*        
       01  RVGPENU .                                                            
           05  GPENU-CTABLEG2    PIC X(15).                                     
           05  GPENU-CTABLEG2-REDEF REDEFINES GPENU-CTABLEG2.                   
               10  GPENU-CGROUP          PIC X(05).                             
           05  GPENU-WTABLEG     PIC X(80).                                     
           05  GPENU-WTABLEG-REDEF  REDEFINES GPENU-WTABLEG.                    
               10  GPENU-SOCREF          PIC X(03).                             
               10  GPENU-SOCREF-N       REDEFINES GPENU-SOCREF                  
                                         PIC 9(03).                             
               10  GPENU-NSEQ            PIC X(02).                             
               10  GPENU-NSEQ-N         REDEFINES GPENU-NSEQ                    
                                         PIC 9(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGPENU-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GPENU-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GPENU-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GPENU-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GPENU-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
