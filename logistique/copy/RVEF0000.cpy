      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVEF0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEF0000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEF0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEF0000.                                                            
      *}                                                                        
           02  EF00-CTRAIT                                                      
               PIC X(0005).                                                     
           02  EF00-NENVOI                                                      
               PIC X(0007).                                                     
           02  EF00-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  EF00-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  EF00-NORIGINE                                                    
               PIC X(0007).                                                     
           02  EF00-NCODIC                                                      
               PIC X(0007).                                                     
           02  EF00-CTIERS                                                      
               PIC X(0005).                                                     
           02  EF00-DENVOI                                                      
               PIC X(0008).                                                     
           02  EF00-NSERIE                                                      
               PIC X(0016).                                                     
           02  EF00-NACCORD                                                     
               PIC X(0012).                                                     
           02  EF00-DACCORD                                                     
               PIC X(0008).                                                     
           02  EF00-LNOMACCORD                                                  
               PIC X(0010).                                                     
           02  EF00-QTENV                                                       
               PIC S9(5) COMP-3.                                                
           02  EF00-QTRDU                                                       
               PIC S9(5) COMP-3.                                                
           02  EF00-CGARANTIE                                                   
               PIC X(0005).                                                     
           02  EF00-CRENDU                                                      
               PIC X(0005).                                                     
           02  EF00-PABASEFACT                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  EF00-MTPROVSAV                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  EF00-NSOCMVTO                                                    
               PIC X(0003).                                                     
           02  EF00-NLIEUMVTO                                                   
               PIC X(0003).                                                     
           02  EF00-NSSLIEUMVTO                                                 
               PIC X(0003).                                                     
           02  EF00-CLIEUTRTMVTO                                                
               PIC X(0005).                                                     
           02  EF00-NSOCMVTD                                                    
               PIC X(0003).                                                     
           02  EF00-NLIEUMVTD                                                   
               PIC X(0003).                                                     
           02  EF00-NSSLIEUMVTD                                                 
               PIC X(0003).                                                     
           02  EF00-CLIEUTRTMVTD                                                
               PIC X(0005).                                                     
           02  EF00-D1RELANC                                                    
               PIC X(0008).                                                     
           02  EF00-D2RELANC                                                    
               PIC X(0008).                                                     
           02  EF00-D3RELANC                                                    
               PIC X(0008).                                                     
           02  EF00-DANNUL                                                      
               PIC X(0008).                                                     
           02  EF00-DMVT                                                        
               PIC X(0008).                                                     
           02  EF00-WSOLD                                                       
               PIC X(0001).                                                     
           02  EF00-LCOMMENT                                                    
               PIC X(0015).                                                     
           02  EF00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEF0000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEF0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEF0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-CTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-CTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NENVOI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NENVOI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-CTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-CTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-DENVOI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-DENVOI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NSERIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NSERIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-DACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-DACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-LNOMACCORD-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-LNOMACCORD-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-QTENV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-QTENV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-QTRDU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-QTRDU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-CGARANTIE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-CGARANTIE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-CRENDU-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-CRENDU-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-PABASEFACT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-PABASEFACT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-MTPROVSAV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-MTPROVSAV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NSOCMVTO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NSOCMVTO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NLIEUMVTO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NLIEUMVTO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NSSLIEUMVTO-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NSSLIEUMVTO-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-CLIEUTRTMVTO-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-CLIEUTRTMVTO-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NSOCMVTD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NSOCMVTD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NLIEUMVTD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NLIEUMVTD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-NSSLIEUMVTD-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-NSSLIEUMVTD-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-CLIEUTRTMVTD-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-CLIEUTRTMVTD-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-D1RELANC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-D1RELANC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-D2RELANC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-D2RELANC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-D3RELANC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-D3RELANC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-DMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-DMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-WSOLD-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-WSOLD-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EF00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EF00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
