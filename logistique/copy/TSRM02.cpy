      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM02                                          *  00020000
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 61.              00080000
       01  TS-DONNEES.                                                  00090000
              10 TS-CFAM         PIC X(5).                              00120000
              10 TS-LFAM         PIC X(20).                             00120100
              10 TS-MW20         PIC X.                                 00121000
              10 TS-MW80         PIC X.                                 00121100
              10 TS-MWTXEMP      PIC X.                                 00122000
              10 TS-MQSEUIL      PIC X(6).                              00122100
              10 TS-DEFFETD      PIC X(10).                             00122200
              10 TS-DFINEFFETD   PIC X(10).                             00122300
              10 TS-INSERT       PIC X.                                 00123000
              10 TS-MODIF        PIC X(5).                              00124000
              10 TS-WEXCEP       PIC X.                                 00125000
                                                                                
