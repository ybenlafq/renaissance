      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IFL020 AU 25/07/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,03,BI,A,                          *        
      *                           24,03,BI,A,                          *        
      *                           27,08,BI,A,                          *        
      *                           35,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IFL020.                                                        
            05 NOMETAT-IFL020           PIC X(6) VALUE 'IFL020'.                
            05 RUPTURES-IFL020.                                                 
           10 IFL020-NSOCGRP            PIC X(03).                      007  003
           10 IFL020-NSOCENTR           PIC X(03).                      010  003
           10 IFL020-NDEPOT             PIC X(03).                      013  003
           10 IFL020-CSELART            PIC X(05).                      016  005
           10 IFL020-NSOCIETE           PIC X(03).                      021  003
           10 IFL020-NLIEU              PIC X(03).                      024  003
           10 IFL020-DMUTATION          PIC X(08).                      027  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IFL020-SEQUENCE           PIC S9(04) COMP.                035  002
      *--                                                                       
           10 IFL020-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IFL020.                                                   
           10 IFL020-LLIEU              PIC X(20).                      037  020
           10 IFL020-LSOCENTR           PIC X(20).                      057  020
           10 IFL020-LSOCGRP            PIC X(20).                      077  020
           10 IFL020-NMUTATION          PIC X(07).                      097  007
           10 IFL020-QNBPIECES          PIC S9(05)      COMP-3.         104  003
           10 IFL020-QNBPIECESPRMP      PIC S9(08)V9(2) COMP-3.         107  006
           10 IFL020-DDEBUT             PIC X(08).                      113  008
           10 IFL020-DFIN               PIC X(08).                      121  008
            05 FILLER                      PIC X(384).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IFL020-LONG           PIC S9(4)   COMP  VALUE +128.           
      *                                                                         
      *--                                                                       
        01  DSECT-IFL020-LONG           PIC S9(4) COMP-5  VALUE +128.           
                                                                                
      *}                                                                        
