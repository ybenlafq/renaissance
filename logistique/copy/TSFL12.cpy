      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TFL12                                          *  00020017
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG              PIC S9(3) COMP-3 VALUE 637.             00080024
       01  TS-RECORD.                                                   00090022
         05  TS-LIGNE         OCCURS 13.                                00100022
           10 TS-NSOCIETE     PIC X(3).                                 00120319
           10 TS-NLIEU        PIC X(3).                                 00120419
           10 TS-CTYPTRAIT    PIC X(5).                                 00120519
           10 TS-CPROAFF      PIC X(5).                                 00120619
           10 TS-LPROAFF      PIC X(20).                                00121019
           10 TS-DEFFET       PIC X(10).                                00121120
           10 TS-SUPP         PIC X.                                    00121221
           10 TS-MAJ-FL05     PIC X.                                    00121319
           10 TS-SUP-FL05     PIC X.                                    00121423
      *    10 TS-FILLER       PIC X(02).                                00126019
                                                                                
