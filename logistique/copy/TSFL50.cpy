      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TFL50                                          *  00020001
      *    CETTE TS CORRESPOND A LA DSECT DE LA TABLE FL50              00030001
      *                                                                 00040001
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG              PIC S9(3) COMP-3 VALUE +472.            00080001
       01  TS-DONNEES.                                                  00090001
           02  TS-DONNEES-AVANT.                                        00100001
               03 TS-NSOCDEPOT-AV                                       00110001
                   PIC X(0003).                                         00120001
               03 TS-NDEPOT-AV                                          00130001
                   PIC X(0003).                                         00140001
               03 TS-NCODIC-AV                                          00150001
                   PIC X(0007).                                         00160001
               03 TS-D1RECEPT-AV                                        00170001
                   PIC X(0008).                                         00180001
               03 TS-CAPPRO-AV                                          00190001
                   PIC X(0005).                                         00200001
               03 TS-WSENSAPPRO-AV                                      00210001
                   PIC X(0001).                                         00220001
               03 TS-QDELAIAPPRO-AV                                     00230001
                   PIC X(0003).                                         00240001
               03 TS-QCOLICDE-AV                                        00250001
                   PIC S9(3) COMP-3.                                    00260001
               03 TS-LEMBALLAGE-AV                                      00270001
                   PIC X(0050).                                         00280001
               03 TS-QCOLIRECEPT-AV                                     00290001
                   PIC S9(5) COMP-3.                                    00300001
               03 TS-QCOLIDSTOCK-AV                                     00310001
                   PIC S9(5) COMP-3.                                    00320001
               03 TS-CMODSTOCK1-AV                                      00330001
                   PIC X(0005).                                         00340001
               03 TS-WMODSTOCK1-AV                                      00350001
                   PIC X(0001).                                         00360001
               03 TS-CMODSTOCK2-AV                                      00370001
                   PIC X(0005).                                         00380001
               03 TS-WMODSTOCK2-AV                                      00390001
                   PIC X(0001).                                         00400001
               03 TS-CMODSTOCK3-AV                                      00410001
                   PIC X(0005).                                         00420001
               03 TS-WMODSTOCK3-AV                                      00430001
                   PIC X(0001).                                         00440001
               03 TS-CFETEMPL-AV                                        00450001
                   PIC X(0001).                                         00460001
               03 TS-QNBRANMAIL-AV                                      00470001
                   PIC S9(3) COMP-3.                                    00480001
               03 TS-QNBNIVGERB-AV                                      00490001
                   PIC S9(3) COMP-3.                                    00500001
               03 TS-CZONEACCES-AV                                      00510001
                   PIC X(0001).                                         00520001
               03 TS-CLASSE-AV                                          00521001
                   PIC X(0001).                                         00522001
               03 TS-CCONTENEUR-AV                                      00530001
                   PIC X(0001).                                         00540001
               03 TS-CSPECIFSTK-AV                                      00550001
                   PIC X(0001).                                         00560001
               03 TS-CCOTEHOLD-AV                                       00570001
                   PIC X(0001).                                         00580001
               03 TS-QNBPVSOL-AV                                        00590001
                   PIC S9(3) COMP-3.                                    00600001
               03 TS-QNBPRACK-AV                                        00610001
                   PIC S9(5) COMP-3.                                    00620001
               03 TS-CTPSUP-AV                                          00630001
                   PIC X(0001).                                         00640001
               03 TS-WRESFOURN-AV                                       00650001
                   PIC X(0001).                                         00660001
               03 TS-CQUOTA-AV                                          00670001
                   PIC X(0005).                                         00680001
               03 TS-CUSINE-AV                                          00690001
                   PIC X(0005).                                         00700001
               03 TS-DMAJ-AV                                            00710001
                   PIC X(0008).                                         00720001
               03 TS-CEXPO-AV                                           00730001
                   PIC X(0005).                                         00740001
               03 TS-LSTATCOMP-AV                                       00750001
                   PIC X(0003).                                         00760001
               03 TS-WSTOCKAVANCE-AV                                    00770001
                   PIC X(0001).                                         00780001
      * LIBELLES                                                        00790001
               03 TS-LUSINE-AV    PIC X(20).                            00800001
               03 TS-LAPPRO-AV    PIC X(20).                            00810001
               03 TS-LEXPO-AV     PIC X(20).                            00820001
               03 TS-LQUOTA-AV    PIC X(20).                            00830001
      *                                                                 00840001
           02 TS-DONNEES-APRES.                                         00850001
               03 TS-NSOCDEPOT-AP                                       00860001
                   PIC X(0003).                                         00870001
               03 TS-NDEPOT-AP                                          00880001
                   PIC X(0003).                                         00890001
               03 TS-NCODIC-AP                                          00900001
                   PIC X(0007).                                         00910001
               03 TS-D1RECEPT-AP                                        00920001
                   PIC X(0008).                                         00930001
               03 TS-CAPPRO-AP                                          00940001
                   PIC X(0005).                                         00950001
               03 TS-WSENSAPPRO-AP                                      00960001
                   PIC X(0001).                                         00970001
               03 TS-QDELAIAPPRO-AP                                     00980001
                   PIC X(0003).                                         00990001
               03 TS-QCOLICDE-AP                                        01000001
                   PIC S9(3) COMP-3.                                    01010001
               03 TS-LEMBALLAGE-AP                                      01020001
                   PIC X(0050).                                         01030001
               03 TS-QCOLIRECEPT-AP                                     01040001
                   PIC S9(5) COMP-3.                                    01050001
               03 TS-QCOLIDSTOCK-AP                                     01060001
                   PIC S9(5) COMP-3.                                    01070001
               03 TS-CMODSTOCK1-AP                                      01080001
                   PIC X(0005).                                         01090001
               03 TS-WMODSTOCK1-AP                                      01100001
                   PIC X(0001).                                         01110001
               03 TS-CMODSTOCK2-AP                                      01120001
                   PIC X(0005).                                         01130001
               03 TS-WMODSTOCK2-AP                                      01140001
                   PIC X(0001).                                         01150001
               03 TS-CMODSTOCK3-AP                                      01160001
                   PIC X(0005).                                         01170001
               03 TS-WMODSTOCK3-AP                                      01180001
                   PIC X(0001).                                         01190001
               03 TS-CFETEMPL-AP                                        01200001
                   PIC X(0001).                                         01210001
               03 TS-QNBRANMAIL-AP                                      01220001
                   PIC S9(3) COMP-3.                                    01230001
               03 TS-QNBNIVGERB-AP                                      01240001
                   PIC S9(3) COMP-3.                                    01250001
               03 TS-CZONEACCES-AP                                      01260001
                   PIC X(0001).                                         01271001
               03 TS-CLASSE-AP                                          01272001
                   PIC X(0001).                                         01273001
               03 TS-CCONTENEUR-AP                                      01280001
                   PIC X(0001).                                         01290001
               03 TS-CSPECIFSTK-AP                                      01300001
                   PIC X(0001).                                         01310001
               03 TS-CCOTEHOLD-AP                                       01320001
                   PIC X(0001).                                         01330001
               03 TS-QNBPVSOL-AP                                        01340001
                   PIC S9(3) COMP-3.                                    01350001
               03 TS-QNBPRACK-AP                                        01360001
                   PIC S9(5) COMP-3.                                    01370001
               03 TS-CTPSUP-AP                                          01380001
                   PIC X(0001).                                         01390001
               03 TS-WRESFOURN-AP                                       01400001
                   PIC X(0001).                                         01410001
               03 TS-CQUOTA-AP                                          01420001
                   PIC X(0005).                                         01430001
               03 TS-CUSINE-AP                                          01440001
                   PIC X(0005).                                         01450001
               03 TS-DMAJ-AP                                            01460001
                   PIC X(0008).                                         01470001
               03 TS-CEXPO-AP                                           01480001
                   PIC X(0005).                                         01490001
               03 TS-LSTATCOMP-AP                                       01500001
                   PIC X(0003).                                         01510001
               03 TS-WSTOCKAVANCE-AP                                    01520001
                   PIC X(0001).                                         01530001
      * LIBELLES                                                        01540001
               03 TS-LUSINE-AP    PIC X(20).                            01550001
               03 TS-LAPPRO-AP    PIC X(20).                            01560001
               03 TS-LEXPO-AP     PIC X(20).                            01570001
               03 TS-LQUOTA-AP    PIC X(20).                            01580001
                                                                                
