      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV00   EAV00                                              00000020
      ***************************************************************** 00000030
       01   ETL16I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * zone de selection                                               00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000150
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MZONCMDI  PIC X(15).                                      00000180
      * date saisie                                                     00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEINL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MDATEINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATEINF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MDATEINI  PIC X(10).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDHHINL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MDHHINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDHHINF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MDHHINI   PIC X(2).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMMINL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MDMMINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDMMINF   PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MDMMINI   PIC X(2).                                       00000310
      * mode de delivrance saisie                                       00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODDELINL    COMP PIC S9(4).                            00000330
      *--                                                                       
           02 MCMODDELINL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCMODDELINF    PIC X.                                     00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MCMODDELINI    PIC X(3).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFILINL    COMP PIC S9(4).                            00000370
      *--                                                                       
           02 MCPROFILINL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCPROFILINF    PIC X.                                     00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MCPROFILINI    PIC X(5).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCIMPRINL      COMP PIC S9(4).                            00000410
      *--                                                                       
           02 MCIMPRINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCIMPRINF      PIC X.                                     00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MCIMPRINI      PIC X(4).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MLIBERRI  PIC X(78).                                      00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MCODTRAI  PIC X(4).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MCICSI    PIC X(5).                                       00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MNETNAMI  PIC X(8).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MSCREENI  PIC X(4).                                       00000640
      ***************************************************************** 00000650
      * SDF: EAV00   EAV00                                              00000660
      ***************************************************************** 00000670
       01   ETL16O REDEFINES ETL16I.                                    00000680
           02 FILLER    PIC X(12).                                      00000690
           02 FILLER    PIC X(2).                                       00000700
           02 MDATJOUA  PIC X.                                          00000710
           02 MDATJOUC  PIC X.                                          00000720
           02 MDATJOUP  PIC X.                                          00000730
           02 MDATJOUH  PIC X.                                          00000740
           02 MDATJOUV  PIC X.                                          00000750
           02 MDATJOUO  PIC X(10).                                      00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MTIMJOUA  PIC X.                                          00000780
           02 MTIMJOUC  PIC X.                                          00000790
           02 MTIMJOUP  PIC X.                                          00000800
           02 MTIMJOUH  PIC X.                                          00000810
           02 MTIMJOUV  PIC X.                                          00000820
           02 MTIMJOUO  PIC X(5).                                       00000830
      * zone de selection                                               00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MZONCMDA  PIC X.                                          00000860
           02 MZONCMDC  PIC X.                                          00000870
           02 MZONCMDP  PIC X.                                          00000880
           02 MZONCMDH  PIC X.                                          00000890
           02 MZONCMDV  PIC X.                                          00000900
           02 MZONCMDO  PIC X(15).                                      00000910
      * date saisie                                                     00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MDATEINA  PIC X.                                          00000940
           02 MDATEINC  PIC X.                                          00000950
           02 MDATEINP  PIC X.                                          00000960
           02 MDATEINH  PIC X.                                          00000970
           02 MDATEINV  PIC X.                                          00000980
           02 MDATEINO  PIC X(10).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDHHINA   PIC X.                                          00001010
           02 MDHHINC   PIC X.                                          00001020
           02 MDHHINP   PIC X.                                          00001030
           02 MDHHINH   PIC X.                                          00001040
           02 MDHHINV   PIC X.                                          00001050
           02 MDHHINO   PIC X(2).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MDMMINA   PIC X.                                          00001080
           02 MDMMINC   PIC X.                                          00001090
           02 MDMMINP   PIC X.                                          00001100
           02 MDMMINH   PIC X.                                          00001110
           02 MDMMINV   PIC X.                                          00001120
           02 MDMMINO   PIC X(2).                                       00001130
      * mode de delivrance saisie                                       00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MCMODDELINA    PIC X.                                     00001160
           02 MCMODDELINC    PIC X.                                     00001170
           02 MCMODDELINP    PIC X.                                     00001180
           02 MCMODDELINH    PIC X.                                     00001190
           02 MCMODDELINV    PIC X.                                     00001200
           02 MCMODDELINO    PIC X(3).                                  00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MCPROFILINA    PIC X.                                     00001230
           02 MCPROFILINC    PIC X.                                     00001240
           02 MCPROFILINP    PIC X.                                     00001250
           02 MCPROFILINH    PIC X.                                     00001260
           02 MCPROFILINV    PIC X.                                     00001270
           02 MCPROFILINO    PIC X(5).                                  00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MCIMPRINA      PIC X.                                     00001300
           02 MCIMPRINC PIC X.                                          00001310
           02 MCIMPRINP PIC X.                                          00001320
           02 MCIMPRINH PIC X.                                          00001330
           02 MCIMPRINV PIC X.                                          00001340
           02 MCIMPRINO      PIC X(4).                                  00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MLIBERRA  PIC X.                                          00001370
           02 MLIBERRC  PIC X.                                          00001380
           02 MLIBERRP  PIC X.                                          00001390
           02 MLIBERRH  PIC X.                                          00001400
           02 MLIBERRV  PIC X.                                          00001410
           02 MLIBERRO  PIC X(78).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MCODTRAA  PIC X.                                          00001440
           02 MCODTRAC  PIC X.                                          00001450
           02 MCODTRAP  PIC X.                                          00001460
           02 MCODTRAH  PIC X.                                          00001470
           02 MCODTRAV  PIC X.                                          00001480
           02 MCODTRAO  PIC X(4).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCICSA    PIC X.                                          00001510
           02 MCICSC    PIC X.                                          00001520
           02 MCICSP    PIC X.                                          00001530
           02 MCICSH    PIC X.                                          00001540
           02 MCICSV    PIC X.                                          00001550
           02 MCICSO    PIC X(5).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MNETNAMA  PIC X.                                          00001580
           02 MNETNAMC  PIC X.                                          00001590
           02 MNETNAMP  PIC X.                                          00001600
           02 MNETNAMH  PIC X.                                          00001610
           02 MNETNAMV  PIC X.                                          00001620
           02 MNETNAMO  PIC X(8).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MSCREENA  PIC X.                                          00001650
           02 MSCREENC  PIC X.                                          00001660
           02 MSCREENP  PIC X.                                          00001670
           02 MSCREENH  PIC X.                                          00001680
           02 MSCREENV  PIC X.                                          00001690
           02 MSCREENO  PIC X(4).                                       00001700
                                                                                
