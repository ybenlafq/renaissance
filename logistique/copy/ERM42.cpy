      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM02   ERM02                                              00000020
      ***************************************************************** 00000030
       01   ERM42I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEMAXL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNPAGEMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNPAGEMAXF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEMAXI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMRL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFAMRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMRF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFAMRI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAMRL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLFAMRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFAMRF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLFAMRI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREGRL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MTYPREGRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREGRF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPREGRI      PIC X(2).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCRL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSOCRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCRF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCRI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGRL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNMAGRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMAGRF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNMAGRI   PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGRL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLMAGRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMAGRF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLMAGRI   PIC X(20).                                      00000450
           02 LIGNEI OCCURS   13 TIMES .                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNSOCI  PIC X(3).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNMAGI  PIC X(3).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMAGL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MLMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLMAGF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLMAGI  PIC X(20).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPREGL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MTYPREGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPREGF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MTYPREGI     PIC X(2).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(80).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * SDF: ERM02   ERM02                                              00000840
      ***************************************************************** 00000850
       01   ERM42O REDEFINES ERM42I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNPAGEA   PIC X.                                          00001030
           02 MNPAGEC   PIC X.                                          00001040
           02 MNPAGEP   PIC X.                                          00001050
           02 MNPAGEH   PIC X.                                          00001060
           02 MNPAGEV   PIC X.                                          00001070
           02 MNPAGEO   PIC X(3).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MNPAGEMAXA     PIC X.                                     00001100
           02 MNPAGEMAXC     PIC X.                                     00001110
           02 MNPAGEMAXP     PIC X.                                     00001120
           02 MNPAGEMAXH     PIC X.                                     00001130
           02 MNPAGEMAXV     PIC X.                                     00001140
           02 MNPAGEMAXO     PIC X(3).                                  00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MCFAMRA   PIC X.                                          00001170
           02 MCFAMRC   PIC X.                                          00001180
           02 MCFAMRP   PIC X.                                          00001190
           02 MCFAMRH   PIC X.                                          00001200
           02 MCFAMRV   PIC X.                                          00001210
           02 MCFAMRO   PIC X(5).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MLFAMRA   PIC X.                                          00001240
           02 MLFAMRC   PIC X.                                          00001250
           02 MLFAMRP   PIC X.                                          00001260
           02 MLFAMRH   PIC X.                                          00001270
           02 MLFAMRV   PIC X.                                          00001280
           02 MLFAMRO   PIC X(20).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MTYPREGRA      PIC X.                                     00001310
           02 MTYPREGRC PIC X.                                          00001320
           02 MTYPREGRP PIC X.                                          00001330
           02 MTYPREGRH PIC X.                                          00001340
           02 MTYPREGRV PIC X.                                          00001350
           02 MTYPREGRO      PIC X(2).                                  00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MNSOCRA   PIC X.                                          00001380
           02 MNSOCRC   PIC X.                                          00001390
           02 MNSOCRP   PIC X.                                          00001400
           02 MNSOCRH   PIC X.                                          00001410
           02 MNSOCRV   PIC X.                                          00001420
           02 MNSOCRO   PIC X(3).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNMAGRA   PIC X.                                          00001450
           02 MNMAGRC   PIC X.                                          00001460
           02 MNMAGRP   PIC X.                                          00001470
           02 MNMAGRH   PIC X.                                          00001480
           02 MNMAGRV   PIC X.                                          00001490
           02 MNMAGRO   PIC X(3).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MLMAGRA   PIC X.                                          00001520
           02 MLMAGRC   PIC X.                                          00001530
           02 MLMAGRP   PIC X.                                          00001540
           02 MLMAGRH   PIC X.                                          00001550
           02 MLMAGRV   PIC X.                                          00001560
           02 MLMAGRO   PIC X(20).                                      00001570
           02 LIGNEO OCCURS   13 TIMES .                                00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MNSOCA  PIC X.                                          00001600
             03 MNSOCC  PIC X.                                          00001610
             03 MNSOCP  PIC X.                                          00001620
             03 MNSOCH  PIC X.                                          00001630
             03 MNSOCV  PIC X.                                          00001640
             03 MNSOCO  PIC X(3).                                       00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MNMAGA  PIC X.                                          00001670
             03 MNMAGC  PIC X.                                          00001680
             03 MNMAGP  PIC X.                                          00001690
             03 MNMAGH  PIC X.                                          00001700
             03 MNMAGV  PIC X.                                          00001710
             03 MNMAGO  PIC X(3).                                       00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MLMAGA  PIC X.                                          00001740
             03 MLMAGC  PIC X.                                          00001750
             03 MLMAGP  PIC X.                                          00001760
             03 MLMAGH  PIC X.                                          00001770
             03 MLMAGV  PIC X.                                          00001780
             03 MLMAGO  PIC X(20).                                      00001790
             03 FILLER       PIC X(2).                                  00001800
             03 MTYPREGA     PIC X.                                     00001810
             03 MTYPREGC     PIC X.                                     00001820
             03 MTYPREGP     PIC X.                                     00001830
             03 MTYPREGH     PIC X.                                     00001840
             03 MTYPREGV     PIC X.                                     00001850
             03 MTYPREGO     PIC X(2).                                  00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLIBERRA  PIC X.                                          00001880
           02 MLIBERRC  PIC X.                                          00001890
           02 MLIBERRP  PIC X.                                          00001900
           02 MLIBERRH  PIC X.                                          00001910
           02 MLIBERRV  PIC X.                                          00001920
           02 MLIBERRO  PIC X(80).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
