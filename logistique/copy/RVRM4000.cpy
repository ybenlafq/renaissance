      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM4000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM4000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM4000.                                                            
           02  RM40-DSIMULATION                                                 
               PIC X(0008).                                                     
           02  RM40-NSIMULATION                                                 
               PIC X(0003).                                                     
           02  RM40-NCODIC                                                      
               PIC X(0007).                                                     
           02  RM40-CREPARTITION                                                
               PIC X(0001).                                                     
           02  RM40-NCODICM                                                     
               PIC X(0007).                                                     
           02  RM40-QSP                                                         
               PIC S9(7) COMP-3.                                                
           02  RM40-QTE1                                                        
               PIC S9(7) COMP-3.                                                
           02  RM40-QTE2                                                        
               PIC S9(7) COMP-3.                                                
           02  RM40-QTE3                                                        
               PIC S9(7) COMP-3.                                                
           02  RM40-QSA1                                                        
               PIC S9(7) COMP-3.                                                
           02  RM40-QSA2                                                        
               PIC S9(7) COMP-3.                                                
           02  RM40-QSA3                                                        
               PIC S9(7) COMP-3.                                                
           02  RM40-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM4000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM4000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-DSIMULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-DSIMULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-NSIMULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-NSIMULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-CREPARTITION-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-CREPARTITION-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-NCODICM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-NCODICM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-QSP-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-QSP-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-QTE1-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-QTE1-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-QTE2-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-QTE2-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-QTE3-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-QTE3-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-QSA1-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-QSA1-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-QSA2-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-QSA2-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-QSA3-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-QSA3-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM40-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM40-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
