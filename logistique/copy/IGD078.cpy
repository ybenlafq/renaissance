      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ** AIDA ************************************************* IRDF **         
      *  DESCRIPTION DE L'ETAT IGD078                                           
      *****************************************************************         
      *                                                                         
       01  IGD078.                                                              
      *****************************************************************         
      *  ENTETE PHYSIQUE DE PAGE                                                
      *****************************************************************         
      *****************************************************************         
          02 HENTET01.                                                          
            04 FILLER PIC X(20) VALUE '    I G D 0 7 8     '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '    E T A B L I S S '.                   
            04 FILLER PIC X(20) VALUE 'E M E N T S    D A R'.                   
            04 FILLER PIC X(20) VALUE ' T Y                '.                   
            04 FILLER PIC X(20) VALUE ' E D I T E    L E   '.                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 H01-DATE PIC X(10).                                              
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 HENTET02.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 HENTET03.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '         LISTE DES A'.                   
            04 FILLER PIC X(20) VALUE 'PPAREILS A COMMUTER '.                   
            04 FILLER PIC X(17) VALUE 'DANS UNE MUTATION'.                      
            04 FILLER PIC X(10) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(6) VALUE 'PAGE  '.                                  
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 H03-NPAGE PIC ZZZ9.                                              
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 HENTET04.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 HENTET05.                                                          
            04 FILLER PIC X(20) VALUE '    ENTREPOT        '.                   
            04 FILLER PIC X(2) VALUE ': '.                                      
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 H05-NSOCDE PIC X(3).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(1) VALUE SPACES.                                    
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 H05-NDEPOT PIC X(3).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(18) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 HENTET06.                                                          
            04 FILLER PIC X(20) VALUE '    JOURNEE         '.                   
            04 FILLER PIC X(2) VALUE ': '.                                      
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 H06-DRAFAL PIC X(8).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(19) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 HENTET07.                                                          
            04 FILLER PIC X(20) VALUE '    NO RAFALE       '.                   
            04 FILLER PIC X(2) VALUE ': '.                                      
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 H07-NRAFAL PIC X(8).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(19) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 HENTET08.                                                          
            04 FILLER PIC X(20) VALUE '    HEURE LANCEMENT '.                   
            04 FILLER PIC X(2) VALUE ': '.                                      
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 H08-DHSAIS PIC X(2).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(1) VALUE SPACES.                                    
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 H08-DMSAIS PIC X(2).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 HENTET08BIS PIC X(132)  VALUE SPACES.                              
          02 HENTET08TER.                                                       
            04 FILLER PIC X(20) VALUE '    SATELLITE       '.                   
            04 FILLER PIC X(2)  VALUE ': '.                                     
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 H08-SATELL PIC X(2).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(1)  VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(2)  VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 HENTET09.                                                          
            04 FILLER PIC X(20) VALUE '   +----------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(12) VALUE '-----------+'.                           
      *****************************************************************         
          02 HENTET10.                                                          
            04 FILLER PIC X(22) VALUE '   ! CODIC  FAM  MARQU'.                 
            04 FILLER PIC X(20) VALUE 'E      REFERENCE    '.                   
            04 FILLER PIC X(20) VALUE '    !  DESTIN. ! SOC'.                   
            04 FILLER PIC X(26) VALUE ' MAG MUTATION  VTE   QTE  '.             
            04 FILLER PIC X(18) VALUE '!    COMMUTATIONS '.                     
            04 FILLER PIC X(19) VALUE 'A EFFECTUER   !EMPL'.                    
            04 FILLER PIC X(07) VALUE 'ACEME.!'.                                
      *****************************************************************         
          02 HENTET11.                                                          
            04 FILLER PIC X(20) VALUE '   !                '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '      !          !  '.                   
            04 FILLER PIC X(26) VALUE SPACES.                                   
            04 FILLER PIC X(18) VALUE '  ! 1     2     3 '.                     
            04 FILLER PIC X(19) VALUE '    4     5     !  '.                    
            04 FILLER PIC X(09) VALUE '        !'.                              
      *****************************************************************         
          02 HENTET12.                                                          
            04 FILLER PIC X(20) VALUE '   +----------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '------+----------+--'.                   
            04 FILLER PIC X(26) VALUE '--------------------------'.             
            04 FILLER PIC X(18) VALUE '--+---------------'.                     
            04 FILLER PIC X(19) VALUE '----------------+--'.                    
            04 FILLER PIC X(09) VALUE '--------+'.                              
      *****************************************************************         
          02 DETAIL01.                                                          
            04 FILLER PIC X(5) VALUE '   ! '.                                   
            04 D01-NCODIC PIC X(7).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-CFAM PIC X(5).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-CMARQ PIC X(5).                                              
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-LIBEL PIC X(20).                                             
            04 FILLER PIC X(3) VALUE ' ! '.                                     
            04 D01-NDEST PIC X(06).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(03) VALUE '  !'.                                    
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-NSOC PIC X(3).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-NMAG PIC X(3).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-NVENTE PIC X(7).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-VTE PIC X(07).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-QCDE PIC X(4).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(1) VALUE '!'.                                       
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-CARAC1 PIC X(5).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-CARAC2 PIC X(5).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-CARAC3 PIC X(5).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-CARAC4 PIC X(5).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 D01-CARAC5 PIC X(5).                                             
            04 FILLER PIC X(3) VALUE ' ! '.                                     
            04 D01-ZONE.                                                        
               05 D01-EMPL1 PIC X(02).                                          
               05 FILLER PIC X(01) VALUE SPACES.                                
               05 D01-EMPL2 PIC X(02).                                          
               05 FILLER PIC X(01) VALUE SPACES.                                
               05 D01-EMPL3 PIC X(03).                                          
            04 FILLER PIC X(1) VALUE '!'.                                       
      *****************************************************************         
          02 DETAIL02.                                                          
            04 FILLER PIC X(5) VALUE '   ! '.                                   
            04 D02-LEMBAL PIC X(40).                                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(26) VALUE '!          !              '.             
            04 FILLER PIC X(18) VALUE '                ! '.                     
            04 FILLER PIC X(20) VALUE '                    '.                   
            04 FILLER PIC X(18) VALUE '          !       '.                     
            04 FILLER PIC X(04) VALUE '   !'.                                   
      *****************************************************************         
          02 DETAIL03.                                                          
            04 FILLER PIC X(20) VALUE '   !                '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '      !          !  '.                   
            04 FILLER PIC X(26) VALUE SPACES.                                   
            04 FILLER PIC X(18) VALUE '  !               '.                     
            04 FILLER PIC X(19) VALUE '                !  '.                    
            04 FILLER PIC X(09) VALUE '        !'.                              
      *****************************************************************         
          02 FINPAG01.                                                          
            04 FILLER PIC X(20) VALUE '   +----------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '------+----------+--'.                   
            04 FILLER PIC X(26) VALUE '--------------------------'.             
            04 FILLER PIC X(18) VALUE '--+---------------'.                     
            04 FILLER PIC X(19) VALUE '----------------+--'.                    
            04 FILLER PIC X(09) VALUE '--------+'.                              
                                                                                
