      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU13   EMU13                                              00000020
      ***************************************************************** 00000030
       01   EMU13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCLIEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSOCLIEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCLIEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCLIEI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENTL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNSOCENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCENTF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCENTI      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNDEPOTI  PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLIGNEL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNBLIGNEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBLIGNEF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNBLIGNEI      PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTATIL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNMUTATIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMUTATIF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNMUTATII      PIC X(7).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPIECEL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNBPIECEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPIECEF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNBPIECEI      PIC X(5).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUOTNBPL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MQUOTNBPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQUOTNBPF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MQUOTNBPI      PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMUTATIL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MDMUTATIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDMUTATIF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDMUTATII      PIC X(10).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVOLTOTL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MVOLTOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MVOLTOTF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MVOLTOTI  PIC X(9).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUOTVOLL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MQUOTVOLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQUOTVOLF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQUOTVOLI      PIC X(7).                                  00000610
           02 M9I OCCURS   11 TIMES .                                   00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MGRPREAPL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MGRPREAPL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MGRPREAPF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MGRPREAPI    PIC X(20).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRESAVTL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MRESAVTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MRESAVTF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MRESAVTI     PIC X(7).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRESMUTL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MRESMUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MRESMUTF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MRESMUTI     PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRESAPRL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MRESAPRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MRESAPRF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MRESAPRI     PIC X(7).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTOTAVTL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MTOTAVTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTOTAVTF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MTOTAVTI     PIC X(7).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTOTMUTL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MTOTMUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTOTMUTF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MTOTMUTI     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTOTAPRL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MTOTAPRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTOTAPRF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MTOTAPRI     PIC X(7).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVOLUMEL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MVOLUMEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVOLUMEF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MVOLUMEI     PIC X(9).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCCL   COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MQCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQCCF   PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQCCI   PIC X(4).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MZONCMDI  PIC X(12).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLIBERRI  PIC X(61).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCICSI    PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MNETNAMI  PIC X(8).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MSCREENI  PIC X(4).                                       00001220
      ***************************************************************** 00001230
      * SDF: EMU13   EMU13                                              00001240
      ***************************************************************** 00001250
       01   EMU13O REDEFINES EMU13I.                                    00001260
           02 FILLER    PIC X(12).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTIMJOUA  PIC X.                                          00001360
           02 MTIMJOUC  PIC X.                                          00001370
           02 MTIMJOUP  PIC X.                                          00001380
           02 MTIMJOUH  PIC X.                                          00001390
           02 MTIMJOUV  PIC X.                                          00001400
           02 MTIMJOUO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNPAGEA   PIC X.                                          00001430
           02 MNPAGEC   PIC X.                                          00001440
           02 MNPAGEP   PIC X.                                          00001450
           02 MNPAGEH   PIC X.                                          00001460
           02 MNPAGEV   PIC X.                                          00001470
           02 MNPAGEO   PIC X(3).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNSOCLIEA      PIC X.                                     00001500
           02 MNSOCLIEC PIC X.                                          00001510
           02 MNSOCLIEP PIC X.                                          00001520
           02 MNSOCLIEH PIC X.                                          00001530
           02 MNSOCLIEV PIC X.                                          00001540
           02 MNSOCLIEO      PIC X(3).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNLIEUA   PIC X.                                          00001570
           02 MNLIEUC   PIC X.                                          00001580
           02 MNLIEUP   PIC X.                                          00001590
           02 MNLIEUH   PIC X.                                          00001600
           02 MNLIEUV   PIC X.                                          00001610
           02 MNLIEUO   PIC X(3).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNSOCENTA      PIC X.                                     00001640
           02 MNSOCENTC PIC X.                                          00001650
           02 MNSOCENTP PIC X.                                          00001660
           02 MNSOCENTH PIC X.                                          00001670
           02 MNSOCENTV PIC X.                                          00001680
           02 MNSOCENTO      PIC X(3).                                  00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNDEPOTA  PIC X.                                          00001710
           02 MNDEPOTC  PIC X.                                          00001720
           02 MNDEPOTP  PIC X.                                          00001730
           02 MNDEPOTH  PIC X.                                          00001740
           02 MNDEPOTV  PIC X.                                          00001750
           02 MNDEPOTO  PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNBLIGNEA      PIC X.                                     00001780
           02 MNBLIGNEC PIC X.                                          00001790
           02 MNBLIGNEP PIC X.                                          00001800
           02 MNBLIGNEH PIC X.                                          00001810
           02 MNBLIGNEV PIC X.                                          00001820
           02 MNBLIGNEO      PIC X(5).                                  00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNMUTATIA      PIC X.                                     00001850
           02 MNMUTATIC PIC X.                                          00001860
           02 MNMUTATIP PIC X.                                          00001870
           02 MNMUTATIH PIC X.                                          00001880
           02 MNMUTATIV PIC X.                                          00001890
           02 MNMUTATIO      PIC X(7).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MNBPIECEA      PIC X.                                     00001920
           02 MNBPIECEC PIC X.                                          00001930
           02 MNBPIECEP PIC X.                                          00001940
           02 MNBPIECEH PIC X.                                          00001950
           02 MNBPIECEV PIC X.                                          00001960
           02 MNBPIECEO      PIC X(5).                                  00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MQUOTNBPA      PIC X.                                     00001990
           02 MQUOTNBPC PIC X.                                          00002000
           02 MQUOTNBPP PIC X.                                          00002010
           02 MQUOTNBPH PIC X.                                          00002020
           02 MQUOTNBPV PIC X.                                          00002030
           02 MQUOTNBPO      PIC X(7).                                  00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MDMUTATIA      PIC X.                                     00002060
           02 MDMUTATIC PIC X.                                          00002070
           02 MDMUTATIP PIC X.                                          00002080
           02 MDMUTATIH PIC X.                                          00002090
           02 MDMUTATIV PIC X.                                          00002100
           02 MDMUTATIO      PIC X(10).                                 00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MVOLTOTA  PIC X.                                          00002130
           02 MVOLTOTC  PIC X.                                          00002140
           02 MVOLTOTP  PIC X.                                          00002150
           02 MVOLTOTH  PIC X.                                          00002160
           02 MVOLTOTV  PIC X.                                          00002170
           02 MVOLTOTO  PIC X(9).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MQUOTVOLA      PIC X.                                     00002200
           02 MQUOTVOLC PIC X.                                          00002210
           02 MQUOTVOLP PIC X.                                          00002220
           02 MQUOTVOLH PIC X.                                          00002230
           02 MQUOTVOLV PIC X.                                          00002240
           02 MQUOTVOLO      PIC X(7).                                  00002250
           02 M9O OCCURS   11 TIMES .                                   00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MGRPREAPA    PIC X.                                     00002280
             03 MGRPREAPC    PIC X.                                     00002290
             03 MGRPREAPP    PIC X.                                     00002300
             03 MGRPREAPH    PIC X.                                     00002310
             03 MGRPREAPV    PIC X.                                     00002320
             03 MGRPREAPO    PIC X(20).                                 00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MRESAVTA     PIC X.                                     00002350
             03 MRESAVTC     PIC X.                                     00002360
             03 MRESAVTP     PIC X.                                     00002370
             03 MRESAVTH     PIC X.                                     00002380
             03 MRESAVTV     PIC X.                                     00002390
             03 MRESAVTO     PIC X(7).                                  00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MRESMUTA     PIC X.                                     00002420
             03 MRESMUTC     PIC X.                                     00002430
             03 MRESMUTP     PIC X.                                     00002440
             03 MRESMUTH     PIC X.                                     00002450
             03 MRESMUTV     PIC X.                                     00002460
             03 MRESMUTO     PIC X(5).                                  00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MRESAPRA     PIC X.                                     00002490
             03 MRESAPRC     PIC X.                                     00002500
             03 MRESAPRP     PIC X.                                     00002510
             03 MRESAPRH     PIC X.                                     00002520
             03 MRESAPRV     PIC X.                                     00002530
             03 MRESAPRO     PIC X(7).                                  00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MTOTAVTA     PIC X.                                     00002560
             03 MTOTAVTC     PIC X.                                     00002570
             03 MTOTAVTP     PIC X.                                     00002580
             03 MTOTAVTH     PIC X.                                     00002590
             03 MTOTAVTV     PIC X.                                     00002600
             03 MTOTAVTO     PIC X(7).                                  00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MTOTMUTA     PIC X.                                     00002630
             03 MTOTMUTC     PIC X.                                     00002640
             03 MTOTMUTP     PIC X.                                     00002650
             03 MTOTMUTH     PIC X.                                     00002660
             03 MTOTMUTV     PIC X.                                     00002670
             03 MTOTMUTO     PIC X(5).                                  00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MTOTAPRA     PIC X.                                     00002700
             03 MTOTAPRC     PIC X.                                     00002710
             03 MTOTAPRP     PIC X.                                     00002720
             03 MTOTAPRH     PIC X.                                     00002730
             03 MTOTAPRV     PIC X.                                     00002740
             03 MTOTAPRO     PIC X(7).                                  00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MVOLUMEA     PIC X.                                     00002770
             03 MVOLUMEC     PIC X.                                     00002780
             03 MVOLUMEP     PIC X.                                     00002790
             03 MVOLUMEH     PIC X.                                     00002800
             03 MVOLUMEV     PIC X.                                     00002810
             03 MVOLUMEO     PIC X(9).                                  00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MQCCA   PIC X.                                          00002840
             03 MQCCC   PIC X.                                          00002850
             03 MQCCP   PIC X.                                          00002860
             03 MQCCH   PIC X.                                          00002870
             03 MQCCV   PIC X.                                          00002880
             03 MQCCO   PIC X(4).                                       00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MZONCMDA  PIC X.                                          00002910
           02 MZONCMDC  PIC X.                                          00002920
           02 MZONCMDP  PIC X.                                          00002930
           02 MZONCMDH  PIC X.                                          00002940
           02 MZONCMDV  PIC X.                                          00002950
           02 MZONCMDO  PIC X(12).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLIBERRA  PIC X.                                          00002980
           02 MLIBERRC  PIC X.                                          00002990
           02 MLIBERRP  PIC X.                                          00003000
           02 MLIBERRH  PIC X.                                          00003010
           02 MLIBERRV  PIC X.                                          00003020
           02 MLIBERRO  PIC X(61).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSO    PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNETNAMA  PIC X.                                          00003190
           02 MNETNAMC  PIC X.                                          00003200
           02 MNETNAMP  PIC X.                                          00003210
           02 MNETNAMH  PIC X.                                          00003220
           02 MNETNAMV  PIC X.                                          00003230
           02 MNETNAMO  PIC X(8).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSCREENA  PIC X.                                          00003260
           02 MSCREENC  PIC X.                                          00003270
           02 MSCREENP  PIC X.                                          00003280
           02 MSCREENH  PIC X.                                          00003290
           02 MSCREENV  PIC X.                                          00003300
           02 MSCREENO  PIC X(4).                                       00003310
                                                                                
