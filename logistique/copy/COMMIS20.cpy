      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      * COMMAREA SPECIFIQUE PRG TIS20 (TIS00 -> MENU)    TR: IS20  *    00030000
      *                                                            *    00031000
      *                       INVENTAIRE                           *    00040000
      *                                                            *    00050000
      * ZONES RESERVEES APPLICATIVES ------------------------------*    00060003
      *                                                            *    00070000
      *        TRANSACTION IS20                                    *    00080000
      *                                                            *    00090000
      *------------------------------ ZONE DONNEES TIS20---2597----*    00110000
                                                                        00720200
           02 COMM-IS20-APPLI   REDEFINES COMM-IS00-APPLI.              00721000
              03 COMM-IS20-SSLIEU      PIC X.                              00743
              03 COMM-IS20-CLIEUTRT    PIC X(5).                           00743
              03 COMM-IS20-ZONE        PIC XX.                             00743
              03 COMM-IS20-SECTEUR     PIC XX.                             00743
              03 COMM-IS20-LIB-ZONE    PIC X(20).                          00743
              03 COMM-IS20-TYPECPT     PIC XX.                             00743
              03 COMM-IS20-QTE         PIC X.                              00743
              03 COMM-IS20-FICHEDEB    PIC X(5).                           00743
              03 COMM-IS20-FICHEFIN    PIC X(5).                           00743
              03 COMM-IS20-GLOBALOK    PIC X.                              00743
              03 COMM-IS20-COMPTAGEG   PIC 9(5).                                
              03 COMM-IS20-DEL-OK      PIC X.                              00743
              03 COMM-IS20-IS21-OK     PIC X.                              00743
              03 COMM-IS20-RANG-TS31   PIC 999.                            00743
              03 COMM-IS20-DEL-OK-GLOBAL  PIC X.                           00743
              03 COMM-IS20-MAX            PIC X(12).                       00743
              03 COMM-IS20-VALID-MAX REDEFINES COMM-IS20-MAX  OCCURS 12.   00743
                 04 COMM-IS20-MAX-ZONE  PIC X.                             00743
            03 COMM-IS20-FILLER     PIC X(2529).                        00743030
                                                                                
                                                                        00750000
