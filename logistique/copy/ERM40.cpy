      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM40                                                      00000020
      ***************************************************************** 00000030
       01   ERM40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFAMI    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLFAMI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNMAGI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLMAGI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAGREGL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLAGREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAGREGF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLAGREGI  PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCODICI  PIC X(7).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODAPPL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCODAPPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODAPPF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCODAPPI  PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOC1L   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNSOC1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOC1F   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNSOC1I   PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAG1L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNMAG1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMAG1F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNMAG1I   PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOC2L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNSOC2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOC2F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNSOC2I   PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAG2L   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNMAG2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMAG2F   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNMAG2I   PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCFAM1I   PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM2L   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCFAM2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM2F   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCFAM2I   PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCEXPOI   PIC X.                                          00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBERRI  PIC X(78).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCODTRAI  PIC X(4).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCICSI    PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNETNAMI  PIC X(8).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSCREENI  PIC X(4).                                       00000970
      ***************************************************************** 00000980
      * SDF: ERM40                                                      00000990
      ***************************************************************** 00001000
       01   ERM40O REDEFINES ERM40I.                                    00001010
           02 FILLER    PIC X(12).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MDATJOUA  PIC X.                                          00001040
           02 MDATJOUC  PIC X.                                          00001050
           02 MDATJOUP  PIC X.                                          00001060
           02 MDATJOUH  PIC X.                                          00001070
           02 MDATJOUV  PIC X.                                          00001080
           02 MDATJOUO  PIC X(10).                                      00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MTIMJOUA  PIC X.                                          00001110
           02 MTIMJOUC  PIC X.                                          00001120
           02 MTIMJOUP  PIC X.                                          00001130
           02 MTIMJOUH  PIC X.                                          00001140
           02 MTIMJOUV  PIC X.                                          00001150
           02 MTIMJOUO  PIC X(5).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MZONCMDA  PIC X.                                          00001180
           02 MZONCMDC  PIC X.                                          00001190
           02 MZONCMDP  PIC X.                                          00001200
           02 MZONCMDH  PIC X.                                          00001210
           02 MZONCMDV  PIC X.                                          00001220
           02 MZONCMDO  PIC X(3).                                       00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNSOCA    PIC X.                                          00001250
           02 MNSOCC    PIC X.                                          00001260
           02 MNSOCP    PIC X.                                          00001270
           02 MNSOCH    PIC X.                                          00001280
           02 MNSOCV    PIC X.                                          00001290
           02 MNSOCO    PIC X(3).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MCFAMA    PIC X.                                          00001320
           02 MCFAMC    PIC X.                                          00001330
           02 MCFAMP    PIC X.                                          00001340
           02 MCFAMH    PIC X.                                          00001350
           02 MCFAMV    PIC X.                                          00001360
           02 MCFAMO    PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MLFAMA    PIC X.                                          00001390
           02 MLFAMC    PIC X.                                          00001400
           02 MLFAMP    PIC X.                                          00001410
           02 MLFAMH    PIC X.                                          00001420
           02 MLFAMV    PIC X.                                          00001430
           02 MLFAMO    PIC X(20).                                      00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNMAGA    PIC X.                                          00001460
           02 MNMAGC    PIC X.                                          00001470
           02 MNMAGP    PIC X.                                          00001480
           02 MNMAGH    PIC X.                                          00001490
           02 MNMAGV    PIC X.                                          00001500
           02 MNMAGO    PIC X(3).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MLMAGA    PIC X.                                          00001530
           02 MLMAGC    PIC X.                                          00001540
           02 MLMAGP    PIC X.                                          00001550
           02 MLMAGH    PIC X.                                          00001560
           02 MLMAGV    PIC X.                                          00001570
           02 MLMAGO    PIC X(20).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MLAGREGA  PIC X.                                          00001600
           02 MLAGREGC  PIC X.                                          00001610
           02 MLAGREGP  PIC X.                                          00001620
           02 MLAGREGH  PIC X.                                          00001630
           02 MLAGREGV  PIC X.                                          00001640
           02 MLAGREGO  PIC X(20).                                      00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MNCODICA  PIC X.                                          00001670
           02 MNCODICC  PIC X.                                          00001680
           02 MNCODICP  PIC X.                                          00001690
           02 MNCODICH  PIC X.                                          00001700
           02 MNCODICV  PIC X.                                          00001710
           02 MNCODICO  PIC X(7).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MCODAPPA  PIC X.                                          00001740
           02 MCODAPPC  PIC X.                                          00001750
           02 MCODAPPP  PIC X.                                          00001760
           02 MCODAPPH  PIC X.                                          00001770
           02 MCODAPPV  PIC X.                                          00001780
           02 MCODAPPO  PIC X(3).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MNSOC1A   PIC X.                                          00001810
           02 MNSOC1C   PIC X.                                          00001820
           02 MNSOC1P   PIC X.                                          00001830
           02 MNSOC1H   PIC X.                                          00001840
           02 MNSOC1V   PIC X.                                          00001850
           02 MNSOC1O   PIC X(3).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MNMAG1A   PIC X.                                          00001880
           02 MNMAG1C   PIC X.                                          00001890
           02 MNMAG1P   PIC X.                                          00001900
           02 MNMAG1H   PIC X.                                          00001910
           02 MNMAG1V   PIC X.                                          00001920
           02 MNMAG1O   PIC X(3).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MNSOC2A   PIC X.                                          00001950
           02 MNSOC2C   PIC X.                                          00001960
           02 MNSOC2P   PIC X.                                          00001970
           02 MNSOC2H   PIC X.                                          00001980
           02 MNSOC2V   PIC X.                                          00001990
           02 MNSOC2O   PIC X(3).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNMAG2A   PIC X.                                          00002020
           02 MNMAG2C   PIC X.                                          00002030
           02 MNMAG2P   PIC X.                                          00002040
           02 MNMAG2H   PIC X.                                          00002050
           02 MNMAG2V   PIC X.                                          00002060
           02 MNMAG2O   PIC X(3).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MCFAM1A   PIC X.                                          00002090
           02 MCFAM1C   PIC X.                                          00002100
           02 MCFAM1P   PIC X.                                          00002110
           02 MCFAM1H   PIC X.                                          00002120
           02 MCFAM1V   PIC X.                                          00002130
           02 MCFAM1O   PIC X(5).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCFAM2A   PIC X.                                          00002160
           02 MCFAM2C   PIC X.                                          00002170
           02 MCFAM2P   PIC X.                                          00002180
           02 MCFAM2H   PIC X.                                          00002190
           02 MCFAM2V   PIC X.                                          00002200
           02 MCFAM2O   PIC X(5).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MCEXPOA   PIC X.                                          00002230
           02 MCEXPOC   PIC X.                                          00002240
           02 MCEXPOP   PIC X.                                          00002250
           02 MCEXPOH   PIC X.                                          00002260
           02 MCEXPOV   PIC X.                                          00002270
           02 MCEXPOO   PIC X.                                          00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MLIBERRA  PIC X.                                          00002300
           02 MLIBERRC  PIC X.                                          00002310
           02 MLIBERRP  PIC X.                                          00002320
           02 MLIBERRH  PIC X.                                          00002330
           02 MLIBERRV  PIC X.                                          00002340
           02 MLIBERRO  PIC X(78).                                      00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MCODTRAA  PIC X.                                          00002370
           02 MCODTRAC  PIC X.                                          00002380
           02 MCODTRAP  PIC X.                                          00002390
           02 MCODTRAH  PIC X.                                          00002400
           02 MCODTRAV  PIC X.                                          00002410
           02 MCODTRAO  PIC X(4).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MCICSA    PIC X.                                          00002440
           02 MCICSC    PIC X.                                          00002450
           02 MCICSP    PIC X.                                          00002460
           02 MCICSH    PIC X.                                          00002470
           02 MCICSV    PIC X.                                          00002480
           02 MCICSO    PIC X(5).                                       00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNETNAMA  PIC X.                                          00002510
           02 MNETNAMC  PIC X.                                          00002520
           02 MNETNAMP  PIC X.                                          00002530
           02 MNETNAMH  PIC X.                                          00002540
           02 MNETNAMV  PIC X.                                          00002550
           02 MNETNAMO  PIC X(8).                                       00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MSCREENA  PIC X.                                          00002580
           02 MSCREENC  PIC X.                                          00002590
           02 MSCREENP  PIC X.                                          00002600
           02 MSCREENH  PIC X.                                          00002610
           02 MSCREENV  PIC X.                                          00002620
           02 MSCREENO  PIC X(4).                                       00002630
                                                                                
