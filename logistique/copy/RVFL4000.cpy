      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFL4000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFL4000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL4000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL4000.                                                            
      *}                                                                        
           02  FL40-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  FL40-NCODIC                                                      
               PIC X(0007).                                                     
           02  FL40-CFAM                                                        
               PIC X(0005).                                                     
           02  FL40-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  FL40-CMARQ                                                       
               PIC X(0005).                                                     
           02  FL40-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  FL40-WDACEM                                                      
               PIC X(0001).                                                     
           02  FL40-CASSORT                                                     
               PIC X(0005).                                                     
           02  FL40-LSTATCOMP                                                   
               PIC X(0003).                                                     
           02  FL40-CEXPO                                                       
               PIC X(0005).                                                     
           02  FL40-WSENSVTE                                                    
               PIC X(0001).                                                     
           02  FL40-CHEFPROD                                                    
               PIC X(0005).                                                     
           02  FL40-CHEFPRM                                                     
               PIC X(0005).                                                     
           02  FL40-QCOLIVTE                                                    
               PIC S9(5) COMP-3.                                                
           02  FL40-WSTOCKAVANCE                                                
               PIC X(0001).                                                     
           02  FL40-WTLMELA                                                     
               PIC X(0003).                                                     
           02  FL40-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFL4000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL4000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL4000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-WDACEM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-WDACEM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-CASSORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-CASSORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-LSTATCOMP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-LSTATCOMP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-CEXPO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-CEXPO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-WSENSVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-WSENSVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-CHEFPROD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-CHEFPROD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-CHEFPRM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-CHEFPRM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-QCOLIVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-QCOLIVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-WSTOCKAVANCE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-WSTOCKAVANCE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-WTLMELA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-WTLMELA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL40-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL40-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
