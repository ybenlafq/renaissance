      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM7500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM7500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM7500.                                                            
           02  RM75-CGROUP                                                      
               PIC X(0005).                                                     
           02  RM75-CFAM                                                        
               PIC X(0005).                                                     
           02  RM75-LAGREGATED                                                  
               PIC X(0020).                                                     
           02  RM75-DSEMAINE                                                    
               PIC X(0006).                                                     
           02  RM75-QPV                                                         
               PIC S9(7) COMP-3.                                                
           02  RM75-WNATURE                                                     
               PIC X(0001).                                                     
           02  RM75-WIMPACT                                                     
               PIC X(0001).                                                     
           02  RM75-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM7500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM7500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM75-CGROUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM75-CGROUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM75-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM75-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM75-LAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM75-LAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM75-DSEMAINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM75-DSEMAINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM75-QPV-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM75-QPV-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM75-WNATURE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM75-WNATURE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM75-WIMPACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM75-WIMPACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM75-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM75-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
