      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGQ01 (TGQ00 -> MENU)    TR: GQ00  *    00002232
      *          GESTION DES PARAMETRES GENERAUX LIVRAISON         *    00002332
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *   RANSACTION GQ01 : DESCRIPTION DES PROFILS                *    00411032
      *                     D'AFFILIATION ( FAMILLE / ENTREPOTS )  *    00411132
      *                                                                 00412000
          02 COMM-GQ01-APPLI REDEFINES COMM-GQ00-APPLI.                 00420032
      *------------------------------ ZONE DONNEES TGQ01                00510032
             03 COMM-GQ01-DONNEES-1-TGQ01.                              00520035
      *------------------------------ CODE FONCTION                     00550033
                04 COMM-GQ01-FONCT             PIC X(3).                00560037
      *------------------------------ CODE PROFIL                       00561033
                04 COMM-GQ01-CPROAFF           PIC X(5).                00562033
      *------------------------------ LIBELLE PROFIL                    00570032
                04 COMM-GQ01-LPROAFF           PIC X(20).               00580032
      *------------------------------ DERNIER IND-TS                    00742435
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GQ01-PAGSUI            PIC S9(4) COMP.          00742535
      *--                                                                       
                04 COMM-GQ01-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN IND-TS                     00742635
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GQ01-PAGENC            PIC S9(4) COMP.          00742735
      *--                                                                       
                04 COMM-GQ01-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00742824
                04 COMM-GQ01-NUMPAG            PIC 9(3).                00742932
      *------------------------------ INDICATEUR FIN DE TABLE           00743035
                04 COMM-GQ01-INDPAG            PIC 9.                   00743135
      *------------------------------ ZONE CONFIRMATION PF3             00743235
                04 COMM-GQ01-CONF-PF3          PIC 9(1).                00743335
      *------------------------------ ZONE CONFIRMATION PF4             00743435
                04 COMM-GQ01-CONF-PF4          PIC 9(1).                00743535
      *------------------------------ OK MAJ SUR RTGQ05 => VALID = '1'  00743636
                04 COMM-GQ01-VALID-RTGQ05   PIC  9.                     00743736
      *------------------------------ OK MAJ SUR RTGA01 => VALID = '1'  00743841
                04 COMM-GQ01-VALID-RTGA01   PIC  9.                     00743941
      *------------------------------    MAJ SUR RTGA01 => VALID = 'M'  00744042
                04 COMM-GQ01-CMAJ-RTGA01    PIC  X.                     00744142
      *------------------------------ TABLE N� TS PR PAGINATION         00744236
                04 COMM-GQ01-TABTS          OCCURS 100.                 00744336
      *------------------------------ 1ERS IND TS DES PAGES <=> FAMILLE 00744436
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               06 COMM-GQ01-NUMTS          PIC S9(4) COMP.       00744536
      *--                                                                       
                      06 COMM-GQ01-NUMTS          PIC S9(4) COMP-5.             
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS               00744636
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GQ01-IND-RECH-TS    PIC S9(4) COMP.             00745036
      *--                                                                       
                04 COMM-GQ01-IND-RECH-TS    PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND TS MAX                        00746036
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GQ01-IND-TS-MAX     PIC S9(4) COMP.             00747036
      *--                                                                       
                04 COMM-GQ01-IND-TS-MAX     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ ZONE DONNEES 2 TGQ01              00760035
             03 COMM-GQ01-DONNEES-2-TGQ01.                              00761035
      *------------------------------ ZONE FAMILLES PAGE                00770035
                04 COMM-GQ01-TABFAM.                                    00780035
      *------------------------------ ZONE LIGNES FAMILLES              00790035
                   05 COMM-GQ01-LIGFAM  OCCURS 13.                      00800035
      *------------------------------ CODE FAMILLE                      00810035
                      06 COMM-GQ01-CFAM           PIC X(5).             00820035
      *------------------------------ LIBELLE FAMILLE                   00830035
                      06 COMM-GQ01-LFAM           PIC X(20).            00840035
      *------------------------------ CODES D'AFFILIATION               00850035
                      06 COMM-GQ01-ENTAFF         OCCURS 5.             00860035
      *------------------------------ CODE SOCIETE D'AFFILIATION        00870035
                         07 COMM-GQ01-CSOCAFF     PIC X(03).            00880035
      *------------------------------ CODE ENTREPOT D'AFFILIATION       00890035
                         07 COMM-GQ01-CDEPAFF     PIC X(03).            00900035
      *------------------------------ ZONE DONNEES 3 TGQ01              01650035
             03 COMM-GQ01-DONNEES-3-TGQ01.                              01651035
      *------------------------------ ZONES SWAP                        01670035
                   05 COMM-GQ01-ATTR           PIC X OCCURS 500.        01680038
      *------------------------------ ZONE LIBRE                        01710035
             03 COMM-GQ01-LIBRE          PIC X(2264).                   01720042
      ***************************************************************** 02170035
                                                                                
