      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JRM221 AU 30/08/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,20,BI,A,                          *        
      *                           27,05,BI,A,                          *        
      *                           32,03,PD,A,                          *        
      *                           35,20,BI,A,                          *        
      *                           55,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JRM221.                                                        
            05 NOMETAT-JRM221           PIC X(6) VALUE 'JRM221'.                
            05 RUPTURES-JRM221.                                                 
           10 JRM221-LCHEFPROD          PIC X(20).                      007  020
           10 JRM221-CGROUP             PIC X(05).                      027  005
           10 JRM221-WSEQFAM            PIC S9(05)      COMP-3.         032  003
           10 JRM221-LAGREGATED         PIC X(20).                      035  020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JRM221-SEQUENCE           PIC S9(04) COMP.                055  002
      *--                                                                       
           10 JRM221-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JRM221.                                                   
           10 JRM221-CHEFPROD           PIC X(05).                      057  005
           10 JRM221-DPM1               PIC X(04).                      062  004
           10 JRM221-DPM2               PIC X(04).                      066  004
           10 JRM221-DPM3               PIC X(04).                      070  004
           10 JRM221-DPM4               PIC X(04).                      074  004
           10 JRM221-DPM5               PIC X(04).                      078  004
           10 JRM221-DPM6               PIC X(04).                      082  004
           10 JRM221-DPM7               PIC X(04).                      086  004
           10 JRM221-DPM8               PIC X(04).                      090  004
           10 JRM221-DPRESTOCK1         PIC X(04).                      094  004
           10 JRM221-DPRESTOCK2         PIC X(04).                      098  004
           10 JRM221-DPV1               PIC X(04).                      102  004
           10 JRM221-DPV2               PIC X(04).                      106  004
           10 JRM221-DPV3               PIC X(04).                      110  004
           10 JRM221-DV1SD              PIC X(04).                      114  004
           10 JRM221-DV1SF              PIC X(04).                      118  004
           10 JRM221-DV2SD              PIC X(04).                      122  004
           10 JRM221-DV2SF              PIC X(04).                      126  004
           10 JRM221-DV3SD              PIC X(04).                      130  004
           10 JRM221-DV3SF              PIC X(04).                      134  004
           10 JRM221-DV4SD              PIC X(04).                      138  004
           10 JRM221-DV4SF              PIC X(04).                      142  004
           10 JRM221-LFAM               PIC X(20).                      146  020
           10 JRM221-NSOCIETE           PIC X(03).                      166  003
           10 JRM221-QPV1               PIC X(06).                      169  006
           10 JRM221-QPV2               PIC X(06).                      175  006
           10 JRM221-QPV3               PIC X(06).                      181  006
           10 JRM221-WEDIT              PIC X(01).                      187  001
           10 JRM221-QPM-S1             PIC S9(07)      COMP-3.         188  004
           10 JRM221-QPM-S2             PIC S9(07)      COMP-3.         192  004
           10 JRM221-QPM-S3             PIC S9(07)      COMP-3.         196  004
           10 JRM221-QPM-S4             PIC S9(07)      COMP-3.         200  004
           10 JRM221-QPM-S5             PIC S9(07)      COMP-3.         204  004
           10 JRM221-QPM-S6             PIC S9(07)      COMP-3.         208  004
           10 JRM221-QPM-S7             PIC S9(07)      COMP-3.         212  004
           10 JRM221-QPM-S8             PIC S9(07)      COMP-3.         216  004
           10 JRM221-QPVS-1             PIC S9(07)      COMP-3.         220  004
           10 JRM221-QPVS-2             PIC S9(07)      COMP-3.         224  004
           10 JRM221-QPVS-3             PIC S9(07)      COMP-3.         228  004
           10 JRM221-QPVS-4             PIC S9(07)      COMP-3.         232  004
           10 JRM221-QV1S               PIC S9(07)      COMP-3.         236  004
           10 JRM221-QV2S               PIC S9(07)      COMP-3.         240  004
           10 JRM221-QV3S               PIC S9(07)      COMP-3.         244  004
           10 JRM221-QV4S               PIC S9(07)      COMP-3.         248  004
            05 FILLER                      PIC X(261).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JRM221-LONG           PIC S9(4)   COMP  VALUE +251.           
      *                                                                         
      *--                                                                       
        01  DSECT-JRM221-LONG           PIC S9(4) COMP-5  VALUE +251.           
                                                                                
      *}                                                                        
