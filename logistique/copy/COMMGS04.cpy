      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
000010* ZONES RESERVEES APPLICATIVES ---------------------------- 3568  00730000
000020     03 COMM-GS04-APPLI REDEFINES COMM-GS00-FILLER.               00970010
000030*                                                                 00720000
000040*                         TABLE DES GROUPES MIS EN RESERVE              00
000194        05 COMM-GS04-TAB-X    OCCURS  10.                               03
000195           07 COMM-GS04-TAB-J    OCCURS  9.                             03
000196              10 COMM-GS04-CGROUP         PIC X(05).              00970003
000197*                                                                       00
000198              10 COMM-GS04-LGROUP         PIC X(20).              00970003
000199*                                                                       00
000202              10 COMM-GS04-AUTOR          PIC X.                  00970003
000203*                                                                       00
000204*                         LIBELLE MARQUE                                00
000205        05 COMM-GS04-LMARQ          PIC X(20).                          03
000206*                                                                       00
000207*                         LIBELLE FAMILLE                               00
000208        05 COMM-GS04-LFAM           PIC X(20).                          03
000209*                                                                       00
000210*                         INDICE POUR FOURNISSEUR                       00
000211        05 COMM-GS04-REPCDE         PIC X.                              03
000212*                                                                       00
000216*                         NOMBRE DE PAGE                                00
000217        05 COMM-GS04-NBRPAG         PIC 99.                             03
000218*                                                                       00
000219*                         NUMERO DE LA PAGE                             00
000220        05 COMM-GS04-NPAGE          PIC 99.                             03
000230*                                                                 00720000
000231*                         CODIC SUIVANT                                 00
000232        05 COMM-GS04-CODIC-SUIV     PIC X(7).                           03
000233*                                                                 00720000
000234*                         CODIC DACEM (O-N)                             00
000235        05 COMM-GS04-WDACEM         PIC X(1).                           03
000236*                                                                 00720000
                                                                                
