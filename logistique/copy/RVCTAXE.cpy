      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CTAXE TYPES DE TAXES                   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCTAXE .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCTAXE .                                                            
      *}                                                                        
           05  CTAXE-CTABLEG2    PIC X(15).                                     
           05  CTAXE-CTABLEG2-REDEF REDEFINES CTAXE-CTABLEG2.                   
               10  CTAXE-CTAXE           PIC X(05).                             
           05  CTAXE-WTABLEG     PIC X(80).                                     
           05  CTAXE-WTABLEG-REDEF  REDEFINES CTAXE-WTABLEG.                    
               10  CTAXE-LTAXE           PIC X(20).                             
               10  CTAXE-WFRN            PIC X(01).                             
               10  CTAXE-NSEQ            PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  CTAXE-NSEQ-N         REDEFINES CTAXE-NSEQ                    
                                         PIC 9(01).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  CTAXE-WACTIF          PIC X(01).                             
               10  CTAXE-TRANSCO         PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCTAXE-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCTAXE-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTAXE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CTAXE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTAXE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CTAXE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
