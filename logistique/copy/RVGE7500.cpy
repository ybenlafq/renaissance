      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGE7500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGE7500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGE7500.                                                            
           02  GE75-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GE75-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GE75-CFAM                                                        
               PIC X(0005).                                                     
           02  GE75-LAGREGATED                                                  
               PIC X(0020).                                                     
           02  GE75-QPCTM                                                       
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GE75-QPCTL                                                       
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GE75-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGE7500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGE7500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE75-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE75-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE75-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE75-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE75-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE75-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE75-LAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE75-LAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE75-QPCTM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE75-QPCTM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE75-QPCTL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE75-QPCTL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE75-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE75-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
