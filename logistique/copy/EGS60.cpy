      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS60   EGS60                                              00000020
      ***************************************************************** 00000030
       01   EGS60I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNOPAGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNOPAGF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNOPAGI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSORIGL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MSORIGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSORIGF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSORIGI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLORIGL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLORIGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLORIGF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLORIGI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLORIGL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MSLORIGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSLORIGF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MSLORIGI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCODICI   PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLCODICI  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDESTL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MSDESTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSDESTF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSDESTI   PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDESTL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLDESTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLDESTF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLDESTI   PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLDESTL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MSLDESTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSLDESTF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSLDESTI  PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAML     COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MFAML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFAMF     PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MFAMI     PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLFAMI    PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPERL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MOPERL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MOPERF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MOPERI    PIC X(10).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMARQUEL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MMARQUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMARQUEF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MMARQUEI  PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQUEL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MLMARQUEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMARQUEF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLMARQUEI      PIC X(20).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOPERL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDOPERF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDOPERI   PIC X(8).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIGINEL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MORIGINEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MORIGINEF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MORIGINEI      PIC X(30).                                 00000770
           02 MSTRUCTUREI OCCURS   7 TIMES .                            00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMOUVTL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MDMOUVTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDMOUVTF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MDMOUVTI     PIC X(8).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEU1L      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MLIEU1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIEU1F      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MLIEU1I      PIC X(3).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEU2L      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MLIEU2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIEU2F      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MLIEU2I      PIC X(3).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEU3L      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MLIEU3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIEU3F      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MLIEU3I      PIC X(3).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPPRETL    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MTYPPRETL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MTYPPRETF    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MTYPPRETI    PIC X.                                     00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMMENTL   COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MLCOMMENTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCOMMENTF   PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MLCOMMENTI   PIC X(30).                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQTEI   PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MSELI   PIC X.                                          00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MZONCMDI  PIC X(12).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MLIBERRI  PIC X(61).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCICSI    PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MNETNAMI  PIC X(8).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MSCREENI  PIC X(4).                                       00001340
      ***************************************************************** 00001350
      * SDF: EGS60   EGS60                                              00001360
      ***************************************************************** 00001370
       01   EGS60O REDEFINES EGS60I.                                    00001380
           02 FILLER    PIC X(12).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDATJOUA  PIC X.                                          00001410
           02 MDATJOUC  PIC X.                                          00001420
           02 MDATJOUP  PIC X.                                          00001430
           02 MDATJOUH  PIC X.                                          00001440
           02 MDATJOUV  PIC X.                                          00001450
           02 MDATJOUO  PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTIMJOUA  PIC X.                                          00001480
           02 MTIMJOUC  PIC X.                                          00001490
           02 MTIMJOUP  PIC X.                                          00001500
           02 MTIMJOUH  PIC X.                                          00001510
           02 MTIMJOUV  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNOPAGA   PIC X.                                          00001550
           02 MNOPAGC   PIC X.                                          00001560
           02 MNOPAGP   PIC X.                                          00001570
           02 MNOPAGH   PIC X.                                          00001580
           02 MNOPAGV   PIC X.                                          00001590
           02 MNOPAGO   PIC X(3).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MSORIGA   PIC X.                                          00001620
           02 MSORIGC   PIC X.                                          00001630
           02 MSORIGP   PIC X.                                          00001640
           02 MSORIGH   PIC X.                                          00001650
           02 MSORIGV   PIC X.                                          00001660
           02 MSORIGO   PIC X(3).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MLORIGA   PIC X.                                          00001690
           02 MLORIGC   PIC X.                                          00001700
           02 MLORIGP   PIC X.                                          00001710
           02 MLORIGH   PIC X.                                          00001720
           02 MLORIGV   PIC X.                                          00001730
           02 MLORIGO   PIC X(3).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MSLORIGA  PIC X.                                          00001760
           02 MSLORIGC  PIC X.                                          00001770
           02 MSLORIGP  PIC X.                                          00001780
           02 MSLORIGH  PIC X.                                          00001790
           02 MSLORIGV  PIC X.                                          00001800
           02 MSLORIGO  PIC X(3).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MCODICA   PIC X.                                          00001830
           02 MCODICC   PIC X.                                          00001840
           02 MCODICP   PIC X.                                          00001850
           02 MCODICH   PIC X.                                          00001860
           02 MCODICV   PIC X.                                          00001870
           02 MCODICO   PIC X(7).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MLCODICA  PIC X.                                          00001900
           02 MLCODICC  PIC X.                                          00001910
           02 MLCODICP  PIC X.                                          00001920
           02 MLCODICH  PIC X.                                          00001930
           02 MLCODICV  PIC X.                                          00001940
           02 MLCODICO  PIC X(20).                                      00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MSDESTA   PIC X.                                          00001970
           02 MSDESTC   PIC X.                                          00001980
           02 MSDESTP   PIC X.                                          00001990
           02 MSDESTH   PIC X.                                          00002000
           02 MSDESTV   PIC X.                                          00002010
           02 MSDESTO   PIC X(3).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MLDESTA   PIC X.                                          00002040
           02 MLDESTC   PIC X.                                          00002050
           02 MLDESTP   PIC X.                                          00002060
           02 MLDESTH   PIC X.                                          00002070
           02 MLDESTV   PIC X.                                          00002080
           02 MLDESTO   PIC X(3).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MSLDESTA  PIC X.                                          00002110
           02 MSLDESTC  PIC X.                                          00002120
           02 MSLDESTP  PIC X.                                          00002130
           02 MSLDESTH  PIC X.                                          00002140
           02 MSLDESTV  PIC X.                                          00002150
           02 MSLDESTO  PIC X(3).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MFAMA     PIC X.                                          00002180
           02 MFAMC     PIC X.                                          00002190
           02 MFAMP     PIC X.                                          00002200
           02 MFAMH     PIC X.                                          00002210
           02 MFAMV     PIC X.                                          00002220
           02 MFAMO     PIC X(5).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MLFAMA    PIC X.                                          00002250
           02 MLFAMC    PIC X.                                          00002260
           02 MLFAMP    PIC X.                                          00002270
           02 MLFAMH    PIC X.                                          00002280
           02 MLFAMV    PIC X.                                          00002290
           02 MLFAMO    PIC X(20).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MOPERA    PIC X.                                          00002320
           02 MOPERC    PIC X.                                          00002330
           02 MOPERP    PIC X.                                          00002340
           02 MOPERH    PIC X.                                          00002350
           02 MOPERV    PIC X.                                          00002360
           02 MOPERO    PIC X(10).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MMARQUEA  PIC X.                                          00002390
           02 MMARQUEC  PIC X.                                          00002400
           02 MMARQUEP  PIC X.                                          00002410
           02 MMARQUEH  PIC X.                                          00002420
           02 MMARQUEV  PIC X.                                          00002430
           02 MMARQUEO  PIC X(5).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MLMARQUEA      PIC X.                                     00002460
           02 MLMARQUEC PIC X.                                          00002470
           02 MLMARQUEP PIC X.                                          00002480
           02 MLMARQUEH PIC X.                                          00002490
           02 MLMARQUEV PIC X.                                          00002500
           02 MLMARQUEO      PIC X(20).                                 00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MDOPERA   PIC X.                                          00002530
           02 MDOPERC   PIC X.                                          00002540
           02 MDOPERP   PIC X.                                          00002550
           02 MDOPERH   PIC X.                                          00002560
           02 MDOPERV   PIC X.                                          00002570
           02 MDOPERO   PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MORIGINEA      PIC X.                                     00002600
           02 MORIGINEC PIC X.                                          00002610
           02 MORIGINEP PIC X.                                          00002620
           02 MORIGINEH PIC X.                                          00002630
           02 MORIGINEV PIC X.                                          00002640
           02 MORIGINEO      PIC X(30).                                 00002650
           02 MSTRUCTUREO OCCURS   7 TIMES .                            00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MDMOUVTA     PIC X.                                     00002680
             03 MDMOUVTC     PIC X.                                     00002690
             03 MDMOUVTP     PIC X.                                     00002700
             03 MDMOUVTH     PIC X.                                     00002710
             03 MDMOUVTV     PIC X.                                     00002720
             03 MDMOUVTO     PIC X(8).                                  00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MLIEU1A      PIC X.                                     00002750
             03 MLIEU1C PIC X.                                          00002760
             03 MLIEU1P PIC X.                                          00002770
             03 MLIEU1H PIC X.                                          00002780
             03 MLIEU1V PIC X.                                          00002790
             03 MLIEU1O      PIC X(3).                                  00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MLIEU2A      PIC X.                                     00002820
             03 MLIEU2C PIC X.                                          00002830
             03 MLIEU2P PIC X.                                          00002840
             03 MLIEU2H PIC X.                                          00002850
             03 MLIEU2V PIC X.                                          00002860
             03 MLIEU2O      PIC X(3).                                  00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MLIEU3A      PIC X.                                     00002890
             03 MLIEU3C PIC X.                                          00002900
             03 MLIEU3P PIC X.                                          00002910
             03 MLIEU3H PIC X.                                          00002920
             03 MLIEU3V PIC X.                                          00002930
             03 MLIEU3O      PIC X(3).                                  00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MTYPPRETA    PIC X.                                     00002960
             03 MTYPPRETC    PIC X.                                     00002970
             03 MTYPPRETP    PIC X.                                     00002980
             03 MTYPPRETH    PIC X.                                     00002990
             03 MTYPPRETV    PIC X.                                     00003000
             03 MTYPPRETO    PIC X.                                     00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MLCOMMENTA   PIC X.                                     00003030
             03 MLCOMMENTC   PIC X.                                     00003040
             03 MLCOMMENTP   PIC X.                                     00003050
             03 MLCOMMENTH   PIC X.                                     00003060
             03 MLCOMMENTV   PIC X.                                     00003070
             03 MLCOMMENTO   PIC X(30).                                 00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MQTEA   PIC X.                                          00003100
             03 MQTEC   PIC X.                                          00003110
             03 MQTEP   PIC X.                                          00003120
             03 MQTEH   PIC X.                                          00003130
             03 MQTEV   PIC X.                                          00003140
             03 MQTEO   PIC X(5).                                       00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MSELA   PIC X.                                          00003170
             03 MSELC   PIC X.                                          00003180
             03 MSELP   PIC X.                                          00003190
             03 MSELH   PIC X.                                          00003200
             03 MSELV   PIC X.                                          00003210
             03 MSELO   PIC X.                                          00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MZONCMDA  PIC X.                                          00003240
           02 MZONCMDC  PIC X.                                          00003250
           02 MZONCMDP  PIC X.                                          00003260
           02 MZONCMDH  PIC X.                                          00003270
           02 MZONCMDV  PIC X.                                          00003280
           02 MZONCMDO  PIC X(12).                                      00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLIBERRA  PIC X.                                          00003310
           02 MLIBERRC  PIC X.                                          00003320
           02 MLIBERRP  PIC X.                                          00003330
           02 MLIBERRH  PIC X.                                          00003340
           02 MLIBERRV  PIC X.                                          00003350
           02 MLIBERRO  PIC X(61).                                      00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCODTRAA  PIC X.                                          00003380
           02 MCODTRAC  PIC X.                                          00003390
           02 MCODTRAP  PIC X.                                          00003400
           02 MCODTRAH  PIC X.                                          00003410
           02 MCODTRAV  PIC X.                                          00003420
           02 MCODTRAO  PIC X(4).                                       00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCICSA    PIC X.                                          00003450
           02 MCICSC    PIC X.                                          00003460
           02 MCICSP    PIC X.                                          00003470
           02 MCICSH    PIC X.                                          00003480
           02 MCICSV    PIC X.                                          00003490
           02 MCICSO    PIC X(5).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MNETNAMA  PIC X.                                          00003520
           02 MNETNAMC  PIC X.                                          00003530
           02 MNETNAMP  PIC X.                                          00003540
           02 MNETNAMH  PIC X.                                          00003550
           02 MNETNAMV  PIC X.                                          00003560
           02 MNETNAMO  PIC X(8).                                       00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MSCREENA  PIC X.                                          00003590
           02 MSCREENC  PIC X.                                          00003600
           02 MSCREENP  PIC X.                                          00003610
           02 MSCREENH  PIC X.                                          00003620
           02 MSCREENV  PIC X.                                          00003630
           02 MSCREENO  PIC X(4).                                       00003640
                                                                                
