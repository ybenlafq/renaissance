      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     :                                                  *        
      *  TRANSACTION: MU69                                             *        
      *  TITRE      : COMMAREA DE LA TRANSACTION MU69                  *        
      *  LONGUEUR   : 4096                                             *        
      *                                                                *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
       01  Z-FILLER-COMMAREA   REDEFINES  Z-COMMAREA.                           
           02  FILLER-MU10     PIC X(0577).                                     
      **** ZONES RESERVEES A AIDA ********************************** 100        
      *                                                                         
      *                                                                         
      **** ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ***** 100        
      *                                                                         
      *                                                                         
      ******* ZONES APPLICATIVES MU69                          ****** 51        
           02 COMM-MU69-ZONES.                                                  
                 05 COMM-MU69-PROGPREC        PIC X(05).                00742002
                 05 COMM-MU69-NSOCIETE        PIC X(03).                00742002
                 05 COMM-MU69-NLIEU           PIC X(03).                00743002
                 05 COMM-MU69-LLIEU           PIC X(20).                00744002
                 05 COMM-MU69-NSOCPTF         PIC X(03).                00745002
                 05 COMM-MU69-NPTF            PIC X(03).                00748002
                 05 COMM-MU69-LPTF            PIC X(20).                00748002
                 05 COMM-MU69-PAGE            PIC 9(04).                        
                 05 COMM-MU69-NBPAGES         PIC 9(04).                        
                 05 COMM-MAJ-NCODIC           PIC X(07).                        
                 05 COMM-MAJ-NVENTE           PIC X(07).                        
                 05 COMM-MAJ-STVTE            PIC X.                            
                 05 COMM-MAJ-HD               PIC X.                            
                 05 COMM-MAJ-CUIS             PIC X.                            
                 05 COMM-MAJ-SOCMAG           PIC XXX.                          
                 05 COMM-MAJ-MAG              PIC XXX.                          
                 05 COMM-MAJ-SOCPTF           PIC XXX.                          
                 05 COMM-MAJ-PTF              PIC XXX.                          
      *****      INDICATEUR SI TS TROP LONGUE                                   
                 05 COMM-MU69-TSLONG          PIC 9(01).                        
                    88 TS-PAS-TROP-LONGUE                       VALUE 0.        
                    88 TS-TROP-LONGUE                           VALUE 1.        
      *                                                                         
                                                                                
