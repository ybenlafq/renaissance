      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : QUOTAS DECENTRALISES                             *        
      *  PROGRAMME  : MZQ10                                            *        
      *  TITRE      : COMMAREA DU PROGRAMME MZQ10                      *        
      *               CONTROLE DU PERIMETRE DE LIVRAISON               *        
      *  LONGUEUR   : 150 C                                            *        
      *  APPELE PAR : PROGRAMMES TZQ11 A TZQ20                         *        
      *                                                                *        
      ******************************************************************        
       01  COMM-MZQ10-APPLI.                                                    
      **** DONNEES CODE RETOUR, MESSAGES D'ERREUR ******************* 60        
           05 COMM-MZQ10-CODRET         PIC X(01).                              
              88 COMM-MZQ10-CODRET-OK                         VALUE ' '.        
              88 COMM-MZQ10-CODRET-ERREUR                     VALUE '1'.        
           05 COMM-MZQ10-LIBERR.                                                
              10 COMM-MZQ10-NSEQERR        PIC X(04).                           
              10 COMM-MZQ10-ERRFIL         PIC X(01).                           
              10 COMM-MZQ10-LMESSAGE       PIC X(54).                           
      *                                                                 00960000
      **** DONNEES PERIMETRES *************************************** 90        
           05 COMM-MZQ10-DONNEES.                                               
      *------ PERIMETRE --------------------------------------------- 33        
              10 COMM-MZQ10-PERIMETRE.                                          
                 20 COMM-MZQ10-CPERIM         PIC X(05).                        
                 20 COMM-MZQ10-LPERIM         PIC X(20).                        
                 20 COMM-MZQ10-LDEPLIV        PIC X(06).                        
                 20 COMM-MZQ10-NBQUOTA        PIC 9(02).                        
      *------ PARAMETRES DU PERIMETRES, OCCURS 1=LIVRE,OCCURS 2=SAV - 24        
                 20 COMM-MZQ10-PARAMETRES     OCCURS 2.                         
                    25 COMM-MZQ10-CTQUOTA        PIC X(01).                     
                    25 COMM-MZQ10-COUPLE.                                       
                       30 COMM-MZQ10-CEQUIP         PIC X(05).                  
                       30 COMM-MZQ10-CMODDEL        PIC X(05).                  
                    25 COMM-MZQ10-WZONE          PIC X(01).                     
              10 COMM-MZQ10-FILLER         PIC X(33).                           
                                                                                
