      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVLG0900                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLG0900                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLG0900.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLG0900.                                                            
      *}                                                                        
           02  LG09-CINSEE                                                      
               PIC X(0005).                                                     
           02  LG09-NVOIE                                                       
               PIC X(0004).                                                     
           02  LG09-CTVOIE                                                      
               PIC X(0004).                                                     
           02  LG09-LNOMVOIE                                                    
               PIC X(0021).                                                     
           02  LG09-LNOMPVOIE                                                   
               PIC X(0042).                                                     
           02  LG09-CILOT                                                       
               PIC X(0008).                                                     
           02  LG09-CPARITE                                                     
               PIC X(0001).                                                     
           02  LG09-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  LG09-NUMVOIE                                                     
               PIC X(0008).                                                     
           02  LG09-DERMOT                                                      
               PIC X(0020).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLG0900                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLG0900-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLG0900-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG09-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG09-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG09-NVOIE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG09-NVOIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG09-CTVOIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG09-CTVOIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG09-LNOMVOIE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG09-LNOMVOIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG09-LNOMPVOIE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG09-LNOMPVOIE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG09-CILOT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG09-CILOT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG09-CPARITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG09-CPARITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG09-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG09-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG09-NUMVOIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG09-NUMVOIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LG09-DERMOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LG09-DERMOT-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
