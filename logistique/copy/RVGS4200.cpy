      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGS4200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS4200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGS4200.                                                            
           02  GS42-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  GS42-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  GS42-NSSLIEUORIG                                                 
               PIC X(0003).                                                     
           02  GS42-NSOCDEST                                                    
               PIC X(0003).                                                     
           02  GS42-NLIEUDEST                                                   
               PIC X(0003).                                                     
           02  GS42-NSSLIEUDEST                                                 
               PIC X(0003).                                                     
           02  GS42-NCODIC                                                      
               PIC X(0007).                                                     
           02  GS42-WSTOCKMASQ                                                  
               PIC X(0001).                                                     
           02  GS42-NSOCVTE                                                     
               PIC X(0003).                                                     
           02  GS42-NLIEUVTE                                                    
               PIC X(0003).                                                     
           02  GS42-NORIGINE                                                    
               PIC X(0007).                                                     
           02  GS42-CMODDEL                                                     
               PIC X(0003).                                                     
           02  GS42-DMVT                                                        
               PIC X(0008).                                                     
           02  GS42-DHMVT                                                       
               PIC X(0002).                                                     
           02  GS42-DMMVT                                                       
               PIC X(0002).                                                     
           02  GS42-CPROG                                                       
               PIC X(0005).                                                     
           02  GS42-COPER                                                       
               PIC X(0010).                                                     
           02  GS42-DOPER                                                       
               PIC X(0008).                                                     
           02  GS42-QMVT                                                        
               PIC S9(5) COMP-3.                                                
           02  GS42-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GS42-CINSEE                                                      
               PIC X(0005).                                                     
           02  GS42-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  GS42-PVTOTAL                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GS42-DCREATION                                                   
               PIC X(0008).                                                     
           02  GS42-CLIEUTRTORIG                                                
               PIC X(0005).                                                     
           02  GS42-CLIEUTRTDEST                                                
               PIC X(0005).                                                     
           02  GS42-DSTAT                                                       
               PIC X(0006).                                                     
           02  GS42-PVTOTALSR                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGS4200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGS4200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-NSSLIEUORIG-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-NSSLIEUORIG-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-NSOCDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-NSOCDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-NLIEUDEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-NLIEUDEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-NSSLIEUDEST-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-NSSLIEUDEST-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-WSTOCKMASQ-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-WSTOCKMASQ-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-NSOCVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-NSOCVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-NLIEUVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-NLIEUVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-NORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-NORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-DMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-DMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-DHMVT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-DHMVT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-DMMVT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-DMMVT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-CPROG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-CPROG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-COPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-COPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-DOPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-DOPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-QMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-QMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-PVTOTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-CLIEUTRTORIG-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-CLIEUTRTORIG-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-CLIEUTRTDEST-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-CLIEUTRTDEST-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-DSTAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-DSTAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS42-PVTOTALSR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS42-PVTOTALSR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
