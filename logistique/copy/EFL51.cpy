      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFL50   EFL50                                              00000020
      ***************************************************************** 00000030
       01   EFL51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLACHL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MSLACHL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSLACHF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSLACHI   PIC X.                                          00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPAGEMAXI      PIC X(2).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCODICI   PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCODICI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMARQI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLMARQI   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSOCIETEI      PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDEPOTI   PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLDEPOTI  PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPTL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MDRECEPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRECEPTF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDRECEPTI      PIC X(10).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MUSINEL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MUSINEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MUSINEF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MUSINEI   PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLUSINEL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLUSINEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLUSINEF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLUSINEI  PIC X(20).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDELAPPRL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MDELAPPRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDELAPPRF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDELAPPRI      PIC X(3).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MUCL      COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MUCL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MUCF      PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MUCI      PIC X(3).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSIL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MSENSIL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSENSIF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSENSII   PIC X.                                          00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRESFRNL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MRESFRNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MRESFRNF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MRESFRNI  PIC X.                                          00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCAPPROI  PIC X(5).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAPPROL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MLAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAPPROF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLAPPROI  PIC X(20).                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MCEXPOI   PIC X(5).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEXPOL   COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLEXPOF   PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLEXPOI   PIC X(20).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTCOMPL      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MLSTCOMPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSTCOMPF      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLSTCOMPI      PIC X(3).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSTKAVL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MWSTKAVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWSTKAVF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MWSTKAVI  PIC X.                                          00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MZONCMDI  PIC X(15).                                      00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MLIBERRI  PIC X(58).                                      00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MCODTRAI  PIC X(4).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MCICSI    PIC X(5).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MNETNAMI  PIC X(8).                                       00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MSCREENI  PIC X(4).                                       00001370
      ***************************************************************** 00001380
      * SDF: EFL50   EFL50                                              00001390
      ***************************************************************** 00001400
       01   EFL51O REDEFINES EFL51I.                                    00001410
           02 FILLER    PIC X(12).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MDATJOUA  PIC X.                                          00001440
           02 MDATJOUC  PIC X.                                          00001450
           02 MDATJOUP  PIC X.                                          00001460
           02 MDATJOUH  PIC X.                                          00001470
           02 MDATJOUV  PIC X.                                          00001480
           02 MDATJOUO  PIC X(10).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MTIMJOUA  PIC X.                                          00001510
           02 MTIMJOUC  PIC X.                                          00001520
           02 MTIMJOUP  PIC X.                                          00001530
           02 MTIMJOUH  PIC X.                                          00001540
           02 MTIMJOUV  PIC X.                                          00001550
           02 MTIMJOUO  PIC X(5).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MPAGEA    PIC X.                                          00001580
           02 MPAGEC    PIC X.                                          00001590
           02 MPAGEP    PIC X.                                          00001600
           02 MPAGEH    PIC X.                                          00001610
           02 MPAGEV    PIC X.                                          00001620
           02 MPAGEO    PIC 99.                                         00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MSLACHA   PIC X.                                          00001650
           02 MSLACHC   PIC X.                                          00001660
           02 MSLACHP   PIC X.                                          00001670
           02 MSLACHH   PIC X.                                          00001680
           02 MSLACHV   PIC X.                                          00001690
           02 MSLACHO   PIC X.                                          00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MPAGEMAXA      PIC X.                                     00001720
           02 MPAGEMAXC PIC X.                                          00001730
           02 MPAGEMAXP PIC X.                                          00001740
           02 MPAGEMAXH PIC X.                                          00001750
           02 MPAGEMAXV PIC X.                                          00001760
           02 MPAGEMAXO      PIC 99.                                    00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCODICA   PIC X.                                          00001790
           02 MCODICC   PIC X.                                          00001800
           02 MCODICP   PIC X.                                          00001810
           02 MCODICH   PIC X.                                          00001820
           02 MCODICV   PIC X.                                          00001830
           02 MCODICO   PIC X(7).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLCODICA  PIC X.                                          00001860
           02 MLCODICC  PIC X.                                          00001870
           02 MLCODICP  PIC X.                                          00001880
           02 MLCODICH  PIC X.                                          00001890
           02 MLCODICV  PIC X.                                          00001900
           02 MLCODICO  PIC X(20).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCFAMA    PIC X.                                          00001930
           02 MCFAMC    PIC X.                                          00001940
           02 MCFAMP    PIC X.                                          00001950
           02 MCFAMH    PIC X.                                          00001960
           02 MCFAMV    PIC X.                                          00001970
           02 MCFAMO    PIC X(5).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MLFAMA    PIC X.                                          00002000
           02 MLFAMC    PIC X.                                          00002010
           02 MLFAMP    PIC X.                                          00002020
           02 MLFAMH    PIC X.                                          00002030
           02 MLFAMV    PIC X.                                          00002040
           02 MLFAMO    PIC X(20).                                      00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MCMARQA   PIC X.                                          00002070
           02 MCMARQC   PIC X.                                          00002080
           02 MCMARQP   PIC X.                                          00002090
           02 MCMARQH   PIC X.                                          00002100
           02 MCMARQV   PIC X.                                          00002110
           02 MCMARQO   PIC X(5).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLMARQA   PIC X.                                          00002140
           02 MLMARQC   PIC X.                                          00002150
           02 MLMARQP   PIC X.                                          00002160
           02 MLMARQH   PIC X.                                          00002170
           02 MLMARQV   PIC X.                                          00002180
           02 MLMARQO   PIC X(20).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MSOCIETEA      PIC X.                                     00002210
           02 MSOCIETEC PIC X.                                          00002220
           02 MSOCIETEP PIC X.                                          00002230
           02 MSOCIETEH PIC X.                                          00002240
           02 MSOCIETEV PIC X.                                          00002250
           02 MSOCIETEO      PIC X(3).                                  00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MDEPOTA   PIC X.                                          00002280
           02 MDEPOTC   PIC X.                                          00002290
           02 MDEPOTP   PIC X.                                          00002300
           02 MDEPOTH   PIC X.                                          00002310
           02 MDEPOTV   PIC X.                                          00002320
           02 MDEPOTO   PIC X(3).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MLDEPOTA  PIC X.                                          00002350
           02 MLDEPOTC  PIC X.                                          00002360
           02 MLDEPOTP  PIC X.                                          00002370
           02 MLDEPOTH  PIC X.                                          00002380
           02 MLDEPOTV  PIC X.                                          00002390
           02 MLDEPOTO  PIC X(20).                                      00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MDRECEPTA      PIC X.                                     00002420
           02 MDRECEPTC PIC X.                                          00002430
           02 MDRECEPTP PIC X.                                          00002440
           02 MDRECEPTH PIC X.                                          00002450
           02 MDRECEPTV PIC X.                                          00002460
           02 MDRECEPTO      PIC X(10).                                 00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MUSINEA   PIC X.                                          00002490
           02 MUSINEC   PIC X.                                          00002500
           02 MUSINEP   PIC X.                                          00002510
           02 MUSINEH   PIC X.                                          00002520
           02 MUSINEV   PIC X.                                          00002530
           02 MUSINEO   PIC X(5).                                       00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MLUSINEA  PIC X.                                          00002560
           02 MLUSINEC  PIC X.                                          00002570
           02 MLUSINEP  PIC X.                                          00002580
           02 MLUSINEH  PIC X.                                          00002590
           02 MLUSINEV  PIC X.                                          00002600
           02 MLUSINEO  PIC X(20).                                      00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MDELAPPRA      PIC X.                                     00002630
           02 MDELAPPRC PIC X.                                          00002640
           02 MDELAPPRP PIC X.                                          00002650
           02 MDELAPPRH PIC X.                                          00002660
           02 MDELAPPRV PIC X.                                          00002670
           02 MDELAPPRO      PIC X(3).                                  00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MUCA      PIC X.                                          00002700
           02 MUCC      PIC X.                                          00002710
           02 MUCP      PIC X.                                          00002720
           02 MUCH      PIC X.                                          00002730
           02 MUCV      PIC X.                                          00002740
           02 MUCO      PIC X(3).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MSENSIA   PIC X.                                          00002770
           02 MSENSIC   PIC X.                                          00002780
           02 MSENSIP   PIC X.                                          00002790
           02 MSENSIH   PIC X.                                          00002800
           02 MSENSIV   PIC X.                                          00002810
           02 MSENSIO   PIC X.                                          00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MRESFRNA  PIC X.                                          00002840
           02 MRESFRNC  PIC X.                                          00002850
           02 MRESFRNP  PIC X.                                          00002860
           02 MRESFRNH  PIC X.                                          00002870
           02 MRESFRNV  PIC X.                                          00002880
           02 MRESFRNO  PIC X.                                          00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MCAPPROA  PIC X.                                          00002910
           02 MCAPPROC  PIC X.                                          00002920
           02 MCAPPROP  PIC X.                                          00002930
           02 MCAPPROH  PIC X.                                          00002940
           02 MCAPPROV  PIC X.                                          00002950
           02 MCAPPROO  PIC X(5).                                       00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLAPPROA  PIC X.                                          00002980
           02 MLAPPROC  PIC X.                                          00002990
           02 MLAPPROP  PIC X.                                          00003000
           02 MLAPPROH  PIC X.                                          00003010
           02 MLAPPROV  PIC X.                                          00003020
           02 MLAPPROO  PIC X(20).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCEXPOA   PIC X.                                          00003050
           02 MCEXPOC   PIC X.                                          00003060
           02 MCEXPOP   PIC X.                                          00003070
           02 MCEXPOH   PIC X.                                          00003080
           02 MCEXPOV   PIC X.                                          00003090
           02 MCEXPOO   PIC X(5).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MLEXPOA   PIC X.                                          00003120
           02 MLEXPOC   PIC X.                                          00003130
           02 MLEXPOP   PIC X.                                          00003140
           02 MLEXPOH   PIC X.                                          00003150
           02 MLEXPOV   PIC X.                                          00003160
           02 MLEXPOO   PIC X(20).                                      00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MLSTCOMPA      PIC X.                                     00003190
           02 MLSTCOMPC PIC X.                                          00003200
           02 MLSTCOMPP PIC X.                                          00003210
           02 MLSTCOMPH PIC X.                                          00003220
           02 MLSTCOMPV PIC X.                                          00003230
           02 MLSTCOMPO      PIC X(3).                                  00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MWSTKAVA  PIC X.                                          00003260
           02 MWSTKAVC  PIC X.                                          00003270
           02 MWSTKAVP  PIC X.                                          00003280
           02 MWSTKAVH  PIC X.                                          00003290
           02 MWSTKAVV  PIC X.                                          00003300
           02 MWSTKAVO  PIC X.                                          00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MZONCMDA  PIC X.                                          00003330
           02 MZONCMDC  PIC X.                                          00003340
           02 MZONCMDP  PIC X.                                          00003350
           02 MZONCMDH  PIC X.                                          00003360
           02 MZONCMDV  PIC X.                                          00003370
           02 MZONCMDO  PIC X(15).                                      00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MLIBERRA  PIC X.                                          00003400
           02 MLIBERRC  PIC X.                                          00003410
           02 MLIBERRP  PIC X.                                          00003420
           02 MLIBERRH  PIC X.                                          00003430
           02 MLIBERRV  PIC X.                                          00003440
           02 MLIBERRO  PIC X(58).                                      00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MCODTRAA  PIC X.                                          00003470
           02 MCODTRAC  PIC X.                                          00003480
           02 MCODTRAP  PIC X.                                          00003490
           02 MCODTRAH  PIC X.                                          00003500
           02 MCODTRAV  PIC X.                                          00003510
           02 MCODTRAO  PIC X(4).                                       00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MCICSA    PIC X.                                          00003540
           02 MCICSC    PIC X.                                          00003550
           02 MCICSP    PIC X.                                          00003560
           02 MCICSH    PIC X.                                          00003570
           02 MCICSV    PIC X.                                          00003580
           02 MCICSO    PIC X(5).                                       00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MNETNAMA  PIC X.                                          00003610
           02 MNETNAMC  PIC X.                                          00003620
           02 MNETNAMP  PIC X.                                          00003630
           02 MNETNAMH  PIC X.                                          00003640
           02 MNETNAMV  PIC X.                                          00003650
           02 MNETNAMO  PIC X(8).                                       00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MSCREENA  PIC X.                                          00003680
           02 MSCREENC  PIC X.                                          00003690
           02 MSCREENP  PIC X.                                          00003700
           02 MSCREENH  PIC X.                                          00003710
           02 MSCREENV  PIC X.                                          00003720
           02 MSCREENO  PIC X(4).                                       00003730
                                                                                
