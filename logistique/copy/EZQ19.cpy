      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EZQ19   EZQ19                                              00000020
      ***************************************************************** 00000030
       01   EZQ19I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPQUOTAL    COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MLTYPQUOTAL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLTYPQUOTAF    PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MLTYPQUOTAI    PIC X(6).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(3).                                       00000210
           02 MDJOURD OCCURS   7 TIMES .                                00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJOURL      COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MDJOURL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDJOURF      PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MDJOURI      PIC X(8).                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEQUIPEL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MEQUIPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MEQUIPEF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MEQUIPEI  PIC X(5).                                       00000300
           02 MLIGNEI OCCURS   16 TIMES .                               00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000320
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000330
             03 FILLER  PIC X(4).                                       00000340
             03 MLIBELLEI    PIC X(8).                                  00000350
             03 MQQUOTAD OCCURS   7 TIMES .                             00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQQUOTAL   COMP PIC S9(4).                            00000370
      *--                                                                       
               04 MQQUOTAL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MQQUOTAF   PIC X.                                     00000380
               04 FILLER     PIC X(4).                                  00000390
               04 MQQUOTAI   PIC X(3).                                  00000400
             03 MQPRISD OCCURS   7 TIMES .                              00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQPRISL    COMP PIC S9(4).                            00000420
      *--                                                                       
               04 MQPRISL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MQPRISF    PIC X.                                     00000430
               04 FILLER     PIC X(4).                                  00000440
               04 MQPRISI    PIC X(3).                                  00000450
           02 MQQUOTAJD OCCURS   7 TIMES .                              00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQQUOTAJL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MQQUOTAJL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQQUOTAJF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MQQUOTAJI    PIC X(5).                                  00000500
           02 MQPRISJD OCCURS   7 TIMES .                               00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPRISJL     COMP PIC S9(4).                            00000520
      *--                                                                       
             03 MQPRISJL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQPRISJF     PIC X.                                     00000530
             03 FILLER  PIC X(4).                                       00000540
             03 MQPRISJI     PIC X(5).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MZONCMDI  PIC X(15).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MLIBERRI  PIC X(58).                                      00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCODTRAI  PIC X(4).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MCICSI    PIC X(5).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MNETNAMI  PIC X(8).                                       00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MSCREENI  PIC X(4).                                       00000790
      ***************************************************************** 00000800
      * SDF: EZQ19   EZQ19                                              00000810
      ***************************************************************** 00000820
       01   EZQ19O REDEFINES EZQ19I.                                    00000830
           02 FILLER    PIC X(12).                                      00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MDATJOUA  PIC X.                                          00000860
           02 MDATJOUC  PIC X.                                          00000870
           02 MDATJOUP  PIC X.                                          00000880
           02 MDATJOUH  PIC X.                                          00000890
           02 MDATJOUV  PIC X.                                          00000900
           02 MDATJOUO  PIC X(10).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MTIMJOUA  PIC X.                                          00000930
           02 MTIMJOUC  PIC X.                                          00000940
           02 MTIMJOUP  PIC X.                                          00000950
           02 MTIMJOUH  PIC X.                                          00000960
           02 MTIMJOUV  PIC X.                                          00000970
           02 MTIMJOUO  PIC X(5).                                       00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MLTYPQUOTAA    PIC X.                                     00001000
           02 MLTYPQUOTAC    PIC X.                                     00001010
           02 MLTYPQUOTAP    PIC X.                                     00001020
           02 MLTYPQUOTAH    PIC X.                                     00001030
           02 MLTYPQUOTAV    PIC X.                                     00001040
           02 MLTYPQUOTAO    PIC X(6).                                  00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MNPAGEA   PIC X.                                          00001070
           02 MNPAGEC   PIC X.                                          00001080
           02 MNPAGEP   PIC X.                                          00001090
           02 MNPAGEH   PIC X.                                          00001100
           02 MNPAGEV   PIC X.                                          00001110
           02 MNPAGEO   PIC X(3).                                       00001120
           02 DFHMS1 OCCURS   7 TIMES .                                 00001130
             03 FILLER       PIC X(2).                                  00001140
             03 MDJOURA      PIC X.                                     00001150
             03 MDJOURC PIC X.                                          00001160
             03 MDJOURP PIC X.                                          00001170
             03 MDJOURH PIC X.                                          00001180
             03 MDJOURV PIC X.                                          00001190
             03 MDJOURO      PIC X(8).                                  00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MEQUIPEA  PIC X.                                          00001220
           02 MEQUIPEC  PIC X.                                          00001230
           02 MEQUIPEP  PIC X.                                          00001240
           02 MEQUIPEH  PIC X.                                          00001250
           02 MEQUIPEV  PIC X.                                          00001260
           02 MEQUIPEO  PIC X(5).                                       00001270
           02 MLIGNEO OCCURS   16 TIMES .                               00001280
             03 FILLER       PIC X(2).                                  00001290
             03 MLIBELLEA    PIC X.                                     00001300
             03 MLIBELLEC    PIC X.                                     00001310
             03 MLIBELLEP    PIC X.                                     00001320
             03 MLIBELLEH    PIC X.                                     00001330
             03 MLIBELLEV    PIC X.                                     00001340
             03 MLIBELLEO    PIC X(8).                                  00001350
             03 DFHMS2 OCCURS   7 TIMES .                               00001360
               04 FILLER     PIC X(2).                                  00001370
               04 MQQUOTAA   PIC X.                                     00001380
               04 MQQUOTAC   PIC X.                                     00001390
               04 MQQUOTAP   PIC X.                                     00001400
               04 MQQUOTAH   PIC X.                                     00001410
               04 MQQUOTAV   PIC X.                                     00001420
               04 MQQUOTAO   PIC X(3).                                  00001430
             03 DFHMS3 OCCURS   7 TIMES .                               00001440
               04 FILLER     PIC X(2).                                  00001450
               04 MQPRISA    PIC X.                                     00001460
               04 MQPRISC    PIC X.                                     00001470
               04 MQPRISP    PIC X.                                     00001480
               04 MQPRISH    PIC X.                                     00001490
               04 MQPRISV    PIC X.                                     00001500
               04 MQPRISO    PIC X(3).                                  00001510
           02 DFHMS4 OCCURS   7 TIMES .                                 00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MQQUOTAJA    PIC X.                                     00001540
             03 MQQUOTAJC    PIC X.                                     00001550
             03 MQQUOTAJP    PIC X.                                     00001560
             03 MQQUOTAJH    PIC X.                                     00001570
             03 MQQUOTAJV    PIC X.                                     00001580
             03 MQQUOTAJO    PIC X(5).                                  00001590
           02 DFHMS5 OCCURS   7 TIMES .                                 00001600
             03 FILLER       PIC X(2).                                  00001610
             03 MQPRISJA     PIC X.                                     00001620
             03 MQPRISJC     PIC X.                                     00001630
             03 MQPRISJP     PIC X.                                     00001640
             03 MQPRISJH     PIC X.                                     00001650
             03 MQPRISJV     PIC X.                                     00001660
             03 MQPRISJO     PIC X(5).                                  00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MZONCMDA  PIC X.                                          00001690
           02 MZONCMDC  PIC X.                                          00001700
           02 MZONCMDP  PIC X.                                          00001710
           02 MZONCMDH  PIC X.                                          00001720
           02 MZONCMDV  PIC X.                                          00001730
           02 MZONCMDO  PIC X(15).                                      00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLIBERRA  PIC X.                                          00001760
           02 MLIBERRC  PIC X.                                          00001770
           02 MLIBERRP  PIC X.                                          00001780
           02 MLIBERRH  PIC X.                                          00001790
           02 MLIBERRV  PIC X.                                          00001800
           02 MLIBERRO  PIC X(58).                                      00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MCODTRAA  PIC X.                                          00001830
           02 MCODTRAC  PIC X.                                          00001840
           02 MCODTRAP  PIC X.                                          00001850
           02 MCODTRAH  PIC X.                                          00001860
           02 MCODTRAV  PIC X.                                          00001870
           02 MCODTRAO  PIC X(4).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MCICSA    PIC X.                                          00001900
           02 MCICSC    PIC X.                                          00001910
           02 MCICSP    PIC X.                                          00001920
           02 MCICSH    PIC X.                                          00001930
           02 MCICSV    PIC X.                                          00001940
           02 MCICSO    PIC X(5).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MNETNAMA  PIC X.                                          00001970
           02 MNETNAMC  PIC X.                                          00001980
           02 MNETNAMP  PIC X.                                          00001990
           02 MNETNAMH  PIC X.                                          00002000
           02 MNETNAMV  PIC X.                                          00002010
           02 MNETNAMO  PIC X(8).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MSCREENA  PIC X.                                          00002040
           02 MSCREENC  PIC X.                                          00002050
           02 MSCREENP  PIC X.                                          00002060
           02 MSCREENH  PIC X.                                          00002070
           02 MSCREENV  PIC X.                                          00002080
           02 MSCREENO  PIC X(4).                                       00002090
                                                                                
