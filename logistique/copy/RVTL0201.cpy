      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVTL0201                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVTL0201                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVTL0201.                                                    00000090
           02  TL02-DDELIV                                              00000100
               PIC X(0008).                                             00000110
           02  TL02-NSOC                                                00000120
               PIC X(0003).                                             00000130
           02  TL02-CPROTOUR                                            00000140
               PIC X(0005).                                             00000150
           02  TL02-CTOURNEE                                            00000160
               PIC X(0003).                                             00000170
           02  TL02-NORDRE                                              00000180
               PIC X(0002).                                             00000190
           02  TL02-NMAG                                                00000200
               PIC X(0003).                                             00000210
           02  TL02-NVENTE                                              00000220
               PIC X(0007).                                             00000230
           02  TL02-CADRTOUR                                            00000240
               PIC X(0001).                                             00000250
           02  TL02-CTYPE                                               00000260
               PIC X(0001).                                             00000270
           02  TL02-CTYPEASS                                            00000280
               PIC X(0002).                                             00000290
           02  TL02-WVAL                                                00000300
               PIC X(0001).                                             00000310
           02  TL02-DSYST                                               00000320
               PIC S9(13) COMP-3.                                       00000330
           02  TL02-CTYPLOGI                                            00000340
               PIC X(0001).                                             00000350
      *                                                                 00000360
      *---------------------------------------------------------        00000370
      *   LISTE DES FLAGS DE LA TABLE RVTL0201                          00000380
      *---------------------------------------------------------        00000390
      *                                                                 00000400
       01  RVTL0201-FLAGS.                                              00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-DDELIV-F                                            00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  TL02-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-NSOC-F                                              00000440
      *        PIC S9(4) COMP.                                          00000450
      *--                                                                       
           02  TL02-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-CPROTOUR-F                                          00000460
      *        PIC S9(4) COMP.                                          00000470
      *--                                                                       
           02  TL02-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-CTOURNEE-F                                          00000480
      *        PIC S9(4) COMP.                                          00000490
      *--                                                                       
           02  TL02-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-NORDRE-F                                            00000500
      *        PIC S9(4) COMP.                                          00000510
      *--                                                                       
           02  TL02-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-NMAG-F                                              00000520
      *        PIC S9(4) COMP.                                          00000530
      *--                                                                       
           02  TL02-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-NVENTE-F                                            00000540
      *        PIC S9(4) COMP.                                          00000550
      *--                                                                       
           02  TL02-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-CADRTOUR-F                                          00000560
      *        PIC S9(4) COMP.                                          00000570
      *--                                                                       
           02  TL02-CADRTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-CTYPE-F                                             00000580
      *        PIC S9(4) COMP.                                          00000590
      *--                                                                       
           02  TL02-CTYPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-CTYPEASS-F                                          00000600
      *        PIC S9(4) COMP.                                          00000610
      *--                                                                       
           02  TL02-CTYPEASS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-WVAL-F                                              00000620
      *        PIC S9(4) COMP.                                          00000630
      *--                                                                       
           02  TL02-WVAL-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-DSYST-F                                             00000640
      *        PIC S9(4) COMP.                                          00000650
      *--                                                                       
           02  TL02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL02-CTYPLOGI-F                                          00000660
      *        PIC S9(4) COMP.                                          00000670
      *--                                                                       
           02  TL02-CTYPLOGI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000680
                                                                                
