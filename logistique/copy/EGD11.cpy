      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD11   EGD11                                              00000020
      ***************************************************************** 00000030
       01   EGD11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MSOCIETEI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTREPOTL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MENTREPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MENTREPOTF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MENTREPOTI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURNEEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MJOURNEEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MJOURNEEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MJOURNEEI      PIC X(8).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRAFALEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNRAFALEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNRAFALEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNRAFALEI      PIC X(3).                                  00000290
           02 M182I OCCURS   14 TIMES .                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCL   COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSOCF   PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MSOCI   PIC X(3).                                       00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUL  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIEUF  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLIEUI  PIC X(3).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MLIBELLEI    PIC X(20).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELARTL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MSELARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELARTF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MSELARTI     PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNEDLL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLIGNEDLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIGNEDLF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLIGNEDLI    PIC X(4).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPIECEDLL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MPIECEDLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPIECEDLF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MPIECEDLI    PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MS1L    COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MS1L COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MS1F    PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MS1I    PIC X(2).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MS2L    COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MS2L COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MS2F    PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MS2I    PIC X(2).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MS3L    COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MS3L COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MS3F    PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MS3I    PIC X(2).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MS4L    COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MS4L COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MS4F    PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MS4I    PIC X(2).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MS5L    COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MS5L COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MS5F    PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MS5I    PIC X(2).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(15).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(58).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: EGD11   EGD11                                              00001000
      ***************************************************************** 00001010
       01   EGD11O REDEFINES EGD11I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MSOCIETEA      PIC X.                                     00001190
           02 MSOCIETEC PIC X.                                          00001200
           02 MSOCIETEP PIC X.                                          00001210
           02 MSOCIETEH PIC X.                                          00001220
           02 MSOCIETEV PIC X.                                          00001230
           02 MSOCIETEO      PIC X(3).                                  00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MENTREPOTA     PIC X.                                     00001260
           02 MENTREPOTC     PIC X.                                     00001270
           02 MENTREPOTP     PIC X.                                     00001280
           02 MENTREPOTH     PIC X.                                     00001290
           02 MENTREPOTV     PIC X.                                     00001300
           02 MENTREPOTO     PIC X(3).                                  00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MJOURNEEA      PIC X.                                     00001330
           02 MJOURNEEC PIC X.                                          00001340
           02 MJOURNEEP PIC X.                                          00001350
           02 MJOURNEEH PIC X.                                          00001360
           02 MJOURNEEV PIC X.                                          00001370
           02 MJOURNEEO      PIC X(8).                                  00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MNRAFALEA      PIC X.                                     00001400
           02 MNRAFALEC PIC X.                                          00001410
           02 MNRAFALEP PIC X.                                          00001420
           02 MNRAFALEH PIC X.                                          00001430
           02 MNRAFALEV PIC X.                                          00001440
           02 MNRAFALEO      PIC X(3).                                  00001450
           02 M182O OCCURS   14 TIMES .                                 00001460
             03 FILLER       PIC X(2).                                  00001470
             03 MSOCA   PIC X.                                          00001480
             03 MSOCC   PIC X.                                          00001490
             03 MSOCP   PIC X.                                          00001500
             03 MSOCH   PIC X.                                          00001510
             03 MSOCV   PIC X.                                          00001520
             03 MSOCO   PIC X(3).                                       00001530
             03 FILLER       PIC X(2).                                  00001540
             03 MLIEUA  PIC X.                                          00001550
             03 MLIEUC  PIC X.                                          00001560
             03 MLIEUP  PIC X.                                          00001570
             03 MLIEUH  PIC X.                                          00001580
             03 MLIEUV  PIC X.                                          00001590
             03 MLIEUO  PIC X(3).                                       00001600
             03 FILLER       PIC X(2).                                  00001610
             03 MLIBELLEA    PIC X.                                     00001620
             03 MLIBELLEC    PIC X.                                     00001630
             03 MLIBELLEP    PIC X.                                     00001640
             03 MLIBELLEH    PIC X.                                     00001650
             03 MLIBELLEV    PIC X.                                     00001660
             03 MLIBELLEO    PIC X(20).                                 00001670
             03 FILLER       PIC X(2).                                  00001680
             03 MSELARTA     PIC X.                                     00001690
             03 MSELARTC     PIC X.                                     00001700
             03 MSELARTP     PIC X.                                     00001710
             03 MSELARTH     PIC X.                                     00001720
             03 MSELARTV     PIC X.                                     00001730
             03 MSELARTO     PIC X(5).                                  00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MLIGNEDLA    PIC X.                                     00001760
             03 MLIGNEDLC    PIC X.                                     00001770
             03 MLIGNEDLP    PIC X.                                     00001780
             03 MLIGNEDLH    PIC X.                                     00001790
             03 MLIGNEDLV    PIC X.                                     00001800
             03 MLIGNEDLO    PIC X(4).                                  00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MPIECEDLA    PIC X.                                     00001830
             03 MPIECEDLC    PIC X.                                     00001840
             03 MPIECEDLP    PIC X.                                     00001850
             03 MPIECEDLH    PIC X.                                     00001860
             03 MPIECEDLV    PIC X.                                     00001870
             03 MPIECEDLO    PIC X(5).                                  00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MS1A    PIC X.                                          00001900
             03 MS1C    PIC X.                                          00001910
             03 MS1P    PIC X.                                          00001920
             03 MS1H    PIC X.                                          00001930
             03 MS1V    PIC X.                                          00001940
             03 MS1O    PIC X(2).                                       00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MS2A    PIC X.                                          00001970
             03 MS2C    PIC X.                                          00001980
             03 MS2P    PIC X.                                          00001990
             03 MS2H    PIC X.                                          00002000
             03 MS2V    PIC X.                                          00002010
             03 MS2O    PIC X(2).                                       00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MS3A    PIC X.                                          00002040
             03 MS3C    PIC X.                                          00002050
             03 MS3P    PIC X.                                          00002060
             03 MS3H    PIC X.                                          00002070
             03 MS3V    PIC X.                                          00002080
             03 MS3O    PIC X(2).                                       00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MS4A    PIC X.                                          00002110
             03 MS4C    PIC X.                                          00002120
             03 MS4P    PIC X.                                          00002130
             03 MS4H    PIC X.                                          00002140
             03 MS4V    PIC X.                                          00002150
             03 MS4O    PIC X(2).                                       00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MS5A    PIC X.                                          00002180
             03 MS5C    PIC X.                                          00002190
             03 MS5P    PIC X.                                          00002200
             03 MS5H    PIC X.                                          00002210
             03 MS5V    PIC X.                                          00002220
             03 MS5O    PIC X(2).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MZONCMDA  PIC X.                                          00002250
           02 MZONCMDC  PIC X.                                          00002260
           02 MZONCMDP  PIC X.                                          00002270
           02 MZONCMDH  PIC X.                                          00002280
           02 MZONCMDV  PIC X.                                          00002290
           02 MZONCMDO  PIC X(15).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(58).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
