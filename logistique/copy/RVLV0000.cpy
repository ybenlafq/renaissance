      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLV0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLV0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLV0000.                                                            
           02  LV00-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  LV00-NDEPOT                                                      
               PIC X(0003).                                                     
           02  LV00-NCODIC                                                      
               PIC X(0007).                                                     
           02  LV00-DMAJ                                                        
               PIC X(0008).                                                     
           02  LV00-DMAJWMS                                                     
               PIC X(0008).                                                     
           02  LV00-QPOIDS                                                      
               PIC S9(7) COMP-3.                                                
           02  LV00-QLARGEUR                                                    
               PIC S9(3) COMP-3.                                                
           02  LV00-QPROFONDEUR                                                 
               PIC S9(3) COMP-3.                                                
           02  LV00-QHAUTEUR                                                    
               PIC S9(3) COMP-3.                                                
           02  LV00-QNBPRACK                                                    
               PIC S9(5) COMP-3.                                                
           02  LV00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLV0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLV0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV00-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV00-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV00-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV00-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV00-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV00-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV00-DMAJWMS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV00-DMAJWMS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV00-QPOIDS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV00-QPOIDS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV00-QLARGEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV00-QLARGEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV00-QPROFONDEUR-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV00-QPROFONDEUR-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV00-QHAUTEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV00-QHAUTEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV00-QNBPRACK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV00-QNBPRACK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
