      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * COM-PF30-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-PF30-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-PF30-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA                                                  
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS                                   
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION                     
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC XX.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS PIC 99.                                        
              05 COMM-DATE-SEMAA PIC 99.                                        
              05 COMM-DATE-SEMNU PIC 99.                                        
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES APPLICATIVES                                            
      *                                                                         
           02  COMM-PF30-MENU.                                                  
      *=> ZONES DE SAUVEGARDE COMMUNES AU MENU PRINCIPAL                        
      *=> ET AUX TRANSACTIONS DE NIVEAU INFERIEUR                               
              03  COMM-PF30.                                                    
                  05 COMM-PF30-MZONCMD           PIC X(03).                     
                  05 COMM-PF30-FONCTION          PIC X(03).                     
                  05 COMM-PF30-NCODIC            PIC X(07).                     
                  05 COMM-PF30-CFAM              PIC X(05).                     
                  05 COMM-PF30-NSOC              PIC X(03).                     
                  05 COMM-PF30-NLIEU             PIC X(03).                     
                  05 COMM-PF30-LIBPTF            PIC X(20).                     
                  05 COMM-PF30-GRPTF             PIC X(05).                     
                  05 COMM-PF30-LIBGRPTF          PIC X(20).                     
                  05 COMM-PF30-NBPTF             PIC 9(02).                     
                  05 COMM-PF30-STKRP-MAX         PIC 9(03).                     
                  05 COMM-PF30-STKRP-MAX-MAJ     PIC 9(03).                     
                  05 COMM-PF30-STKRP-TOP-PF      PIC X(01).                     
                  05 COMM-PF30-CUMUL-SO          PIC 9(05).                     
                  05 COMM-PF30-DAT-DEB-V4S       PIC X(08).                     
                  05 COMM-PF30-DAT-FIN-V4S       PIC X(08).                     
                  05 COMM-PF30-SEM-DEB-V4S       PIC X(06).                     
                  05 COMM-PF30-SEM-FIN-V4S       PIC X(06).                     
                  05 COMM-PF30-FAMILLES.                                        
                     10 COMM-PF30-LIGNE       OCCURS 100.                       
                        15 COMM-PF30-CFAMSTKAV   PIC X(05).                     
                  05 COMM-PF30-PLATEFORMES.                                     
                     10 COMM-PF30-LIGNE       OCCURS 100.                       
                        15 COMM-PF30-NSOCGRPTF   PIC X(03).                     
                        15 COMM-PF30-NLIEUGRPTF  PIC X(03).                     
                  05 COMM-PF30-DONNEES-PF.                                      
                     10 COMM-PF30-DONNEES     OCCURS 100.                       
                        15 COMM-PF30-TSTKRP-MAX  PIC 9(05).                     
                        15 COMM-PF30-TCUMUL-SO   PIC 9(05).                     
                  05 COMM-PF30-SOCIETES.                                        
                     10 COMM-PF30-LIGNE       OCCURS 10.                        
                        15 COMM-PF30-NSOC-GROUPE PIC X(03).                     
                  05 COMM-PF30-CACID             PIC X(07).                     
                  05 COMM-PF30-NPROG             PIC X(07).                     
                  05 COMM-PF30-MAJDB             PIC X(1).                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           05 COMM-SWAP-CURS              PIC S9(4) COMP.                
      *--                                                                       
                  05 COMM-SWAP-CURS              PIC S9(4) COMP-5.              
      *}                                                                        
                  05 COMM-PF30-FILLER            PIC X(200).                    
      *=> DE NIVEAU INFERIEUR                                                   
             02  COMM-PF30-APPLI                 PIC X(1300).                   
             02  COMM-PF31-APPLI REDEFINES COMM-PF30-APPLI.                     
                 03 COMM-PF31-MODIF-DONNEE       PIC X.                         
                 03 COMM-PF31-PAGE               PIC 9(3).                      
                 03 COMM-PF31-PAGEENC            PIC 9(3).                      
                 03 COMM-PF31-PAGEMAXI           PIC 9(3).                      
                 03 COMM-PF31-NSOC               PIC X(3).                      
                 03 COMM-PF31-NLIEU              PIC X(3).                      
                 03 COMM-PF31-CFAM               PIC X(5).                      
                 03 COMM-PF31-SNCODIC            PIC X(7).                      
                 03 COMM-PF31-CMARQ              PIC X(5).                      
                 03 COMM-PF31-LREFFOURN          PIC X(20).                     
                 03 COMM-PF31-WSENSVTE           PIC X(1).                      
                 03 COMM-PF31-CEXPO              PIC X(1).                      
                 03 COMM-PF31-LSTATCOMP          PIC X(3).                      
                 03 COMM-PF31-WSENSAPPRO         PIC X(1).                      
                 03 COMM-PF31-QPIECES            PIC X(5).                      
                 03 COMM-PF31-QVENDUE            PIC X(5).                      
                 03 COMM-PF31-NB-PTF-ACTIF       PIC X(2).                      
                 03 COMM-PF31-QSO                PIC X(3).                      
                 03 COMM-PF31-ACTIVA             PIC X(1).                      
                 03 COMM-PF31-ACQSO              PIC X(3).                      
                 03 COMM-PF31-ACPRI              PIC X(1).                      
                 03 COMM-PF31-MAJDB              PIC X(1).                      
                 03 COMM-PF31-MAJDB2             PIC X(1).                      
      *                                                                         
      *=> DE NIVEAU INFERIEUR                                                   
             02  COMM-PF32-APPLI REDEFINES COMM-PF30-APPLI.                     
                 03 COMM-PF32-MODIF-DONNEE       PIC X.                         
                 03 COMM-PF32-PAGE               PIC 9(3).                      
                 03 COMM-PF32-PAGEENC            PIC 9(3).                      
                 03 COMM-PF32-PAGEMAXI           PIC 9(3).                      
                 03 COMM-PF32-NCODIC             PIC X(7).                      
                 03 COMM-PF32-LREFFOURN          PIC X(20).                     
                 03 COMM-PF32-CFAM               PIC X(5).                      
                 03 COMM-PF32-LFAM               PIC X(20).                     
                 03 COMM-PF32-WSENSVTE           PIC X(1).                      
                 03 COMM-PF32-CEXPO              PIC X(1).                      
                 03 COMM-PF32-LSTATCOMP          PIC X(3).                      
                 03 COMM-PF32-WSENSAPPRO         PIC X(1).                      
                 03 COMM-PF32-NDEPOT1            PIC X(3).                      
                 03 COMM-PF32-QSTKDEP1           PIC X(5).                      
                 03 COMM-PF32-NDEPOT2            PIC X(3).                      
                 03 COMM-PF32-QSTKDEP2           PIC X(5).                      
                 03 COMM-PF32-QV4SFIL            PIC X(5).                      
                 03 COMM-PF32-QV4SGRPTF          PIC X(5).                      
                 03 COMM-PF32-SSTKOBJ            PIC X(3).                      
                 03 COMM-PF32-SPRIORITE          PIC X(1).                      
                 03 COMM-PF32-MAJDB              PIC X(1).                      
                 03 COMM-PF32-MAJDB2             PIC X(1).                      
      *                                                                         
      *=> DE NIVEAU INFERIEUR                                                   
             02  COMM-PF33-APPLI REDEFINES COMM-PF30-APPLI.                     
                 03 COMM-PF33-PAGE               PIC 9(3).                      
                 03 COMM-PF33-PAGEENC            PIC 9(3).                      
                 03 COMM-PF33-PAGEMAXI           PIC 9(3).                      
                 03 COMM-PF33-SNCODIC            PIC X(7).                      
                 03 COMM-PF33-CFAM               PIC X(5).                      
      *                                                                         
      *=> DE NIVEAU INFERIEUR                                                   
             02  COMM-PF34-APPLI REDEFINES COMM-PF30-APPLI.                     
                 03 COMM-PF34-MODIF-DONNEE       PIC X.                         
                 03 COMM-PF34-PAGE               PIC 9(3).                      
                 03 COMM-PF34-PAGEENC            PIC 9(3).                      
                 03 COMM-PF34-PAGEMAXI           PIC 9(3).                      
                 03 COMM-PF34-NSOC               PIC X(3).                      
                 03 COMM-PF34-NLIEU              PIC X(3).                      
                 03 COMM-PF34-CFAM               PIC X(5).                      
                 03 COMM-PF34-SNCODIC            PIC X(7).                      
                 03 COMM-PF34-CMARQ              PIC X(5).                      
                 03 COMM-PF34-LREFFOURN          PIC X(20).                     
                 03 COMM-PF34-WSENSVTE           PIC X(1).                      
                 03 COMM-PF34-CEXPO              PIC X(1).                      
                 03 COMM-PF34-LSTATCOMP          PIC X(3).                      
                 03 COMM-PF34-WSENSAPPRO         PIC X(1).                      
                 03 COMM-PF34-QPIECES            PIC X(5).                      
                 03 COMM-PF34-QVENDUE            PIC X(5).                      
                 03 COMM-PF34-STKRP              PIC X(3).                      
                 03 COMM-PF34-STKDEP             PIC X(5).                      
                 03 COMM-PF34-STKOBJ             PIC X(3).                      
                 03 COMM-PF34-ACPRI              PIC X(1).                      
                 03 COMM-PF34-MAJDB              PIC X(1).                      
                 03 COMM-PF34-MAJDB2             PIC X(1).                      
      *                                                                         
      *=> DE NIVEAU INFERIEUR                                                   
             02  COMM-PF35-APPLI REDEFINES COMM-PF30-APPLI.                     
                 03 COMM-PF35-PAGE               PIC 9(3).                      
                 03 COMM-PF35-PAGEENC            PIC 9(3).                      
                 03 COMM-PF35-PAGEMAXI           PIC 9(3).                      
                 03 COMM-PF35-CPARAM             PIC X(05).                     
                 03 COMM-PF35-LPARAM             PIC X(20).                     
                 03 COMM-PF35-PAGE-EXIST         PIC 9(03).                     
                 03 COMM-PF35-POS-MAX            PIC 9(03).                     
                 03 COMM-PF35-NOLIGNE            PIC 9(03).                     
                 03 COMM-PF35-TS-LIGNE .                                        
                    08 COMM-PF35-LIGNE         OCCURS 15.                       
                       10 COMM-PF35-NSOCO          PIC X(3).                    
                       10 COMM-PF35-NLIEUO         PIC X(3).                    
                       10 COMM-PF35-SSLIEO         PIC X(5).                    
                       10 COMM-PF35-LVPARAM        PIC X(20).                   
      *------------------------------ ZONES ECRAN                               
                03 COMM-PF35-ECR-LIGNE .                                        
                   08 COMM-PF35-ELIGNE         OCCURS 15.                       
                     10 COMM-PF35-ENSOCO          PIC X(3).                     
                     10 COMM-PF35-ENLIEUO         PIC X(3).                     
                     10 COMM-PF35-ESSLIEO         PIC X(5).                     
                     10 COMM-PF35-ELVPARAM        PIC X(20).                    
      *                                                                         
                                                                                
