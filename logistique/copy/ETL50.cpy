      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ETL50   ETL50                                              00000020
      ***************************************************************** 00000030
       01   ETL50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCODICI  PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLREFFOURNI    PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLFAMI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARQI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLMARQI   PIC X(20).                                      00000410
           02 MHSI OCCURS   11 TIMES .                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MSELI   PIC X.                                          00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUENTL   COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNLIEUENTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNLIEUENTF   PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNLIEUENTI   PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNHSL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MNHSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNHSF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNHSI   PIC X(7).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQHSL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MQHSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQHSF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MQHSI   PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCDESTL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MSOCDESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSOCDESTF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MSOCDESTI    PIC X(3).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUDESTL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLIEUDESTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLIEUDESTF   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLIEUDESTI   PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSSLIEUDESTL      COMP PIC S9(4).                       00000670
      *--                                                                       
             03 MSSLIEUDESTL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MSSLIEUDESTF      PIC X.                                00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MSSLIEUDESTI      PIC X(3).                             00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMOTIFENTL  COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MCMOTIFENTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCMOTIFENTF  PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCMOTIFENTI  PIC X(10).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(15).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(58).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: ETL50   ETL50                                              00001000
      ***************************************************************** 00001010
       01   ETL50O REDEFINES ETL50I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MPAGEA    PIC X.                                          00001190
           02 MPAGEC    PIC X.                                          00001200
           02 MPAGEP    PIC X.                                          00001210
           02 MPAGEH    PIC X.                                          00001220
           02 MPAGEV    PIC X.                                          00001230
           02 MPAGEO    PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MNCODICA  PIC X.                                          00001260
           02 MNCODICC  PIC X.                                          00001270
           02 MNCODICP  PIC X.                                          00001280
           02 MNCODICH  PIC X.                                          00001290
           02 MNCODICV  PIC X.                                          00001300
           02 MNCODICO  PIC X(7).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLREFFOURNA    PIC X.                                     00001330
           02 MLREFFOURNC    PIC X.                                     00001340
           02 MLREFFOURNP    PIC X.                                     00001350
           02 MLREFFOURNH    PIC X.                                     00001360
           02 MLREFFOURNV    PIC X.                                     00001370
           02 MLREFFOURNO    PIC X(20).                                 00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCFAMA    PIC X.                                          00001400
           02 MCFAMC    PIC X.                                          00001410
           02 MCFAMP    PIC X.                                          00001420
           02 MCFAMH    PIC X.                                          00001430
           02 MCFAMV    PIC X.                                          00001440
           02 MCFAMO    PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MLFAMA    PIC X.                                          00001470
           02 MLFAMC    PIC X.                                          00001480
           02 MLFAMP    PIC X.                                          00001490
           02 MLFAMH    PIC X.                                          00001500
           02 MLFAMV    PIC X.                                          00001510
           02 MLFAMO    PIC X(20).                                      00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MCMARQA   PIC X.                                          00001540
           02 MCMARQC   PIC X.                                          00001550
           02 MCMARQP   PIC X.                                          00001560
           02 MCMARQH   PIC X.                                          00001570
           02 MCMARQV   PIC X.                                          00001580
           02 MCMARQO   PIC X(5).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MLMARQA   PIC X.                                          00001610
           02 MLMARQC   PIC X.                                          00001620
           02 MLMARQP   PIC X.                                          00001630
           02 MLMARQH   PIC X.                                          00001640
           02 MLMARQV   PIC X.                                          00001650
           02 MLMARQO   PIC X(20).                                      00001660
           02 MHSO OCCURS   11 TIMES .                                  00001670
             03 FILLER       PIC X(2).                                  00001680
             03 MSELA   PIC X.                                          00001690
             03 MSELC   PIC X.                                          00001700
             03 MSELP   PIC X.                                          00001710
             03 MSELH   PIC X.                                          00001720
             03 MSELV   PIC X.                                          00001730
             03 MSELO   PIC X.                                          00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MNLIEUENTA   PIC X.                                     00001760
             03 MNLIEUENTC   PIC X.                                     00001770
             03 MNLIEUENTP   PIC X.                                     00001780
             03 MNLIEUENTH   PIC X.                                     00001790
             03 MNLIEUENTV   PIC X.                                     00001800
             03 MNLIEUENTO   PIC X(3).                                  00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MNHSA   PIC X.                                          00001830
             03 MNHSC   PIC X.                                          00001840
             03 MNHSP   PIC X.                                          00001850
             03 MNHSH   PIC X.                                          00001860
             03 MNHSV   PIC X.                                          00001870
             03 MNHSO   PIC X(7).                                       00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MQHSA   PIC X.                                          00001900
             03 MQHSC   PIC X.                                          00001910
             03 MQHSP   PIC X.                                          00001920
             03 MQHSH   PIC X.                                          00001930
             03 MQHSV   PIC X.                                          00001940
             03 MQHSO   PIC X(5).                                       00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MSOCDESTA    PIC X.                                     00001970
             03 MSOCDESTC    PIC X.                                     00001980
             03 MSOCDESTP    PIC X.                                     00001990
             03 MSOCDESTH    PIC X.                                     00002000
             03 MSOCDESTV    PIC X.                                     00002010
             03 MSOCDESTO    PIC X(3).                                  00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MLIEUDESTA   PIC X.                                     00002040
             03 MLIEUDESTC   PIC X.                                     00002050
             03 MLIEUDESTP   PIC X.                                     00002060
             03 MLIEUDESTH   PIC X.                                     00002070
             03 MLIEUDESTV   PIC X.                                     00002080
             03 MLIEUDESTO   PIC X(3).                                  00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MSSLIEUDESTA      PIC X.                                00002110
             03 MSSLIEUDESTC PIC X.                                     00002120
             03 MSSLIEUDESTP PIC X.                                     00002130
             03 MSSLIEUDESTH PIC X.                                     00002140
             03 MSSLIEUDESTV PIC X.                                     00002150
             03 MSSLIEUDESTO      PIC X(3).                             00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MCMOTIFENTA  PIC X.                                     00002180
             03 MCMOTIFENTC  PIC X.                                     00002190
             03 MCMOTIFENTP  PIC X.                                     00002200
             03 MCMOTIFENTH  PIC X.                                     00002210
             03 MCMOTIFENTV  PIC X.                                     00002220
             03 MCMOTIFENTO  PIC X(10).                                 00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MZONCMDA  PIC X.                                          00002250
           02 MZONCMDC  PIC X.                                          00002260
           02 MZONCMDP  PIC X.                                          00002270
           02 MZONCMDH  PIC X.                                          00002280
           02 MZONCMDV  PIC X.                                          00002290
           02 MZONCMDO  PIC X(15).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(58).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
