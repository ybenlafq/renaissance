      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGQ1500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGQ1500                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGQ1500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGQ1500.                                                            
      *}                                                                        
           02  GQ15-CPROAFF                                                     
               PIC X(0005).                                                     
           02  GQ15-CFAM                                                        
               PIC X(0005).                                                     
           02  GQ15-NSOC                                                        
               PIC X(0003).                                                     
           02  GQ15-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GQ15-NPRIORITE                                                   
               PIC X(0003).                                                     
           02  GQ15-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGQ1500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGQ1500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGQ1500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ15-CPROAFF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ15-CPROAFF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ15-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ15-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ15-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ15-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ15-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ15-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ15-NPRIORITE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ15-NPRIORITE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ15-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
