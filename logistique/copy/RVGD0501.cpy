      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGD0501                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGD0501                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGD0501.                                                            
           02  GD05-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GD05-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GD05-DATELIVR                                                    
               PIC X(0008).                                                     
           02  GD05-CPROTOUR                                                    
               PIC X(0005).                                                     
           02  GD05-CTOURNEE                                                    
               PIC X(0003).                                                     
           02  GD05-NSOC                                                        
               PIC X(0003).                                                     
           02  GD05-NMAGASIN                                                    
               PIC X(0003).                                                     
           02  GD05-NVENTE                                                      
               PIC X(0007).                                                     
           02  GD05-CMODLIVR                                                    
               PIC X(0003).                                                     
           02  GD05-NCODIC                                                      
               PIC X(0007).                                                     
           02  GD05-NSEQ                                                        
               PIC X(0002).                                                     
           02  GD05-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  GD05-DRAFALE                                                     
               PIC X(0008).                                                     
           02  GD05-NRAFALE                                                     
               PIC X(0003).                                                     
           02  GD05-NSATELLITE                                                  
               PIC X(0002).                                                     
           02  GD05-NCASE                                                       
               PIC X(0003).                                                     
           02  GD05-WCOMMUT                                                     
               PIC X(0001).                                                     
           02  GD05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GD05-NBONENLV                                                    
               PIC X(0007).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGD0501                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGD0501-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-DATELIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-DATELIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-CPROTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-CTOURNEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-NMAGASIN-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-NMAGASIN-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-CMODLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-CMODLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-DRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-DRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-NRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-NRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-NSATELLITE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-NSATELLITE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-NCASE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-NCASE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-WCOMMUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-WCOMMUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD05-NBONENLV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD05-NBONENLV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
