      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS55   EGS55                                              00000020
      ***************************************************************** 00000030
       01   EGS55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCOL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MSOCOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCOF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MSOCOI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUOL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLIEUOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUOF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIEUOI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLIEUOL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSLIEUOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSLIEUOF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSLIEUOI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBOL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLIBOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIBOF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIBOI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSOCDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCDF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSOCDI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUDL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLIEUDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUDF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIEUDI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLIEUDL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MSLIEUDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSLIEUDF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSLIEUDI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBDL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLIBDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIBDF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLIBDI    PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPERL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MOPERL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MOPERF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MOPERI    PIC X(10).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOPERL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDOPERF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDOPERI   PIC X(8).                                       00000530
           02 MSTRUCTI OCCURS   12 TIMES .                              00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCODICI      PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MFAMI   PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMQEL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MMQEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMQEF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MMQEI   PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MREFI   PIC X(20).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQTEI   PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPPRETL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MTYPPRETL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MTYPPRETF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MTYPPRETI    PIC X.                                     00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENTL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCOMMENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOMMENTF    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCOMMENTI    PIC X(28).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MZONCMDI  PIC X(12).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(61).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: EGS55   EGS55                                              00001080
      ***************************************************************** 00001090
       01   EGS55O REDEFINES EGS55I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MSOCOA    PIC X.                                          00001270
           02 MSOCOC    PIC X.                                          00001280
           02 MSOCOP    PIC X.                                          00001290
           02 MSOCOH    PIC X.                                          00001300
           02 MSOCOV    PIC X.                                          00001310
           02 MSOCOO    PIC X(3).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MLIEUOA   PIC X.                                          00001340
           02 MLIEUOC   PIC X.                                          00001350
           02 MLIEUOP   PIC X.                                          00001360
           02 MLIEUOH   PIC X.                                          00001370
           02 MLIEUOV   PIC X.                                          00001380
           02 MLIEUOO   PIC X(3).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MSLIEUOA  PIC X.                                          00001410
           02 MSLIEUOC  PIC X.                                          00001420
           02 MSLIEUOP  PIC X.                                          00001430
           02 MSLIEUOH  PIC X.                                          00001440
           02 MSLIEUOV  PIC X.                                          00001450
           02 MSLIEUOO  PIC X(3).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MLIBOA    PIC X.                                          00001480
           02 MLIBOC    PIC X.                                          00001490
           02 MLIBOP    PIC X.                                          00001500
           02 MLIBOH    PIC X.                                          00001510
           02 MLIBOV    PIC X.                                          00001520
           02 MLIBOO    PIC X(20).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MSOCDA    PIC X.                                          00001550
           02 MSOCDC    PIC X.                                          00001560
           02 MSOCDP    PIC X.                                          00001570
           02 MSOCDH    PIC X.                                          00001580
           02 MSOCDV    PIC X.                                          00001590
           02 MSOCDO    PIC X(3).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MLIEUDA   PIC X.                                          00001620
           02 MLIEUDC   PIC X.                                          00001630
           02 MLIEUDP   PIC X.                                          00001640
           02 MLIEUDH   PIC X.                                          00001650
           02 MLIEUDV   PIC X.                                          00001660
           02 MLIEUDO   PIC X(3).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MSLIEUDA  PIC X.                                          00001690
           02 MSLIEUDC  PIC X.                                          00001700
           02 MSLIEUDP  PIC X.                                          00001710
           02 MSLIEUDH  PIC X.                                          00001720
           02 MSLIEUDV  PIC X.                                          00001730
           02 MSLIEUDO  PIC X(3).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLIBDA    PIC X.                                          00001760
           02 MLIBDC    PIC X.                                          00001770
           02 MLIBDP    PIC X.                                          00001780
           02 MLIBDH    PIC X.                                          00001790
           02 MLIBDV    PIC X.                                          00001800
           02 MLIBDO    PIC X(20).                                      00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MOPERA    PIC X.                                          00001830
           02 MOPERC    PIC X.                                          00001840
           02 MOPERP    PIC X.                                          00001850
           02 MOPERH    PIC X.                                          00001860
           02 MOPERV    PIC X.                                          00001870
           02 MOPERO    PIC X(10).                                      00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MDOPERA   PIC X.                                          00001900
           02 MDOPERC   PIC X.                                          00001910
           02 MDOPERP   PIC X.                                          00001920
           02 MDOPERH   PIC X.                                          00001930
           02 MDOPERV   PIC X.                                          00001940
           02 MDOPERO   PIC X(8).                                       00001950
           02 MSTRUCTO OCCURS   12 TIMES .                              00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MCODICA      PIC X.                                     00001980
             03 MCODICC PIC X.                                          00001990
             03 MCODICP PIC X.                                          00002000
             03 MCODICH PIC X.                                          00002010
             03 MCODICV PIC X.                                          00002020
             03 MCODICO      PIC X(7).                                  00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MFAMA   PIC X.                                          00002050
             03 MFAMC   PIC X.                                          00002060
             03 MFAMP   PIC X.                                          00002070
             03 MFAMH   PIC X.                                          00002080
             03 MFAMV   PIC X.                                          00002090
             03 MFAMO   PIC X(5).                                       00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MMQEA   PIC X.                                          00002120
             03 MMQEC   PIC X.                                          00002130
             03 MMQEP   PIC X.                                          00002140
             03 MMQEH   PIC X.                                          00002150
             03 MMQEV   PIC X.                                          00002160
             03 MMQEO   PIC X(5).                                       00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MREFA   PIC X.                                          00002190
             03 MREFC   PIC X.                                          00002200
             03 MREFP   PIC X.                                          00002210
             03 MREFH   PIC X.                                          00002220
             03 MREFV   PIC X.                                          00002230
             03 MREFO   PIC X(20).                                      00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MQTEA   PIC X.                                          00002260
             03 MQTEC   PIC X.                                          00002270
             03 MQTEP   PIC X.                                          00002280
             03 MQTEH   PIC X.                                          00002290
             03 MQTEV   PIC X.                                          00002300
             03 MQTEO   PIC X(5).                                       00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MTYPPRETA    PIC X.                                     00002330
             03 MTYPPRETC    PIC X.                                     00002340
             03 MTYPPRETP    PIC X.                                     00002350
             03 MTYPPRETH    PIC X.                                     00002360
             03 MTYPPRETV    PIC X.                                     00002370
             03 MTYPPRETO    PIC X.                                     00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MCOMMENTA    PIC X.                                     00002400
             03 MCOMMENTC    PIC X.                                     00002410
             03 MCOMMENTP    PIC X.                                     00002420
             03 MCOMMENTH    PIC X.                                     00002430
             03 MCOMMENTV    PIC X.                                     00002440
             03 MCOMMENTO    PIC X(28).                                 00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MZONCMDA  PIC X.                                          00002470
           02 MZONCMDC  PIC X.                                          00002480
           02 MZONCMDP  PIC X.                                          00002490
           02 MZONCMDH  PIC X.                                          00002500
           02 MZONCMDV  PIC X.                                          00002510
           02 MZONCMDO  PIC X(12).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(61).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
