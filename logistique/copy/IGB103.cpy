      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGB103 AU 16/11/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,24,BI,A,                          *        
      *                           37,05,BI,A,                          *        
      *                           42,07,BI,A,                          *        
      *                           49,03,PD,A,                          *        
      *                           52,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGB103.                                                        
            05 NOMETAT-IGB103           PIC X(6) VALUE 'IGB103'.                
            05 RUPTURES-IGB103.                                                 
           10 IGB103-NSOCENTR           PIC X(03).                      007  003
           10 IGB103-NDEPOT             PIC X(03).                      010  003
           10 IGB103-DDESTOCK           PIC X(24).                      013  024
           10 IGB103-W-SEG1             PIC X(05).                      037  005
           10 IGB103-W-SEG2             PIC X(07).                      042  007
           10 IGB103-WSEQFAM            PIC S9(05)      COMP-3.         049  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGB103-SEQUENCE           PIC S9(04) COMP.                052  002
      *--                                                                       
           10 IGB103-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGB103.                                                   
           10 IGB103-CFAM               PIC X(05).                      054  005
           10 IGB103-CSELART1           PIC X(05).                      059  005
           10 IGB103-CSELART10          PIC X(05).                      064  005
           10 IGB103-CSELART11          PIC X(05).                      069  005
           10 IGB103-CSELART12          PIC X(05).                      074  005
           10 IGB103-CSELART13          PIC X(05).                      079  005
           10 IGB103-CSELART2           PIC X(05).                      084  005
           10 IGB103-CSELART3           PIC X(05).                      089  005
           10 IGB103-CSELART4           PIC X(05).                      094  005
           10 IGB103-CSELART5           PIC X(05).                      099  005
           10 IGB103-CSELART6           PIC X(05).                      104  005
           10 IGB103-CSELART7           PIC X(05).                      109  005
           10 IGB103-CSELART8           PIC X(05).                      114  005
           10 IGB103-CSELART9           PIC X(05).                      119  005
           10 IGB103-QDEMANDEE1         PIC S9(06)      COMP-3.         124  004
           10 IGB103-QDEMANDEE10        PIC S9(06)      COMP-3.         128  004
           10 IGB103-QDEMANDEE11        PIC S9(06)      COMP-3.         132  004
           10 IGB103-QDEMANDEE12        PIC S9(06)      COMP-3.         136  004
           10 IGB103-QDEMANDEE13        PIC S9(06)      COMP-3.         140  004
           10 IGB103-QDEMANDEE2         PIC S9(06)      COMP-3.         144  004
           10 IGB103-QDEMANDEE3         PIC S9(06)      COMP-3.         148  004
           10 IGB103-QDEMANDEE4         PIC S9(06)      COMP-3.         152  004
           10 IGB103-QDEMANDEE5         PIC S9(06)      COMP-3.         156  004
           10 IGB103-QDEMANDEE6         PIC S9(06)      COMP-3.         160  004
           10 IGB103-QDEMANDEE7         PIC S9(06)      COMP-3.         164  004
           10 IGB103-QDEMANDEE8         PIC S9(06)      COMP-3.         168  004
           10 IGB103-QDEMANDEE9         PIC S9(06)      COMP-3.         172  004
            05 FILLER                      PIC X(337).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGB103-LONG           PIC S9(4)   COMP  VALUE +175.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGB103-LONG           PIC S9(4) COMP-5  VALUE +175.           
                                                                                
      *}                                                                        
