      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM55   ERM55                                              00000020
      ***************************************************************** 00000030
       01   ERM55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCGROUPI  PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCRL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCRF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCRI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGRL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNMAGRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMAGRF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMAGRI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMRL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAMRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMRF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMRI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICRL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNCODICRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICRF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCODICRI      PIC X(7).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETML      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MDEFFETML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEFFETMF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEFFETMI      PIC X(10).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETRL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MDEFFETRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEFFETRF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDEFFETRI      PIC X(10).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINEFFRL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDFINEFFRL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDFINEFFRF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDFINEFFRI     PIC X(10).                                 00000490
           02 LIGNEI OCCURS   14 TIMES .                                00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCMAGL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNSOCMAGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSOCMAGF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNSOCMAGI    PIC X(6).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNCODICI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCFAMI  PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCMARQI      PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLREFI  PIC X(20).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MQSOL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQSOF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQSOI   PIC X(3).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSML   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MQSML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQSMF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQSMI   PIC X(3).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 WBLOQUEL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 WBLOQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 WBLOQUEF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 WBLOQUEI     PIC X.                                     00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MDEFFETI     PIC X(10).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINEFFL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDFINEFFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDFINEFFF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDFINEFFI    PIC X(10).                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(12).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(61).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: ERM55   ERM55                                              00001160
      ***************************************************************** 00001170
       01   ERM55O REDEFINES ERM55I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCGROUPA  PIC X.                                          00001350
           02 MCGROUPC  PIC X.                                          00001360
           02 MCGROUPP  PIC X.                                          00001370
           02 MCGROUPH  PIC X.                                          00001380
           02 MCGROUPV  PIC X.                                          00001390
           02 MCGROUPO  PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNPAGEA   PIC X.                                          00001420
           02 MNPAGEC   PIC X.                                          00001430
           02 MNPAGEP   PIC X.                                          00001440
           02 MNPAGEH   PIC X.                                          00001450
           02 MNPAGEV   PIC X.                                          00001460
           02 MNPAGEO   PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNSOCRA   PIC X.                                          00001490
           02 MNSOCRC   PIC X.                                          00001500
           02 MNSOCRP   PIC X.                                          00001510
           02 MNSOCRH   PIC X.                                          00001520
           02 MNSOCRV   PIC X.                                          00001530
           02 MNSOCRO   PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNMAGRA   PIC X.                                          00001560
           02 MNMAGRC   PIC X.                                          00001570
           02 MNMAGRP   PIC X.                                          00001580
           02 MNMAGRH   PIC X.                                          00001590
           02 MNMAGRV   PIC X.                                          00001600
           02 MNMAGRO   PIC X(3).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCFAMRA   PIC X.                                          00001630
           02 MCFAMRC   PIC X.                                          00001640
           02 MCFAMRP   PIC X.                                          00001650
           02 MCFAMRH   PIC X.                                          00001660
           02 MCFAMRV   PIC X.                                          00001670
           02 MCFAMRO   PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNCODICRA      PIC X.                                     00001700
           02 MNCODICRC PIC X.                                          00001710
           02 MNCODICRP PIC X.                                          00001720
           02 MNCODICRH PIC X.                                          00001730
           02 MNCODICRV PIC X.                                          00001740
           02 MNCODICRO      PIC X(7).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MDEFFETMA      PIC X.                                     00001770
           02 MDEFFETMC PIC X.                                          00001780
           02 MDEFFETMP PIC X.                                          00001790
           02 MDEFFETMH PIC X.                                          00001800
           02 MDEFFETMV PIC X.                                          00001810
           02 MDEFFETMO      PIC X(10).                                 00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MDEFFETRA      PIC X.                                     00001840
           02 MDEFFETRC PIC X.                                          00001850
           02 MDEFFETRP PIC X.                                          00001860
           02 MDEFFETRH PIC X.                                          00001870
           02 MDEFFETRV PIC X.                                          00001880
           02 MDEFFETRO      PIC X(10).                                 00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MDFINEFFRA     PIC X.                                     00001910
           02 MDFINEFFRC     PIC X.                                     00001920
           02 MDFINEFFRP     PIC X.                                     00001930
           02 MDFINEFFRH     PIC X.                                     00001940
           02 MDFINEFFRV     PIC X.                                     00001950
           02 MDFINEFFRO     PIC X(10).                                 00001960
           02 LIGNEO OCCURS   14 TIMES .                                00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MNSOCMAGA    PIC X.                                     00001990
             03 MNSOCMAGC    PIC X.                                     00002000
             03 MNSOCMAGP    PIC X.                                     00002010
             03 MNSOCMAGH    PIC X.                                     00002020
             03 MNSOCMAGV    PIC X.                                     00002030
             03 MNSOCMAGO    PIC X(6).                                  00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MNCODICA     PIC X.                                     00002060
             03 MNCODICC     PIC X.                                     00002070
             03 MNCODICP     PIC X.                                     00002080
             03 MNCODICH     PIC X.                                     00002090
             03 MNCODICV     PIC X.                                     00002100
             03 MNCODICO     PIC X(7).                                  00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MCFAMA  PIC X.                                          00002130
             03 MCFAMC  PIC X.                                          00002140
             03 MCFAMP  PIC X.                                          00002150
             03 MCFAMH  PIC X.                                          00002160
             03 MCFAMV  PIC X.                                          00002170
             03 MCFAMO  PIC X(5).                                       00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MCMARQA      PIC X.                                     00002200
             03 MCMARQC PIC X.                                          00002210
             03 MCMARQP PIC X.                                          00002220
             03 MCMARQH PIC X.                                          00002230
             03 MCMARQV PIC X.                                          00002240
             03 MCMARQO      PIC X(5).                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MLREFA  PIC X.                                          00002270
             03 MLREFC  PIC X.                                          00002280
             03 MLREFP  PIC X.                                          00002290
             03 MLREFH  PIC X.                                          00002300
             03 MLREFV  PIC X.                                          00002310
             03 MLREFO  PIC X(20).                                      00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MQSOA   PIC X.                                          00002340
             03 MQSOC   PIC X.                                          00002350
             03 MQSOP   PIC X.                                          00002360
             03 MQSOH   PIC X.                                          00002370
             03 MQSOV   PIC X.                                          00002380
             03 MQSOO   PIC ZZ9.                                        00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MQSMA   PIC X.                                          00002410
             03 MQSMC   PIC X.                                          00002420
             03 MQSMP   PIC X.                                          00002430
             03 MQSMH   PIC X.                                          00002440
             03 MQSMV   PIC X.                                          00002450
             03 MQSMO   PIC ZZ9.                                        00002460
             03 FILLER       PIC X(2).                                  00002470
             03 WBLOQUEA     PIC X.                                     00002480
             03 WBLOQUEC     PIC X.                                     00002490
             03 WBLOQUEP     PIC X.                                     00002500
             03 WBLOQUEH     PIC X.                                     00002510
             03 WBLOQUEV     PIC X.                                     00002520
             03 WBLOQUEO     PIC X.                                     00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MDEFFETA     PIC X.                                     00002550
             03 MDEFFETC     PIC X.                                     00002560
             03 MDEFFETP     PIC X.                                     00002570
             03 MDEFFETH     PIC X.                                     00002580
             03 MDEFFETV     PIC X.                                     00002590
             03 MDEFFETO     PIC X(10).                                 00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MDFINEFFA    PIC X.                                     00002620
             03 MDFINEFFC    PIC X.                                     00002630
             03 MDFINEFFP    PIC X.                                     00002640
             03 MDFINEFFH    PIC X.                                     00002650
             03 MDFINEFFV    PIC X.                                     00002660
             03 MDFINEFFO    PIC X(10).                                 00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(12).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(61).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
