      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGE3000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGE3000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGE3000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGE3000.                                                            
      *}                                                                        
           02  GE30-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GE30-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GE30-CALLEE                                                      
               PIC X(0002).                                                     
           02  GE30-CNIVEAU                                                     
               PIC X(0002).                                                     
           02  GE30-NPOSITION                                                   
               PIC X(0003).                                                     
           02  GE30-CTYPEMPL                                                    
               PIC X(0001).                                                     
           02  GE30-CZONEACCES                                                  
               PIC X(0001).                                                     
           02  GE30-CCONTENEUR                                                  
               PIC X(0001).                                                     
           02  GE30-CSPECIF1                                                    
               PIC X(0001).                                                     
           02  GE30-CSPECIF2                                                    
               PIC X(0001).                                                     
           02  GE30-CSPECIF3                                                    
               PIC X(0001).                                                     
           02  GE30-CSPECIF4                                                    
               PIC X(0001).                                                     
           02  GE30-CSPECIF5                                                    
               PIC X(0001).                                                     
           02  GE30-CFETEMPL                                                    
               PIC X(0001).                                                     
           02  GE30-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GE30-NORDRE                                                      
               PIC S9(7) COMP-3.                                                
           02  GE30-NPRIORITE                                                   
               PIC X(0001).                                                     
           02  GE30-NRUPT                                                       
               PIC S9(5) COMP-3.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGE3000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGE3000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGE3000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-CALLEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-CALLEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-CNIVEAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-CNIVEAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-NPOSITION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-NPOSITION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-CTYPEMPL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-CTYPEMPL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-CZONEACCES-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-CZONEACCES-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-CCONTENEUR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-CCONTENEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-CSPECIF1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-CSPECIF1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-CSPECIF2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-CSPECIF2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-CSPECIF3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-CSPECIF3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-CSPECIF4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-CSPECIF4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-CSPECIF5-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-CSPECIF5-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-CFETEMPL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-CFETEMPL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-NPRIORITE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE30-NPRIORITE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE30-NRUPT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GE30-NRUPT-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
