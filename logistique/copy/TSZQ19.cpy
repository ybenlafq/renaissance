      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *  PROJET     : QUOTAS DECENTRALISES                             *        
      *  TITRE      : TS D'AFFICHAGE DE L'ECRAN EZQ19 (SAISIE QUOTA)   *        
      *  APPELER PAR: TZQ19 - SAISIE DES QUOTAS - PAR JOUR/ZONE LIVRAIS*        
      *                                                                *        
      *  CONTENU    : 1 ITEM PAR PAGE                                  *00000050
      *               NIVEAU 1 :  7 JOURS                              *        
      *               NIVEAU 2 : 16 ZONES DE LIVRAISON                 *        
      ******************************************************************        
      *                                                                         
      **** LONGUEUR TS ************************* 7J * (16Z * 30C) = 3920        
       01  TS-DONNEES.                                                          
           05 TS-JOUR                OCCURS 7.                                  
              10 TS-ZONE                OCCURS 16.                              
                 15 TS-CZONLIV             PIC X(05).                           
                 15 TS-QQUOTA-FICHIER      PIC S9(05) COMP-3.                   
                 15 TS-QQUOTA-ECRAN        PIC S9(05) COMP-3.                   
                 15 TS-QPRIS-ECRAN         PIC S9(05) COMP-3.                   
      *......... NUMERO DE L'ITEM (CORRESPOND A CE COUPLE JOUR/ZONE)            
      *......... DE LA TS CONTENANT TOUTES LES TRANCHES HORAIRES                
                 15 TS-RANG-TS2            PIC S9(05) COMP-3.                   
      *......... FLAG DU TAUX DE REMPLISSAGE POUR AFFICHAGE COULEUR             
                 15 TS-WREMPLISSAGE        PIC X.                               
                    88 TS-REMPLISSAGE-100                  VALUE '1'.           
                    88 TS-REMPLISSAGE-SUP-BORNE            VALUE 'S'.           
                    88 TS-REMPLISSAGE-INF-BORNE            VALUE ' '.           
AL1901           15 TS-WILLIM              PIC X.                               
                 15 TS-FILLER1             PIC X(11).                           
                                                                                
