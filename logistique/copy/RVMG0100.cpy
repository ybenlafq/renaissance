      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVMG0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVMG0100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVMG0100.                                                            
           02  MG01-CFAMMGI                                                     
               PIC X(0004).                                                     
           02  MG01-LFAMMGI                                                     
               PIC X(0020).                                                     
           02  MG01-CPSEMGI                                                     
               PIC X(0002).                                                     
           02  MG01-CTVAMGI                                                     
               PIC X(0001).                                                     
           02  MG01-CTEMGI                                                      
               PIC X(0001).                                                     
           02  MG01-CBBMGI                                                      
               PIC X(0001).                                                     
           02  MG01-CGRPMGI                                                     
               PIC X(0001).                                                     
           02  MG01-CLIVMGI                                                     
               PIC X(0001).                                                     
           02  MG01-CPRVMGI                                                     
               PIC X(0002).                                                     
           02  MG01-CCQCMGI                                                     
               PIC X(0003).                                                     
           02  MG01-CGARMGI                                                     
               PIC X(0002).                                                     
           02  MG01-CDECMGI                                                     
               PIC X(0001).                                                     
           02  MG01-CETQMGI                                                     
               PIC X(0002).                                                     
           02  MG01-QTVMGI                                                      
               PIC X(0002).                                                     
           02  MG01-QEXPMGI                                                     
               PIC X(0003).                                                     
           02  MG01-LFAMCLTMGI                                                  
               PIC X(0025).                                                     
           02  MG01-CRUB1MGI                                                    
               PIC X(0008).                                                     
           02  MG01-CRUB2MGI                                                    
               PIC X(0008).                                                     
           02  MG01-CRUB3MGI                                                    
               PIC X(0008).                                                     
           02  MG01-CRUB4MGI                                                    
               PIC X(0008).                                                     
           02  MG01-CRUB5MGI                                                    
               PIC X(0008).                                                     
           02  MG01-CRUB6MGI                                                    
               PIC X(0008).                                                     
           02  MG01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  MG01-CREMVMGI                                                    
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVMG0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVMG0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CFAMMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CFAMMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-LFAMMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-LFAMMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CPSEMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CPSEMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CTVAMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CTVAMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CTEMGI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CTEMGI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CBBMGI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CBBMGI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CGRPMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CGRPMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CLIVMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CLIVMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CPRVMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CPRVMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CCQCMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CCQCMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CGARMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CGARMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CDECMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CDECMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CETQMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CETQMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-QTVMGI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-QTVMGI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-QEXPMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-QEXPMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-LFAMCLTMGI-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-LFAMCLTMGI-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CRUB1MGI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CRUB1MGI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CRUB2MGI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CRUB2MGI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CRUB3MGI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CRUB3MGI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CRUB4MGI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CRUB4MGI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CRUB5MGI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CRUB5MGI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CRUB6MGI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CRUB6MGI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MG01-CREMVMGI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MG01-CREMVMGI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
