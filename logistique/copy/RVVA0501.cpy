      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVVA0501                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVA0501                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVVA0501.                                                            
           02  VA05-NCODIC                                                      
               PIC X(0007).                                                     
           02  VA05-NSOCVALO                                                    
               PIC X(0003).                                                     
           02  VA05-NLIEUVALO                                                   
               PIC X(0003).                                                     
           02  VA05-QSTOCKINIT                                                  
               PIC S9(11) COMP-3.                                               
           02  VA05-QSTOCKSORTIE                                                
               PIC S9(11) COMP-3.                                               
           02  VA05-QSTOCKENTREE                                                
               PIC S9(11) COMP-3.                                               
           02  VA05-QSTOCKFINAL                                                 
               PIC S9(11) COMP-3.                                               
           02  VA05-PSTOCKINIT                                                  
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA05-PSTOCKSORTIE                                                
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA05-PSTOCKENTREE                                                
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA05-PSTOCKFINAL                                                 
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  VA05-NSOCCOMPT                                                   
               PIC X(0003).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVVA0501                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVVA0501-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-NSOCVALO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-NSOCVALO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-NLIEUVALO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-NLIEUVALO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-QSTOCKINIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-QSTOCKINIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-QSTOCKSORTIE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-QSTOCKSORTIE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-QSTOCKENTREE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-QSTOCKENTREE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-QSTOCKFINAL-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-QSTOCKFINAL-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-PSTOCKINIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-PSTOCKINIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-PSTOCKSORTIE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-PSTOCKSORTIE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-PSTOCKENTREE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-PSTOCKENTREE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-PSTOCKFINAL-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-PSTOCKFINAL-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA05-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA05-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
