      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TFL02                    TR: FL02  *    00020000
      *             PARAMETRAGE DE GESTION DES DEPOTS              *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00050000
      *                                                                 00060000
          02 COMM-FL02-APPLI REDEFINES COMM-GA00-APPLI.                 00070000
      *------------------------------ CODE SOCIETE                      00080006
             03 COMM-FL02-NSOCIETE       PIC X(3).                      00090006
      *------------------------------ CODE DEPOT                        00100000
             03 COMM-FL02-NDEPOT         PIC X(3).                      00110000
      *------------------------------ LIBELLE DEPOT                     00120000
             03 COMM-FL02-LDEPOT         PIC X(20).                     00130000
      *------------------------------ TABLE FL90                        00140003
             03 COMM-FL02-DONNEES.                                      00150000
                   05 COMM-FL02-WGENGRO      PIC X(3).                  00160003
                   05 COMM-FL02-WFM          PIC X(3).                  00170003
                   05 COMM-FL02-GEMPLACT     PIC X(1).                  00180003
                   05 COMM-FL02-WQJOUR       PIC X(1).                  00190003
                   05 COMM-FL02-WGRQUAI      PIC X(1).                  00200003
                   05 COMM-FL02-WTGL00       PIC X(1).                  00210003
                   05 COMM-FL02-WTGL71       PIC X(5).                  00220003
                   05 COMM-FL02-QPRCTE       PIC 9(3)V9(2).             00230005
                   05 COMM-FL02-QVOLGD22     PIC 9(1)V9(2).             00240005
                   05 COMM-FL02-QNBMAXGD22   PIC 9(2).                  00250005
                   05 COMM-FL02-WTGD93       PIC X(1).                  00260003
                   05 COMM-FL02-WRETHS       PIC X(1).                  00270003
                   05 COMM-FL02-WRESFOUR     PIC X(1).                  00280003
                   05 COMM-FL02-WGVEMD       PIC X(1).                  00290003
                   05 COMM-FL02-QTLM         PIC 9(5).                  00300005
                   05 COMM-FL02-QELA         PIC 9(5).                  00310005
      ***************************************************************** 00320000
                                                                                
