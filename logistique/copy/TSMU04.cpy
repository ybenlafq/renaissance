      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TMU04                                          *  00020001
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 79.              00080001
       01  TS-DONNEES.                                                  00090000
              10 TS-NMUTATION   PIC X(07).                              00100001
              10 TS-DO          PIC 9(08).                              00120001
              10 TS-DF          PIC 9(08).                              00121001
              10 TS-DD          PIC 9(08).                              00122001
              10 TS-DM          PIC 9(08).                              00123001
              10 TS-SOC         PIC X(03).                              00130001
              10 TS-LIEU        PIC X(03).                              00140001
              10 TS-SELART      PIC X(05).                              00141001
              10 TS-PRO         PIC X(01).                              00150001
              10 TS-HEURE       PIC 9(02).                              00160001
              10 TS-MINUTE      PIC 9(02).                              00161001
              10 TS-QM3         PIC S9(3)V99 COMP-3.                    00180003
CL0410*       10 TS-QP          PIC S9(5)    COMP-3.                    00180103
     |        10 TS-QP          PIC S9(4)    COMP-3.                    00182001
     |        10 TS-QPM3        PIC S9(3)V99 COMP-3.                    00183004
     |        10 TS-QPP         PIC S9(4)    COMP-3.                    00184001
     |        10 TS-QEMD        PIC S9(5)    COMP-3.                    00185002
              10 TS-COMMENT     PIC X(05).                              00200001
              10 TS-DC          PIC 9(08).                              00210001
              10 TS-HCHAR       PIC X(05).                              00220001
                                                                                
