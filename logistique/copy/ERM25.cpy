      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM25   ERM25                                              00000020
      ***************************************************************** 00000030
       01   ERM25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNMAGI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLMAGI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAGREGL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLAGREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAGREGF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLAGREGI  PIC X(20).                                      00000410
           02 LIGNEI OCCURS   12 TIMES .                                00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNCODICI     PIC X(7).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCMARQI      PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLREFI  PIC X(20).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXPOL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCEXPOF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCEXPOI      PIC X.                                     00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWASSOSTDL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MWASSOSTDL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWASSOSTDF   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MWASSOSTDI   PIC X.                                     00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWASSORTL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MWASSORTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWASSORTF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MWASSORTI    PIC X.                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPOL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MQEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQEXPOF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQEXPOI      PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLSL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MQLSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQLSF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQLSI   PIC X(3).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQV8SRL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MQV8SRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQV8SRF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQV8SRI      PIC X(3).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFLAGL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MFLAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MFLAGF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MFLAGI  PIC X.                                          00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQV8SPL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQV8SPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQV8SPF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQV8SPI      PIC X(3).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOL   COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MQSOL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQSOF   PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQSOI   PIC X(3).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSML   COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MQSML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQSMF   PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQSMI   PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOPL  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MQSOPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSOPF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQSOPI  PIC X(3).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSMPL  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MQSMPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSMPF  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQSMPI  PIC X(3).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(12).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(61).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * SDF: ERM25   ERM25                                              00001280
      ***************************************************************** 00001290
       01   ERM25O REDEFINES ERM25I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNPAGEA   PIC X.                                          00001470
           02 MNPAGEC   PIC X.                                          00001480
           02 MNPAGEP   PIC X.                                          00001490
           02 MNPAGEH   PIC X.                                          00001500
           02 MNPAGEV   PIC X.                                          00001510
           02 MNPAGEO   PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNSOCA    PIC X.                                          00001540
           02 MNSOCC    PIC X.                                          00001550
           02 MNSOCP    PIC X.                                          00001560
           02 MNSOCH    PIC X.                                          00001570
           02 MNSOCV    PIC X.                                          00001580
           02 MNSOCO    PIC X(3).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNMAGA    PIC X.                                          00001610
           02 MNMAGC    PIC X.                                          00001620
           02 MNMAGP    PIC X.                                          00001630
           02 MNMAGH    PIC X.                                          00001640
           02 MNMAGV    PIC X.                                          00001650
           02 MNMAGO    PIC X(3).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLMAGA    PIC X.                                          00001680
           02 MLMAGC    PIC X.                                          00001690
           02 MLMAGP    PIC X.                                          00001700
           02 MLMAGH    PIC X.                                          00001710
           02 MLMAGV    PIC X.                                          00001720
           02 MLMAGO    PIC X(20).                                      00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCFAMA    PIC X.                                          00001750
           02 MCFAMC    PIC X.                                          00001760
           02 MCFAMP    PIC X.                                          00001770
           02 MCFAMH    PIC X.                                          00001780
           02 MCFAMV    PIC X.                                          00001790
           02 MCFAMO    PIC X(5).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MLFAMA    PIC X.                                          00001820
           02 MLFAMC    PIC X.                                          00001830
           02 MLFAMP    PIC X.                                          00001840
           02 MLFAMH    PIC X.                                          00001850
           02 MLFAMV    PIC X.                                          00001860
           02 MLFAMO    PIC X(20).                                      00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MLAGREGA  PIC X.                                          00001890
           02 MLAGREGC  PIC X.                                          00001900
           02 MLAGREGP  PIC X.                                          00001910
           02 MLAGREGH  PIC X.                                          00001920
           02 MLAGREGV  PIC X.                                          00001930
           02 MLAGREGO  PIC X(20).                                      00001940
           02 LIGNEO OCCURS   12 TIMES .                                00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MNCODICA     PIC X.                                     00001970
             03 MNCODICC     PIC X.                                     00001980
             03 MNCODICP     PIC X.                                     00001990
             03 MNCODICH     PIC X.                                     00002000
             03 MNCODICV     PIC X.                                     00002010
             03 MNCODICO     PIC X(7).                                  00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MCMARQA      PIC X.                                     00002040
             03 MCMARQC PIC X.                                          00002050
             03 MCMARQP PIC X.                                          00002060
             03 MCMARQH PIC X.                                          00002070
             03 MCMARQV PIC X.                                          00002080
             03 MCMARQO      PIC X(5).                                  00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MLREFA  PIC X.                                          00002110
             03 MLREFC  PIC X.                                          00002120
             03 MLREFP  PIC X.                                          00002130
             03 MLREFH  PIC X.                                          00002140
             03 MLREFV  PIC X.                                          00002150
             03 MLREFO  PIC X(20).                                      00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MCEXPOA      PIC X.                                     00002180
             03 MCEXPOC PIC X.                                          00002190
             03 MCEXPOP PIC X.                                          00002200
             03 MCEXPOH PIC X.                                          00002210
             03 MCEXPOV PIC X.                                          00002220
             03 MCEXPOO      PIC X.                                     00002230
             03 FILLER       PIC X(2).                                  00002240
             03 MWASSOSTDA   PIC X.                                     00002250
             03 MWASSOSTDC   PIC X.                                     00002260
             03 MWASSOSTDP   PIC X.                                     00002270
             03 MWASSOSTDH   PIC X.                                     00002280
             03 MWASSOSTDV   PIC X.                                     00002290
             03 MWASSOSTDO   PIC X.                                     00002300
             03 FILLER       PIC X(2).                                  00002310
             03 MWASSORTA    PIC X.                                     00002320
             03 MWASSORTC    PIC X.                                     00002330
             03 MWASSORTP    PIC X.                                     00002340
             03 MWASSORTH    PIC X.                                     00002350
             03 MWASSORTV    PIC X.                                     00002360
             03 MWASSORTO    PIC X.                                     00002370
             03 FILLER       PIC X(2).                                  00002380
             03 MQEXPOA      PIC X.                                     00002390
             03 MQEXPOC PIC X.                                          00002400
             03 MQEXPOP PIC X.                                          00002410
             03 MQEXPOH PIC X.                                          00002420
             03 MQEXPOV PIC X.                                          00002430
             03 MQEXPOO      PIC X(3).                                  00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MQLSA   PIC X.                                          00002460
             03 MQLSC   PIC X.                                          00002470
             03 MQLSP   PIC X.                                          00002480
             03 MQLSH   PIC X.                                          00002490
             03 MQLSV   PIC X.                                          00002500
             03 MQLSO   PIC X(3).                                       00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MQV8SRA      PIC X.                                     00002530
             03 MQV8SRC PIC X.                                          00002540
             03 MQV8SRP PIC X.                                          00002550
             03 MQV8SRH PIC X.                                          00002560
             03 MQV8SRV PIC X.                                          00002570
             03 MQV8SRO      PIC X(3).                                  00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MFLAGA  PIC X.                                          00002600
             03 MFLAGC  PIC X.                                          00002610
             03 MFLAGP  PIC X.                                          00002620
             03 MFLAGH  PIC X.                                          00002630
             03 MFLAGV  PIC X.                                          00002640
             03 MFLAGO  PIC X.                                          00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MQV8SPA      PIC X.                                     00002670
             03 MQV8SPC PIC X.                                          00002680
             03 MQV8SPP PIC X.                                          00002690
             03 MQV8SPH PIC X.                                          00002700
             03 MQV8SPV PIC X.                                          00002710
             03 MQV8SPO      PIC X(3).                                  00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MQSOA   PIC X.                                          00002740
             03 MQSOC   PIC X.                                          00002750
             03 MQSOP   PIC X.                                          00002760
             03 MQSOH   PIC X.                                          00002770
             03 MQSOV   PIC X.                                          00002780
             03 MQSOO   PIC X(3).                                       00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MQSMA   PIC X.                                          00002810
             03 MQSMC   PIC X.                                          00002820
             03 MQSMP   PIC X.                                          00002830
             03 MQSMH   PIC X.                                          00002840
             03 MQSMV   PIC X.                                          00002850
             03 MQSMO   PIC X(3).                                       00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MQSOPA  PIC X.                                          00002880
             03 MQSOPC  PIC X.                                          00002890
             03 MQSOPP  PIC X.                                          00002900
             03 MQSOPH  PIC X.                                          00002910
             03 MQSOPV  PIC X.                                          00002920
             03 MQSOPO  PIC X(3).                                       00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MQSMPA  PIC X.                                          00002950
             03 MQSMPC  PIC X.                                          00002960
             03 MQSMPP  PIC X.                                          00002970
             03 MQSMPH  PIC X.                                          00002980
             03 MQSMPV  PIC X.                                          00002990
             03 MQSMPO  PIC X(3).                                       00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MZONCMDA  PIC X.                                          00003020
           02 MZONCMDC  PIC X.                                          00003030
           02 MZONCMDP  PIC X.                                          00003040
           02 MZONCMDH  PIC X.                                          00003050
           02 MZONCMDV  PIC X.                                          00003060
           02 MZONCMDO  PIC X(12).                                      00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(61).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
