      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00010000
      *             REAPPROVISONNEMENT MAGASIN                       *  00030001
      *         - TRANSACTION RM01                                   *  00040001
      *           (CREATION/MISE A JOUR DES CODES GROUPE DE MAGASIN) *  00041001
      ****************************************************************  00050000
       01  (P)-LONG        PIC S9(4) COMP-3 VALUE +2386.                00080003
       01  (P)-DONNEES.                                                 00090002
           03  FILLER       PIC X(12).                                  00760001
           03  FILLER       PIC X(2).                                   00770001
           03  (P)-MDATJOUA PIC X.                                      00780002
           03  (P)-MDATJOUC PIC X.                                      00790002
           03  (P)-MDATJOUP PIC X.                                      00800002
           03  (P)-MDATJOUH PIC X.                                      00810002
           03  (P)-MDATJOUV PIC X.                                      00820002
           03  (P)-MDATJOUO PIC X(10).                                  00830002
           03  FILLER       PIC X(2).                                   00840001
           03  (P)-MTIMJOUA PIC X.                                      00850002
           03  (P)-MTIMJOUC PIC X.                                      00860002
           03  (P)-MTIMJOUP PIC X.                                      00870002
           03  (P)-MTIMJOUH PIC X.                                      00880002
           03  (P)-MTIMJOUV PIC X.                                      00890002
           03  (P)-MTIMJOUO PIC X(5).                                   00900002
           03  FILLER       PIC X(2).                                   00910001
           03  (P)-MNPAGEA  PIC X.                                      00920002
           03  (P)-MNPAGEC  PIC X.                                      00930002
           03  (P)-MNPAGEP  PIC X.                                      00940002
           03  (P)-MNPAGEH  PIC X.                                      00950002
           03  (P)-MNPAGEV  PIC X.                                      00960002
           03  (P)-MNPAGEO  PIC X(3).                                   00970002
           03  FILLER       PIC X(2).                                   00980001
           03  (P)-MCGROUPA PIC X.                                      00990002
           03  (P)-MCGROUPC PIC X.                                      01000002
           03  (P)-MCGROUPP PIC X.                                      01010002
           03  (P)-MCGROUPH PIC X.                                      01020002
           03  (P)-MCGROUPV PIC X.                                      01030002
           03  (P)-MCGROUPO PIC X(5).                                   01040002
           03  FILLER       PIC X(2).                                   01050001
           03  (P)-MLGROUPA PIC X.                                      01060002
           03  (P)-MLGROUPC PIC X.                                      01070002
           03  (P)-MLGROUPP PIC X.                                      01080002
           03  (P)-MLGROUPH PIC X.                                      01090002
           03  (P)-MLGROUPV PIC X.                                      01100002
           03  (P)-MLGROUPO PIC X(20).                                  01110002
           03  LNSOCO  OCCURS  45 TIMES .                               01120001
               05  FILLER        PIC X(2).                              01130001
               05  (P)-MNSOC1A   PIC X.                                 01140002
               05  (P)-MNSOC1C PIC X.                                   01150002
               05  (P)-MNSOC1P PIC X.                                   01160002
               05  (P)-MNSOC1H PIC X.                                   01170002
               05  (P)-MNSOC1V PIC X.                                   01180002
               05  (P)-MNSOC1O   PIC X(3).                              01190002
           03  LNMAGO  OCCURS  45 TIMES .                               01190003
               05  FILLER        PIC X(2).                              01200001
               05  (P)-MNMAG1A   PIC X.                                 01210002
               05  (P)-MNMAG1C PIC X.                                   01220002
               05  (P)-MNMAG1P PIC X.                                   01230002
               05  (P)-MNMAG1H PIC X.                                   01240002
               05  (P)-MNMAG1V PIC X.                                   01250002
               05  (P)-MNMAG1O   PIC X(3).                              01260002
           03  LLMAGO  OCCURS  45 TIMES .                               01260003
               05  FILLER        PIC X(2).                              01270001
               05  (P)-MLMAG1A   PIC X.                                 01280002
               05  (P)-MLMAG1C PIC X.                                   01290002
               05  (P)-MLMAG1P PIC X.                                   01300002
               05  (P)-MLMAG1H PIC X.                                   01310002
               05  (P)-MLMAG1V PIC X.                                   01320002
               05  (P)-MLMAG1O   PIC X(17).                             01330002
           03  FILLER       PIC X(2).                                   01340001
           03  (P)-MZONCMDA PIC X.                                      01350002
           03  (P)-MZONCMDC PIC X.                                      01360002
           03  (P)-MZONCMDP PIC X.                                      01370002
           03  (P)-MZONCMDH PIC X.                                      01380002
           03  (P)-MZONCMDV PIC X.                                      01390002
           03  (P)-MZONCMDO PIC X(12).                                  01400002
           03  FILLER       PIC X(2).                                   01410001
           03  (P)-MLIBERRA PIC X.                                      01420002
           03  (P)-MLIBERRC PIC X.                                      01430002
           03  (P)-MLIBERRP PIC X.                                      01440002
           03  (P)-MLIBERRH PIC X.                                      01450002
           03  (P)-MLIBERRV PIC X.                                      01460002
           03  (P)-MLIBERRO PIC X(61).                                  01470002
           03  FILLER       PIC X(2).                                   01480001
           03  (P)-MCODTRAA PIC X.                                      01490002
           03  (P)-MCODTRAC PIC X.                                      01500002
           03  (P)-MCODTRAP PIC X.                                      01510002
           03  (P)-MCODTRAH PIC X.                                      01520002
           03  (P)-MCODTRAV PIC X.                                      01530002
           03  (P)-MCODTRAO PIC X(4).                                   01540002
           03  FILLER       PIC X(2).                                   01550001
           03  (P)-MCICSA   PIC X.                                      01560002
           03  (P)-MCICSC   PIC X.                                      01570002
           03  (P)-MCICSP   PIC X.                                      01580002
           03  (P)-MCICSH   PIC X.                                      01590002
           03  (P)-MCICSV   PIC X.                                      01600002
           03  (P)-MCICSO   PIC X(5).                                   01610002
           03  FILLER       PIC X(2).                                   01620001
           03  (P)-MNETNAMA PIC X.                                      01630002
           03  (P)-MNETNAMC PIC X.                                      01640002
           03  (P)-MNETNAMP PIC X.                                      01650002
           03  (P)-MNETNAMH PIC X.                                      01660002
           03  (P)-MNETNAMV PIC X.                                      01670002
           03  (P)-MNETNAMO PIC X(8).                                   01680002
           03  FILLER       PIC X(2).                                   01690001
           03  (P)-MSCREENA PIC X.                                      01700002
           03  (P)-MSCREENC PIC X.                                      01710002
           03  (P)-MSCREENP PIC X.                                      01720002
           03  (P)-MSCREENH PIC X.                                      01730002
           03  (P)-MSCREENV PIC X.                                      01740002
           03  (P)-MSCREENO PIC X(4).                                   01750002
      *01 (P)-DONNEES-VH.                                                       
           03  LQRESER OCCURS 45 TIMES.                                         
               05  (P)-MQRESER PIC S9V99 COMP-3.                                
           03  LNTABLJOUR OCCURS 45 TIMES.                                      
               05  (P)-NTABLJOUR PIC X(2).                                      
                                                                                
