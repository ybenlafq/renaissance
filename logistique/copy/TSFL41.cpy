      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : DEFINITION DES PROFILS D'AFFILIATION (FAM / ENTREPOT ) *         
      *        POUR MISE A JOUR VUE RVGA01WN     (PGR : TFL41)        *         
      *****************************************************************         
      *                                                               *         
       01  TS-FL41.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-FL41-LONG                 PIC S9(3) COMP-3                     
                                           VALUE +58.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-FL41-DONNEES.                                                  
      *----------------------------------  MODE DE MAJ                          
      *                                         ' ' : PAS DE MAJ                
      *                                         'C' : CREATION                  
      *                                         'M' : MODIFICATION              
              03 TS-FL41-CMAJ              PIC X(1).                            
      *----------------------------------  CODIC                                
              03 TS-FL41-CDAC              PIC X(1).                            
      *----------------------------------  CODIC                                
              03 TS-FL41-CODIC             PIC X(7).                            
      *----------------------------------  LIBELLE CODIC                        
              03 TS-FL41-LCODIC            PIC X(20).                           
      *----------------------------------  CODE MARQUE DU CODIC                 
              03 TS-FL41-CDMARQ            PIC X(05).                           
      *----------------------------------  ZONE ENTREPOTS D'AFFILIATION         
              03 TS-FL41-ENTAFF            OCCURS 4.                            
      *----------------------------------  CODE SOCIETE                         
                 04 TS-FL41-CSOCAFF           PIC X(03).                        
      *----------------------------------  CODE ENTREPOT                        
                 04 TS-FL41-CDEPAFF           PIC X(03).                        
                                                                                
