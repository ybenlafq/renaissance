      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      *                                                                 00050000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00060000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00070000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00080000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00090000
      *                                                                 00100000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00110000
      * COMPRENANT :                                                    00120000
      * 1 - LES ZONES RESERVEES A AIDA                                  00130000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00140000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00150000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00160000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00170000
      *                                                                 00180000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00190000
      * PAR AIDA                                                        00200000
      *                                                                 00210000
      *-------------------------------------------------------------    00220000
      *                                                                 00230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GD00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00240001
      *--                                                                       
       01  COM-GD00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00730000
           02 COMM-GD00-APPLI.                                          00740000
              03 COMM-GD00-NSOC           PIC X(3).                     00741002
              03 COMM-GD00-NDEPOT         PIC X(3).                     00741002
              03 COMM-GD00-DJOUR          PIC X(8).                     00742002
              03 COMM-GD00-WFONC          PIC X(3).                     00743002
              03 COMM-GD00-NRAFALE        PIC X(3).                     00744002
              03 COMM-GD00-CMODSTOCK      PIC X(5).                     00745002
              03 COMM-GD00-NCODIC         PIC X(7).                     00748002
              03 COMM-GD00-MUTVIDE        PIC X.                        00748002
              03 COMM-GD00-MESSAGE        PIC X(50).                    00748002
              03 COMM-GD00-MESSAGE2       PIC X(50).                    00748002
              03 COMM-GD00-CFAM           PIC X(5).                     00748002
              03 COMM-GD00-CMARQ          PIC X(5).                     00748002
              03 COMM-GD00-LREFFOURN      PIC X(20).                    00748002
              03 COMM-GD00-QNBRANMAIL     PIC S9(3) COMP-3.             00748002
              03 COMM-GD00-QNBNIVGERB     PIC S9(3) COMP-3.             00748002
              03 COMM-GD00-QLARGEUR       PIC S9(3) COMP-3.             00748002
              03 COMM-GD00-CCOTEHOLD      PIC X.                        00748002
              03 COMM-GD00-QPROFONDEUR    PIC S9(3) COMP-3.             00748002
              03 COMM-GD00-QHAUTEUR       PIC S9(3) COMP-3.             00748002
              03 COMM-GD00-NB-SELECTIONS  PIC S9(4) COMP-3.             00748002
                 88 COMM-GD00-CREATION-RAFALE    VALUE 0.               00748002
                 88 COMM-GD00-SUPPRESSION-RAFALE VALUE 0.               00748002
              03 COMM-GD00-DHSAISIE              PIC XX.                        
              03 COMM-GD00-DMSAISIE              PIC XX.                        
              03 COMM-GD00-WETATRAFALE           PIC X.                         
      * POS-MAX = NBRE D'ITEMS DE TS CREES DANS TGD45 ************              
              03 COMM-GD00-POS-MAX               PIC X.                         
      * NIMPRIM = NO DE L'IMPRIMANTE CHOISIE POUR LES ETIQUETTES        00960000
              03 COMM-GD00-NIMPRIM               PIC X(4).                      
      * QCONDT  = QTE DOIT ETRE MULTIPLE DU CONDT UNITAIRE DANS TGD55   00960000
              03 COMM-GD00-QCONDT                PIC S9(5) COMP-3.      00748002
              03 COMM-GD00-CSELART        PIC X(05).                    00970003
              03 COMM-GD00-ZUT-OUBLI      PIC X(85).                    00970003
              03 COMM-GD00-ITEM-MAX-TS1   PIC 9(02).                    00970003
              03 COMM-GD00-LEMBALLAGE     PIC X(50).                    00970003
              03 COMM-GD00-CSPECIFSTK     PIC X(01).                    00970003
              03 COMM-GD00-CEMP1          PIC X(2).                     00748002
              03 COMM-GD00-CEMP2          PIC X(2).                     00748002
              03 COMM-GD00-CEMP3          PIC X(3).                     00748002
              03 COMM-GD00-TABLE-CALLEE.                                00970003
                 05 COMM-GD00-TB-CALLEE      PIC X(2) OCCURS 50.        00970003
              03 COMM-GD00-NSATELLITE     PIC X(2).                     00970003
      * MODIF POUR TGD50                                                        
              03 COMM-GD00-WTYPEMPL       PIC X(1).                             
              03 COMM-GD00-CALLEE1        PIC X(2).                             
              03 COMM-GD00-CALLEE2        PIC X(2).                             
      *       MODIF POUR NLG                                                    
              03 COMM-GD00-TYPUSER        PIC X(8).                             
              03 COMM-GD00-TAB-SOC.                                             
                 04 COMM-GD00-AUTOR-DEPOT      OCCURS 10.                       
                    05 COMM-GD00-AUTOR-NSOC    PIC X(3).                        
                    05 COMM-GD00-AUTOR-NDEPOT  PIC X(3).                        
      *       MODIF POUR ABC                                                    
              03 COMM-GD00-WABC           PIC X(1).                     00970003
              03 COMM-GD00-WRP            PIC X(1).                     00970003
              03 COMM-GD00-APPLI-FILLER   PIC X(09).                    00970003
              03 COMM-GD00-FILLER         PIC X(3200).                  00970003
                                                                                
