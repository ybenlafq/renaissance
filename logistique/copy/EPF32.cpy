      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Suivi codic pour ptf d'un groupe                                00000020
      ***************************************************************** 00000030
       01   EPF32I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBGRPTFL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLIBGRPTFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBGRPTFF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLIBGRPTFI     PIC X(20).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBSTKDEP1L   COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLIBSTKDEP1L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLIBSTKDEP1F   PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIBSTKDEP1I   PIC X(13).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTKDEP1L      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MSTKDEP1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSTKDEP1F      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSTKDEP1I      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCODICI  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLREFFOURNI    PIC X(15).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBSTKDEP2L   COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLIBSTKDEP2L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLIBSTKDEP2F   PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLIBSTKDEP2I   PIC X(13).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTKDEP2L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MSTKDEP2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSTKDEP2F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSTKDEP2I      PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCFAMI    PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLFAMI    PIC X(15).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOAL      COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MOAL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MOAF      PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MOAI      PIC X.                                          00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCEXPOI   PIC X.                                          00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTATCOMPL    COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MLSTATCOMPL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLSTATCOMPF    PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLSTATCOMPI    PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSAL      COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSAL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MSAF      PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSAI      PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQV4SFILL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MQV4SFILL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQV4SFILF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MQV4SFILI      PIC X(5).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGRPTFL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MGRPTFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MGRPTFF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MGRPTFI   PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQV4SGRPL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MQV4SGRPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQV4SGRPF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MQV4SGRPI      PIC X(5).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSTKOBJL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MSSTKOBJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSSTKOBJF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSSTKOBJI      PIC X(3).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSPRIORL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MSPRIORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSPRIORF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MSPRIORI  PIC X.                                          00000930
           02 M172I OCCURS   12 TIMES .                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MNSOCI  PIC X(3).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MNLIEUI      PIC X(3).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBPTFL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MLIBPTFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBPTFF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MLIBPTFI     PIC X(20).                                 00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTKRPL      COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MSTKRPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSTKRPF      PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MSTKRPI      PIC X(3).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTKOBJL     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MSTKOBJL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTKOBJF     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MSTKOBJI     PIC X(3).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIORITEL   COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MPRIORITEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MPRIORITEF   PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MPRIORITEI   PIC X.                                     00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MDEFFETI     PIC X(8).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MV4SSTKAVL   COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MV4SSTKAVL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MV4SSTKAVF   PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MV4SSTKAVI   PIC X(4).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBREFACTL   COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MNBREFACTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNBREFACTF   PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MNBREFACTI   PIC X(3).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCUMULQSOL   COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MCUMULQSOL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCUMULQSOF   PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MCUMULQSOI   PIC X(3).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAPACITEL   COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MCAPACITEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCAPACITEF   PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MCAPACITEI   PIC X(3).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MLIBERRI  PIC X(79).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MCODTRAI  PIC X(4).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCICSI    PIC X(5).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MNETNAMI  PIC X(8).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MSCREENI  PIC X(4).                                       00001580
      ***************************************************************** 00001590
      * Suivi codic pour ptf d'un groupe                                00001600
      ***************************************************************** 00001610
       01   EPF32O REDEFINES EPF32I.                                    00001620
           02 FILLER    PIC X(12).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDATJOUA  PIC X.                                          00001650
           02 MDATJOUC  PIC X.                                          00001660
           02 MDATJOUP  PIC X.                                          00001670
           02 MDATJOUH  PIC X.                                          00001680
           02 MDATJOUV  PIC X.                                          00001690
           02 MDATJOUO  PIC X(10).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MTIMJOUA  PIC X.                                          00001720
           02 MTIMJOUC  PIC X.                                          00001730
           02 MTIMJOUP  PIC X.                                          00001740
           02 MTIMJOUH  PIC X.                                          00001750
           02 MTIMJOUV  PIC X.                                          00001760
           02 MTIMJOUO  PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MPAGEA    PIC X.                                          00001790
           02 MPAGEC    PIC X.                                          00001800
           02 MPAGEP    PIC X.                                          00001810
           02 MPAGEH    PIC X.                                          00001820
           02 MPAGEV    PIC X.                                          00001830
           02 MPAGEO    PIC X(3).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MPAGEMAXA      PIC X.                                     00001860
           02 MPAGEMAXC PIC X.                                          00001870
           02 MPAGEMAXP PIC X.                                          00001880
           02 MPAGEMAXH PIC X.                                          00001890
           02 MPAGEMAXV PIC X.                                          00001900
           02 MPAGEMAXO      PIC X(3).                                  00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MLIBGRPTFA     PIC X.                                     00001930
           02 MLIBGRPTFC     PIC X.                                     00001940
           02 MLIBGRPTFP     PIC X.                                     00001950
           02 MLIBGRPTFH     PIC X.                                     00001960
           02 MLIBGRPTFV     PIC X.                                     00001970
           02 MLIBGRPTFO     PIC X(20).                                 00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MLIBSTKDEP1A   PIC X.                                     00002000
           02 MLIBSTKDEP1C   PIC X.                                     00002010
           02 MLIBSTKDEP1P   PIC X.                                     00002020
           02 MLIBSTKDEP1H   PIC X.                                     00002030
           02 MLIBSTKDEP1V   PIC X.                                     00002040
           02 MLIBSTKDEP1O   PIC X(13).                                 00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MSTKDEP1A      PIC X.                                     00002070
           02 MSTKDEP1C PIC X.                                          00002080
           02 MSTKDEP1P PIC X.                                          00002090
           02 MSTKDEP1H PIC X.                                          00002100
           02 MSTKDEP1V PIC X.                                          00002110
           02 MSTKDEP1O      PIC X(5).                                  00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MNCODICA  PIC X.                                          00002140
           02 MNCODICC  PIC X.                                          00002150
           02 MNCODICP  PIC X.                                          00002160
           02 MNCODICH  PIC X.                                          00002170
           02 MNCODICV  PIC X.                                          00002180
           02 MNCODICO  PIC X(7).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLREFFOURNA    PIC X.                                     00002210
           02 MLREFFOURNC    PIC X.                                     00002220
           02 MLREFFOURNP    PIC X.                                     00002230
           02 MLREFFOURNH    PIC X.                                     00002240
           02 MLREFFOURNV    PIC X.                                     00002250
           02 MLREFFOURNO    PIC X(15).                                 00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MLIBSTKDEP2A   PIC X.                                     00002280
           02 MLIBSTKDEP2C   PIC X.                                     00002290
           02 MLIBSTKDEP2P   PIC X.                                     00002300
           02 MLIBSTKDEP2H   PIC X.                                     00002310
           02 MLIBSTKDEP2V   PIC X.                                     00002320
           02 MLIBSTKDEP2O   PIC X(13).                                 00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MSTKDEP2A      PIC X.                                     00002350
           02 MSTKDEP2C PIC X.                                          00002360
           02 MSTKDEP2P PIC X.                                          00002370
           02 MSTKDEP2H PIC X.                                          00002380
           02 MSTKDEP2V PIC X.                                          00002390
           02 MSTKDEP2O      PIC X(5).                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MCFAMA    PIC X.                                          00002420
           02 MCFAMC    PIC X.                                          00002430
           02 MCFAMP    PIC X.                                          00002440
           02 MCFAMH    PIC X.                                          00002450
           02 MCFAMV    PIC X.                                          00002460
           02 MCFAMO    PIC X(5).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MLFAMA    PIC X.                                          00002490
           02 MLFAMC    PIC X.                                          00002500
           02 MLFAMP    PIC X.                                          00002510
           02 MLFAMH    PIC X.                                          00002520
           02 MLFAMV    PIC X.                                          00002530
           02 MLFAMO    PIC X(15).                                      00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MOAA      PIC X.                                          00002560
           02 MOAC      PIC X.                                          00002570
           02 MOAP      PIC X.                                          00002580
           02 MOAH      PIC X.                                          00002590
           02 MOAV      PIC X.                                          00002600
           02 MOAO      PIC X.                                          00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MCEXPOA   PIC X.                                          00002630
           02 MCEXPOC   PIC X.                                          00002640
           02 MCEXPOP   PIC X.                                          00002650
           02 MCEXPOH   PIC X.                                          00002660
           02 MCEXPOV   PIC X.                                          00002670
           02 MCEXPOO   PIC X.                                          00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MLSTATCOMPA    PIC X.                                     00002700
           02 MLSTATCOMPC    PIC X.                                     00002710
           02 MLSTATCOMPP    PIC X.                                     00002720
           02 MLSTATCOMPH    PIC X.                                     00002730
           02 MLSTATCOMPV    PIC X.                                     00002740
           02 MLSTATCOMPO    PIC X(3).                                  00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MSAA      PIC X.                                          00002770
           02 MSAC      PIC X.                                          00002780
           02 MSAP      PIC X.                                          00002790
           02 MSAH      PIC X.                                          00002800
           02 MSAV      PIC X.                                          00002810
           02 MSAO      PIC X.                                          00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MQV4SFILA      PIC X.                                     00002840
           02 MQV4SFILC PIC X.                                          00002850
           02 MQV4SFILP PIC X.                                          00002860
           02 MQV4SFILH PIC X.                                          00002870
           02 MQV4SFILV PIC X.                                          00002880
           02 MQV4SFILO      PIC X(5).                                  00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MGRPTFA   PIC X.                                          00002910
           02 MGRPTFC   PIC X.                                          00002920
           02 MGRPTFP   PIC X.                                          00002930
           02 MGRPTFH   PIC X.                                          00002940
           02 MGRPTFV   PIC X.                                          00002950
           02 MGRPTFO   PIC X(5).                                       00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MQV4SGRPA      PIC X.                                     00002980
           02 MQV4SGRPC PIC X.                                          00002990
           02 MQV4SGRPP PIC X.                                          00003000
           02 MQV4SGRPH PIC X.                                          00003010
           02 MQV4SGRPV PIC X.                                          00003020
           02 MQV4SGRPO      PIC X(5).                                  00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MSSTKOBJA      PIC X.                                     00003050
           02 MSSTKOBJC PIC X.                                          00003060
           02 MSSTKOBJP PIC X.                                          00003070
           02 MSSTKOBJH PIC X.                                          00003080
           02 MSSTKOBJV PIC X.                                          00003090
           02 MSSTKOBJO      PIC X(3).                                  00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MSPRIORA  PIC X.                                          00003120
           02 MSPRIORC  PIC X.                                          00003130
           02 MSPRIORP  PIC X.                                          00003140
           02 MSPRIORH  PIC X.                                          00003150
           02 MSPRIORV  PIC X.                                          00003160
           02 MSPRIORO  PIC X.                                          00003170
           02 M172O OCCURS   12 TIMES .                                 00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MNSOCA  PIC X.                                          00003200
             03 MNSOCC  PIC X.                                          00003210
             03 MNSOCP  PIC X.                                          00003220
             03 MNSOCH  PIC X.                                          00003230
             03 MNSOCV  PIC X.                                          00003240
             03 MNSOCO  PIC X(3).                                       00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MNLIEUA      PIC X.                                     00003270
             03 MNLIEUC PIC X.                                          00003280
             03 MNLIEUP PIC X.                                          00003290
             03 MNLIEUH PIC X.                                          00003300
             03 MNLIEUV PIC X.                                          00003310
             03 MNLIEUO      PIC X(3).                                  00003320
             03 FILLER       PIC X(2).                                  00003330
             03 MLIBPTFA     PIC X.                                     00003340
             03 MLIBPTFC     PIC X.                                     00003350
             03 MLIBPTFP     PIC X.                                     00003360
             03 MLIBPTFH     PIC X.                                     00003370
             03 MLIBPTFV     PIC X.                                     00003380
             03 MLIBPTFO     PIC X(20).                                 00003390
             03 FILLER       PIC X(2).                                  00003400
             03 MSTKRPA      PIC X.                                     00003410
             03 MSTKRPC PIC X.                                          00003420
             03 MSTKRPP PIC X.                                          00003430
             03 MSTKRPH PIC X.                                          00003440
             03 MSTKRPV PIC X.                                          00003450
             03 MSTKRPO      PIC X(3).                                  00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MSTKOBJA     PIC X.                                     00003480
             03 MSTKOBJC     PIC X.                                     00003490
             03 MSTKOBJP     PIC X.                                     00003500
             03 MSTKOBJH     PIC X.                                     00003510
             03 MSTKOBJV     PIC X.                                     00003520
             03 MSTKOBJO     PIC X(3).                                  00003530
             03 FILLER       PIC X(2).                                  00003540
             03 MPRIORITEA   PIC X.                                     00003550
             03 MPRIORITEC   PIC X.                                     00003560
             03 MPRIORITEP   PIC X.                                     00003570
             03 MPRIORITEH   PIC X.                                     00003580
             03 MPRIORITEV   PIC X.                                     00003590
             03 MPRIORITEO   PIC X.                                     00003600
             03 FILLER       PIC X(2).                                  00003610
             03 MDEFFETA     PIC X.                                     00003620
             03 MDEFFETC     PIC X.                                     00003630
             03 MDEFFETP     PIC X.                                     00003640
             03 MDEFFETH     PIC X.                                     00003650
             03 MDEFFETV     PIC X.                                     00003660
             03 MDEFFETO     PIC X(8).                                  00003670
             03 FILLER       PIC X(2).                                  00003680
             03 MV4SSTKAVA   PIC X.                                     00003690
             03 MV4SSTKAVC   PIC X.                                     00003700
             03 MV4SSTKAVP   PIC X.                                     00003710
             03 MV4SSTKAVH   PIC X.                                     00003720
             03 MV4SSTKAVV   PIC X.                                     00003730
             03 MV4SSTKAVO   PIC X(4).                                  00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MNBREFACTA   PIC X.                                     00003760
             03 MNBREFACTC   PIC X.                                     00003770
             03 MNBREFACTP   PIC X.                                     00003780
             03 MNBREFACTH   PIC X.                                     00003790
             03 MNBREFACTV   PIC X.                                     00003800
             03 MNBREFACTO   PIC X(3).                                  00003810
             03 FILLER       PIC X(2).                                  00003820
             03 MCUMULQSOA   PIC X.                                     00003830
             03 MCUMULQSOC   PIC X.                                     00003840
             03 MCUMULQSOP   PIC X.                                     00003850
             03 MCUMULQSOH   PIC X.                                     00003860
             03 MCUMULQSOV   PIC X.                                     00003870
             03 MCUMULQSOO   PIC X(3).                                  00003880
             03 FILLER       PIC X(2).                                  00003890
             03 MCAPACITEA   PIC X.                                     00003900
             03 MCAPACITEC   PIC X.                                     00003910
             03 MCAPACITEP   PIC X.                                     00003920
             03 MCAPACITEH   PIC X.                                     00003930
             03 MCAPACITEV   PIC X.                                     00003940
             03 MCAPACITEO   PIC X(3).                                  00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MLIBERRA  PIC X.                                          00003970
           02 MLIBERRC  PIC X.                                          00003980
           02 MLIBERRP  PIC X.                                          00003990
           02 MLIBERRH  PIC X.                                          00004000
           02 MLIBERRV  PIC X.                                          00004010
           02 MLIBERRO  PIC X(79).                                      00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MCODTRAA  PIC X.                                          00004040
           02 MCODTRAC  PIC X.                                          00004050
           02 MCODTRAP  PIC X.                                          00004060
           02 MCODTRAH  PIC X.                                          00004070
           02 MCODTRAV  PIC X.                                          00004080
           02 MCODTRAO  PIC X(4).                                       00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MCICSA    PIC X.                                          00004110
           02 MCICSC    PIC X.                                          00004120
           02 MCICSP    PIC X.                                          00004130
           02 MCICSH    PIC X.                                          00004140
           02 MCICSV    PIC X.                                          00004150
           02 MCICSO    PIC X(5).                                       00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MNETNAMA  PIC X.                                          00004180
           02 MNETNAMC  PIC X.                                          00004190
           02 MNETNAMP  PIC X.                                          00004200
           02 MNETNAMH  PIC X.                                          00004210
           02 MNETNAMV  PIC X.                                          00004220
           02 MNETNAMO  PIC X(8).                                       00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MSCREENA  PIC X.                                          00004250
           02 MSCREENC  PIC X.                                          00004260
           02 MSCREENP  PIC X.                                          00004270
           02 MSCREENH  PIC X.                                          00004280
           02 MSCREENV  PIC X.                                          00004290
           02 MSCREENO  PIC X(4).                                       00004300
                                                                                
