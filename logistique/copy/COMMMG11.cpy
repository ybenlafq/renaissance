      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  COMM-MG11-APPLI.                                                     
           05  COMM-MG11-NCODIC         PIC  X(7).                              
           05  COMM-MG11-DATEDEB        PIC  X(8).                              
           05  COMM-MG11-DATEFIN        PIC  X(8).                              
           05  COMM-MG11-DATA.                                                  
               10  COMM-MG11-PRIX.                                              
                   15 COMM-MG11-PRMP    PIC  S9(11)V9(6)  OCCURS 31.    .       
               10  COMM-MG11-DEFFET     PIC  X(8).                              
               10  COMM-MG11-TABLE      PIC  X(6).                              
               10  COMM-MG11-CODRET     PIC  S9(4).                             
               10  COMM-MG11-MESS       PIC  X(50).                             
                                                                                
