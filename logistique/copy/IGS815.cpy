      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGS815 AU 02/10/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,08,BI,A,                          *        
      *                           21,05,BI,A,                          *        
      *                           26,05,BI,A,                          *        
      *                           31,03,PD,A,                          *        
      *                           34,05,BI,A,                          *        
      *                           39,07,BI,A,                          *        
      *                           46,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGS815.                                                        
            05 NOMETAT-IGS815           PIC X(6) VALUE 'IGS815'.                
            05 RUPTURES-IGS815.                                                 
           10 IGS815-NSOCIETE           PIC X(03).                      007  003
           10 IGS815-NDEPOT             PIC X(03).                      010  003
           10 IGS815-DMUTATION          PIC X(08).                      013  008
           10 IGS815-CREC               PIC X(05).                      021  005
           10 IGS815-CQUOTA             PIC X(05).                      026  005
           10 IGS815-WSEQFAM            PIC S9(05)      COMP-3.         031  003
           10 IGS815-CMARQ              PIC X(05).                      034  005
           10 IGS815-NCODIC             PIC X(07).                      039  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGS815-SEQUENCE           PIC S9(04) COMP.                046  002
      *--                                                                       
           10 IGS815-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGS815.                                                   
           10 IGS815-CFAM               PIC X(05).                      048  005
           10 IGS815-LDEPOT             PIC X(20).                      053  020
           10 IGS815-LREC               PIC X(20).                      073  020
           10 IGS815-LREFFOURN          PIC X(20).                      093  020
           10 IGS815-NDEPOTL            PIC X(03).                      113  003
           10 IGS815-NSOCDEPOT          PIC X(03).                      116  003
           10 IGS815-QCDE               PIC S9(07)      COMP-3.         119  004
           10 IGS815-QDISPO             PIC S9(07)      COMP-3.         123  004
           10 IGS815-QMUTEE             PIC S9(05)      COMP-3.         127  003
           10 IGS815-QMUTEET            PIC S9(07)      COMP-3.         130  004
           10 IGS815-QUOCDE             PIC S9(07)      COMP-3.         134  004
           10 IGS815-QUOMUT             PIC S9(05)      COMP-3.         138  003
           10 IGS815-QUOMUTEST          PIC S9(07)      COMP-3.         141  004
           10 IGS815-QUOTAJ             PIC S9(07)      COMP-3.         145  004
            05 FILLER                      PIC X(364).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGS815-LONG           PIC S9(4)   COMP  VALUE +148.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGS815-LONG           PIC S9(4) COMP-5  VALUE +148.           
                                                                                
      *}                                                                        
