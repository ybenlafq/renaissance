      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGB0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB0000.                                                            
           02  GB00-NSOCENTR                                                    
               PIC X(0003).                                                     
           02  GB00-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GB00-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GB00-NLIEU                                                       
               PIC X(0003).                                                     
           02  GB00-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GB00-DDEBSAIS                                                    
               PIC X(0008).                                                     
           02  GB00-DFINSAIS                                                    
               PIC X(0008).                                                     
           02  GB00-DDESTOCK                                                    
               PIC X(0008).                                                     
           02  GB00-DMUTATION                                                   
               PIC X(0008).                                                     
           02  GB00-CSELART                                                     
               PIC X(0005).                                                     
           02  GB00-DMODIF                                                      
               PIC X(0008).                                                     
           02  GB00-WACTION                                                     
               PIC X(0003).                                                     
           02  GB00-IDUSER                                                      
               PIC X(0008).                                                     
           02  GB00-PROGID                                                      
               PIC X(0008).                                                     
           02  GB00-NCODIC                                                      
               PIC X(0007).                                                     
           02  GB00-QDEMINIT                                                    
               PIC S9(8) COMP-3.                                                
           02  GB00-QDEMFINALE                                                  
               PIC S9(8) COMP-3.                                                
           02  GB00-DMAJ                                                        
               PIC X(0026).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGB0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-NSOCENTR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-NSOCENTR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-DDEBSAIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-DDEBSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-DFINSAIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-DFINSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-DDESTOCK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-DDESTOCK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-DMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-DMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-CSELART-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-CSELART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-DMODIF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-DMODIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-WACTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-WACTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-IDUSER-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-IDUSER-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-PROGID-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-PROGID-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-QDEMINIT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-QDEMINIT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-QDEMFINALE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-QDEMFINALE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB00-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB00-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
