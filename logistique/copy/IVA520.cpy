      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA520 AU 23/05/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,02,BI,A,                          *        
      *                           15,20,BI,A,                          *        
      *                           35,02,BI,A,                          *        
      *                           37,07,BI,A,                          *        
      *                           44,03,PD,A,                          *        
      *                           47,05,BI,A,                          *        
      *                           52,07,BI,A,                          *        
      *                           59,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA520.                                                        
            05 NOMETAT-IVA520           PIC X(6) VALUE 'IVA520'.                
            05 RUPTURES-IVA520.                                                 
           10 IVA520-NSOCVALO           PIC X(03).                      007  003
           10 IVA520-NLIEUVALO          PIC X(03).                      010  003
           10 IVA520-CSEQRAYON1         PIC X(02).                      013  002
           10 IVA520-LENTCDE            PIC X(20).                      015  020
           10 IVA520-CSEQRAYON2         PIC X(02).                      035  002
           10 IVA520-NORIGINE           PIC X(07).                      037  007
           10 IVA520-WSEQFAM            PIC S9(05)      COMP-3.         044  003
           10 IVA520-CMARQ              PIC X(05).                      047  005
           10 IVA520-NCODIC             PIC X(07).                      052  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA520-SEQUENCE           PIC S9(04) COMP.                059  002
      *--                                                                       
           10 IVA520-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA520.                                                   
           10 IVA520-CFAM               PIC X(05).                      061  005
           10 IVA520-CRAYON1            PIC X(05).                      066  005
           10 IVA520-DEVISE             PIC X(03).                      071  003
           10 IVA520-DOPER              PIC X(06).                      074  006
           10 IVA520-LIBEURO            PIC X(02).                      080  002
           10 IVA520-LRAYON2            PIC X(20).                      082  020
           10 IVA520-LREFFOURN          PIC X(20).                      102  020
           10 IVA520-NENTCDE            PIC X(05).                      122  005
           10 IVA520-PRA                PIC S9(07)V9(2) COMP-3.         127  005
           10 IVA520-PRMP               PIC S9(07)V9(6) COMP-3.         132  007
           10 IVA520-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         139  008
           10 IVA520-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         147  008
           10 IVA520-QSTOCKENTREE       PIC S9(11)      COMP-3.         155  006
           10 IVA520-QSTOCKSORTIE       PIC S9(11)      COMP-3.         161  006
            05 FILLER                      PIC X(346).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IVA520-LONG           PIC S9(4)   COMP  VALUE +166.           
      *                                                                         
      *--                                                                       
        01  DSECT-IVA520-LONG           PIC S9(4) COMP-5  VALUE +166.           
                                                                                
      *}                                                                        
