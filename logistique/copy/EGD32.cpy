      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD32   EGD32                                              00000020
      ***************************************************************** 00000030
       01   EGD32I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MSOCIETEI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEI    PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDEPOTI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MWFONCI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAMILLEL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MFAMILLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFAMILLEF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MFAMILLEI      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMARQUEL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MMARQUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMARQUEF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MMARQUEI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF1L    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDEF1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF1F    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEF1I    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF2L    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDEF2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF2F    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDEF2I    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF3L    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MDEF3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF3F    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDEF3I    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF4L    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDEF4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF4F    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDEF4I    PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEF5L    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDEF5L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEF5F    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDEF5I    PIC X(5).                                       00000570
           02 MTABLEI OCCURS   12 TIMES .                               00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MFAMI   PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMARQL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MMARQL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MMARQF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MMARQI  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MCODL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCODF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCODI   PIC X(7).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MREFI   PIC X(20).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCK1L     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MSTOCK1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCK1F     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MSTOCK1I     PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCK2L     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MSTOCK2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCK2F     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MSTOCK2I     PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCK3L     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MSTOCK3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCK3F     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MSTOCK3I     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCK4L     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MSTOCK4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCK4F     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MSTOCK4I     PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCK5L     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MSTOCK5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTOCK5F     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MSTOCK5I     PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(78).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(11).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: EGD32   EGD32                                              00001200
      ***************************************************************** 00001210
       01   EGD32O REDEFINES EGD32I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MSOCIETEA      PIC X.                                     00001390
           02 MSOCIETEC PIC X.                                          00001400
           02 MSOCIETEP PIC X.                                          00001410
           02 MSOCIETEH PIC X.                                          00001420
           02 MSOCIETEV PIC X.                                          00001430
           02 MSOCIETEO      PIC X(3).                                  00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MPAGEA    PIC X.                                          00001460
           02 MPAGEC    PIC X.                                          00001470
           02 MPAGEP    PIC X.                                          00001480
           02 MPAGEH    PIC X.                                          00001490
           02 MPAGEV    PIC X.                                          00001500
           02 MPAGEO    PIC X(7).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MDEPOTA   PIC X.                                          00001530
           02 MDEPOTC   PIC X.                                          00001540
           02 MDEPOTP   PIC X.                                          00001550
           02 MDEPOTH   PIC X.                                          00001560
           02 MDEPOTV   PIC X.                                          00001570
           02 MDEPOTO   PIC X(3).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MWFONCA   PIC X.                                          00001600
           02 MWFONCC   PIC X.                                          00001610
           02 MWFONCP   PIC X.                                          00001620
           02 MWFONCH   PIC X.                                          00001630
           02 MWFONCV   PIC X.                                          00001640
           02 MWFONCO   PIC X(3).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MFAMILLEA      PIC X.                                     00001670
           02 MFAMILLEC PIC X.                                          00001680
           02 MFAMILLEP PIC X.                                          00001690
           02 MFAMILLEH PIC X.                                          00001700
           02 MFAMILLEV PIC X.                                          00001710
           02 MFAMILLEO      PIC X(5).                                  00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MMARQUEA  PIC X.                                          00001740
           02 MMARQUEC  PIC X.                                          00001750
           02 MMARQUEP  PIC X.                                          00001760
           02 MMARQUEH  PIC X.                                          00001770
           02 MMARQUEV  PIC X.                                          00001780
           02 MMARQUEO  PIC X(5).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MDEF1A    PIC X.                                          00001810
           02 MDEF1C    PIC X.                                          00001820
           02 MDEF1P    PIC X.                                          00001830
           02 MDEF1H    PIC X.                                          00001840
           02 MDEF1V    PIC X.                                          00001850
           02 MDEF1O    PIC X(5).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MDEF2A    PIC X.                                          00001880
           02 MDEF2C    PIC X.                                          00001890
           02 MDEF2P    PIC X.                                          00001900
           02 MDEF2H    PIC X.                                          00001910
           02 MDEF2V    PIC X.                                          00001920
           02 MDEF2O    PIC X(5).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MDEF3A    PIC X.                                          00001950
           02 MDEF3C    PIC X.                                          00001960
           02 MDEF3P    PIC X.                                          00001970
           02 MDEF3H    PIC X.                                          00001980
           02 MDEF3V    PIC X.                                          00001990
           02 MDEF3O    PIC X(5).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MDEF4A    PIC X.                                          00002020
           02 MDEF4C    PIC X.                                          00002030
           02 MDEF4P    PIC X.                                          00002040
           02 MDEF4H    PIC X.                                          00002050
           02 MDEF4V    PIC X.                                          00002060
           02 MDEF4O    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MDEF5A    PIC X.                                          00002090
           02 MDEF5C    PIC X.                                          00002100
           02 MDEF5P    PIC X.                                          00002110
           02 MDEF5H    PIC X.                                          00002120
           02 MDEF5V    PIC X.                                          00002130
           02 MDEF5O    PIC X(5).                                       00002140
           02 MTABLEO OCCURS   12 TIMES .                               00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MFAMA   PIC X.                                          00002170
             03 MFAMC   PIC X.                                          00002180
             03 MFAMP   PIC X.                                          00002190
             03 MFAMH   PIC X.                                          00002200
             03 MFAMV   PIC X.                                          00002210
             03 MFAMO   PIC X(5).                                       00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MMARQA  PIC X.                                          00002240
             03 MMARQC  PIC X.                                          00002250
             03 MMARQP  PIC X.                                          00002260
             03 MMARQH  PIC X.                                          00002270
             03 MMARQV  PIC X.                                          00002280
             03 MMARQO  PIC X(5).                                       00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MCODA   PIC X.                                          00002310
             03 MCODC   PIC X.                                          00002320
             03 MCODP   PIC X.                                          00002330
             03 MCODH   PIC X.                                          00002340
             03 MCODV   PIC X.                                          00002350
             03 MCODO   PIC X(7).                                       00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MREFA   PIC X.                                          00002380
             03 MREFC   PIC X.                                          00002390
             03 MREFP   PIC X.                                          00002400
             03 MREFH   PIC X.                                          00002410
             03 MREFV   PIC X.                                          00002420
             03 MREFO   PIC X(20).                                      00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MSTOCK1A     PIC X.                                     00002450
             03 MSTOCK1C     PIC X.                                     00002460
             03 MSTOCK1P     PIC X.                                     00002470
             03 MSTOCK1H     PIC X.                                     00002480
             03 MSTOCK1V     PIC X.                                     00002490
             03 MSTOCK1O     PIC X(5).                                  00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MSTOCK2A     PIC X.                                     00002520
             03 MSTOCK2C     PIC X.                                     00002530
             03 MSTOCK2P     PIC X.                                     00002540
             03 MSTOCK2H     PIC X.                                     00002550
             03 MSTOCK2V     PIC X.                                     00002560
             03 MSTOCK2O     PIC X(5).                                  00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MSTOCK3A     PIC X.                                     00002590
             03 MSTOCK3C     PIC X.                                     00002600
             03 MSTOCK3P     PIC X.                                     00002610
             03 MSTOCK3H     PIC X.                                     00002620
             03 MSTOCK3V     PIC X.                                     00002630
             03 MSTOCK3O     PIC X(5).                                  00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MSTOCK4A     PIC X.                                     00002660
             03 MSTOCK4C     PIC X.                                     00002670
             03 MSTOCK4P     PIC X.                                     00002680
             03 MSTOCK4H     PIC X.                                     00002690
             03 MSTOCK4V     PIC X.                                     00002700
             03 MSTOCK4O     PIC X(5).                                  00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MSTOCK5A     PIC X.                                     00002730
             03 MSTOCK5C     PIC X.                                     00002740
             03 MSTOCK5P     PIC X.                                     00002750
             03 MSTOCK5H     PIC X.                                     00002760
             03 MSTOCK5V     PIC X.                                     00002770
             03 MSTOCK5O     PIC X(5).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MLIBERRA  PIC X.                                          00002800
           02 MLIBERRC  PIC X.                                          00002810
           02 MLIBERRP  PIC X.                                          00002820
           02 MLIBERRH  PIC X.                                          00002830
           02 MLIBERRV  PIC X.                                          00002840
           02 MLIBERRO  PIC X(78).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MCODTRAA  PIC X.                                          00002870
           02 MCODTRAC  PIC X.                                          00002880
           02 MCODTRAP  PIC X.                                          00002890
           02 MCODTRAH  PIC X.                                          00002900
           02 MCODTRAV  PIC X.                                          00002910
           02 MCODTRAO  PIC X(4).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MZONCMDA  PIC X.                                          00002940
           02 MZONCMDC  PIC X.                                          00002950
           02 MZONCMDP  PIC X.                                          00002960
           02 MZONCMDH  PIC X.                                          00002970
           02 MZONCMDV  PIC X.                                          00002980
           02 MZONCMDO  PIC X(11).                                      00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
