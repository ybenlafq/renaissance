      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU72   EMU72                                              00000020
      ***************************************************************** 00000030
       01   EMU72I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCENTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSOCENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCENTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCENTI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTL     COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MENTL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MENTF     PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MENTI     PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBENTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLIBENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBENTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLIBENTI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBCLIL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLIBCLIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBCLIF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIBCLII  PIC X(18).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCPTFL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MSOCPTFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCPTFF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSOCPTFI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPTFL     COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPTFL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPTFF     PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPTFI     PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBPTFL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLIBPTFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBPTFF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIBPTFI  PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDELIVL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDELIVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDELIVF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDELIVI   PIC X(10).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCMAGL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MNSOCMAGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCMAGF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNSOCMAGI      PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNMAGI    PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNVENTEI  PIC X(7).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTACTL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNMUTACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMUTACTF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNMUTACTI      PIC X(7).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELACTL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSELACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSELACTF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSELACTI  PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATMUTL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MDATMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATMUTF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDATMUTI  PIC X(10).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVOLL     COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MVOLL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MVOLF     PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MVOLI     PIC X(6).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPACTL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNBPACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPACTF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNBPACTI  PIC X(3).                                       00000850
           02 MLIGNEI OCCURS   8 TIMES .                                00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDELIVL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDDELIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDDELIVF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDDELIVI     PIC X(8).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTL  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MNMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMUTF  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MNMUTI  PIC X(7).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVDISL      COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MQVDISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQVDISF      PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQVDISI      PIC X(6).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVPRISL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MQVPRISL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQVPRISF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQVPRISI     PIC X(6).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPDISL      COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MQPDISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPDISF      PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQPDISI      PIC X(5).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPPRISL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MQPPRISL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQPPRISF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MQPPRISI     PIC X(5).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELARTL     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MSELARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELARTF     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MSELARTI     PIC X(5).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MSELI   PIC X.                                          00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MZONCMDI  PIC X(15).                                      00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MLIBERRI  PIC X(58).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCODTRAI  PIC X(4).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCICSI    PIC X(5).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MNETNAMI  PIC X(8).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MSCREENI  PIC X(4).                                       00001420
      ***************************************************************** 00001430
      * SDF: EMU72   EMU72                                              00001440
      ***************************************************************** 00001450
       01   EMU72O REDEFINES EMU72I.                                    00001460
           02 FILLER    PIC X(12).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MDATJOUA  PIC X.                                          00001490
           02 MDATJOUC  PIC X.                                          00001500
           02 MDATJOUP  PIC X.                                          00001510
           02 MDATJOUH  PIC X.                                          00001520
           02 MDATJOUV  PIC X.                                          00001530
           02 MDATJOUO  PIC X(10).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MTIMJOUA  PIC X.                                          00001560
           02 MTIMJOUC  PIC X.                                          00001570
           02 MTIMJOUP  PIC X.                                          00001580
           02 MTIMJOUH  PIC X.                                          00001590
           02 MTIMJOUV  PIC X.                                          00001600
           02 MTIMJOUO  PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNPAGEA   PIC X.                                          00001630
           02 MNPAGEC   PIC X.                                          00001640
           02 MNPAGEP   PIC X.                                          00001650
           02 MNPAGEH   PIC X.                                          00001660
           02 MNPAGEV   PIC X.                                          00001670
           02 MNPAGEO   PIC ZZZ9.                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNBPA     PIC X.                                          00001700
           02 MNBPC     PIC X.                                          00001710
           02 MNBPP     PIC X.                                          00001720
           02 MNBPH     PIC X.                                          00001730
           02 MNBPV     PIC X.                                          00001740
           02 MNBPO     PIC X(4).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MSOCENTA  PIC X.                                          00001770
           02 MSOCENTC  PIC X.                                          00001780
           02 MSOCENTP  PIC X.                                          00001790
           02 MSOCENTH  PIC X.                                          00001800
           02 MSOCENTV  PIC X.                                          00001810
           02 MSOCENTO  PIC X(3).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MENTA     PIC X.                                          00001840
           02 MENTC     PIC X.                                          00001850
           02 MENTP     PIC X.                                          00001860
           02 MENTH     PIC X.                                          00001870
           02 MENTV     PIC X.                                          00001880
           02 MENTO     PIC X(3).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MLIBENTA  PIC X.                                          00001910
           02 MLIBENTC  PIC X.                                          00001920
           02 MLIBENTP  PIC X.                                          00001930
           02 MLIBENTH  PIC X.                                          00001940
           02 MLIBENTV  PIC X.                                          00001950
           02 MLIBENTO  PIC X(20).                                      00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MLIBCLIA  PIC X.                                          00001980
           02 MLIBCLIC  PIC X.                                          00001990
           02 MLIBCLIP  PIC X.                                          00002000
           02 MLIBCLIH  PIC X.                                          00002010
           02 MLIBCLIV  PIC X.                                          00002020
           02 MLIBCLIO  PIC X(18).                                      00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSOCPTFA  PIC X.                                          00002050
           02 MSOCPTFC  PIC X.                                          00002060
           02 MSOCPTFP  PIC X.                                          00002070
           02 MSOCPTFH  PIC X.                                          00002080
           02 MSOCPTFV  PIC X.                                          00002090
           02 MSOCPTFO  PIC X(3).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MPTFA     PIC X.                                          00002120
           02 MPTFC     PIC X.                                          00002130
           02 MPTFP     PIC X.                                          00002140
           02 MPTFH     PIC X.                                          00002150
           02 MPTFV     PIC X.                                          00002160
           02 MPTFO     PIC X(3).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLIBPTFA  PIC X.                                          00002190
           02 MLIBPTFC  PIC X.                                          00002200
           02 MLIBPTFP  PIC X.                                          00002210
           02 MLIBPTFH  PIC X.                                          00002220
           02 MLIBPTFV  PIC X.                                          00002230
           02 MLIBPTFO  PIC X(20).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MDELIVA   PIC X.                                          00002260
           02 MDELIVC   PIC X.                                          00002270
           02 MDELIVP   PIC X.                                          00002280
           02 MDELIVH   PIC X.                                          00002290
           02 MDELIVV   PIC X.                                          00002300
           02 MDELIVO   PIC X(10).                                      00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MNSOCMAGA      PIC X.                                     00002330
           02 MNSOCMAGC PIC X.                                          00002340
           02 MNSOCMAGP PIC X.                                          00002350
           02 MNSOCMAGH PIC X.                                          00002360
           02 MNSOCMAGV PIC X.                                          00002370
           02 MNSOCMAGO      PIC X(3).                                  00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MNMAGA    PIC X.                                          00002400
           02 MNMAGC    PIC X.                                          00002410
           02 MNMAGP    PIC X.                                          00002420
           02 MNMAGH    PIC X.                                          00002430
           02 MNMAGV    PIC X.                                          00002440
           02 MNMAGO    PIC X(3).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MNVENTEA  PIC X.                                          00002470
           02 MNVENTEC  PIC X.                                          00002480
           02 MNVENTEP  PIC X.                                          00002490
           02 MNVENTEH  PIC X.                                          00002500
           02 MNVENTEV  PIC X.                                          00002510
           02 MNVENTEO  PIC X(7).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MNMUTACTA      PIC X.                                     00002540
           02 MNMUTACTC PIC X.                                          00002550
           02 MNMUTACTP PIC X.                                          00002560
           02 MNMUTACTH PIC X.                                          00002570
           02 MNMUTACTV PIC X.                                          00002580
           02 MNMUTACTO      PIC X(7).                                  00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MSELACTA  PIC X.                                          00002610
           02 MSELACTC  PIC X.                                          00002620
           02 MSELACTP  PIC X.                                          00002630
           02 MSELACTH  PIC X.                                          00002640
           02 MSELACTV  PIC X.                                          00002650
           02 MSELACTO  PIC X(5).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MDATMUTA  PIC X.                                          00002680
           02 MDATMUTC  PIC X.                                          00002690
           02 MDATMUTP  PIC X.                                          00002700
           02 MDATMUTH  PIC X.                                          00002710
           02 MDATMUTV  PIC X.                                          00002720
           02 MDATMUTO  PIC X(10).                                      00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MVOLA     PIC X.                                          00002750
           02 MVOLC     PIC X.                                          00002760
           02 MVOLP     PIC X.                                          00002770
           02 MVOLH     PIC X.                                          00002780
           02 MVOLV     PIC X.                                          00002790
           02 MVOLO     PIC X(6).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MNBPACTA  PIC X.                                          00002820
           02 MNBPACTC  PIC X.                                          00002830
           02 MNBPACTP  PIC X.                                          00002840
           02 MNBPACTH  PIC X.                                          00002850
           02 MNBPACTV  PIC X.                                          00002860
           02 MNBPACTO  PIC X(3).                                       00002870
           02 MLIGNEO OCCURS   8 TIMES .                                00002880
             03 FILLER       PIC X(2).                                  00002890
             03 MDDELIVA     PIC X.                                     00002900
             03 MDDELIVC     PIC X.                                     00002910
             03 MDDELIVP     PIC X.                                     00002920
             03 MDDELIVH     PIC X.                                     00002930
             03 MDDELIVV     PIC X.                                     00002940
             03 MDDELIVO     PIC X(8).                                  00002950
             03 FILLER       PIC X(2).                                  00002960
             03 MNMUTA  PIC X.                                          00002970
             03 MNMUTC  PIC X.                                          00002980
             03 MNMUTP  PIC X.                                          00002990
             03 MNMUTH  PIC X.                                          00003000
             03 MNMUTV  PIC X.                                          00003010
             03 MNMUTO  PIC X(7).                                       00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MQVDISA      PIC X.                                     00003040
             03 MQVDISC PIC X.                                          00003050
             03 MQVDISP PIC X.                                          00003060
             03 MQVDISH PIC X.                                          00003070
             03 MQVDISV PIC X.                                          00003080
             03 MQVDISO      PIC X(6).                                  00003090
             03 FILLER       PIC X(2).                                  00003100
             03 MQVPRISA     PIC X.                                     00003110
             03 MQVPRISC     PIC X.                                     00003120
             03 MQVPRISP     PIC X.                                     00003130
             03 MQVPRISH     PIC X.                                     00003140
             03 MQVPRISV     PIC X.                                     00003150
             03 MQVPRISO     PIC X(6).                                  00003160
             03 FILLER       PIC X(2).                                  00003170
             03 MQPDISA      PIC X.                                     00003180
             03 MQPDISC PIC X.                                          00003190
             03 MQPDISP PIC X.                                          00003200
             03 MQPDISH PIC X.                                          00003210
             03 MQPDISV PIC X.                                          00003220
             03 MQPDISO      PIC X(5).                                  00003230
             03 FILLER       PIC X(2).                                  00003240
             03 MQPPRISA     PIC X.                                     00003250
             03 MQPPRISC     PIC X.                                     00003260
             03 MQPPRISP     PIC X.                                     00003270
             03 MQPPRISH     PIC X.                                     00003280
             03 MQPPRISV     PIC X.                                     00003290
             03 MQPPRISO     PIC X(5).                                  00003300
             03 FILLER       PIC X(2).                                  00003310
             03 MSELARTA     PIC X.                                     00003320
             03 MSELARTC     PIC X.                                     00003330
             03 MSELARTP     PIC X.                                     00003340
             03 MSELARTH     PIC X.                                     00003350
             03 MSELARTV     PIC X.                                     00003360
             03 MSELARTO     PIC X(5).                                  00003370
             03 FILLER       PIC X(2).                                  00003380
             03 MSELA   PIC X.                                          00003390
             03 MSELC   PIC X.                                          00003400
             03 MSELP   PIC X.                                          00003410
             03 MSELH   PIC X.                                          00003420
             03 MSELV   PIC X.                                          00003430
             03 MSELO   PIC X.                                          00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MZONCMDA  PIC X.                                          00003460
           02 MZONCMDC  PIC X.                                          00003470
           02 MZONCMDP  PIC X.                                          00003480
           02 MZONCMDH  PIC X.                                          00003490
           02 MZONCMDV  PIC X.                                          00003500
           02 MZONCMDO  PIC X(15).                                      00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MLIBERRA  PIC X.                                          00003530
           02 MLIBERRC  PIC X.                                          00003540
           02 MLIBERRP  PIC X.                                          00003550
           02 MLIBERRH  PIC X.                                          00003560
           02 MLIBERRV  PIC X.                                          00003570
           02 MLIBERRO  PIC X(58).                                      00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MCODTRAA  PIC X.                                          00003600
           02 MCODTRAC  PIC X.                                          00003610
           02 MCODTRAP  PIC X.                                          00003620
           02 MCODTRAH  PIC X.                                          00003630
           02 MCODTRAV  PIC X.                                          00003640
           02 MCODTRAO  PIC X(4).                                       00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MCICSA    PIC X.                                          00003670
           02 MCICSC    PIC X.                                          00003680
           02 MCICSP    PIC X.                                          00003690
           02 MCICSH    PIC X.                                          00003700
           02 MCICSV    PIC X.                                          00003710
           02 MCICSO    PIC X(5).                                       00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MNETNAMA  PIC X.                                          00003740
           02 MNETNAMC  PIC X.                                          00003750
           02 MNETNAMP  PIC X.                                          00003760
           02 MNETNAMH  PIC X.                                          00003770
           02 MNETNAMV  PIC X.                                          00003780
           02 MNETNAMO  PIC X(8).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MSCREENA  PIC X.                                          00003810
           02 MSCREENC  PIC X.                                          00003820
           02 MSCREENP  PIC X.                                          00003830
           02 MSCREENH  PIC X.                                          00003840
           02 MSCREENV  PIC X.                                          00003850
           02 MSCREENO  PIC X(4).                                       00003860
                                                                                
