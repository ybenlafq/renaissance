      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGS070 AU 05/04/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,07,BI,A,                          *        
      *                           32,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGS070.                                                        
            05 NOMETAT-IGS070           PIC X(6) VALUE 'IGS070'.                
            05 RUPTURES-IGS070.                                                 
           10 IGS070-NLIEU              PIC X(03).                      007  003
           10 IGS070-CRAYON             PIC X(05).                      010  005
           10 IGS070-CFAM               PIC X(05).                      015  005
           10 IGS070-CMARQ              PIC X(05).                      020  005
           10 IGS070-NCODIC             PIC X(07).                      025  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGS070-SEQUENCE           PIC S9(04) COMP.                032  002
      *--                                                                       
           10 IGS070-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGS070.                                                   
           10 IGS070-LLIEU              PIC X(20).                      034  020
           10 IGS070-LREFFOURN          PIC X(20).                      054  020
           10 IGS070-NSOCIETE           PIC X(03).                      074  003
           10 IGS070-DATE               PIC X(08).                      077  008
           10 IGS070-QTE                PIC S9(05)     .                085  005
            05 FILLER                      PIC X(423).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGS070-LONG           PIC S9(4)   COMP  VALUE +089.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGS070-LONG           PIC S9(4) COMP-5  VALUE +089.           
                                                                                
      *}                                                                        
