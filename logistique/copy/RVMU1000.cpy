      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVMU1000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVMU1000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMU1000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMU1000.                                                            
      *}                                                                        
           02  MU10-NMUTATION                                                   
               PIC X(0007).                                                     
           02  MU10-NSOCLIEU                                                    
               PIC X(0003).                                                     
           02  MU10-NLIEU                                                       
               PIC X(0003).                                                     
           02  MU10-CRAYONFAM                                                   
               PIC X(0005).                                                     
           02  MU10-WSEQED                                                      
               PIC S9(5) COMP-3.                                                
           02  MU10-WRAYONFAM                                                   
               PIC X(0001).                                                     
           02  MU10-QSTOCKRESMUT                                                
               PIC S9(7) COMP-3.                                                
           02  MU10-QSTOCKLIEU                                                  
               PIC S9(7) COMP-3.                                                
           02  MU10-QVOLUMEGRP                                                  
               PIC S9(11) COMP-3.                                               
           02  MU10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVMU1000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMU1000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMU1000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU10-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU10-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU10-NSOCLIEU-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU10-NSOCLIEU-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU10-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU10-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU10-CRAYONFAM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU10-CRAYONFAM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU10-WSEQED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU10-WSEQED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU10-WRAYONFAM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU10-WRAYONFAM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU10-QSTOCKRESMUT-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU10-QSTOCKRESMUT-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU10-QSTOCKLIEU-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU10-QSTOCKLIEU-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU10-QVOLUMEGRP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU10-QVOLUMEGRP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  MU10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
