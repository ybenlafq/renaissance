      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EIE30   EIE30                                              00000020
      ***************************************************************** 00000030
       01   EIS30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE JOUR TRAITEMENT                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE TRAITEMENT                                                00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * ZONE DE COMMANDE                                                00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MZONCMDI  PIC X(15).                                      00000200
      * NUMERO SOCIETE                                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      * NUMERO DE LIEU                                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNDEPOTI  PIC X(3).                                       00000300
      * NUMERO DE ZONE                                                  00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONEL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MZONEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MZONEF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MZONEI    PIC X(2).                                       00000350
      * NUMERO DE SECTEUR                                               00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTL    COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MSECTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSECTF    PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MSECTI    PIC X(2).                                       00000400
      * FLAG TOUTES ZONES                                               00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTZONESL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MTTZONESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTTZONESF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MTTZONESI      PIC X.                                     00000450
      * NUM CODIC                                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MNCODICI  PIC X(7).                                       00000500
      * ZONE MESSAGE                                                    00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MLIBERRI  PIC X(78).                                      00000550
      * CODE TRANSACTION                                                00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MCODTRAI  PIC X(4).                                       00000600
      * NOM DU CICS                                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCICSI    PIC X(5).                                       00000650
      * NOM LIGNE VTAM                                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNETNAMI  PIC X(8).                                       00000700
      * CODE TERMINAL                                                   00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MSCREENI  PIC X(4).                                       00000750
      ***************************************************************** 00000760
      * SDF: EIE30   EIE30                                              00000770
      ***************************************************************** 00000780
       01   EIS30O REDEFINES EIS30I.                                    00000790
           02 FILLER    PIC X(12).                                      00000800
      * DATE JOUR TRAITEMENT                                            00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MDATJOUA  PIC X.                                          00000830
           02 MDATJOUC  PIC X.                                          00000840
           02 MDATJOUP  PIC X.                                          00000850
           02 MDATJOUH  PIC X.                                          00000860
           02 MDATJOUV  PIC X.                                          00000870
           02 MDATJOUO  PIC X(10).                                      00000880
      * HEURE TRAITEMENT                                                00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MTIMJOUA  PIC X.                                          00000910
           02 MTIMJOUC  PIC X.                                          00000920
           02 MTIMJOUP  PIC X.                                          00000930
           02 MTIMJOUH  PIC X.                                          00000940
           02 MTIMJOUV  PIC X.                                          00000950
           02 MTIMJOUO  PIC X(5).                                       00000960
      * ZONE DE COMMANDE                                                00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MZONCMDA  PIC X.                                          00000990
           02 MZONCMDC  PIC X.                                          00001000
           02 MZONCMDP  PIC X.                                          00001010
           02 MZONCMDH  PIC X.                                          00001020
           02 MZONCMDV  PIC X.                                          00001030
           02 MZONCMDO  PIC X(15).                                      00001040
      * NUMERO SOCIETE                                                  00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MNSOCA    PIC X.                                          00001070
           02 MNSOCC    PIC X.                                          00001080
           02 MNSOCP    PIC X.                                          00001090
           02 MNSOCH    PIC X.                                          00001100
           02 MNSOCV    PIC X.                                          00001110
           02 MNSOCO    PIC X(3).                                       00001120
      * NUMERO DE LIEU                                                  00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNDEPOTA  PIC X.                                          00001150
           02 MNDEPOTC  PIC X.                                          00001160
           02 MNDEPOTP  PIC X.                                          00001170
           02 MNDEPOTH  PIC X.                                          00001180
           02 MNDEPOTV  PIC X.                                          00001190
           02 MNDEPOTO  PIC X(3).                                       00001200
      * NUMERO DE ZONE                                                  00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MZONEA    PIC X.                                          00001230
           02 MZONEC    PIC X.                                          00001240
           02 MZONEP    PIC X.                                          00001250
           02 MZONEH    PIC X.                                          00001260
           02 MZONEV    PIC X.                                          00001270
           02 MZONEO    PIC X(2).                                       00001280
      * NUMERO DE SECTEUR                                               00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MSECTA    PIC X.                                          00001310
           02 MSECTC    PIC X.                                          00001320
           02 MSECTP    PIC X.                                          00001330
           02 MSECTH    PIC X.                                          00001340
           02 MSECTV    PIC X.                                          00001350
           02 MSECTO    PIC X(2).                                       00001360
      * FLAG TOUTES ZONES                                               00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MTTZONESA      PIC X.                                     00001390
           02 MTTZONESC PIC X.                                          00001400
           02 MTTZONESP PIC X.                                          00001410
           02 MTTZONESH PIC X.                                          00001420
           02 MTTZONESV PIC X.                                          00001430
           02 MTTZONESO      PIC X.                                     00001440
      * NUM CODIC                                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNCODICA  PIC X.                                          00001470
           02 MNCODICC  PIC X.                                          00001480
           02 MNCODICP  PIC X.                                          00001490
           02 MNCODICH  PIC X.                                          00001500
           02 MNCODICV  PIC X.                                          00001510
           02 MNCODICO  PIC X(7).                                       00001520
      * ZONE MESSAGE                                                    00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MLIBERRA  PIC X.                                          00001550
           02 MLIBERRC  PIC X.                                          00001560
           02 MLIBERRP  PIC X.                                          00001570
           02 MLIBERRH  PIC X.                                          00001580
           02 MLIBERRV  PIC X.                                          00001590
           02 MLIBERRO  PIC X(78).                                      00001600
      * CODE TRANSACTION                                                00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCODTRAA  PIC X.                                          00001630
           02 MCODTRAC  PIC X.                                          00001640
           02 MCODTRAP  PIC X.                                          00001650
           02 MCODTRAH  PIC X.                                          00001660
           02 MCODTRAV  PIC X.                                          00001670
           02 MCODTRAO  PIC X(4).                                       00001680
      * NOM DU CICS                                                     00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCICSA    PIC X.                                          00001710
           02 MCICSC    PIC X.                                          00001720
           02 MCICSP    PIC X.                                          00001730
           02 MCICSH    PIC X.                                          00001740
           02 MCICSV    PIC X.                                          00001750
           02 MCICSO    PIC X(5).                                       00001760
      * NOM LIGNE VTAM                                                  00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MNETNAMA  PIC X.                                          00001790
           02 MNETNAMC  PIC X.                                          00001800
           02 MNETNAMP  PIC X.                                          00001810
           02 MNETNAMH  PIC X.                                          00001820
           02 MNETNAMV  PIC X.                                          00001830
           02 MNETNAMO  PIC X(8).                                       00001840
      * CODE TERMINAL                                                   00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MSCREENA  PIC X.                                          00001870
           02 MSCREENC  PIC X.                                          00001880
           02 MSCREENP  PIC X.                                          00001890
           02 MSCREENH  PIC X.                                          00001900
           02 MSCREENV  PIC X.                                          00001910
           02 MSCREENO  PIC X(4).                                       00001920
                                                                                
