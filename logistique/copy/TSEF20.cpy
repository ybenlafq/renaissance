      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      *                                                                         
      * COPIE DE LA TS UTILIISEE DEUX FOIS (EF20 ET EF21)                       
      * DANS PROGRAMME TEF20                                                    
      *                                                                         
       01  TS-:EF20:-IDENT.                                                     
           05  TS-:EF20:-NAME      PIC X(04)      VALUE SPACE.                  
           05  TS-:EF20:-TERM      PIC X(04)      VALUE SPACE.                  
       01  TS-:EF20:-NOM  REDEFINES TS-:EF20:-IDENT                             
                                   PIC X(08).                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-EF20-LONG          PIC S9(4) COMP VALUE 1350.                     
      *--                                                                       
       01  TS-:EF20:-LONG          PIC S9(4) COMP-5 VALUE 1350.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  RANG-TS-EF20          PIC S9(4) COMP VALUE 0.                        
      *--                                                                       
       01  RANG-TS-:EF20:          PIC S9(4) COMP-5 VALUE 0.                    
      *}                                                                        
      *                                                                         
       01  TS-:EF20:-DATA.                                                      
           03 TS-:EF20:-LIGNE OCCURS 10.                                        
      *                                                                         
01            05 TS-:EF20:-SELECT          PIC X(01).                           
              05 TS-:EF20:-AFFIC.                                               
66               10 TS-:EF20:-NLIEUHS     PIC X(03).                            
                 10 TS-:EF20:-NORIGINE    PIC X(07).                            
                 10 FILLER                PIC X(02).                            
                 10 TS-:EF20:-NCODIC      PIC X(07).                            
                 10 FILLER                PIC X(01).                            
                 10 TS-:EF20:-CFAM        PIC X(05).                            
                 10 FILLER                PIC X(01).                            
                 10 TS-:EF20:-CMARQ       PIC X(05).                            
                 10 FILLER                PIC X(01).                            
                 10 TS-:EF20:-LREF        PIC X(20).                            
                 10 FILLER                PIC X(02).                            
                 10 TS-:EF20:-DENV        PIC X(04).                            
                 10 FILLER                PIC X(01).                            
                 10 TS-:EF20:-NENVOI      PIC X(07).                            
10            05 TS-:EF20:-VALEUR          PIC X(10).                           
58            05 TS-:EF20:-NON-AFF.                                             
                 10 TS-:EF20:-NSOCORIG     PIC X(03).                           
                 10 TS-:EF20:-NLIEUORIG    PIC X(03).                           
                 10 TS-:EF20:-CGARANTIE    PIC X(05).                           
                 10 TS-:EF20:-QTENV        PIC S9(5)  COMP-3.                   
                 10 TS-:EF20:-QTRDU        PIC S9(5)  COMP-3.                   
                 10 TS-:EF20:-NSOCMVTO     PIC X(03).                           
                 10 TS-:EF20:-NLIEUMVTO    PIC X(03).                           
                 10 TS-:EF20:-NSLIEUMVTO   PIC X(03).                           
                 10 TS-:EF20:-NSOCMVTD     PIC X(03).                           
                 10 TS-:EF20:-NLIEUMVTD    PIC X(03).                           
                 10 TS-:EF20:-NSLIEUMVTD   PIC X(03).                           
                 10 TS-:EF20:-CLIEUTRTMVTD PIC X(05).                           
                 10 TS-:EF20:-DENVOI       PIC X(8).                            
                 10 TS-:EF20:-MTPROV       PIC S9(7)V9(2)  COMP-3.              
                 10 TS-:EF20:-MTRDU        PIC S9(7)V9(2)  COMP-3.              
                                                                                
