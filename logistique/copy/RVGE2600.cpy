      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGE2600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGE2600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGE2600.                                                            
           02  GE26-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GE26-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GE26-CAIRE                                                       
               PIC X(0002).                                                     
           02  GE26-CCOTE                                                       
               PIC X(0002).                                                     
           02  GE26-NPOSITION                                                   
               PIC X(0003).                                                     
           02  GE26-NCODIC                                                      
               PIC X(0007).                                                     
           02  GE26-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGE2600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGE2600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE26-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE26-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE26-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE26-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE26-CAIRE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE26-CAIRE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE26-CCOTE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE26-CCOTE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE26-NPOSITION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE26-NPOSITION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE26-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE26-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE26-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE26-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
