      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *  PROJET     : QUOTAS DECENTRALISES                             *        
      *  TITRE      : TS MAJ DE LA TABLE RTGQ03 (QUOTAS) TSZQ21        *        
      *  APPELER PAR: TZQ18 - AFFECTATION PROFIL TYPE / JOUR           *        
      *                                                                *        
      *  CONTENU    : CONTIENT TOUTES LES LIGNES DETAILS (RTGQ03)      *00000050
      *               1- TZQ18: POUR UN JOUR DE QUOTA (ENR. RTGQ00)    *        
      *               2- TZQ19: POUR UN COUPLE JOUR/ZONE LIVRAISON     *        
      *               3- TZQ19: POUR UN COUPLE JOUR/TYPE QUOTA         *        
      *  POSTE      : 494 POSTES (TRANCHES HORAIRES)                   *        
      *               CORRESPOND A 13 LG * 38 PG (=494) DANS EZQ16     *        
      *               1 POSTE = 1 LIGNE DE LA RTGQ03                   *        
      ******************************************************************        
      *                                                                         
       01  FILLER                    PIC X(16) VALUE '** INDICE TS2 **'.        
       01  IT2                       PIC S9(04) COMP-3    VALUE ZEROES.         
       01  IT2-MAX                   PIC S9(04) COMP-3    VALUE 4000.           
      *                                                                         
      **** LONGUEUR TS ******************************* 44+8892+564= 9500        
       01  TS2-DONNEES.                                                         
           05 TS2-RVGQ0300.                                                     
              10 TS2-CMODDEL            PIC X(03).                              
              10 TS2-CEQUIP             PIC X(05).                              
              10 TS2-DJOUR              PIC X(08).                              
              10 TS2-POSTEA             PIC X(18).                              
              10 TS2-DSYST              PIC S9(13) COMP-3.                      
           05 TS2-NBTRANCHE          PIC S9(05) COMP-3.                         
      **** ENREGISTREMENTS TABLE RTGQ03 ******************* 494*18= 8892        
           05 TS2-POSTE              OCCURS 4000.                               
              10 TS2-CZONLIV            PIC X(05).                              
              10 TS2-WZONACT            PIC X(01).                              
              10 TS2-QPSR               PIC S9(03)V99 COMP-3.                   
              10 TS2-CPLAGE             PIC X(02).                              
              10 TS2-QQUOTA             PIC S9(05) COMP-3.                      
              10 TS2-QPRIS              PIC S9(05) COMP-3.                      
              10 TS2-WILLIM             PIC X(01).                              
           05 TS2-FILLER             PIC X(564).                                
                                                                                
