      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVPF3400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPF3400                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPF3400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPF3400.                                                            
      *}                                                                        
           10 PF34-NSOC            PIC X(3).                                    
           10 PF34-NLIEU           PIC X(3).                                    
           10 PF34-NCODIC          PIC X(7).                                    
           10 PF34-NVENTE          PIC X(7).                                    
           10 PF34-DVENTE          PIC X(8).                                    
           10 PF34-QVENDUE         PIC S9(5)V USAGE COMP-3.                     
           10 PF34-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPF3400                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPF3400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPF3400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF34-NSOC-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 PF34-NSOC-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF34-NLIEU-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 PF34-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF34-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 PF34-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF34-NVENTE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 PF34-NVENTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF34-DVENTE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 PF34-DVENTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF34-QVENDUE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 PF34-QVENDUE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PF34-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 PF34-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
