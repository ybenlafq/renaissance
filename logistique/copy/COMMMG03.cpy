      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  COMM-MG03-APPLI.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-MG03-CODRET       COMP     PIC  S9(4).                      
      *--                                                                       
           05  COMM-MG03-CODRET COMP-5     PIC  S9(4).                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-MG03-TOTAL        COMP     PIC  S9(4).                      
      *--                                                                       
           05  COMM-MG03-TOTAL COMP-5     PIC  S9(4).                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-MG03-RAYON-MAX    COMP     PIC  S9(4).                      
      *--                                                                       
           05  COMM-MG03-RAYON-MAX COMP-5     PIC  S9(4).                       
      *}                                                                        
           05  COMM-MG03-RAYON   OCCURS 1000   PIC  X(5).                       
           05  COMM-MG03-FAMILLE.                                               
               10 COMM-MG03-FAM OCCURS 1000    PIC  X(5).                       
           05  FILLER                          PIC  X(90).                      
                                                                                
