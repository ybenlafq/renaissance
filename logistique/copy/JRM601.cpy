      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JRM601 AU 27/04/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,20,BI,A,                          *        
      *                           37,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JRM601.                                                        
            05 NOMETAT-JRM601           PIC X(6) VALUE 'JRM601'.                
            05 RUPTURES-JRM601.                                                 
           10 JRM601-CRMGROUP           PIC X(05).                      007  005
           10 JRM601-CFAM               PIC X(05).                      012  005
           10 JRM601-LAGREGATED         PIC X(20).                      017  020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JRM601-SEQUENCE           PIC S9(04) COMP.                037  002
      *--                                                                       
           10 JRM601-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JRM601.                                                   
           10 JRM601-CMARQ              PIC X(05).                      039  005
           10 JRM601-LFAM               PIC X(20).                      044  020
           10 JRM601-LREFFOURN          PIC X(20).                      064  020
           10 JRM601-LSTATCOMP          PIC X(03).                      084  003
           10 JRM601-NCODIC             PIC X(07).                      087  007
           10 JRM601-NSOCDEPOT1         PIC X(06).                      094  006
           10 JRM601-NSOCDEPOT10        PIC X(06).                      100  006
           10 JRM601-NSOCDEPOT2         PIC X(06).                      106  006
           10 JRM601-NSOCDEPOT3         PIC X(06).                      112  006
           10 JRM601-NSOCDEPOT4         PIC X(06).                      118  006
           10 JRM601-NSOCDEPOT5         PIC X(06).                      124  006
           10 JRM601-NSOCDEPOT6         PIC X(06).                      130  006
           10 JRM601-NSOCDEPOT7         PIC X(06).                      136  006
           10 JRM601-NSOCDEPOT8         PIC X(06).                      142  006
           10 JRM601-NSOCDEPOT9         PIC X(06).                      148  006
           10 JRM601-WE9                PIC X(01).                      154  001
           10 JRM601-WRM80              PIC X(01).                      155  001
           10 JRM601-PRIX-VENTE         PIC S9(07)V9(2) COMP-3.         156  005
           10 JRM601-QMUTATT            PIC S9(05)      COMP-3.         161  003
           10 JRM601-QNBMAG             PIC S9(03)      COMP-3.         164  002
           10 JRM601-QPV                PIC S9(07)      COMP-3.         166  004
           10 JRM601-QPV-SIGNE          PIC S9(07)      COMP-3.         170  004
           10 JRM601-QSA                PIC S9(07)      COMP-3.         174  004
           10 JRM601-QSASIMU            PIC S9(07)      COMP-3.         178  004
           10 JRM601-QSO                PIC S9(07)      COMP-3.         182  004
           10 JRM601-QSOSIMU            PIC S9(07)      COMP-3.         186  004
           10 JRM601-QSTOCKDEP          PIC S9(05)      COMP-3.         190  003
           10 JRM601-QSTOCKMAG          PIC S9(05)      COMP-3.         193  003
           10 JRM601-S-1                PIC S9(05)      COMP-3.         196  003
           10 JRM601-S-2                PIC S9(05)      COMP-3.         199  003
           10 JRM601-S-3                PIC S9(05)      COMP-3.         202  003
           10 JRM601-S-4                PIC S9(05)      COMP-3.         205  003
            05 FILLER                      PIC X(305).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JRM601-LONG           PIC S9(4)   COMP  VALUE +207.           
      *                                                                         
      *--                                                                       
        01  DSECT-JRM601-LONG           PIC S9(4) COMP-5  VALUE +207.           
                                                                                
      *}                                                                        
