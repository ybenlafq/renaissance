      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGQ16   EGQ16                                              00000020
      ***************************************************************** 00000030
       01   EGQ16I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MFONCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDATEI    PIC X(8).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDELL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCDELL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCDELF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCDELI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGENTL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLMAGENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMAGENTF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLMAGENTI      PIC X(9).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAGL     COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMAGF     PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MMAGI     PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEPOTI   PIC X(3).                                       00000410
           02 TABLEI OCCURS   14 TIMES .                                00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MJOURL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MJOURL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MJOURF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MJOURI  PIC X(8).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTAL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MQUOTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQUOTAF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MQUOTAI      PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRISL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MPRISL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPRISF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MPRISI  PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPAGE1L     COMP PIC S9(4).                            00000550
      *--                                                                       
           02 MTOTPAGE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTOTPAGE1F     PIC X.                                     00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MTOTPAGE1I     PIC X(8).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPAGE2L     COMP PIC S9(4).                            00000590
      *--                                                                       
           02 MTOTPAGE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTOTPAGE2F     PIC X.                                     00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MTOTPAGE2I     PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTGEN1L      COMP PIC S9(4).                            00000630
      *--                                                                       
           02 MTOTGEN1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTGEN1F      PIC X.                                     00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MTOTGEN1I      PIC X(8).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTGEN2L      COMP PIC S9(4).                            00000670
      *--                                                                       
           02 MTOTGEN2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTGEN2F      PIC X.                                     00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MTOTGEN2I      PIC X(8).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MZONCMDI  PIC X(15).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(58).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: EGQ16   EGQ16                                              00000960
      ***************************************************************** 00000970
       01   EGQ16O REDEFINES EGQ16I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEA    PIC X.                                          00001150
           02 MPAGEC    PIC X.                                          00001160
           02 MPAGEP    PIC X.                                          00001170
           02 MPAGEH    PIC X.                                          00001180
           02 MPAGEV    PIC X.                                          00001190
           02 MPAGEO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MFONCA    PIC X.                                          00001220
           02 MFONCC    PIC X.                                          00001230
           02 MFONCP    PIC X.                                          00001240
           02 MFONCH    PIC X.                                          00001250
           02 MFONCV    PIC X.                                          00001260
           02 MFONCO    PIC X(3).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATEA    PIC X.                                          00001290
           02 MDATEC    PIC X.                                          00001300
           02 MDATEP    PIC X.                                          00001310
           02 MDATEH    PIC X.                                          00001320
           02 MDATEV    PIC X.                                          00001330
           02 MDATEO    PIC X(8).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MCDELA    PIC X.                                          00001360
           02 MCDELC    PIC X.                                          00001370
           02 MCDELP    PIC X.                                          00001380
           02 MCDELH    PIC X.                                          00001390
           02 MCDELV    PIC X.                                          00001400
           02 MCDELO    PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLMAGENTA      PIC X.                                     00001430
           02 MLMAGENTC PIC X.                                          00001440
           02 MLMAGENTP PIC X.                                          00001450
           02 MLMAGENTH PIC X.                                          00001460
           02 MLMAGENTV PIC X.                                          00001470
           02 MLMAGENTO      PIC X(9).                                  00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MMAGA     PIC X.                                          00001500
           02 MMAGC     PIC X.                                          00001510
           02 MMAGP     PIC X.                                          00001520
           02 MMAGH     PIC X.                                          00001530
           02 MMAGV     PIC X.                                          00001540
           02 MMAGO     PIC X(3).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDEPOTA   PIC X.                                          00001570
           02 MDEPOTC   PIC X.                                          00001580
           02 MDEPOTP   PIC X.                                          00001590
           02 MDEPOTH   PIC X.                                          00001600
           02 MDEPOTV   PIC X.                                          00001610
           02 MDEPOTO   PIC X(3).                                       00001620
           02 TABLEO OCCURS   14 TIMES .                                00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MJOURA  PIC X.                                          00001650
             03 MJOURC  PIC X.                                          00001660
             03 MJOURP  PIC X.                                          00001670
             03 MJOURH  PIC X.                                          00001680
             03 MJOURV  PIC X.                                          00001690
             03 MJOURO  PIC X(8).                                       00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MQUOTAA      PIC X.                                     00001720
             03 MQUOTAC PIC X.                                          00001730
             03 MQUOTAP PIC X.                                          00001740
             03 MQUOTAH PIC X.                                          00001750
             03 MQUOTAV PIC X.                                          00001760
             03 MQUOTAO      PIC X(5).                                  00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MPRISA  PIC X.                                          00001790
             03 MPRISC  PIC X.                                          00001800
             03 MPRISP  PIC X.                                          00001810
             03 MPRISH  PIC X.                                          00001820
             03 MPRISV  PIC X.                                          00001830
             03 MPRISO  PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MTOTPAGE1A     PIC X.                                     00001860
           02 MTOTPAGE1C     PIC X.                                     00001870
           02 MTOTPAGE1P     PIC X.                                     00001880
           02 MTOTPAGE1H     PIC X.                                     00001890
           02 MTOTPAGE1V     PIC X.                                     00001900
           02 MTOTPAGE1O     PIC X(8).                                  00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MTOTPAGE2A     PIC X.                                     00001930
           02 MTOTPAGE2C     PIC X.                                     00001940
           02 MTOTPAGE2P     PIC X.                                     00001950
           02 MTOTPAGE2H     PIC X.                                     00001960
           02 MTOTPAGE2V     PIC X.                                     00001970
           02 MTOTPAGE2O     PIC X(8).                                  00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MTOTGEN1A      PIC X.                                     00002000
           02 MTOTGEN1C PIC X.                                          00002010
           02 MTOTGEN1P PIC X.                                          00002020
           02 MTOTGEN1H PIC X.                                          00002030
           02 MTOTGEN1V PIC X.                                          00002040
           02 MTOTGEN1O      PIC X(8).                                  00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MTOTGEN2A      PIC X.                                     00002070
           02 MTOTGEN2C PIC X.                                          00002080
           02 MTOTGEN2P PIC X.                                          00002090
           02 MTOTGEN2H PIC X.                                          00002100
           02 MTOTGEN2V PIC X.                                          00002110
           02 MTOTGEN2O      PIC X(8).                                  00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MZONCMDA  PIC X.                                          00002140
           02 MZONCMDC  PIC X.                                          00002150
           02 MZONCMDP  PIC X.                                          00002160
           02 MZONCMDH  PIC X.                                          00002170
           02 MZONCMDV  PIC X.                                          00002180
           02 MZONCMDO  PIC X(15).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(58).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
