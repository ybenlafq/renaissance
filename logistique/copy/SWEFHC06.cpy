      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------------------------*               
      *    FICHIER CSV EXTRACTION DE L'ETAT JHE601              *               
      *    LONGUEUR : 210                                       *               
      *---------------------------------------------------------*               
       01  HC06-ENREG.                                                          
           05  HC06-DONNEES.                                             000008 
               10  HC06-NOMETAT      PIC X(06).                          000008 
               10  FILLER            PIC X(01)  VALUE ';'.               000019 
               10  HC06-DATED        PIC X(08).                          000008 
               10  FILLER            PIC X(01)  VALUE ';'.               000019 
               10  HC06-DATEF        PIC X(08).                          000008 
               10  FILLER            PIC X(01)  VALUE ';'.               000019 
               10  HC06-CTIERS       PIC X(17).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-NENVOI       PIC X(07).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-DENVOI       PIC X(08).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-ADRESSE      PIC X(08).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-NCODIC       PIC X(07).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-QTE          PIC X(01).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-CFAM         PIC X(05).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-CMARQ        PIC X(05).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-LREF         PIC X(20).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-CSERIE       PIC X(16).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-NLIEUHC      PIC X(03).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-NCHS         PIC X(07).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-NACCORD      PIC X(13).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-DACCFOUR     PIC X(08).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-INTERLOC     PIC X(10).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-PVTTC        PIC -9(7)V99.                              
               10  HC06-PVTTC-R      REDEFINES  HC06-PVTTC                      
                                     PIC X(10).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-PRMP         PIC -9(7)V9(6).                            
               10  HC06-PRMP-R       REDEFINES  HC06-PRMP                       
                                     PIC X(14).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  HC06-ECOP         PIC -9(5)V99.                              
               10  HC06-ECOP-R       REDEFINES  HC06-ECOP                       
                                     PIC X(08).                                 
               10  FILLER            PIC X(01)  VALUE ';'.               000019 
      *                                                                         
      *---------------------------------------------------------*               
                                                                                
