      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS01   EGS01                                              00000020
      ***************************************************************** 00000030
       01   EGS01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDEPOTI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRAYONL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MRAYONL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRAYONF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MRAYONI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRAYONL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRAYONF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLRAYONI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASSORL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCASSORF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCASSORI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSORL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLASSORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLASSORF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLASSORI  PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPROL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPROF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCAPPROI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAPPROL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLAPPROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAPPROF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLAPPROI  PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODIC1L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNCODIC1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODIC1F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNCODIC1I      PIC X(7).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCEXPOI   PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEXPOL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLEXPOF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLEXPOI   PIC X(20).                                      00000690
           02 M4I OCCURS   11 TIMES .                                   00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNCODICI     PIC X(7).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCMARQI      PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOURNL  COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MLREFFOURNL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLREFFOURNF  PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLREFFOURNI  PIC X(18).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCROSSDOCKL  COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MCROSSDOCKL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCROSSDOCKF  PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCROSSDOCKI  PIC X.                                     00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSTATCOMPL  COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MLSTATCOMPL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLSTATCOMPF  PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MLSTATCOMPI  PIC X(3).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSENSAPPROL  COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MSENSAPPROL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MSENSAPPROF  PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MSENSAPPROI  PIC X.                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSENSVTEL    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MSENSVTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSENSVTEF    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MSENSVTEI    PIC X.                                     00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKTOTL  COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MQSTOCKTOTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQSTOCKTOTF  PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQSTOCKTOTI  PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKDISL  COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MQSTOCKDISL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQSTOCKDISF  PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQSTOCKDISI  PIC X(5).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKRESL  COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MQSTOCKRESL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQSTOCKRESF  PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MQSTOCKRESI  PIC X(5).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKCOTL  COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MQSTOCKCOTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQSTOCKCOTF  PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MQSTOCKCOTI  PIC X(5).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKPREL  COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MQSTOCKPREL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQSTOCKPREF  PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MQSTOCKPREI  PIC X(4).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKTRAL  COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MQSTOCKTRAL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQSTOCKTRAF  PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MQSTOCKTRAI  PIC X(3).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKHSL   COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MQSTOCKHSL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQSTOCKHSF   PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MQSTOCKHSI   PIC X(3).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MZONCMDI  PIC X(12).                                      00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLIBERRI  PIC X(61).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCODTRAI  PIC X(4).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCICSI    PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MNETNAMI  PIC X(8).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MSCREENI  PIC X(4).                                       00001500
      ***************************************************************** 00001510
      * SDF: EGS01   EGS01                                              00001520
      ***************************************************************** 00001530
       01   EGS01O REDEFINES EGS01I.                                    00001540
           02 FILLER    PIC X(12).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDATJOUA  PIC X.                                          00001570
           02 MDATJOUC  PIC X.                                          00001580
           02 MDATJOUP  PIC X.                                          00001590
           02 MDATJOUH  PIC X.                                          00001600
           02 MDATJOUV  PIC X.                                          00001610
           02 MDATJOUO  PIC X(10).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MTIMJOUA  PIC X.                                          00001640
           02 MTIMJOUC  PIC X.                                          00001650
           02 MTIMJOUP  PIC X.                                          00001660
           02 MTIMJOUH  PIC X.                                          00001670
           02 MTIMJOUV  PIC X.                                          00001680
           02 MTIMJOUO  PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MPAGEA    PIC X.                                          00001710
           02 MPAGEC    PIC X.                                          00001720
           02 MPAGEP    PIC X.                                          00001730
           02 MPAGEH    PIC X.                                          00001740
           02 MPAGEV    PIC X.                                          00001750
           02 MPAGEO    PIC X(5).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNSOCA    PIC X.                                          00001780
           02 MNSOCC    PIC X.                                          00001790
           02 MNSOCP    PIC X.                                          00001800
           02 MNSOCH    PIC X.                                          00001810
           02 MNSOCV    PIC X.                                          00001820
           02 MNSOCO    PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDEPOTA   PIC X.                                          00001850
           02 MDEPOTC   PIC X.                                          00001860
           02 MDEPOTP   PIC X.                                          00001870
           02 MDEPOTH   PIC X.                                          00001880
           02 MDEPOTV   PIC X.                                          00001890
           02 MDEPOTO   PIC X(3).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MRAYONA   PIC X.                                          00001920
           02 MRAYONC   PIC X.                                          00001930
           02 MRAYONP   PIC X.                                          00001940
           02 MRAYONH   PIC X.                                          00001950
           02 MRAYONV   PIC X.                                          00001960
           02 MRAYONO   PIC X(5).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLRAYONA  PIC X.                                          00001990
           02 MLRAYONC  PIC X.                                          00002000
           02 MLRAYONP  PIC X.                                          00002010
           02 MLRAYONH  PIC X.                                          00002020
           02 MLRAYONV  PIC X.                                          00002030
           02 MLRAYONO  PIC X(20).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCASSORA  PIC X.                                          00002060
           02 MCASSORC  PIC X.                                          00002070
           02 MCASSORP  PIC X.                                          00002080
           02 MCASSORH  PIC X.                                          00002090
           02 MCASSORV  PIC X.                                          00002100
           02 MCASSORO  PIC X(5).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MLASSORA  PIC X.                                          00002130
           02 MLASSORC  PIC X.                                          00002140
           02 MLASSORP  PIC X.                                          00002150
           02 MLASSORH  PIC X.                                          00002160
           02 MLASSORV  PIC X.                                          00002170
           02 MLASSORO  PIC X(20).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCFAMA    PIC X.                                          00002200
           02 MCFAMC    PIC X.                                          00002210
           02 MCFAMP    PIC X.                                          00002220
           02 MCFAMH    PIC X.                                          00002230
           02 MCFAMV    PIC X.                                          00002240
           02 MCFAMO    PIC X(5).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLFAMA    PIC X.                                          00002270
           02 MLFAMC    PIC X.                                          00002280
           02 MLFAMP    PIC X.                                          00002290
           02 MLFAMH    PIC X.                                          00002300
           02 MLFAMV    PIC X.                                          00002310
           02 MLFAMO    PIC X(20).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCAPPROA  PIC X.                                          00002340
           02 MCAPPROC  PIC X.                                          00002350
           02 MCAPPROP  PIC X.                                          00002360
           02 MCAPPROH  PIC X.                                          00002370
           02 MCAPPROV  PIC X.                                          00002380
           02 MCAPPROO  PIC X(5).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLAPPROA  PIC X.                                          00002410
           02 MLAPPROC  PIC X.                                          00002420
           02 MLAPPROP  PIC X.                                          00002430
           02 MLAPPROH  PIC X.                                          00002440
           02 MLAPPROV  PIC X.                                          00002450
           02 MLAPPROO  PIC X(20).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MNCODIC1A      PIC X.                                     00002480
           02 MNCODIC1C PIC X.                                          00002490
           02 MNCODIC1P PIC X.                                          00002500
           02 MNCODIC1H PIC X.                                          00002510
           02 MNCODIC1V PIC X.                                          00002520
           02 MNCODIC1O      PIC X(7).                                  00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MCEXPOA   PIC X.                                          00002550
           02 MCEXPOC   PIC X.                                          00002560
           02 MCEXPOP   PIC X.                                          00002570
           02 MCEXPOH   PIC X.                                          00002580
           02 MCEXPOV   PIC X.                                          00002590
           02 MCEXPOO   PIC X(5).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MLEXPOA   PIC X.                                          00002620
           02 MLEXPOC   PIC X.                                          00002630
           02 MLEXPOP   PIC X.                                          00002640
           02 MLEXPOH   PIC X.                                          00002650
           02 MLEXPOV   PIC X.                                          00002660
           02 MLEXPOO   PIC X(20).                                      00002670
           02 M4O OCCURS   11 TIMES .                                   00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MNCODICA     PIC X.                                     00002700
             03 MNCODICC     PIC X.                                     00002710
             03 MNCODICP     PIC X.                                     00002720
             03 MNCODICH     PIC X.                                     00002730
             03 MNCODICV     PIC X.                                     00002740
             03 MNCODICO     PIC X(7).                                  00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MCMARQA      PIC X.                                     00002770
             03 MCMARQC PIC X.                                          00002780
             03 MCMARQP PIC X.                                          00002790
             03 MCMARQH PIC X.                                          00002800
             03 MCMARQV PIC X.                                          00002810
             03 MCMARQO      PIC X(5).                                  00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MLREFFOURNA  PIC X.                                     00002840
             03 MLREFFOURNC  PIC X.                                     00002850
             03 MLREFFOURNP  PIC X.                                     00002860
             03 MLREFFOURNH  PIC X.                                     00002870
             03 MLREFFOURNV  PIC X.                                     00002880
             03 MLREFFOURNO  PIC X(18).                                 00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MCROSSDOCKA  PIC X.                                     00002910
             03 MCROSSDOCKC  PIC X.                                     00002920
             03 MCROSSDOCKP  PIC X.                                     00002930
             03 MCROSSDOCKH  PIC X.                                     00002940
             03 MCROSSDOCKV  PIC X.                                     00002950
             03 MCROSSDOCKO  PIC X.                                     00002960
             03 FILLER       PIC X(2).                                  00002970
             03 MLSTATCOMPA  PIC X.                                     00002980
             03 MLSTATCOMPC  PIC X.                                     00002990
             03 MLSTATCOMPP  PIC X.                                     00003000
             03 MLSTATCOMPH  PIC X.                                     00003010
             03 MLSTATCOMPV  PIC X.                                     00003020
             03 MLSTATCOMPO  PIC X(3).                                  00003030
             03 FILLER       PIC X(2).                                  00003040
             03 MSENSAPPROA  PIC X.                                     00003050
             03 MSENSAPPROC  PIC X.                                     00003060
             03 MSENSAPPROP  PIC X.                                     00003070
             03 MSENSAPPROH  PIC X.                                     00003080
             03 MSENSAPPROV  PIC X.                                     00003090
             03 MSENSAPPROO  PIC X.                                     00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MSENSVTEA    PIC X.                                     00003120
             03 MSENSVTEC    PIC X.                                     00003130
             03 MSENSVTEP    PIC X.                                     00003140
             03 MSENSVTEH    PIC X.                                     00003150
             03 MSENSVTEV    PIC X.                                     00003160
             03 MSENSVTEO    PIC X.                                     00003170
             03 FILLER       PIC X(2).                                  00003180
             03 MQSTOCKTOTA  PIC X.                                     00003190
             03 MQSTOCKTOTC  PIC X.                                     00003200
             03 MQSTOCKTOTP  PIC X.                                     00003210
             03 MQSTOCKTOTH  PIC X.                                     00003220
             03 MQSTOCKTOTV  PIC X.                                     00003230
             03 MQSTOCKTOTO  PIC X(5).                                  00003240
             03 FILLER       PIC X(2).                                  00003250
             03 MQSTOCKDISA  PIC X.                                     00003260
             03 MQSTOCKDISC  PIC X.                                     00003270
             03 MQSTOCKDISP  PIC X.                                     00003280
             03 MQSTOCKDISH  PIC X.                                     00003290
             03 MQSTOCKDISV  PIC X.                                     00003300
             03 MQSTOCKDISO  PIC X(5).                                  00003310
             03 FILLER       PIC X(2).                                  00003320
             03 MQSTOCKRESA  PIC X.                                     00003330
             03 MQSTOCKRESC  PIC X.                                     00003340
             03 MQSTOCKRESP  PIC X.                                     00003350
             03 MQSTOCKRESH  PIC X.                                     00003360
             03 MQSTOCKRESV  PIC X.                                     00003370
             03 MQSTOCKRESO  PIC X(5).                                  00003380
             03 FILLER       PIC X(2).                                  00003390
             03 MQSTOCKCOTA  PIC X.                                     00003400
             03 MQSTOCKCOTC  PIC X.                                     00003410
             03 MQSTOCKCOTP  PIC X.                                     00003420
             03 MQSTOCKCOTH  PIC X.                                     00003430
             03 MQSTOCKCOTV  PIC X.                                     00003440
             03 MQSTOCKCOTO  PIC X(5).                                  00003450
             03 FILLER       PIC X(2).                                  00003460
             03 MQSTOCKPREA  PIC X.                                     00003470
             03 MQSTOCKPREC  PIC X.                                     00003480
             03 MQSTOCKPREP  PIC X.                                     00003490
             03 MQSTOCKPREH  PIC X.                                     00003500
             03 MQSTOCKPREV  PIC X.                                     00003510
             03 MQSTOCKPREO  PIC X(4).                                  00003520
             03 FILLER       PIC X(2).                                  00003530
             03 MQSTOCKTRAA  PIC X.                                     00003540
             03 MQSTOCKTRAC  PIC X.                                     00003550
             03 MQSTOCKTRAP  PIC X.                                     00003560
             03 MQSTOCKTRAH  PIC X.                                     00003570
             03 MQSTOCKTRAV  PIC X.                                     00003580
             03 MQSTOCKTRAO  PIC X(3).                                  00003590
             03 FILLER       PIC X(2).                                  00003600
             03 MQSTOCKHSA   PIC X.                                     00003610
             03 MQSTOCKHSC   PIC X.                                     00003620
             03 MQSTOCKHSP   PIC X.                                     00003630
             03 MQSTOCKHSH   PIC X.                                     00003640
             03 MQSTOCKHSV   PIC X.                                     00003650
             03 MQSTOCKHSO   PIC X(3).                                  00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MZONCMDA  PIC X.                                          00003680
           02 MZONCMDC  PIC X.                                          00003690
           02 MZONCMDP  PIC X.                                          00003700
           02 MZONCMDH  PIC X.                                          00003710
           02 MZONCMDV  PIC X.                                          00003720
           02 MZONCMDO  PIC X(12).                                      00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MLIBERRA  PIC X.                                          00003750
           02 MLIBERRC  PIC X.                                          00003760
           02 MLIBERRP  PIC X.                                          00003770
           02 MLIBERRH  PIC X.                                          00003780
           02 MLIBERRV  PIC X.                                          00003790
           02 MLIBERRO  PIC X(61).                                      00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MCODTRAA  PIC X.                                          00003820
           02 MCODTRAC  PIC X.                                          00003830
           02 MCODTRAP  PIC X.                                          00003840
           02 MCODTRAH  PIC X.                                          00003850
           02 MCODTRAV  PIC X.                                          00003860
           02 MCODTRAO  PIC X(4).                                       00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MCICSA    PIC X.                                          00003890
           02 MCICSC    PIC X.                                          00003900
           02 MCICSP    PIC X.                                          00003910
           02 MCICSH    PIC X.                                          00003920
           02 MCICSV    PIC X.                                          00003930
           02 MCICSO    PIC X(5).                                       00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MNETNAMA  PIC X.                                          00003960
           02 MNETNAMC  PIC X.                                          00003970
           02 MNETNAMP  PIC X.                                          00003980
           02 MNETNAMH  PIC X.                                          00003990
           02 MNETNAMV  PIC X.                                          00004000
           02 MNETNAMO  PIC X(8).                                       00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MSCREENA  PIC X.                                          00004030
           02 MSCREENC  PIC X.                                          00004040
           02 MSCREENP  PIC X.                                          00004050
           02 MSCREENH  PIC X.                                          00004060
           02 MSCREENV  PIC X.                                          00004070
           02 MSCREENO  PIC X(4).                                       00004080
                                                                                
