      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : MAJ SAISIE DE COMMANDE FOURNISSEUR                     *         
      *            ARTICLES-QUANTITES (TRX:GL17)                      *         
      *****************************************************************         
      *   LL  1100C.                                                            
      *****************************************************************         
       01  TS-GL17-DESCR.                                                       
      *-----------------------------------------------------LONGUEUR TS         
           02 TS-GL17-LONG PIC S9(5) COMP-3  VALUE +1100.                       
      *    02 TS-GL17-LONG PIC S9(5) COMP-3  VALUE +1120.                       
      *                                                                         
           02 TS-GL17.                                                          
      *-----------------------------------------INFOS ISSU DE GF..---           
              03 TS-GL17-NCDE              PIC X(07).                           
              03 TS-GL17-NCODIC            PIC X(07).                           
      *-----------------------------------------INFOS ISSU DE GF20---..         
              03 TS-GL17-QSOLDE            PIC S9(05) COMP-3.                   
      *-----------------------------------------VARIABLE TRT---------..         
              03 TS-GL17-CMAJ              PIC  X(01).                          
      *{ remove-comma-in-dde 1.5                                                
      *          88 AFFICHABLE        VALUE 'B' , 'M'.                          
      *--                                                                       
                 88 AFFICHABLE        VALUE 'B'   'M'.                          
      *}                                                                        
                 88 LIGNE-SANS-MODIF  VALUE 'B'.                                
                 88 LIGNE-MODIFIEE    VALUE 'M'.                                
                 88 LIGNE-SUPPRIMEE   VALUE 'S'.                                
      *-----------------------------------------VARIBLE TRT----------..         
              03 TS-GL17-NSOCIETE          PIC X(03).                           
              03 TS-GL17-NLIEU             PIC X(03).                           
      *-----------------------------------------INFOS ISSU DE GA00---33         
              03 TS-GL17-LREFFOURN         PIC X(20).                           
              03 TS-GL17-CFAM              PIC X(05).                           
              03 TS-GL17-CMARQ             PIC X(05).                           
              03 TS-GL17-QCOLIRECEPT       PIC S9(05) COMP-3.                   
              03 TS-GL17-QCONDT            PIC S9(05) COMP-3.                   
              03 TS-GL17-CMODSTOCK-CODIC   PIC  X(05).                          
              03 TS-GL17-CMODSTOCK-CDE     PIC  X(05).                          
              03 TS-GL17-CONTROL-COLIRECEP PIC  X(01).                          
              03 TS-GL17-CQUOTA            PIC  X(05).                          
      *-----LL = 24 * 20 = 480------------------TABLEAU 4 X 5 ---------         
              03 TS-GL17-INFOS.                                                 
               04 TS-GL17-POSTE OCCURS 20 TIMES.                                
                 05 TS-GL17-QCDE              PIC S9(05) COMP-3.                
                 05 TS-GL17-QREC              PIC S9(05) COMP-3.                
                 05 TS-GL17-QUOLIVR           PIC S9(05) COMP-3.                
                 05 TS-GL17-QUOPAL            PIC S9(05) COMP-3.                
                 05 TS-GL17-DLIVRAISON        PIC  X(08).                       
                 05 TS-GL17-NLIVRAISON        PIC  X(07).                       
      *-----LL = 1 * 20 = 20-------------------------------------------         
      *       03 TS-GL17-REASONCODE OCCURS 20.                                  
      *          04 TS-GL17-AFFECT-CREAS      PIC X(01).                        
      *-----LL = 24 * 20 = 480--ORIGINE---------TABLEAU 4 X 5 ---------         
              03 TS-GL17-0-INFOS.                                               
               04 TS-GL17-0-POSTE OCCURS 20 TIMES.                              
                 05 TS-GL17-0-QCDE              PIC S9(05) COMP-3.              
                 05 TS-GL17-0-QREC              PIC S9(05) COMP-3.              
                 05 TS-GL17-0-QUOLIVR           PIC S9(05) COMP-3.              
                 05 TS-GL17-0-QUOPAL            PIC S9(05) COMP-3.              
                 05 TS-GL17-0-DLIVRAISON        PIC  X(08).                     
                 05 TS-GL17-0-NLIVRAISON        PIC  X(07).                     
      *-----------------------------------------FILLER-----------------         
              03 TS-GL17-FILLER              PIC  X(69).                        
                                                                                
