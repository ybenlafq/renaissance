      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM05   ERM05                                              00000020
      ***************************************************************** 00000030
       01   ERM05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNMAGI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLMAGI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCGROUPI  PIC X(5).                                       00000290
           02 LIGNEI OCCURS   15 TIMES .                                00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAM1L      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCFAM1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCFAM1F      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCFAM1I      PIC X(5).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAM1L      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLFAM1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLFAM1F      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLFAM1I      PIC X(20).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQFREQ1L     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MQFREQ1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQFREQ1F     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MQFREQ1I     PIC X(4).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAM2L      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCFAM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCFAM2F      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCFAM2I      PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAM2L      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLFAM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLFAM2F      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLFAM2I      PIC X(20).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQFREQ2L     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MQFREQ2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQFREQ2F     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MQFREQ2I     PIC X(4).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MZONCMDI  PIC X(12).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(61).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * SDF: ERM05   ERM05                                              00000800
      ***************************************************************** 00000810
       01   ERM05O REDEFINES ERM05I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MNSOCA    PIC X.                                          00000990
           02 MNSOCC    PIC X.                                          00001000
           02 MNSOCP    PIC X.                                          00001010
           02 MNSOCH    PIC X.                                          00001020
           02 MNSOCV    PIC X.                                          00001030
           02 MNSOCO    PIC X(3).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MNMAGA    PIC X.                                          00001060
           02 MNMAGC    PIC X.                                          00001070
           02 MNMAGP    PIC X.                                          00001080
           02 MNMAGH    PIC X.                                          00001090
           02 MNMAGV    PIC X.                                          00001100
           02 MNMAGO    PIC X(3).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MLMAGA    PIC X.                                          00001130
           02 MLMAGC    PIC X.                                          00001140
           02 MLMAGP    PIC X.                                          00001150
           02 MLMAGH    PIC X.                                          00001160
           02 MLMAGV    PIC X.                                          00001170
           02 MLMAGO    PIC X(20).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MCGROUPA  PIC X.                                          00001200
           02 MCGROUPC  PIC X.                                          00001210
           02 MCGROUPP  PIC X.                                          00001220
           02 MCGROUPH  PIC X.                                          00001230
           02 MCGROUPV  PIC X.                                          00001240
           02 MCGROUPO  PIC X(5).                                       00001250
           02 LIGNEO OCCURS   15 TIMES .                                00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MCFAM1A      PIC X.                                     00001280
             03 MCFAM1C PIC X.                                          00001290
             03 MCFAM1P PIC X.                                          00001300
             03 MCFAM1H PIC X.                                          00001310
             03 MCFAM1V PIC X.                                          00001320
             03 MCFAM1O      PIC X(5).                                  00001330
             03 FILLER       PIC X(2).                                  00001340
             03 MLFAM1A      PIC X.                                     00001350
             03 MLFAM1C PIC X.                                          00001360
             03 MLFAM1P PIC X.                                          00001370
             03 MLFAM1H PIC X.                                          00001380
             03 MLFAM1V PIC X.                                          00001390
             03 MLFAM1O      PIC X(20).                                 00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MQFREQ1A     PIC X.                                     00001420
             03 MQFREQ1C     PIC X.                                     00001430
             03 MQFREQ1P     PIC X.                                     00001440
             03 MQFREQ1H     PIC X.                                     00001450
             03 MQFREQ1V     PIC X.                                     00001460
             03 MQFREQ1O     PIC X(4).                                  00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MCFAM2A      PIC X.                                     00001490
             03 MCFAM2C PIC X.                                          00001500
             03 MCFAM2P PIC X.                                          00001510
             03 MCFAM2H PIC X.                                          00001520
             03 MCFAM2V PIC X.                                          00001530
             03 MCFAM2O      PIC X(5).                                  00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MLFAM2A      PIC X.                                     00001560
             03 MLFAM2C PIC X.                                          00001570
             03 MLFAM2P PIC X.                                          00001580
             03 MLFAM2H PIC X.                                          00001590
             03 MLFAM2V PIC X.                                          00001600
             03 MLFAM2O      PIC X(20).                                 00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MQFREQ2A     PIC X.                                     00001630
             03 MQFREQ2C     PIC X.                                     00001640
             03 MQFREQ2P     PIC X.                                     00001650
             03 MQFREQ2H     PIC X.                                     00001660
             03 MQFREQ2V     PIC X.                                     00001670
             03 MQFREQ2O     PIC X(4).                                  00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MZONCMDA  PIC X.                                          00001700
           02 MZONCMDC  PIC X.                                          00001710
           02 MZONCMDP  PIC X.                                          00001720
           02 MZONCMDH  PIC X.                                          00001730
           02 MZONCMDV  PIC X.                                          00001740
           02 MZONCMDO  PIC X(12).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBERRA  PIC X.                                          00001770
           02 MLIBERRC  PIC X.                                          00001780
           02 MLIBERRP  PIC X.                                          00001790
           02 MLIBERRH  PIC X.                                          00001800
           02 MLIBERRV  PIC X.                                          00001810
           02 MLIBERRO  PIC X(61).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCODTRAA  PIC X.                                          00001840
           02 MCODTRAC  PIC X.                                          00001850
           02 MCODTRAP  PIC X.                                          00001860
           02 MCODTRAH  PIC X.                                          00001870
           02 MCODTRAV  PIC X.                                          00001880
           02 MCODTRAO  PIC X(4).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCICSA    PIC X.                                          00001910
           02 MCICSC    PIC X.                                          00001920
           02 MCICSP    PIC X.                                          00001930
           02 MCICSH    PIC X.                                          00001940
           02 MCICSV    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
