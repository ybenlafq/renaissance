      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TFL21 (TFL10 -> MENU)    TR: FL10  *    00002211
      *          GESTION DES PROFILS D'AFFILIATION                 *    00002311
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *  TRANSACTION FL21 : EXCEPTION DES CODIC                    *    00411011
      *                                                                 00412000
          02 COMM-FL21-APPLI REDEFINES COMM-FL10-APPLI.                 00420011
      *------------------------------ ZONE DONNEES TFL21                00510011
             03 COMM-FL21-DONNEES-1-TFL21.                              00520011
      *------------------------------ CODE FONCTION                     00550001
                04 COMM-FL21-FONCT             PIC X(3).                00560011
      *------------------------------ CODE PROFIL                       00561001
                04 COMM-FL21-CPROAFF           PIC X(5).                00562011
      *------------------------------ LIBELLE PROFIL                    00570001
                04 COMM-FL21-LPROAFF           PIC X(20).               00580011
      *------------------------------ DERNIER IND-TS                    00742401
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL21-PAGSUI            PIC S9(4) COMP.          00742511
      *--                                                                       
                04 COMM-FL21-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN IND-TS                     00742601
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL21-PAGENC            PIC S9(4) COMP.          00742711
      *--                                                                       
                04 COMM-FL21-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00742801
                04 COMM-FL21-NUMPAG            PIC 9(3).                00742911
      *------------------------------ NOMBRE DE PAGE                    00743013
                04 COMM-FL21-NBRPAG            PIC 9(3).                00743113
      *------------------------------ INDICATEUR FIN DE TABLE           00743201
                04 COMM-FL21-INDPAG            PIC 9.                   00743311
      *------------------------------ ZONE MODIFICATION TS              00743416
                04 COMM-FL21-MODIF             PIC 9(1).                00743516
      *------------------------------ ZONE CONFIRMATION PF4             00743601
                04 COMM-FL21-CONF-PF4          PIC 9(1).                00743711
      *------------------------------ OK MAJ SUR RTFL06 => VALID = '1'  00743811
                04 COMM-FL21-VALID-RTFL06   PIC  9.                     00743911
      *------------------------------ TABLE N� TS PR PAGINATION         00744201
                04 COMM-FL21-TABTS          OCCURS 100.                 00744311
      *------------------------------ 1ERS IND TS DES PAGES <=> FAMILLE 00744401
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               06 COMM-FL21-NUMTS          PIC S9(4) COMP.       00744511
      *--                                                                       
                      06 COMM-FL21-NUMTS          PIC S9(4) COMP-5.             
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS               00744601
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL21-IND-RECH-TS    PIC S9(4) COMP.             00745011
      *--                                                                       
                04 COMM-FL21-IND-RECH-TS    PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND TS MAX                        00746001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL21-IND-TS-MAX     PIC S9(4) COMP.             00747011
      *--                                                                       
                04 COMM-FL21-IND-TS-MAX     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ ZONE DONNEES 2 TFL21              00760011
             03 COMM-FL21-DONNEES-2-TFL21.                              00761011
      *------------------------------ CODES D'AFFILIATION PAR DEFAUT    00762001
                04 COMM-FL21-ENTAFF-DF      OCCURS 4.                   00763012
      *------------------------------ CODE SOCIETE D'AFFILIATION DEFAUT 00764001
                   05 COMM-FL21-CSOCAFF-DF  PIC X(03).                  00765011
      *------------------------------ CODE ENTREPOT D'AFFILIATION DEFAUT00766001
                   05 COMM-FL21-CDEPAFF-DF  PIC X(03).                  00767011
      *------------------------------ ZONE FAMILLES PAGE                00770001
                04 COMM-FL21-TABFAM.                                    00780011
      *------------------------------ ZONE LIGNES FAMILLES              00790001
                   05 COMM-FL21-LIGFAM  OCCURS 15.                      00800011
      *------------------------------ CODIC                             00801013
                      06 COMM-FL21-CDAC           PIC X(1).             00802013
      *------------------------------ CODIC                             00810011
                      06 COMM-FL21-CODIC          PIC X(7).             00820011
      *------------------------------ LIBELLE CODIC                     00830011
                      06 COMM-FL21-LCODIC         PIC X(20).            00840011
      *------------------------------ CODE MARQUE CODIC                 00841011
                      06 COMM-FL21-CDMARQ         PIC X(5).             00842013
      *------------------------------ CODES D'AFFILIATION               00850001
                      06 COMM-FL21-ENTAFF         OCCURS 4.             00860011
      *------------------------------ CODE SOCIETE D'AFFILIATION        00870001
                         07 COMM-FL21-CSOCAFF     PIC X(03).            00880011
      *------------------------------ CODE ENTREPOT D'AFFILIATION       00890001
                         07 COMM-FL21-CDEPAFF     PIC X(03).            00900011
      *------------------------------ ZONE DONNEES 3 TFL21              01650011
             03 COMM-FL21-DONNEES-3-TFL21.                              01651011
      *------------------------------ ZONES SWAP                        01670001
                   05 COMM-FL21-ATTR           PIC X OCCURS 500.        01680011
      *------------------------------ ZONE LIBRE                        01710001
             03 COMM-FL21-LIBRE          PIC X(2097).                   01720015
      ***************************************************************** 02170001
                                                                                
