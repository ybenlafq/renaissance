      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * COMMAREA DE L'ECOUTEUR DE LA TABLE RTLV98 DES STOCKS INNOVENTE          
      *****************************************************************         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COM-LV50-LONG-COMMAREA PIC S9(4) COMP VALUE +70.                      
      *--                                                                       
       01 COM-LV50-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +70.                    
      *}                                                                        
       01 COMM-LV50.                                                            
          02 COMM-LV50-ALLEE.                                                   
             03 COMM-LV50-CLE                 PIC X(50).                        
          02 COMM-LV50-RETOUR.                                                  
             03 COMM-LV50-CRETOUR             PIC X(02).                        
             03 COMM-LV50-LRETOUR             PIC X(20).                        
      *                                                                         
                                                                                
