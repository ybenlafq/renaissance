      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFA0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFA0100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFA0100.                                                            
           02  FA01-NSOC                                                        
               PIC X(0003).                                                     
           02  FA01-NLIEU                                                       
               PIC X(0003).                                                     
           02  FA01-NFACT                                                       
               PIC X(0007).                                                     
           02  FA01-DFACT                                                       
               PIC X(0008).                                                     
           02  FA01-HFACT                                                       
               PIC X(0006).                                                     
           02  FA01-CODOPE                                                      
               PIC X(0007).                                                     
           02  FA01-PFACRF                                                      
               PIC S9(07)V9(02) COMP-3.                                         
           02  FA01-CDEVRF                                                      
               PIC X(0003).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFA0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFA0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA01-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA01-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA01-NFACT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA01-NFACT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA01-DFACT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA01-DFACT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA01-HFACT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA01-HFACT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA01-CODOPE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA01-CODOPE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA01-PFACRF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA01-PFACRF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA01-CDEVRF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA01-CDEVRF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
