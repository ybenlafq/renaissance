      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM03   ERM03                                              00000020
      ***************************************************************** 00000030
       01   ERM03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCGROUPI  PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQINDISDL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MQINDISDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQINDISDF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MQINDISDI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPONDDL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MQPONDDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPONDDF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MQPONDDI  PIC X(4).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNINDIDL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNINDIDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNINDIDF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNINDIDI  PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBSEML  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MQNBSEML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQNBSEMF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MQNBSEMI  PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQSEUILDL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MQSEUILDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQSEUILDF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MQSEUILDI      PIC X(6).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPVMDL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MQPVMDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQPVMDF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MQPVMDI   PIC X(4).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQEXPMAXDL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MQEXPMAXDL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQEXPMAXDF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MQEXPMAXDI     PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQLSMAXDL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MQLSMAXDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQLSMAXDF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MQLSMAXDI      PIC X(5).                                  00000530
           02 LIGNEI OCCURS   13 TIMES .                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCFAMI  PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLFAMI  PIC X(20).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQINDISPL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MQINDISPL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQINDISPF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQINDISPI    PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPONDL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MQPONDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPONDF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQPONDI      PIC X(4).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNINDICL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNINDICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNINDICF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNINDICI     PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSEUILL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MQSEUILL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQSEUILF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQSEUILI     PIC X(6).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVML  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MQPVML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQPVMF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQPVMI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPMAXL    COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQEXPMAXL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQEXPMAXF    PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQEXPMAXI    PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLSMAXL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQLSMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQLSMAXF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQLSMAXI     PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(12).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(61).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: ERM03   ERM03                                              00001160
      ***************************************************************** 00001170
       01   ERM03O REDEFINES ERM03I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCGROUPA  PIC X.                                          00001350
           02 MCGROUPC  PIC X.                                          00001360
           02 MCGROUPP  PIC X.                                          00001370
           02 MCGROUPH  PIC X.                                          00001380
           02 MCGROUPV  PIC X.                                          00001390
           02 MCGROUPO  PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNPAGEA   PIC X.                                          00001420
           02 MNPAGEC   PIC X.                                          00001430
           02 MNPAGEP   PIC X.                                          00001440
           02 MNPAGEH   PIC X.                                          00001450
           02 MNPAGEV   PIC X.                                          00001460
           02 MNPAGEO   PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MQINDISDA      PIC X.                                     00001490
           02 MQINDISDC PIC X.                                          00001500
           02 MQINDISDP PIC X.                                          00001510
           02 MQINDISDH PIC X.                                          00001520
           02 MQINDISDV PIC X.                                          00001530
           02 MQINDISDO      PIC X(5).                                  00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MQPONDDA  PIC X.                                          00001560
           02 MQPONDDC  PIC X.                                          00001570
           02 MQPONDDP  PIC X.                                          00001580
           02 MQPONDDH  PIC X.                                          00001590
           02 MQPONDDV  PIC X.                                          00001600
           02 MQPONDDO  PIC X(4).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNINDIDA  PIC X.                                          00001630
           02 MNINDIDC  PIC X.                                          00001640
           02 MNINDIDP  PIC X.                                          00001650
           02 MNINDIDH  PIC X.                                          00001660
           02 MNINDIDV  PIC X.                                          00001670
           02 MNINDIDO  PIC X.                                          00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MQNBSEMA  PIC X.                                          00001700
           02 MQNBSEMC  PIC X.                                          00001710
           02 MQNBSEMP  PIC X.                                          00001720
           02 MQNBSEMH  PIC X.                                          00001730
           02 MQNBSEMV  PIC X.                                          00001740
           02 MQNBSEMO  PIC X.                                          00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MQSEUILDA      PIC X.                                     00001770
           02 MQSEUILDC PIC X.                                          00001780
           02 MQSEUILDP PIC X.                                          00001790
           02 MQSEUILDH PIC X.                                          00001800
           02 MQSEUILDV PIC X.                                          00001810
           02 MQSEUILDO      PIC X(6).                                  00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MQPVMDA   PIC X.                                          00001840
           02 MQPVMDC   PIC X.                                          00001850
           02 MQPVMDP   PIC X.                                          00001860
           02 MQPVMDH   PIC X.                                          00001870
           02 MQPVMDV   PIC X.                                          00001880
           02 MQPVMDO   PIC X(4).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MQEXPMAXDA     PIC X.                                     00001910
           02 MQEXPMAXDC     PIC X.                                     00001920
           02 MQEXPMAXDP     PIC X.                                     00001930
           02 MQEXPMAXDH     PIC X.                                     00001940
           02 MQEXPMAXDV     PIC X.                                     00001950
           02 MQEXPMAXDO     PIC X(5).                                  00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MQLSMAXDA      PIC X.                                     00001980
           02 MQLSMAXDC PIC X.                                          00001990
           02 MQLSMAXDP PIC X.                                          00002000
           02 MQLSMAXDH PIC X.                                          00002010
           02 MQLSMAXDV PIC X.                                          00002020
           02 MQLSMAXDO      PIC X(5).                                  00002030
           02 LIGNEO OCCURS   13 TIMES .                                00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MCFAMA  PIC X.                                          00002060
             03 MCFAMC  PIC X.                                          00002070
             03 MCFAMP  PIC X.                                          00002080
             03 MCFAMH  PIC X.                                          00002090
             03 MCFAMV  PIC X.                                          00002100
             03 MCFAMO  PIC X(5).                                       00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MLFAMA  PIC X.                                          00002130
             03 MLFAMC  PIC X.                                          00002140
             03 MLFAMP  PIC X.                                          00002150
             03 MLFAMH  PIC X.                                          00002160
             03 MLFAMV  PIC X.                                          00002170
             03 MLFAMO  PIC X(20).                                      00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MQINDISPA    PIC X.                                     00002200
             03 MQINDISPC    PIC X.                                     00002210
             03 MQINDISPP    PIC X.                                     00002220
             03 MQINDISPH    PIC X.                                     00002230
             03 MQINDISPV    PIC X.                                     00002240
             03 MQINDISPO    PIC X(5).                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MQPONDA      PIC X.                                     00002270
             03 MQPONDC PIC X.                                          00002280
             03 MQPONDP PIC X.                                          00002290
             03 MQPONDH PIC X.                                          00002300
             03 MQPONDV PIC X.                                          00002310
             03 MQPONDO      PIC X(4).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MNINDICA     PIC X.                                     00002340
             03 MNINDICC     PIC X.                                     00002350
             03 MNINDICP     PIC X.                                     00002360
             03 MNINDICH     PIC X.                                     00002370
             03 MNINDICV     PIC X.                                     00002380
             03 MNINDICO     PIC X.                                     00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MQSEUILA     PIC X.                                     00002410
             03 MQSEUILC     PIC X.                                     00002420
             03 MQSEUILP     PIC X.                                     00002430
             03 MQSEUILH     PIC X.                                     00002440
             03 MQSEUILV     PIC X.                                     00002450
             03 MQSEUILO     PIC X(6).                                  00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MQPVMA  PIC X.                                          00002480
             03 MQPVMC  PIC X.                                          00002490
             03 MQPVMP  PIC X.                                          00002500
             03 MQPVMH  PIC X.                                          00002510
             03 MQPVMV  PIC X.                                          00002520
             03 MQPVMO  PIC X(4).                                       00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MQEXPMAXA    PIC X.                                     00002550
             03 MQEXPMAXC    PIC X.                                     00002560
             03 MQEXPMAXP    PIC X.                                     00002570
             03 MQEXPMAXH    PIC X.                                     00002580
             03 MQEXPMAXV    PIC X.                                     00002590
             03 MQEXPMAXO    PIC X(5).                                  00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MQLSMAXA     PIC X.                                     00002620
             03 MQLSMAXC     PIC X.                                     00002630
             03 MQLSMAXP     PIC X.                                     00002640
             03 MQLSMAXH     PIC X.                                     00002650
             03 MQLSMAXV     PIC X.                                     00002660
             03 MQLSMAXO     PIC X(5).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(12).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(61).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
