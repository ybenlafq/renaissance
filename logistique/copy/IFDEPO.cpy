      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IFDEPO AU 22/02/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,09,BI,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IFDEPO.                                                        
            05 NOMETAT-IFDEPO           PIC X(6) VALUE 'IFDEPO'.                
            05 RUPTURES-IFDEPO.                                                 
           10 IFDEPO-CTRAIT             PIC X(05).                      007  005
           10 IFDEPO-NLIEU              PIC X(09).                      012  009
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IFDEPO-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IFDEPO-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IFDEPO.                                                   
           10 IFDEPO-ANNEE              PIC X(04).                      023  004
           10 IFDEPO-CFAM               PIC X(05).                      027  005
           10 IFDEPO-CLIEUTRTMVTD       PIC X(05).                      032  005
           10 IFDEPO-CMARQ              PIC X(05).                      037  005
           10 IFDEPO-DEV-ENCOURS        PIC X(03).                      042  003
           10 IFDEPO-DEV-EQU            PIC X(03).                      045  003
           10 IFDEPO-LREFFOURN          PIC X(20).                      048  020
           10 IFDEPO-MOIS               PIC X(02).                      068  002
           10 IFDEPO-NCODIC             PIC X(07).                      070  007
           10 IFDEPO-NORIGINE           PIC X(07).                      077  007
           10 IFDEPO-NRENDU             PIC X(20).                      084  020
           10 IFDEPO-NSSLIEUMVTD        PIC X(03).                      104  003
           10 IFDEPO-PRMP               PIC S9(08)V9(2) COMP-3.         107  006
           10 IFDEPO-QTRENDU            PIC S9(05)      COMP-3.         113  003
           10 IFDEPO-TAUX               PIC S9(01)V9(6) COMP-3.         116  004
            05 FILLER                      PIC X(393).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IFDEPO-LONG           PIC S9(4)   COMP  VALUE +119.           
      *                                                                         
      *--                                                                       
        01  DSECT-IFDEPO-LONG           PIC S9(4) COMP-5  VALUE +119.           
                                                                                
      *}                                                                        
