      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TLW02                                          *  00020001
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-LW02-LONG            PIC S9(3) COMP-3 VALUE 71.           00060002
       01  TS-LW02-DONNEES.                                             00070001
              10 TS-LW02-DJOUR     PIC X(08).                           00080001
              10 TS-LW02-NSOCDEPOT PIC X(03).                           00090001
              10 TS-LW02-NDEPOT    PIC X(03).                           00100001
              10 TS-LW02-NMUT      PIC X(07).                           00110001
              10 TS-LW02-NSOC      PIC X(03).                           00120001
              10 TS-LW02-NLIEU     PIC X(03).                           00130001
              10 TS-LW02-NSELAR    PIC X(05).                           00140001
              10 TS-LW02-NQP       PIC X(05).                           00150001
              10 TS-LW02-NQV       PIC X(06).                           00160001
              10 TS-LW02-NQQP      PIC X(05).                           00170001
              10 TS-LW02-NQQV      PIC X(06).                           00180001
              10 TS-LW02-NDDEST    PIC X(10).                           00190001
              10 TS-LW02-HCHRGT    PIC X(05).                           00191002
              10 TS-LW02-SELECT    PIC X.                               00200001
              10 TS-LW02-TRANSFERT PIC X.                               00210001
                                                                                
