      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EEF01   EEF01                                              00000020
      ***************************************************************** 00000030
       01   EEF01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIXL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCHOIXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHOIXF   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCHOIXI   PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MCTRAITI  PIC X(5).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MLTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRAITF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLTRAITI  PIC X(30).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLTIERSI  PIC X(15).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTIERSI  PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITOL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MLTRAITOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLTRAITOF      PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLTRAITOI      PIC X(19).                                 00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITOL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MCTRAITOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTRAITOF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCTRAITOI      PIC X(5).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCTRAITOL     COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MLCTRAITOL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCTRAITOF     PIC X.                                     00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MLCTRAITOI     PIC X(20).                                 00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MNENVOII  PIC X(7).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MDENVOII  PIC X(10).                                      00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOI-FL     COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MDENVOI-FL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDENVOI-FF     PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MDENVOI-FI     PIC X(10).                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCIMPL    COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCIMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCIMPF    PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCIMPI    PIC X(4).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCORDL      COMP PIC S9(4).                            00000640
      *--                                                                       
           02 MNACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNACCORDF      PIC X.                                     00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MNACCORDI      PIC X(12).                                 00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACCORDL      COMP PIC S9(4).                            00000680
      *--                                                                       
           02 MDACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDACCORDF      PIC X.                                     00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MDACCORDI      PIC X(10).                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHSL      COMP PIC S9(4).                            00000720
      *--                                                                       
           02 MNLIEUHSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHSF      PIC X.                                     00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MNLIEUHSI      PIC X(3).                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNIDENTIFL     COMP PIC S9(4).                            00000760
      *--                                                                       
           02 MNIDENTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNIDENTIFF     PIC X.                                     00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MNIDENTIFI     PIC X(7).                                  00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRENDUL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MNRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRENDUF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MNRENDUI  PIC X(20).                                      00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRENDUL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MDRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRENDUF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MDRENDUI  PIC X(10).                                      00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMRENDUL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MMRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMRENDUF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MMRENDUI  PIC X(5).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAUTOLGL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MAUTOLGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MAUTOLGF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MAUTOLGI  PIC X.                                          00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGLOBALL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MGLOBALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MGLOBALF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MGLOBALI  PIC X.                                          00000990
           02 MMENUOPTI OCCURS   8 TIMES .                              00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOPT1L      COMP PIC S9(4).                            00001010
      *--                                                                       
             03 MNOPT1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNOPT1F      PIC X.                                     00001020
             03 FILLER  PIC X(4).                                       00001030
             03 MNOPT1I      PIC X(2).                                  00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTIRET1L     COMP PIC S9(4).                            00001050
      *--                                                                       
             03 MTIRET1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTIRET1F     PIC X.                                     00001060
             03 FILLER  PIC X(4).                                       00001070
             03 MTIRET1I     PIC X.                                     00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLOPT1L      COMP PIC S9(4).                            00001090
      *--                                                                       
             03 MLOPT1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLOPT1F      PIC X.                                     00001100
             03 FILLER  PIC X(4).                                       00001110
             03 MLOPT1I      PIC X(30).                                 00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOPT2L      COMP PIC S9(4).                            00001130
      *--                                                                       
             03 MNOPT2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNOPT2F      PIC X.                                     00001140
             03 FILLER  PIC X(4).                                       00001150
             03 MNOPT2I      PIC X(2).                                  00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTIRET2L     COMP PIC S9(4).                            00001170
      *--                                                                       
             03 MTIRET2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTIRET2F     PIC X.                                     00001180
             03 FILLER  PIC X(4).                                       00001190
             03 MTIRET2I     PIC X.                                     00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLOPT2L      COMP PIC S9(4).                            00001210
      *--                                                                       
             03 MLOPT2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLOPT2F      PIC X.                                     00001220
             03 FILLER  PIC X(4).                                       00001230
             03 MLOPT2I      PIC X(30).                                 00001240
      * MESSAGE ERREUR                                                  00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MLIBERRI  PIC X(78).                                      00001290
      * CODE TRANSACTION                                                00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCODTRAI  PIC X(4).                                       00001340
      * CICS DE TRAVAIL                                                 00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MCICSI    PIC X(5).                                       00001390
      * NETNAME                                                         00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001410
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001420
           02 FILLER    PIC X(4).                                       00001430
           02 MNETNAMI  PIC X(8).                                       00001440
      * CODE TERMINAL                                                   00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MSCREENI  PIC X(5).                                       00001490
      ***************************************************************** 00001500
      * SDF: EEF01   EEF01                                              00001510
      ***************************************************************** 00001520
       01   EEF01O REDEFINES EEF01I.                                    00001530
           02 FILLER    PIC X(12).                                      00001540
      * DATE DU JOUR                                                    00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDATJOUA  PIC X.                                          00001570
           02 MDATJOUC  PIC X.                                          00001580
           02 MDATJOUP  PIC X.                                          00001590
           02 MDATJOUH  PIC X.                                          00001600
           02 MDATJOUV  PIC X.                                          00001610
           02 MDATJOUO  PIC X(10).                                      00001620
      * HEURE                                                           00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MTIMJOUA  PIC X.                                          00001650
           02 MTIMJOUC  PIC X.                                          00001660
           02 MTIMJOUP  PIC X.                                          00001670
           02 MTIMJOUH  PIC X.                                          00001680
           02 MTIMJOUV  PIC X.                                          00001690
           02 MTIMJOUO  PIC X(5).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MCHOIXA   PIC X.                                          00001720
           02 MCHOIXC   PIC X.                                          00001730
           02 MCHOIXP   PIC X.                                          00001740
           02 MCHOIXH   PIC X.                                          00001750
           02 MCHOIXV   PIC X.                                          00001760
           02 MCHOIXO   PIC X(2).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCTRAITA  PIC X.                                          00001790
           02 MCTRAITC  PIC X.                                          00001800
           02 MCTRAITP  PIC X.                                          00001810
           02 MCTRAITH  PIC X.                                          00001820
           02 MCTRAITV  PIC X.                                          00001830
           02 MCTRAITO  PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLTRAITA  PIC X.                                          00001860
           02 MLTRAITC  PIC X.                                          00001870
           02 MLTRAITP  PIC X.                                          00001880
           02 MLTRAITH  PIC X.                                          00001890
           02 MLTRAITV  PIC X.                                          00001900
           02 MLTRAITO  PIC X(30).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MLTIERSA  PIC X.                                          00001930
           02 MLTIERSC  PIC X.                                          00001940
           02 MLTIERSP  PIC X.                                          00001950
           02 MLTIERSH  PIC X.                                          00001960
           02 MLTIERSV  PIC X.                                          00001970
           02 MLTIERSO  PIC X(15).                                      00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MCTIERSA  PIC X.                                          00002000
           02 MCTIERSC  PIC X.                                          00002010
           02 MCTIERSP  PIC X.                                          00002020
           02 MCTIERSH  PIC X.                                          00002030
           02 MCTIERSV  PIC X.                                          00002040
           02 MCTIERSO  PIC X(5).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MLTRAITOA      PIC X.                                     00002070
           02 MLTRAITOC PIC X.                                          00002080
           02 MLTRAITOP PIC X.                                          00002090
           02 MLTRAITOH PIC X.                                          00002100
           02 MLTRAITOV PIC X.                                          00002110
           02 MLTRAITOO      PIC X(19).                                 00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCTRAITOA      PIC X.                                     00002140
           02 MCTRAITOC PIC X.                                          00002150
           02 MCTRAITOP PIC X.                                          00002160
           02 MCTRAITOH PIC X.                                          00002170
           02 MCTRAITOV PIC X.                                          00002180
           02 MCTRAITOO      PIC X(5).                                  00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLCTRAITOA     PIC X.                                     00002210
           02 MLCTRAITOC     PIC X.                                     00002220
           02 MLCTRAITOP     PIC X.                                     00002230
           02 MLCTRAITOH     PIC X.                                     00002240
           02 MLCTRAITOV     PIC X.                                     00002250
           02 MLCTRAITOO     PIC X(20).                                 00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MNENVOIA  PIC X.                                          00002280
           02 MNENVOIC  PIC X.                                          00002290
           02 MNENVOIP  PIC X.                                          00002300
           02 MNENVOIH  PIC X.                                          00002310
           02 MNENVOIV  PIC X.                                          00002320
           02 MNENVOIO  PIC 9999999.                                    00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MDENVOIA  PIC X.                                          00002350
           02 MDENVOIC  PIC X.                                          00002360
           02 MDENVOIP  PIC X.                                          00002370
           02 MDENVOIH  PIC X.                                          00002380
           02 MDENVOIV  PIC X.                                          00002390
           02 MDENVOIO  PIC X(10).                                      00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MDENVOI-FA     PIC X.                                     00002420
           02 MDENVOI-FC     PIC X.                                     00002430
           02 MDENVOI-FP     PIC X.                                     00002440
           02 MDENVOI-FH     PIC X.                                     00002450
           02 MDENVOI-FV     PIC X.                                     00002460
           02 MDENVOI-FO     PIC X(10).                                 00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MCIMPA    PIC X.                                          00002490
           02 MCIMPC    PIC X.                                          00002500
           02 MCIMPP    PIC X.                                          00002510
           02 MCIMPH    PIC X.                                          00002520
           02 MCIMPV    PIC X.                                          00002530
           02 MCIMPO    PIC X(4).                                       00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MNACCORDA      PIC X.                                     00002560
           02 MNACCORDC PIC X.                                          00002570
           02 MNACCORDP PIC X.                                          00002580
           02 MNACCORDH PIC X.                                          00002590
           02 MNACCORDV PIC X.                                          00002600
           02 MNACCORDO      PIC 999999999999.                          00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MDACCORDA      PIC X.                                     00002630
           02 MDACCORDC PIC X.                                          00002640
           02 MDACCORDP PIC X.                                          00002650
           02 MDACCORDH PIC X.                                          00002660
           02 MDACCORDV PIC X.                                          00002670
           02 MDACCORDO      PIC X(10).                                 00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MNLIEUHSA      PIC X.                                     00002700
           02 MNLIEUHSC PIC X.                                          00002710
           02 MNLIEUHSP PIC X.                                          00002720
           02 MNLIEUHSH PIC X.                                          00002730
           02 MNLIEUHSV PIC X.                                          00002740
           02 MNLIEUHSO      PIC X(3).                                  00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MNIDENTIFA     PIC X.                                     00002770
           02 MNIDENTIFC     PIC X.                                     00002780
           02 MNIDENTIFP     PIC X.                                     00002790
           02 MNIDENTIFH     PIC X.                                     00002800
           02 MNIDENTIFV     PIC X.                                     00002810
           02 MNIDENTIFO     PIC 9999999.                               00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MNRENDUA  PIC X.                                          00002840
           02 MNRENDUC  PIC X.                                          00002850
           02 MNRENDUP  PIC X.                                          00002860
           02 MNRENDUH  PIC X.                                          00002870
           02 MNRENDUV  PIC X.                                          00002880
           02 MNRENDUO  PIC X(20).                                      00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MDRENDUA  PIC X.                                          00002910
           02 MDRENDUC  PIC X.                                          00002920
           02 MDRENDUP  PIC X.                                          00002930
           02 MDRENDUH  PIC X.                                          00002940
           02 MDRENDUV  PIC X.                                          00002950
           02 MDRENDUO  PIC X(10).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MMRENDUA  PIC X.                                          00002980
           02 MMRENDUC  PIC X.                                          00002990
           02 MMRENDUP  PIC X.                                          00003000
           02 MMRENDUH  PIC X.                                          00003010
           02 MMRENDUV  PIC X.                                          00003020
           02 MMRENDUO  PIC X(5).                                       00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MAUTOLGA  PIC X.                                          00003050
           02 MAUTOLGC  PIC X.                                          00003060
           02 MAUTOLGP  PIC X.                                          00003070
           02 MAUTOLGH  PIC X.                                          00003080
           02 MAUTOLGV  PIC X.                                          00003090
           02 MAUTOLGO  PIC X.                                          00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MGLOBALA  PIC X.                                          00003120
           02 MGLOBALC  PIC X.                                          00003130
           02 MGLOBALP  PIC X.                                          00003140
           02 MGLOBALH  PIC X.                                          00003150
           02 MGLOBALV  PIC X.                                          00003160
           02 MGLOBALO  PIC X.                                          00003170
           02 MMENUOPTO OCCURS   8 TIMES .                              00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MNOPT1A      PIC X.                                     00003200
             03 MNOPT1C PIC X.                                          00003210
             03 MNOPT1P PIC X.                                          00003220
             03 MNOPT1H PIC X.                                          00003230
             03 MNOPT1V PIC X.                                          00003240
             03 MNOPT1O      PIC X(2).                                  00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MTIRET1A     PIC X.                                     00003270
             03 MTIRET1C     PIC X.                                     00003280
             03 MTIRET1P     PIC X.                                     00003290
             03 MTIRET1H     PIC X.                                     00003300
             03 MTIRET1V     PIC X.                                     00003310
             03 MTIRET1O     PIC X.                                     00003320
             03 FILLER       PIC X(2).                                  00003330
             03 MLOPT1A      PIC X.                                     00003340
             03 MLOPT1C PIC X.                                          00003350
             03 MLOPT1P PIC X.                                          00003360
             03 MLOPT1H PIC X.                                          00003370
             03 MLOPT1V PIC X.                                          00003380
             03 MLOPT1O      PIC X(30).                                 00003390
             03 FILLER       PIC X(2).                                  00003400
             03 MNOPT2A      PIC X.                                     00003410
             03 MNOPT2C PIC X.                                          00003420
             03 MNOPT2P PIC X.                                          00003430
             03 MNOPT2H PIC X.                                          00003440
             03 MNOPT2V PIC X.                                          00003450
             03 MNOPT2O      PIC X(2).                                  00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MTIRET2A     PIC X.                                     00003480
             03 MTIRET2C     PIC X.                                     00003490
             03 MTIRET2P     PIC X.                                     00003500
             03 MTIRET2H     PIC X.                                     00003510
             03 MTIRET2V     PIC X.                                     00003520
             03 MTIRET2O     PIC X.                                     00003530
             03 FILLER       PIC X(2).                                  00003540
             03 MLOPT2A      PIC X.                                     00003550
             03 MLOPT2C PIC X.                                          00003560
             03 MLOPT2P PIC X.                                          00003570
             03 MLOPT2H PIC X.                                          00003580
             03 MLOPT2V PIC X.                                          00003590
             03 MLOPT2O      PIC X(30).                                 00003600
      * MESSAGE ERREUR                                                  00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MLIBERRA  PIC X.                                          00003630
           02 MLIBERRC  PIC X.                                          00003640
           02 MLIBERRP  PIC X.                                          00003650
           02 MLIBERRH  PIC X.                                          00003660
           02 MLIBERRV  PIC X.                                          00003670
           02 MLIBERRO  PIC X(78).                                      00003680
      * CODE TRANSACTION                                                00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MCODTRAA  PIC X.                                          00003710
           02 MCODTRAC  PIC X.                                          00003720
           02 MCODTRAP  PIC X.                                          00003730
           02 MCODTRAH  PIC X.                                          00003740
           02 MCODTRAV  PIC X.                                          00003750
           02 MCODTRAO  PIC X(4).                                       00003760
      * CICS DE TRAVAIL                                                 00003770
           02 FILLER    PIC X(2).                                       00003780
           02 MCICSA    PIC X.                                          00003790
           02 MCICSC    PIC X.                                          00003800
           02 MCICSP    PIC X.                                          00003810
           02 MCICSH    PIC X.                                          00003820
           02 MCICSV    PIC X.                                          00003830
           02 MCICSO    PIC X(5).                                       00003840
      * NETNAME                                                         00003850
           02 FILLER    PIC X(2).                                       00003860
           02 MNETNAMA  PIC X.                                          00003870
           02 MNETNAMC  PIC X.                                          00003880
           02 MNETNAMP  PIC X.                                          00003890
           02 MNETNAMH  PIC X.                                          00003900
           02 MNETNAMV  PIC X.                                          00003910
           02 MNETNAMO  PIC X(8).                                       00003920
      * CODE TERMINAL                                                   00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MSCREENA  PIC X.                                          00003950
           02 MSCREENC  PIC X.                                          00003960
           02 MSCREENP  PIC X.                                          00003970
           02 MSCREENH  PIC X.                                          00003980
           02 MSCREENV  PIC X.                                          00003990
           02 MSCREENO  PIC X(5).                                       00004000
                                                                                
