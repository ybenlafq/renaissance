      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU61   EMU61                                              00000020
      ***************************************************************** 00000030
       01   EMU61I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDEPF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNSOCDEPI      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNDEPOTI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLDEPOTI  PIC X(20).                                      00000410
           02 FILLER  OCCURS   12 TIMES .                               00000420
      * code famille                                                    00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000440
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000450
             03 FILLER  PIC X(4).                                       00000460
             03 MCFAMI  PIC X(5).                                       00000470
      * code marque                                                     00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MCMARQI      PIC X(5).                                  00000520
      * reference                                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000540
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000550
             03 FILLER  PIC X(4).                                       00000560
             03 MREFI   PIC X(20).                                      00000570
      * codic                                                           00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCODICI      PIC X(7).                                  00000620
      * qte � muter                                                     00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000640
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000650
             03 FILLER  PIC X(4).                                       00000660
             03 MQTEI   PIC X(4).                                       00000670
      * n� document N�CDE                                               00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDOCL  COMP PIC S9(4).                                 00000690
      *--                                                                       
             03 MNDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNDOCF  PIC X.                                          00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MNDOCI  PIC X(7).                                       00000720
      * date mutation prochain                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMUTL  COMP PIC S9(4).                                 00000740
      *--                                                                       
             03 MDMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDMUTF  PIC X.                                          00000750
             03 FILLER  PIC X(4).                                       00000760
             03 MDMUTI  PIC X(6).                                       00000770
      * prix vente unitaire                                             00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPVUNL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MPVUNL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPVUNF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MPVUNI  PIC X(9).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSUPL   COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MSUPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSUPF   PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MSUPI   PIC X.                                          00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MZONCMDI  PIC X(15).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLIBERRI  PIC X(58).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCICSI    PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MNETNAMI  PIC X(8).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MSCREENI  PIC X(4).                                       00001100
      ***************************************************************** 00001110
      * SDF: EMU61   EMU61                                              00001120
      ***************************************************************** 00001130
       01   EMU61O REDEFINES EMU61I.                                    00001140
           02 FILLER    PIC X(12).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDATJOUA  PIC X.                                          00001170
           02 MDATJOUC  PIC X.                                          00001180
           02 MDATJOUP  PIC X.                                          00001190
           02 MDATJOUH  PIC X.                                          00001200
           02 MDATJOUV  PIC X.                                          00001210
           02 MDATJOUO  PIC X(10).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MPAGEA    PIC X.                                          00001310
           02 MPAGEC    PIC X.                                          00001320
           02 MPAGEP    PIC X.                                          00001330
           02 MPAGEH    PIC X.                                          00001340
           02 MPAGEV    PIC X.                                          00001350
           02 MPAGEO    PIC X(3).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MNSOCA    PIC X.                                          00001380
           02 MNSOCC    PIC X.                                          00001390
           02 MNSOCP    PIC X.                                          00001400
           02 MNSOCH    PIC X.                                          00001410
           02 MNSOCV    PIC X.                                          00001420
           02 MNSOCO    PIC X(3).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNLIEUA   PIC X.                                          00001450
           02 MNLIEUC   PIC X.                                          00001460
           02 MNLIEUP   PIC X.                                          00001470
           02 MNLIEUH   PIC X.                                          00001480
           02 MNLIEUV   PIC X.                                          00001490
           02 MNLIEUO   PIC X(3).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MLLIEUA   PIC X.                                          00001520
           02 MLLIEUC   PIC X.                                          00001530
           02 MLLIEUP   PIC X.                                          00001540
           02 MLLIEUH   PIC X.                                          00001550
           02 MLLIEUV   PIC X.                                          00001560
           02 MLLIEUO   PIC X(20).                                      00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNSOCDEPA      PIC X.                                     00001590
           02 MNSOCDEPC PIC X.                                          00001600
           02 MNSOCDEPP PIC X.                                          00001610
           02 MNSOCDEPH PIC X.                                          00001620
           02 MNSOCDEPV PIC X.                                          00001630
           02 MNSOCDEPO      PIC X(3).                                  00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MNDEPOTA  PIC X.                                          00001660
           02 MNDEPOTC  PIC X.                                          00001670
           02 MNDEPOTP  PIC X.                                          00001680
           02 MNDEPOTH  PIC X.                                          00001690
           02 MNDEPOTV  PIC X.                                          00001700
           02 MNDEPOTO  PIC X(3).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MLDEPOTA  PIC X.                                          00001730
           02 MLDEPOTC  PIC X.                                          00001740
           02 MLDEPOTP  PIC X.                                          00001750
           02 MLDEPOTH  PIC X.                                          00001760
           02 MLDEPOTV  PIC X.                                          00001770
           02 MLDEPOTO  PIC X(20).                                      00001780
           02 FILLER  OCCURS   12 TIMES .                               00001790
      * code famille                                                    00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MCFAMA  PIC X.                                          00001820
             03 MCFAMC  PIC X.                                          00001830
             03 MCFAMP  PIC X.                                          00001840
             03 MCFAMH  PIC X.                                          00001850
             03 MCFAMV  PIC X.                                          00001860
             03 MCFAMO  PIC X(5).                                       00001870
      * code marque                                                     00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MCMARQA      PIC X.                                     00001900
             03 MCMARQC PIC X.                                          00001910
             03 MCMARQP PIC X.                                          00001920
             03 MCMARQH PIC X.                                          00001930
             03 MCMARQV PIC X.                                          00001940
             03 MCMARQO      PIC X(5).                                  00001950
      * reference                                                       00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MREFA   PIC X.                                          00001980
             03 MREFC   PIC X.                                          00001990
             03 MREFP   PIC X.                                          00002000
             03 MREFH   PIC X.                                          00002010
             03 MREFV   PIC X.                                          00002020
             03 MREFO   PIC X(20).                                      00002030
      * codic                                                           00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MCODICA      PIC X.                                     00002060
             03 MCODICC PIC X.                                          00002070
             03 MCODICP PIC X.                                          00002080
             03 MCODICH PIC X.                                          00002090
             03 MCODICV PIC X.                                          00002100
             03 MCODICO      PIC X(7).                                  00002110
      * qte � muter                                                     00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MQTEA   PIC X.                                          00002140
             03 MQTEC   PIC X.                                          00002150
             03 MQTEP   PIC X.                                          00002160
             03 MQTEH   PIC X.                                          00002170
             03 MQTEV   PIC X.                                          00002180
             03 MQTEO   PIC ZZZ9.                                       00002190
      * n� document N�CDE                                               00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MNDOCA  PIC X.                                          00002220
             03 MNDOCC  PIC X.                                          00002230
             03 MNDOCP  PIC X.                                          00002240
             03 MNDOCH  PIC X.                                          00002250
             03 MNDOCV  PIC X.                                          00002260
             03 MNDOCO  PIC X(7).                                       00002270
      * date mutation prochain                                          00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MDMUTA  PIC X.                                          00002300
             03 MDMUTC  PIC X.                                          00002310
             03 MDMUTP  PIC X.                                          00002320
             03 MDMUTH  PIC X.                                          00002330
             03 MDMUTV  PIC X.                                          00002340
             03 MDMUTO  PIC X(6).                                       00002350
      * prix vente unitaire                                             00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MPVUNA  PIC X.                                          00002380
             03 MPVUNC  PIC X.                                          00002390
             03 MPVUNP  PIC X.                                          00002400
             03 MPVUNH  PIC X.                                          00002410
             03 MPVUNV  PIC X.                                          00002420
             03 MPVUNO  PIC ZZZZZ9,99.                                  00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MSUPA   PIC X.                                          00002450
             03 MSUPC   PIC X.                                          00002460
             03 MSUPP   PIC X.                                          00002470
             03 MSUPH   PIC X.                                          00002480
             03 MSUPV   PIC X.                                          00002490
             03 MSUPO   PIC X.                                          00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MZONCMDA  PIC X.                                          00002520
           02 MZONCMDC  PIC X.                                          00002530
           02 MZONCMDP  PIC X.                                          00002540
           02 MZONCMDH  PIC X.                                          00002550
           02 MZONCMDV  PIC X.                                          00002560
           02 MZONCMDO  PIC X(15).                                      00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MLIBERRA  PIC X.                                          00002590
           02 MLIBERRC  PIC X.                                          00002600
           02 MLIBERRP  PIC X.                                          00002610
           02 MLIBERRH  PIC X.                                          00002620
           02 MLIBERRV  PIC X.                                          00002630
           02 MLIBERRO  PIC X(58).                                      00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MCODTRAA  PIC X.                                          00002660
           02 MCODTRAC  PIC X.                                          00002670
           02 MCODTRAP  PIC X.                                          00002680
           02 MCODTRAH  PIC X.                                          00002690
           02 MCODTRAV  PIC X.                                          00002700
           02 MCODTRAO  PIC X(4).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCICSA    PIC X.                                          00002730
           02 MCICSC    PIC X.                                          00002740
           02 MCICSP    PIC X.                                          00002750
           02 MCICSH    PIC X.                                          00002760
           02 MCICSV    PIC X.                                          00002770
           02 MCICSO    PIC X(5).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MNETNAMA  PIC X.                                          00002800
           02 MNETNAMC  PIC X.                                          00002810
           02 MNETNAMP  PIC X.                                          00002820
           02 MNETNAMH  PIC X.                                          00002830
           02 MNETNAMV  PIC X.                                          00002840
           02 MNETNAMO  PIC X(8).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MSCREENA  PIC X.                                          00002870
           02 MSCREENC  PIC X.                                          00002880
           02 MSCREENP  PIC X.                                          00002890
           02 MSCREENH  PIC X.                                          00002900
           02 MSCREENV  PIC X.                                          00002910
           02 MSCREENO  PIC X(4).                                       00002920
                                                                                
