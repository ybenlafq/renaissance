      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *            COMMAREA DES PROGRAMMES GESTION STOCKS MAG         *         
      *****************************************************************         
1                  02 COMM-GS15 REDEFINES COMM-GS10-APPLI.                      
                      03 COMM-GS15-PAG.                                         
                         05 COMM-GS15-PAGE        PIC 9(3).                     
                         05 COMM-GS15-DER-PAGE    PIC 9(3).                     
                         05 COMM-GS15-DERN-LIEU   PIC X(3).                     
                         05 COMM-GS15-CODE-FIN    PIC X.                        
                      03 COMM-GS15-DCDE           PIC X(8).                     
                      03 COMM-GS15-QTEDISP        PIC S9(5).                    
                      03 COMM-GS15-QTEPLAFT       PIC 9(5).                     
                      03 COMM-GS15-DMUT           PIC X(8).                     
                      03 COMM-GS15-QTEMUT         PIC 9(5).                     
                      03 COMM-GS15-STODEP         PIC S9(5).                    
                      03 COMM-GS15-CTYPLIEU       PIC X.                        
                      03 COMM-GS15-CTYPSOC        PIC X(3).                     
      *               03 COMM-GS15-FILLER         PIC X(3022).                  
MB                    03 COMM-GS15-NSOCDEPOT      PIC X(3).                     
MB                    03 COMM-GS15-NDEPOT         PIC X(3).                     
MB                    03 COMM-GS15-NLIEU          PIC X(3).                     
YS1364                03 COMM-GS15-NSOCDEPO1      PIC X(3).                     
YS1364                03 COMM-GS15-NDEPOT1        PIC X(3).                     
YS1364                03 COMM-GS15-STODEP1        PIC S9(5).                    
YS1364                03 COMM-GS15-DCDE1          PIC X(8).                     
YS1364                03 COMM-GS15-QTEDISP1       PIC S9(5).                    
NT    *YS1364         03 COMM-GS15-FILLER         PIC X(2473).                  
YS1364                03 COMM-GS15-FILLER         PIC X(2449).                  
      *                                                                         
                                                                                
