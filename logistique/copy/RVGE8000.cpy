      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGE8000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGE8000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGE8000.                                                            
           02  GE80-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GE80-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GE80-NCODIC                                                      
               PIC X(0007).                                                     
           02  GE80-QPCTM                                                       
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GE80-QPCTL                                                       
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GE80-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGE8000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGE8000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE80-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE80-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE80-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE80-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE80-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE80-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE80-QPCTM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE80-QPCTM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE80-QPCTL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE80-QPCTL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE80-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE80-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
