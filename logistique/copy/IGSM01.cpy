      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGSM01 AU 06/11/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,20,BI,A,                          *        
      *                           42,07,BI,A,                          *        
      *                           49,02,BI,A,                          *        
      *                           51,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGSM01.                                                        
            05 NOMETAT-IGSM01           PIC X(6) VALUE 'IGSM01'.                
            05 RUPTURES-IGSM01.                                                 
           10 IGSM01-CHEFP              PIC X(05).                      007  005
           10 IGSM01-CFAM               PIC X(05).                      012  005
           10 IGSM01-CMARQ              PIC X(05).                      017  005
           10 IGSM01-WTRI               PIC X(20).                      022  020
           10 IGSM01-NCODIC             PIC X(07).                      042  007
           10 IGSM01-NAGREGATED         PIC X(02).                      049  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGSM01-SEQUENCE           PIC S9(04) COMP.                051  002
      *--                                                                       
           10 IGSM01-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGSM01.                                                   
           10 IGSM01-COPERA1            PIC X(05).                      053  005
           10 IGSM01-COPERA2            PIC X(05).                      058  005
           10 IGSM01-COPERA3            PIC X(05).                      063  005
           10 IGSM01-LAGREGATED         PIC X(20).                      068  020
           10 IGSM01-LCHEFP             PIC X(20).                      088  020
           10 IGSM01-LFAM               PIC X(20).                      108  020
           10 IGSM01-LMARQ              PIC X(20).                      128  020
           10 IGSM01-LREFFOURN          PIC X(20).                      148  020
           10 IGSM01-LSTATCOMP          PIC X(03).                      168  003
           10 IGSM01-NOFFRE-1           PIC X(07).                      171  007
           10 IGSM01-NOFFRE-2           PIC X(07).                      178  007
           10 IGSM01-NOFFRE-3           PIC X(07).                      185  007
           10 IGSM01-MBU                PIC S9(03)V9(1) COMP-3.         192  003
           10 IGSM01-MBU-1              PIC S9(03)V9(1) COMP-3.         195  003
           10 IGSM01-MBU-2              PIC S9(03)V9(1) COMP-3.         198  003
           10 IGSM01-MBU-3              PIC S9(03)V9(1) COMP-3.         201  003
           10 IGSM01-P                  PIC S9(03)      COMP-3.         204  002
           10 IGSM01-PRIME              PIC S9(02)V9(2) COMP-3.         206  003
           10 IGSM01-PRIME-1            PIC S9(02)V9(2) COMP-3.         209  003
           10 IGSM01-PRIME-2            PIC S9(02)V9(2) COMP-3.         212  003
           10 IGSM01-PRIME-3            PIC S9(02)V9(2) COMP-3.         215  003
           10 IGSM01-PRIX               PIC S9(05)V9(2) COMP-3.         218  004
           10 IGSM01-PRIX-1             PIC S9(05)V9(2) COMP-3.         222  004
           10 IGSM01-PRIX-2             PIC S9(05)V9(2) COMP-3.         226  004
           10 IGSM01-PRIX-3             PIC S9(05)V9(2) COMP-3.         230  004
           10 IGSM01-P1                 PIC S9(03)      COMP-3.         234  002
           10 IGSM01-P2                 PIC S9(03)      COMP-3.         236  002
           10 IGSM01-P3                 PIC S9(03)      COMP-3.         238  002
           10 IGSM01-QS-0               PIC S9(04)      COMP-3.         240  003
           10 IGSM01-QS-0T              PIC S9(04)      COMP-3.         243  003
           10 IGSM01-QS-1               PIC S9(04)      COMP-3.         246  003
           10 IGSM01-QS-2               PIC S9(04)      COMP-3.         249  003
           10 IGSM01-QS-3               PIC S9(04)      COMP-3.         252  003
           10 IGSM01-QS-4               PIC S9(04)      COMP-3.         255  003
           10 IGSM01-QSTOCKDEP          PIC S9(05)      COMP-3.         258  003
           10 IGSM01-QSTOCKMAG          PIC S9(05)      COMP-3.         261  003
           10 IGSM01-QV4S               PIC S9(05)      COMP-3.         264  003
           10 IGSM01-QV4ST              PIC S9(05)      COMP-3.         267  003
           10 IGSM01-Q1S-0              PIC S9(04)      COMP-3.         270  003
           10 IGSM01-Q1S-1              PIC S9(04)      COMP-3.         273  003
           10 IGSM01-Q1S-2              PIC S9(04)      COMP-3.         276  003
           10 IGSM01-Q1S-3              PIC S9(04)      COMP-3.         279  003
           10 IGSM01-Q1S-4              PIC S9(04)      COMP-3.         282  003
           10 IGSM01-Q1V4S              PIC S9(05)      COMP-3.         285  003
           10 IGSM01-Q2S-0              PIC S9(04)      COMP-3.         288  003
           10 IGSM01-Q2S-1              PIC S9(04)      COMP-3.         291  003
           10 IGSM01-Q2S-2              PIC S9(04)      COMP-3.         294  003
           10 IGSM01-Q2S-3              PIC S9(04)      COMP-3.         297  003
           10 IGSM01-Q2S-4              PIC S9(04)      COMP-3.         300  003
           10 IGSM01-Q2V4S              PIC S9(05)      COMP-3.         303  003
           10 IGSM01-Q3S-0              PIC S9(04)      COMP-3.         306  003
           10 IGSM01-Q3S-1              PIC S9(04)      COMP-3.         309  003
           10 IGSM01-Q3S-2              PIC S9(04)      COMP-3.         312  003
           10 IGSM01-Q3S-3              PIC S9(04)      COMP-3.         315  003
           10 IGSM01-Q3S-4              PIC S9(04)      COMP-3.         318  003
           10 IGSM01-Q3V4S              PIC S9(05)      COMP-3.         321  003
           10 IGSM01-REFI-1             PIC S9(04)V9(2) COMP-3.         324  004
           10 IGSM01-REFI-2             PIC S9(04)V9(2) COMP-3.         328  004
           10 IGSM01-REFI-3             PIC S9(04)V9(2) COMP-3.         332  004
           10 IGSM01-TMBU               PIC S9(04)V9(2) COMP-3.         336  004
           10 IGSM01-TMBU-1             PIC S9(04)V9(2) COMP-3.         340  004
           10 IGSM01-TMBU-2             PIC S9(03)V9(2) COMP-3.         344  003
           10 IGSM01-TMBU-3             PIC S9(04)V9(2) COMP-3.         347  004
            05 FILLER                      PIC X(162).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGSM01-LONG           PIC S9(4)   COMP  VALUE +350.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGSM01-LONG           PIC S9(4) COMP-5  VALUE +350.           
                                                                                
      *}                                                                        
