      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MUTLM MUTATIONS A TRAITER - LM7        *        
      *----------------------------------------------------------------*        
       01  RVMUTLM .                                                            
           05  MUTLM-CTABLEG2    PIC X(15).                                     
           05  MUTLM-CTABLEG2-REDEF REDEFINES MUTLM-CTABLEG2.                   
               10  MUTLM-NSOCDEP         PIC X(06).                             
               10  MUTLM-NSOCLIE         PIC X(06).                             
               10  MUTLM-NSEQ            PIC X(02).                             
           05  MUTLM-WTABLEG     PIC X(80).                                     
           05  MUTLM-WTABLEG-REDEF  REDEFINES MUTLM-WTABLEG.                    
               10  MUTLM-CSELART         PIC X(05).                             
               10  MUTLM-HCHARGT         PIC X(05).                             
               10  MUTLM-WFLAG           PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVMUTLM-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MUTLM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MUTLM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MUTLM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MUTLM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
