      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      * MODIF AP DSA005 - LE 17/02/99                                           
      * SUITE A PB NB POSTE DANS SOUS-TABLES LITRT, TPRET ET IE60I              
      * ==> JE REDUIT LE NBRE DE CAR DE LA ZONE LIBELLE DE POSTE2               
      * ON PASE DE 110 A 170 POSTES                                             
      **************************************************************    00040000
      *                                                                 00050000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00060000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00070000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00080000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00090000
      *                                                                 00100000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +8192    00110000
      * COMPRENANT :                                                    00120000
      * 1 - LES ZONES RESERVEES A AIDA                                  00130000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00140000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00150000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00160000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00170000
      *                                                                 00180000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00190000
      * PAR AIDA                                                        00200000
      *                                                                 00210000
      *-------------------------------------------------------------    00220000
      *                                                                 00230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GS40-LONG-COMMAREA PIC S9(4) COMP VALUE +8192.           00240001
      *--                                                                       
       01  COM-GS40-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +8192.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00730000
      ************************************************************************  
      *                                                                      *  
      *               COMMAREA MENU CONSULTATION ET EDITION                  *  
      *                     DES MOUVEMENTS DE STOCKS                         *  
      *                                                                      *  
      *  MENU GENERAL    : GS40                                              *  
      *  TRANSACTION     : GS40                                              *  
      *  PROGRAMME       : TGS40                                             *  
      *                                                                      *  
      *  LONGUEUR DE LA ZONE APPLICATIVE RESERVEE : 3724                     *  
      *                                                                      *  
      ************************************************************************  
      *                                                                      *  
      ****              DONNEES COMMUNES AU PROGRAMME TGS40 ET TGS41      ****  
          02 COMM-GS40-SSTAB.                                                   
      *      03 COMM-GS40-ENR-SSTAB     OCCURS 250.                             
             03 COMM-GS40-ENR-SSTAB     OCCURS 300.                             
                05 COMM-GS40-CODE-TAB          PIC X(1).                        
                05 COMM-GS40-ZONE1             PIC X(11).                       
                05 COMM-GS40-ZONE1-IE60I  REDEFINES COMM-GS40-ZONE1.            
                   07 COMM-GS40-ZONE1I-NSOC    PIC X(3).                        
                   07 COMM-GS40-ZONE1I-NDEPOT  PIC X(3).                        
                   07 COMM-GS40-ZONE1I-NSSLIEU PIC X(3).                        
                   07 FILLER                   PIC X(2).                        
                05 COMM-GS40-ZONE1-LITRT  REDEFINES COMM-GS40-ZONE1.            
                   07 COMM-GS40-ZONE1L-NSOCDEP PIC X(6).                        
                   07 COMM-GS40-ZONE1L-CLITRT  PIC X(5).                        
                05 COMM-GS40-ZONE1-TPRET  REDEFINES COMM-GS40-ZONE1.            
                   07 COMM-GS40-ZONE1P-NSOC    PIC X(3).                        
                   07 COMM-GS40-ZONE1P-TYPPRET PIC X(1).                        
                   07 FILLER                   PIC X(7).                        
                05 COMM-GS40-ZONE2             PIC X(13).                       
                05 COMM-GS40-ZONE2-IE60I  REDEFINES COMM-GS40-ZONE2.            
                   07 COMM-GS40-ZONE2I-CLIEUTRT PIC X(5).                       
                   07 COMM-GS40-ZONE2I-WREMONT  PIC X(1).                       
                   07 FILLER                    PIC X(07).                      
                05 COMM-GS40-ZONE2-LIPRET REDEFINES COMM-GS40-ZONE2.            
                   07 COMM-GS40-ZONE2LP-LIB     PIC X(10).                      
                   07 COMM-GS40-ZONE2LP-WASSOC  PIC X(3).                       
          02 COMM-GS40-APPLI.                                                   
             03 COMM-GS40-NCODIC                 PIC X(7).                      
             03 COMM-GS40-NBENR-SSTAB            PIC 999.                       
             03 COMM-GS40-LREFFOURN              PIC X(20).                     
             03 COMM-GS40-CMARQ                  PIC X(5).                      
             03 COMM-GS40-CFAM                   PIC X(5).                      
             03 COMM-GS40-DATEDU                 PIC X(8).                      
             03 COMM-GS40-DATEAU                 PIC X(8).                      
             03 COMM-GS40-ORIGDEST1              PIC X(3).                      
             03 COMM-GS40-ORIGDEST2              PIC X(3).                      
             03 COMM-GS40-ORIGDEST3              PIC X(3).                      
             03 COMM-GS40-ORIGDEST4              PIC X(5).                      
             03 COMM-GS40-ORIG1                  PIC X(3).                      
             03 COMM-GS40-ORIG2                  PIC X(3).                      
             03 COMM-GS40-ORIG3                  PIC X(3).                      
             03 COMM-GS40-ORIG4                  PIC X(5).                      
             03 COMM-GS40-DEST1                  PIC X(3).                      
             03 COMM-GS40-DEST2                  PIC X(3).                      
             03 COMM-GS40-DEST3                  PIC X(3).                      
             03 COMM-GS40-DEST4                  PIC X(5).                      
             03 COMM-GS40-UTIL                   PIC X(5).                      
             03 COMM-GS40-DINVENTAIRE            PIC X(8).                      
             03 COMM-GS40-OCCULT                 PIC X(1).                      
             03 COMM-GS40-CTYPLIEU               PIC X(1).                      
             03 COMM-GS40-WREMONT                PIC X(1).                      
             03 COMM-GS40-LFAM                   PIC X(20).                     
             03 COMM-GS40-LMARQ                  PIC X(20).                     
             03 COMM-GS40-CASSOC                 PIC X(3).                      
             03 COMM-GS40-NBCASSOC               PIC 999.                       
             03 COMM-GS40-REINIT-STK             PIC X(1).                      
             03 COMM-GS40-DATE-FLAG2             PIC X(8).                      
             03 COMM-GS40-SAISIE-DDEBUT          PIC X(1).                      
             03 COMM-GS40-CAS-TRAITE             PIC X(1).                      
      *      03 COMM-GS40-FILLER                 PIC X(2589).                   
      *                                                                      *  
      ****              DONNEES NECESSAIRES A TGS41                       ****  
             03 COMM-GS40-GS41                   PIC X(60).                     
             03 COMM-GS41-APPLI REDEFINES COMM-GS40-GS41.                       
      *                                                                      *  
      ****              DONNEES NECESSAIRES A LA PAGINATION               ****  
                05 COMM-GS41-NPAGE               PIC 9(3).                      
                05 COMM-GS41-NPAGE-MAX           PIC 9(3).                      
      *                                                                      *  
      ****              DONNEES DE L'ECRAN                                ****  
                05 COMM-GS41-LMARQ               PIC X(20).                     
                05 COMM-GS41-LFAM                PIC X(20).                     
                05 COMM-GS41-OCCULT-STKINIT      PIC X(1).                      
                05 COMM-GS41-REINIT              PIC X(1).                      
                05 COMM-GS41-DINV-NULLE          PIC X(1).                      
             05 COMM-GS41-FILLER                 PIC X(11).                     
                                                                                
