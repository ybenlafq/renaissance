      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVCD3500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCD3500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVCD3500.                                                            
           02  CD35-NCODIC                                                      
               PIC X(0007).                                                     
           02  CD35-NMAG                                                        
               PIC X(0003).                                                     
           02  CD35-DMAJ                                                        
               PIC X(0008).                                                     
           02  CD35-QSTOCKMAXANC                                                
               PIC X(0005).                                                     
           02  CD35-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVCD3500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVCD3500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD35-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD35-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD35-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD35-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD35-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD35-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD35-QSTOCKMAXANC-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD35-QSTOCKMAXANC-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD35-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD35-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
