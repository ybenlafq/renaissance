      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM1600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM1600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1600.                                                            
           02  RM16-NTABLJOUR                                                   
               PIC X(0002).                                                     
           02  RM16-QPOIDS1                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM16-QPOIDS2                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM16-QPOIDS3                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM16-QPOIDS4                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM16-QPOIDS5                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM16-QPOIDS6                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM16-QPOIDS7                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM16-FLGENT                                                      
               PIC X(0001).                                                     
           02  RM16-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM1600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM16-NTABLJOUR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM16-NTABLJOUR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM16-QPOIDS1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM16-QPOIDS1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM16-QPOIDS2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM16-QPOIDS2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM16-QPOIDS3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM16-QPOIDS3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM16-QPOIDS4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM16-QPOIDS4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM16-QPOIDS5-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM16-QPOIDS5-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM16-QPOIDS6-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM16-QPOIDS6-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM16-QPOIDS7-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM16-QPOIDS7-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM16-FLGENT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM16-FLGENT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM16-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM16-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
