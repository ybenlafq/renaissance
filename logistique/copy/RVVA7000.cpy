      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVVA7000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVA7000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVVA7000.                                                            
           02  VA70-NSOCCOMPT                                                   
               PIC X(0003).                                                     
           02  VA70-NCODIC                                                      
               PIC X(0007).                                                     
           02  VA70-DOPER                                                       
               PIC X(0008).                                                     
           02  VA70-NORIGINE                                                    
               PIC X(0007).                                                     
           02  VA70-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  VA70-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  VA70-PRA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VA70-QTEREC                                                      
               PIC S9(5) COMP-3.                                                
           02  VA70-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVVA7000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVVA7000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA70-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA70-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA70-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA70-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA70-DOPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA70-DOPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA70-NORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA70-NORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA70-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA70-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA70-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA70-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA70-PRA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA70-PRA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA70-QTEREC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA70-QTEREC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA70-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA70-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
