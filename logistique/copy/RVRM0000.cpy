      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM0000.                                                            
           02  RM00-CGROUP                                                      
               PIC X(0005).                                                     
           02  RM00-LGROUP                                                      
               PIC X(0020).                                                     
           02  RM00-QSEUILSEGMENT                                               
               PIC S9(2) COMP-3.                                                
           02  RM00-QCSTLISSAGE                                                 
               PIC S9(1)V9(0002) COMP-3.                                        
           02  RM00-QNBSEM                                                      
               PIC S9(3) COMP-3.                                                
           02  RM00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM00-CGROUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM00-CGROUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM00-LGROUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM00-LGROUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM00-QSEUILSEGMENT-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM00-QSEUILSEGMENT-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM00-QCSTLISSAGE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM00-QCSTLISSAGE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM00-QNBSEM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM00-QNBSEM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
