      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *              COMMAREA TRANSACTION ZQ40                     *            
      *          PARAMETRAGE DE LA LIVRAISON MONOEQUIPAGE          *            
      **************************************************************            
      *                                                                         
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :        *            
      * 1 - LES ZONES RESERVEES A AIDA                       100 C *            
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS         20 C *            
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT      100 C *            
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP           152 C *            
      * 5 - LES ZONES RESERVEES APPLICATIVES                3724 C *            
      *                                                                         
       01  Z-COMMAREA.                                                          
      *    ZONES RESERVEES A AIDA                              100              
           02 FILLER-COM-AIDA                     PIC X(100).                   
      *    ZONES RESERVEES EN PROVENANCE DE CICS                20              
           02 COMM-CICS-APPLID                    PIC X(8).                     
           02 COMM-CICS-NETNAM                    PIC X(8).                     
           02 COMM-CICS-TRANSA                    PIC X(4).                     
      *    ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION 100              
           02 COMM-DATE-SIECLE                    PIC XX.                       
           02 COMM-DATE-ANNEE                     PIC XX.                       
           02 COMM-DATE-MOIS                      PIC XX.                       
           02 COMM-DATE-JOUR                      PIC XX.                       
           02 COMM-DATE-QNTA                      PIC 9(03).                    
           02 COMM-DATE-QNT0                      PIC 9(05).                    
           02 COMM-DATE-BISX                      PIC 9(01).                    
           02 COMM-DATE-JSM                       PIC 9(01).                    
           02 COMM-DATE-JSM-LC                    PIC X(03).                    
           02 COMM-DATE-JSM-LL                    PIC X(08).                    
           02 COMM-DATE-MOIS-LC                   PIC X(03).                    
           02 COMM-DATE-MOIS-LL                   PIC X(08).                    
           02 COMM-DATE-SSAAMMJJ                  PIC X(8).                     
           02 COMM-DATE-AAMMJJ                    PIC X(6).                     
           02 COMM-DATE-JJMMSSAA                  PIC X(8).                     
           02 COMM-DATE-JJMMAA                    PIC X(6).                     
           02 COMM-DATE-JJ-MM-AA                  PIC X(8).                     
           02 COMM-DATE-JJ-MM-SSAA                PIC X(10).                    
           02 COMM-DATE-WEEK.                                           00690020
              03 COMM-DATE-SEMSS                  PIC 99.               00690030
              03 COMM-DATE-SEMAA                  PIC 99.               00690040
              03 COMM-DATE-SEMNU                  PIC 99.               00690050
           02 COMM-DATE-FILLER                    PIC X(08).                    
      *                                                                         
      *    ZONES RESERVEES TRAITEMENT DU SWAP                  152              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS                    PIC S9(4) COMP VALUE -1.        
      *--                                                                       
           02 COMM-SWAP-CURS                    PIC S9(4) COMP-5 VALUE          
                                                                     -1.        
      *}                                                                        
           02 COMM-SWAP-ATTR         OCCURS 150 PIC X(1).                       
      *                                                                         
      * ZONES RESERVEES APPLICATIVES                          3724              
      *=> ZONES DE SAUVEGARDE COMMUNES AU MENU PRINCIPAL                        
      *=> ET AUX TRANSACTIONS DE NIVEAU INFERIEUR                               
      *==> COMM-ZQ40-APPLI   =  23 + FILLER 477                500              
           02 COMM-ZQ40.                                                        
              03 COMM-ZQ40-APPLI.                                               
                 04 COMM-ZQ40-MZONCMD             PIC X(15).                    
                 04 COMM-ZQ40-NSOC                PIC X(03).                    
                 04 COMM-ZQ40-NLIEU               PIC X(03).                    
                 04 COMM-ZQ40-CFAM                PIC X(05).                    
                 04 COMM-ZQ40-NCODIC              PIC X(07).                    
AD01             04 COMM-ZQ40-CMARK               PIC X(05).                    
AD01             04 COMM-ZQ40-CVMARK              PIC X(05).                    
AD01             04 COMM-ZQ40-CICS-NSOC           PIC X(03).                    
AD01             04 COMM-ZQ40-CICS-NLIEU          PIC X(03).                    
AD01  *          04 COMM-ZQ40-FILLER              PIC X(477).                   
AD01             04 COMM-ZQ40-FILLER              PIC X(461).                   
      *===>   ZONES DE SAUVEGARDE DES NIVEAUX INFERIEURS      3224              
              03 COMM-ZQ4X-APPLI                  PIC X(3224).                  
      *==> ZONES APPLICATIVES GA00                            3724              
           02 COMM-GA00-APPLI REDEFINES COMM-ZQ40 PIC X(3724).                  
                                                                                
