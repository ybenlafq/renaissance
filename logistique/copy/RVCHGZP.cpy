      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CHGZP CHANGEMENT MAG ZP PRESTATION     *        
      *----------------------------------------------------------------*        
       01  RVCHGZP.                                                             
           05  CHGZP-CTABLEG2    PIC X(15).                                     
           05  CHGZP-CTABLEG2-REDEF REDEFINES CHGZP-CTABLEG2.                   
               10  CHGZP-NSOCIETE        PIC X(03).                             
               10  CHGZP-NSOCIETE-N     REDEFINES CHGZP-NSOCIETE                
                                         PIC 9(03).                             
               10  CHGZP-NLIEU           PIC X(03).                             
               10  CHGZP-NLIEU-N        REDEFINES CHGZP-NLIEU                   
                                         PIC 9(03).                             
               10  CHGZP-ANCZP           PIC X(02).                             
               10  CHGZP-ANCZP-N        REDEFINES CHGZP-ANCZP                   
                                         PIC 9(02).                             
               10  CHGZP-NOUVZP          PIC X(02).                             
               10  CHGZP-NOUVZP-N       REDEFINES CHGZP-NOUVZP                  
                                         PIC 9(02).                             
           05  CHGZP-WTABLEG     PIC X(80).                                     
           05  CHGZP-WTABLEG-REDEF  REDEFINES CHGZP-WTABLEG.                    
               10  CHGZP-DATEMAJ         PIC X(08).                             
               10  CHGZP-DATEMAJ-N      REDEFINES CHGZP-DATEMAJ                 
                                         PIC 9(08).                             
               10  CHGZP-COMMENT         PIC X(40).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCHGZP-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CHGZP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CHGZP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CHGZP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CHGZP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
