      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPF22   EPF22                                              00000020
      ***************************************************************** 00000030
       01   EPF22I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGEI  PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCENTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSOCENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCENTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCENTI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUENTL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLIEUENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUENTF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIEUENTI      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBENTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLIBENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBENTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLIBENTI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCPFL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSOCPFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSOCPFF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSOCPFI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUPFL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLIEUPFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIEUPFF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIEUPFI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBPFL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLIBPFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBPFF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLIBPFI   PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDESTOCL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDDESTOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDDESTOCF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDDESTOCI      PIC X(10).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTACTL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNMUTACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMUTACTF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNMUTACTI      PIC X(7).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELACTL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSELACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSELACTF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSELACTI  PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMUTACTL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MDMUTACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDMUTACTF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDMUTACTI      PIC X(10).                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVOLACTL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MVOLACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MVOLACTF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MVOLACTI  PIC X(6).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPACTL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNBPACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPACTF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNBPACTI  PIC X(5).                                       00000690
           02 MLIGNEI OCCURS   9 TIMES .                                00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCHARL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDCHARL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDCHARF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDCHARI      PIC X(8).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHCHARL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MHCHARL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MHCHARF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MHCHARI      PIC X(6).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MNMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMUTF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNMUTI  PIC X(7).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVINITL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQVINITL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQVINITF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQVINITI     PIC X(6).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVPRISL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQVPRISL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQVPRISF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQVPRISI     PIC X(6).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPINITL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQPINITL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQPINITF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQPINITI     PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPPRISL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MQPPRISL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQPPRISF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQPPRISI     PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELARTL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MSELARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELARTF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MSELARTI     PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MSELI   PIC X.                                          00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(78).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * SDF: EPF22   EPF22                                              00001280
      ***************************************************************** 00001290
       01   EPF22O REDEFINES EPF22I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNPAGEA   PIC X.                                          00001470
           02 MNPAGEC   PIC X.                                          00001480
           02 MNPAGEP   PIC X.                                          00001490
           02 MNPAGEH   PIC X.                                          00001500
           02 MNPAGEV   PIC X.                                          00001510
           02 MNPAGEO   PIC ZZZ9.                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNBPAGEA  PIC X.                                          00001540
           02 MNBPAGEC  PIC X.                                          00001550
           02 MNBPAGEP  PIC X.                                          00001560
           02 MNBPAGEH  PIC X.                                          00001570
           02 MNBPAGEV  PIC X.                                          00001580
           02 MNBPAGEO  PIC X(4).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MSOCENTA  PIC X.                                          00001610
           02 MSOCENTC  PIC X.                                          00001620
           02 MSOCENTP  PIC X.                                          00001630
           02 MSOCENTH  PIC X.                                          00001640
           02 MSOCENTV  PIC X.                                          00001650
           02 MSOCENTO  PIC X(3).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLIEUENTA      PIC X.                                     00001680
           02 MLIEUENTC PIC X.                                          00001690
           02 MLIEUENTP PIC X.                                          00001700
           02 MLIEUENTH PIC X.                                          00001710
           02 MLIEUENTV PIC X.                                          00001720
           02 MLIEUENTO      PIC X(3).                                  00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MLIBENTA  PIC X.                                          00001750
           02 MLIBENTC  PIC X.                                          00001760
           02 MLIBENTP  PIC X.                                          00001770
           02 MLIBENTH  PIC X.                                          00001780
           02 MLIBENTV  PIC X.                                          00001790
           02 MLIBENTO  PIC X(20).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MSOCPFA   PIC X.                                          00001820
           02 MSOCPFC   PIC X.                                          00001830
           02 MSOCPFP   PIC X.                                          00001840
           02 MSOCPFH   PIC X.                                          00001850
           02 MSOCPFV   PIC X.                                          00001860
           02 MSOCPFO   PIC X(3).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MLIEUPFA  PIC X.                                          00001890
           02 MLIEUPFC  PIC X.                                          00001900
           02 MLIEUPFP  PIC X.                                          00001910
           02 MLIEUPFH  PIC X.                                          00001920
           02 MLIEUPFV  PIC X.                                          00001930
           02 MLIEUPFO  PIC X(3).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MLIBPFA   PIC X.                                          00001960
           02 MLIBPFC   PIC X.                                          00001970
           02 MLIBPFP   PIC X.                                          00001980
           02 MLIBPFH   PIC X.                                          00001990
           02 MLIBPFV   PIC X.                                          00002000
           02 MLIBPFO   PIC X(20).                                      00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MDDESTOCA      PIC X.                                     00002030
           02 MDDESTOCC PIC X.                                          00002040
           02 MDDESTOCP PIC X.                                          00002050
           02 MDDESTOCH PIC X.                                          00002060
           02 MDDESTOCV PIC X.                                          00002070
           02 MDDESTOCO      PIC X(10).                                 00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNMUTACTA      PIC X.                                     00002100
           02 MNMUTACTC PIC X.                                          00002110
           02 MNMUTACTP PIC X.                                          00002120
           02 MNMUTACTH PIC X.                                          00002130
           02 MNMUTACTV PIC X.                                          00002140
           02 MNMUTACTO      PIC X(7).                                  00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MSELACTA  PIC X.                                          00002170
           02 MSELACTC  PIC X.                                          00002180
           02 MSELACTP  PIC X.                                          00002190
           02 MSELACTH  PIC X.                                          00002200
           02 MSELACTV  PIC X.                                          00002210
           02 MSELACTO  PIC X(5).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MDMUTACTA      PIC X.                                     00002240
           02 MDMUTACTC PIC X.                                          00002250
           02 MDMUTACTP PIC X.                                          00002260
           02 MDMUTACTH PIC X.                                          00002270
           02 MDMUTACTV PIC X.                                          00002280
           02 MDMUTACTO      PIC X(10).                                 00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MVOLACTA  PIC X.                                          00002310
           02 MVOLACTC  PIC X.                                          00002320
           02 MVOLACTP  PIC X.                                          00002330
           02 MVOLACTH  PIC X.                                          00002340
           02 MVOLACTV  PIC X.                                          00002350
           02 MVOLACTO  PIC X(6).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MNBPACTA  PIC X.                                          00002380
           02 MNBPACTC  PIC X.                                          00002390
           02 MNBPACTP  PIC X.                                          00002400
           02 MNBPACTH  PIC X.                                          00002410
           02 MNBPACTV  PIC X.                                          00002420
           02 MNBPACTO  PIC X(5).                                       00002430
           02 MLIGNEO OCCURS   9 TIMES .                                00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MDCHARA      PIC X.                                     00002460
             03 MDCHARC PIC X.                                          00002470
             03 MDCHARP PIC X.                                          00002480
             03 MDCHARH PIC X.                                          00002490
             03 MDCHARV PIC X.                                          00002500
             03 MDCHARO      PIC X(8).                                  00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MHCHARA      PIC X.                                     00002530
             03 MHCHARC PIC X.                                          00002540
             03 MHCHARP PIC X.                                          00002550
             03 MHCHARH PIC X.                                          00002560
             03 MHCHARV PIC X.                                          00002570
             03 MHCHARO      PIC X(6).                                  00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MNMUTA  PIC X.                                          00002600
             03 MNMUTC  PIC X.                                          00002610
             03 MNMUTP  PIC X.                                          00002620
             03 MNMUTH  PIC X.                                          00002630
             03 MNMUTV  PIC X.                                          00002640
             03 MNMUTO  PIC X(7).                                       00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MQVINITA     PIC X.                                     00002670
             03 MQVINITC     PIC X.                                     00002680
             03 MQVINITP     PIC X.                                     00002690
             03 MQVINITH     PIC X.                                     00002700
             03 MQVINITV     PIC X.                                     00002710
             03 MQVINITO     PIC X(6).                                  00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MQVPRISA     PIC X.                                     00002740
             03 MQVPRISC     PIC X.                                     00002750
             03 MQVPRISP     PIC X.                                     00002760
             03 MQVPRISH     PIC X.                                     00002770
             03 MQVPRISV     PIC X.                                     00002780
             03 MQVPRISO     PIC X(6).                                  00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MQPINITA     PIC X.                                     00002810
             03 MQPINITC     PIC X.                                     00002820
             03 MQPINITP     PIC X.                                     00002830
             03 MQPINITH     PIC X.                                     00002840
             03 MQPINITV     PIC X.                                     00002850
             03 MQPINITO     PIC X(5).                                  00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MQPPRISA     PIC X.                                     00002880
             03 MQPPRISC     PIC X.                                     00002890
             03 MQPPRISP     PIC X.                                     00002900
             03 MQPPRISH     PIC X.                                     00002910
             03 MQPPRISV     PIC X.                                     00002920
             03 MQPPRISO     PIC X(5).                                  00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MSELARTA     PIC X.                                     00002950
             03 MSELARTC     PIC X.                                     00002960
             03 MSELARTP     PIC X.                                     00002970
             03 MSELARTH     PIC X.                                     00002980
             03 MSELARTV     PIC X.                                     00002990
             03 MSELARTO     PIC X(5).                                  00003000
             03 FILLER       PIC X(2).                                  00003010
             03 MSELA   PIC X.                                          00003020
             03 MSELC   PIC X.                                          00003030
             03 MSELP   PIC X.                                          00003040
             03 MSELH   PIC X.                                          00003050
             03 MSELV   PIC X.                                          00003060
             03 MSELO   PIC X.                                          00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(78).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
