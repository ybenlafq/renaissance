      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM21                                          *  00020000
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 58.              00080001
       01  TS-DONNEES.                                                  00090000
              10 TS-CFAM         PIC X(5).                              00120000
              10 TS-LFAM         PIC X(20).                             00120101
              10 TS-LAGREG       PIC X(20).                             00120202
              10 TS-DSEM         PIC 9(6).                              00121000
              10 TS-QPM          PIC X(5).                              00121100
              10 TS-INSERT       PIC X.                                 00123000
              10 TS-MODIF        PIC X.                                 00124000
                                                                                
