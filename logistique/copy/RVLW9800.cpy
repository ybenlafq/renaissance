      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLW9800                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLW9800                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW9800.                                                            
           02  LW98-CTYPMES                                                     
               PIC X(0003).                                                     
           02  LW98-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  LW98-NLIEU                                                       
               PIC X(0003).                                                     
           02  LW98-DRECEPT                                                     
               PIC X(0026).                                                     
           02  LW98-NCLETECH                                                    
               PIC X(0010).                                                     
           02  LW98-NCLEFONC                                                    
               PIC X(0050).                                                     
           02  LW98-WTRAIT                                                      
               PIC X(0002).                                                     
           02  LW98-LTRAIT                                                      
               PIC X(0020).                                                     
           02  LW98-DTRAIT                                                      
               PIC X(0008).                                                     
           02  LW98-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLW9800                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW9800-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW98-CTYPMES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW98-CTYPMES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW98-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW98-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW98-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW98-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW98-DRECEPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW98-DRECEPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW98-NCLETECH-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW98-NCLETECH-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW98-NCLEFONC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW98-NCLEFONC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW98-WTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW98-WTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW98-LTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW98-LTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW98-DTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW98-DTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW98-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW98-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
