      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  COMMGD30.                                                            
           05  COMM-GD30-NSOCDEPOT        PIC X(3).                             
           05  COMM-GD30-NDEPOT           PIC X(3).                             
           05  COMM-GD30-DRAFALE          PIC X(8).                             
           05  COMM-GD30-NRAFALE          PIC X(3).                             
           05  COMM-GD30-WCOMMUT          PIC X(1).                             
           05  COMM-GD30-CODRET           PIC S9(5)  COMP-3.                    
               88 CODE-RETOUR-GD30-NORMAL            VALUE 0  THRU 1.           
           05  COMM-GD30-TABLES           PIC X(8).                             
           05  COMM-GD30-FONCTION         PIC X(8).                             
           05  FILLER                     PIC X(35).                            
                                                                                
