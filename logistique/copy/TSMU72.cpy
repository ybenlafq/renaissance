      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *       TS SPECIFIQUE  OPTIMISATION TRANSPORT TMU72            *          
      ****************************************************************          
       01  TS-MU72-LONG             PIC S9(4) COMP-3 VALUE +42.                 
       01  TS-MU72-DATA.                                                        
      *************************************** LONGUEUR LIGNE = 45               
         05  TS-MU72-LIGNE.                                                     
           10 TS-MU72-DMUT        PIC X(8).                                     
           10 TS-MU72-NMUT        PIC X(7).                                     
           10 TS-MU72-SELART      PIC X(5).                                     
           10 TS-MU72-QNBM3QUOTA  PIC S9(3)V99    COMP-3.                       
           10 TS-MU72-QNBM3DISPO  PIC S9(3)V99    COMP-3.                       
           10 TS-MU72-QNBM3PRIS   PIC S9(3)V99    COMP-3.                       
           10 TS-MU72-QNBPQUOTA   PIC S9(5)       COMP-3.                       
           10 TS-MU72-QNBPDISPO   PIC S9(5)       COMP-3.                       
           10 TS-MU72-QNBPPRIS    PIC S9(5)       COMP-3.                       
           10 TS-MU72-ACTION      PIC X(1).                                     
                                                                                
