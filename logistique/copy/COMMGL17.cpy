      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGL17                             *            
      *       TR : GL00  GESTION DES LIVRAISON                     *            
      *       PG : TGL17 PROGRAMMATION DES LIVRAISONS              *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                                         
      *                                                                         
          02 COMM-GL17-APPLI REDEFINES COMM-GL00-APPLI.                         
             03 COMM-GL17-ALIGNEMENT     PIC X(207).                            
      *------------------------------ CODE FONCTION 207                         
             03 COMM-GL17-BASE.                                                 
                04 COMM-GL17-WFONC          PIC X(3).                           
      *---------                                                                
                04 COMM-GL17-NCDE           PIC X(7).                           
      *---------                                                                
                04 COMM-GL17-NENTCDE        PIC X(5).                           
      *---------                                                                
                04 COMM-GL17-NSOCLIVR       PIC X(3).                           
      *---------                                                                
                04 COMM-GL17-NDEPOT         PIC X(3).                           
      *------------------------------ PARTIE COMMUNE               243  00060004
             03 COMM-GL17-PAGE           PIC S9(003).                   00070004
             03 COMM-GL17-PAGE-MAX       PIC S9(003).                   00080004
             03 COMM-GL17-COLS           PIC S9(03).                    00090004
             03 COMM-GL17-CREC           PIC X(05).                     00090004
             03 COMM-GL17-LREC           PIC X(20).                     00090004
      *--                                                          252  00100004
             03  VERTICAL-88 PIC X.                                     00110004
               88  PAS-VERTICAL VALUE '0'.                              00120004
               88  VERTICAL     VALUE '1'.                              00130004
      *--                                                               00140004
             03  HORIZONTAL-88 PIC X.                                   00150004
               88  PAS-HORIZONTAL VALUE '0'.                            00160004
               88  HORIZONTAL     VALUE '1'.                            00170004
      *------                                                     254   00180004
             03 COMM-GL17-RANG-TS-HV.                                   00190004
               04 COMM-GL17-RANG-TS      PIC S9(4).                     00200007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GL17-I-RELIQUAT     PIC S9(4) COMP.                00210004
      *--                                                                       
             03 COMM-GL17-I-RELIQUAT     PIC S9(4) COMP-5.                      
      *}                                                                        
             03 COMM-GL17-1ERE-LIGNE     PIC S9(05).                    00220004
             03 COMM-GL17-IL-MAX         PIC S9(05).                    00230004
      *------LL = 680C.                                          270    00240004
             03 COMM-GL17-DLIVRAISONS.                                  00250004
      *------                                                           00260004
               04 COMM-GL17-POSTE OCCURS 20 TIMES.                      00270005
      *------                                    100                    00280004
                 05 COMM-GL17-DLIVRAISON.                               00290005
                  06 SS                                 PIC X(02).      00300005
                  06 AA                                 PIC X(02).      00310005
                  06 MM                                 PIC X(02).      00320005
                  06 JJ                                 PIC X(02).      00330005
                 05 COMM-GL17-SSAAR  REDEFINES   COMM-GL17-DLIVRAISON.  00340005
                  06 COMM-GL17-SSAA                     PIC X(04).      00350005
                  06 COMM-GL17-MMAA                     PIC X(02).      00360005
                  06 COMM-GL17-JJAA                     PIC X(02).      00370005
      *------                                                           00380004
                 05 COMM-GL17-ANNEE-SEMAINE.                            00390005
                  06 COMM-GL17-SSANNEE          PIC X(04).              00400005
                  06 COMM-GL17-DSEMAINE         PIC X(02).              00410005
                  06 COMM-GL17-JOUR             PIC X(01).              00420005
      *------                                                                   
             03 COMM-GL17-REASONCODE-FLAG       PIC X(01).                      
      *------                                                   1270    00560004
             03 COMM-GL17-REASONCODE    OCCURS 20.                              
               04 COMM-GL17-AFFECT-CREAS        PIC X(01).                      
               04 COMM-GL17-CREASON             PIC X(5).                       
      *------                                                                   
             03 COMM-GL17-0-DLIVRAISONS.                                00570004
      *------                                                           00580004
               04 COMM-GL17-0-POSTE OCCURS 20 TIMES.                    00590005
      *------                                                           00600004
                 05 COMM-GL17-0-DLIVRAISON.                             00610005
                  06 SS                                 PIC X(02).      00620005
                  06 AA                                 PIC X(02).      00630005
                  06 MM                                 PIC X(02).      00640005
                  06 JJ                                 PIC X(02).      00650005
                 05 COMM-GL17-0-SSAAR REDEFINES                                 
                              COMM-GL17-0-DLIVRAISON.                           
                  06 COMM-GL17-0-SSAA                   PIC X(04).      00670005
                  06 COMM-GL17-0-MMAA                   PIC X(02).      00680005
                  06 COMM-GL17-0-JJAA                   PIC X(02).      00690005
      *------                                                           00700004
                 05 COMM-GL17-0-ANNEE-SEMAINE.                          00710005
                  06 COMM-GL17-0-SSANNEE        PIC X(04).              00720005
                  06 COMM-GL17-0-DSEMAINE       PIC X(02).              00730005
                  06 COMM-GL17-0-JOUR           PIC X(01).              00740005
      *------                                                           00700004
             03 COMM-GL17-IND-Q          PIC S9(03) COMP-3.             00230004
             03 COMM-GL17-IND-MAX        PIC S9(03) COMP-3.             00230004
      *                                                                 01050005
             03 COMM-GL17-QUOTA.                                        00570004
                04 COMM-GL17-POSTE-QUOTA OCCURS 20 TIMES.               00590005
                   05 COMM-GL17-CQUOTA PIC X(5).                        00610005
      *-------- SI SURCOMMANDE 'O' SINON 'N' --------------------               
             03 COMM-GL17-SURCDE                PIC X.                          
      *------ FLAG INTERFACAGE NCG / JDA                                        
             03 COMM-GL17-INTRFC             PIC X(1).                          
      *------ TOP MODIFICATION POUR CALCUL DE L'AVENANT                         
             03 COMM-GL17-TOP            PIC X(1).                              
      *------------------------------ LIBRE                  2270       01060005
             03 COMM-GL17-FLGDAT         PIC X(1).                              
             03 COMM-GL17-PARMDAT        PIC 9(10).                             
             03 COMM-GL17-DATEPARAM      PIC X(8).                              
             03 COMM-GL17-XCTRL-WQCOLIRECEPT PIC X.                             
      *---------                                                                
             03 COMM-GL17-ORIGDATE       PIC X(8).                              
             03 COMM-GL17-SATDUEDATE     PIC X(8).                              
             03 COMM-GL17-LIBELLE        PIC X(19).                             
             03 COMM-GL17-CSTATUT              PIC X(3).                        
             03 COMM-GL17-CQUALIF              PIC X(6).                        
      *--- ZONE D'AFFICHAGE VARIABLE                                            
             03 COMM-GL17-LIGNES.                                               
                05 COMM-GL17-LIGNE11           PIC X(24).                       
                05 COMM-GL17-LIGNE11-A         PIC X.                           
                05 COMM-GL17-LIGNE13           PIC X(24).                       
                05 COMM-GL17-LIGNE13-A         PIC X.                           
                05 COMM-GL17-LIGNE15           PIC X(12).                       
                05 COMM-GL17-VALOMANO          PIC X.                           
                05 COMM-GL17-WSAP              PIC X.                           
                05 COMM-GL17-MODIF-SAP         PIC X.                           
                   88 MODIF-SAP-KO             VALUE '0'.                       
                   88 MODIF-SAP                VALUE '1'.                       
      *      03 COMM-GL17-FILLER         PIC X(1180).                           
      *                                                                 01090004
      ***************************************************************** 01100004
                                                                                
