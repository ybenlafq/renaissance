      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM45   D4469  Maquette                                    00000020
      ***************************************************************** 00000030
       01   ERM45I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEMAXL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNPAGEMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNPAGEMAXF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEMAXI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCGROUPI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPOL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCEXPOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXPOF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCEXPOI   PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMPL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAMPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMPF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMPI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREG1L      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MTYPREG1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREG1F      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MTYPREG1I      PIC X(2).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREG2L      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MTYPREG2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREG2F      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTYPREG2I      PIC X(2).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREG3L      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MTYPREG3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREG3F      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MTYPREG3I      PIC X(2).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPREG4L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MTYPREG4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPREG4F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTYPREG4I      PIC X(2).                                  00000490
           02 MLIGNEI OCCURS   13 TIMES .                               00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCFAMI  PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLFAMI  PIC X(20).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTIONL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MACTIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MACTIONF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MACTIONI     PIC X.                                     00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPO1L     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MQEXPO1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQEXPO1F     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQEXPO1I     PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLS1L  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MQLS1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQLS1F  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQLS1I  PIC X(3).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPO2L     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MQEXPO2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQEXPO2F     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQEXPO2I     PIC X(3).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLS2L  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MQLS2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQLS2F  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQLS2I  PIC X(3).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPO3L     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQEXPO3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQEXPO3F     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQEXPO3I     PIC X(3).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLS3L  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MQLS3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQLS3F  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQLS3I  PIC X(3).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPO4L     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQEXPO4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQEXPO4F     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQEXPO4I     PIC X(3).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLS4L  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MQLS4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQLS4F  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQLS4I  PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPF11L   COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLPF11L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPF11F   PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLPF11I   PIC X(13).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(80).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: ERM45   D4469  Maquette                                    00001200
      ***************************************************************** 00001210
       01   ERM45O REDEFINES ERM45I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNPAGEA   PIC X.                                          00001390
           02 MNPAGEC   PIC X.                                          00001400
           02 MNPAGEP   PIC X.                                          00001410
           02 MNPAGEH   PIC X.                                          00001420
           02 MNPAGEV   PIC X.                                          00001430
           02 MNPAGEO   PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNPAGEMAXA     PIC X.                                     00001460
           02 MNPAGEMAXC     PIC X.                                     00001470
           02 MNPAGEMAXP     PIC X.                                     00001480
           02 MNPAGEMAXH     PIC X.                                     00001490
           02 MNPAGEMAXV     PIC X.                                     00001500
           02 MNPAGEMAXO     PIC X(3).                                  00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MCGROUPA  PIC X.                                          00001530
           02 MCGROUPC  PIC X.                                          00001540
           02 MCGROUPP  PIC X.                                          00001550
           02 MCGROUPH  PIC X.                                          00001560
           02 MCGROUPV  PIC X.                                          00001570
           02 MCGROUPO  PIC X(5).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCEXPOA   PIC X.                                          00001600
           02 MCEXPOC   PIC X.                                          00001610
           02 MCEXPOP   PIC X.                                          00001620
           02 MCEXPOH   PIC X.                                          00001630
           02 MCEXPOV   PIC X.                                          00001640
           02 MCEXPOO   PIC X.                                          00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCFAMPA   PIC X.                                          00001670
           02 MCFAMPC   PIC X.                                          00001680
           02 MCFAMPP   PIC X.                                          00001690
           02 MCFAMPH   PIC X.                                          00001700
           02 MCFAMPV   PIC X.                                          00001710
           02 MCFAMPO   PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MTYPREG1A      PIC X.                                     00001740
           02 MTYPREG1C PIC X.                                          00001750
           02 MTYPREG1P PIC X.                                          00001760
           02 MTYPREG1H PIC X.                                          00001770
           02 MTYPREG1V PIC X.                                          00001780
           02 MTYPREG1O      PIC X(2).                                  00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MTYPREG2A      PIC X.                                     00001810
           02 MTYPREG2C PIC X.                                          00001820
           02 MTYPREG2P PIC X.                                          00001830
           02 MTYPREG2H PIC X.                                          00001840
           02 MTYPREG2V PIC X.                                          00001850
           02 MTYPREG2O      PIC X(2).                                  00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MTYPREG3A      PIC X.                                     00001880
           02 MTYPREG3C PIC X.                                          00001890
           02 MTYPREG3P PIC X.                                          00001900
           02 MTYPREG3H PIC X.                                          00001910
           02 MTYPREG3V PIC X.                                          00001920
           02 MTYPREG3O      PIC X(2).                                  00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MTYPREG4A      PIC X.                                     00001950
           02 MTYPREG4C PIC X.                                          00001960
           02 MTYPREG4P PIC X.                                          00001970
           02 MTYPREG4H PIC X.                                          00001980
           02 MTYPREG4V PIC X.                                          00001990
           02 MTYPREG4O      PIC X(2).                                  00002000
           02 MLIGNEO OCCURS   13 TIMES .                               00002010
             03 FILLER       PIC X(2).                                  00002020
             03 MCFAMA  PIC X.                                          00002030
             03 MCFAMC  PIC X.                                          00002040
             03 MCFAMP  PIC X.                                          00002050
             03 MCFAMH  PIC X.                                          00002060
             03 MCFAMV  PIC X.                                          00002070
             03 MCFAMO  PIC X(5).                                       00002080
             03 FILLER       PIC X(2).                                  00002090
             03 MLFAMA  PIC X.                                          00002100
             03 MLFAMC  PIC X.                                          00002110
             03 MLFAMP  PIC X.                                          00002120
             03 MLFAMH  PIC X.                                          00002130
             03 MLFAMV  PIC X.                                          00002140
             03 MLFAMO  PIC X(20).                                      00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MACTIONA     PIC X.                                     00002170
             03 MACTIONC     PIC X.                                     00002180
             03 MACTIONP     PIC X.                                     00002190
             03 MACTIONH     PIC X.                                     00002200
             03 MACTIONV     PIC X.                                     00002210
             03 MACTIONO     PIC X.                                     00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MQEXPO1A     PIC X.                                     00002240
             03 MQEXPO1C     PIC X.                                     00002250
             03 MQEXPO1P     PIC X.                                     00002260
             03 MQEXPO1H     PIC X.                                     00002270
             03 MQEXPO1V     PIC X.                                     00002280
             03 MQEXPO1O     PIC X(3).                                  00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MQLS1A  PIC X.                                          00002310
             03 MQLS1C  PIC X.                                          00002320
             03 MQLS1P  PIC X.                                          00002330
             03 MQLS1H  PIC X.                                          00002340
             03 MQLS1V  PIC X.                                          00002350
             03 MQLS1O  PIC X(3).                                       00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MQEXPO2A     PIC X.                                     00002380
             03 MQEXPO2C     PIC X.                                     00002390
             03 MQEXPO2P     PIC X.                                     00002400
             03 MQEXPO2H     PIC X.                                     00002410
             03 MQEXPO2V     PIC X.                                     00002420
             03 MQEXPO2O     PIC X(3).                                  00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MQLS2A  PIC X.                                          00002450
             03 MQLS2C  PIC X.                                          00002460
             03 MQLS2P  PIC X.                                          00002470
             03 MQLS2H  PIC X.                                          00002480
             03 MQLS2V  PIC X.                                          00002490
             03 MQLS2O  PIC X(3).                                       00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MQEXPO3A     PIC X.                                     00002520
             03 MQEXPO3C     PIC X.                                     00002530
             03 MQEXPO3P     PIC X.                                     00002540
             03 MQEXPO3H     PIC X.                                     00002550
             03 MQEXPO3V     PIC X.                                     00002560
             03 MQEXPO3O     PIC X(3).                                  00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MQLS3A  PIC X.                                          00002590
             03 MQLS3C  PIC X.                                          00002600
             03 MQLS3P  PIC X.                                          00002610
             03 MQLS3H  PIC X.                                          00002620
             03 MQLS3V  PIC X.                                          00002630
             03 MQLS3O  PIC X(3).                                       00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MQEXPO4A     PIC X.                                     00002660
             03 MQEXPO4C     PIC X.                                     00002670
             03 MQEXPO4P     PIC X.                                     00002680
             03 MQEXPO4H     PIC X.                                     00002690
             03 MQEXPO4V     PIC X.                                     00002700
             03 MQEXPO4O     PIC X(3).                                  00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MQLS4A  PIC X.                                          00002730
             03 MQLS4C  PIC X.                                          00002740
             03 MQLS4P  PIC X.                                          00002750
             03 MQLS4H  PIC X.                                          00002760
             03 MQLS4V  PIC X.                                          00002770
             03 MQLS4O  PIC X(3).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MLPF11A   PIC X.                                          00002800
           02 MLPF11C   PIC X.                                          00002810
           02 MLPF11P   PIC X.                                          00002820
           02 MLPF11H   PIC X.                                          00002830
           02 MLPF11V   PIC X.                                          00002840
           02 MLPF11O   PIC X(13).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(80).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
