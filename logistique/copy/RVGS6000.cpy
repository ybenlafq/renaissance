      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGS6000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS6000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS6000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS6000.                                                            
      *}                                                                        
           02  GS60-NLIEUENT                                                    
               PIC X(0003).                                                     
           02  GS60-NHS                                                         
               PIC X(0007).                                                     
           02  GS60-NCODIC                                                      
               PIC X(0007).                                                     
           02  GS60-WCODCART                                                    
               PIC X(0001).                                                     
           02  GS60-WARTINC                                                     
               PIC X(0001).                                                     
           02  GS60-QHS                                                         
               PIC S9(5) COMP-3.                                                
           02  GS60-CMOTIFENT                                                   
               PIC X(0010).                                                     
           02  GS60-LCOMMENTENT                                                 
               PIC X(0020).                                                     
           02  GS60-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  GS60-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  GS60-NSSLIEUORIG                                                 
               PIC X(0003).                                                     
           02  GS60-NSOCTRT                                                     
               PIC X(0003).                                                     
           02  GS60-NLIEUTRT                                                    
               PIC X(0003).                                                     
           02  GS60-NSSLIEUTRT                                                  
               PIC X(0003).                                                     
           02  GS60-CLIEUTRT                                                    
               PIC X(0005).                                                     
           02  GS60-CSTATUTTRT                                                  
               PIC X(0005).                                                     
           02  GS60-LEMPLACT                                                    
               PIC X(0005).                                                     
           02  GS60-NSOCDEST                                                    
               PIC X(0003).                                                     
           02  GS60-NLIEUDEST                                                   
               PIC X(0003).                                                     
           02  GS60-NSSLIEUDEST                                                 
               PIC X(0003).                                                     
           02  GS60-LCOMMENTSOR                                                 
               PIC X(0020).                                                     
           02  GS60-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGS6000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS6000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS6000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NLIEUENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NLIEUENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NHS-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NHS-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-WCODCART-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-WCODCART-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-WARTINC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-WARTINC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-QHS-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-QHS-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-CMOTIFENT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-CMOTIFENT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-LCOMMENTENT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-LCOMMENTENT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NSSLIEUORIG-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NSSLIEUORIG-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NSOCTRT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NSOCTRT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NLIEUTRT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NLIEUTRT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NSSLIEUTRT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NSSLIEUTRT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-CLIEUTRT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-CLIEUTRT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-CSTATUTTRT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-CSTATUTTRT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-LEMPLACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-LEMPLACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NSOCDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NSOCDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NLIEUDEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NLIEUDEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-NSSLIEUDEST-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-NSSLIEUDEST-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-LCOMMENTSOR-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-LCOMMENTSOR-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS60-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS60-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
