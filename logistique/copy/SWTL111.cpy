      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*00000010
      *                                                                *00000020
      *   Gestion des statistiques de livraison                        *00000030
      *                                                                *00000040
      *   D�finition de la structure du fichier FTL111                 *00000050
      *     (Les mouvements de livraison et de retour)                 *00000060
      *                                                                *00000070
      *   Fichier cr�� par extraction des tables GS40 et TL04.         *00000080
      *   S�quentiel ; Longueur 80                                     *00000090
      *                                                                *00000100
      *----------------------------------------------------------------*00000110
      *                                                                *00000120
      * FTL111-NSOC       Code soci�t�                         001/003 *00000130
      *       -NDEPOT     Code du d�p�t                        004/006 *00000140
      *       -DMVT       Date du mouvement (top�)             007/014 *00000150
      *       -PERIM      P�rim�tre de livraison               015/019 *00000160
      *       -CEQUIPE    Equipe de livraison                  020/024 *00000170
      *       -CEQUIP     Type d'�quipage                      025/029 *00000180
      *       -NMAG       Magasin de vente                     030/032 *00000190
      *       -NVENTE     N� de vente                          033/039 *00000200
      *       -DDELIV     Date de livraison                    040/047 *00000210
      *       -CLIVR1     1er code livreur                     048/057 *00000220
      *       -CLIVR2     2�me code livreur                    058/067 *00000230
      *       -CLIVR3     3�me code livreur                    068/077 *00000240
      *       -QEQUIP     Quantit� d'�quipage                  078/080 *00000250
      *       -CRETOUR    Code retour                          081/085 *00000260
      *       -QPIECESDEP Nbr de pi�ces du bon                 086/089 *00000270
      *       -QPIECESRET Nbr de pi�ces retourn�es             090/093 *00000280
      *                   Disponible                           094/120 *00000290
      *                                                                *00000300
      *----------------------------------------------------------------*00000310
       01  FTL111-DSECT.                                                00000320
           05  FTL111-NSOC             PIC  X(03).                      00000330
           05  FTL111-NDEPOT           PIC  X(03).                      00000340
           05  FTL111-DMVT             PIC  X(08).                      00000350
           05  FTL111-PERIM            PIC  X(05).                      00000360
           05  FTL111-CEQUIPE          PIC  X(05).                      00000370
           05  FTL111-CEQUIP           PIC  X(05).                      00000380
           05  FTL111-NMAG             PIC  X(03).                      00000390
           05  FTL111-NVENTE           PIC  X(07).                      00000400
           05  FTL111-DDELIV           PIC  X(08).                      00000410
           05  FTL111-CLIVR1           PIC  X(10).                      00000420
           05  FTL111-CLIVR2           PIC  X(10).                      00000430
           05  FTL111-CLIVR3           PIC  X(10).                      00000440
           05  FTL111-QEQUIP           PIC S9(03)V99   COMP-3.          00000450
           05  FTL111-CRETOUR          PIC  X(05).                      00000460
           05  FTL111-QPIECESDEP       PIC  9(07)      COMP-3.          00000470
           05  FTL111-QPIECESRET       PIC  9(07)      COMP-3.          00000480
           05  FILLER                  PIC  X(27).                      00000490
      *                                                                 00000500
                                                                                
