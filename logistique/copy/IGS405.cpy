      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGS405 AU 03/06/2002  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,05,BI,A,                          *        
      *                           23,07,BI,A,                          *        
      *                           30,10,BI,A,                          *        
      *                           40,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGS405.                                                        
            05 NOMETAT-IGS405           PIC X(6) VALUE 'IGS405'.                
            05 RUPTURES-IGS405.                                                 
           10 IGS405-NSOCDEST           PIC X(03).                      007  003
           10 IGS405-NLIEUDEST          PIC X(03).                      010  003
           10 IGS405-WSEQFAM            PIC 9(05).                      013  005
           10 IGS405-CMARQ              PIC X(05).                      018  005
           10 IGS405-NCODIC             PIC X(07).                      023  007
           10 IGS405-COPER              PIC X(10).                      030  010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGS405-SEQUENCE           PIC S9(04) COMP.                040  002
      *--                                                                       
           10 IGS405-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGS405.                                                   
           10 IGS405-CFAM               PIC X(05).                      042  005
           10 IGS405-CLIEUTRTORIG       PIC X(05).                      047  005
           10 IGS405-CPROG              PIC X(05).                      052  005
           10 IGS405-DMVT               PIC X(08).                      057  008
           10 IGS405-LLIEU              PIC X(20).                      065  020
           10 IGS405-LREFFOURN          PIC X(20).                      085  020
           10 IGS405-NLIEUORIG          PIC X(03).                      105  003
           10 IGS405-NORIGINE           PIC X(07).                      108  007
           10 IGS405-NSOCORIG           PIC X(03).                      115  003
           10 IGS405-NSSLIEUORIG        PIC X(03).                      118  003
           10 IGS405-QMVT               PIC S9(07)      COMP-3.         121  004
            05 FILLER                      PIC X(388).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGS405-LONG           PIC S9(4)   COMP  VALUE +124.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGS405-LONG           PIC S9(4) COMP-5  VALUE +124.           
                                                                                
      *}                                                                        
