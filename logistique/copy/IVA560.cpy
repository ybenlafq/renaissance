      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  DSECT-IVA560.                                                        
            05 RUPTURES-IVA560.                                                 
           10 IVA560-NSOCVALO           PIC X(03).                              
           10 IVA560-CSEQRAYON1         PIC X(02).                              
           10 IVA560-CSEQRAYON2         PIC X(02).                              
           10 IVA560-WSEQFAM            PIC S9(05)      COMP-3.                 
            05 CHAMPS-IVA560.                                                   
           10 IVA560-CRAYON1            PIC X(05).                              
           10 IVA560-LFAM               PIC X(20).                              
           10 IVA560-LRAYON1            PIC X(20).                              
           10 IVA560-LRAYON2            PIC X(20).                              
           10 IVA560-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-PSTOCKENTREE-E     PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-PSTOCKENTREE-M     PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-PSTOCKENTREE-R     PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-PSTOCKRECYCL-E     PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-PSTOCKFINAL        PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-PSTOCKINIT         PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-PSTOCKSORTIE-E     PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-PSTOCKSORTIE-M     PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-PSTOCKSORTIE-R     PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-PSTOCKRECYCL-S     PIC S9(09)V9(6) COMP-3.                 
           10 IVA560-QSTOCKENTREE       PIC S9(11)      COMP-3.                 
           10 IVA560-QSTOCKENTREE-E     PIC S9(11)      COMP-3.                 
           10 IVA560-QSTOCKENTREE-M     PIC S9(11)      COMP-3.                 
           10 IVA560-QSTOCKENTREE-R     PIC S9(11)      COMP-3.                 
           10 IVA560-QSTOCKFINAL        PIC S9(11)      COMP-3.                 
           10 IVA560-QSTOCKINIT         PIC S9(11)      COMP-3.                 
           10 IVA560-QSTOCKSORTIE       PIC S9(11)      COMP-3.                 
           10 IVA560-QSTOCKSORTIE-E     PIC S9(11)      COMP-3.                 
           10 IVA560-QSTOCKSORTIE-M     PIC S9(11)      COMP-3.                 
           10 IVA560-QSTOCKSORTIE-R     PIC S9(11)      COMP-3.                 
             05 FILLER                  PIC X(281).                             
                                                                                
