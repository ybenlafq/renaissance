      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
          02 COMM-GL11-APPLI REDEFINES COMM-GL00-APPLI.                         
             05 COMM-GL11-CODESFONCTION.                                        
                10 COMM-GL11-CODES-FONC01   OCCURS 3.                           
                   15 COMM-GL11-FONC01      PIC X(03).                          
                   15 FILLER                PIC X(01).                          
             05 COMM-GL11-WFONC             PIC X(03).                          
             05 COMM-GL11-CREC              PIC X(05).                          
             05 COMM-GL11-LREC              PIC X(20).                          
             05 COMM-GL11-NLIVRAIS          PIC X(07).                          
             05 COMM-GL11-DJOUR             PIC X(10).                          
             05 COMM-GL11-DJOUR-ORIG        PIC X(10).                          
             05 COMM-GL11-DJOURX            PIC X(08).                          
             05 COMM-GL11-DANNEE-SEM        PIC X(04).                          
             05 COMM-GL11-NSEMAINE          PIC X(02).                          
             05 COMM-GL11-NJOUR             PIC 9(01).                          
             05 COMM-GL11-DHEURE            PIC 9(02).                          
             05 COMM-GL11-DMINUT            PIC 9(02).                          
             05 COMM-GL11-NSOCIETE          PIC X(03).                          
             05 COMM-GL11-NDEPOT            PIC X(03).                          
             05 COMM-GL11-WQJOUR            PIC X(01).                          
             05 COMM-GL11-WPALET            PIC X(01).                          
             05 COMM-GL11-WPALET-ORIG       PIC X(01).                          
             05 COMM-GL11-CLIVR             PIC X(05).                          
             05 COMM-GL11-LLIVR             PIC X(20).                          
             05 COMM-GL11-LCOMM             PIC X(20).                          
             05 COMM-GL11-CTRANS            PIC X(05).                          
             05 COMM-GL11-LTRANS            PIC X(20).                          
             05 COMM-GL11-RECUP-NLIVR       PIC X(01).                          
             05 COMM-GL11-DATAS.                                                
                10 COMM-GL11-WPAGG          PIC 9(03).                          
                10 COMM-GL11-WPAGGMAX       PIC 9(03).                          
                10 COMM-GL11-WPAGD          PIC 9(03).                          
                10 COMM-GL11-WPAGDMAX       PIC 9(03).                          
                10 COMM-GL11-QNBP           PIC 9(05).                          
                10 COMM-GL11-QTNBUO         PIC 9(04).                          
                10 COMM-GL11-NFOUR          PIC X(05).                          
                10 COMM-GL11-LFOUR          PIC X(20).                          
                10 LIGNES-GAUCHES.                                              
                   15 COMM-GL11-GAUCHE      OCCURS 10.                          
                      20 COMM-GL11-NCDEG    PIC X(07).                          
                      20 COMM-GL11-CODICG   PIC X(07).                          
                      20 COMM-GL11-LREFFG   PIC X(11).                          
                      20 COMM-GL11-QTEG     PIC 9(05).                          
                      20 COMM-GL11-WSELG    PIC X(01).                          
                      20 COMM-GL11-TV-STDG  PIC X(01).                          
                10 LIGNES-DROITES.                                              
                   15 COMM-GL11-DROITE      OCCURS 10.                          
                      20 COMM-GL11-NFOURD   PIC X(05).                          
                      20 COMM-GL11-NCDED    PIC X(07).                          
                      20 COMM-GL11-CODICD   PIC X(07).                          
                      20 COMM-GL11-LREFFD   PIC X(11).                          
                      20 COMM-GL11-QTED     PIC 9(05).                          
                      20 COMM-GL11-QUOD     PIC 9(04).                          
                      20 COMM-GL11-TV-STDD  PIC X(01).                          
                      20 COMM-GL11-DATA-CD  PIC X(01).                          
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05 COMM-GL11-IND-TS-GL1G-MAX   PIC S9(04) COMP.                    
      *--                                                                       
             05 COMM-GL11-IND-TS-GL1G-MAX   PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05 COMM-GL11-IND-RECH-TS-GL1G  PIC S9(04) COMP.                    
      *--                                                                       
             05 COMM-GL11-IND-RECH-TS-GL1G  PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05 COMM-GL11-IND-TS-GL1D-MAX   PIC S9(04) COMP.                    
      *--                                                                       
             05 COMM-GL11-IND-TS-GL1D-MAX   PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05 COMM-GL11-IND-RECH-TS-GL1D  PIC S9(04) COMP.                    
      *--                                                                       
             05 COMM-GL11-IND-RECH-TS-GL1D  PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05 COMM-GL11-IND-TS-GL1D-MAJ   PIC S9(04) COMP.                    
      *--                                                                       
             05 COMM-GL11-IND-TS-GL1D-MAJ   PIC S9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05 COMM-GL11-IND-TS-GL1M-MAX   PIC S9(04) COMP.                    
      *--                                                                       
             05 COMM-GL11-IND-TS-GL1M-MAX   PIC S9(04) COMP-5.                  
      *}                                                                        
             05 COMM-GL11-TS-GAUCHE         PIC X(04).                          
             05 COMM-GL11-TS-MILIEU         PIC X(04).                          
             05 COMM-GL11-TS-DROITE         PIC X(04).                          
             05 COMM-GL11-FLAGS.                                                
                10 COMM-GL11-VALID-NLIVR    PIC X.                              
                   88 VALID-NLIVR           VALUE 'O'.                          
                   88 PAS-VALID-NLIVR       VALUE 'N'.                          
                10 COMM-GL11-CHGT-DJOUR     PIC X.                              
                   88 CHGT-DJOUR            VALUE 'O'.                          
                   88 PAS-CHGT-DJOUR        VALUE 'N'.                          
                10 COMM-GL11-CHGT-NSOC      PIC X.                              
                   88 CHGT-NSOC             VALUE 'O'.                          
                   88 PAS-CHGT-NSOC         VALUE 'N'.                          
                10 COMM-GL11-CHGT-PALET     PIC X.                              
                   88 CHGT-PALET            VALUE 'O'.                          
                   88 PAS-CHGT-PALET        VALUE 'N'.                          
                10 COMM-GL11-CHGT-NFOUR     PIC X.                              
                   88 CHGT-NFOUR            VALUE 'O'.                          
                   88 PAS-CHGT-NFOUR        VALUE 'N'.                          
                10 COMM-GL11-MODIF          PIC X.                              
                   88 MODIF                 VALUE 'O'.                          
                   88 NO-MODIF              VALUE 'N'.                          
             05 COMM-GL11-MGL20-FLAG        PIC X.                              
                88 APPEL-MGL20              VALUE 'O'.                          
                88 PAS-APPEL-MGL20          VALUE 'N'.                          
             05 COMM-GL11-HEURE-FLAG        PIC X.                              
                88 AFFICHAGE-HEURE          VALUE 'O'.                          
                88 PAS-AFFICHAGE-HEURE      VALUE 'N'.                          
             05 COMM-GL11-CODE-RAISON       PIC X.                              
                88 COMM-GL11-CREASON        VALUE 'O'.                          
                88 COMM-GL11-NOT-CREASON    VALUE 'N'.                          
             05 COMM-GL11-FPLANDATE         PIC X(08).                          
             05 COMM-GL11-FPLANFLAG         PIC X(01).                          
             05 COMM-GL11-FPLANFLAG-ORIG    PIC X(01).                          
E0145        05 COMM-GL11-DATE-LIMITE       PIC X(08).                          
      *** ZONE UTILISEE PAR LE PROGRAMME TGL12                                  
             05 COMM-GL12-DATA.                                                 
                10 COMM-GL12-NLIVRAIS       PIC X(7).                           
                10 COMM-GL12-NFOUR          PIC X(5).                           
                10 COMM-GL12-LFOUR          PIC X(20).                          
                10 COMM-GL12-NCDE           PIC X(7).                           
      *----------  LIGNES A ECLATER                                             
                10 LIGNES-ARTICLES.                                             
                   15 COMM-GL12-CDE-CODIC   OCCURS 50.                          
                      20 COMM-GL12-CODIC    PIC X(07).                          
                      20 COMM-GL12-DATEC    PIC X(08).                          
                      20 COMM-GL12-SOCDEP   PIC X(06).                          
      *----------  GESTION PAGINATION                                           
                10 COMM-GL12-PAGE           PIC 9(2).                   01261500
                10 COMM-GL12-PAGE-MAX       PIC 9(2).                   01261600
                10 COMM-GL12-MODIF-88       PIC X.                      01261600
                   88 COMM-MODIF-OK         VALUE '0'.                  01261600
                   88 COMM-MODIF-KO         VALUE '1'.                  01261600
                10 COMM-GL12-MEM-ERR-QCOLIR PIC X.                      01261600
                10 COMM-GL12-XCTRL-QCOLIRECEPT PIC X.                   01261600
C0256-*** ZONE UTILISEE PAR LES PROGRAMME TGL11 & TGL12                         
             05 COMM-GL11-GL12-CTL-INTEGRITE   PIC X.                           
                88 COMM-CTL-INT-OK          VALUE '0'.                          
-C0256          88 COMM-CTL-INT-KO          VALUE '1'.                          
      ***************************************************************** 01270000
C0256        05 FILLER                      PIC X(5142).                        
C0256 *      05 FILLER                      PIC X(5143).                        
E0145**      05 FILLER                      PIC X(5155).                        
      *** GARDER CE FILLER DE 316 CONTENU DANS COMMGL00 (WORKGF55 + 16)         
             05 FILLER                      PIC X(316).                         
                                                                                
