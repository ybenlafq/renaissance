      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE INCRD DESCRIPTIF CODES GA30 INCRD      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVINCRD .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVINCRD .                                                            
      *}                                                                        
           05  INCRD-CTABLEG2    PIC X(15).                                     
           05  INCRD-CTABLEG2-REDEF REDEFINES INCRD-CTABLEG2.                   
               10  INCRD-TYPE            PIC X(06).                             
               10  INCRD-VALEUR          PIC X(05).                             
               10  INCRD-POSIT           PIC X(02).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  INCRD-POSIT-N        REDEFINES INCRD-POSIT                   
                                         PIC 9(02).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  INCRD-WTABLEG     PIC X(80).                                     
           05  INCRD-WTABLEG-REDEF  REDEFINES INCRD-WTABLEG.                    
               10  INCRD-DATA            PIC X(50).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVINCRD-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVINCRD-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  INCRD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  INCRD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  INCRD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  INCRD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
