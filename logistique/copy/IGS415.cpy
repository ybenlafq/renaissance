      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGS415 AU 31/01/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,03,BI,A,                          *        
      *                           19,06,BI,A,                          *        
      *                           25,02,BI,A,                          *        
      *                           27,05,BI,A,                          *        
      *                           32,05,BI,A,                          *        
      *                           37,05,BI,A,                          *        
      *                           42,07,BI,A,                          *        
      *                           49,06,BI,A,                          *        
      *                           55,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGS415.                                                        
            05 NOMETAT-IGS415           PIC X(6) VALUE 'IGS415'.                
            05 RUPTURES-IGS415.                                                 
           10 IGS415-NSOCDEPOT          PIC X(03).                      007  003
           10 IGS415-NDEPOT             PIC X(03).                      010  003
           10 IGS415-NSOC               PIC X(03).                      013  003
           10 IGS415-CTYPLIEU           PIC X(03).                      016  003
           10 IGS415-DMVT               PIC X(06).                      019  006
           10 IGS415-TEBRB              PIC X(02).                      025  002
           10 IGS415-WSEQFAM            PIC 9(05).                      027  005
           10 IGS415-CFAM               PIC X(05).                      032  005
           10 IGS415-CMARQ              PIC X(05).                      037  005
           10 IGS415-NCODIC             PIC X(07).                      042  007
           10 IGS415-DDELIV             PIC X(06).                      049  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGS415-SEQUENCE           PIC S9(04) COMP.                055  002
      *--                                                                       
           10 IGS415-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGS415.                                                   
           10 IGS415-CMODDEL            PIC X(03).                      057  003
           10 IGS415-COMMENTAIRES       PIC X(30).                      060  030
           10 IGS415-CTYPE              PIC X(01).                      090  001
           10 IGS415-LLIEU              PIC X(20).                      091  020
           10 IGS415-LREFFOURN          PIC X(20).                      111  020
           10 IGS415-NLIEU              PIC X(03).                      131  003
           10 IGS415-NMUTATION          PIC X(07).                      134  007
           10 IGS415-NRESERVATION       PIC X(01).                      141  001
           10 IGS415-NSOCIETE           PIC X(03).                      142  003
           10 IGS415-NVENTE             PIC X(07).                      145  007
           10 IGS415-RESERVATION        PIC X(01).                      152  001
           10 IGS415-QMVT               PIC S9(04)      COMP-3.         153  003
           10 IGS415-QRESDISPO          PIC S9(05)      COMP-3.         156  003
           10 IGS415-QTOTAL             PIC S9(05)      COMP-3.         159  003
           10 IGS415-QVENDABLE          PIC S9(05)      COMP-3.         162  003
           10 IGS415-QVENDUE            PIC S9(04)      COMP-3.         165  003
            05 FILLER                      PIC X(345).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGS415-LONG           PIC S9(4)   COMP  VALUE +167.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGS415-LONG           PIC S9(4) COMP-5  VALUE +167.           
                                                                                
      *}                                                                        
