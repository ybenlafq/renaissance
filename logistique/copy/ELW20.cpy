      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU07   EMU07                                              00000020
      ***************************************************************** 00000030
       01   ELW20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTL    COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSOCDEPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPOTF    PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCDEPOTI    PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNDEPOTI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCIMPRL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCIMPRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCIMPRF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCIMPRI   PIC X(4).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDESTOCKL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MDDESTOCKL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDDESTOCKF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDDESTOCKI     PIC X(8).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSELGLOBL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MWSELGLOBL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWSELGLOBF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MWSELGLOBI     PIC X.                                     00000370
           02 M153I OCCURS   15 TIMES .                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTATIONL  COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNMUTATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNMUTATIONF  PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNMUTATIONI  PIC X(7).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCIETEL   COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNSOCIETEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCIETEF   PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNSOCIETEI   PIC X(3).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNLIEUI      PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSELARTL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCSELARTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCSELARTF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCSELARTI    PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELINDL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MWSELINDL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWSELINDF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MWSELINDI    PIC X.                                     00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEDITIONL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDEDITIONL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDEDITIONF   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDEDITIONI   PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MZONCMDI  PIC X(15).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(58).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * SDF: EMU07   EMU07                                              00000880
      ***************************************************************** 00000890
       01   ELW20O REDEFINES ELW20I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MPAGEA    PIC X.                                          00001070
           02 MPAGEC    PIC X.                                          00001080
           02 MPAGEP    PIC X.                                          00001090
           02 MPAGEH    PIC X.                                          00001100
           02 MPAGEV    PIC X.                                          00001110
           02 MPAGEO    PIC X(3).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MNSOCDEPOTA    PIC X.                                     00001140
           02 MNSOCDEPOTC    PIC X.                                     00001150
           02 MNSOCDEPOTP    PIC X.                                     00001160
           02 MNSOCDEPOTH    PIC X.                                     00001170
           02 MNSOCDEPOTV    PIC X.                                     00001180
           02 MNSOCDEPOTO    PIC X(3).                                  00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MNDEPOTA  PIC X.                                          00001210
           02 MNDEPOTC  PIC X.                                          00001220
           02 MNDEPOTP  PIC X.                                          00001230
           02 MNDEPOTH  PIC X.                                          00001240
           02 MNDEPOTV  PIC X.                                          00001250
           02 MNDEPOTO  PIC X(3).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MCIMPRA   PIC X.                                          00001280
           02 MCIMPRC   PIC X.                                          00001290
           02 MCIMPRP   PIC X.                                          00001300
           02 MCIMPRH   PIC X.                                          00001310
           02 MCIMPRV   PIC X.                                          00001320
           02 MCIMPRO   PIC X(4).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MDDESTOCKA     PIC X.                                     00001350
           02 MDDESTOCKC     PIC X.                                     00001360
           02 MDDESTOCKP     PIC X.                                     00001370
           02 MDDESTOCKH     PIC X.                                     00001380
           02 MDDESTOCKV     PIC X.                                     00001390
           02 MDDESTOCKO     PIC X(8).                                  00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MWSELGLOBA     PIC X.                                     00001420
           02 MWSELGLOBC     PIC X.                                     00001430
           02 MWSELGLOBP     PIC X.                                     00001440
           02 MWSELGLOBH     PIC X.                                     00001450
           02 MWSELGLOBV     PIC X.                                     00001460
           02 MWSELGLOBO     PIC X.                                     00001470
           02 M153O OCCURS   15 TIMES .                                 00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MNMUTATIONA  PIC X.                                     00001500
             03 MNMUTATIONC  PIC X.                                     00001510
             03 MNMUTATIONP  PIC X.                                     00001520
             03 MNMUTATIONH  PIC X.                                     00001530
             03 MNMUTATIONV  PIC X.                                     00001540
             03 MNMUTATIONO  PIC X(7).                                  00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MNSOCIETEA   PIC X.                                     00001570
             03 MNSOCIETEC   PIC X.                                     00001580
             03 MNSOCIETEP   PIC X.                                     00001590
             03 MNSOCIETEH   PIC X.                                     00001600
             03 MNSOCIETEV   PIC X.                                     00001610
             03 MNSOCIETEO   PIC X(3).                                  00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MNLIEUA      PIC X.                                     00001640
             03 MNLIEUC PIC X.                                          00001650
             03 MNLIEUP PIC X.                                          00001660
             03 MNLIEUH PIC X.                                          00001670
             03 MNLIEUV PIC X.                                          00001680
             03 MNLIEUO      PIC X(3).                                  00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MCSELARTA    PIC X.                                     00001710
             03 MCSELARTC    PIC X.                                     00001720
             03 MCSELARTP    PIC X.                                     00001730
             03 MCSELARTH    PIC X.                                     00001740
             03 MCSELARTV    PIC X.                                     00001750
             03 MCSELARTO    PIC X(5).                                  00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MWSELINDA    PIC X.                                     00001780
             03 MWSELINDC    PIC X.                                     00001790
             03 MWSELINDP    PIC X.                                     00001800
             03 MWSELINDH    PIC X.                                     00001810
             03 MWSELINDV    PIC X.                                     00001820
             03 MWSELINDO    PIC X.                                     00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MDEDITIONA   PIC X.                                     00001850
             03 MDEDITIONC   PIC X.                                     00001860
             03 MDEDITIONP   PIC X.                                     00001870
             03 MDEDITIONH   PIC X.                                     00001880
             03 MDEDITIONV   PIC X.                                     00001890
             03 MDEDITIONO   PIC X(8).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MZONCMDA  PIC X.                                          00001920
           02 MZONCMDC  PIC X.                                          00001930
           02 MZONCMDP  PIC X.                                          00001940
           02 MZONCMDH  PIC X.                                          00001950
           02 MZONCMDV  PIC X.                                          00001960
           02 MZONCMDO  PIC X(15).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(58).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
