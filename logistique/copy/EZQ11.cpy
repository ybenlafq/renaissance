      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EZQ11   EZQ11                                              00000020
      ***************************************************************** 00000030
       01   EZQ11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTITREL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLTITREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTITREF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLTITREI  PIC X(25).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCIETEI     PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNLIEUI   PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLLIEUI   PIC X(18).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPLIVL   COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNSOCDEPLIVL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNSOCDEPLIVF   PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNSOCDEPLIVI   PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUDEPLIVL  COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNLIEUDEPLIVL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNLIEUDEPLIVF  PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNLIEUDEPLIVI  PIC X(3).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPERIML  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCPERIML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPERIMF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCPERIMI  PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPERIML  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLPERIML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPERIMF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLPERIMI  PIC X(20).                                      00000530
           02 MLIGNEI OCCURS   13 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCZONELEL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCZONELEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCZONELEF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCZONELEI    PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLZONELEL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLZONELEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLZONELEF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLZONELEI    PIC X(20).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSUPPL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MWSUPPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWSUPPF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MWSUPPI      PIC X.                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MZONCMDI  PIC X(15).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(58).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: EZQ11   EZQ11                                              00000920
      ***************************************************************** 00000930
       01   EZQ11O REDEFINES EZQ11I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MNPAGEA   PIC X.                                          00001110
           02 MNPAGEC   PIC X.                                          00001120
           02 MNPAGEP   PIC X.                                          00001130
           02 MNPAGEH   PIC X.                                          00001140
           02 MNPAGEV   PIC X.                                          00001150
           02 MNPAGEO   PIC X(3).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MPAGEMAXA      PIC X.                                     00001180
           02 MPAGEMAXC PIC X.                                          00001190
           02 MPAGEMAXP PIC X.                                          00001200
           02 MPAGEMAXH PIC X.                                          00001210
           02 MPAGEMAXV PIC X.                                          00001220
           02 MPAGEMAXO      PIC X(3).                                  00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MLTITREA  PIC X.                                          00001250
           02 MLTITREC  PIC X.                                          00001260
           02 MLTITREP  PIC X.                                          00001270
           02 MLTITREH  PIC X.                                          00001280
           02 MLTITREV  PIC X.                                          00001290
           02 MLTITREO  PIC X(25).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MNSOCIETEA     PIC X.                                     00001320
           02 MNSOCIETEC     PIC X.                                     00001330
           02 MNSOCIETEP     PIC X.                                     00001340
           02 MNSOCIETEH     PIC X.                                     00001350
           02 MNSOCIETEV     PIC X.                                     00001360
           02 MNSOCIETEO     PIC X(3).                                  00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNLIEUA   PIC X.                                          00001390
           02 MNLIEUC   PIC X.                                          00001400
           02 MNLIEUP   PIC X.                                          00001410
           02 MNLIEUH   PIC X.                                          00001420
           02 MNLIEUV   PIC X.                                          00001430
           02 MNLIEUO   PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MLLIEUA   PIC X.                                          00001460
           02 MLLIEUC   PIC X.                                          00001470
           02 MLLIEUP   PIC X.                                          00001480
           02 MLLIEUH   PIC X.                                          00001490
           02 MLLIEUV   PIC X.                                          00001500
           02 MLLIEUO   PIC X(18).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MNSOCDEPLIVA   PIC X.                                     00001530
           02 MNSOCDEPLIVC   PIC X.                                     00001540
           02 MNSOCDEPLIVP   PIC X.                                     00001550
           02 MNSOCDEPLIVH   PIC X.                                     00001560
           02 MNSOCDEPLIVV   PIC X.                                     00001570
           02 MNSOCDEPLIVO   PIC X(3).                                  00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNLIEUDEPLIVA  PIC X.                                     00001600
           02 MNLIEUDEPLIVC  PIC X.                                     00001610
           02 MNLIEUDEPLIVP  PIC X.                                     00001620
           02 MNLIEUDEPLIVH  PIC X.                                     00001630
           02 MNLIEUDEPLIVV  PIC X.                                     00001640
           02 MNLIEUDEPLIVO  PIC X(3).                                  00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCPERIMA  PIC X.                                          00001670
           02 MCPERIMC  PIC X.                                          00001680
           02 MCPERIMP  PIC X.                                          00001690
           02 MCPERIMH  PIC X.                                          00001700
           02 MCPERIMV  PIC X.                                          00001710
           02 MCPERIMO  PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLPERIMA  PIC X.                                          00001740
           02 MLPERIMC  PIC X.                                          00001750
           02 MLPERIMP  PIC X.                                          00001760
           02 MLPERIMH  PIC X.                                          00001770
           02 MLPERIMV  PIC X.                                          00001780
           02 MLPERIMO  PIC X(20).                                      00001790
           02 MLIGNEO OCCURS   13 TIMES .                               00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MCZONELEA    PIC X.                                     00001820
             03 MCZONELEC    PIC X.                                     00001830
             03 MCZONELEP    PIC X.                                     00001840
             03 MCZONELEH    PIC X.                                     00001850
             03 MCZONELEV    PIC X.                                     00001860
             03 MCZONELEO    PIC X(5).                                  00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MLZONELEA    PIC X.                                     00001890
             03 MLZONELEC    PIC X.                                     00001900
             03 MLZONELEP    PIC X.                                     00001910
             03 MLZONELEH    PIC X.                                     00001920
             03 MLZONELEV    PIC X.                                     00001930
             03 MLZONELEO    PIC X(20).                                 00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MWSUPPA      PIC X.                                     00001960
             03 MWSUPPC PIC X.                                          00001970
             03 MWSUPPP PIC X.                                          00001980
             03 MWSUPPH PIC X.                                          00001990
             03 MWSUPPV PIC X.                                          00002000
             03 MWSUPPO      PIC X.                                     00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MZONCMDA  PIC X.                                          00002030
           02 MZONCMDC  PIC X.                                          00002040
           02 MZONCMDP  PIC X.                                          00002050
           02 MZONCMDH  PIC X.                                          00002060
           02 MZONCMDV  PIC X.                                          00002070
           02 MZONCMDO  PIC X(15).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(58).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
