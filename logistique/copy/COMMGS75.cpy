      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGS75 (TGQ00 -> MENU)    TR: GQ00  *    00002237
      *   GESTION DES CODES OPERATIONS                             *    00002337
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *  TRANSACTION GS75 : REMPLISSAGE DE LA TABLE RTGS05         *    00411037
      *                                                                 00411038
          02 COMM-GS75-APPLI REDEFINES COMM-GQ00-APPLI.                 00420037
      *--MODIF NLG------------------- CODE CISOC                                
             03 COMM-GS75-NSOCIETE       PIC X(03).                             
      *------------------------------ CODE OPERATION                    00550037
             03 COMM-GS75-COPER          PIC X(10).                     00560037
      *------------------------------ LIBELLE CODE OPERATION            00560038
             03 COMM-GS75-LCOPER         PIC X(20).                     00560039
      *------------------------------ ZONE LIBRE                        01710035
             03 COMM-GS75-LIBRE          PIC X(3691).                   01720037
      ***************************************************************** 02170035
                                                                                
