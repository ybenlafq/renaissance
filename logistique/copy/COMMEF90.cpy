      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *==> DARTY ****************************************************** 00000010
      *    MISE A JOUR DE LA TABLE RTEF00                               00000020
      ***************************************************************** 00000030
                                                                        00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
AM    *01  COMM-EF90-LONG-COMMAREA PIC S9(4) COMP VALUE +223.           00000050
      *                                                                         
      *--                                                                       
       01  COMM-EF90-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +223.                 
                                                                        00000060
      *}                                                                        
       01  Z-COMMAREA-EF90.                                             00000070
           02  COMM-EF90-CTRAIT        PIC X(05).                       00000080
           02  COMM-EF90-NENVOI        PIC X(07).                       00000090
           02  COMM-EF90-NSOCORIG      PIC X(03).                       00000100
           02  COMM-EF90-NLIEUORIG     PIC X(03).                       00000110
           02  COMM-EF90-NORIGINE      PIC X(07).                       00000120
           02  COMM-EF90-NCODIC        PIC X(07).                       00000130
           02  COMM-EF90-CTIERS        PIC X(05).                       00000140
           02  COMM-EF90-DENVOI        PIC X(08).                       00000150
           02  COMM-EF90-NSERIE        PIC X(16).                       00000160
           02  COMM-EF90-NACCORD       PIC X(12).                       00000170
           02  COMM-EF90-DACCORD       PIC X(08).                       00000180
           02  COMM-EF90-LNOMACCORD    PIC X(10).                       00000190
           02  COMM-EF90-QTENV         PIC 9(05).                       00000200
           02  COMM-EF90-CRENDU        PIC X(05).                       00000210
           02  COMM-EF90-CGARANTIE     PIC X(05).                       00000220
           02  COMM-EF90-NSOCMVTO      PIC X(03).                       00000230
           02  COMM-EF90-NLIEUMVTO     PIC X(03).                       00000240
           02  COMM-EF90-NSLIEUMVTO    PIC X(03).                       00000250
           02  COMM-EF90-CLIEUTRTO     PIC X(05).                       00000260
           02  COMM-EF90-NSOCMVTD      PIC X(03).                       00000270
           02  COMM-EF90-NLIEUMVTD     PIC X(03).                       00000280
           02  COMM-EF90-NSLIEUMVTD    PIC X(03).                       00000290
           02  COMM-EF90-CLIEUTRTD     PIC X(05).                       00000300
           02  COMM-EF90-CISOC         PIC X(05).                       00000310
AM         02  COMM-EF90-NENTCDE       PIC X(05).                       00000320
                                                                        00000330
      * ZONES RETOURNEES PAR MEF90                                      00000340
      *                                                                 00000350
      * REPONSE  OK = 0   SINON = 1 : ERREUR NON BLOQUANTE              00000360
      *                           2 : ERREUR     BLOQUANTE              00000370
                                                                        00000380
           02  COMM-EF90-CRETOUR       PIC 9.                           00000390
           02  COMM-EF90-MESS-ERR      PIC X(78).                       00000400
                                                                                
