      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : AFFECTATION DES EQUIPES DE LIVRAISON                   *         
      *        ( MODE DELIVRANCE / ZONE LIVRAISON / DATE EFFET //     *         
      *          FAMILLES / EQUIPES DE LIVRAISON )                    *         
      *        POUR MISE A JOUR VUE RVGQ0400     (PGR : TGQ03)        *         
      *****************************************************************         
      *                                                               *         
       01  TS-GQ03.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GQ03-LONG                 PIC S9(3) COMP-3                     
                                           VALUE +51.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GQ03-DONNEES.                                                  
      *----------------------------------  MODE DE MAJ                          
      *                                         ' ' : PAS DE MAJ                
      *                                         'C' : CREATION                  
      *                                         'M' : MODIFICATION              
              03 TS-GQ03-CMAJ              PIC X(1).                            
      *----------------------------------  CODE FAMILLE                         
              03 TS-GQ03-CFAM              PIC X(5).                            
      *----------------------------------  LIBELLE FAMILLE                      
              03 TS-GQ03-LFAM              PIC X(20).                           
      *----------------------------------  ZONE EQUIPES DE LIVRAISON            
              03 TS-GQ03-EQUIPLIV          OCCURS 5.                            
      *----------------------------------  CODE EQUIPE                          
                 04 TS-GQ03-CEQUIP            PIC X(05).                        
                                                                                
