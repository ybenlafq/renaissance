      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGS3700                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS3700                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGS3700.                                                            
           02  GS37-NCODIC                                                      
               PIC X(0007).                                                     
           02  GS37-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GS37-NMAG                                                        
               PIC X(0003).                                                     
           02  GS37-QDISPO                                                      
               PIC S9(5) COMP-3.                                                
           02  GS37-QMUTATT                                                     
               PIC S9(5) COMP-3.                                                
           02  GS37-DEXTRACTION                                                 
               PIC X(0008).                                                     
           02  GS37-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGS3700                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGS3700-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS37-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS37-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS37-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS37-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS37-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS37-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS37-QDISPO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS37-QDISPO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS37-QMUTATT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS37-QMUTATT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS37-DEXTRACTION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS37-DEXTRACTION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS37-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS37-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
