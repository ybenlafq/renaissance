      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:29 >
      
      *----------------------------------------------------------------*
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JTL161 AU 25/05/1999  *
      *                                                                *
      *          CRITERES DE TRI  07,05,BI,A,                          *
      *                           12,09,BI,A,                          *
      *                           21,03,BI,A,                          *
      *                           24,02,BI,A,                          *
      *                           26,03,BI,A,                          *
      *                           29,02,BI,A,                          *
      *                                                                *
      *----------------------------------------------------------------*
       01  DSECT-JTL161.
            05 NOMETAT-JTL161           PIC X(6) VALUE 'JTL161'.
            05 RUPTURES-JTL161.
           10 JTL161-CPROTOUR           PIC X(05).
           10 JTL161-LSTOCK             PIC X(09).
           10 JTL161-CMODDELIN2         PIC X(03).
           10 JTL161-NSATELLITE         PIC X(02).
           10 JTL161-NCASE              PIC X(03).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    10 JTL161-SEQUENCE           PIC S9(04) COMP.
      *--
           10 JTL161-SEQUENCE           PIC S9(04) COMP-5.
      *}
            05 CHAMPS-JTL161.
           10 JTL161-CCOMMUT            PIC X(01).
           10 JTL161-CFAM               PIC X(05).
           10 JTL161-CMARQUE            PIC X(05).
           10 JTL161-CMODDELIN          PIC X(03).
           10 JTL161-CPROTOURIN         PIC X(05).
           10 JTL161-CRANGEMENT         PIC X(17).
           10 JTL161-CRANGEMENT2        PIC X(17).
           10 JTL161-HEUREIN            PIC X(05).
           10 JTL161-LRANGEMENT         PIC X(17).
           10 JTL161-LRANGEMENT2        PIC X(17).
           10 JTL161-LREF               PIC X(20).
           10 JTL161-NCODIC             PIC X(07).
           10 JTL161-NDEPOT             PIC X(03).
           10 JTL161-NLIEU              PIC X(03).
           10 JTL161-NSOCIETE           PIC X(03).
           10 JTL161-NVENTE             PIC X(07).
           10 JTL161-DATEIN             PIC X(08).
            05 FILLER                      PIC X(339).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      * 01  DSECT-JTL161-LONG           PIC S9(4)   COMP  VALUE +173.
      *
      *--
        01  DSECT-JTL161-LONG           PIC S9(4) COMP-5  VALUE +173.
      
      *}
