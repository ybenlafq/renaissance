      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CTSOC SOC COMPTA ICS                   *        
      *----------------------------------------------------------------*        
       01  RVCTSOC .                                                            
           05  CTSOC-CTABLEG2    PIC X(15).                                     
           05  CTSOC-CTABLEG2-REDEF REDEFINES CTSOC-CTABLEG2.                   
               10  CTSOC-NSOC            PIC X(03).                             
               10  CTSOC-NLIEU           PIC X(03).                             
               10  CTSOC-DATEFFET        PIC X(08).                             
           05  CTSOC-WTABLEG     PIC X(80).                                     
           05  CTSOC-WTABLEG-REDEF  REDEFINES CTSOC-WTABLEG.                    
               10  CTSOC-WACTIF          PIC X(01).                             
               10  CTSOC-SOCCPT          PIC X(03).                             
               10  CTSOC-ETACPT          PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCTSOC-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTSOC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CTSOC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTSOC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CTSOC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
