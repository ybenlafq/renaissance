      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGQ14   EGQ14                                              00000020
      ***************************************************************** 00000030
       01   EGQ14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MFONCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDATEI    PIC X(8).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDELL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCDELL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCDELF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCDELI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEQUIPL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MEQUIPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEQUIPF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MEQUIPI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURNEEL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MJOURNEEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MJOURNEEF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MJOURNEEI      PIC X(8).                                  00000370
           02 M124I OCCURS   14 TIMES .                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONEL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MZONEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MZONEF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MZONEI  PIC X(5).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MACTI   PIC X.                                          00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPLAGEL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MPLAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPLAGEF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MPLAGEI      PIC X(8).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA2L     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MQUOTA2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA2F     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MQUOTA2I     PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIS2L      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MPRIS2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRIS2F      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MPRIS2I      PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTRL    COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MTRL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MTRF    PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MTRI    PIC X(3).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MPSL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MPSF    PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MPSI    PIC X(3).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRL    COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MPRL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MPRF    PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MPRI    PIC X(3).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MJOURL  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MJOURL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MJOURF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MJOURI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA1L     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MQUOTA1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA1F     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQUOTA1I     PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIS1L      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MPRIS1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRIS1F      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MPRIS1I      PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTOTL   COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MTOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MTOTF   PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MTOTI   PIC X(2).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMINL   COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MMINL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMINF   PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MMINI   PIC X(3).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMOYL   COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MMOYL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMOYF   PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MMOYI   PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMAXL   COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MMAXL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMAXF   PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MMAXI   PIC X(3).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPAGE1L     COMP PIC S9(4).                            00000990
      *--                                                                       
           02 MTOTPAGE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTOTPAGE1F     PIC X.                                     00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MTOTPAGE1I     PIC X(8).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPAGE2L     COMP PIC S9(4).                            00001030
      *--                                                                       
           02 MTOTPAGE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTOTPAGE2F     PIC X.                                     00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MTOTPAGE2I     PIC X(8).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTGEN1L      COMP PIC S9(4).                            00001070
      *--                                                                       
           02 MTOTGEN1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTGEN1F      PIC X.                                     00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MTOTGEN1I      PIC X(8).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTGEN2L      COMP PIC S9(4).                            00001110
      *--                                                                       
           02 MTOTGEN2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTGEN2F      PIC X.                                     00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MTOTGEN2I      PIC X(8).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MZONCMDI  PIC X(15).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MLIBERRI  PIC X(58).                                      00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCODTRAI  PIC X(4).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCICSI    PIC X(5).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MNETNAMI  PIC X(8).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MSCREENI  PIC X(4).                                       00001380
      ***************************************************************** 00001390
      * SDF: EGQ14   EGQ14                                              00001400
      ***************************************************************** 00001410
       01   EGQ14O REDEFINES EGQ14I.                                    00001420
           02 FILLER    PIC X(12).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MDATJOUA  PIC X.                                          00001450
           02 MDATJOUC  PIC X.                                          00001460
           02 MDATJOUP  PIC X.                                          00001470
           02 MDATJOUH  PIC X.                                          00001480
           02 MDATJOUV  PIC X.                                          00001490
           02 MDATJOUO  PIC X(10).                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MTIMJOUA  PIC X.                                          00001520
           02 MTIMJOUC  PIC X.                                          00001530
           02 MTIMJOUP  PIC X.                                          00001540
           02 MTIMJOUH  PIC X.                                          00001550
           02 MTIMJOUV  PIC X.                                          00001560
           02 MTIMJOUO  PIC X(5).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MPAGEA    PIC X.                                          00001590
           02 MPAGEC    PIC X.                                          00001600
           02 MPAGEP    PIC X.                                          00001610
           02 MPAGEH    PIC X.                                          00001620
           02 MPAGEV    PIC X.                                          00001630
           02 MPAGEO    PIC X(3).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MFONCA    PIC X.                                          00001660
           02 MFONCC    PIC X.                                          00001670
           02 MFONCP    PIC X.                                          00001680
           02 MFONCH    PIC X.                                          00001690
           02 MFONCV    PIC X.                                          00001700
           02 MFONCO    PIC X(3).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MDATEA    PIC X.                                          00001730
           02 MDATEC    PIC X.                                          00001740
           02 MDATEP    PIC X.                                          00001750
           02 MDATEH    PIC X.                                          00001760
           02 MDATEV    PIC X.                                          00001770
           02 MDATEO    PIC X(8).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCDELA    PIC X.                                          00001800
           02 MCDELC    PIC X.                                          00001810
           02 MCDELP    PIC X.                                          00001820
           02 MCDELH    PIC X.                                          00001830
           02 MCDELV    PIC X.                                          00001840
           02 MCDELO    PIC X(3).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MEQUIPA   PIC X.                                          00001870
           02 MEQUIPC   PIC X.                                          00001880
           02 MEQUIPP   PIC X.                                          00001890
           02 MEQUIPH   PIC X.                                          00001900
           02 MEQUIPV   PIC X.                                          00001910
           02 MEQUIPO   PIC X(5).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MJOURNEEA      PIC X.                                     00001940
           02 MJOURNEEC PIC X.                                          00001950
           02 MJOURNEEP PIC X.                                          00001960
           02 MJOURNEEH PIC X.                                          00001970
           02 MJOURNEEV PIC X.                                          00001980
           02 MJOURNEEO      PIC X(8).                                  00001990
           02 M124O OCCURS   14 TIMES .                                 00002000
             03 FILLER       PIC X(2).                                  00002010
             03 MZONEA  PIC X.                                          00002020
             03 MZONEC  PIC X.                                          00002030
             03 MZONEP  PIC X.                                          00002040
             03 MZONEH  PIC X.                                          00002050
             03 MZONEV  PIC X.                                          00002060
             03 MZONEO  PIC X(5).                                       00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MACTA   PIC X.                                          00002090
             03 MACTC   PIC X.                                          00002100
             03 MACTP   PIC X.                                          00002110
             03 MACTH   PIC X.                                          00002120
             03 MACTV   PIC X.                                          00002130
             03 MACTO   PIC X.                                          00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MPLAGEA      PIC X.                                     00002160
             03 MPLAGEC PIC X.                                          00002170
             03 MPLAGEP PIC X.                                          00002180
             03 MPLAGEH PIC X.                                          00002190
             03 MPLAGEV PIC X.                                          00002200
             03 MPLAGEO      PIC X(8).                                  00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MQUOTA2A     PIC X.                                     00002230
             03 MQUOTA2C     PIC X.                                     00002240
             03 MQUOTA2P     PIC X.                                     00002250
             03 MQUOTA2H     PIC X.                                     00002260
             03 MQUOTA2V     PIC X.                                     00002270
             03 MQUOTA2O     PIC X(5).                                  00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MPRIS2A      PIC X.                                     00002300
             03 MPRIS2C PIC X.                                          00002310
             03 MPRIS2P PIC X.                                          00002320
             03 MPRIS2H PIC X.                                          00002330
             03 MPRIS2V PIC X.                                          00002340
             03 MPRIS2O      PIC X(5).                                  00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MTRA    PIC X.                                          00002370
             03 MTRC    PIC X.                                          00002380
             03 MTRP    PIC X.                                          00002390
             03 MTRH    PIC X.                                          00002400
             03 MTRV    PIC X.                                          00002410
             03 MTRO    PIC X(3).                                       00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MPSA    PIC X.                                          00002440
             03 MPSC    PIC X.                                          00002450
             03 MPSP    PIC X.                                          00002460
             03 MPSH    PIC X.                                          00002470
             03 MPSV    PIC X.                                          00002480
             03 MPSO    PIC X(3).                                       00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MPRA    PIC X.                                          00002510
             03 MPRC    PIC X.                                          00002520
             03 MPRP    PIC X.                                          00002530
             03 MPRH    PIC X.                                          00002540
             03 MPRV    PIC X.                                          00002550
             03 MPRO    PIC X(3).                                       00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MJOURA  PIC X.                                          00002580
             03 MJOURC  PIC X.                                          00002590
             03 MJOURP  PIC X.                                          00002600
             03 MJOURH  PIC X.                                          00002610
             03 MJOURV  PIC X.                                          00002620
             03 MJOURO  PIC X(8).                                       00002630
             03 FILLER       PIC X(2).                                  00002640
             03 MQUOTA1A     PIC X.                                     00002650
             03 MQUOTA1C     PIC X.                                     00002660
             03 MQUOTA1P     PIC X.                                     00002670
             03 MQUOTA1H     PIC X.                                     00002680
             03 MQUOTA1V     PIC X.                                     00002690
             03 MQUOTA1O     PIC X(5).                                  00002700
             03 FILLER       PIC X(2).                                  00002710
             03 MPRIS1A      PIC X.                                     00002720
             03 MPRIS1C PIC X.                                          00002730
             03 MPRIS1P PIC X.                                          00002740
             03 MPRIS1H PIC X.                                          00002750
             03 MPRIS1V PIC X.                                          00002760
             03 MPRIS1O      PIC X(5).                                  00002770
             03 FILLER       PIC X(2).                                  00002780
             03 MTOTA   PIC X.                                          00002790
             03 MTOTC   PIC X.                                          00002800
             03 MTOTP   PIC X.                                          00002810
             03 MTOTH   PIC X.                                          00002820
             03 MTOTV   PIC X.                                          00002830
             03 MTOTO   PIC X(2).                                       00002840
             03 FILLER       PIC X(2).                                  00002850
             03 MMINA   PIC X.                                          00002860
             03 MMINC   PIC X.                                          00002870
             03 MMINP   PIC X.                                          00002880
             03 MMINH   PIC X.                                          00002890
             03 MMINV   PIC X.                                          00002900
             03 MMINO   PIC X(3).                                       00002910
             03 FILLER       PIC X(2).                                  00002920
             03 MMOYA   PIC X.                                          00002930
             03 MMOYC   PIC X.                                          00002940
             03 MMOYP   PIC X.                                          00002950
             03 MMOYH   PIC X.                                          00002960
             03 MMOYV   PIC X.                                          00002970
             03 MMOYO   PIC X(3).                                       00002980
             03 FILLER       PIC X(2).                                  00002990
             03 MMAXA   PIC X.                                          00003000
             03 MMAXC   PIC X.                                          00003010
             03 MMAXP   PIC X.                                          00003020
             03 MMAXH   PIC X.                                          00003030
             03 MMAXV   PIC X.                                          00003040
             03 MMAXO   PIC X(3).                                       00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MTOTPAGE1A     PIC X.                                     00003070
           02 MTOTPAGE1C     PIC X.                                     00003080
           02 MTOTPAGE1P     PIC X.                                     00003090
           02 MTOTPAGE1H     PIC X.                                     00003100
           02 MTOTPAGE1V     PIC X.                                     00003110
           02 MTOTPAGE1O     PIC X(8).                                  00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MTOTPAGE2A     PIC X.                                     00003140
           02 MTOTPAGE2C     PIC X.                                     00003150
           02 MTOTPAGE2P     PIC X.                                     00003160
           02 MTOTPAGE2H     PIC X.                                     00003170
           02 MTOTPAGE2V     PIC X.                                     00003180
           02 MTOTPAGE2O     PIC X(8).                                  00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MTOTGEN1A      PIC X.                                     00003210
           02 MTOTGEN1C PIC X.                                          00003220
           02 MTOTGEN1P PIC X.                                          00003230
           02 MTOTGEN1H PIC X.                                          00003240
           02 MTOTGEN1V PIC X.                                          00003250
           02 MTOTGEN1O      PIC X(8).                                  00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MTOTGEN2A      PIC X.                                     00003280
           02 MTOTGEN2C PIC X.                                          00003290
           02 MTOTGEN2P PIC X.                                          00003300
           02 MTOTGEN2H PIC X.                                          00003310
           02 MTOTGEN2V PIC X.                                          00003320
           02 MTOTGEN2O      PIC X(8).                                  00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MZONCMDA  PIC X.                                          00003350
           02 MZONCMDC  PIC X.                                          00003360
           02 MZONCMDP  PIC X.                                          00003370
           02 MZONCMDH  PIC X.                                          00003380
           02 MZONCMDV  PIC X.                                          00003390
           02 MZONCMDO  PIC X(15).                                      00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MLIBERRA  PIC X.                                          00003420
           02 MLIBERRC  PIC X.                                          00003430
           02 MLIBERRP  PIC X.                                          00003440
           02 MLIBERRH  PIC X.                                          00003450
           02 MLIBERRV  PIC X.                                          00003460
           02 MLIBERRO  PIC X(58).                                      00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MCODTRAA  PIC X.                                          00003490
           02 MCODTRAC  PIC X.                                          00003500
           02 MCODTRAP  PIC X.                                          00003510
           02 MCODTRAH  PIC X.                                          00003520
           02 MCODTRAV  PIC X.                                          00003530
           02 MCODTRAO  PIC X(4).                                       00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MCICSA    PIC X.                                          00003560
           02 MCICSC    PIC X.                                          00003570
           02 MCICSP    PIC X.                                          00003580
           02 MCICSH    PIC X.                                          00003590
           02 MCICSV    PIC X.                                          00003600
           02 MCICSO    PIC X(5).                                       00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MNETNAMA  PIC X.                                          00003630
           02 MNETNAMC  PIC X.                                          00003640
           02 MNETNAMP  PIC X.                                          00003650
           02 MNETNAMH  PIC X.                                          00003660
           02 MNETNAMV  PIC X.                                          00003670
           02 MNETNAMO  PIC X(8).                                       00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MSCREENA  PIC X.                                          00003700
           02 MSCREENC  PIC X.                                          00003710
           02 MSCREENP  PIC X.                                          00003720
           02 MSCREENH  PIC X.                                          00003730
           02 MSCREENV  PIC X.                                          00003740
           02 MSCREENO  PIC X(4).                                       00003750
                                                                                
