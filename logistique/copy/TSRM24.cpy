      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM24                                          *  00020000
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
AA0213*01  TS-LONG              PIC S9(3) COMP-3 VALUE 84.              00060001
AA0213 01  TS-LONG              PIC S9(3) COMP-3 VALUE 89.              00061001
       01  TS-DONNEES.                                                  00070000
              05 TS-NSOC       PIC X(03).                               00080000
              05 TS-NMAG       PIC X(03).                               00090000
              05 TS-MLMAG      PIC X(20).                               00100000
AA0213*       05 TS-CEXPOMAG   PIC X(05).                               00110001
AA0213        05 TS-CEXPOMAG   PIC X(10).                               00111001
              05 TS-WASSORT    PIC X(01).                               00120000
              05 TS-WASSOSTD   PIC X(01).                               00130000
              05 TS-QEXPO      PIC X(03).                               00140000
              05 TS-QLS        PIC X(03).                               00150000
              05 TS-QV8SP      PIC X(05).                               00160001
              05 TS-QV8SR      PIC X(05).                               00170001
              05 TS-QSO        PIC X(03).                               00180000
              05 TS-QSM        PIC X(03).                               00190001
              05 TS-QSOP       PIC X(03).                               00200000
              05 TS-QSMP       PIC X(03).                               00210001
              05 TS-MODIF      PIC X(01).                               00220000
              05 TS-QSO-SAVE   PIC X(03).                               00230001
              05 TS-QSM-SAVE   PIC X(03).                               00240001
              05 TS-QSOTROUVE  PIC X.                                   00250001
              05 TS-QSATROUVE  PIC X.                                   00260001
              05 TS-QSOMIN     PIC X(03).                               00270001
              05 TS-QSMMIN     PIC X(03).                               00280001
              05 TS-QSOMAX     PIC X(03).                               00290001
              05 TS-QSMMAX     PIC X(03).                               00300001
              05 TS-W80        PIC X.                                   00310001
              05 TS-FLAG       PIC X.                                   00320001
                                                                                
