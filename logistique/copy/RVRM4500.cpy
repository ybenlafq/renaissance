      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM4500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM4500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM4500.                                                            
           02  RM45-DSIMULATION                                                 
               PIC X(0008).                                                     
           02  RM45-NSIMULATION                                                 
               PIC X(0003).                                                     
           02  RM45-NCODIC                                                      
               PIC X(0007).                                                     
           02  RM45-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RM45-NLIEU                                                       
               PIC X(0003).                                                     
           02  RM45-QTE1                                                        
               PIC S9(7) COMP-3.                                                
           02  RM45-QTE2                                                        
               PIC S9(7) COMP-3.                                                
           02  RM45-QTE3                                                        
               PIC S9(7) COMP-3.                                                
           02  RM45-QSA1                                                        
               PIC S9(7) COMP-3.                                                
           02  RM45-QSA2                                                        
               PIC S9(7) COMP-3.                                                
           02  RM45-QSA3                                                        
               PIC S9(7) COMP-3.                                                
           02  RM45-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM4500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM4500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-DSIMULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-DSIMULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-NSIMULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-NSIMULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-QTE1-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-QTE1-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-QTE2-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-QTE2-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-QTE3-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-QTE3-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-QSA1-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-QSA1-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-QSA2-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-QSA2-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-QSA3-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-QSA3-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM45-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM45-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
