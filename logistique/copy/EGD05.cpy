      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD05   EGD05                                              00000020
      ***************************************************************** 00000030
       01   EGD05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTL    COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSOCDEPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPOTF    PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCDEPOTI    PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNDEPOTI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDESTL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MDATEDESTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDATEDESTF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATEDESTI     PIC X(8).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLTOTML     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MQNBLTOTML COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBLTOTMF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MQNBLTOTMI     PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPTOTML     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MQNBPTOTML COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBPTOTMF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MQNBPTOTMI     PIC X(6).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLSOLML     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MQNBLSOLML COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBLSOLMF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MQNBLSOLMI     PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPSOLML     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MQNBPSOLML COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBPSOLMF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MQNBPSOLMI     PIC X(6).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLRACKML    COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MQNBLRACKML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQNBLRACKMF    PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MQNBLRACKMI    PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPRACKML    COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MQNBPRACKML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQNBPRACKMF    PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MQNBPRACKMI    PIC X(6).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLTOTSL     COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MQNBLTOTSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBLTOTSF     PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MQNBLTOTSI     PIC X(5).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPTOTSL     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MQNBPTOTSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBPTOTSF     PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQNBPTOTSI     PIC X(6).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLSOLSL     COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MQNBLSOLSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBLSOLSF     PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQNBLSOLSI     PIC X(5).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPSOLSL     COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MQNBPSOLSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBPSOLSF     PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MQNBPSOLSI     PIC X(6).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLRACKSL    COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MQNBLRACKSL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQNBLRACKSF    PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MQNBLRACKSI    PIC X(5).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPRACKSL    COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MQNBPRACKSL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQNBPRACKSF    PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MQNBPRACKSI    PIC X(6).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLTOTCL     COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MQNBLTOTCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBLTOTCF     PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MQNBLTOTCI     PIC X(5).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPTOTCL     COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MQNBPTOTCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBPTOTCF     PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MQNBPTOTCI     PIC X(6).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLSOLCL     COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MQNBLSOLCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBLSOLCF     PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MQNBLSOLCI     PIC X(5).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPSOLCL     COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MQNBPSOLCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBPSOLCF     PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MQNBPSOLCI     PIC X(6).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLRACKCL    COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MQNBLRACKCL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQNBLRACKCF    PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MQNBLRACKCI    PIC X(5).                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPRACKCL    COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MQNBPRACKCL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQNBPRACKCF    PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MQNBPRACKCI    PIC X(6).                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCOMMUTL      COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MWCOMMUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWCOMMUTF      PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MWCOMMUTI      PIC X(4).                                  00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLTOTGL     COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MQNBLTOTGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBLTOTGF     PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MQNBLTOTGI     PIC X(5).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPTOTGL     COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MQNBPTOTGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBPTOTGF     PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MQNBPTOTGI     PIC X(6).                                  00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLSOLGL     COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MQNBLSOLGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBLSOLGF     PIC X.                                     00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MQNBLSOLGI     PIC X(5).                                  00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPSOLGL     COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MQNBPSOLGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQNBPSOLGF     PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MQNBPSOLGI     PIC X(6).                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBLRACKGL    COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MQNBLRACKGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQNBLRACKGF    PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MQNBLRACKGI    PIC X(5).                                  00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQNBPRACKGL    COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MQNBPRACKGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQNBPRACKGF    PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MQNBPRACKGI    PIC X(6).                                  00001290
           02 MTABLEAUI OCCURS   10 TIMES .                             00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00001310
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MNSOCI  PIC X(3).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MNLIEUI      PIC X(3).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODLIVRL   COMP PIC S9(4).                            00001390
      *--                                                                       
             03 MCMODLIVRL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCMODLIVRF   PIC X.                                     00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MCMODLIVRI   PIC X(3).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEQUIPLIVL   COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MEQUIPLIVL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MEQUIPLIVF   PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MEQUIPLIVI   PIC X(5).                                  00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLHEURELIML  COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MLHEURELIML COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLHEURELIMF  PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MLHEURELIMI  PIC X(10).                                 00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBLTOTL    COMP PIC S9(4).                            00001510
      *--                                                                       
             03 MQNBLTOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQNBLTOTF    PIC X.                                     00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MQNBLTOTI    PIC X(4).                                  00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBPTOTL    COMP PIC S9(4).                            00001550
      *--                                                                       
             03 MQNBPTOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQNBPTOTF    PIC X.                                     00001560
             03 FILLER  PIC X(4).                                       00001570
             03 MQNBPTOTI    PIC X(5).                                  00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBLSOLL    COMP PIC S9(4).                            00001590
      *--                                                                       
             03 MQNBLSOLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQNBLSOLF    PIC X.                                     00001600
             03 FILLER  PIC X(4).                                       00001610
             03 MQNBLSOLI    PIC X(4).                                  00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBPSOLL    COMP PIC S9(4).                            00001630
      *--                                                                       
             03 MQNBPSOLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQNBPSOLF    PIC X.                                     00001640
             03 FILLER  PIC X(4).                                       00001650
             03 MQNBPSOLI    PIC X(5).                                  00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBLRACKL   COMP PIC S9(4).                            00001670
      *--                                                                       
             03 MQNBLRACKL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQNBLRACKF   PIC X.                                     00001680
             03 FILLER  PIC X(4).                                       00001690
             03 MQNBLRACKI   PIC X(4).                                  00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBPRACKL   COMP PIC S9(4).                            00001710
      *--                                                                       
             03 MQNBPRACKL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQNBPRACKF   PIC X.                                     00001720
             03 FILLER  PIC X(4).                                       00001730
             03 MQNBPRACKI   PIC X(5).                                  00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBLVRACL   COMP PIC S9(4).                            00001750
      *--                                                                       
             03 MQNBLVRACL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQNBLVRACF   PIC X.                                     00001760
             03 FILLER  PIC X(4).                                       00001770
             03 MQNBLVRACI   PIC X(4).                                  00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBPVRACL   COMP PIC S9(4).                            00001790
      *--                                                                       
             03 MQNBPVRACL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQNBPVRACF   PIC X.                                     00001800
             03 FILLER  PIC X(4).                                       00001810
             03 MQNBPVRACI   PIC X(5).                                  00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001840
           02 FILLER    PIC X(4).                                       00001850
           02 MZONCMDI  PIC X(15).                                      00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001880
           02 FILLER    PIC X(4).                                       00001890
           02 MLIBERRI  PIC X(58).                                      00001900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001920
           02 FILLER    PIC X(4).                                       00001930
           02 MCODTRAI  PIC X(4).                                       00001940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001960
           02 FILLER    PIC X(4).                                       00001970
           02 MCICSI    PIC X(5).                                       00001980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002000
           02 FILLER    PIC X(4).                                       00002010
           02 MNETNAMI  PIC X(8).                                       00002020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002040
           02 FILLER    PIC X(4).                                       00002050
           02 MSCREENI  PIC X(4).                                       00002060
      ***************************************************************** 00002070
      * SDF: EGD05   EGD05                                              00002080
      ***************************************************************** 00002090
       01   EGD05O REDEFINES EGD05I.                                    00002100
           02 FILLER    PIC X(12).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MDATJOUA  PIC X.                                          00002130
           02 MDATJOUC  PIC X.                                          00002140
           02 MDATJOUP  PIC X.                                          00002150
           02 MDATJOUH  PIC X.                                          00002160
           02 MDATJOUV  PIC X.                                          00002170
           02 MDATJOUO  PIC X(10).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MTIMJOUA  PIC X.                                          00002200
           02 MTIMJOUC  PIC X.                                          00002210
           02 MTIMJOUP  PIC X.                                          00002220
           02 MTIMJOUH  PIC X.                                          00002230
           02 MTIMJOUV  PIC X.                                          00002240
           02 MTIMJOUO  PIC X(5).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MPAGEA    PIC X.                                          00002270
           02 MPAGEC    PIC X.                                          00002280
           02 MPAGEP    PIC X.                                          00002290
           02 MPAGEH    PIC X.                                          00002300
           02 MPAGEV    PIC X.                                          00002310
           02 MPAGEO    PIC X(3).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MNSOCDEPOTA    PIC X.                                     00002340
           02 MNSOCDEPOTC    PIC X.                                     00002350
           02 MNSOCDEPOTP    PIC X.                                     00002360
           02 MNSOCDEPOTH    PIC X.                                     00002370
           02 MNSOCDEPOTV    PIC X.                                     00002380
           02 MNSOCDEPOTO    PIC X(3).                                  00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MNDEPOTA  PIC X.                                          00002410
           02 MNDEPOTC  PIC X.                                          00002420
           02 MNDEPOTP  PIC X.                                          00002430
           02 MNDEPOTH  PIC X.                                          00002440
           02 MNDEPOTV  PIC X.                                          00002450
           02 MNDEPOTO  PIC X(3).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MDATEDESTA     PIC X.                                     00002480
           02 MDATEDESTC     PIC X.                                     00002490
           02 MDATEDESTP     PIC X.                                     00002500
           02 MDATEDESTH     PIC X.                                     00002510
           02 MDATEDESTV     PIC X.                                     00002520
           02 MDATEDESTO     PIC X(8).                                  00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MQNBLTOTMA     PIC X.                                     00002550
           02 MQNBLTOTMC     PIC X.                                     00002560
           02 MQNBLTOTMP     PIC X.                                     00002570
           02 MQNBLTOTMH     PIC X.                                     00002580
           02 MQNBLTOTMV     PIC X.                                     00002590
           02 MQNBLTOTMO     PIC ZZZZZ.                                 00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MQNBPTOTMA     PIC X.                                     00002620
           02 MQNBPTOTMC     PIC X.                                     00002630
           02 MQNBPTOTMP     PIC X.                                     00002640
           02 MQNBPTOTMH     PIC X.                                     00002650
           02 MQNBPTOTMV     PIC X.                                     00002660
           02 MQNBPTOTMO     PIC ZZZZZZ.                                00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MQNBLSOLMA     PIC X.                                     00002690
           02 MQNBLSOLMC     PIC X.                                     00002700
           02 MQNBLSOLMP     PIC X.                                     00002710
           02 MQNBLSOLMH     PIC X.                                     00002720
           02 MQNBLSOLMV     PIC X.                                     00002730
           02 MQNBLSOLMO     PIC ZZZZZ.                                 00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MQNBPSOLMA     PIC X.                                     00002760
           02 MQNBPSOLMC     PIC X.                                     00002770
           02 MQNBPSOLMP     PIC X.                                     00002780
           02 MQNBPSOLMH     PIC X.                                     00002790
           02 MQNBPSOLMV     PIC X.                                     00002800
           02 MQNBPSOLMO     PIC ZZZZZZ.                                00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MQNBLRACKMA    PIC X.                                     00002830
           02 MQNBLRACKMC    PIC X.                                     00002840
           02 MQNBLRACKMP    PIC X.                                     00002850
           02 MQNBLRACKMH    PIC X.                                     00002860
           02 MQNBLRACKMV    PIC X.                                     00002870
           02 MQNBLRACKMO    PIC ZZZZZ.                                 00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MQNBPRACKMA    PIC X.                                     00002900
           02 MQNBPRACKMC    PIC X.                                     00002910
           02 MQNBPRACKMP    PIC X.                                     00002920
           02 MQNBPRACKMH    PIC X.                                     00002930
           02 MQNBPRACKMV    PIC X.                                     00002940
           02 MQNBPRACKMO    PIC ZZZZZZ.                                00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MQNBLTOTSA     PIC X.                                     00002970
           02 MQNBLTOTSC     PIC X.                                     00002980
           02 MQNBLTOTSP     PIC X.                                     00002990
           02 MQNBLTOTSH     PIC X.                                     00003000
           02 MQNBLTOTSV     PIC X.                                     00003010
           02 MQNBLTOTSO     PIC ZZZZZ.                                 00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MQNBPTOTSA     PIC X.                                     00003040
           02 MQNBPTOTSC     PIC X.                                     00003050
           02 MQNBPTOTSP     PIC X.                                     00003060
           02 MQNBPTOTSH     PIC X.                                     00003070
           02 MQNBPTOTSV     PIC X.                                     00003080
           02 MQNBPTOTSO     PIC ZZZZZZ.                                00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MQNBLSOLSA     PIC X.                                     00003110
           02 MQNBLSOLSC     PIC X.                                     00003120
           02 MQNBLSOLSP     PIC X.                                     00003130
           02 MQNBLSOLSH     PIC X.                                     00003140
           02 MQNBLSOLSV     PIC X.                                     00003150
           02 MQNBLSOLSO     PIC ZZZZZ.                                 00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MQNBPSOLSA     PIC X.                                     00003180
           02 MQNBPSOLSC     PIC X.                                     00003190
           02 MQNBPSOLSP     PIC X.                                     00003200
           02 MQNBPSOLSH     PIC X.                                     00003210
           02 MQNBPSOLSV     PIC X.                                     00003220
           02 MQNBPSOLSO     PIC ZZZZZZ.                                00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MQNBLRACKSA    PIC X.                                     00003250
           02 MQNBLRACKSC    PIC X.                                     00003260
           02 MQNBLRACKSP    PIC X.                                     00003270
           02 MQNBLRACKSH    PIC X.                                     00003280
           02 MQNBLRACKSV    PIC X.                                     00003290
           02 MQNBLRACKSO    PIC ZZZZZ.                                 00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MQNBPRACKSA    PIC X.                                     00003320
           02 MQNBPRACKSC    PIC X.                                     00003330
           02 MQNBPRACKSP    PIC X.                                     00003340
           02 MQNBPRACKSH    PIC X.                                     00003350
           02 MQNBPRACKSV    PIC X.                                     00003360
           02 MQNBPRACKSO    PIC ZZZZZZ.                                00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MQNBLTOTCA     PIC X.                                     00003390
           02 MQNBLTOTCC     PIC X.                                     00003400
           02 MQNBLTOTCP     PIC X.                                     00003410
           02 MQNBLTOTCH     PIC X.                                     00003420
           02 MQNBLTOTCV     PIC X.                                     00003430
           02 MQNBLTOTCO     PIC ZZZZZ.                                 00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MQNBPTOTCA     PIC X.                                     00003460
           02 MQNBPTOTCC     PIC X.                                     00003470
           02 MQNBPTOTCP     PIC X.                                     00003480
           02 MQNBPTOTCH     PIC X.                                     00003490
           02 MQNBPTOTCV     PIC X.                                     00003500
           02 MQNBPTOTCO     PIC ZZZZZZ.                                00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MQNBLSOLCA     PIC X.                                     00003530
           02 MQNBLSOLCC     PIC X.                                     00003540
           02 MQNBLSOLCP     PIC X.                                     00003550
           02 MQNBLSOLCH     PIC X.                                     00003560
           02 MQNBLSOLCV     PIC X.                                     00003570
           02 MQNBLSOLCO     PIC ZZZZZ.                                 00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MQNBPSOLCA     PIC X.                                     00003600
           02 MQNBPSOLCC     PIC X.                                     00003610
           02 MQNBPSOLCP     PIC X.                                     00003620
           02 MQNBPSOLCH     PIC X.                                     00003630
           02 MQNBPSOLCV     PIC X.                                     00003640
           02 MQNBPSOLCO     PIC ZZZZZZ.                                00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MQNBLRACKCA    PIC X.                                     00003670
           02 MQNBLRACKCC    PIC X.                                     00003680
           02 MQNBLRACKCP    PIC X.                                     00003690
           02 MQNBLRACKCH    PIC X.                                     00003700
           02 MQNBLRACKCV    PIC X.                                     00003710
           02 MQNBLRACKCO    PIC ZZZZZ.                                 00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MQNBPRACKCA    PIC X.                                     00003740
           02 MQNBPRACKCC    PIC X.                                     00003750
           02 MQNBPRACKCP    PIC X.                                     00003760
           02 MQNBPRACKCH    PIC X.                                     00003770
           02 MQNBPRACKCV    PIC X.                                     00003780
           02 MQNBPRACKCO    PIC ZZZZZZ.                                00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MWCOMMUTA      PIC X.                                     00003810
           02 MWCOMMUTC PIC X.                                          00003820
           02 MWCOMMUTP PIC X.                                          00003830
           02 MWCOMMUTH PIC X.                                          00003840
           02 MWCOMMUTV PIC X.                                          00003850
           02 MWCOMMUTO      PIC ZZZZ.                                  00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MQNBLTOTGA     PIC X.                                     00003880
           02 MQNBLTOTGC     PIC X.                                     00003890
           02 MQNBLTOTGP     PIC X.                                     00003900
           02 MQNBLTOTGH     PIC X.                                     00003910
           02 MQNBLTOTGV     PIC X.                                     00003920
           02 MQNBLTOTGO     PIC ZZZZZ.                                 00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MQNBPTOTGA     PIC X.                                     00003950
           02 MQNBPTOTGC     PIC X.                                     00003960
           02 MQNBPTOTGP     PIC X.                                     00003970
           02 MQNBPTOTGH     PIC X.                                     00003980
           02 MQNBPTOTGV     PIC X.                                     00003990
           02 MQNBPTOTGO     PIC ZZZZZZ.                                00004000
           02 FILLER    PIC X(2).                                       00004010
           02 MQNBLSOLGA     PIC X.                                     00004020
           02 MQNBLSOLGC     PIC X.                                     00004030
           02 MQNBLSOLGP     PIC X.                                     00004040
           02 MQNBLSOLGH     PIC X.                                     00004050
           02 MQNBLSOLGV     PIC X.                                     00004060
           02 MQNBLSOLGO     PIC ZZZZZ.                                 00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MQNBPSOLGA     PIC X.                                     00004090
           02 MQNBPSOLGC     PIC X.                                     00004100
           02 MQNBPSOLGP     PIC X.                                     00004110
           02 MQNBPSOLGH     PIC X.                                     00004120
           02 MQNBPSOLGV     PIC X.                                     00004130
           02 MQNBPSOLGO     PIC ZZZZZZ.                                00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MQNBLRACKGA    PIC X.                                     00004160
           02 MQNBLRACKGC    PIC X.                                     00004170
           02 MQNBLRACKGP    PIC X.                                     00004180
           02 MQNBLRACKGH    PIC X.                                     00004190
           02 MQNBLRACKGV    PIC X.                                     00004200
           02 MQNBLRACKGO    PIC ZZZZZ.                                 00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MQNBPRACKGA    PIC X.                                     00004230
           02 MQNBPRACKGC    PIC X.                                     00004240
           02 MQNBPRACKGP    PIC X.                                     00004250
           02 MQNBPRACKGH    PIC X.                                     00004260
           02 MQNBPRACKGV    PIC X.                                     00004270
           02 MQNBPRACKGO    PIC ZZZZZZ.                                00004280
           02 MTABLEAUO OCCURS   10 TIMES .                             00004290
             03 FILLER       PIC X(2).                                  00004300
             03 MNSOCA  PIC X.                                          00004310
             03 MNSOCC  PIC X.                                          00004320
             03 MNSOCP  PIC X.                                          00004330
             03 MNSOCH  PIC X.                                          00004340
             03 MNSOCV  PIC X.                                          00004350
             03 MNSOCO  PIC X(3).                                       00004360
             03 FILLER       PIC X(2).                                  00004370
             03 MNLIEUA      PIC X.                                     00004380
             03 MNLIEUC PIC X.                                          00004390
             03 MNLIEUP PIC X.                                          00004400
             03 MNLIEUH PIC X.                                          00004410
             03 MNLIEUV PIC X.                                          00004420
             03 MNLIEUO      PIC X(3).                                  00004430
             03 FILLER       PIC X(2).                                  00004440
             03 MCMODLIVRA   PIC X.                                     00004450
             03 MCMODLIVRC   PIC X.                                     00004460
             03 MCMODLIVRP   PIC X.                                     00004470
             03 MCMODLIVRH   PIC X.                                     00004480
             03 MCMODLIVRV   PIC X.                                     00004490
             03 MCMODLIVRO   PIC X(3).                                  00004500
             03 FILLER       PIC X(2).                                  00004510
             03 MEQUIPLIVA   PIC X.                                     00004520
             03 MEQUIPLIVC   PIC X.                                     00004530
             03 MEQUIPLIVP   PIC X.                                     00004540
             03 MEQUIPLIVH   PIC X.                                     00004550
             03 MEQUIPLIVV   PIC X.                                     00004560
             03 MEQUIPLIVO   PIC X(5).                                  00004570
             03 FILLER       PIC X(2).                                  00004580
             03 MLHEURELIMA  PIC X.                                     00004590
             03 MLHEURELIMC  PIC X.                                     00004600
             03 MLHEURELIMP  PIC X.                                     00004610
             03 MLHEURELIMH  PIC X.                                     00004620
             03 MLHEURELIMV  PIC X.                                     00004630
             03 MLHEURELIMO  PIC X(10).                                 00004640
             03 FILLER       PIC X(2).                                  00004650
             03 MQNBLTOTA    PIC X.                                     00004660
             03 MQNBLTOTC    PIC X.                                     00004670
             03 MQNBLTOTP    PIC X.                                     00004680
             03 MQNBLTOTH    PIC X.                                     00004690
             03 MQNBLTOTV    PIC X.                                     00004700
             03 MQNBLTOTO    PIC ZZZZ.                                  00004710
             03 FILLER       PIC X(2).                                  00004720
             03 MQNBPTOTA    PIC X.                                     00004730
             03 MQNBPTOTC    PIC X.                                     00004740
             03 MQNBPTOTP    PIC X.                                     00004750
             03 MQNBPTOTH    PIC X.                                     00004760
             03 MQNBPTOTV    PIC X.                                     00004770
             03 MQNBPTOTO    PIC ZZZZZ.                                 00004780
             03 FILLER       PIC X(2).                                  00004790
             03 MQNBLSOLA    PIC X.                                     00004800
             03 MQNBLSOLC    PIC X.                                     00004810
             03 MQNBLSOLP    PIC X.                                     00004820
             03 MQNBLSOLH    PIC X.                                     00004830
             03 MQNBLSOLV    PIC X.                                     00004840
             03 MQNBLSOLO    PIC ZZZZ.                                  00004850
             03 FILLER       PIC X(2).                                  00004860
             03 MQNBPSOLA    PIC X.                                     00004870
             03 MQNBPSOLC    PIC X.                                     00004880
             03 MQNBPSOLP    PIC X.                                     00004890
             03 MQNBPSOLH    PIC X.                                     00004900
             03 MQNBPSOLV    PIC X.                                     00004910
             03 MQNBPSOLO    PIC ZZZZZ.                                 00004920
             03 FILLER       PIC X(2).                                  00004930
             03 MQNBLRACKA   PIC X.                                     00004940
             03 MQNBLRACKC   PIC X.                                     00004950
             03 MQNBLRACKP   PIC X.                                     00004960
             03 MQNBLRACKH   PIC X.                                     00004970
             03 MQNBLRACKV   PIC X.                                     00004980
             03 MQNBLRACKO   PIC ZZZZ.                                  00004990
             03 FILLER       PIC X(2).                                  00005000
             03 MQNBPRACKA   PIC X.                                     00005010
             03 MQNBPRACKC   PIC X.                                     00005020
             03 MQNBPRACKP   PIC X.                                     00005030
             03 MQNBPRACKH   PIC X.                                     00005040
             03 MQNBPRACKV   PIC X.                                     00005050
             03 MQNBPRACKO   PIC ZZZZZ.                                 00005060
             03 FILLER       PIC X(2).                                  00005070
             03 MQNBLVRACA   PIC X.                                     00005080
             03 MQNBLVRACC   PIC X.                                     00005090
             03 MQNBLVRACP   PIC X.                                     00005100
             03 MQNBLVRACH   PIC X.                                     00005110
             03 MQNBLVRACV   PIC X.                                     00005120
             03 MQNBLVRACO   PIC ZZZZ.                                  00005130
             03 FILLER       PIC X(2).                                  00005140
             03 MQNBPVRACA   PIC X.                                     00005150
             03 MQNBPVRACC   PIC X.                                     00005160
             03 MQNBPVRACP   PIC X.                                     00005170
             03 MQNBPVRACH   PIC X.                                     00005180
             03 MQNBPVRACV   PIC X.                                     00005190
             03 MQNBPVRACO   PIC ZZZZZ.                                 00005200
           02 FILLER    PIC X(2).                                       00005210
           02 MZONCMDA  PIC X.                                          00005220
           02 MZONCMDC  PIC X.                                          00005230
           02 MZONCMDP  PIC X.                                          00005240
           02 MZONCMDH  PIC X.                                          00005250
           02 MZONCMDV  PIC X.                                          00005260
           02 MZONCMDO  PIC X(15).                                      00005270
           02 FILLER    PIC X(2).                                       00005280
           02 MLIBERRA  PIC X.                                          00005290
           02 MLIBERRC  PIC X.                                          00005300
           02 MLIBERRP  PIC X.                                          00005310
           02 MLIBERRH  PIC X.                                          00005320
           02 MLIBERRV  PIC X.                                          00005330
           02 MLIBERRO  PIC X(58).                                      00005340
           02 FILLER    PIC X(2).                                       00005350
           02 MCODTRAA  PIC X.                                          00005360
           02 MCODTRAC  PIC X.                                          00005370
           02 MCODTRAP  PIC X.                                          00005380
           02 MCODTRAH  PIC X.                                          00005390
           02 MCODTRAV  PIC X.                                          00005400
           02 MCODTRAO  PIC X(4).                                       00005410
           02 FILLER    PIC X(2).                                       00005420
           02 MCICSA    PIC X.                                          00005430
           02 MCICSC    PIC X.                                          00005440
           02 MCICSP    PIC X.                                          00005450
           02 MCICSH    PIC X.                                          00005460
           02 MCICSV    PIC X.                                          00005470
           02 MCICSO    PIC X(5).                                       00005480
           02 FILLER    PIC X(2).                                       00005490
           02 MNETNAMA  PIC X.                                          00005500
           02 MNETNAMC  PIC X.                                          00005510
           02 MNETNAMP  PIC X.                                          00005520
           02 MNETNAMH  PIC X.                                          00005530
           02 MNETNAMV  PIC X.                                          00005540
           02 MNETNAMO  PIC X(8).                                       00005550
           02 FILLER    PIC X(2).                                       00005560
           02 MSCREENA  PIC X.                                          00005570
           02 MSCREENC  PIC X.                                          00005580
           02 MSCREENP  PIC X.                                          00005590
           02 MSCREENH  PIC X.                                          00005600
           02 MSCREENV  PIC X.                                          00005610
           02 MSCREENO  PIC X(4).                                       00005620
                                                                                
