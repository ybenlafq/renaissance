      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGB5502                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB5502                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB5502.                                                            
           02  GB55-NSOCENTR                                                    
               PIC X(0003).                                                     
           02  GB55-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GB55-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GB55-NLIEU                                                       
               PIC X(0003).                                                     
           02  GB55-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GB55-DDEBSAIS                                                    
               PIC X(0008).                                                     
           02  GB55-DFINSAIS                                                    
               PIC X(0008).                                                     
           02  GB55-DDESTOCK                                                    
               PIC X(0008).                                                     
           02  GB55-DMUTATION                                                   
               PIC X(0008).                                                     
           02  GB55-CSELART                                                     
               PIC X(0005).                                                     
           02  GB55-DHEURMUT                                                    
               PIC X(0002).                                                     
           02  GB55-DMINUMUT                                                    
               PIC X(0002).                                                     
           02  GB55-QNBCAMIONS                                                  
               PIC S9(2) COMP-3.                                                
           02  GB55-QNBM3QUOTA                                                  
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GB55-QNBPQUOTA                                                   
               PIC S9(5) COMP-3.                                                
           02  GB55-QVOLUME                                                     
               PIC S9(11) COMP-3.                                               
           02  GB55-QNBPIECES                                                   
               PIC S9(5) COMP-3.                                                
           02  GB55-QNBLIGNES                                                   
               PIC S9(5) COMP-3.                                                
           02  GB55-QNBPLANCE                                                   
               PIC S9(5) COMP-3.                                                
           02  GB55-QNBLLANCE                                                   
               PIC S9(5) COMP-3.                                                
           02  GB55-WPROPERMIS                                                  
               PIC X(0001).                                                     
           02  GB55-WVAL                                                        
               PIC X(0001).                                                     
           02  GB55-LHEURLIMIT                                                  
               PIC X(0010).                                                     
           02  GB55-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GB55-DVALID                                                      
               PIC X(0008).                                                     
           02  GB55-QNBM3PROPOS                                                 
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GB55-QNBPPROPOS                                                  
               PIC S9(5) COMP-3.                                                
           02  GB55-DCHARGT                                                     
               PIC X(0008).                                                     
           02  GB55-HCHARGT                                                     
               PIC X(0005).                                                     
           02  GB55-NLIGTRANS                                                   
               PIC X(0006).                                                     
           02  GB55-NSEQUENCE                                                   
               PIC X(0002).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGB5502                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGB5502-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NSOCENTR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-NSOCENTR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DDEBSAIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-DDEBSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DFINSAIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-DFINSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DDESTOCK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-DDESTOCK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-DMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-CSELART-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-CSELART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DHEURMUT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-DHEURMUT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DMINUMUT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-DMINUMUT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBCAMIONS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-QNBCAMIONS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBM3QUOTA-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-QNBM3QUOTA-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBPQUOTA-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-QNBPQUOTA-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QVOLUME-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-QVOLUME-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBPIECES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-QNBPIECES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBLIGNES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-QNBLIGNES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBPLANCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-QNBPLANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBLLANCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-QNBLLANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-WPROPERMIS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-WPROPERMIS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-WVAL-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-WVAL-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-LHEURLIMIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-LHEURLIMIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DVALID-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-DVALID-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBM3PROPOS-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-QNBM3PROPOS-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-QNBPPROPOS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-QNBPPROPOS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-DCHARGT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-DCHARGT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-HCHARGT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-HCHARGT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NLIGTRANS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-NLIGTRANS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB55-NSEQUENCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB55-NSEQUENCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
