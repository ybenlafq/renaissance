      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVGS7000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS7000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS7000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS7000.                                                            
      *}                                                                        
           02  GS70-NLIEUENT                                                    
               PIC X(0003).                                                     
           02  GS70-NHS                                                         
               PIC X(0007).                                                     
           02  GS70-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GS70-NLIEU                                                       
               PIC X(0003).                                                     
           02  GS70-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  GS70-DMVT                                                        
               PIC X(0008).                                                     
           02  GS70-CLIEUTRT                                                    
               PIC X(0005).                                                     
           02  GS70-CSTATUTTRT                                                  
               PIC X(0005).                                                     
           02  GS70-LEMPLACT                                                    
               PIC X(0005).                                                     
           02  GS70-LCOMMENTTRT                                                 
               PIC X(0020).                                                     
           02  GS70-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGS7000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS7000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS7000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS70-NLIEUENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS70-NLIEUENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS70-NHS-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS70-NHS-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS70-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS70-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS70-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS70-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS70-NSSLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS70-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS70-DMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS70-DMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS70-CLIEUTRT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS70-CLIEUTRT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS70-CSTATUTTRT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS70-CSTATUTTRT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS70-LEMPLACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS70-LEMPLACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS70-LCOMMENTTRT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS70-LCOMMENTTRT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS70-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS70-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
