      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * TS SPECIFIQUE TGL06                                        *            
      *       TR : GL00  GESTION DES LIVRAISONS                                 
      *       PG : TGL06 PLANNING DES LIVRAISONS                   *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES -----------------------------              
      *                                                                         
       01  TS-LONG            PIC S9(4) COMP-3 VALUE 306.                       
       01  TS-DONNEES.                                                          
           05 TS-LIGNE   OCCURS 2.                                              
              10 TS-QUOTA           PIC X(5).                                   
              10 TS-LQUOTA          PIC X(20).                                  
              10 TS-MONTANT         OCCURS 8.                                   
                 15 TS-QTOT           PIC S9(7) COMP-3.                         
                 15 TS-QDISP          PIC S9(7) COMP-3.                         
                 15 TS-QPRIS          PIC S9(7) COMP-3.                         
                 15 TS-NBP            PIC S9(7) COMP-3.                         
                                                                                
