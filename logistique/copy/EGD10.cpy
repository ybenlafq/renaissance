      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD10   EGD10                                              00000020
      ***************************************************************** 00000030
       01   EGD10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSOCIETEI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTREPOTL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MENTREPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MENTREPOTF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MENTREPOTI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURNEEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MJOURNEEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MJOURNEEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MJOURNEEI      PIC X(8).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVOLMINIL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MVOLMINIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MVOLMINIF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MVOLMINII      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVOLMAXIL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MVOLMAXIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MVOLMAXIF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MVOLMAXII      PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRAFALEL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNRAFALEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNRAFALEF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNRAFALEI      PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPDSMINIL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MPDSMINIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPDSMINIF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPDSMINII      PIC X(5).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPDSMAXIL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MPDSMAXIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPDSMAXIF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MPDSMAXII      PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLANCEHHL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLANCEHHL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLANCEHHF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLANCEHHI      PIC X(2).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLANCEMML      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLANCEMML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLANCEMMF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLANCEMMI      PIC X(2).                                  00000570
           02 M172I OCCURS   12 TIMES .                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCL   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSOCF   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MSOCI   PIC X(3).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIEUF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLIEUI  PIC X(3).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLIBELLEI    PIC X(20).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELARTL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MSELARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELARTF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSELARTI     PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHEURELIML   COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MHEURELIML COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MHEURELIMF   PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MHEURELIMI   PIC X(10).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNEDLL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MLIGNEDLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIGNEDLF    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLIGNEDLI    PIC X(4).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPIECEDLL    COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MPIECEDLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPIECEDLF    PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MPIECEDLI    PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNEALL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MLIGNEALL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIGNEALF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MLIGNEALI    PIC X(4).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPIECEALL    COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MPIECEALL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPIECEALF    PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MPIECEALI    PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MSELI   PIC X.                                          00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSATELITL    COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MSATELITL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSATELITF    PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MSATELITI    PIC X(2).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCASEL  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MCASEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCASEF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MCASEI  PIC X(3).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MZONCMDI  PIC X(15).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MLIBERRI  PIC X(58).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCODTRAI  PIC X(4).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCICSI    PIC X(5).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MNETNAMI  PIC X(8).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MSCREENI  PIC X(4).                                       00001300
      ***************************************************************** 00001310
      * SDF: EGD10   EGD10                                              00001320
      ***************************************************************** 00001330
       01   EGD10O REDEFINES EGD10I.                                    00001340
           02 FILLER    PIC X(12).                                      00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATJOUA  PIC X.                                          00001370
           02 MDATJOUC  PIC X.                                          00001380
           02 MDATJOUP  PIC X.                                          00001390
           02 MDATJOUH  PIC X.                                          00001400
           02 MDATJOUV  PIC X.                                          00001410
           02 MDATJOUO  PIC X(10).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MTIMJOUA  PIC X.                                          00001440
           02 MTIMJOUC  PIC X.                                          00001450
           02 MTIMJOUP  PIC X.                                          00001460
           02 MTIMJOUH  PIC X.                                          00001470
           02 MTIMJOUV  PIC X.                                          00001480
           02 MTIMJOUO  PIC X(5).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MPAGEA    PIC X.                                          00001510
           02 MPAGEC    PIC X.                                          00001520
           02 MPAGEP    PIC X.                                          00001530
           02 MPAGEH    PIC X.                                          00001540
           02 MPAGEV    PIC X.                                          00001550
           02 MPAGEO    PIC X(3).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MSOCIETEA      PIC X.                                     00001580
           02 MSOCIETEC PIC X.                                          00001590
           02 MSOCIETEP PIC X.                                          00001600
           02 MSOCIETEH PIC X.                                          00001610
           02 MSOCIETEV PIC X.                                          00001620
           02 MSOCIETEO      PIC X(3).                                  00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MENTREPOTA     PIC X.                                     00001650
           02 MENTREPOTC     PIC X.                                     00001660
           02 MENTREPOTP     PIC X.                                     00001670
           02 MENTREPOTH     PIC X.                                     00001680
           02 MENTREPOTV     PIC X.                                     00001690
           02 MENTREPOTO     PIC X(3).                                  00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MJOURNEEA      PIC X.                                     00001720
           02 MJOURNEEC PIC X.                                          00001730
           02 MJOURNEEP PIC X.                                          00001740
           02 MJOURNEEH PIC X.                                          00001750
           02 MJOURNEEV PIC X.                                          00001760
           02 MJOURNEEO      PIC X(8).                                  00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MVOLMINIA      PIC X.                                     00001790
           02 MVOLMINIC PIC X.                                          00001800
           02 MVOLMINIP PIC X.                                          00001810
           02 MVOLMINIH PIC X.                                          00001820
           02 MVOLMINIV PIC X.                                          00001830
           02 MVOLMINIO      PIC ZZZZZ.                                 00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MVOLMAXIA      PIC X.                                     00001860
           02 MVOLMAXIC PIC X.                                          00001870
           02 MVOLMAXIP PIC X.                                          00001880
           02 MVOLMAXIH PIC X.                                          00001890
           02 MVOLMAXIV PIC X.                                          00001900
           02 MVOLMAXIO      PIC ZZZZZ.                                 00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNRAFALEA      PIC X.                                     00001930
           02 MNRAFALEC PIC X.                                          00001940
           02 MNRAFALEP PIC X.                                          00001950
           02 MNRAFALEH PIC X.                                          00001960
           02 MNRAFALEV PIC X.                                          00001970
           02 MNRAFALEO      PIC X(3).                                  00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MPDSMINIA      PIC X.                                     00002000
           02 MPDSMINIC PIC X.                                          00002010
           02 MPDSMINIP PIC X.                                          00002020
           02 MPDSMINIH PIC X.                                          00002030
           02 MPDSMINIV PIC X.                                          00002040
           02 MPDSMINIO      PIC ZZZZZ.                                 00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MPDSMAXIA      PIC X.                                     00002070
           02 MPDSMAXIC PIC X.                                          00002080
           02 MPDSMAXIP PIC X.                                          00002090
           02 MPDSMAXIH PIC X.                                          00002100
           02 MPDSMAXIV PIC X.                                          00002110
           02 MPDSMAXIO      PIC ZZZZZ.                                 00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLANCEHHA      PIC X.                                     00002140
           02 MLANCEHHC PIC X.                                          00002150
           02 MLANCEHHP PIC X.                                          00002160
           02 MLANCEHHH PIC X.                                          00002170
           02 MLANCEHHV PIC X.                                          00002180
           02 MLANCEHHO      PIC X(2).                                  00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLANCEMMA      PIC X.                                     00002210
           02 MLANCEMMC PIC X.                                          00002220
           02 MLANCEMMP PIC X.                                          00002230
           02 MLANCEMMH PIC X.                                          00002240
           02 MLANCEMMV PIC X.                                          00002250
           02 MLANCEMMO      PIC X(2).                                  00002260
           02 M172O OCCURS   12 TIMES .                                 00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MSOCA   PIC X.                                          00002290
             03 MSOCC   PIC X.                                          00002300
             03 MSOCP   PIC X.                                          00002310
             03 MSOCH   PIC X.                                          00002320
             03 MSOCV   PIC X.                                          00002330
             03 MSOCO   PIC X(3).                                       00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MLIEUA  PIC X.                                          00002360
             03 MLIEUC  PIC X.                                          00002370
             03 MLIEUP  PIC X.                                          00002380
             03 MLIEUH  PIC X.                                          00002390
             03 MLIEUV  PIC X.                                          00002400
             03 MLIEUO  PIC X(3).                                       00002410
             03 FILLER       PIC X(2).                                  00002420
             03 MLIBELLEA    PIC X.                                     00002430
             03 MLIBELLEC    PIC X.                                     00002440
             03 MLIBELLEP    PIC X.                                     00002450
             03 MLIBELLEH    PIC X.                                     00002460
             03 MLIBELLEV    PIC X.                                     00002470
             03 MLIBELLEO    PIC X(20).                                 00002480
             03 FILLER       PIC X(2).                                  00002490
             03 MSELARTA     PIC X.                                     00002500
             03 MSELARTC     PIC X.                                     00002510
             03 MSELARTP     PIC X.                                     00002520
             03 MSELARTH     PIC X.                                     00002530
             03 MSELARTV     PIC X.                                     00002540
             03 MSELARTO     PIC X(5).                                  00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MHEURELIMA   PIC X.                                     00002570
             03 MHEURELIMC   PIC X.                                     00002580
             03 MHEURELIMP   PIC X.                                     00002590
             03 MHEURELIMH   PIC X.                                     00002600
             03 MHEURELIMV   PIC X.                                     00002610
             03 MHEURELIMO   PIC X(10).                                 00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MLIGNEDLA    PIC X.                                     00002640
             03 MLIGNEDLC    PIC X.                                     00002650
             03 MLIGNEDLP    PIC X.                                     00002660
             03 MLIGNEDLH    PIC X.                                     00002670
             03 MLIGNEDLV    PIC X.                                     00002680
             03 MLIGNEDLO    PIC X(4).                                  00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MPIECEDLA    PIC X.                                     00002710
             03 MPIECEDLC    PIC X.                                     00002720
             03 MPIECEDLP    PIC X.                                     00002730
             03 MPIECEDLH    PIC X.                                     00002740
             03 MPIECEDLV    PIC X.                                     00002750
             03 MPIECEDLO    PIC X(5).                                  00002760
             03 FILLER       PIC X(2).                                  00002770
             03 MLIGNEALA    PIC X.                                     00002780
             03 MLIGNEALC    PIC X.                                     00002790
             03 MLIGNEALP    PIC X.                                     00002800
             03 MLIGNEALH    PIC X.                                     00002810
             03 MLIGNEALV    PIC X.                                     00002820
             03 MLIGNEALO    PIC X(4).                                  00002830
             03 FILLER       PIC X(2).                                  00002840
             03 MPIECEALA    PIC X.                                     00002850
             03 MPIECEALC    PIC X.                                     00002860
             03 MPIECEALP    PIC X.                                     00002870
             03 MPIECEALH    PIC X.                                     00002880
             03 MPIECEALV    PIC X.                                     00002890
             03 MPIECEALO    PIC X(5).                                  00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MSELA   PIC X.                                          00002920
             03 MSELC   PIC X.                                          00002930
             03 MSELP   PIC X.                                          00002940
             03 MSELH   PIC X.                                          00002950
             03 MSELV   PIC X.                                          00002960
             03 MSELO   PIC X.                                          00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MSATELITA    PIC X.                                     00002990
             03 MSATELITC    PIC X.                                     00003000
             03 MSATELITP    PIC X.                                     00003010
             03 MSATELITH    PIC X.                                     00003020
             03 MSATELITV    PIC X.                                     00003030
             03 MSATELITO    PIC X(2).                                  00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MCASEA  PIC X.                                          00003060
             03 MCASEC  PIC X.                                          00003070
             03 MCASEP  PIC X.                                          00003080
             03 MCASEH  PIC X.                                          00003090
             03 MCASEV  PIC X.                                          00003100
             03 MCASEO  PIC X(3).                                       00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MZONCMDA  PIC X.                                          00003130
           02 MZONCMDC  PIC X.                                          00003140
           02 MZONCMDP  PIC X.                                          00003150
           02 MZONCMDH  PIC X.                                          00003160
           02 MZONCMDV  PIC X.                                          00003170
           02 MZONCMDO  PIC X(15).                                      00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MLIBERRA  PIC X.                                          00003200
           02 MLIBERRC  PIC X.                                          00003210
           02 MLIBERRP  PIC X.                                          00003220
           02 MLIBERRH  PIC X.                                          00003230
           02 MLIBERRV  PIC X.                                          00003240
           02 MLIBERRO  PIC X(58).                                      00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MCODTRAA  PIC X.                                          00003270
           02 MCODTRAC  PIC X.                                          00003280
           02 MCODTRAP  PIC X.                                          00003290
           02 MCODTRAH  PIC X.                                          00003300
           02 MCODTRAV  PIC X.                                          00003310
           02 MCODTRAO  PIC X(4).                                       00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MCICSA    PIC X.                                          00003340
           02 MCICSC    PIC X.                                          00003350
           02 MCICSP    PIC X.                                          00003360
           02 MCICSH    PIC X.                                          00003370
           02 MCICSV    PIC X.                                          00003380
           02 MCICSO    PIC X(5).                                       00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MNETNAMA  PIC X.                                          00003410
           02 MNETNAMC  PIC X.                                          00003420
           02 MNETNAMP  PIC X.                                          00003430
           02 MNETNAMH  PIC X.                                          00003440
           02 MNETNAMV  PIC X.                                          00003450
           02 MNETNAMO  PIC X(8).                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MSCREENA  PIC X.                                          00003480
           02 MSCREENC  PIC X.                                          00003490
           02 MSCREENP  PIC X.                                          00003500
           02 MSCREENH  PIC X.                                          00003510
           02 MSCREENV  PIC X.                                          00003520
           02 MSCREENO  PIC X(4).                                       00003530
                                                                                
