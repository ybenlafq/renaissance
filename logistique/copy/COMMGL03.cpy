      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGL03                             *            
      *       TR : GL00  GESTION DES LIVRAISON                     *            
      *       PG : TGL03 PROGRAMMATION DES LIVRAISONS              *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                                         
      *                                                                         
          02 COMM-GL03-APPLI REDEFINES COMM-GL00-APPLI.                         
      *---------                                                                
             03 COMM-GL03-NSOC               PIC X(3).                          
      *---------                                                                
             03 COMM-GL03-LSOC               PIC X(20).                         
      *---------                                                                
             03 COMM-GL03-NDEPOT             PIC X(3).                          
      *---------                                                                
             03 COMM-GL03-LDEPOT             PIC X(20).                         
      *---------                                                                
             03 COMM-GL03-LIVREUR            PIC X(5).                          
      *---------                                                                
             03 COMM-GL03-LLIVREUR           PIC X(20).                         
      *---------                                                                
             03 COMM-GL03-CREC               PIC X(5).                          
      *---------                                                                
             03 COMM-GL03-LREC               PIC X(20).                         
      *---------                                                                
             03 COMM-GL03-PAGE               PIC 9(3).                          
             03 COMM-GL03-PAGEMAX            PIC 9(3).                          
      *---------                                                                
      *                                                                         
      *****************************************************************         
                                                                                
