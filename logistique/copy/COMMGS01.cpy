      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00414101
      * COMMAREA SPECIFIQUE PRG TGS01 (TGS00 -> MENU)    TR: GS00  *    00414201
      *               GESTION DES STOCKS ENTREPOT                  *    00414301
      *                                                                 00414401
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3568  00414501
      *                                                            *    00414601
      *        TRANSACTION GS01 : CONSULTATION DES STOCKS          *    00414701
      *                                                                 00414801
          02 COMM-GS01-APPLI REDEFINES COMM-GS00-APPLI.                 00414901
      *------------------------------ ZONE DONNEES TGS01                00415001
             03 COMM-GS01-DONNEES-TGS01.                                00416001
      *------------------------------ LIBELLE STATUT ASSORTIMENT        00417001
                04 COMM-GS01-LSTATASSOR        PIC X(20).               00418001
      *------------------------------ LIBELLE STATUT APPROVISIONNEMENT  00419001
                04 COMM-GS01-LSTATAPPRO        PIC X(20).               00420001
      *------------------------------ LIBELLE STATUT EXPOSITION         00430001
                04 COMM-GS01-LSTATEXPO         PIC X(20).               00440001
      *------------------------------ N� PAGE COURANT                   00740303
                04 COMM-GS01-PAGE            PIC 9(5).                  00740403
      *------------------------------ N� PAGE MAXI                      00740503
                04 COMM-GS01-PAGE-MAX        PIC 9(5).                  00740603
      *------------------------------ RUPTURE-FAMILLE                   00741502
                04 COMM-GS01-FAMILLE           PIC X(05).               00741602
      *------------------------------ FAMILLE EN COURS                  00741704
                04 COMM-GS01-FAM-COURANTE      PIC X(05).               00741804
      *------------------------------ PAGINATION FAMILLE MAX            00756104
                04 COMM-GS17-PAGE-MAX          PIC 9(03).               00756105
      *------------------------------ PAGINATION FAMILLE EN COURS       00756106
                04 COMM-GS17-PAGE              PIC 9(03).               00756107
      *------------------------------ ZONES ATTRIBUTS POUR SWAP         00756108
             03 COMM-GS01-ZONSWAP.                                      00757004
                04 COMM-GS01-ATTR              PIC 9 OCCURS 500.        00760004
      ***************************************************************** 02180001
                                                                                
