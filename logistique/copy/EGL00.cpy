      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGL00   EGL00                                              00000020
      ***************************************************************** 00000030
       01   EGL00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MFONCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRECL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCRECF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCRECI    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRECL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLRECF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLRECI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNSOCI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNDEPOTI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCDEL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MZONCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCDEF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MZONCDEI  PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIVRL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIVRF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNLIVRI   PIC X(7).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCODL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MZONCODL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCODF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MZONCODI  PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNCDEI    PIC X(7).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSEM1L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDSEM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDSEM1F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDSEM1I   PIC X(6).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJOUR1L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MDJOUR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDJOUR1F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDJOUR1I  PIC X(10).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIVRL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIVRF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCLIVRI   PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSEM2L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDSEM2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDSEM2F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDSEM2I   PIC X(6).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJOUR2L  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDJOUR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDJOUR2F  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDJOUR2I  PIC X(10).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBERRI  PIC X(78).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCODTRAI  PIC X(4).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCICSI    PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MNETNAMI  PIC X(8).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MSCREENI  PIC X(4).                                       00000930
      ***************************************************************** 00000940
      * SDF: EGL00   EGL00                                              00000950
      ***************************************************************** 00000960
       01   EGL00O REDEFINES EGL00I.                                    00000970
           02 FILLER    PIC X(12).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MDATJOUA  PIC X.                                          00001000
           02 MDATJOUC  PIC X.                                          00001010
           02 MDATJOUP  PIC X.                                          00001020
           02 MDATJOUH  PIC X.                                          00001030
           02 MDATJOUV  PIC X.                                          00001040
           02 MDATJOUO  PIC X(10).                                      00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MTIMJOUA  PIC X.                                          00001070
           02 MTIMJOUC  PIC X.                                          00001080
           02 MTIMJOUP  PIC X.                                          00001090
           02 MTIMJOUH  PIC X.                                          00001100
           02 MTIMJOUV  PIC X.                                          00001110
           02 MTIMJOUO  PIC X(5).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MZONCMDA  PIC X.                                          00001140
           02 MZONCMDC  PIC X.                                          00001150
           02 MZONCMDP  PIC X.                                          00001160
           02 MZONCMDH  PIC X.                                          00001170
           02 MZONCMDV  PIC X.                                          00001180
           02 MZONCMDO  PIC X(15).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MFONCA    PIC X.                                          00001210
           02 MFONCC    PIC X.                                          00001220
           02 MFONCP    PIC X.                                          00001230
           02 MFONCH    PIC X.                                          00001240
           02 MFONCV    PIC X.                                          00001250
           02 MFONCO    PIC X(3).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MCRECA    PIC X.                                          00001280
           02 MCRECC    PIC X.                                          00001290
           02 MCRECP    PIC X.                                          00001300
           02 MCRECH    PIC X.                                          00001310
           02 MCRECV    PIC X.                                          00001320
           02 MCRECO    PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MLRECA    PIC X.                                          00001350
           02 MLRECC    PIC X.                                          00001360
           02 MLRECP    PIC X.                                          00001370
           02 MLRECH    PIC X.                                          00001380
           02 MLRECV    PIC X.                                          00001390
           02 MLRECO    PIC X(20).                                      00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNSOCA    PIC X.                                          00001420
           02 MNSOCC    PIC X.                                          00001430
           02 MNSOCP    PIC X.                                          00001440
           02 MNSOCH    PIC X.                                          00001450
           02 MNSOCV    PIC X.                                          00001460
           02 MNSOCO    PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNDEPOTA  PIC X.                                          00001490
           02 MNDEPOTC  PIC X.                                          00001500
           02 MNDEPOTP  PIC X.                                          00001510
           02 MNDEPOTH  PIC X.                                          00001520
           02 MNDEPOTV  PIC X.                                          00001530
           02 MNDEPOTO  PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MZONCDEA  PIC X.                                          00001560
           02 MZONCDEC  PIC X.                                          00001570
           02 MZONCDEP  PIC X.                                          00001580
           02 MZONCDEH  PIC X.                                          00001590
           02 MZONCDEV  PIC X.                                          00001600
           02 MZONCDEO  PIC X.                                          00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNLIVRA   PIC X.                                          00001630
           02 MNLIVRC   PIC X.                                          00001640
           02 MNLIVRP   PIC X.                                          00001650
           02 MNLIVRH   PIC X.                                          00001660
           02 MNLIVRV   PIC X.                                          00001670
           02 MNLIVRO   PIC X(7).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MZONCODA  PIC X.                                          00001700
           02 MZONCODC  PIC X.                                          00001710
           02 MZONCODP  PIC X.                                          00001720
           02 MZONCODH  PIC X.                                          00001730
           02 MZONCODV  PIC X.                                          00001740
           02 MZONCODO  PIC X.                                          00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNCDEA    PIC X.                                          00001770
           02 MNCDEC    PIC X.                                          00001780
           02 MNCDEP    PIC X.                                          00001790
           02 MNCDEH    PIC X.                                          00001800
           02 MNCDEV    PIC X.                                          00001810
           02 MNCDEO    PIC X(7).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MDSEM1A   PIC X.                                          00001840
           02 MDSEM1C   PIC X.                                          00001850
           02 MDSEM1P   PIC X.                                          00001860
           02 MDSEM1H   PIC X.                                          00001870
           02 MDSEM1V   PIC X.                                          00001880
           02 MDSEM1O   PIC X(6).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MDJOUR1A  PIC X.                                          00001910
           02 MDJOUR1C  PIC X.                                          00001920
           02 MDJOUR1P  PIC X.                                          00001930
           02 MDJOUR1H  PIC X.                                          00001940
           02 MDJOUR1V  PIC X.                                          00001950
           02 MDJOUR1O  PIC X(10).                                      00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MCLIVRA   PIC X.                                          00001980
           02 MCLIVRC   PIC X.                                          00001990
           02 MCLIVRP   PIC X.                                          00002000
           02 MCLIVRH   PIC X.                                          00002010
           02 MCLIVRV   PIC X.                                          00002020
           02 MCLIVRO   PIC X(5).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MDSEM2A   PIC X.                                          00002050
           02 MDSEM2C   PIC X.                                          00002060
           02 MDSEM2P   PIC X.                                          00002070
           02 MDSEM2H   PIC X.                                          00002080
           02 MDSEM2V   PIC X.                                          00002090
           02 MDSEM2O   PIC X(6).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MDJOUR2A  PIC X.                                          00002120
           02 MDJOUR2C  PIC X.                                          00002130
           02 MDJOUR2P  PIC X.                                          00002140
           02 MDJOUR2H  PIC X.                                          00002150
           02 MDJOUR2V  PIC X.                                          00002160
           02 MDJOUR2O  PIC X(10).                                      00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLIBERRA  PIC X.                                          00002190
           02 MLIBERRC  PIC X.                                          00002200
           02 MLIBERRP  PIC X.                                          00002210
           02 MLIBERRH  PIC X.                                          00002220
           02 MLIBERRV  PIC X.                                          00002230
           02 MLIBERRO  PIC X(78).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCODTRAA  PIC X.                                          00002260
           02 MCODTRAC  PIC X.                                          00002270
           02 MCODTRAP  PIC X.                                          00002280
           02 MCODTRAH  PIC X.                                          00002290
           02 MCODTRAV  PIC X.                                          00002300
           02 MCODTRAO  PIC X(4).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCICSA    PIC X.                                          00002330
           02 MCICSC    PIC X.                                          00002340
           02 MCICSP    PIC X.                                          00002350
           02 MCICSH    PIC X.                                          00002360
           02 MCICSV    PIC X.                                          00002370
           02 MCICSO    PIC X(5).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MNETNAMA  PIC X.                                          00002400
           02 MNETNAMC  PIC X.                                          00002410
           02 MNETNAMP  PIC X.                                          00002420
           02 MNETNAMH  PIC X.                                          00002430
           02 MNETNAMV  PIC X.                                          00002440
           02 MNETNAMO  PIC X(8).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MSCREENA  PIC X.                                          00002470
           02 MSCREENC  PIC X.                                          00002480
           02 MSCREENP  PIC X.                                          00002490
           02 MSCREENH  PIC X.                                          00002500
           02 MSCREENV  PIC X.                                          00002510
           02 MSCREENO  PIC X(4).                                       00002520
                                                                                
