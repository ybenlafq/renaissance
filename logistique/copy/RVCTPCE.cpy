      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CTPCE TRANSCO TYPE DE PIECE SAP        *        
      *----------------------------------------------------------------*        
       01  RVCTPCE.                                                             
           05  CTPCE-CTABLEG2    PIC X(15).                                     
           05  CTPCE-CTABLEG2-REDEF REDEFINES CTPCE-CTABLEG2.                   
               10  CTPCE-CINTER          PIC X(05).                             
               10  CTPCE-NATURE          PIC X(03).                             
           05  CTPCE-WTABLEG     PIC X(80).                                     
           05  CTPCE-WTABLEG-REDEF  REDEFINES CTPCE-WTABLEG.                    
               10  CTPCE-WACTIF          PIC X(01).                             
               10  CTPCE-TYPPCSAP        PIC X(02).                             
               10  CTPCE-LIBPCSAP        PIC X(20).                             
               10  CTPCE-NATTVA          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCTPCE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTPCE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CTPCE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTPCE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CTPCE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
