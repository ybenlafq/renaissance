      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CTRAT CODE CONTRAT INNOVENTE           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCTRAT.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCTRAT.                                                             
      *}                                                                        
           05  CTRAT-CTABLEG2    PIC X(15).                                     
           05  CTRAT-CTABLEG2-REDEF REDEFINES CTRAT-CTABLEG2.                   
               10  CTRAT-CCONTRAT        PIC X(05).                             
           05  CTRAT-WTABLEG     PIC X(80).                                     
           05  CTRAT-WTABLEG-REDEF  REDEFINES CTRAT-WTABLEG.                    
               10  CTRAT-LCONTRAT        PIC X(20).                             
               10  CTRAT-WACTIF          PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCTRAT-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCTRAT-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTRAT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CTRAT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTRAT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CTRAT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
