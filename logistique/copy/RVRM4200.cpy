      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM4200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM4200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM4200.                                                            
           02  RM42-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RM42-NLIEU                                                       
               PIC X(0003).                                                     
           02  RM42-CFAM                                                        
               PIC X(0005).                                                     
           02  RM42-DEFFET                                                      
               PIC X(0008).                                                     
           02  RM42-CTYPREG                                                     
               PIC X(0002).                                                     
           02  RM42-CACID                                                       
               PIC X(0008).                                                     
           02  RM42-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM4200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM4200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM42-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM42-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM42-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM42-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM42-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM42-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM42-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM42-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM42-CTYPREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM42-CTYPREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM42-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM42-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM42-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM42-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
