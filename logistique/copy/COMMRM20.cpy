      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * COM-RM20-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
YS1194*01  COM-RM20-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
YS1194*01  COM-RM20-LONG-COMMAREA PIC S9(4) COMP VALUE +8438.                   
      *--                                                                       
       01  COM-RM20-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +8438.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA                                                  
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS                                   
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION                     
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC XX.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS PIC 99.                                        
              05 COMM-DATE-SEMAA PIC 99.                                        
              05 COMM-DATE-SEMNU PIC 99.                                        
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES APPLICATIVES                                            
      *                                                                         
      *                                                                         
           02  COMM-RM20-APPLI.                                                 
      *=> ZONES DE SAUVEGARDE COMMUNES AU MENU PRINCIPAL                        
      *=> ET AUX TRANSACTIONS DE NIVEAU INFERIEUR                               
              03  COMM-RM20.                                                    
                  05  COMM-RM20-MZONCMD      PIC X(15).                         
                  05  COMM-RM20-MCGROUP      PIC X(5).                          
                  05  COMM-RM20-MCHEFP       PIC X(5).                          
                  05  COMM-RM20-MLCHEFP      PIC X(20).                         
                  05  COMM-RM20-MCFAM        PIC X(5).                          
                  05  COMM-RM20-MLFAM        PIC X(20).                         
                  05  COMM-RM20-MLAGREG      PIC X(20).                         
                  05  COMM-RM20-MDSEM        PIC 9(6).                          
                  05  COMM-RM20-MNCODIC      PIC 9(7).                          
                  05  COMM-RM20-MNLIEU       PIC X(3).                          
                  05  COMM-RM20-NSOCIETE     PIC X(3).                          
                  05  COMM-RM20-MNSOC        PIC X(3).                          
                  05  COMM-RM20-TRAIT-ORI    PIC X(5).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           05  COMM-RM20-PAGE-ORI     PIC S9(2) COMP.                    
      *--                                                                       
                  05  COMM-RM20-PAGE-ORI     PIC S9(2) COMP-5.                  
      *}                                                                        
                  05  COMM-RM20-TYPE-SIMU    PIC X.                             
                  05  COMM-RM20-C-SIMU       PIC X(7).                          
                  05  COMM-RM20-S-SIMU       PIC X(20).                         
                  05  COMM-RM20-F-SIMU       PIC X(5).                          
                  05  COMM-RM20-TAB-SOC.                                00020000
SR                    10 COMM-RM20-OCC-SOC   PIC X(3) OCCURS 5.         00030000
SR                    10 COMM-RM20-OCC-DEP   PIC X(3) OCCURS 5.         00030000
                  05 COMM-RM20-CACID         PIC X(4).                  00050005
SR                05 COMM-RM20-NSOCCICS      PIC X(3).                  00040000
SR                05 COMM-RM20-CFLGMAG       PIC X(5).                  00050005
SR                05 COMM-RM20-NSOC          PIC X(3).                          
SR                05 COMM-RM20-NDEPOT        PIC X(3).                          
                  05 COMM-RM20-NBSOC         PIC S9(3) COMP-3.          00040000
SR                05 COMM-RM20-NBDEP         PIC S9(3) COMP-3.          00040000
      *=> ZONES DE SAUVEGARDE PROPRES A UNE TRANSACTION ACTIVE                  
      *=> DE NIVEAU INFERIEUR                                                   
SR    *    02  COMM-RM21-RM22-RM23           PIC X(3688).                       
SR    *    02  COMM-RM21-RM22-RM23           PIC X(3672).                       
YS1194     02  COMM-RM21-RM22-RM23           PIC X(8014).                       
  |   *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION TRM21                    
  |        02  COMM-RM21 REDEFINES COMM-RM21-RM22-RM23.                         
  |            05 COMM-RM21-MODIF-DONNEE    PIC X.                              
  |            05 COMM-RM21-PAGE            PIC S9(5) COMP-3.                   
  |            05 COMM-RM21-PAGE-MAX        PIC S9(5) COMP-3.                   
  |            05 COMM-RM21-ITEMAX          PIC S9(5) COMP-3.                   
  |   *        05 COMM-SWAP-CURS            PIC S9(4) COMP.                     
  |            05 COMM-RM21-ACCES-CHEFP     PIC X.                              
  |            05 COMM-RM21-TAB-PAGE.                                           
  |               10 COMM-RM21-TABLEAU OCCURS 1000.                             
  |                  15 COMM-RM21-CFAM      PIC X(5).                           
  |                  15 COMM-RM21-NUM-PAGE  PIC S9(5) COMP-3.                   
YS1194            10 COMM-RM21-TABMAX       PIC S9(5) COMP-3.                   
      *=> ZONES DE SAUVEGARDE PROPRES A LA TRANSACTION TRM22                    
           02  COMM-RM22 REDEFINES COMM-RM21-RM22-RM23.                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05  COMM-RM22-PAGE            PIC S9(2) COMP.                    
      *--                                                                       
               05  COMM-RM22-PAGE            PIC S9(2) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05  COMM-RM22-PAGE-TOT        PIC S9(2) COMP.                    
      *--                                                                       
               05  COMM-RM22-PAGE-TOT        PIC S9(2) COMP-5.                  
      *}                                                                        
               05  COMM-RM22-SEM             PIC X(06).                         
               05  COMM-RM22-SEM1            PIC X(06).                         
               05  COMM-RM22-SEM2            PIC X(06).                         
               05  COMM-RM22-SEM3            PIC X(06).                         
               05  COMM-RM22-SEM4            PIC X(06).                         
               05  COMM-RM22-SEM5            PIC X(06).                         
               05  COMM-RM22-SEM6            PIC X(06).                         
               05  COMM-RM22-SEM7            PIC X(06).                         
               05  COMM-RM22-SEM8            PIC X(06).                         
               05  COMM-RM22-SEM-1           PIC X(06).                         
               05  COMM-RM22-SEM-2           PIC X(06).                         
               05  COMM-RM22-SEM-3           PIC X(06).                         
               05  COMM-RM22-SEM-4           PIC X(06).                         
               05  COMM-RM22-SEM-5           PIC X(06).                         
               05  COMM-RM22-SEM-6           PIC X(06).                         
               05  COMM-RM22-SEM-7           PIC X(06).                         
               05  COMM-RM22-DDEB1           PIC X(8).                          
               05  COMM-RM22-DDEB1-2         PIC X(10).                         
               05  COMM-RM22-DDEB2           PIC X(8).                          
               05  COMM-RM22-DDEB2-2         PIC X(10).                         
               05  COMM-RM22-DDEB3           PIC X(8).                          
               05  COMM-RM22-DDEB3-2         PIC X(10).                         
               05  COMM-RM22-DDEB4           PIC X(8).                          
               05  COMM-RM22-DDEB4-2         PIC X(10).                         
               05  COMM-RM22-DFIN1           PIC X(8).                          
               05  COMM-RM22-DFIN1-2         PIC X(10).                         
               05  COMM-RM22-DFIN2           PIC X(8).                          
               05  COMM-RM22-DFIN2-2         PIC X(10).                         
               05  COMM-RM22-DFIN3           PIC X(8).                          
               05  COMM-RM22-DFIN3-2         PIC X(10).                         
               05  COMM-RM22-DFIN4           PIC X(8).                          
               05  COMM-RM22-DFIN4-2         PIC X(10).                         
               05  COMM-RM22-GROUPE          PIC X.                             
               05  COMM-RM22-ACCES-CHEFP     PIC X.                             
               05  COMM-RM22-DONNEES-FAM.                                       
YS1194*            10 COMM-RM22-TAB-FAM OCCURS 200.                             
YS1194             10 COMM-RM22-TAB-FAM OCCURS 1000.                            
                      15 COMM-RM22-CFAM         PIC X(5).                       
YS1194*        05  COMM-RM22-NFAM-OK         PIC S9(3) COMP-3.                  
YS1194         05  COMM-RM22-NFAM-OK         PIC S9(5) COMP-3.                  
YS1194*        05  COMM-RM22-NFAM-MAX        PIC S9(3) COMP-3.                  
YS1194         05  COMM-RM22-NFAM-MAX        PIC S9(5) COMP-3.                  
               05  COMM-RM23-MODIF-DONNEE    PIC X.                             
               05  COMM-RM23-PAGE            PIC  9(3).                         
               05  COMM-RM23-PAGE-MAX        PIC  9(3).                         
               05  COMM-RM23-ITEMAX          PIC S9(5) COMP-3.                  
               05  COMM-RM23-LIGNE           PIC 9.                             
               05  COMM-RM23-CHOIX           PIC 9.                             
               05  COMM-RM23-WNATURE         PIC X.                             
               05  COMM-RM23-WIMPACT         PIC X OCCURS 3.                    
               05  COMM-RM23-NUMSEM          OCCURS 3.                          
                  10  COMM-RM23-SEMAINE      PIC X(6).                          
               05  COMM-RM23-QUANTITE-SEG    OCCURS 3.                          
                  10  COMM-RM23-QPV          PIC S9(6).                         
                  10  COMM-RM23-QPV-T        PIC X.                             
               05  COMM-RM23-QUANTITE-ART    OCCURS 3.                          
                  10  COMM-RM23-QART         PIC 9(6).                          
               05  COMM-RM23-QPV-S1          PIC S9(6).                         
               05  COMM-RM26-DEBGRP          PIC X(5).                          
               05  COMM-RM26-DEBSEM.                                            
                   15  COMM-RM26-DEBSEM-SS       PIC X(2).                      
                   15  COMM-RM26-DEBSEM-AA       PIC X(2).                      
                   15  COMM-RM26-DEBSEM-WW       PIC X(2).                      
      *        05  COMM-RM20-FILLER          PIC X(2359).                       
      *        05  COMM-RM20-FILLER          PIC X(2343).                       
YS1194         05  COMM-RM20-FILLER          PIC X(2672).                       
                                                                                
