      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFL5001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFL5001                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL5001.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL5001.                                                            
      *}                                                                        
           02  FL50-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  FL50-NDEPOT                                                      
               PIC X(0003).                                                     
           02  FL50-NCODIC                                                      
               PIC X(0007).                                                     
           02  FL50-D1RECEPT                                                    
               PIC X(0008).                                                     
           02  FL50-CAPPRO                                                      
               PIC X(0005).                                                     
           02  FL50-WSENSAPPRO                                                  
               PIC X(0001).                                                     
           02  FL50-QDELAIAPPRO                                                 
               PIC X(0003).                                                     
           02  FL50-QCOLICDE                                                    
               PIC S9(3) COMP-3.                                                
           02  FL50-LEMBALLAGE                                                  
               PIC X(0050).                                                     
           02  FL50-QCOLIRECEPT                                                 
               PIC S9(5) COMP-3.                                                
           02  FL50-QCOLIDSTOCK                                                 
               PIC S9(5) COMP-3.                                                
           02  FL50-CMODSTOCK1                                                  
               PIC X(0005).                                                     
           02  FL50-WMODSTOCK1                                                  
               PIC X(0001).                                                     
           02  FL50-CMODSTOCK2                                                  
               PIC X(0005).                                                     
           02  FL50-WMODSTOCK2                                                  
               PIC X(0001).                                                     
           02  FL50-CMODSTOCK3                                                  
               PIC X(0005).                                                     
           02  FL50-WMODSTOCK3                                                  
               PIC X(0001).                                                     
           02  FL50-CFETEMPL                                                    
               PIC X(0001).                                                     
           02  FL50-QNBRANMAIL                                                  
               PIC S9(3) COMP-3.                                                
           02  FL50-QNBNIVGERB                                                  
               PIC S9(3) COMP-3.                                                
           02  FL50-CZONEACCES                                                  
               PIC X(0001).                                                     
           02  FL50-CCONTENEUR                                                  
               PIC X(0001).                                                     
           02  FL50-CSPECIFSTK                                                  
               PIC X(0001).                                                     
           02  FL50-CCOTEHOLD                                                   
               PIC X(0001).                                                     
           02  FL50-QNBPVSOL                                                    
               PIC S9(3) COMP-3.                                                
           02  FL50-QNBPRACK                                                    
               PIC S9(5) COMP-3.                                                
           02  FL50-CTPSUP                                                      
               PIC X(0001).                                                     
           02  FL50-WRESFOURN                                                   
               PIC X(0001).                                                     
           02  FL50-CQUOTA                                                      
               PIC X(0005).                                                     
           02  FL50-CUSINE                                                      
               PIC X(0005).                                                     
           02  FL50-DMAJ                                                        
               PIC X(0008).                                                     
           02  FL50-CEXPO                                                       
               PIC X(0005).                                                     
           02  FL50-LSTATCOMP                                                   
               PIC X(0003).                                                     
           02  FL50-WSTOCKAVANCE                                                
               PIC X(0001).                                                     
           02  FL50-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FL50-CLASSE                                                      
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFL5001                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL5001-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL5001-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-D1RECEPT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-D1RECEPT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CAPPRO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CAPPRO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-WSENSAPPRO-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-WSENSAPPRO-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-QDELAIAPPRO-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-QDELAIAPPRO-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-QCOLICDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-QCOLICDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-LEMBALLAGE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-LEMBALLAGE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-QCOLIRECEPT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-QCOLIRECEPT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-QCOLIDSTOCK-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-QCOLIDSTOCK-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CMODSTOCK1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CMODSTOCK1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-WMODSTOCK1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-WMODSTOCK1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CMODSTOCK2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CMODSTOCK2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-WMODSTOCK2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-WMODSTOCK2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CMODSTOCK3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CMODSTOCK3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-WMODSTOCK3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-WMODSTOCK3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CFETEMPL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CFETEMPL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-QNBRANMAIL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-QNBRANMAIL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-QNBNIVGERB-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-QNBNIVGERB-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CZONEACCES-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CZONEACCES-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CCONTENEUR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CCONTENEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CSPECIFSTK-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CSPECIFSTK-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CCOTEHOLD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CCOTEHOLD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-QNBPVSOL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-QNBPVSOL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-QNBPRACK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-QNBPRACK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CTPSUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CTPSUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-WRESFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-WRESFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CQUOTA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CQUOTA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CUSINE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CUSINE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CEXPO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CEXPO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-LSTATCOMP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-LSTATCOMP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-WSTOCKAVANCE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-WSTOCKAVANCE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL50-CLASSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL50-CLASSE-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
