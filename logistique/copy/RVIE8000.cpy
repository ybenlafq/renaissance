      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVIE8000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIE8000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVIE8000.                                                            
           02  IE80-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  IE80-NLIEU                                                       
               PIC X(0003).                                                     
           02  IE80-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  IE80-CLIEUTRT                                                    
               PIC X(0005).                                                     
           02  IE80-NCODIC                                                      
               PIC X(0007).                                                     
           02  IE80-QHS                                                         
               PIC S9(5) COMP-3.                                                
           02  IE80-QENC                                                        
               PIC S9(5) COMP-3.                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVIE8000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVIE8000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE80-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE80-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE80-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE80-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE80-NSSLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE80-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE80-CLIEUTRT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE80-CLIEUTRT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE80-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE80-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE80-QHS-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE80-QHS-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE80-QENC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE80-QENC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
