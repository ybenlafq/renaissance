      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DU CALENDRIER DE MUTATIONS      *        
      *----------------------------------------------------------------*        
       01  DSECT-FMU030.                                                        
         05 CHAMPS-FMU030.                                                      
           10 FMU030-NSOCENTR           PIC X(03).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-NDEPOT             PIC X(03).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-CSELART            PIC X(05).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-NSOCIETE           PIC X(03).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-NLIEU              PIC X(03).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-LLIEU              PIC X(20).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-NMUTATION          PIC X(07).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-DDEBSAIS           PIC X(08).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-DFINSAIS           PIC X(08).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-JOUR               PIC X(03).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-DDESTOCK           PIC X(08).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-DCHARGT            PIC X(08).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-HCHARGT            PIC X(05).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-DMUTATION          PIC X(08).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-WPROPERMIS         PIC X(01).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-QNBM3QUOTA         PIC 999,99.                             
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-QNBPQUOTA          PIC 9(05).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU030-NOTES              PIC X(05).                              
           10 FILLER                    PIC X VALUE ';'.                        
       01 FMU030-LIBELLES.                                                      
         05 FILLER                      PIC X(08) VALUE 'NSOCENTR'.             
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(05) VALUE 'DEPOT'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(06) VALUE 'SELART'.               
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(04) VALUE 'NSOC'.                 
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(05) VALUE 'NLIEU'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(05) VALUE 'LLIEU'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(03) VALUE 'MUT'.                  
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(08) VALUE 'DDEBSAIS'.             
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(08) VALUE 'DFINSAIS'.             
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(04) VALUE 'JOUR'.                 
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(08) VALUE 'DDESTOCK'.             
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'DCHARGT'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'HCHARGT'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(04) VALUE 'DMUT'.                 
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(03) VALUE 'PRO'.                  
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(08) VALUE 'QUOTA M3'.             
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'QUOTA P'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(05) VALUE 'NOTES'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(004) VALUE SPACES.                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FMU030-LONG           PIC S9(4)   COMP  VALUE +127.            
      *                                                                         
      *--                                                                       
       01  DSECT-FMU030-LONG           PIC S9(4) COMP-5  VALUE +127.            
                                                                                
      *}                                                                        
