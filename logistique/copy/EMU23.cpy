      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU23   EMU23                                              00000020
      ***************************************************************** 00000030
       01   EMU23I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENTL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNSOCENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCENTF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCENTI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNDEPOTI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLDEPOTI  PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTATIL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNMUTATIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMUTATIF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMUTATII      PIC X(7).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNSOCIETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCIETF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNSOCIETI      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNLIEUI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMUTATIL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MDMUTATIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDMUTATIF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDMUTATII      PIC X(8).                                  00000410
           02 M8I OCCURS   12 TIMES .                                   00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCFAMI  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCMARQI      PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOUL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLREFFOUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLREFFOUF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLREFFOUI    PIC X(20).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNCODICI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMUTANCL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MQMUTANCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQMUTANCF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MQMUTANCI    PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMUTEEL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MQMUTEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQMUTEEF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQMUTEEI     PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDIFFERL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MDIFFERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDIFFERF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MDIFFERI     PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQREGULL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MQREGULL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQREGULF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQREGULI     PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(12).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(61).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: EMU23   EMU23                                              00001000
      ***************************************************************** 00001010
       01   EMU23O REDEFINES EMU23I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MNSOCENTA      PIC X.                                     00001190
           02 MNSOCENTC PIC X.                                          00001200
           02 MNSOCENTP PIC X.                                          00001210
           02 MNSOCENTH PIC X.                                          00001220
           02 MNSOCENTV PIC X.                                          00001230
           02 MNSOCENTO      PIC X(3).                                  00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MNDEPOTA  PIC X.                                          00001260
           02 MNDEPOTC  PIC X.                                          00001270
           02 MNDEPOTP  PIC X.                                          00001280
           02 MNDEPOTH  PIC X.                                          00001290
           02 MNDEPOTV  PIC X.                                          00001300
           02 MNDEPOTO  PIC X(3).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLDEPOTA  PIC X.                                          00001330
           02 MLDEPOTC  PIC X.                                          00001340
           02 MLDEPOTP  PIC X.                                          00001350
           02 MLDEPOTH  PIC X.                                          00001360
           02 MLDEPOTV  PIC X.                                          00001370
           02 MLDEPOTO  PIC X(20).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MNMUTATIA      PIC X.                                     00001400
           02 MNMUTATIC PIC X.                                          00001410
           02 MNMUTATIP PIC X.                                          00001420
           02 MNMUTATIH PIC X.                                          00001430
           02 MNMUTATIV PIC X.                                          00001440
           02 MNMUTATIO      PIC X(7).                                  00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNSOCIETA      PIC X.                                     00001470
           02 MNSOCIETC PIC X.                                          00001480
           02 MNSOCIETP PIC X.                                          00001490
           02 MNSOCIETH PIC X.                                          00001500
           02 MNSOCIETV PIC X.                                          00001510
           02 MNSOCIETO      PIC X(3).                                  00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNLIEUA   PIC X.                                          00001540
           02 MNLIEUC   PIC X.                                          00001550
           02 MNLIEUP   PIC X.                                          00001560
           02 MNLIEUH   PIC X.                                          00001570
           02 MNLIEUV   PIC X.                                          00001580
           02 MNLIEUO   PIC X(3).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MDMUTATIA      PIC X.                                     00001610
           02 MDMUTATIC PIC X.                                          00001620
           02 MDMUTATIP PIC X.                                          00001630
           02 MDMUTATIH PIC X.                                          00001640
           02 MDMUTATIV PIC X.                                          00001650
           02 MDMUTATIO      PIC X(8).                                  00001660
           02 M8O OCCURS   12 TIMES .                                   00001670
             03 FILLER       PIC X(2).                                  00001680
             03 MCFAMA  PIC X.                                          00001690
             03 MCFAMC  PIC X.                                          00001700
             03 MCFAMP  PIC X.                                          00001710
             03 MCFAMH  PIC X.                                          00001720
             03 MCFAMV  PIC X.                                          00001730
             03 MCFAMO  PIC X(5).                                       00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MCMARQA      PIC X.                                     00001760
             03 MCMARQC PIC X.                                          00001770
             03 MCMARQP PIC X.                                          00001780
             03 MCMARQH PIC X.                                          00001790
             03 MCMARQV PIC X.                                          00001800
             03 MCMARQO      PIC X(5).                                  00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MLREFFOUA    PIC X.                                     00001830
             03 MLREFFOUC    PIC X.                                     00001840
             03 MLREFFOUP    PIC X.                                     00001850
             03 MLREFFOUH    PIC X.                                     00001860
             03 MLREFFOUV    PIC X.                                     00001870
             03 MLREFFOUO    PIC X(20).                                 00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MNCODICA     PIC X.                                     00001900
             03 MNCODICC     PIC X.                                     00001910
             03 MNCODICP     PIC X.                                     00001920
             03 MNCODICH     PIC X.                                     00001930
             03 MNCODICV     PIC X.                                     00001940
             03 MNCODICO     PIC X(7).                                  00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MQMUTANCA    PIC X.                                     00001970
             03 MQMUTANCC    PIC X.                                     00001980
             03 MQMUTANCP    PIC X.                                     00001990
             03 MQMUTANCH    PIC X.                                     00002000
             03 MQMUTANCV    PIC X.                                     00002010
             03 MQMUTANCO    PIC X(5).                                  00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MQMUTEEA     PIC X.                                     00002040
             03 MQMUTEEC     PIC X.                                     00002050
             03 MQMUTEEP     PIC X.                                     00002060
             03 MQMUTEEH     PIC X.                                     00002070
             03 MQMUTEEV     PIC X.                                     00002080
             03 MQMUTEEO     PIC X(5).                                  00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MDIFFERA     PIC X.                                     00002110
             03 MDIFFERC     PIC X.                                     00002120
             03 MDIFFERP     PIC X.                                     00002130
             03 MDIFFERH     PIC X.                                     00002140
             03 MDIFFERV     PIC X.                                     00002150
             03 MDIFFERO     PIC X(5).                                  00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MQREGULA     PIC X.                                     00002180
             03 MQREGULC     PIC X.                                     00002190
             03 MQREGULP     PIC X.                                     00002200
             03 MQREGULH     PIC X.                                     00002210
             03 MQREGULV     PIC X.                                     00002220
             03 MQREGULO     PIC X(5).                                  00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MZONCMDA  PIC X.                                          00002250
           02 MZONCMDC  PIC X.                                          00002260
           02 MZONCMDP  PIC X.                                          00002270
           02 MZONCMDH  PIC X.                                          00002280
           02 MZONCMDV  PIC X.                                          00002290
           02 MZONCMDO  PIC X(12).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(61).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
