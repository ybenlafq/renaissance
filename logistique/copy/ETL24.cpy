      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ETL27   ETL27                                              00000020
      ***************************************************************** 00000030
       01   ETL24I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLSOCI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLIVRL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDLIVRF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDLIVRI   PIC X(8).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIVR1L  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCLIVR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIVR1F  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCLIVR1I  PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIVR1L  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLLIVR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIVR1F  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLLIVR1I  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCPROFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPROFF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCPROFI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTOURNL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCTOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTOURNF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCTOURNI  PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIVR2L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCLIVR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIVR2F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCLIVR2I  PIC X(10).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIVR2L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLLIVR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIVR2F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLLIVR2I  PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIVR3L  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCLIVR3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIVR3F  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCLIVR3I  PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIVR3L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLLIVR3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIVR3F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLLIVR3I  PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTDEV0L  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MMTDEV0L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMTDEV0F  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MMTDEV0I  PIC X(9).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEV0L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCDEV0L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCDEV0F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCDEV0I   PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTDEV1L  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MMTDEV1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMTDEV1F  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MMTDEV1I  PIC X(9).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEV1L   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCDEV1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCDEV1F   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCDEV1I   PIC X(3).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTREG1L  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MMTREG1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMTREG1F  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MMTREG1I  PIC X(9).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCREG1L   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCREG1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCREG1F   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCREG1I   PIC X(3).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTREG2L  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MMTREG2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMTREG2F  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MMTREG2I  PIC X(9).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCREG2L   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCREG2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCREG2F   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCREG2I   PIC X(3).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTREG3L  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MMTREG3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMTREG3F  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MMTREG3I  PIC X(9).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCREG3L   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MCREG3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCREG3F   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MCREG3I   PIC X(3).                                       00001010
           02 M214I OCCURS   10 TIMES .                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNMAGI  PIC X(3).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MNVENTEI     PIC X(7).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCLIENTL    COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MLCLIENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLCLIENTF    PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MLCLIENTI    PIC X(10).                                 00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTOPL  COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MWTOPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWTOPF  PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MWTOPI  PIC X.                                          00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMONTOTL    COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MPMONTOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPMONTOTF    PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MPMONTOTI    PIC X(8).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMONT1L     COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MPMONT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPMONT1F     PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MPMONT1I     PIC X(8).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODP1L     COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MCMODP1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCMODP1F     PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MCMODP1I     PIC X(3).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMONT2L     COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MPMONT2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPMONT2F     PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MPMONT2I     PIC X(8).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODP2L     COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MCMODP2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCMODP2F     PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MCMODP2I     PIC X(3).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMONT3L     COMP PIC S9(4).                            00001390
      *--                                                                       
             03 MPMONT3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPMONT3F     PIC X.                                     00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MPMONT3I     PIC X(8).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODP3L     COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MCMODP3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCMODP3F     PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MCMODP3I     PIC X(3).                                  00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVENTEL     COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MQVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQVENTEF     PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MQVENTEI     PIC X(2).                                  00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MZONCMDI  PIC X(15).                                      00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MLIBERRI  PIC X(58).                                      00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MCODTRAI  PIC X(4).                                       00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MCICSI    PIC X(5).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MNETNAMI  PIC X(8).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MSCREENI  PIC X(4).                                       00001740
      ***************************************************************** 00001750
      * SDF: ETL27   ETL27                                              00001760
      ***************************************************************** 00001770
       01   ETL24O REDEFINES ETL24I.                                    00001780
           02 FILLER    PIC X(12).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MDATJOUA  PIC X.                                          00001810
           02 MDATJOUC  PIC X.                                          00001820
           02 MDATJOUP  PIC X.                                          00001830
           02 MDATJOUH  PIC X.                                          00001840
           02 MDATJOUV  PIC X.                                          00001850
           02 MDATJOUO  PIC X(10).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MTIMJOUA  PIC X.                                          00001880
           02 MTIMJOUC  PIC X.                                          00001890
           02 MTIMJOUP  PIC X.                                          00001900
           02 MTIMJOUH  PIC X.                                          00001910
           02 MTIMJOUV  PIC X.                                          00001920
           02 MTIMJOUO  PIC X(5).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MPAGEA    PIC X.                                          00001950
           02 MPAGEC    PIC X.                                          00001960
           02 MPAGEP    PIC X.                                          00001970
           02 MPAGEH    PIC X.                                          00001980
           02 MPAGEV    PIC X.                                          00001990
           02 MPAGEO    PIC X(3).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNSOCA    PIC X.                                          00002020
           02 MNSOCC    PIC X.                                          00002030
           02 MNSOCP    PIC X.                                          00002040
           02 MNSOCH    PIC X.                                          00002050
           02 MNSOCV    PIC X.                                          00002060
           02 MNSOCO    PIC X(3).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MLSOCA    PIC X.                                          00002090
           02 MLSOCC    PIC X.                                          00002100
           02 MLSOCP    PIC X.                                          00002110
           02 MLSOCH    PIC X.                                          00002120
           02 MLSOCV    PIC X.                                          00002130
           02 MLSOCO    PIC X(20).                                      00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MDLIVRA   PIC X.                                          00002160
           02 MDLIVRC   PIC X.                                          00002170
           02 MDLIVRP   PIC X.                                          00002180
           02 MDLIVRH   PIC X.                                          00002190
           02 MDLIVRV   PIC X.                                          00002200
           02 MDLIVRO   PIC X(8).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MCLIVR1A  PIC X.                                          00002230
           02 MCLIVR1C  PIC X.                                          00002240
           02 MCLIVR1P  PIC X.                                          00002250
           02 MCLIVR1H  PIC X.                                          00002260
           02 MCLIVR1V  PIC X.                                          00002270
           02 MCLIVR1O  PIC X(10).                                      00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MLLIVR1A  PIC X.                                          00002300
           02 MLLIVR1C  PIC X.                                          00002310
           02 MLLIVR1P  PIC X.                                          00002320
           02 MLLIVR1H  PIC X.                                          00002330
           02 MLLIVR1V  PIC X.                                          00002340
           02 MLLIVR1O  PIC X(20).                                      00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MCPROFA   PIC X.                                          00002370
           02 MCPROFC   PIC X.                                          00002380
           02 MCPROFP   PIC X.                                          00002390
           02 MCPROFH   PIC X.                                          00002400
           02 MCPROFV   PIC X.                                          00002410
           02 MCPROFO   PIC X(5).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MCTOURNA  PIC X.                                          00002440
           02 MCTOURNC  PIC X.                                          00002450
           02 MCTOURNP  PIC X.                                          00002460
           02 MCTOURNH  PIC X.                                          00002470
           02 MCTOURNV  PIC X.                                          00002480
           02 MCTOURNO  PIC X(3).                                       00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MCLIVR2A  PIC X.                                          00002510
           02 MCLIVR2C  PIC X.                                          00002520
           02 MCLIVR2P  PIC X.                                          00002530
           02 MCLIVR2H  PIC X.                                          00002540
           02 MCLIVR2V  PIC X.                                          00002550
           02 MCLIVR2O  PIC X(10).                                      00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MLLIVR2A  PIC X.                                          00002580
           02 MLLIVR2C  PIC X.                                          00002590
           02 MLLIVR2P  PIC X.                                          00002600
           02 MLLIVR2H  PIC X.                                          00002610
           02 MLLIVR2V  PIC X.                                          00002620
           02 MLLIVR2O  PIC X(20).                                      00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MCLIVR3A  PIC X.                                          00002650
           02 MCLIVR3C  PIC X.                                          00002660
           02 MCLIVR3P  PIC X.                                          00002670
           02 MCLIVR3H  PIC X.                                          00002680
           02 MCLIVR3V  PIC X.                                          00002690
           02 MCLIVR3O  PIC X(10).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MLLIVR3A  PIC X.                                          00002720
           02 MLLIVR3C  PIC X.                                          00002730
           02 MLLIVR3P  PIC X.                                          00002740
           02 MLLIVR3H  PIC X.                                          00002750
           02 MLLIVR3V  PIC X.                                          00002760
           02 MLLIVR3O  PIC X(20).                                      00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MMTDEV0A  PIC X.                                          00002790
           02 MMTDEV0C  PIC X.                                          00002800
           02 MMTDEV0P  PIC X.                                          00002810
           02 MMTDEV0H  PIC X.                                          00002820
           02 MMTDEV0V  PIC X.                                          00002830
           02 MMTDEV0O  PIC ZZZZZ9,99.                                  00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MCDEV0A   PIC X.                                          00002860
           02 MCDEV0C   PIC X.                                          00002870
           02 MCDEV0P   PIC X.                                          00002880
           02 MCDEV0H   PIC X.                                          00002890
           02 MCDEV0V   PIC X.                                          00002900
           02 MCDEV0O   PIC X(3).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MMTDEV1A  PIC X.                                          00002930
           02 MMTDEV1C  PIC X.                                          00002940
           02 MMTDEV1P  PIC X.                                          00002950
           02 MMTDEV1H  PIC X.                                          00002960
           02 MMTDEV1V  PIC X.                                          00002970
           02 MMTDEV1O  PIC ZZZZZ9,99.                                  00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MCDEV1A   PIC X.                                          00003000
           02 MCDEV1C   PIC X.                                          00003010
           02 MCDEV1P   PIC X.                                          00003020
           02 MCDEV1H   PIC X.                                          00003030
           02 MCDEV1V   PIC X.                                          00003040
           02 MCDEV1O   PIC X(3).                                       00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MMTREG1A  PIC X.                                          00003070
           02 MMTREG1C  PIC X.                                          00003080
           02 MMTREG1P  PIC X.                                          00003090
           02 MMTREG1H  PIC X.                                          00003100
           02 MMTREG1V  PIC X.                                          00003110
           02 MMTREG1O  PIC ZZZZZ9,99.                                  00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MCREG1A   PIC X.                                          00003140
           02 MCREG1C   PIC X.                                          00003150
           02 MCREG1P   PIC X.                                          00003160
           02 MCREG1H   PIC X.                                          00003170
           02 MCREG1V   PIC X.                                          00003180
           02 MCREG1O   PIC X(3).                                       00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MMTREG2A  PIC X.                                          00003210
           02 MMTREG2C  PIC X.                                          00003220
           02 MMTREG2P  PIC X.                                          00003230
           02 MMTREG2H  PIC X.                                          00003240
           02 MMTREG2V  PIC X.                                          00003250
           02 MMTREG2O  PIC ZZZZZ9,99.                                  00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MCREG2A   PIC X.                                          00003280
           02 MCREG2C   PIC X.                                          00003290
           02 MCREG2P   PIC X.                                          00003300
           02 MCREG2H   PIC X.                                          00003310
           02 MCREG2V   PIC X.                                          00003320
           02 MCREG2O   PIC X(3).                                       00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MMTREG3A  PIC X.                                          00003350
           02 MMTREG3C  PIC X.                                          00003360
           02 MMTREG3P  PIC X.                                          00003370
           02 MMTREG3H  PIC X.                                          00003380
           02 MMTREG3V  PIC X.                                          00003390
           02 MMTREG3O  PIC ZZZZZ9,99.                                  00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MCREG3A   PIC X.                                          00003420
           02 MCREG3C   PIC X.                                          00003430
           02 MCREG3P   PIC X.                                          00003440
           02 MCREG3H   PIC X.                                          00003450
           02 MCREG3V   PIC X.                                          00003460
           02 MCREG3O   PIC X(3).                                       00003470
           02 M214O OCCURS   10 TIMES .                                 00003480
             03 FILLER       PIC X(2).                                  00003490
             03 MNMAGA  PIC X.                                          00003500
             03 MNMAGC  PIC X.                                          00003510
             03 MNMAGP  PIC X.                                          00003520
             03 MNMAGH  PIC X.                                          00003530
             03 MNMAGV  PIC X.                                          00003540
             03 MNMAGO  PIC X(3).                                       00003550
             03 FILLER       PIC X(2).                                  00003560
             03 MNVENTEA     PIC X.                                     00003570
             03 MNVENTEC     PIC X.                                     00003580
             03 MNVENTEP     PIC X.                                     00003590
             03 MNVENTEH     PIC X.                                     00003600
             03 MNVENTEV     PIC X.                                     00003610
             03 MNVENTEO     PIC X(7).                                  00003620
             03 FILLER       PIC X(2).                                  00003630
             03 MLCLIENTA    PIC X.                                     00003640
             03 MLCLIENTC    PIC X.                                     00003650
             03 MLCLIENTP    PIC X.                                     00003660
             03 MLCLIENTH    PIC X.                                     00003670
             03 MLCLIENTV    PIC X.                                     00003680
             03 MLCLIENTO    PIC X(10).                                 00003690
             03 FILLER       PIC X(2).                                  00003700
             03 MWTOPA  PIC X.                                          00003710
             03 MWTOPC  PIC X.                                          00003720
             03 MWTOPP  PIC X.                                          00003730
             03 MWTOPH  PIC X.                                          00003740
             03 MWTOPV  PIC X.                                          00003750
             03 MWTOPO  PIC X.                                          00003760
             03 FILLER       PIC X(2).                                  00003770
             03 MPMONTOTA    PIC X.                                     00003780
             03 MPMONTOTC    PIC X.                                     00003790
             03 MPMONTOTP    PIC X.                                     00003800
             03 MPMONTOTH    PIC X.                                     00003810
             03 MPMONTOTV    PIC X.                                     00003820
             03 MPMONTOTO    PIC X(8).                                  00003830
             03 FILLER       PIC X(2).                                  00003840
             03 MPMONT1A     PIC X.                                     00003850
             03 MPMONT1C     PIC X.                                     00003860
             03 MPMONT1P     PIC X.                                     00003870
             03 MPMONT1H     PIC X.                                     00003880
             03 MPMONT1V     PIC X.                                     00003890
             03 MPMONT1O     PIC ZZZZ9,99.                              00003900
             03 FILLER       PIC X(2).                                  00003910
             03 MCMODP1A     PIC X.                                     00003920
             03 MCMODP1C     PIC X.                                     00003930
             03 MCMODP1P     PIC X.                                     00003940
             03 MCMODP1H     PIC X.                                     00003950
             03 MCMODP1V     PIC X.                                     00003960
             03 MCMODP1O     PIC X(3).                                  00003970
             03 FILLER       PIC X(2).                                  00003980
             03 MPMONT2A     PIC X.                                     00003990
             03 MPMONT2C     PIC X.                                     00004000
             03 MPMONT2P     PIC X.                                     00004010
             03 MPMONT2H     PIC X.                                     00004020
             03 MPMONT2V     PIC X.                                     00004030
             03 MPMONT2O     PIC ZZZZ9,99.                              00004040
             03 FILLER       PIC X(2).                                  00004050
             03 MCMODP2A     PIC X.                                     00004060
             03 MCMODP2C     PIC X.                                     00004070
             03 MCMODP2P     PIC X.                                     00004080
             03 MCMODP2H     PIC X.                                     00004090
             03 MCMODP2V     PIC X.                                     00004100
             03 MCMODP2O     PIC X(3).                                  00004110
             03 FILLER       PIC X(2).                                  00004120
             03 MPMONT3A     PIC X.                                     00004130
             03 MPMONT3C     PIC X.                                     00004140
             03 MPMONT3P     PIC X.                                     00004150
             03 MPMONT3H     PIC X.                                     00004160
             03 MPMONT3V     PIC X.                                     00004170
             03 MPMONT3O     PIC ZZZZ9,99.                              00004180
             03 FILLER       PIC X(2).                                  00004190
             03 MCMODP3A     PIC X.                                     00004200
             03 MCMODP3C     PIC X.                                     00004210
             03 MCMODP3P     PIC X.                                     00004220
             03 MCMODP3H     PIC X.                                     00004230
             03 MCMODP3V     PIC X.                                     00004240
             03 MCMODP3O     PIC X(3).                                  00004250
             03 FILLER       PIC X(2).                                  00004260
             03 MQVENTEA     PIC X.                                     00004270
             03 MQVENTEC     PIC X.                                     00004280
             03 MQVENTEP     PIC X.                                     00004290
             03 MQVENTEH     PIC X.                                     00004300
             03 MQVENTEV     PIC X.                                     00004310
             03 MQVENTEO     PIC X(2).                                  00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MZONCMDA  PIC X.                                          00004340
           02 MZONCMDC  PIC X.                                          00004350
           02 MZONCMDP  PIC X.                                          00004360
           02 MZONCMDH  PIC X.                                          00004370
           02 MZONCMDV  PIC X.                                          00004380
           02 MZONCMDO  PIC X(15).                                      00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MLIBERRA  PIC X.                                          00004410
           02 MLIBERRC  PIC X.                                          00004420
           02 MLIBERRP  PIC X.                                          00004430
           02 MLIBERRH  PIC X.                                          00004440
           02 MLIBERRV  PIC X.                                          00004450
           02 MLIBERRO  PIC X(58).                                      00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MCODTRAA  PIC X.                                          00004480
           02 MCODTRAC  PIC X.                                          00004490
           02 MCODTRAP  PIC X.                                          00004500
           02 MCODTRAH  PIC X.                                          00004510
           02 MCODTRAV  PIC X.                                          00004520
           02 MCODTRAO  PIC X(4).                                       00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MCICSA    PIC X.                                          00004550
           02 MCICSC    PIC X.                                          00004560
           02 MCICSP    PIC X.                                          00004570
           02 MCICSH    PIC X.                                          00004580
           02 MCICSV    PIC X.                                          00004590
           02 MCICSO    PIC X(5).                                       00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MNETNAMA  PIC X.                                          00004620
           02 MNETNAMC  PIC X.                                          00004630
           02 MNETNAMP  PIC X.                                          00004640
           02 MNETNAMH  PIC X.                                          00004650
           02 MNETNAMV  PIC X.                                          00004660
           02 MNETNAMO  PIC X(8).                                       00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MSCREENA  PIC X.                                          00004690
           02 MSCREENC  PIC X.                                          00004700
           02 MSCREENP  PIC X.                                          00004710
           02 MSCREENH  PIC X.                                          00004720
           02 MSCREENV  PIC X.                                          00004730
           02 MSCREENO  PIC X(4).                                       00004740
                                                                                
