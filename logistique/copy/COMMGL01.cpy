      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PROG TGL01                             *            
      *       TR : GL00  GESTION DES LIVRAISON                     *            
      *       PG : TGL01 PROGRAMMATION DES LIVRAISONS              *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ----------------------------  7519         
      * +80 7599                                                                
      *                                                                         
          02 COMM-GL01-APPLI REDEFINES COMM-GL00-APPLI.                         
      *----------------------------        CODE FONCTION                        
             03 COMM-GL01-WFONC          PIC X(3).                              
      *-------                           CHOIX                                  
             03 COMM-GL01-ZONCMD         PIC X(15).                             
      *-------                                                                  
             03 COMM-GL01-NLIVRAIS       PIC X(7).                              
      *-------                                                                  
             03 COMM-GL01-NSOCIETE       PIC X(3).                              
      *-------                                                                  
             03 COMM-GL01-NDEPOT         PIC X(3).                              
      *                                                                         
      *-------                           TYPE DE RECEPTION                      
             03 COMM-GL01-CREC           PIC X(5).                              
             03 COMM-GL01-LREC           PIC X(20).                             
      *                                                                         
      *-------                           EN SSAAMMJJ EN IXE                     
             03 COMM-GL01-DATLIVRX       PIC X(8).                              
      *                                                                         
      *-------                           EN JJ/MM/AA                            
             03 COMM-GL01-SEMAINLV       PIC X(6).                              
      *                                                                         
      *-------                           JOUR DE LA LIVRAISON                   
             03 COMM-GL01-JOURLIV        PIC 9.                                 
      *                                                                         
      *-------                                                                  
             03 COMM-GL01-HEURLIVR       PIC X(2).                              
      *-------                                                                  
             03 COMM-GL01-MINUTLIV       PIC X(2).                              
      *-------                                                                  
             03 COMM-GL01-WPALETIS       PIC X(1).                              
      *-------                                                                  
             03 COMM-GL01-CLIVREUR       PIC X(5).                              
      *-------                                                                  
             03 COMM-GL01-LLIVREUR       PIC X(20).                             
      *-------                                                                  
             03 COMM-GL01-COMMENT        PIC X(20).                             
      *-------                                                                  
             03 COMM-GL01-CTRANS         PIC X(5).                              
      *-------                                                                  
             03 COMM-GL01-LTRANS         PIC X(20).                             
      *-------                                                                  
             03 COMM-GL01-QNBP           PIC 9(5).                              
      *-------                                                                  
             03 COMM-GL01-QBUO           PIC 9(4).                              
      *-------                                                                  
             03 COMM-GL01-NFOUR          PIC X(5).                              
      *-------                                                                  
             03 COMM-GL01-LFOUR          PIC X(20).                             
      *-------                                                                  
             03 COMM-GL01-PAGGAUCH       PIC 9(3).                              
      *-------                                                                  
             03 COMM-GL01-PAGDROIT       PIC 9(3).                              
      *-------                                                                  
      *-------                                                                  
             03 COMM-GL01-PAGG-MAX       PIC 9(3).                              
      *-------                                                                  
             03 COMM-GL01-PAGD-MAX       PIC 9(3).                              
      *-------                                                                  
             03 LIGNES-GAUCHES.                                                 
                05 COMM-GL01-NCDE-DATES OCCURS 10.                              
                   07 COMM-GL01-NCDEG    PIC X(7).                              
                   07 COMM-GL01-QTEG     PIC 9(5).                              
                   07 COMM-GL01-SELG     PIC X.                                 
                   07 COMM-GL01-SATDUEDATEG PIC X(8).                           
      *                                                                         
      *-------                                                                  
             03 LIGNES-DROITES.                                                 
                05 COMM-GL01-NFOUR-DATES OCCURS 10.                             
                   07 COMM-GL01-NENT     PIC X(5).                              
                   07 COMM-GL01-LENT     PIC X(20).                             
                   07 COMM-GL01-NCDED    PIC X(7).                              
                   07 COMM-GL01-QTED     PIC 9(5).                              
                   07 COMM-GL01-QUOD     PIC 9(4).                              
                   07 COMM-GL01-QPAL     PIC 9(4).                              
                   07 COMM-GL01-SATDUEDATED PIC X(8).                           
                   07 COMM-GL01-AFFECT-CREASOND PIC X(1).                       
      *-------                           GESTION SS-TABLE XCTRL                 
             03 COMM-GL01-INTRFC         PIC X(1).                              
             03 COMM-GL01-AFFTIM         PIC X(1).                              
      *-------                           DIFFERENT TYPE DE MODIF                
             03 COMM-GL01-TYPMODIF.                                             
               05 COMM-GL01-MAJGL0       PIC X.                                 
               05 COMM-GL01-CHGDAT       PIC X.                                 
               05 COMM-GL01-CHGPAL       PIC X.                                 
               05 COMM-GL01-CHGFOU       PIC X.                                 
               05 COMM-GL01-AFFECT       PIC X.                                 
               05 COMM-GL01-DESAFF       PIC X.                                 
      *-------                           FILLER                                 
             03 COMM-GL01-FLAG-PASSAGE   PIC X.                                 
             03 COMM-GL01-RECUP-NLIVR    PIC X.                                 
             03 COMM-GL01-FPLANFLAG      PIC X.                                 
             03 COMM-GL01-FPLANDATE      PIC X(8).                              
             03 COMM-GL01-AFFECTCREAS    PIC X(1).                              
             03 COMM-GL01-DATLIVRXN      PIC X(8).                              
      *      03 FILLER                   PIC X(2641).                           
             03 COMM-GL01-DATE-LIMITE    PIC X(8).                              
             03 COMM-GL01-FLAG-PASTDATE  PIC X(1).                              
               88 COMM-GL01-PASTDATE-OK  VALUE 'O'.                             
               88 COMM-GL01-PASTDATE-KO  VALUE 'N'.                             
             03 COMM-GL01-CTL-INTEGRITE  PIC X.                                 
                88 COMM-CTL-INT-OK       VALUE '0'.                             
                88 COMM-CTL-INT-KO       VALUE '1'.                             
             03 COMM-GL01-FILLER         PIC X(2443).                           
      *      03 COMM-GL01-FILLER         PIC X(2444).                           
      *      03 COMM-GL01-FILLER         PIC X(2453).                           
      *                                                                         
                                                                                
