      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM0100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM0100.                                                            
           02  RM01-CRMGROUP                                                    
               PIC X(0005).                                                     
           02  RM01-LRMGROUP                                                    
               PIC X(0020).                                                     
           02  RM01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM01-CRMGROUP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM01-CRMGROUP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM01-LRMGROUP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM01-LRMGROUP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
