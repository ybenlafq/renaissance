      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....:                                                 * 00030000
      * FICHIER.....: LIGNE D�BUT DE TYPE D�BUT                       * 00031000
      *---------------------------------------------------------------* 00060000
      * CR   .......: 12/10/2011  17:46:00                            * 00070000
      * MODIFIE.....:   /  /                                          * 00080000
      * VERSION N�..: 001                                             * 00090000
      * LONGUEUR....: 038                                             * 00091000
      * GENERATEUR..:         - VERSION    12/10/11                   * 00100000
      ***************************************************************** 00110000
      *                                                                 00120000
       01  LM7DEB.                                                      00130000
      *                                                                 00140000
           05      LM7DEB-TYP-DEBUT       PIC  X(0001).                 00150000
           05      LM7DEB-NOM-FICORIG     PIC  X(0020).                 00160000
           05      LM7DEB-DCREATION       PIC  X(0008).                 00161000
           05      LM7DEB-HCREATION       PIC  X(0006).                 00161100
           05      LM7DEB-VERSION         PIC  X(0003).                 00162001
                                                                                
