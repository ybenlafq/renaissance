      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *  PROJET     : QUOTAS DECENTRALISES                             *        
      *  TITRE      : TS DE MISE A JOUR DE LA TABLE RTGQ00 (CALENDRIER)*        
      *  APPELER PAR: TZQ18 - AFFECTATION PROFIL TYPE / JOUR           *        
      *                                                                *        
      *  CONTENU    : 1 ITEM PAR PAGE                                  *00000050
      *               NIVEAU 1 : 2 TYPES DE QUOTA (1=LIVRE, 2=SAV)     *        
      *               NIVEAU 2 : 28 JOURS (4 SEMAINES * 7 JOURS)       *        
      ******************************************************************        
      *                                                                         
      **** LONGUEUR TS ********************************** 560+3360= 3920        
       01  TS-DONNEES.                                                          
      ******* DATE *************************************** 28LG*20C= 560        
           05 TS-DATE                OCCURS 28.                                 
              10 TS-DJOUR               PIC X(08).                              
              10 TS-NSMN                PIC 9(01).                              
              10 TS-FILLER1             PIC X(11).                              
      ******* TYPE QUOTA, JOUR ************************ 2*28LG*60C= 3360        
           05 TS-TYP                 OCCURS 2.                                  
              10 TS-JOUR                OCCURS 28.                              
                 15 TS-ACTION              PIC X(01).                           
                    88 TS-INACTION             VALUE ' '.                       
                    88 TS-CREATION             VALUE 'C'.                       
                    88 TS-MODIFICATION         VALUE 'M'.                       
                    88 TS-SUPPRESSION          VALUE 'S'.                       
                    88 TS-SUPPRMODIF           VALUE 'H'.                       
                    88 TS-SUPPRCREATE          VALUE 'I'.                       
                 15 TS-EXISTE              PIC X(01).                           
                    88 TS-INEXISTENCE          VALUE ' '.                       
                    88 TS-EXISTENCE            VALUE 'E'.                       
                 15 TS-RVGQ0000-FICHIER.                                        
                    20 TS-FILLER-FICHIER      PIC X(16).                        
                    20 TS-CPRODEL-FICHIER     PIC X(05).                        
                 15 TS-RVGQ0000-ECRAN.                                          
                    20 TS-FILLER-ECRAN        PIC X(16).                        
                    20 TS-CPRODEL-ECRAN       PIC X(05).                        
      ********** CUMUL DES QUOTAS DETAILS PREVUS POUR LE JOUR                   
                 15 TS-RVGQ0300-FICHIER.                                        
                    20 TS-QQUOTA-FICHIER      PIC S9(05) COMP-3.                
                    20 TS-WILLIM-FICHIER      PIC X(01).                        
                 15 TS-RVGQ0300-ECRAN.                                          
                    20 TS-QQUOTA-ECRAN        PIC S9(05) COMP-3.                
                    20 TS-WILLIM-ECRAN        PIC X(01).                        
                       88 TS-WILLIM-ECRAN-OUI              VALUE 'O'.           
                 15 TS-COULEUR             PIC X(01).                           
                 15 TS-FILLER2             PIC X(07).                           
                                                                                
