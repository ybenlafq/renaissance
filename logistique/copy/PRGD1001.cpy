      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE GD1001                       
      ******************************************************************        
      *                                                                         
       CLEF-GD1001             SECTION.                                         
      *                                                                         
           MOVE 'RVGD1000          '       TO   TABLE-NAME.                     
           MOVE 'GD1001'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-GD1001. EXIT.                                                   
                EJECT                                                           
                                                                                
