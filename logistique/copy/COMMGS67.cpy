      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3568  00730000
           03 COMM-GS67-APPLI REDEFINES COMM-GS00-FILLER.               00970010
      *                                                                 00720000
      *                         TABLE DES GROUPES MIS EN RESERVE              00
              05 COMM-GS67-TAB-X    OCCURS  5.                                03
                 07 COMM-GS67-TAB-J    OCCURS 18.                             03
                    10 COMM-GS67-CGROUP         PIC X(05).              00970003
                    10 COMM-GS67-LGROUP         PIC X(20).              00970003
                    10 COMM-GS67-AUTOR          PIC X.                  00970003
      *                                                                       00
      *                         LIBELLE MARQUE                                00
              05 COMM-GS67-LMARQ          PIC X(20).                          03
      *                                                                       00
      *                         LIBELLE FAMILLE                               00
              05 COMM-GS67-LFAM           PIC X(20).                          03
      *                                                                       00
      *                         INDICE POUR FOURNISSEUR                       00
              05 COMM-GS67-REPCDE         PIC X.                              03
      *                                                                       00
      *                         NOMBRE DE PAGE                                00
              05 COMM-GS67-NBRPAG         PIC 99.                             03
      *                                                                       00
      *                         NUMERO DE LA PAGE                             00
              05 COMM-GS67-NPAGE          PIC 99.                             03
      *                                                                 00720000
      *                         CODIC SUIVANT                                 00
              05 COMM-GS67-CODIC-SUIV     PIC X(7).                           03
      *                                                                 00720000
      *                         CODIC DACEM (O-N)                             00
              05 COMM-GS67-WDACEM         PIC X(1).                           03
      *                                                                 00720000
                                                                                
