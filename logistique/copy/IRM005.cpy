      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRM005 AU 20/02/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,07,BI,A,                          *        
      *                           19,03,BI,A,                          *        
      *                           22,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRM005.                                                        
            05 NOMETAT-IRM005           PIC X(6) VALUE 'IRM005'.                
            05 RUPTURES-IRM005.                                                 
           10 IRM005-TYPARTICLE         PIC X(05).                      007  005
           10 IRM005-NCODIC             PIC X(07).                      012  007
           10 IRM005-FILIALE            PIC X(03).                      019  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRM005-SEQUENCE           PIC S9(04) COMP.                022  002
      *--                                                                       
           10 IRM005-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRM005.                                                   
           10 IRM005-LAGREGATED1        PIC X(20).                      024  020
           10 IRM005-LAGREGATED2        PIC X(20).                      044  020
           10 IRM005-NCODICLIE          PIC X(07).                      064  007
            05 FILLER                      PIC X(442).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRM005-LONG           PIC S9(4)   COMP  VALUE +070.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRM005-LONG           PIC S9(4) COMP-5  VALUE +070.           
                                                                                
      *}                                                                        
