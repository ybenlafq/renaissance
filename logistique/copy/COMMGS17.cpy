      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010001
      * COMMAREA SPECIFIQUE PRG TGS17 (TGS10 -> MENU)    TR: GS10  *    00020001
      *               GESTION DES STOCKS ENTREPOT                  *    00030001
      *                                                                 00040001
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3534  00050001
      *                                                            *    00060001
      *        TRANSACTION GS17 : CONSULTATION DES STOCKS          *    00070001
      *                                                                 00080001
          02 COMM-GS17-APPLI REDEFINES COMM-GS10-APPLI.                 00090001
      *------------------------------ ZONE DONNEES TGS17                00100001
             03 COMM-GS17-DONNEES-TGS17.                                00110001
      *-------------------------- ZONE CODIC   PAGE                     00180001
                04 COMM-GS17-LIGCODICS         OCCURS 11.               00190001
                   05 COMM-GS17-NCODIC         PIC X(07).               00200001
                   05 COMM-GS17-CFAM           PIC X(05).               00200002
                   05 COMM-GS17-CMARQ          PIC X(05).               00210001
                   05 COMM-GS17-LREFFOURN      PIC X(20).               00220001
                   05 COMM-GS17-LSTATCOMP      PIC X(03).               00230001
                   05 COMM-GS17-WSENSAPPRO     PIC X(01).               00240001
                   05 COMM-GS17-WSENSVTE       PIC X(01).               00250001
                   05 COMM-GS17-ZONSTOCK.                               00260001
                      06 COMM-GS17-QSTOCK1     PIC 9(05) OCCURS 4.      00270001
                      06 COMM-GS17-QSTOCK2     PIC 9(03) OCCURS 3.      00280001
      *------------------------------ TABLE N� TS PR PAGINATION         00290001
                04 COMM-GS17-TABART         OCCURS 500.                 00300001
      *------------------------------ 1ERS IND TS DES PAGES <=> CODIC   00310001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-GS17-NUMTS          PIC S9(4) COMP.          00320001
      *--                                                                       
                   05 COMM-GS17-NUMTS          PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ INDICATEUR FIN DE TABLE           00330001
                04 COMM-GS17-INDPAG            PIC 9.                   00340001
      *------------------------------ DERNIER IND-TS                    00350001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS17-PAGSUI            PIC S9(4) COMP.          00360001
      *--                                                                       
                04 COMM-GS17-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN  IND-TS                    00370001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS17-PAGENC            PIC S9(4) COMP.          00380001
      *--                                                                       
                04 COMM-GS17-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00390001
                04 COMM-GS17-NUMPAG            PIC 9(3).                00400001
      *------------------------------ IND POUR RECH DS TS               00410001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS17-IND-RECH-TS       PIC S9(4) COMP.          00420001
      *--                                                                       
                04 COMM-GS17-IND-RECH-TS       PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ IND TS MAX                        00430001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS17-IND-TS-MAX        PIC S9(4) COMP.          00440001
      *--                                                                       
                04 COMM-GS17-IND-TS-MAX        PIC S9(4) COMP-5.                
      *}                                                                        
                04 COMM-GS17-PLAFD             PIC X.                           
                   88 COMM-GS17-PLAFD-OK       VALUE '1'.                       
                   88 COMM-GS17-PLAFD-KO       VALUE '2'.                       
      *------------------------------ ZONE DONNEES TGS49                00100001
             03 COMM-GS49-DONNEES-TGS49.                                00110001
                04 COMM-GS49-NCODIC            PIC X(7).                00470003
                04 COMM-GS49-CFAM              PIC X(5).                00470003
                04 COMM-GS49-CMARQ             PIC X(5).                00470003
                04 COMM-GS49-LREFFOURN         PIC X(20).               00470003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS49-ITEM              PIC S9(4)    COMP.       00470004
      *--                                                                       
                04 COMM-GS49-ITEM              PIC S9(4) COMP-5.                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GS49-POS-MAX           PIC S9(4)    COMP.       00470005
      *--                                                                       
                04 COMM-GS49-POS-MAX           PIC S9(4) COMP-5.                
      *}                                                                        
                04 COMM-GS49-VENDABLE          PIC S9(7)    COMP-3.     00470006
                04 COMM-GS49-COTE              PIC S9(7)    COMP-3.     00470007
                04 COMM-GS49-TRANSIT           PIC S9(7)    COMP-3.     00470008
                04 COMM-GS49-PRET              PIC S9(7)    COMP-3.     00470009
                04 COMM-GS49-HS                PIC S9(7)    COMP-3.     00470010
                04 COMM-GS49-TOTAL             PIC S9(7)    COMP-3.     00470011
                04 COMM-GS49-LITIGE            PIC S9(7)    COMP-3.     00470012
                04 COMM-GS49-DISPONIBLE        PIC S9(7)    COMP-3.     00470013
                04 COMM-GS49-RESDISPO          PIC S9(7)    COMP-3.     00470014
                04 COMM-GS49-RESFOUR           PIC S9(7)    COMP-3.     00470015
                04 COMM-GS49-RESCDE            PIC S9(7)    COMP-3.     00470016
             03 COMM-MU17-APPLI.                                        00420009
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-MU17-PAGSUI            PIC S9(04) COMP.         00743709
      *--                                                                       
                04 COMM-MU17-PAGSUI            PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-MU17-PAGENC            PIC S9(04) COMP.         00743909
      *--                                                                       
                04 COMM-MU17-PAGENC            PIC S9(04) COMP-5.               
      *}                                                                        
                04 COMM-MU17-NUMPAG            PIC 9(3).                00745009
                04 COMM-MU17-INDPAG            PIC 9.                   00747009
                04 COMM-MU17-TABTS             OCCURS 200.              00755011
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-MU17-NUMTS          PIC S9(04) COMP.         00757009
      *--                                                                       
                   05 COMM-MU17-NUMTS          PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-MU17-IND-RECH-TS       PIC S9(04) COMP.         00759009
      *--                                                                       
                04 COMM-MU17-IND-RECH-TS       PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-MU17-IND-TS-MAX        PIC S9(04) COMP.         00759210
      *--                                                                       
                04 COMM-MU17-IND-TS-MAX        PIC S9(04) COMP-5.               
      *}                                                                        
                04 COMM-MU17-TABSTK.                                    00780009
                   05 COMM-MU17-LIGSTK         OCCURS 13.               00800009
                      07 COMM-MU17-NSOCIETE    PIC X(3).                00840009
                      07 COMM-MU17-NLIEU       PIC X(3).                00840009
                      07 COMM-MU17-NSSLIEU     PIC X(01).               00860009
                      07 COMM-MU17-QSTOCK      PIC S9(5) COMP-3.        00890009
                      07 COMM-MU17-TYPELIEU    PIC X(5).                00932009
                      07 COMM-MU17-LTYPELIEU   PIC X(20).               00950009
                                                                                
