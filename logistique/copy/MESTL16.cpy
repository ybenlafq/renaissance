      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------         
      * COPY DE LA PARTIE DATA DU MESSAGE MQ RE�U POUR MTL16                    
      * >JUSTE DERRIERE L'ENTETE G�N�RALIS�E MESSLENT                           
      * >CORRESPOND � LA COPIE 400 CCBEMQTLTW                                   
      *----------------------------------------------------------------         
      * (LONG=23023)                                      DEBUT   LONG          
             10 MESTL16-DATA.                                                   
      *            CODE PROFIL DE LA TOURNEE                  1   5             
                20 MESTL16-PROFIL          PIC   X(05).                         
      *            DATE DE LIVRAISON DE LA TOURNEE            6   8             
                20 MESTL16-DDELIV          PIC   X(08).                         
      *            CODE DE LA TOURNEE                        14   3             
                20 MESTL16-NTOUR           PIC   X(03).                         
      *            NOMBRE DE VENTES DANS LA TOURNEE          17   3             
                20 MESTL16-NBVTE           PIC   9(03).                         
      *            NOMBRE DE LIGNES DANS LA TOURNEE          20   3             
                20 MESTL16-NBLGN           PIC   9(03).                         
      *            LIGNES DE VENTES                          23   23000         
                20 MESTL16-LIGNES.                                              
      *            LIGNE                              23 + 46*I   46            
                   25 MESTL16-LGE-ENR      OCCURS 500.                          
                      30 MESTL16-VENTE.                                         
      *                     LIEU DE VENTE             23 + 46*I   3             
                         35 MESTL16-NLIEU  PIC   X(03).                         
      *                     N� VENTE                  26 + 46*I   7             
                         35 MESTL16-NORDV  PIC   X(07).                         
      *                                               33 + 46*I   7             
                      30 MESTL16-NCODIC    PIC   X(07).                         
      *                  NSEQNQ DE LA VENTE(NMD)      40 + 46*I   5             
                      30 MESTL16-NSEQNQ    PIC   9(05).                         
      *                  QUANTIT� � LIVRER            45 + 46*I   5             
                      30 MESTL16-QCOMM     PIC   9(05).                         
      *                  QUANTIT� LIVR�E (R�ELLEMENT) 50 + 46*I   5             
                      30 MESTL16-QLIVRE    PIC   9(05).                         
      *                  MONTANT DU BL                55 + 46*I   9             
                      30 MESTL16-MTBL      PIC  S9(07)V99.                      
      *                  TYPE DE R�GLEMENT            64 + 46*I   1             
                      30 MESTL16-CTYPE     PIC   X(01).                         
      *                  RETOUR GLOBALE DE LA VENTE   65 + 46*I   2             
                      30 MESTL16-RETGLO    PIC   X(02).                         
      *                  RETOUR UNITAIRE DE LA LIGNE  67 + 46*I   2             
                      30 MESTL16-RETUNI    PIC   X(02).                         
                                                                                
