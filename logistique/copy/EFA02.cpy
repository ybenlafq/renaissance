      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFA02   EFA02                                              00000020
      ***************************************************************** 00000030
       01   EFA02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSATL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSATL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSATF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSATI    PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCASEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCASEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCASEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCASEI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMTAIREL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCMTAIREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMTAIREF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMTAIREI      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLSOCI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIV1L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCLIV1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIV1F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCLIV1I   PIC X(10).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIV1L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLLIV1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIV1F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLLIV1I   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCPROFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPROFF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCPROFI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROFL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLPROFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPROFF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLPROFI   PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIV2L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCLIV2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIV2F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCLIV2I   PIC X(10).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIV2L   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLLIV2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIV2F   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLLIV2I   PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDELIVL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDDELIVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDELIVF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDDELIVI  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTOURNEEL     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MCTOURNEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTOURNEEF     PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCTOURNEEI     PIC X(3).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIV3L   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCLIV3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIV3F   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCLIV3I   PIC X(10).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIV3L   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLLIV3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIV3F   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLLIV3I   PIC X(20).                                      00000810
           02 M267I OCCURS   12 TIMES .                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNMAGI  PIC X(3).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MNVENTEI     PIC X(7).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPEL      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MCTYPEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCTYPEF      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCTYPEI      PIC X.                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPEASSL   COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MCTYPEASSL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPEASSF   PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MCTYPEASSI   PIC X(2).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPLAGEL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MLPLAGEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLPLAGEF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MLPLAGEI     PIC X(8).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLNOML  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MLNOML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLNOMF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MLNOMI  PIC X(15).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPTTL  COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MCPTTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCPTTF  PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MCPTTI  PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLOCALITEL   COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MLOCALITEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLOCALITEF   PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MLOCALITEI   PIC X(22).                                 00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRENEAUL    COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MCRENEAUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCRENEAUF    PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MCRENEAUI    PIC X(7).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MZONCMDI  PIC X(15).                                      00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MLIBERRI  PIC X(58).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCODTRAI  PIC X(4).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCICSI    PIC X(5).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MNETNAMI  PIC X(8).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MSCREENI  PIC X(4).                                       00001420
      ***************************************************************** 00001430
      * SDF: EFA02   EFA02                                              00001440
      ***************************************************************** 00001450
       01   EFA02O REDEFINES EFA02I.                                    00001460
           02 FILLER    PIC X(12).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MDATJOUA  PIC X.                                          00001490
           02 MDATJOUC  PIC X.                                          00001500
           02 MDATJOUP  PIC X.                                          00001510
           02 MDATJOUH  PIC X.                                          00001520
           02 MDATJOUV  PIC X.                                          00001530
           02 MDATJOUO  PIC X(10).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MTIMJOUA  PIC X.                                          00001560
           02 MTIMJOUC  PIC X.                                          00001570
           02 MTIMJOUP  PIC X.                                          00001580
           02 MTIMJOUH  PIC X.                                          00001590
           02 MTIMJOUV  PIC X.                                          00001600
           02 MTIMJOUO  PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MPAGEA    PIC X.                                          00001630
           02 MPAGEC    PIC X.                                          00001640
           02 MPAGEP    PIC X.                                          00001650
           02 MPAGEH    PIC X.                                          00001660
           02 MPAGEV    PIC X.                                          00001670
           02 MPAGEO    PIC X(3).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MWFONCA   PIC X.                                          00001700
           02 MWFONCC   PIC X.                                          00001710
           02 MWFONCP   PIC X.                                          00001720
           02 MWFONCH   PIC X.                                          00001730
           02 MWFONCV   PIC X.                                          00001740
           02 MWFONCO   PIC X(3).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNSATA    PIC X.                                          00001770
           02 MNSATC    PIC X.                                          00001780
           02 MNSATP    PIC X.                                          00001790
           02 MNSATH    PIC X.                                          00001800
           02 MNSATV    PIC X.                                          00001810
           02 MNSATO    PIC X(2).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MNCASEA   PIC X.                                          00001840
           02 MNCASEC   PIC X.                                          00001850
           02 MNCASEP   PIC X.                                          00001860
           02 MNCASEH   PIC X.                                          00001870
           02 MNCASEV   PIC X.                                          00001880
           02 MNCASEO   PIC X(3).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCMTAIREA      PIC X.                                     00001910
           02 MCMTAIREC PIC X.                                          00001920
           02 MCMTAIREP PIC X.                                          00001930
           02 MCMTAIREH PIC X.                                          00001940
           02 MCMTAIREV PIC X.                                          00001950
           02 MCMTAIREO      PIC X(5).                                  00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNSOCA    PIC X.                                          00001980
           02 MNSOCC    PIC X.                                          00001990
           02 MNSOCP    PIC X.                                          00002000
           02 MNSOCH    PIC X.                                          00002010
           02 MNSOCV    PIC X.                                          00002020
           02 MNSOCO    PIC X(3).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MLSOCA    PIC X.                                          00002050
           02 MLSOCC    PIC X.                                          00002060
           02 MLSOCP    PIC X.                                          00002070
           02 MLSOCH    PIC X.                                          00002080
           02 MLSOCV    PIC X.                                          00002090
           02 MLSOCO    PIC X(20).                                      00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MCLIV1A   PIC X.                                          00002120
           02 MCLIV1C   PIC X.                                          00002130
           02 MCLIV1P   PIC X.                                          00002140
           02 MCLIV1H   PIC X.                                          00002150
           02 MCLIV1V   PIC X.                                          00002160
           02 MCLIV1O   PIC X(10).                                      00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLLIV1A   PIC X.                                          00002190
           02 MLLIV1C   PIC X.                                          00002200
           02 MLLIV1P   PIC X.                                          00002210
           02 MLLIV1H   PIC X.                                          00002220
           02 MLLIV1V   PIC X.                                          00002230
           02 MLLIV1O   PIC X(20).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCPROFA   PIC X.                                          00002260
           02 MCPROFC   PIC X.                                          00002270
           02 MCPROFP   PIC X.                                          00002280
           02 MCPROFH   PIC X.                                          00002290
           02 MCPROFV   PIC X.                                          00002300
           02 MCPROFO   PIC X(5).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLPROFA   PIC X.                                          00002330
           02 MLPROFC   PIC X.                                          00002340
           02 MLPROFP   PIC X.                                          00002350
           02 MLPROFH   PIC X.                                          00002360
           02 MLPROFV   PIC X.                                          00002370
           02 MLPROFO   PIC X(20).                                      00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MCLIV2A   PIC X.                                          00002400
           02 MCLIV2C   PIC X.                                          00002410
           02 MCLIV2P   PIC X.                                          00002420
           02 MCLIV2H   PIC X.                                          00002430
           02 MCLIV2V   PIC X.                                          00002440
           02 MCLIV2O   PIC X(10).                                      00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MLLIV2A   PIC X.                                          00002470
           02 MLLIV2C   PIC X.                                          00002480
           02 MLLIV2P   PIC X.                                          00002490
           02 MLLIV2H   PIC X.                                          00002500
           02 MLLIV2V   PIC X.                                          00002510
           02 MLLIV2O   PIC X(20).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MDDELIVA  PIC X.                                          00002540
           02 MDDELIVC  PIC X.                                          00002550
           02 MDDELIVP  PIC X.                                          00002560
           02 MDDELIVH  PIC X.                                          00002570
           02 MDDELIVV  PIC X.                                          00002580
           02 MDDELIVO  PIC X(8).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCTOURNEEA     PIC X.                                     00002610
           02 MCTOURNEEC     PIC X.                                     00002620
           02 MCTOURNEEP     PIC X.                                     00002630
           02 MCTOURNEEH     PIC X.                                     00002640
           02 MCTOURNEEV     PIC X.                                     00002650
           02 MCTOURNEEO     PIC X(3).                                  00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCLIV3A   PIC X.                                          00002680
           02 MCLIV3C   PIC X.                                          00002690
           02 MCLIV3P   PIC X.                                          00002700
           02 MCLIV3H   PIC X.                                          00002710
           02 MCLIV3V   PIC X.                                          00002720
           02 MCLIV3O   PIC X(10).                                      00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MLLIV3A   PIC X.                                          00002750
           02 MLLIV3C   PIC X.                                          00002760
           02 MLLIV3P   PIC X.                                          00002770
           02 MLLIV3H   PIC X.                                          00002780
           02 MLLIV3V   PIC X.                                          00002790
           02 MLLIV3O   PIC X(20).                                      00002800
           02 M267O OCCURS   12 TIMES .                                 00002810
             03 FILLER       PIC X(2).                                  00002820
             03 MNMAGA  PIC X.                                          00002830
             03 MNMAGC  PIC X.                                          00002840
             03 MNMAGP  PIC X.                                          00002850
             03 MNMAGH  PIC X.                                          00002860
             03 MNMAGV  PIC X.                                          00002870
             03 MNMAGO  PIC X(3).                                       00002880
             03 FILLER       PIC X(2).                                  00002890
             03 MNVENTEA     PIC X.                                     00002900
             03 MNVENTEC     PIC X.                                     00002910
             03 MNVENTEP     PIC X.                                     00002920
             03 MNVENTEH     PIC X.                                     00002930
             03 MNVENTEV     PIC X.                                     00002940
             03 MNVENTEO     PIC X(7).                                  00002950
             03 FILLER       PIC X(2).                                  00002960
             03 MCTYPEA      PIC X.                                     00002970
             03 MCTYPEC PIC X.                                          00002980
             03 MCTYPEP PIC X.                                          00002990
             03 MCTYPEH PIC X.                                          00003000
             03 MCTYPEV PIC X.                                          00003010
             03 MCTYPEO      PIC X.                                     00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MCTYPEASSA   PIC X.                                     00003040
             03 MCTYPEASSC   PIC X.                                     00003050
             03 MCTYPEASSP   PIC X.                                     00003060
             03 MCTYPEASSH   PIC X.                                     00003070
             03 MCTYPEASSV   PIC X.                                     00003080
             03 MCTYPEASSO   PIC X(2).                                  00003090
             03 FILLER       PIC X(2).                                  00003100
             03 MLPLAGEA     PIC X.                                     00003110
             03 MLPLAGEC     PIC X.                                     00003120
             03 MLPLAGEP     PIC X.                                     00003130
             03 MLPLAGEH     PIC X.                                     00003140
             03 MLPLAGEV     PIC X.                                     00003150
             03 MLPLAGEO     PIC X(8).                                  00003160
             03 FILLER       PIC X(2).                                  00003170
             03 MLNOMA  PIC X.                                          00003180
             03 MLNOMC  PIC X.                                          00003190
             03 MLNOMP  PIC X.                                          00003200
             03 MLNOMH  PIC X.                                          00003210
             03 MLNOMV  PIC X.                                          00003220
             03 MLNOMO  PIC X(15).                                      00003230
             03 FILLER       PIC X(2).                                  00003240
             03 MCPTTA  PIC X.                                          00003250
             03 MCPTTC  PIC X.                                          00003260
             03 MCPTTP  PIC X.                                          00003270
             03 MCPTTH  PIC X.                                          00003280
             03 MCPTTV  PIC X.                                          00003290
             03 MCPTTO  PIC X(5).                                       00003300
             03 FILLER       PIC X(2).                                  00003310
             03 MLOCALITEA   PIC X.                                     00003320
             03 MLOCALITEC   PIC X.                                     00003330
             03 MLOCALITEP   PIC X.                                     00003340
             03 MLOCALITEH   PIC X.                                     00003350
             03 MLOCALITEV   PIC X.                                     00003360
             03 MLOCALITEO   PIC X(22).                                 00003370
             03 FILLER       PIC X(2).                                  00003380
             03 MCRENEAUA    PIC X.                                     00003390
             03 MCRENEAUC    PIC X.                                     00003400
             03 MCRENEAUP    PIC X.                                     00003410
             03 MCRENEAUH    PIC X.                                     00003420
             03 MCRENEAUV    PIC X.                                     00003430
             03 MCRENEAUO    PIC X(7).                                  00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MZONCMDA  PIC X.                                          00003460
           02 MZONCMDC  PIC X.                                          00003470
           02 MZONCMDP  PIC X.                                          00003480
           02 MZONCMDH  PIC X.                                          00003490
           02 MZONCMDV  PIC X.                                          00003500
           02 MZONCMDO  PIC X(15).                                      00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MLIBERRA  PIC X.                                          00003530
           02 MLIBERRC  PIC X.                                          00003540
           02 MLIBERRP  PIC X.                                          00003550
           02 MLIBERRH  PIC X.                                          00003560
           02 MLIBERRV  PIC X.                                          00003570
           02 MLIBERRO  PIC X(58).                                      00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MCODTRAA  PIC X.                                          00003600
           02 MCODTRAC  PIC X.                                          00003610
           02 MCODTRAP  PIC X.                                          00003620
           02 MCODTRAH  PIC X.                                          00003630
           02 MCODTRAV  PIC X.                                          00003640
           02 MCODTRAO  PIC X(4).                                       00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MCICSA    PIC X.                                          00003670
           02 MCICSC    PIC X.                                          00003680
           02 MCICSP    PIC X.                                          00003690
           02 MCICSH    PIC X.                                          00003700
           02 MCICSV    PIC X.                                          00003710
           02 MCICSO    PIC X(5).                                       00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MNETNAMA  PIC X.                                          00003740
           02 MNETNAMC  PIC X.                                          00003750
           02 MNETNAMP  PIC X.                                          00003760
           02 MNETNAMH  PIC X.                                          00003770
           02 MNETNAMV  PIC X.                                          00003780
           02 MNETNAMO  PIC X(8).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MSCREENA  PIC X.                                          00003810
           02 MSCREENC  PIC X.                                          00003820
           02 MSCREENP  PIC X.                                          00003830
           02 MSCREENH  PIC X.                                          00003840
           02 MSCREENV  PIC X.                                          00003850
           02 MSCREENO  PIC X(4).                                       00003860
                                                                                
