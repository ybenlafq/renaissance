      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRM035 AU 01/09/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,20,BI,A,                          *        
      *                           37,05,BI,A,                          *        
      *                           42,03,BI,A,                          *        
      *                           45,03,BI,A,                          *        
      *                           48,01,BI,A,                          *        
      *                           49,03,PD,A,                          *        
      *                           52,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRM035.                                                        
            05 NOMETAT-IRM035           PIC X(6) VALUE 'IRM035'.                
            05 RUPTURES-IRM035.                                                 
           10 IRM035-CHEFPROD           PIC X(05).                      007  005
           10 IRM035-CFAM               PIC X(05).                      012  005
           10 IRM035-LAGREGATED         PIC X(20).                      017  020
           10 IRM035-CGROUP             PIC X(05).                      037  005
           10 IRM035-NSOCIETE           PIC X(03).                      042  003
           10 IRM035-NLIEU              PIC X(03).                      045  003
           10 IRM035-W8020-RUP          PIC X(01).                      048  001
           10 IRM035-QRANG              PIC S9(05)      COMP-3.         049  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRM035-SEQUENCE           PIC S9(04) COMP.                052  002
      *--                                                                       
           10 IRM035-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRM035.                                                   
           10 IRM035-AFF8020            PIC X(02).                      054  002
           10 IRM035-CMARQ              PIC X(05).                      056  005
           10 IRM035-LCHEFPROD          PIC X(20).                      061  020
           10 IRM035-LFAM               PIC X(20).                      081  020
           10 IRM035-LMAG               PIC X(20).                      101  020
           10 IRM035-LREFFOURN          PIC X(20).                      121  020
           10 IRM035-NCODIC             PIC X(07).                      141  007
           10 IRM035-WASSORTMAG         PIC X(01).                      148  001
           10 IRM035-WASSORTSTD         PIC X(01).                      149  001
           10 IRM035-WE9                PIC X(01).                      150  001
           10 IRM035-QEXPO              PIC S9(05)      COMP-3.         151  003
           10 IRM035-QIND-CODIC-SOC     PIC S9(03)V9(4) COMP-3.         154  003
           10 IRM035-QIND-SEG-ENT       PIC S9(03)V9(4) COMP-3.         157  003
           10 IRM035-QIND-SEG-MAG       PIC S9(03)V9(4) COMP-3.         160  003
           10 IRM035-QIND-SEG-SOC       PIC S9(03)V9(4) COMP-3.         163  003
           10 IRM035-QINDCOD            PIC S9(03)V9(4) COMP-3.         166  003
           10 IRM035-QINDENT            PIC S9(03)V9(4) COMP-3.         169  003
           10 IRM035-QLS                PIC S9(05)      COMP-3.         172  003
           10 IRM035-QSM                PIC S9(07)      COMP-3.         175  004
           10 IRM035-QSMS               PIC S9(07)      COMP-3.         179  004
           10 IRM035-QSO                PIC S9(07)      COMP-3.         183  004
           10 IRM035-QSOS               PIC S9(07)      COMP-3.         187  004
           10 IRM035-QSTOCK             PIC S9(07)      COMP-3.         191  004
           10 IRM035-QV8SC              PIC S9(07)      COMP-3.         195  004
           10 IRM035-QV8SR              PIC S9(07)      COMP-3.         199  004
           10 IRM035-QSM-MAG            PIC S9(07)      COMP-3.         203  004
           10 IRM035-QSMS-MAG           PIC S9(07)      COMP-3.         207  004
           10 IRM035-QSO-MAG            PIC S9(07)      COMP-3.         211  004
           10 IRM035-QSOS-MAG           PIC S9(07)      COMP-3.         215  004
           10 IRM035-NSOCATTACHE        PIC  X(03).                     219  003
           10 IRM035-NMAGATTACHE        PIC  X(03).                     222  003
            05 FILLER                      PIC X(282).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IRM035-LONG           PIC S9(4)   COMP  VALUE +230.           
      *                                                                         
      *--                                                                       
        01  DSECT-IRM035-LONG           PIC S9(4) COMP-5  VALUE +230.           
                                                                                
      *}                                                                        
