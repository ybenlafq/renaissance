      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM8501                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM8501                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM8501.                                                            
           02  RM85-CGROUP                                                      
               PIC X(0005).                                                     
           02  RM85-CFAM                                                        
               PIC X(0005).                                                     
           02  RM85-LAGREGATED                                                  
               PIC X(0020).                                                     
           02  RM85-NCODIC                                                      
               PIC X(0007).                                                     
           02  RM85-QRANG                                                       
               PIC S9(5) COMP-3.                                                
           02  RM85-QRANGHER                                                    
               PIC S9(5) COMP-3.                                                
           02  RM85-QRANGP                                                      
               PIC S9(5) COMP-3.                                                
           02  RM85-QPOIDS                                                      
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM85-QPOIDSR                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM85-QPOIDSP                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM85-QNBREF                                                      
               PIC S9(5) COMP-3.                                                
           02  RM85-QNBREFR                                                     
               PIC S9(5) COMP-3.                                                
           02  RM85-QNBREFP                                                     
               PIC S9(5) COMP-3.                                                
           02  RM85-CVDESCRIPT1                                                 
               PIC X(0005).                                                     
           02  RM85-CVDESCRIPT2                                                 
               PIC X(0005).                                                     
           02  RM85-CVDESCRIPT3                                                 
               PIC X(0005).                                                     
           02  RM85-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  RM85-QV4SER                                                      
               PIC S9(7) COMP-3.                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM8501                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM8501-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-CGROUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-CGROUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-LAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-LAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-QRANG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-QRANG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-QRANGHER-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-QRANGHER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-QRANGP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-QRANGP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-QPOIDS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-QPOIDS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-QPOIDSR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-QPOIDSR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-QPOIDSP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-QPOIDSP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-QNBREF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-QNBREF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-QNBREFR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-QNBREFR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-QNBREFP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-QNBREFP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-CVDESCRIPT1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-CVDESCRIPT1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-CVDESCRIPT2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-CVDESCRIPT2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-CVDESCRIPT3-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-CVDESCRIPT3-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM85-QV4SER-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM85-QV4SER-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
