      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM22   ERM22                                              00000020
      ***************************************************************** 00000030
       01   ERM62I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CRMGROUPL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 CRMGROUPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 CRMGROUPF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 CRMGROUPI      PIC X(5).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGESL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNPAGESL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNPAGESF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNPAGESI  PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFGL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCHEFGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFGF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCHEFGI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFGL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLCHEFGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCHEFGF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCHEFGI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEB4L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDDEB4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDDEB4F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDDEB4I   PIC X(10).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFIN4L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MDFIN4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDFIN4F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDFIN4I   PIC X(10).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEB3L   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDDEB3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDDEB3F   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDDEB3I   PIC X(10).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFIN3L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDFIN3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDFIN3F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDFIN3I   PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEB2L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MDDEB2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDDEB2F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDDEB2I   PIC X(10).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFIN2L   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDFIN2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDFIN2F   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDFIN2I   PIC X(10).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEB1L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDDEB1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDDEB1F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDDEB1I   PIC X(10).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFIN1L   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDFIN1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDFIN1F   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDFIN1I   PIC X(10).                                      00000730
           02 MLAGREGD OCCURS   5 TIMES .                               00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLAGREGL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MLAGREGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLAGREGF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MLAGREGI     PIC X(20).                                 00000780
           02 MQHV4D OCCURS   5 TIMES .                                 00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQHV4L  COMP PIC S9(4).                                 00000800
      *--                                                                       
             03 MQHV4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQHV4F  PIC X.                                          00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MQHV4I  PIC X(6).                                       00000830
           02 MQHV3D OCCURS   5 TIMES .                                 00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQHV3L  COMP PIC S9(4).                                 00000850
      *--                                                                       
             03 MQHV3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQHV3F  PIC X.                                          00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MQHV3I  PIC X(6).                                       00000880
           02 MQHV2D OCCURS   5 TIMES .                                 00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQHV2L  COMP PIC S9(4).                                 00000900
      *--                                                                       
             03 MQHV2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQHV2F  PIC X.                                          00000910
             03 FILLER  PIC X(4).                                       00000920
             03 MQHV2I  PIC X(6).                                       00000930
           02 MQHV1D OCCURS   5 TIMES .                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQHV1L  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MQHV1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQHV1F  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQHV1I  PIC X(6).                                       00000980
           02 MQPM1D OCCURS   5 TIMES .                                 00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPM1L  COMP PIC S9(4).                                 00001000
      *--                                                                       
             03 MQPM1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQPM1F  PIC X.                                          00001010
             03 FILLER  PIC X(4).                                       00001020
             03 MQPM1I  PIC X(6).                                       00001030
           02 MQPM2D OCCURS   5 TIMES .                                 00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPM2L  COMP PIC S9(4).                                 00001050
      *--                                                                       
             03 MQPM2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQPM2F  PIC X.                                          00001060
             03 FILLER  PIC X(4).                                       00001070
             03 MQPM2I  PIC X(6).                                       00001080
           02 MQPM3D OCCURS   5 TIMES .                                 00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPM3L  COMP PIC S9(4).                                 00001100
      *--                                                                       
             03 MQPM3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQPM3F  PIC X.                                          00001110
             03 FILLER  PIC X(4).                                       00001120
             03 MQPM3I  PIC X(6).                                       00001130
           02 MQSV4D OCCURS   5 TIMES .                                 00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSV4L  COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MQSV4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSV4F  PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MQSV4I  PIC X(6).                                       00001180
           02 MQSV3D OCCURS   5 TIMES .                                 00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSV3L  COMP PIC S9(4).                                 00001200
      *--                                                                       
             03 MQSV3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSV3F  PIC X.                                          00001210
             03 FILLER  PIC X(4).                                       00001220
             03 MQSV3I  PIC X(6).                                       00001230
           02 MQSV2D OCCURS   5 TIMES .                                 00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSV2L  COMP PIC S9(4).                                 00001250
      *--                                                                       
             03 MQSV2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSV2F  PIC X.                                          00001260
             03 FILLER  PIC X(4).                                       00001270
             03 MQSV2I  PIC X(6).                                       00001280
           02 MQSV1D OCCURS   5 TIMES .                                 00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSV1L  COMP PIC S9(4).                                 00001300
      *--                                                                       
             03 MQSV1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSV1F  PIC X.                                          00001310
             03 FILLER  PIC X(4).                                       00001320
             03 MQSV1I  PIC X(6).                                       00001330
           02 MQPV0D OCCURS   5 TIMES .                                 00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPV0L  COMP PIC S9(4).                                 00001350
      *--                                                                       
             03 MQPV0L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQPV0F  PIC X.                                          00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MQPV0I  PIC X(6).                                       00001380
           02 MQPVF1D OCCURS   5 TIMES .                                00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVF1L      COMP PIC S9(4).                            00001400
      *--                                                                       
             03 MQPVF1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPVF1F      PIC X.                                     00001410
             03 FILLER  PIC X(4).                                       00001420
             03 MQPVF1I      PIC X(6).                                  00001430
           02 MQPVF2D OCCURS   5 TIMES .                                00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVF2L      COMP PIC S9(4).                            00001450
      *--                                                                       
             03 MQPVF2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPVF2F      PIC X.                                     00001460
             03 FILLER  PIC X(4).                                       00001470
             03 MQPVF2I      PIC X(6).                                  00001480
           02 MQPVF3D OCCURS   5 TIMES .                                00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVF3L      COMP PIC S9(4).                            00001500
      *--                                                                       
             03 MQPVF3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPVF3F      PIC X.                                     00001510
             03 FILLER  PIC X(4).                                       00001520
             03 MQPVF3I      PIC X(6).                                  00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MLIBERRI  PIC X(78).                                      00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MCODTRAI  PIC X(4).                                       00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MCICSI    PIC X(5).                                       00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MNETNAMI  PIC X(8).                                       00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MSCREENI  PIC X(4).                                       00001730
      ***************************************************************** 00001740
      * SDF: ERM22   ERM22                                              00001750
      ***************************************************************** 00001760
       01   ERM62O REDEFINES ERM62I.                                    00001770
           02 FILLER    PIC X(12).                                      00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MDATJOUA  PIC X.                                          00001800
           02 MDATJOUC  PIC X.                                          00001810
           02 MDATJOUP  PIC X.                                          00001820
           02 MDATJOUH  PIC X.                                          00001830
           02 MDATJOUV  PIC X.                                          00001840
           02 MDATJOUO  PIC X(10).                                      00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MTIMJOUA  PIC X.                                          00001870
           02 MTIMJOUC  PIC X.                                          00001880
           02 MTIMJOUP  PIC X.                                          00001890
           02 MTIMJOUH  PIC X.                                          00001900
           02 MTIMJOUV  PIC X.                                          00001910
           02 MTIMJOUO  PIC X(5).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 CRMGROUPA      PIC X.                                     00001940
           02 CRMGROUPC PIC X.                                          00001950
           02 CRMGROUPP PIC X.                                          00001960
           02 CRMGROUPH PIC X.                                          00001970
           02 CRMGROUPV PIC X.                                          00001980
           02 CRMGROUPO      PIC X(5).                                  00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MNPAGEA   PIC X.                                          00002010
           02 MNPAGEC   PIC X.                                          00002020
           02 MNPAGEP   PIC X.                                          00002030
           02 MNPAGEH   PIC X.                                          00002040
           02 MNPAGEV   PIC X.                                          00002050
           02 MNPAGEO   PIC X(2).                                       00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MNPAGESA  PIC X.                                          00002080
           02 MNPAGESC  PIC X.                                          00002090
           02 MNPAGESP  PIC X.                                          00002100
           02 MNPAGESH  PIC X.                                          00002110
           02 MNPAGESV  PIC X.                                          00002120
           02 MNPAGESO  PIC X(2).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MCHEFGA   PIC X.                                          00002150
           02 MCHEFGC   PIC X.                                          00002160
           02 MCHEFGP   PIC X.                                          00002170
           02 MCHEFGH   PIC X.                                          00002180
           02 MCHEFGV   PIC X.                                          00002190
           02 MCHEFGO   PIC X(5).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MLCHEFGA  PIC X.                                          00002220
           02 MLCHEFGC  PIC X.                                          00002230
           02 MLCHEFGP  PIC X.                                          00002240
           02 MLCHEFGH  PIC X.                                          00002250
           02 MLCHEFGV  PIC X.                                          00002260
           02 MLCHEFGO  PIC X(20).                                      00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MCFAMA    PIC X.                                          00002290
           02 MCFAMC    PIC X.                                          00002300
           02 MCFAMP    PIC X.                                          00002310
           02 MCFAMH    PIC X.                                          00002320
           02 MCFAMV    PIC X.                                          00002330
           02 MCFAMO    PIC X(5).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MLFAMA    PIC X.                                          00002360
           02 MLFAMC    PIC X.                                          00002370
           02 MLFAMP    PIC X.                                          00002380
           02 MLFAMH    PIC X.                                          00002390
           02 MLFAMV    PIC X.                                          00002400
           02 MLFAMO    PIC X(20).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MDDEB4A   PIC X.                                          00002430
           02 MDDEB4C   PIC X.                                          00002440
           02 MDDEB4P   PIC X.                                          00002450
           02 MDDEB4H   PIC X.                                          00002460
           02 MDDEB4V   PIC X.                                          00002470
           02 MDDEB4O   PIC X(10).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MDFIN4A   PIC X.                                          00002500
           02 MDFIN4C   PIC X.                                          00002510
           02 MDFIN4P   PIC X.                                          00002520
           02 MDFIN4H   PIC X.                                          00002530
           02 MDFIN4V   PIC X.                                          00002540
           02 MDFIN4O   PIC X(10).                                      00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MDDEB3A   PIC X.                                          00002570
           02 MDDEB3C   PIC X.                                          00002580
           02 MDDEB3P   PIC X.                                          00002590
           02 MDDEB3H   PIC X.                                          00002600
           02 MDDEB3V   PIC X.                                          00002610
           02 MDDEB3O   PIC X(10).                                      00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MDFIN3A   PIC X.                                          00002640
           02 MDFIN3C   PIC X.                                          00002650
           02 MDFIN3P   PIC X.                                          00002660
           02 MDFIN3H   PIC X.                                          00002670
           02 MDFIN3V   PIC X.                                          00002680
           02 MDFIN3O   PIC X(10).                                      00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MDDEB2A   PIC X.                                          00002710
           02 MDDEB2C   PIC X.                                          00002720
           02 MDDEB2P   PIC X.                                          00002730
           02 MDDEB2H   PIC X.                                          00002740
           02 MDDEB2V   PIC X.                                          00002750
           02 MDDEB2O   PIC X(10).                                      00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MDFIN2A   PIC X.                                          00002780
           02 MDFIN2C   PIC X.                                          00002790
           02 MDFIN2P   PIC X.                                          00002800
           02 MDFIN2H   PIC X.                                          00002810
           02 MDFIN2V   PIC X.                                          00002820
           02 MDFIN2O   PIC X(10).                                      00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MDDEB1A   PIC X.                                          00002850
           02 MDDEB1C   PIC X.                                          00002860
           02 MDDEB1P   PIC X.                                          00002870
           02 MDDEB1H   PIC X.                                          00002880
           02 MDDEB1V   PIC X.                                          00002890
           02 MDDEB1O   PIC X(10).                                      00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MDFIN1A   PIC X.                                          00002920
           02 MDFIN1C   PIC X.                                          00002930
           02 MDFIN1P   PIC X.                                          00002940
           02 MDFIN1H   PIC X.                                          00002950
           02 MDFIN1V   PIC X.                                          00002960
           02 MDFIN1O   PIC X(10).                                      00002970
           02 DFHMS1 OCCURS   5 TIMES .                                 00002980
             03 FILLER       PIC X(2).                                  00002990
             03 MLAGREGA     PIC X.                                     00003000
             03 MLAGREGC     PIC X.                                     00003010
             03 MLAGREGP     PIC X.                                     00003020
             03 MLAGREGH     PIC X.                                     00003030
             03 MLAGREGV     PIC X.                                     00003040
             03 MLAGREGO     PIC X(20).                                 00003050
           02 DFHMS2 OCCURS   5 TIMES .                                 00003060
             03 FILLER       PIC X(2).                                  00003070
             03 MQHV4A  PIC X.                                          00003080
             03 MQHV4C  PIC X.                                          00003090
             03 MQHV4P  PIC X.                                          00003100
             03 MQHV4H  PIC X.                                          00003110
             03 MQHV4V  PIC X.                                          00003120
             03 MQHV4O  PIC ZZZZZZ.                                     00003130
           02 DFHMS3 OCCURS   5 TIMES .                                 00003140
             03 FILLER       PIC X(2).                                  00003150
             03 MQHV3A  PIC X.                                          00003160
             03 MQHV3C  PIC X.                                          00003170
             03 MQHV3P  PIC X.                                          00003180
             03 MQHV3H  PIC X.                                          00003190
             03 MQHV3V  PIC X.                                          00003200
             03 MQHV3O  PIC ZZZZZZ.                                     00003210
           02 DFHMS4 OCCURS   5 TIMES .                                 00003220
             03 FILLER       PIC X(2).                                  00003230
             03 MQHV2A  PIC X.                                          00003240
             03 MQHV2C  PIC X.                                          00003250
             03 MQHV2P  PIC X.                                          00003260
             03 MQHV2H  PIC X.                                          00003270
             03 MQHV2V  PIC X.                                          00003280
             03 MQHV2O  PIC ZZZZZZ.                                     00003290
           02 DFHMS5 OCCURS   5 TIMES .                                 00003300
             03 FILLER       PIC X(2).                                  00003310
             03 MQHV1A  PIC X.                                          00003320
             03 MQHV1C  PIC X.                                          00003330
             03 MQHV1P  PIC X.                                          00003340
             03 MQHV1H  PIC X.                                          00003350
             03 MQHV1V  PIC X.                                          00003360
             03 MQHV1O  PIC ZZZZZZ.                                     00003370
           02 DFHMS6 OCCURS   5 TIMES .                                 00003380
             03 FILLER       PIC X(2).                                  00003390
             03 MQPM1A  PIC X.                                          00003400
             03 MQPM1C  PIC X.                                          00003410
             03 MQPM1P  PIC X.                                          00003420
             03 MQPM1H  PIC X.                                          00003430
             03 MQPM1V  PIC X.                                          00003440
             03 MQPM1O  PIC X(6).                                       00003450
           02 DFHMS7 OCCURS   5 TIMES .                                 00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MQPM2A  PIC X.                                          00003480
             03 MQPM2C  PIC X.                                          00003490
             03 MQPM2P  PIC X.                                          00003500
             03 MQPM2H  PIC X.                                          00003510
             03 MQPM2V  PIC X.                                          00003520
             03 MQPM2O  PIC X(6).                                       00003530
           02 DFHMS8 OCCURS   5 TIMES .                                 00003540
             03 FILLER       PIC X(2).                                  00003550
             03 MQPM3A  PIC X.                                          00003560
             03 MQPM3C  PIC X.                                          00003570
             03 MQPM3P  PIC X.                                          00003580
             03 MQPM3H  PIC X.                                          00003590
             03 MQPM3V  PIC X.                                          00003600
             03 MQPM3O  PIC X(6).                                       00003610
           02 DFHMS9 OCCURS   5 TIMES .                                 00003620
             03 FILLER       PIC X(2).                                  00003630
             03 MQSV4A  PIC X.                                          00003640
             03 MQSV4C  PIC X.                                          00003650
             03 MQSV4P  PIC X.                                          00003660
             03 MQSV4H  PIC X.                                          00003670
             03 MQSV4V  PIC X.                                          00003680
             03 MQSV4O  PIC ZZZZZZ.                                     00003690
           02 DFHMS10 OCCURS   5 TIMES .                                00003700
             03 FILLER       PIC X(2).                                  00003710
             03 MQSV3A  PIC X.                                          00003720
             03 MQSV3C  PIC X.                                          00003730
             03 MQSV3P  PIC X.                                          00003740
             03 MQSV3H  PIC X.                                          00003750
             03 MQSV3V  PIC X.                                          00003760
             03 MQSV3O  PIC ZZZZZZ.                                     00003770
           02 DFHMS11 OCCURS   5 TIMES .                                00003780
             03 FILLER       PIC X(2).                                  00003790
             03 MQSV2A  PIC X.                                          00003800
             03 MQSV2C  PIC X.                                          00003810
             03 MQSV2P  PIC X.                                          00003820
             03 MQSV2H  PIC X.                                          00003830
             03 MQSV2V  PIC X.                                          00003840
             03 MQSV2O  PIC ZZZZZZ.                                     00003850
           02 DFHMS12 OCCURS   5 TIMES .                                00003860
             03 FILLER       PIC X(2).                                  00003870
             03 MQSV1A  PIC X.                                          00003880
             03 MQSV1C  PIC X.                                          00003890
             03 MQSV1P  PIC X.                                          00003900
             03 MQSV1H  PIC X.                                          00003910
             03 MQSV1V  PIC X.                                          00003920
             03 MQSV1O  PIC ZZZZZZ.                                     00003930
           02 DFHMS13 OCCURS   5 TIMES .                                00003940
             03 FILLER       PIC X(2).                                  00003950
             03 MQPV0A  PIC X.                                          00003960
             03 MQPV0C  PIC X.                                          00003970
             03 MQPV0P  PIC X.                                          00003980
             03 MQPV0H  PIC X.                                          00003990
             03 MQPV0V  PIC X.                                          00004000
             03 MQPV0O  PIC X(6).                                       00004010
           02 DFHMS14 OCCURS   5 TIMES .                                00004020
             03 FILLER       PIC X(2).                                  00004030
             03 MQPVF1A      PIC X.                                     00004040
             03 MQPVF1C PIC X.                                          00004050
             03 MQPVF1P PIC X.                                          00004060
             03 MQPVF1H PIC X.                                          00004070
             03 MQPVF1V PIC X.                                          00004080
             03 MQPVF1O      PIC X(6).                                  00004090
           02 DFHMS15 OCCURS   5 TIMES .                                00004100
             03 FILLER       PIC X(2).                                  00004110
             03 MQPVF2A      PIC X.                                     00004120
             03 MQPVF2C PIC X.                                          00004130
             03 MQPVF2P PIC X.                                          00004140
             03 MQPVF2H PIC X.                                          00004150
             03 MQPVF2V PIC X.                                          00004160
             03 MQPVF2O      PIC X(6).                                  00004170
           02 DFHMS16 OCCURS   5 TIMES .                                00004180
             03 FILLER       PIC X(2).                                  00004190
             03 MQPVF3A      PIC X.                                     00004200
             03 MQPVF3C PIC X.                                          00004210
             03 MQPVF3P PIC X.                                          00004220
             03 MQPVF3H PIC X.                                          00004230
             03 MQPVF3V PIC X.                                          00004240
             03 MQPVF3O      PIC X(6).                                  00004250
           02 FILLER    PIC X(2).                                       00004260
           02 MLIBERRA  PIC X.                                          00004270
           02 MLIBERRC  PIC X.                                          00004280
           02 MLIBERRP  PIC X.                                          00004290
           02 MLIBERRH  PIC X.                                          00004300
           02 MLIBERRV  PIC X.                                          00004310
           02 MLIBERRO  PIC X(78).                                      00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MCODTRAA  PIC X.                                          00004340
           02 MCODTRAC  PIC X.                                          00004350
           02 MCODTRAP  PIC X.                                          00004360
           02 MCODTRAH  PIC X.                                          00004370
           02 MCODTRAV  PIC X.                                          00004380
           02 MCODTRAO  PIC X(4).                                       00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MCICSA    PIC X.                                          00004410
           02 MCICSC    PIC X.                                          00004420
           02 MCICSP    PIC X.                                          00004430
           02 MCICSH    PIC X.                                          00004440
           02 MCICSV    PIC X.                                          00004450
           02 MCICSO    PIC X(5).                                       00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MNETNAMA  PIC X.                                          00004480
           02 MNETNAMC  PIC X.                                          00004490
           02 MNETNAMP  PIC X.                                          00004500
           02 MNETNAMH  PIC X.                                          00004510
           02 MNETNAMV  PIC X.                                          00004520
           02 MNETNAMO  PIC X(8).                                       00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MSCREENA  PIC X.                                          00004550
           02 MSCREENC  PIC X.                                          00004560
           02 MSCREENP  PIC X.                                          00004570
           02 MSCREENH  PIC X.                                          00004580
           02 MSCREENV  PIC X.                                          00004590
           02 MSCREENO  PIC X(4).                                       00004600
                                                                                
