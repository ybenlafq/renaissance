      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RTGQ06  GROUPE                                       
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGQ9699                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGQ9699.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGQ9699.                                                            
      *}                                                                        
           02  GQ96-CTYPE                                                       
               PIC X(0001).                                                     
           02  GQ96-CINSEE                                                      
               PIC X(0005).                                                     
           02  GQ96-WPARCOM                                                     
               PIC X(0001).                                                     
           02  GQ96-LCOMUNE                                                     
               PIC X(0032).                                                     
           02  GQ96-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  GQ96-WPARBUR                                                     
               PIC X(0001).                                                     
           02  GQ96-LBUREAU                                                     
               PIC X(0026).                                                     
           02  GQ96-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGQ9699                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGQ9699-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGQ9699-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ96-CTYPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ96-CTYPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ96-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ96-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ96-WPARCOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ96-WPARCOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ96-LCOMUNE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ96-LCOMUNE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ96-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ96-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ96-WPARBUR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ96-WPARBUR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ96-LBUREAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ96-LBUREAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ96-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GQ96-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
