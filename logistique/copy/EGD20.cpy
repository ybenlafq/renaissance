      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD20   EGD20                                              00000020
      ***************************************************************** 00000030
       01   EGD20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSOCIETEI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDEPOTI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURNEEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MJOURNEEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MJOURNEEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MJOURNEEI      PIC X(8).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRAFALEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MRAFALEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MRAFALEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MRAFALEI  PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOPLISTEL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MTOPLISTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTOPLISTEF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MTOPLISTEI     PIC X.                                     00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHLANCTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MHLANCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MHLANCTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MHLANCTI  PIC X(2).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMLANCTL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MMLANCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMLANCTF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MMLANCTI  PIC X(2).                                       00000450
           02 M202I OCCURS   11 TIMES .                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MJOURL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MJOURL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MJOURF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MJOURI  PIC X(8).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPROFILL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLPROFILL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLPROFILF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLPROFILI    PIC X(20).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTOURNEEL   COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MWTOURNEEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWTOURNEEF   PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MWTOURNEEI   PIC X.                                     00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHTOURNEEL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MHTOURNEEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MHTOURNEEF   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MHTOURNEEI   PIC X(2).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTOURNEEL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MMTOURNEEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MMTOURNEEF   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MMTOURNEEI   PIC X(2).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWDESTOCKL   COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MWDESTOCKL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWDESTOCKF   PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MWDESTOCKI   PIC X.                                     00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHDESTOCKL   COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MHDESTOCKL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MHDESTOCKF   PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MHDESTOCKI   PIC X(2).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMDESTOCKL   COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MMDESTOCKL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MMDESTOCKF   PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MMDESTOCKI   PIC X(2).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBLL   COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MNBLL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNBLF   PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNBLI   PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBPL   COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MNBPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNBPF   PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNBPI   PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MSELI   PIC X.                                          00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTLCOML      COMP PIC S9(4).                            00000910
      *--                                                                       
           02 MTOTLCOML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTLCOMF      PIC X.                                     00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MTOTLCOMI      PIC X(4).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPCOML      COMP PIC S9(4).                            00000950
      *--                                                                       
           02 MTOTPCOML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTPCOMF      PIC X.                                     00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MTOTPCOMI      PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MZONCMDI  PIC X(15).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLIBERRI  PIC X(58).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCICSI    PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MNETNAMI  PIC X(8).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MSCREENI  PIC X(4).                                       00001220
      ***************************************************************** 00001230
      * SDF: EGD20   EGD20                                              00001240
      ***************************************************************** 00001250
       01   EGD20O REDEFINES EGD20I.                                    00001260
           02 FILLER    PIC X(12).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTIMJOUA  PIC X.                                          00001360
           02 MTIMJOUC  PIC X.                                          00001370
           02 MTIMJOUP  PIC X.                                          00001380
           02 MTIMJOUH  PIC X.                                          00001390
           02 MTIMJOUV  PIC X.                                          00001400
           02 MTIMJOUO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MPAGEA    PIC X.                                          00001430
           02 MPAGEC    PIC X.                                          00001440
           02 MPAGEP    PIC X.                                          00001450
           02 MPAGEH    PIC X.                                          00001460
           02 MPAGEV    PIC X.                                          00001470
           02 MPAGEO    PIC X(3).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MSOCIETEA      PIC X.                                     00001500
           02 MSOCIETEC PIC X.                                          00001510
           02 MSOCIETEP PIC X.                                          00001520
           02 MSOCIETEH PIC X.                                          00001530
           02 MSOCIETEV PIC X.                                          00001540
           02 MSOCIETEO      PIC X(3).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDEPOTA   PIC X.                                          00001570
           02 MDEPOTC   PIC X.                                          00001580
           02 MDEPOTP   PIC X.                                          00001590
           02 MDEPOTH   PIC X.                                          00001600
           02 MDEPOTV   PIC X.                                          00001610
           02 MDEPOTO   PIC X(3).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MJOURNEEA      PIC X.                                     00001640
           02 MJOURNEEC PIC X.                                          00001650
           02 MJOURNEEP PIC X.                                          00001660
           02 MJOURNEEH PIC X.                                          00001670
           02 MJOURNEEV PIC X.                                          00001680
           02 MJOURNEEO      PIC X(8).                                  00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MRAFALEA  PIC X.                                          00001710
           02 MRAFALEC  PIC X.                                          00001720
           02 MRAFALEP  PIC X.                                          00001730
           02 MRAFALEH  PIC X.                                          00001740
           02 MRAFALEV  PIC X.                                          00001750
           02 MRAFALEO  PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MTOPLISTEA     PIC X.                                     00001780
           02 MTOPLISTEC     PIC X.                                     00001790
           02 MTOPLISTEP     PIC X.                                     00001800
           02 MTOPLISTEH     PIC X.                                     00001810
           02 MTOPLISTEV     PIC X.                                     00001820
           02 MTOPLISTEO     PIC X.                                     00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MHLANCTA  PIC X.                                          00001850
           02 MHLANCTC  PIC X.                                          00001860
           02 MHLANCTP  PIC X.                                          00001870
           02 MHLANCTH  PIC X.                                          00001880
           02 MHLANCTV  PIC X.                                          00001890
           02 MHLANCTO  PIC X(2).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MMLANCTA  PIC X.                                          00001920
           02 MMLANCTC  PIC X.                                          00001930
           02 MMLANCTP  PIC X.                                          00001940
           02 MMLANCTH  PIC X.                                          00001950
           02 MMLANCTV  PIC X.                                          00001960
           02 MMLANCTO  PIC X(2).                                       00001970
           02 M202O OCCURS   11 TIMES .                                 00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MJOURA  PIC X.                                          00002000
             03 MJOURC  PIC X.                                          00002010
             03 MJOURP  PIC X.                                          00002020
             03 MJOURH  PIC X.                                          00002030
             03 MJOURV  PIC X.                                          00002040
             03 MJOURO  PIC X(8).                                       00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MLPROFILA    PIC X.                                     00002070
             03 MLPROFILC    PIC X.                                     00002080
             03 MLPROFILP    PIC X.                                     00002090
             03 MLPROFILH    PIC X.                                     00002100
             03 MLPROFILV    PIC X.                                     00002110
             03 MLPROFILO    PIC X(20).                                 00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MWTOURNEEA   PIC X.                                     00002140
             03 MWTOURNEEC   PIC X.                                     00002150
             03 MWTOURNEEP   PIC X.                                     00002160
             03 MWTOURNEEH   PIC X.                                     00002170
             03 MWTOURNEEV   PIC X.                                     00002180
             03 MWTOURNEEO   PIC X.                                     00002190
             03 FILLER       PIC X(2).                                  00002200
             03 MHTOURNEEA   PIC X.                                     00002210
             03 MHTOURNEEC   PIC X.                                     00002220
             03 MHTOURNEEP   PIC X.                                     00002230
             03 MHTOURNEEH   PIC X.                                     00002240
             03 MHTOURNEEV   PIC X.                                     00002250
             03 MHTOURNEEO   PIC X(2).                                  00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MMTOURNEEA   PIC X.                                     00002280
             03 MMTOURNEEC   PIC X.                                     00002290
             03 MMTOURNEEP   PIC X.                                     00002300
             03 MMTOURNEEH   PIC X.                                     00002310
             03 MMTOURNEEV   PIC X.                                     00002320
             03 MMTOURNEEO   PIC X(2).                                  00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MWDESTOCKA   PIC X.                                     00002350
             03 MWDESTOCKC   PIC X.                                     00002360
             03 MWDESTOCKP   PIC X.                                     00002370
             03 MWDESTOCKH   PIC X.                                     00002380
             03 MWDESTOCKV   PIC X.                                     00002390
             03 MWDESTOCKO   PIC X.                                     00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MHDESTOCKA   PIC X.                                     00002420
             03 MHDESTOCKC   PIC X.                                     00002430
             03 MHDESTOCKP   PIC X.                                     00002440
             03 MHDESTOCKH   PIC X.                                     00002450
             03 MHDESTOCKV   PIC X.                                     00002460
             03 MHDESTOCKO   PIC X(2).                                  00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MMDESTOCKA   PIC X.                                     00002490
             03 MMDESTOCKC   PIC X.                                     00002500
             03 MMDESTOCKP   PIC X.                                     00002510
             03 MMDESTOCKH   PIC X.                                     00002520
             03 MMDESTOCKV   PIC X.                                     00002530
             03 MMDESTOCKO   PIC X(2).                                  00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MNBLA   PIC X.                                          00002560
             03 MNBLC   PIC X.                                          00002570
             03 MNBLP   PIC X.                                          00002580
             03 MNBLH   PIC X.                                          00002590
             03 MNBLV   PIC X.                                          00002600
             03 MNBLO   PIC X(4).                                       00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MNBPA   PIC X.                                          00002630
             03 MNBPC   PIC X.                                          00002640
             03 MNBPP   PIC X.                                          00002650
             03 MNBPH   PIC X.                                          00002660
             03 MNBPV   PIC X.                                          00002670
             03 MNBPO   PIC X(5).                                       00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MSELA   PIC X.                                          00002700
             03 MSELC   PIC X.                                          00002710
             03 MSELP   PIC X.                                          00002720
             03 MSELH   PIC X.                                          00002730
             03 MSELV   PIC X.                                          00002740
             03 MSELO   PIC X.                                          00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MTOTLCOMA      PIC X.                                     00002770
           02 MTOTLCOMC PIC X.                                          00002780
           02 MTOTLCOMP PIC X.                                          00002790
           02 MTOTLCOMH PIC X.                                          00002800
           02 MTOTLCOMV PIC X.                                          00002810
           02 MTOTLCOMO      PIC X(4).                                  00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MTOTPCOMA      PIC X.                                     00002840
           02 MTOTPCOMC PIC X.                                          00002850
           02 MTOTPCOMP PIC X.                                          00002860
           02 MTOTPCOMH PIC X.                                          00002870
           02 MTOTPCOMV PIC X.                                          00002880
           02 MTOTPCOMO      PIC X(5).                                  00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MZONCMDA  PIC X.                                          00002910
           02 MZONCMDC  PIC X.                                          00002920
           02 MZONCMDP  PIC X.                                          00002930
           02 MZONCMDH  PIC X.                                          00002940
           02 MZONCMDV  PIC X.                                          00002950
           02 MZONCMDO  PIC X(15).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLIBERRA  PIC X.                                          00002980
           02 MLIBERRC  PIC X.                                          00002990
           02 MLIBERRP  PIC X.                                          00003000
           02 MLIBERRH  PIC X.                                          00003010
           02 MLIBERRV  PIC X.                                          00003020
           02 MLIBERRO  PIC X(58).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSO    PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNETNAMA  PIC X.                                          00003190
           02 MNETNAMC  PIC X.                                          00003200
           02 MNETNAMP  PIC X.                                          00003210
           02 MNETNAMH  PIC X.                                          00003220
           02 MNETNAMV  PIC X.                                          00003230
           02 MNETNAMO  PIC X(8).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSCREENA  PIC X.                                          00003260
           02 MSCREENC  PIC X.                                          00003270
           02 MSCREENP  PIC X.                                          00003280
           02 MSCREENH  PIC X.                                          00003290
           02 MSCREENV  PIC X.                                          00003300
           02 MSCREENO  PIC X(4).                                       00003310
                                                                                
