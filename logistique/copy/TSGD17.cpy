      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *---------------------------------------------------------------*         
      * DESCRIPTION DE LA TSGD17                                      *         
      *---------------------------------------------------------------*         
      *                                                               *         
       01  TS-LONG                PIC S9(3) COMP-3 VALUE +830.                  
      *                                                               *         
      *................................................................         
      *                                                               *         
       01  TS-DONNEES.                                                          
           05 TS-NPAGE            PIC 99.                                       
           05 TS-LIGNE    OCCURS 12.                                            
              10 TS-NRAFALE               PIC X(2).                             
              10 TS-SOC                   PIC X(3).                             
              10 TS-LIEU                  PIC X(3).                             
              10 TS-LIBELLE               PIC X(20).                            
              10 TS-MST                   PIC X(5).                             
              10 TS-SM                    PIC X(1).                             
              10 TS-SELART                PIC X(5).                             
              10 TS-QVOL                  PIC 9(2)V99.                          
              10 TS-QNBL                  PIC 9(4).                             
              10 TS-QNBP                  PIC 9(5).                             
              10 TS-SATELLITE              OCCURS 5.                            
                 15 TS-SAT                PIC XX.                               
              10 TS-CASE                  PIC X(3).                             
              10 TS-SAT-MODIFIE           PIC X.                                
              10 TS-CAS-MODIFIEE          PIC X.                                
              10 TS-ETAT-LIGNE            PIC 9.                                
                 88  TS-LIGNE-BLOQUEE     VALUE 1.                              
                 88  TS-LIGNE-EN-SAISIE   VALUE 0.                              
              10 TS-ETAT-SAT              PIC 9.                                
                 88  TS-SAT-BLOQUEE     VALUE 1.                                
                 88  TS-SAT-EN-SAISIE   VALUE 0.                                
                                                                                
