      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGB105 AU 27/10/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,03,BI,A,                          *        
      *                           19,03,BI,A,                          *        
      *                           22,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGB105.                                                        
            05 NOMETAT-IGB105           PIC X(6) VALUE 'IGB105'.                
            05 RUPTURES-IGB105.                                                 
           10 IGB105-NSOCGRP            PIC X(03).                      007  003
           10 IGB105-NSOCIETE           PIC X(03).                      010  003
           10 IGB105-NLIEU              PIC X(03).                      013  003
           10 IGB105-NSOCENTR           PIC X(03).                      016  003
           10 IGB105-NDEPOT             PIC X(03).                      019  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGB105-SEQUENCE           PIC S9(04) COMP.                022  002
      *--                                                                       
           10 IGB105-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGB105.                                                   
           10 IGB105-DATE1              PIC X(10).                      024  010
           10 IGB105-DATE2              PIC X(10).                      034  010
           10 IGB105-LLIEU              PIC X(20).                      044  020
           10 IGB105-QMUTEE             PIC S9(07)      COMP-3.         064  004
            05 FILLER                      PIC X(445).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGB105-LONG           PIC S9(4)   COMP  VALUE +067.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGB105-LONG           PIC S9(4) COMP-5  VALUE +067.           
                                                                                
      *}                                                                        
