      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM15   ERM15                                              00000020
      ***************************************************************** 00000030
       01   ERM15I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCGROUPI  PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQFAIBLEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MQFAIBLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQFAIBLEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MQFAIBLEI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQFORTESL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MQFORTESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQFORTESF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MQFORTESI      PIC X(2).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MW20FL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MW20FL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MW20FF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MW20FI    PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MW80FL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MW80FL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MW80FF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MW80FI    PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTXEMPFL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MWTXEMPFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWTXEMPFF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MWTXEMPFI      PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQSEUILEXFL    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MQSEUILEXFL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQSEUILEXFF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MQSEUILEXFI    PIC X(6).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETFL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDEFFETFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEFFETFF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDEFFETFI      PIC X(10).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINEFFFL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MDFINEFFFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDFINEFFFF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDFINEFFFI     PIC X(10).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLFAMI    PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQINDISFL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MQINDISFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQINDISFF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQINDISFI      PIC X(5).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPONDFL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MQPONDFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPONDFF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQPONDFI  PIC X(4).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNINDIFL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNINDIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNINDIFF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNINDIFI  PIC X.                                          00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQSEUILFL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MQSEUILFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQSEUILFF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MQSEUILFI      PIC X(6).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPVMFL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MQPVMFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQPVMFF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MQPVMFI   PIC X(4).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQEXPMAXFL     COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MQEXPMAXFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQEXPMAXFF     PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MQEXPMAXFI     PIC X(5).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQLSMAXFL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MQLSMAXFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQLSMAXFF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MQLSMAXFI      PIC X(5).                                  00000850
           02 MLAGREGD OCCURS   7 TIMES .                               00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLAGREGL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MLAGREGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLAGREGF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MLAGREGI     PIC X(20).                                 00000900
           02 MW20D OCCURS   7 TIMES .                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MW20L   COMP PIC S9(4).                                 00000920
      *--                                                                       
             03 MW20L COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MW20F   PIC X.                                          00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MW20I   PIC X.                                          00000950
           02 MW80D OCCURS   7 TIMES .                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MW80L   COMP PIC S9(4).                                 00000970
      *--                                                                       
             03 MW80L COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MW80F   PIC X.                                          00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MW80I   PIC X.                                          00001000
           02 MWTXEMPD OCCURS   7 TIMES .                               00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTXEMPL     COMP PIC S9(4).                            00001020
      *--                                                                       
             03 MWTXEMPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWTXEMPF     PIC X.                                     00001030
             03 FILLER  PIC X(4).                                       00001040
             03 MWTXEMPI     PIC X.                                     00001050
           02 MQSEUILEXD OCCURS   7 TIMES .                             00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSEUILEXL   COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MQSEUILEXL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQSEUILEXF   PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MQSEUILEXI   PIC X(6).                                  00001100
           02 MDEFFETD OCCURS   7 TIMES .                               00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00001120
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MDEFFETI     PIC X(10).                                 00001150
           02 MDFINEFFD OCCURS   7 TIMES .                              00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINEFFL    COMP PIC S9(4).                            00001170
      *--                                                                       
             03 MDFINEFFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDFINEFFF    PIC X.                                     00001180
             03 FILLER  PIC X(4).                                       00001190
             03 MDFINEFFI    PIC X(10).                                 00001200
           02 MQINDISPD OCCURS   7 TIMES .                              00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQINDISPL    COMP PIC S9(4).                            00001220
      *--                                                                       
             03 MQINDISPL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQINDISPF    PIC X.                                     00001230
             03 FILLER  PIC X(4).                                       00001240
             03 MQINDISPI    PIC X(5).                                  00001250
           02 MQPONDD OCCURS   7 TIMES .                                00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPONDL      COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MQPONDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPONDF      PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MQPONDI      PIC X(4).                                  00001300
           02 MNINDICD OCCURS   7 TIMES .                               00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNINDICL     COMP PIC S9(4).                            00001320
      *--                                                                       
             03 MNINDICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNINDICF     PIC X.                                     00001330
             03 FILLER  PIC X(4).                                       00001340
             03 MNINDICI     PIC X.                                     00001350
           02 MQSEUILD OCCURS   7 TIMES .                               00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSEUILL     COMP PIC S9(4).                            00001370
      *--                                                                       
             03 MQSEUILL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQSEUILF     PIC X.                                     00001380
             03 FILLER  PIC X(4).                                       00001390
             03 MQSEUILI     PIC X(6).                                  00001400
           02 MQPVMD OCCURS   7 TIMES .                                 00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPVML  COMP PIC S9(4).                                 00001420
      *--                                                                       
             03 MQPVML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQPVMF  PIC X.                                          00001430
             03 FILLER  PIC X(4).                                       00001440
             03 MQPVMI  PIC X(4).                                       00001450
           02 MQEXPMAXD OCCURS   7 TIMES .                              00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPMAXL    COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MQEXPMAXL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQEXPMAXF    PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MQEXPMAXI    PIC X(5).                                  00001500
           02 MQLSMAXD OCCURS   7 TIMES .                               00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLSMAXL     COMP PIC S9(4).                            00001520
      *--                                                                       
             03 MQLSMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQLSMAXF     PIC X.                                     00001530
             03 FILLER  PIC X(4).                                       00001540
             03 MQLSMAXI     PIC X(5).                                  00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001560
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MLIBERRI  PIC X(80).                                      00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001600
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001610
           02 FILLER    PIC X(4).                                       00001620
           02 MCODTRAI  PIC X(4).                                       00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001640
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MCICSI    PIC X(5).                                       00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001680
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001690
           02 FILLER    PIC X(4).                                       00001700
           02 MNETNAMI  PIC X(8).                                       00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001720
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001730
           02 FILLER    PIC X(4).                                       00001740
           02 MSCREENI  PIC X(4).                                       00001750
      ***************************************************************** 00001760
      * SDF: ERM15   ERM15                                              00001770
      ***************************************************************** 00001780
       01   ERM15O REDEFINES ERM15I.                                    00001790
           02 FILLER    PIC X(12).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MDATJOUA  PIC X.                                          00001820
           02 MDATJOUC  PIC X.                                          00001830
           02 MDATJOUP  PIC X.                                          00001840
           02 MDATJOUH  PIC X.                                          00001850
           02 MDATJOUV  PIC X.                                          00001860
           02 MDATJOUO  PIC X(10).                                      00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MTIMJOUA  PIC X.                                          00001890
           02 MTIMJOUC  PIC X.                                          00001900
           02 MTIMJOUP  PIC X.                                          00001910
           02 MTIMJOUH  PIC X.                                          00001920
           02 MTIMJOUV  PIC X.                                          00001930
           02 MTIMJOUO  PIC X(5).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MCGROUPA  PIC X.                                          00001960
           02 MCGROUPC  PIC X.                                          00001970
           02 MCGROUPP  PIC X.                                          00001980
           02 MCGROUPH  PIC X.                                          00001990
           02 MCGROUPV  PIC X.                                          00002000
           02 MCGROUPO  PIC X(5).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MQFAIBLEA      PIC X.                                     00002030
           02 MQFAIBLEC PIC X.                                          00002040
           02 MQFAIBLEP PIC X.                                          00002050
           02 MQFAIBLEH PIC X.                                          00002060
           02 MQFAIBLEV PIC X.                                          00002070
           02 MQFAIBLEO      PIC X(2).                                  00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MQFORTESA      PIC X.                                     00002100
           02 MQFORTESC PIC X.                                          00002110
           02 MQFORTESP PIC X.                                          00002120
           02 MQFORTESH PIC X.                                          00002130
           02 MQFORTESV PIC X.                                          00002140
           02 MQFORTESO      PIC X(2).                                  00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCFAMA    PIC X.                                          00002170
           02 MCFAMC    PIC X.                                          00002180
           02 MCFAMP    PIC X.                                          00002190
           02 MCFAMH    PIC X.                                          00002200
           02 MCFAMV    PIC X.                                          00002210
           02 MCFAMO    PIC X(5).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MW20FA    PIC X.                                          00002240
           02 MW20FC    PIC X.                                          00002250
           02 MW20FP    PIC X.                                          00002260
           02 MW20FH    PIC X.                                          00002270
           02 MW20FV    PIC X.                                          00002280
           02 MW20FO    PIC X.                                          00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MW80FA    PIC X.                                          00002310
           02 MW80FC    PIC X.                                          00002320
           02 MW80FP    PIC X.                                          00002330
           02 MW80FH    PIC X.                                          00002340
           02 MW80FV    PIC X.                                          00002350
           02 MW80FO    PIC X.                                          00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MWTXEMPFA      PIC X.                                     00002380
           02 MWTXEMPFC PIC X.                                          00002390
           02 MWTXEMPFP PIC X.                                          00002400
           02 MWTXEMPFH PIC X.                                          00002410
           02 MWTXEMPFV PIC X.                                          00002420
           02 MWTXEMPFO      PIC X.                                     00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MQSEUILEXFA    PIC X.                                     00002450
           02 MQSEUILEXFC    PIC X.                                     00002460
           02 MQSEUILEXFP    PIC X.                                     00002470
           02 MQSEUILEXFH    PIC X.                                     00002480
           02 MQSEUILEXFV    PIC X.                                     00002490
           02 MQSEUILEXFO    PIC X(6).                                  00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MDEFFETFA      PIC X.                                     00002520
           02 MDEFFETFC PIC X.                                          00002530
           02 MDEFFETFP PIC X.                                          00002540
           02 MDEFFETFH PIC X.                                          00002550
           02 MDEFFETFV PIC X.                                          00002560
           02 MDEFFETFO      PIC X(10).                                 00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MDFINEFFFA     PIC X.                                     00002590
           02 MDFINEFFFC     PIC X.                                     00002600
           02 MDFINEFFFP     PIC X.                                     00002610
           02 MDFINEFFFH     PIC X.                                     00002620
           02 MDFINEFFFV     PIC X.                                     00002630
           02 MDFINEFFFO     PIC X(10).                                 00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MLFAMA    PIC X.                                          00002660
           02 MLFAMC    PIC X.                                          00002670
           02 MLFAMP    PIC X.                                          00002680
           02 MLFAMH    PIC X.                                          00002690
           02 MLFAMV    PIC X.                                          00002700
           02 MLFAMO    PIC X(20).                                      00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MQINDISFA      PIC X.                                     00002730
           02 MQINDISFC PIC X.                                          00002740
           02 MQINDISFP PIC X.                                          00002750
           02 MQINDISFH PIC X.                                          00002760
           02 MQINDISFV PIC X.                                          00002770
           02 MQINDISFO      PIC X(5).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MQPONDFA  PIC X.                                          00002800
           02 MQPONDFC  PIC X.                                          00002810
           02 MQPONDFP  PIC X.                                          00002820
           02 MQPONDFH  PIC X.                                          00002830
           02 MQPONDFV  PIC X.                                          00002840
           02 MQPONDFO  PIC X(4).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MNINDIFA  PIC X.                                          00002870
           02 MNINDIFC  PIC X.                                          00002880
           02 MNINDIFP  PIC X.                                          00002890
           02 MNINDIFH  PIC X.                                          00002900
           02 MNINDIFV  PIC X.                                          00002910
           02 MNINDIFO  PIC X.                                          00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MQSEUILFA      PIC X.                                     00002940
           02 MQSEUILFC PIC X.                                          00002950
           02 MQSEUILFP PIC X.                                          00002960
           02 MQSEUILFH PIC X.                                          00002970
           02 MQSEUILFV PIC X.                                          00002980
           02 MQSEUILFO      PIC X(6).                                  00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MQPVMFA   PIC X.                                          00003010
           02 MQPVMFC   PIC X.                                          00003020
           02 MQPVMFP   PIC X.                                          00003030
           02 MQPVMFH   PIC X.                                          00003040
           02 MQPVMFV   PIC X.                                          00003050
           02 MQPVMFO   PIC X(4).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MQEXPMAXFA     PIC X.                                     00003080
           02 MQEXPMAXFC     PIC X.                                     00003090
           02 MQEXPMAXFP     PIC X.                                     00003100
           02 MQEXPMAXFH     PIC X.                                     00003110
           02 MQEXPMAXFV     PIC X.                                     00003120
           02 MQEXPMAXFO     PIC X(5).                                  00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MQLSMAXFA      PIC X.                                     00003150
           02 MQLSMAXFC PIC X.                                          00003160
           02 MQLSMAXFP PIC X.                                          00003170
           02 MQLSMAXFH PIC X.                                          00003180
           02 MQLSMAXFV PIC X.                                          00003190
           02 MQLSMAXFO      PIC X(5).                                  00003200
           02 DFHMS1 OCCURS   7 TIMES .                                 00003210
             03 FILLER       PIC X(2).                                  00003220
             03 MLAGREGA     PIC X.                                     00003230
             03 MLAGREGC     PIC X.                                     00003240
             03 MLAGREGP     PIC X.                                     00003250
             03 MLAGREGH     PIC X.                                     00003260
             03 MLAGREGV     PIC X.                                     00003270
             03 MLAGREGO     PIC X(20).                                 00003280
           02 DFHMS2 OCCURS   7 TIMES .                                 00003290
             03 FILLER       PIC X(2).                                  00003300
             03 MW20A   PIC X.                                          00003310
             03 MW20C   PIC X.                                          00003320
             03 MW20P   PIC X.                                          00003330
             03 MW20H   PIC X.                                          00003340
             03 MW20V   PIC X.                                          00003350
             03 MW20O   PIC X.                                          00003360
           02 DFHMS3 OCCURS   7 TIMES .                                 00003370
             03 FILLER       PIC X(2).                                  00003380
             03 MW80A   PIC X.                                          00003390
             03 MW80C   PIC X.                                          00003400
             03 MW80P   PIC X.                                          00003410
             03 MW80H   PIC X.                                          00003420
             03 MW80V   PIC X.                                          00003430
             03 MW80O   PIC X.                                          00003440
           02 DFHMS4 OCCURS   7 TIMES .                                 00003450
             03 FILLER       PIC X(2).                                  00003460
             03 MWTXEMPA     PIC X.                                     00003470
             03 MWTXEMPC     PIC X.                                     00003480
             03 MWTXEMPP     PIC X.                                     00003490
             03 MWTXEMPH     PIC X.                                     00003500
             03 MWTXEMPV     PIC X.                                     00003510
             03 MWTXEMPO     PIC X.                                     00003520
           02 DFHMS5 OCCURS   7 TIMES .                                 00003530
             03 FILLER       PIC X(2).                                  00003540
             03 MQSEUILEXA   PIC X.                                     00003550
             03 MQSEUILEXC   PIC X.                                     00003560
             03 MQSEUILEXP   PIC X.                                     00003570
             03 MQSEUILEXH   PIC X.                                     00003580
             03 MQSEUILEXV   PIC X.                                     00003590
             03 MQSEUILEXO   PIC X(6).                                  00003600
           02 DFHMS6 OCCURS   7 TIMES .                                 00003610
             03 FILLER       PIC X(2).                                  00003620
             03 MDEFFETA     PIC X.                                     00003630
             03 MDEFFETC     PIC X.                                     00003640
             03 MDEFFETP     PIC X.                                     00003650
             03 MDEFFETH     PIC X.                                     00003660
             03 MDEFFETV     PIC X.                                     00003670
             03 MDEFFETO     PIC X(10).                                 00003680
           02 DFHMS7 OCCURS   7 TIMES .                                 00003690
             03 FILLER       PIC X(2).                                  00003700
             03 MDFINEFFA    PIC X.                                     00003710
             03 MDFINEFFC    PIC X.                                     00003720
             03 MDFINEFFP    PIC X.                                     00003730
             03 MDFINEFFH    PIC X.                                     00003740
             03 MDFINEFFV    PIC X.                                     00003750
             03 MDFINEFFO    PIC X(10).                                 00003760
           02 DFHMS8 OCCURS   7 TIMES .                                 00003770
             03 FILLER       PIC X(2).                                  00003780
             03 MQINDISPA    PIC X.                                     00003790
             03 MQINDISPC    PIC X.                                     00003800
             03 MQINDISPP    PIC X.                                     00003810
             03 MQINDISPH    PIC X.                                     00003820
             03 MQINDISPV    PIC X.                                     00003830
             03 MQINDISPO    PIC X(5).                                  00003840
           02 DFHMS9 OCCURS   7 TIMES .                                 00003850
             03 FILLER       PIC X(2).                                  00003860
             03 MQPONDA      PIC X.                                     00003870
             03 MQPONDC PIC X.                                          00003880
             03 MQPONDP PIC X.                                          00003890
             03 MQPONDH PIC X.                                          00003900
             03 MQPONDV PIC X.                                          00003910
             03 MQPONDO      PIC X(4).                                  00003920
           02 DFHMS10 OCCURS   7 TIMES .                                00003930
             03 FILLER       PIC X(2).                                  00003940
             03 MNINDICA     PIC X.                                     00003950
             03 MNINDICC     PIC X.                                     00003960
             03 MNINDICP     PIC X.                                     00003970
             03 MNINDICH     PIC X.                                     00003980
             03 MNINDICV     PIC X.                                     00003990
             03 MNINDICO     PIC X.                                     00004000
           02 DFHMS11 OCCURS   7 TIMES .                                00004010
             03 FILLER       PIC X(2).                                  00004020
             03 MQSEUILA     PIC X.                                     00004030
             03 MQSEUILC     PIC X.                                     00004040
             03 MQSEUILP     PIC X.                                     00004050
             03 MQSEUILH     PIC X.                                     00004060
             03 MQSEUILV     PIC X.                                     00004070
             03 MQSEUILO     PIC X(6).                                  00004080
           02 DFHMS12 OCCURS   7 TIMES .                                00004090
             03 FILLER       PIC X(2).                                  00004100
             03 MQPVMA  PIC X.                                          00004110
             03 MQPVMC  PIC X.                                          00004120
             03 MQPVMP  PIC X.                                          00004130
             03 MQPVMH  PIC X.                                          00004140
             03 MQPVMV  PIC X.                                          00004150
             03 MQPVMO  PIC X(4).                                       00004160
           02 DFHMS13 OCCURS   7 TIMES .                                00004170
             03 FILLER       PIC X(2).                                  00004180
             03 MQEXPMAXA    PIC X.                                     00004190
             03 MQEXPMAXC    PIC X.                                     00004200
             03 MQEXPMAXP    PIC X.                                     00004210
             03 MQEXPMAXH    PIC X.                                     00004220
             03 MQEXPMAXV    PIC X.                                     00004230
             03 MQEXPMAXO    PIC X(5).                                  00004240
           02 DFHMS14 OCCURS   7 TIMES .                                00004250
             03 FILLER       PIC X(2).                                  00004260
             03 MQLSMAXA     PIC X.                                     00004270
             03 MQLSMAXC     PIC X.                                     00004280
             03 MQLSMAXP     PIC X.                                     00004290
             03 MQLSMAXH     PIC X.                                     00004300
             03 MQLSMAXV     PIC X.                                     00004310
             03 MQLSMAXO     PIC X(5).                                  00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MLIBERRA  PIC X.                                          00004340
           02 MLIBERRC  PIC X.                                          00004350
           02 MLIBERRP  PIC X.                                          00004360
           02 MLIBERRH  PIC X.                                          00004370
           02 MLIBERRV  PIC X.                                          00004380
           02 MLIBERRO  PIC X(80).                                      00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MCODTRAA  PIC X.                                          00004410
           02 MCODTRAC  PIC X.                                          00004420
           02 MCODTRAP  PIC X.                                          00004430
           02 MCODTRAH  PIC X.                                          00004440
           02 MCODTRAV  PIC X.                                          00004450
           02 MCODTRAO  PIC X(4).                                       00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MCICSA    PIC X.                                          00004480
           02 MCICSC    PIC X.                                          00004490
           02 MCICSP    PIC X.                                          00004500
           02 MCICSH    PIC X.                                          00004510
           02 MCICSV    PIC X.                                          00004520
           02 MCICSO    PIC X(5).                                       00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MNETNAMA  PIC X.                                          00004550
           02 MNETNAMC  PIC X.                                          00004560
           02 MNETNAMP  PIC X.                                          00004570
           02 MNETNAMH  PIC X.                                          00004580
           02 MNETNAMV  PIC X.                                          00004590
           02 MNETNAMO  PIC X(8).                                       00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MSCREENA  PIC X.                                          00004620
           02 MSCREENC  PIC X.                                          00004630
           02 MSCREENP  PIC X.                                          00004640
           02 MSCREENH  PIC X.                                          00004650
           02 MSCREENV  PIC X.                                          00004660
           02 MSCREENO  PIC X(4).                                       00004670
                                                                                
