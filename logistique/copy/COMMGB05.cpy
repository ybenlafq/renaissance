      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
       01  COMM-MGB05-APPLI.                                            00000010
           02 COMM-MGB05-ZONES-ENTREE.                                  00000020
              05 COMM-MGB05-NSOCDEPOT          PIC X(03).               00000030
              05 COMM-MGB05-NDEPOT             PIC X(03).               00000040
              05 COMM-MGB05-NSOCIETE           PIC X(03).               00000050
              05 COMM-MGB05-NLIEU              PIC X(03).               00000060
              05 COMM-MGB05-CFAM               PIC X(05).               00000070
              05 COMM-MGB05-DDELIV             PIC X(08).               00000080
              05 COMM-MGB05-CSELART-REFUSE     PIC X(05).               00000090
           02 COMM-MGB05-ZONES-SORTIE.                                  00000100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-MGB05-CODRET      COMP   PIC  S9(4).              00000110
      *--                                                                       
              05 COMM-MGB05-CODRET COMP-5   PIC  S9(4).                         
      *}                                                                        
              05 COMM-MGB05-LMESS              PIC X(35).               00000120
              05 COMM-MGB05-RVGB0500           PIC X(113).              00000130
                                                                                
