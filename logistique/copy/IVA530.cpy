      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA530      *        
      *                                                                *        
      *          CRITERES DE TRI  01,03,BI,A,                          *        
      *                           04,02,BI,A,                          *        
      *                           06,03,BI,A,                          *        
      *                           09,02,BI,A,                          *        
      *                           11,03,PD,A,                          *        
      *                           14,05,BI,A,                          *        
      *                           19,07,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA530.                                                        
            05 RUPTURES-IVA530.                                                 
           10 IVA530-NSOCVALO           PIC X(03).                      001  003
           10 IVA530-CSEQRAYON1         PIC X(02).                      004  002
           10 IVA530-NLIEUVALO          PIC X(03).                      006  003
           10 IVA530-CSEQRAYON2         PIC X(02).                      009  002
           10 IVA530-WSEQFAM            PIC S9(05)      COMP-3.         011  003
           10 IVA530-CMARQ              PIC X(05).                      014  005
           10 IVA530-NCODIC             PIC X(07).                      019  007
            05 CHAMPS-IVA530.                                                   
           10 IVA530-CFAM               PIC X(05).                      026  005
           10 IVA530-CRAYON1            PIC X(05).                      031  005
           10 IVA530-DEVISE             PIC X(03).                      036  003
           10 IVA530-LFAM               PIC X(20).                      039  020
           10 IVA530-LLIEU              PIC X(20).                      059  020
           10 IVA530-LRAYON1            PIC X(20).                      079  020
           10 IVA530-LRAYON2            PIC X(20).                      099  020
           10 IVA530-LREFFOURN          PIC X(20).                      119  020
           10 IVA530-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         139  008
           10 IVA530-PSTOCKENTREE-E     PIC S9(09)V9(6) COMP-3.         147  008
           10 IVA530-PSTOCKENTREE-M     PIC S9(09)V9(6) COMP-3.         155  008
           10 IVA530-PSTOCKENTREE-R     PIC S9(09)V9(6) COMP-3.         163  008
           10 IVA530-PSTOCKRECYCL-E     PIC S9(09)V9(6) COMP-3.         171  008
           10 IVA530-PSTOCKFINAL        PIC S9(09)V9(6) COMP-3.         179  008
           10 IVA530-PSTOCKINIT         PIC S9(09)V9(6) COMP-3.         187  008
           10 IVA530-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         195  008
           10 IVA530-PSTOCKSORTIE-E     PIC S9(09)V9(6) COMP-3.         203  008
           10 IVA530-PSTOCKSORTIE-M     PIC S9(09)V9(6) COMP-3.         211  008
           10 IVA530-PSTOCKSORTIE-R     PIC S9(09)V9(6) COMP-3.         219  008
           10 IVA530-PSTOCKRECYCL-S     PIC S9(09)V9(6) COMP-3.         227  008
           10 IVA530-QSTOCKENTREE       PIC S9(11)      COMP-3.         235  006
           10 IVA530-QSTOCKENTREE-E     PIC S9(11)      COMP-3.         241  006
           10 IVA530-QSTOCKENTREE-M     PIC S9(11)      COMP-3.         247  006
           10 IVA530-QSTOCKENTREE-R     PIC S9(11)      COMP-3.         253  006
           10 IVA530-QSTOCKFINAL        PIC S9(11)      COMP-3.         259  006
           10 IVA530-QSTOCKINIT         PIC S9(11)      COMP-3.         265  006
           10 IVA530-QSTOCKSORTIE       PIC S9(11)      COMP-3.         271  006
           10 IVA530-QSTOCKSORTIE-E     PIC S9(11)      COMP-3.         277  006
           10 IVA530-QSTOCKSORTIE-M     PIC S9(11)      COMP-3.         283  006
           10 IVA530-QSTOCKSORTIE-R     PIC S9(11)      COMP-3.         289  006
           10 IVA530-TXEURO             PIC S9(01)V9(5) COMP-3.         295  004
            05 FILLER                      PIC X(214).                          
      *     05 FILLER                      PIC X(221).                          
                                                                                
