      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER INTERMEDIAIRE DE L'ETAT IVA540     *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,02,BI,A,                          *        
      *                           17,03,PD,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,07,BI,A,                          *        
      *                           32,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA525.                                                        
            05 NOMETAT-IVA525           PIC X(6) VALUE 'IVA540'.                
            05 RUPTURES-IVA525.                                                 
           10 IVA525-NSOCVALO           PIC X(03).                      007  003
           10 IVA525-CSEQRAYON1         PIC X(02).                      010  002
           10 IVA525-NLIEUVALO          PIC X(03).                      012  003
           10 IVA525-CSEQRAYON2         PIC X(02).                      015  002
           10 IVA525-WSEQFAM            PIC S9(05)      COMP-3.         017  003
           10 IVA525-CMARQ              PIC X(05).                      020  005
           10 IVA525-NCODIC             PIC X(07).                      025  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA525-SEQUENCE           PIC S9(04) COMP.                032  002
      *--                                                                       
           10 IVA525-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA525.                                                   
           10 IVA525-CFAM               PIC X(05).                      034  005
           10 IVA525-CRAYON1            PIC X(05).                      039  005
NT         10 IVA525-DEVISE             PIC X(03).                      044  003
NT         10 IVA525-LIB1               PIC X(01).                      047  001
NT         10 IVA525-LIB11              PIC X(01).                      048  001
NT         10 IVA525-LIB2               PIC X(01).                      049  001
NT         10 IVA525-LIB21              PIC X(01).                      050  001
           10 IVA525-LLIEU              PIC X(20).                      051  020
           10 IVA525-LRAYON1            PIC X(20).                      071  020
           10 IVA525-LRAYON2            PIC X(20).                      091  020
           10 IVA525-LREFFOURN          PIC X(20).                      111  020
           10 IVA525-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         131  008
           10 IVA525-PSTOCKENTREE-E     PIC S9(09)V9(6) COMP-3.         139  008
           10 IVA525-PSTOCKENTREE-M     PIC S9(09)V9(6) COMP-3.         147  008
           10 IVA525-PSTOCKENTREE-R     PIC S9(09)V9(6) COMP-3.         155  008
           10 IVA525-PSTOCKRECYCL-E     PIC S9(09)V9(6) COMP-3.         163  008
           10 IVA525-PSTOCKFINAL        PIC S9(09)V9(6) COMP-3.         171  008
           10 IVA525-PSTOCKINIT         PIC S9(09)V9(6) COMP-3.         179  008
           10 IVA525-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         187  008
           10 IVA525-PSTOCKSORTIE-E     PIC S9(09)V9(6) COMP-3.         195  008
           10 IVA525-PSTOCKSORTIE-M     PIC S9(09)V9(6) COMP-3.         203  008
           10 IVA525-PSTOCKSORTIE-R     PIC S9(09)V9(6) COMP-3.         211  008
           10 IVA525-PSTOCKRECYCL-S     PIC S9(09)V9(6) COMP-3.         219  008
           10 IVA525-QSTOCKENTREE       PIC S9(11)      COMP-3.         227  006
           10 IVA525-QSTOCKENTREE-E     PIC S9(11)      COMP-3.         233  006
           10 IVA525-QSTOCKENTREE-M     PIC S9(11)      COMP-3.         239  006
           10 IVA525-QSTOCKENTREE-R     PIC S9(11)      COMP-3.         245  006
           10 IVA525-QSTOCKFINAL        PIC S9(11)      COMP-3.         251  006
           10 IVA525-QSTOCKINIT         PIC S9(11)      COMP-3.         257  006
           10 IVA525-QSTOCKSORTIE       PIC S9(11)      COMP-3.         263  006
           10 IVA525-QSTOCKSORTIE-E     PIC S9(11)      COMP-3.         269  006
           10 IVA525-QSTOCKSORTIE-M     PIC S9(11)      COMP-3.         275  006
           10 IVA525-QSTOCKSORTIE-R     PIC S9(11)      COMP-3.         281  006
NT         10 IVA525-TXEURO             PIC S9(01)V9(5) COMP-3.         287  004
            05 FILLER                      PIC X(222).                          
      *     05 FILLER                      PIC X(233).                          
                                                                                
