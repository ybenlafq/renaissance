      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Ese00                                                      00000020
      ***************************************************************** 00000030
       01   EPF00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSOCML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCMI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUML   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLIEUML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUMF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIEUMI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCPLL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSOCPLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSOCPLF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSOCPLI   PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUPLL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLIEUPLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIEUPLF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIEUPLI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRCDEBL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDRCDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRCDEBF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDRCDEBI  PIC X(8).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRCFINL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDRCFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRCFINF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDRCFINI  PIC X(8).                                       00000450
           02 MLIGNEI OCCURS   15 TIMES .                               00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDELIVL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MDDELIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDDELIVF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MDDELIVI     PIC X(8).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRECEPL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MDRECEPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDRECEPF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MDRECEPI     PIC X(8).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCODICI      PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCFAMI  PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNUMSOCL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNUMSOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNUMSOCF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNUMSOCI     PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNUMMAGL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNUMMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNUMMAGF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNUMMAGI     PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVENTEL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MVENTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MVENTEF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MVENTEI      PIC X(7).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIENTL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCLIENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCLIENTF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCLIENTI     PIC X(11).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMDDELL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCMDDELL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCMDDELF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCMDDELI     PIC X.                                     00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSDEPOTL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MSDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSDEPOTF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MSDEPOTI     PIC X(3).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDEPOTL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MLDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLDEPOTF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MLDEPOTI     PIC X(3).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPTFSOCL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MPTFSOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPTFSOCF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MPTFSOCI     PIC X(3).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPFLIEUL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MPFLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPFLIEUF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MPFLIEUI     PIC X(3).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MZONCMDI  PIC X(15).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLIBERRI  PIC X(58).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCICSI    PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MNETNAMI  PIC X(8).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MSCREENI  PIC X(4).                                       00001220
      ***************************************************************** 00001230
      * SDF: Ese00                                                      00001240
      ***************************************************************** 00001250
       01   EPF00O REDEFINES EPF00I.                                    00001260
           02 FILLER    PIC X(12).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTIMJOUA  PIC X.                                          00001360
           02 MTIMJOUC  PIC X.                                          00001370
           02 MTIMJOUP  PIC X.                                          00001380
           02 MTIMJOUH  PIC X.                                          00001390
           02 MTIMJOUV  PIC X.                                          00001400
           02 MTIMJOUO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MPAGEA    PIC X.                                          00001430
           02 MPAGEC    PIC X.                                          00001440
           02 MPAGEP    PIC X.                                          00001450
           02 MPAGEH    PIC X.                                          00001460
           02 MPAGEV    PIC X.                                          00001470
           02 MPAGEO    PIC X(3).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MPAGEMAXA      PIC X.                                     00001500
           02 MPAGEMAXC PIC X.                                          00001510
           02 MPAGEMAXP PIC X.                                          00001520
           02 MPAGEMAXH PIC X.                                          00001530
           02 MPAGEMAXV PIC X.                                          00001540
           02 MPAGEMAXO      PIC X(3).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MSOCMA    PIC X.                                          00001570
           02 MSOCMC    PIC X.                                          00001580
           02 MSOCMP    PIC X.                                          00001590
           02 MSOCMH    PIC X.                                          00001600
           02 MSOCMV    PIC X.                                          00001610
           02 MSOCMO    PIC X(3).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLIEUMA   PIC X.                                          00001640
           02 MLIEUMC   PIC X.                                          00001650
           02 MLIEUMP   PIC X.                                          00001660
           02 MLIEUMH   PIC X.                                          00001670
           02 MLIEUMV   PIC X.                                          00001680
           02 MLIEUMO   PIC X(3).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MSOCPLA   PIC X.                                          00001710
           02 MSOCPLC   PIC X.                                          00001720
           02 MSOCPLP   PIC X.                                          00001730
           02 MSOCPLH   PIC X.                                          00001740
           02 MSOCPLV   PIC X.                                          00001750
           02 MSOCPLO   PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MLIEUPLA  PIC X.                                          00001780
           02 MLIEUPLC  PIC X.                                          00001790
           02 MLIEUPLP  PIC X.                                          00001800
           02 MLIEUPLH  PIC X.                                          00001810
           02 MLIEUPLV  PIC X.                                          00001820
           02 MLIEUPLO  PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDRCDEBA  PIC X.                                          00001850
           02 MDRCDEBC  PIC X.                                          00001860
           02 MDRCDEBP  PIC X.                                          00001870
           02 MDRCDEBH  PIC X.                                          00001880
           02 MDRCDEBV  PIC X.                                          00001890
           02 MDRCDEBO  PIC X(8).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MDRCFINA  PIC X.                                          00001920
           02 MDRCFINC  PIC X.                                          00001930
           02 MDRCFINP  PIC X.                                          00001940
           02 MDRCFINH  PIC X.                                          00001950
           02 MDRCFINV  PIC X.                                          00001960
           02 MDRCFINO  PIC X(8).                                       00001970
           02 MLIGNEO OCCURS   15 TIMES .                               00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MDDELIVA     PIC X.                                     00002000
             03 MDDELIVC     PIC X.                                     00002010
             03 MDDELIVP     PIC X.                                     00002020
             03 MDDELIVH     PIC X.                                     00002030
             03 MDDELIVV     PIC X.                                     00002040
             03 MDDELIVO     PIC X(8).                                  00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MDRECEPA     PIC X.                                     00002070
             03 MDRECEPC     PIC X.                                     00002080
             03 MDRECEPP     PIC X.                                     00002090
             03 MDRECEPH     PIC X.                                     00002100
             03 MDRECEPV     PIC X.                                     00002110
             03 MDRECEPO     PIC X(8).                                  00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MCODICA      PIC X.                                     00002140
             03 MCODICC PIC X.                                          00002150
             03 MCODICP PIC X.                                          00002160
             03 MCODICH PIC X.                                          00002170
             03 MCODICV PIC X.                                          00002180
             03 MCODICO      PIC X(7).                                  00002190
             03 FILLER       PIC X(2).                                  00002200
             03 MCFAMA  PIC X.                                          00002210
             03 MCFAMC  PIC X.                                          00002220
             03 MCFAMP  PIC X.                                          00002230
             03 MCFAMH  PIC X.                                          00002240
             03 MCFAMV  PIC X.                                          00002250
             03 MCFAMO  PIC X(5).                                       00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MNUMSOCA     PIC X.                                     00002280
             03 MNUMSOCC     PIC X.                                     00002290
             03 MNUMSOCP     PIC X.                                     00002300
             03 MNUMSOCH     PIC X.                                     00002310
             03 MNUMSOCV     PIC X.                                     00002320
             03 MNUMSOCO     PIC X(3).                                  00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MNUMMAGA     PIC X.                                     00002350
             03 MNUMMAGC     PIC X.                                     00002360
             03 MNUMMAGP     PIC X.                                     00002370
             03 MNUMMAGH     PIC X.                                     00002380
             03 MNUMMAGV     PIC X.                                     00002390
             03 MNUMMAGO     PIC X(3).                                  00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MVENTEA      PIC X.                                     00002420
             03 MVENTEC PIC X.                                          00002430
             03 MVENTEP PIC X.                                          00002440
             03 MVENTEH PIC X.                                          00002450
             03 MVENTEV PIC X.                                          00002460
             03 MVENTEO      PIC X(7).                                  00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MCLIENTA     PIC X.                                     00002490
             03 MCLIENTC     PIC X.                                     00002500
             03 MCLIENTP     PIC X.                                     00002510
             03 MCLIENTH     PIC X.                                     00002520
             03 MCLIENTV     PIC X.                                     00002530
             03 MCLIENTO     PIC X(11).                                 00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MCMDDELA     PIC X.                                     00002560
             03 MCMDDELC     PIC X.                                     00002570
             03 MCMDDELP     PIC X.                                     00002580
             03 MCMDDELH     PIC X.                                     00002590
             03 MCMDDELV     PIC X.                                     00002600
             03 MCMDDELO     PIC X.                                     00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MSDEPOTA     PIC X.                                     00002630
             03 MSDEPOTC     PIC X.                                     00002640
             03 MSDEPOTP     PIC X.                                     00002650
             03 MSDEPOTH     PIC X.                                     00002660
             03 MSDEPOTV     PIC X.                                     00002670
             03 MSDEPOTO     PIC X(3).                                  00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MLDEPOTA     PIC X.                                     00002700
             03 MLDEPOTC     PIC X.                                     00002710
             03 MLDEPOTP     PIC X.                                     00002720
             03 MLDEPOTH     PIC X.                                     00002730
             03 MLDEPOTV     PIC X.                                     00002740
             03 MLDEPOTO     PIC X(3).                                  00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MPTFSOCA     PIC X.                                     00002770
             03 MPTFSOCC     PIC X.                                     00002780
             03 MPTFSOCP     PIC X.                                     00002790
             03 MPTFSOCH     PIC X.                                     00002800
             03 MPTFSOCV     PIC X.                                     00002810
             03 MPTFSOCO     PIC X(3).                                  00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MPFLIEUA     PIC X.                                     00002840
             03 MPFLIEUC     PIC X.                                     00002850
             03 MPFLIEUP     PIC X.                                     00002860
             03 MPFLIEUH     PIC X.                                     00002870
             03 MPFLIEUV     PIC X.                                     00002880
             03 MPFLIEUO     PIC X(3).                                  00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MZONCMDA  PIC X.                                          00002910
           02 MZONCMDC  PIC X.                                          00002920
           02 MZONCMDP  PIC X.                                          00002930
           02 MZONCMDH  PIC X.                                          00002940
           02 MZONCMDV  PIC X.                                          00002950
           02 MZONCMDO  PIC X(15).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLIBERRA  PIC X.                                          00002980
           02 MLIBERRC  PIC X.                                          00002990
           02 MLIBERRP  PIC X.                                          00003000
           02 MLIBERRH  PIC X.                                          00003010
           02 MLIBERRV  PIC X.                                          00003020
           02 MLIBERRO  PIC X(58).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSO    PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNETNAMA  PIC X.                                          00003190
           02 MNETNAMC  PIC X.                                          00003200
           02 MNETNAMP  PIC X.                                          00003210
           02 MNETNAMH  PIC X.                                          00003220
           02 MNETNAMV  PIC X.                                          00003230
           02 MNETNAMO  PIC X(8).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSCREENA  PIC X.                                          00003260
           02 MSCREENC  PIC X.                                          00003270
           02 MSCREENP  PIC X.                                          00003280
           02 MSCREENH  PIC X.                                          00003290
           02 MSCREENV  PIC X.                                          00003300
           02 MSCREENO  PIC X(4).                                       00003310
                                                                                
