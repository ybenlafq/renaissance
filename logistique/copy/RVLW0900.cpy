      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLW0900                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLW0900                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW0900.                                                            
           02  LW09-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  LW09-NDEPOT                                                      
               PIC X(0003).                                                     
           02  LW09-NCODIC                                                      
               PIC X(0007).                                                     
           02  LW09-CEMPL                                                       
               PIC X(0005).                                                     
           02  LW09-ADRESSE                                                     
               PIC X(0020).                                                     
           02  LW09-NSUPPORT                                                    
               PIC X(0018).                                                     
           02  LW09-DSTOCK                                                      
               PIC X(0008).                                                     
           02  LW09-QSTOCK                                                      
               PIC S9(9) COMP-3.                                                
           02  LW09-CETAT                                                       
               PIC X(0002).                                                     
           02  LW09-CBLOCAGE                                                    
               PIC X(0003).                                                     
           02  LW09-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLW0900                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW0900-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW09-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW09-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW09-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW09-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW09-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW09-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW09-CEMPL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW09-CEMPL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW09-ADRESSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW09-ADRESSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW09-NSUPPORT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW09-NSUPPORT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW09-DSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW09-DSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW09-QSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW09-QSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW09-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW09-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW09-CBLOCAGE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW09-CBLOCAGE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW09-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW09-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
