      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IQST40 AU 27/04/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,08,BI,A,                          *        
      *                           23,06,BI,A,                          *        
      *                           29,20,BI,A,                          *        
      *                           49,07,BI,A,                          *        
      *                           56,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IQST40.                                                        
            05 NOMETAT-IQST40           PIC X(6) VALUE 'IQST40'.                
            05 RUPTURES-IQST40.                                                 
           10 IQST40-CFAM               PIC X(08).                      007  008
           10 IQST40-DMAJSTOCK          PIC X(08).                      015  008
           10 IQST40-CMARQ              PIC X(06).                      023  006
           10 IQST40-LREFFOURN          PIC X(20).                      029  020
           10 IQST40-NCODIC             PIC X(07).                      049  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IQST40-SEQUENCE           PIC S9(04) COMP.                056  002
      *--                                                                       
           10 IQST40-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IQST40.                                                   
           10 IQST40-NLIEUDEST          PIC X(04).                      058  004
           10 IQST40-NLIEUORIG          PIC X(04).                      062  004
           10 IQST40-NLIEUVTE           PIC X(03).                      066  003
           10 IQST40-NORIGINE           PIC X(07).                      069  007
           10 IQST40-QMVT               PIC X(07).                      076  007
           10 IQST40-QSTOCK             PIC S9(07)     .                083  007
            05 FILLER                      PIC X(423).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IQST40-LONG           PIC S9(4)   COMP  VALUE +089.           
      *                                                                         
      *--                                                                       
        01  DSECT-IQST40-LONG           PIC S9(4) COMP-5  VALUE +089.           
                                                                                
      *}                                                                        
