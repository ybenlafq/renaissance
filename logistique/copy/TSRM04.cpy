      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00010001
      * TS SPECIFIQUE TRM04                                          *  00020001
      ****************************************************************  00050001
      *                                                                 00060001
      *------------------------------ LONGUEUR                          00070001
YS1864*01  TS-LONG              PIC S9(2) COMP-3 VALUE 54.              00080003
YS1864*01  TS-LONG              PIC S9(2) COMP-3 VALUE 58.              00081005
YS2594 01  TS-LONG              PIC S9(2) COMP-3 VALUE 82.              00082005
       01  TS-DONNEES.                                                  00090001
              10 TS-MNSOC       PIC X(03).                              00100001
              10 TS-MNMAG       PIC X(03).                              00120001
              10 TS-MLMAG       PIC X(20).                              00121001
              10 TS-MQTRESER    PIC X(04).                              00122001
              10 TS-MNPOIDSJ    PIC X(02).                              00123001
              10 TS-MQFREQ1     PIC X(04).                              00124001
              10 TS-MQFREQ2     PIC X(04).                              00124101
              10 TS-MQFREQ3     PIC X(04).                              00124201
              10 TS-MQFREQ4     PIC X(04).                              00124301
              10 TS-MQFREQ5     PIC X(04).                              00124401
YS1864        10 TS-MQFREQ6     PIC X(04).                              00124503
YS2594        10 TS-MQFREQ7     PIC X(04).                              00124604
YS2594        10 TS-MQFREQ8     PIC X(04).                              00124704
YS2594        10 TS-MQFREQ9     PIC X(04).                              00124804
YS2594        10 TS-MQFREQ10    PIC X(04).                              00124904
YS2594        10 TS-MQFREQ11    PIC X(04).                              00125004
YS2594        10 TS-MQFREQ12    PIC X(04).                              00125104
              10 TS-MWEXCFM     PIC X.                                  00125204
              10 TS-CHGT        PIC X.                                  00126004
                                                                                
