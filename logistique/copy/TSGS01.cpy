      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *             TRANSACTION "GS00" / PROGRAMME TGS01              *         
      *   TS : CONSULTATION DES STOCKS ENTREPOT PAR FAMILLE           *         
      *****************************************************************         
      *                                                               *         
       01  TS-NOM.                                                              
           05  FILLER                      PIC X(4) VALUE 'GS01'.               
           05  TS-GS01-TERMINAL            PIC X(4).                            
      *----------------------------83 * 11 LONGUEUR TS                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-GS01-LONG                    PIC S9(4) COMP VALUE +946.           
      *--                                                                       
       01  TS-GS01-LONG                    PIC S9(4) COMP-5 VALUE +946.         
      *}                                                                        
      *----------------------------------  DONNEES  TS                          
       01  TS-GS01-DONNEES.                                                     
          02 TS-GS01-LIGNE                 OCCURS 11.                           
      *----------------------------------  CODE FAMILLE                         
              03 TS-GS01-CFAM              PIC X(5).                            
      *----------------------------------  LIBELLE FAMILLE                      
              03 TS-GS01-LFAM              PIC X(20).                           
      *----------------------------------  CODE ARTICLE                         
              03 TS-GS01-NCODIC            PIC X(7).                            
      *----------------------------------  CODE MARQUE                          
              03 TS-GS01-CMARQ             PIC X(5).                            
      *----------------------------------  LIBELLE REFERENCE                    
              03 TS-GS01-LREFFOURN         PIC X(20).                           
      *----------------------------------  CODE STATUT COMPACTE                 
              03 TS-GS01-LSTATCOMP         PIC X(3).                            
      *----------------------------------  CODE SENSIBILITE APPRO               
              03 TS-GS01-WSENSAPPRO        PIC X(1).                            
      *----------------------------------  CODE SENSIBILITE VENTE               
              03 TS-GS01-WSENSVTE          PIC X(1).                            
      *----------------------------------  ZONE STOCKS                          
AA1115        03 TS-GS01-CROSSDOCKFOUR     PIC X(1).                            
      *----------------------------------  ZONE STOCKS                          
              03 TS-GS01-ZONSTOCK.                                              
      *----------------------------------  ZONE STOCKS 1                        
                 04 TS-GS01-QSTOCK1        PIC S9(5) COMP-3 OCCURS 4.           
      *----------------------------------  ZONE STOCKS 2                        
                 04 TS-GS01-QSTOCK2        PIC S9(5) COMP-3 OCCURS 3.           
                                                                                
