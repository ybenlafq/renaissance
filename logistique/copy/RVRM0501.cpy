      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM0501                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM0501                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM0501.                                                            
           02  RM05-CGROUP                                                      
               PIC X(0005).                                                     
           02  RM05-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RM05-NLIEU                                                       
               PIC X(0003).                                                     
           02  RM05-QTAILLRESERVE                                               
               PIC S9(1)V9(0002) COMP-3.                                        
           02  RM05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  RM05-NTABLJOUR                                                   
               PIC X(0002).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM0501                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM0501-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM05-CGROUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM05-CGROUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM05-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM05-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM05-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM05-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM05-QTAILLRESERVE-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM05-QTAILLRESERVE-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM05-NTABLJOUR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM05-NTABLJOUR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
