      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGQ03   EGQ03                                              00000020
      ***************************************************************** 00000030
       01   EGQ03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MFONCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODDELL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCMODDELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMODDELF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCMODDELI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMODDELL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLMODDELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLMODDELF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLMODDELI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPERIML  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCPERIML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPERIMF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCPERIMI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPERIML  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLPERIML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPERIMF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLPERIMI  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEFFETI  PIC X(8).                                       00000410
           02 MTABLEI OCCURS   13 TIMES .                               00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCFAMI  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLFAMI  PIC X(20).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEQUIP1L    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCEQUIP1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCEQUIP1F    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCEQUIP1I    PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEQUIP2L    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCEQUIP2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCEQUIP2F    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCEQUIP2I    PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEQUIP3L    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCEQUIP3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCEQUIP3F    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCEQUIP3I    PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEQUIP4L    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCEQUIP4L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCEQUIP4F    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCEQUIP4I    PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEQUIP5L    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCEQUIP5L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCEQUIP5F    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCEQUIP5I    PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MZONCMDI  PIC X(15).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(58).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: EGQ03   EGQ03                                              00000960
      ***************************************************************** 00000970
       01   EGQ03O REDEFINES EGQ03I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEA    PIC X.                                          00001150
           02 MPAGEC    PIC X.                                          00001160
           02 MPAGEP    PIC X.                                          00001170
           02 MPAGEH    PIC X.                                          00001180
           02 MPAGEV    PIC X.                                          00001190
           02 MPAGEO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MFONCA    PIC X.                                          00001220
           02 MFONCC    PIC X.                                          00001230
           02 MFONCP    PIC X.                                          00001240
           02 MFONCH    PIC X.                                          00001250
           02 MFONCV    PIC X.                                          00001260
           02 MFONCO    PIC X(3).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MCMODDELA      PIC X.                                     00001290
           02 MCMODDELC PIC X.                                          00001300
           02 MCMODDELP PIC X.                                          00001310
           02 MCMODDELH PIC X.                                          00001320
           02 MCMODDELV PIC X.                                          00001330
           02 MCMODDELO      PIC X(3).                                  00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLMODDELA      PIC X.                                     00001360
           02 MLMODDELC PIC X.                                          00001370
           02 MLMODDELP PIC X.                                          00001380
           02 MLMODDELH PIC X.                                          00001390
           02 MLMODDELV PIC X.                                          00001400
           02 MLMODDELO      PIC X(20).                                 00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCPERIMA  PIC X.                                          00001430
           02 MCPERIMC  PIC X.                                          00001440
           02 MCPERIMP  PIC X.                                          00001450
           02 MCPERIMH  PIC X.                                          00001460
           02 MCPERIMV  PIC X.                                          00001470
           02 MCPERIMO  PIC X(5).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MLPERIMA  PIC X.                                          00001500
           02 MLPERIMC  PIC X.                                          00001510
           02 MLPERIMP  PIC X.                                          00001520
           02 MLPERIMH  PIC X.                                          00001530
           02 MLPERIMV  PIC X.                                          00001540
           02 MLPERIMO  PIC X(20).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDEFFETA  PIC X.                                          00001570
           02 MDEFFETC  PIC X.                                          00001580
           02 MDEFFETP  PIC X.                                          00001590
           02 MDEFFETH  PIC X.                                          00001600
           02 MDEFFETV  PIC X.                                          00001610
           02 MDEFFETO  PIC X(8).                                       00001620
           02 MTABLEO OCCURS   13 TIMES .                               00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MCFAMA  PIC X.                                          00001650
             03 MCFAMC  PIC X.                                          00001660
             03 MCFAMP  PIC X.                                          00001670
             03 MCFAMH  PIC X.                                          00001680
             03 MCFAMV  PIC X.                                          00001690
             03 MCFAMO  PIC X(5).                                       00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MLFAMA  PIC X.                                          00001720
             03 MLFAMC  PIC X.                                          00001730
             03 MLFAMP  PIC X.                                          00001740
             03 MLFAMH  PIC X.                                          00001750
             03 MLFAMV  PIC X.                                          00001760
             03 MLFAMO  PIC X(20).                                      00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MCEQUIP1A    PIC X.                                     00001790
             03 MCEQUIP1C    PIC X.                                     00001800
             03 MCEQUIP1P    PIC X.                                     00001810
             03 MCEQUIP1H    PIC X.                                     00001820
             03 MCEQUIP1V    PIC X.                                     00001830
             03 MCEQUIP1O    PIC X(5).                                  00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MCEQUIP2A    PIC X.                                     00001860
             03 MCEQUIP2C    PIC X.                                     00001870
             03 MCEQUIP2P    PIC X.                                     00001880
             03 MCEQUIP2H    PIC X.                                     00001890
             03 MCEQUIP2V    PIC X.                                     00001900
             03 MCEQUIP2O    PIC X(5).                                  00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MCEQUIP3A    PIC X.                                     00001930
             03 MCEQUIP3C    PIC X.                                     00001940
             03 MCEQUIP3P    PIC X.                                     00001950
             03 MCEQUIP3H    PIC X.                                     00001960
             03 MCEQUIP3V    PIC X.                                     00001970
             03 MCEQUIP3O    PIC X(5).                                  00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MCEQUIP4A    PIC X.                                     00002000
             03 MCEQUIP4C    PIC X.                                     00002010
             03 MCEQUIP4P    PIC X.                                     00002020
             03 MCEQUIP4H    PIC X.                                     00002030
             03 MCEQUIP4V    PIC X.                                     00002040
             03 MCEQUIP4O    PIC X(5).                                  00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MCEQUIP5A    PIC X.                                     00002070
             03 MCEQUIP5C    PIC X.                                     00002080
             03 MCEQUIP5P    PIC X.                                     00002090
             03 MCEQUIP5H    PIC X.                                     00002100
             03 MCEQUIP5V    PIC X.                                     00002110
             03 MCEQUIP5O    PIC X(5).                                  00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MZONCMDA  PIC X.                                          00002140
           02 MZONCMDC  PIC X.                                          00002150
           02 MZONCMDP  PIC X.                                          00002160
           02 MZONCMDH  PIC X.                                          00002170
           02 MZONCMDV  PIC X.                                          00002180
           02 MZONCMDO  PIC X(15).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(58).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
