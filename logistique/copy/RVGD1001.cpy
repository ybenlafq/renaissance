      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGD1001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGD1001                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGD1001.                                                            
           02  GD10-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GD10-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GD10-DRAFALE                                                     
               PIC X(0008).                                                     
           02  GD10-NRAFALE                                                     
               PIC X(0003).                                                     
           02  GD10-NCODIC                                                      
               PIC X(0007).                                                     
           02  GD10-WSEQ                                                        
               PIC S9(3) COMP-3.                                                
           02  GD10-WCOMMUT                                                     
               PIC X(0001).                                                     
           02  GD10-WVENTMUT                                                    
               PIC X(0001).                                                     
           02  GD10-NSOC                                                        
               PIC X(0003).                                                     
           02  GD10-NMAGASIN                                                    
               PIC X(0003).                                                     
           02  GD10-NORIGINE                                                    
               PIC X(0007).                                                     
           02  GD10-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  GD10-NSEQ                                                        
               PIC X(0002).                                                     
           02  GD10-CZONE                                                       
               PIC X(0002).                                                     
           02  GD10-CNIVEAU                                                     
               PIC X(0002).                                                     
           02  GD10-NPOSITION                                                   
               PIC X(0003).                                                     
           02  GD10-NSATELLITE                                                  
               PIC X(0002).                                                     
           02  GD10-NCASE                                                       
               PIC X(0003).                                                     
           02  GD10-WTYPEMPL                                                    
               PIC X(0001).                                                     
           02  GD10-WCODICCART                                                  
               PIC X(0001).                                                     
           02  GD10-WREMPEMPL                                                   
               PIC X(0001).                                                     
           02  GD10-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  GD10-CFAM                                                        
               PIC X(0005).                                                     
           02  GD10-CMARQ                                                       
               PIC X(0005).                                                     
           02  GD10-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  GD10-LEMBALLAGE                                                  
               PIC X(0050).                                                     
           02  GD10-QCONDT                                                      
               PIC S9(5) COMP-3.                                                
           02  GD10-QCDEDEM                                                     
               PIC S9(5) COMP-3.                                                
           02  GD10-QCDERES                                                     
               PIC S9(5) COMP-3.                                                
           02  GD10-QCDEPREL                                                    
               PIC S9(5) COMP-3.                                                
           02  GD10-NORDRE                                                      
               PIC S9(7) COMP-3.                                                
           02  GD10-CMODLIVR                                                    
               PIC X(0003).                                                     
           02  GD10-NPRIORITE                                                   
               PIC X(0001).                                                     
           02  GD10-NRUPT                                                       
               PIC S9(5) COMP-3.                                                
           02  GD10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GD10-NBONENLV                                                    
               PIC X(0007).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGD1001                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGD1001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-DRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-DRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-WSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-WSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-WCOMMUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-WCOMMUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-WVENTMUT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-WVENTMUT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NMAGASIN-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NMAGASIN-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-CZONE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-CZONE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-CNIVEAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-CNIVEAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NPOSITION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NPOSITION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NSATELLITE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NSATELLITE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NCASE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NCASE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-WTYPEMPL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-WTYPEMPL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-WCODICCART-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-WCODICCART-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-WREMPEMPL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-WREMPEMPL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-LEMBALLAGE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-LEMBALLAGE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-QCONDT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-QCONDT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-QCDEDEM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-QCDEDEM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-QCDERES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-QCDERES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-QCDEPREL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-QCDEPREL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-CMODLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-CMODLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NPRIORITE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NPRIORITE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NRUPT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NRUPT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD10-NBONENLV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD10-NBONENLV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
