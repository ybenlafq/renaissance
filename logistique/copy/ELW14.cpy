      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SUIVI DES COMMANDES LM7                                         00000020
      ***************************************************************** 00000030
       01   ELW14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDEPF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCDEPI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPTL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MDRECEPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRECEPTF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDRECEPTI      PIC X(8).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTRIL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MWTRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWTRIF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MWTRII    PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSELL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MWSELL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWSELF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MWSELI    PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTOTVOLL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MQTOTVOLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQTOTVOLF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MQTOTVOLI      PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTOTQTEL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MQTOTQTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQTOTQTEF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MQTOTQTEI      PIC X(5).                                  00000530
           02 MTIRETD OCCURS   7 TIMES .                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTIRETL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MTIRETL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTIRETF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MTIRETI      PIC X.                                     00000580
           02 MLENTD OCCURS   7 TIMES .                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTL  COMP PIC S9(4).                                 00000600
      *--                                                                       
             03 MLENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLENTF  PIC X.                                          00000610
             03 FILLER  PIC X(4).                                       00000620
             03 MLENTI  PIC X(20).                                      00000630
           02 MNCDED OCCURS   7 TIMES .                                 00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEL  COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MNCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCDEF  PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MNCDEI  PIC X(7).                                       00000680
           02 MQVOLD OCCURS   7 TIMES .                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVOLL  COMP PIC S9(4).                                 00000700
      *--                                                                       
             03 MQVOLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQVOLF  PIC X.                                          00000710
             03 FILLER  PIC X(4).                                       00000720
             03 MQVOLI  PIC X(5).                                       00000730
           02 MNMAGVTED OCCURS   7 TIMES .                              00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGVTEL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNMAGVTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNMAGVTEF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNMAGVTEI    PIC X(6).                                  00000780
           02 MNVENTED OCCURS   7 TIMES .                               00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000800
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MNVENTEI     PIC X(7).                                  00000830
           02 MQCDED OCCURS   7 TIMES .                                 00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCDEL  COMP PIC S9(4).                                 00000850
      *--                                                                       
             03 MQCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQCDEF  PIC X.                                          00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MQCDEI  PIC X(5).                                       00000880
           02 MNCODICD OCCURS   7 TIMES .                               00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000900
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000910
             03 FILLER  PIC X(4).                                       00000920
             03 MNCODICI     PIC X(7).                                  00000930
           02 MCFAMD OCCURS   7 TIMES .                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MCFAMI  PIC X(5).                                       00000980
           02 MCMARQD OCCURS   7 TIMES .                                00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00001000
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00001010
             03 FILLER  PIC X(4).                                       00001020
             03 MCMARQI      PIC X(5).                                  00001030
           02 MLREFD OCCURS   7 TIMES .                                 00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00001050
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00001060
             03 FILLER  PIC X(4).                                       00001070
             03 MLREFI  PIC X(14).                                      00001080
           02 MNDESTD OCCURS   7 TIMES .                                00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDESTL      COMP PIC S9(4).                            00001100
      *--                                                                       
             03 MNDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNDESTF      PIC X.                                     00001110
             03 FILLER  PIC X(4).                                       00001120
             03 MNDESTI      PIC X(6).                                  00001130
           02 MNMUTD OCCURS   7 TIMES .                                 00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTL  COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MNMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMUTF  PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MNMUTI  PIC X(7).                                       00001180
           02 MQRECD OCCURS   7 TIMES .                                 00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRECL  COMP PIC S9(4).                                 00001200
      *--                                                                       
             03 MQRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQRECF  PIC X.                                          00001210
             03 FILLER  PIC X(4).                                       00001220
             03 MQRECI  PIC X(5).                                       00001230
           02 MDRECD OCCURS   7 TIMES .                                 00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRECL  COMP PIC S9(4).                                 00001250
      *--                                                                       
             03 MDRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDRECF  PIC X.                                          00001260
             03 FILLER  PIC X(4).                                       00001270
             03 MDRECI  PIC X(8).                                       00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MLIBERRI  PIC X(78).                                      00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001340
           02 FILLER    PIC X(4).                                       00001350
           02 MCODTRAI  PIC X(4).                                       00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001370
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001380
           02 FILLER    PIC X(4).                                       00001390
           02 MCICSI    PIC X(5).                                       00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001410
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001420
           02 FILLER    PIC X(4).                                       00001430
           02 MNETNAMI  PIC X(8).                                       00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001450
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001460
           02 FILLER    PIC X(4).                                       00001470
           02 MSCREENI  PIC X(4).                                       00001480
      ***************************************************************** 00001490
      * SUIVI DES COMMANDES LM7                                         00001500
      ***************************************************************** 00001510
       01   ELW14O REDEFINES ELW14I.                                    00001520
           02 FILLER    PIC X(12).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MDATJOUA  PIC X.                                          00001550
           02 MDATJOUC  PIC X.                                          00001560
           02 MDATJOUP  PIC X.                                          00001570
           02 MDATJOUH  PIC X.                                          00001580
           02 MDATJOUV  PIC X.                                          00001590
           02 MDATJOUO  PIC X(10).                                      00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MTIMJOUA  PIC X.                                          00001620
           02 MTIMJOUC  PIC X.                                          00001630
           02 MTIMJOUP  PIC X.                                          00001640
           02 MTIMJOUH  PIC X.                                          00001650
           02 MTIMJOUV  PIC X.                                          00001660
           02 MTIMJOUO  PIC X(5).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MPAGEA    PIC X.                                          00001690
           02 MPAGEC    PIC X.                                          00001700
           02 MPAGEP    PIC X.                                          00001710
           02 MPAGEH    PIC X.                                          00001720
           02 MPAGEV    PIC X.                                          00001730
           02 MPAGEO    PIC X(4).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNPAGEA   PIC X.                                          00001760
           02 MNPAGEC   PIC X.                                          00001770
           02 MNPAGEP   PIC X.                                          00001780
           02 MNPAGEH   PIC X.                                          00001790
           02 MNPAGEV   PIC X.                                          00001800
           02 MNPAGEO   PIC X(4).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MNSOCDEPA      PIC X.                                     00001830
           02 MNSOCDEPC PIC X.                                          00001840
           02 MNSOCDEPP PIC X.                                          00001850
           02 MNSOCDEPH PIC X.                                          00001860
           02 MNSOCDEPV PIC X.                                          00001870
           02 MNSOCDEPO      PIC X(3).                                  00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MNDEPOTA  PIC X.                                          00001900
           02 MNDEPOTC  PIC X.                                          00001910
           02 MNDEPOTP  PIC X.                                          00001920
           02 MNDEPOTH  PIC X.                                          00001930
           02 MNDEPOTV  PIC X.                                          00001940
           02 MNDEPOTO  PIC X(3).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MLLIEUA   PIC X.                                          00001970
           02 MLLIEUC   PIC X.                                          00001980
           02 MLLIEUP   PIC X.                                          00001990
           02 MLLIEUH   PIC X.                                          00002000
           02 MLLIEUV   PIC X.                                          00002010
           02 MLLIEUO   PIC X(20).                                      00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MDRECEPTA      PIC X.                                     00002040
           02 MDRECEPTC PIC X.                                          00002050
           02 MDRECEPTP PIC X.                                          00002060
           02 MDRECEPTH PIC X.                                          00002070
           02 MDRECEPTV PIC X.                                          00002080
           02 MDRECEPTO      PIC X(8).                                  00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MWTRIA    PIC X.                                          00002110
           02 MWTRIC    PIC X.                                          00002120
           02 MWTRIP    PIC X.                                          00002130
           02 MWTRIH    PIC X.                                          00002140
           02 MWTRIV    PIC X.                                          00002150
           02 MWTRIO    PIC X.                                          00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MWSELA    PIC X.                                          00002180
           02 MWSELC    PIC X.                                          00002190
           02 MWSELP    PIC X.                                          00002200
           02 MWSELH    PIC X.                                          00002210
           02 MWSELV    PIC X.                                          00002220
           02 MWSELO    PIC X.                                          00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MQTOTVOLA      PIC X.                                     00002250
           02 MQTOTVOLC PIC X.                                          00002260
           02 MQTOTVOLP PIC X.                                          00002270
           02 MQTOTVOLH PIC X.                                          00002280
           02 MQTOTVOLV PIC X.                                          00002290
           02 MQTOTVOLO      PIC X(7).                                  00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MQTOTQTEA      PIC X.                                     00002320
           02 MQTOTQTEC PIC X.                                          00002330
           02 MQTOTQTEP PIC X.                                          00002340
           02 MQTOTQTEH PIC X.                                          00002350
           02 MQTOTQTEV PIC X.                                          00002360
           02 MQTOTQTEO      PIC X(5).                                  00002370
           02 DFHMS1 OCCURS   7 TIMES .                                 00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MTIRETA      PIC X.                                     00002400
             03 MTIRETC PIC X.                                          00002410
             03 MTIRETP PIC X.                                          00002420
             03 MTIRETH PIC X.                                          00002430
             03 MTIRETV PIC X.                                          00002440
             03 MTIRETO      PIC X.                                     00002450
           02 DFHMS2 OCCURS   7 TIMES .                                 00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MLENTA  PIC X.                                          00002480
             03 MLENTC  PIC X.                                          00002490
             03 MLENTP  PIC X.                                          00002500
             03 MLENTH  PIC X.                                          00002510
             03 MLENTV  PIC X.                                          00002520
             03 MLENTO  PIC X(20).                                      00002530
           02 DFHMS3 OCCURS   7 TIMES .                                 00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MNCDEA  PIC X.                                          00002560
             03 MNCDEC  PIC X.                                          00002570
             03 MNCDEP  PIC X.                                          00002580
             03 MNCDEH  PIC X.                                          00002590
             03 MNCDEV  PIC X.                                          00002600
             03 MNCDEO  PIC X(7).                                       00002610
           02 DFHMS4 OCCURS   7 TIMES .                                 00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MQVOLA  PIC X.                                          00002640
             03 MQVOLC  PIC X.                                          00002650
             03 MQVOLP  PIC X.                                          00002660
             03 MQVOLH  PIC X.                                          00002670
             03 MQVOLV  PIC X.                                          00002680
             03 MQVOLO  PIC X(5).                                       00002690
           02 DFHMS5 OCCURS   7 TIMES .                                 00002700
             03 FILLER       PIC X(2).                                  00002710
             03 MNMAGVTEA    PIC X.                                     00002720
             03 MNMAGVTEC    PIC X.                                     00002730
             03 MNMAGVTEP    PIC X.                                     00002740
             03 MNMAGVTEH    PIC X.                                     00002750
             03 MNMAGVTEV    PIC X.                                     00002760
             03 MNMAGVTEO    PIC X(6).                                  00002770
           02 DFHMS6 OCCURS   7 TIMES .                                 00002780
             03 FILLER       PIC X(2).                                  00002790
             03 MNVENTEA     PIC X.                                     00002800
             03 MNVENTEC     PIC X.                                     00002810
             03 MNVENTEP     PIC X.                                     00002820
             03 MNVENTEH     PIC X.                                     00002830
             03 MNVENTEV     PIC X.                                     00002840
             03 MNVENTEO     PIC X(7).                                  00002850
           02 DFHMS7 OCCURS   7 TIMES .                                 00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MQCDEA  PIC X.                                          00002880
             03 MQCDEC  PIC X.                                          00002890
             03 MQCDEP  PIC X.                                          00002900
             03 MQCDEH  PIC X.                                          00002910
             03 MQCDEV  PIC X.                                          00002920
             03 MQCDEO  PIC X(5).                                       00002930
           02 DFHMS8 OCCURS   7 TIMES .                                 00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MNCODICA     PIC X.                                     00002960
             03 MNCODICC     PIC X.                                     00002970
             03 MNCODICP     PIC X.                                     00002980
             03 MNCODICH     PIC X.                                     00002990
             03 MNCODICV     PIC X.                                     00003000
             03 MNCODICO     PIC X(7).                                  00003010
           02 DFHMS9 OCCURS   7 TIMES .                                 00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MCFAMA  PIC X.                                          00003040
             03 MCFAMC  PIC X.                                          00003050
             03 MCFAMP  PIC X.                                          00003060
             03 MCFAMH  PIC X.                                          00003070
             03 MCFAMV  PIC X.                                          00003080
             03 MCFAMO  PIC X(5).                                       00003090
           02 DFHMS10 OCCURS   7 TIMES .                                00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MCMARQA      PIC X.                                     00003120
             03 MCMARQC PIC X.                                          00003130
             03 MCMARQP PIC X.                                          00003140
             03 MCMARQH PIC X.                                          00003150
             03 MCMARQV PIC X.                                          00003160
             03 MCMARQO      PIC X(5).                                  00003170
           02 DFHMS11 OCCURS   7 TIMES .                                00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MLREFA  PIC X.                                          00003200
             03 MLREFC  PIC X.                                          00003210
             03 MLREFP  PIC X.                                          00003220
             03 MLREFH  PIC X.                                          00003230
             03 MLREFV  PIC X.                                          00003240
             03 MLREFO  PIC X(14).                                      00003250
           02 DFHMS12 OCCURS   7 TIMES .                                00003260
             03 FILLER       PIC X(2).                                  00003270
             03 MNDESTA      PIC X.                                     00003280
             03 MNDESTC PIC X.                                          00003290
             03 MNDESTP PIC X.                                          00003300
             03 MNDESTH PIC X.                                          00003310
             03 MNDESTV PIC X.                                          00003320
             03 MNDESTO      PIC X(6).                                  00003330
           02 DFHMS13 OCCURS   7 TIMES .                                00003340
             03 FILLER       PIC X(2).                                  00003350
             03 MNMUTA  PIC X.                                          00003360
             03 MNMUTC  PIC X.                                          00003370
             03 MNMUTP  PIC X.                                          00003380
             03 MNMUTH  PIC X.                                          00003390
             03 MNMUTV  PIC X.                                          00003400
             03 MNMUTO  PIC X(7).                                       00003410
           02 DFHMS14 OCCURS   7 TIMES .                                00003420
             03 FILLER       PIC X(2).                                  00003430
             03 MQRECA  PIC X.                                          00003440
             03 MQRECC  PIC X.                                          00003450
             03 MQRECP  PIC X.                                          00003460
             03 MQRECH  PIC X.                                          00003470
             03 MQRECV  PIC X.                                          00003480
             03 MQRECO  PIC X(5).                                       00003490
           02 DFHMS15 OCCURS   7 TIMES .                                00003500
             03 FILLER       PIC X(2).                                  00003510
             03 MDRECA  PIC X.                                          00003520
             03 MDRECC  PIC X.                                          00003530
             03 MDRECP  PIC X.                                          00003540
             03 MDRECH  PIC X.                                          00003550
             03 MDRECV  PIC X.                                          00003560
             03 MDRECO  PIC X(8).                                       00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MLIBERRA  PIC X.                                          00003590
           02 MLIBERRC  PIC X.                                          00003600
           02 MLIBERRP  PIC X.                                          00003610
           02 MLIBERRH  PIC X.                                          00003620
           02 MLIBERRV  PIC X.                                          00003630
           02 MLIBERRO  PIC X(78).                                      00003640
           02 FILLER    PIC X(2).                                       00003650
           02 MCODTRAA  PIC X.                                          00003660
           02 MCODTRAC  PIC X.                                          00003670
           02 MCODTRAP  PIC X.                                          00003680
           02 MCODTRAH  PIC X.                                          00003690
           02 MCODTRAV  PIC X.                                          00003700
           02 MCODTRAO  PIC X(4).                                       00003710
           02 FILLER    PIC X(2).                                       00003720
           02 MCICSA    PIC X.                                          00003730
           02 MCICSC    PIC X.                                          00003740
           02 MCICSP    PIC X.                                          00003750
           02 MCICSH    PIC X.                                          00003760
           02 MCICSV    PIC X.                                          00003770
           02 MCICSO    PIC X(5).                                       00003780
           02 FILLER    PIC X(2).                                       00003790
           02 MNETNAMA  PIC X.                                          00003800
           02 MNETNAMC  PIC X.                                          00003810
           02 MNETNAMP  PIC X.                                          00003820
           02 MNETNAMH  PIC X.                                          00003830
           02 MNETNAMV  PIC X.                                          00003840
           02 MNETNAMO  PIC X(8).                                       00003850
           02 FILLER    PIC X(2).                                       00003860
           02 MSCREENA  PIC X.                                          00003870
           02 MSCREENC  PIC X.                                          00003880
           02 MSCREENP  PIC X.                                          00003890
           02 MSCREENH  PIC X.                                          00003900
           02 MSCREENV  PIC X.                                          00003910
           02 MSCREENO  PIC X(4).                                       00003920
                                                                                
