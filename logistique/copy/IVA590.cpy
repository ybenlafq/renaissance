      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA590      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,03,PD,A,                          *        
      *                           23,07,BI,A,                          *        
      *                           30,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA590.                                                        
            05 NOMETAT-IVA590           PIC X(6) VALUE 'IVA590'.                
            05 RUPTURES-IVA590.                                                 
           10 IVA590-NSOCVALO           PIC X(03).                      007  003
           10 IVA590-CSEQRAYON          PIC X(02).                      010  002
           10 IVA590-NLIEUVALO          PIC X(03).                      012  003
           10 IVA590-CMARQ              PIC X(05).                      015  005
           10 IVA590-WSEQFAM            PIC S9(05)      COMP-3.         020  003
           10 IVA590-NCODIC             PIC X(07).                      023  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA590-SEQUENCE           PIC S9(04) COMP.                030  002
      *--                                                                       
           10 IVA590-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA590.                                                   
           10 IVA590-CFAM               PIC X(05).                      032  005
           10 IVA590-CRAYON1            PIC X(05).                      037  005
           10 IVA590-LREFFOURN          PIC X(20).                      042  020
           10 IVA590-NMVTENTREE         PIC X(07) OCCURS 2.             062  014
           10 IVA590-NMVTSORTIE         PIC X(07) OCCURS 2.             076  014
           10 IVA590-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         090  008
           10 IVA590-PSTOCKFINAL        PIC S9(09)V9(6) COMP-3.         098  008
           10 IVA590-PSTOCKINIT         PIC S9(09)V9(6) COMP-3.         106  008
           10 IVA590-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         114  008
           10 IVA590-QSTOCKENTREE       PIC S9(11)      COMP-3.         122  006
           10 IVA590-QSTOCKFINAL        PIC S9(11)      COMP-3.         128  006
           10 IVA590-QSTOCKINIT         PIC S9(11)      COMP-3.         134  006
           10 IVA590-QSTOCKSORTIE       PIC S9(11)      COMP-3.         140  006
            05 FILLER                      PIC X(367).                          
                                                                                
