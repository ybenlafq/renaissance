      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS59   EGS59                                              00000020
      ***************************************************************** 00000030
       01   EGS59I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MPAGEI .                                                  00000170
             03 MNPAGEI      PIC X(3).                                  00000180
             03 MSLASHI      PIC X.                                     00000190
             03 MNBPAGEI     PIC X(3).                                  00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENTL      COMP PIC S9(4).                            00000210
      *--                                                                       
           02 MNSOCENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCENTF      PIC X.                                     00000220
           02 FILLER    PIC X(2).                                       00000230
           02 MNSOCENTI      PIC X(3).                                  00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000260
           02 FILLER    PIC X(2).                                       00000270
           02 MNDEPOTI  PIC X(3).                                       00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTATIL      COMP PIC S9(4).                            00000290
      *--                                                                       
           02 MNMUTATIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMUTATIF      PIC X.                                     00000300
           02 FILLER    PIC X(2).                                       00000310
           02 MNMUTATII      PIC X(7).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETL      COMP PIC S9(4).                            00000330
      *--                                                                       
           02 MNSOCIETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCIETF      PIC X.                                     00000340
           02 FILLER    PIC X(2).                                       00000350
           02 MNSOCIETI      PIC X(3).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000380
           02 FILLER    PIC X(2).                                       00000390
           02 MNLIEUI   PIC X(3).                                       00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000420
           02 FILLER    PIC X(2).                                       00000430
           02 MLLIEUI   PIC X(20).                                      00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000460
           02 FILLER    PIC X(2).                                       00000470
           02 MNCODICI  PIC X(7).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000500
           02 FILLER    PIC X(2).                                       00000510
           02 MCFAMI    PIC X(5).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000540
           02 FILLER    PIC X(2).                                       00000550
           02 MCMARQI   PIC X(5).                                       00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOUL      COMP PIC S9(4).                            00000570
      *--                                                                       
           02 MLREFFOUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLREFFOUF      PIC X.                                     00000580
           02 FILLER    PIC X(2).                                       00000590
           02 MLREFFOUI      PIC X(20).                                 00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEDEML  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MQTEDEML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTEDEMF  PIC X.                                          00000620
           02 FILLER    PIC X(2).                                       00000630
           02 MQTEDEMI  PIC X(5).                                       00000640
           02 FILLER  OCCURS   12 TIMES .                               00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTL     COMP PIC S9(4).                            00000660
      *--                                                                       
             03 MSELECTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELECTF     PIC X.                                     00000670
             03 FILLER  PIC X(2).                                       00000680
             03 MSELECTI     PIC X.                                     00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUENTL   COMP PIC S9(4).                            00000700
      *--                                                                       
             03 MNLIEUENTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNLIEUENTF   PIC X.                                     00000710
             03 FILLER  PIC X(2).                                       00000720
             03 MNLIEUENTI   PIC X(3).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNHSL   COMP PIC S9(4).                                 00000740
      *--                                                                       
             03 MNHSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNHSF   PIC X.                                          00000750
             03 FILLER  PIC X(2).                                       00000760
             03 MNHSI   PIC X(7).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQHSL   COMP PIC S9(4).                                 00000780
      *--                                                                       
             03 MQHSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQHSF   PIC X.                                          00000790
             03 FILLER  PIC X(2).                                       00000800
             03 MQHSI   PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MLIBERRI  PIC X(78).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MCODTRAI  PIC X(4).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MZONCMDI  PIC X(15).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MCICSI    PIC X(5).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MNETNAMI  PIC X(8).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MSCREENI  PIC X(4).                                       00001050
      ***************************************************************** 00001060
      * SDF: EGS59   EGS59                                              00001070
      ***************************************************************** 00001080
       01   EGS59O REDEFINES EGS59I.                                    00001090
           02 FILLER    PIC X(12).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MDATJOUA  PIC X.                                          00001120
           02 MDATJOUC  PIC X.                                          00001130
           02 MDATJOUH  PIC X.                                          00001140
           02 MDATJOUO  PIC X(10).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MTIMJOUA  PIC X.                                          00001170
           02 MTIMJOUC  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUO  PIC X(5).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MPAGEA    PIC X.                                          00001220
           02 MPAGEC    PIC X.                                          00001230
           02 MPAGEH    PIC X.                                          00001240
           02 MPAGEO .                                                  00001250
             03 MNPAGEO      PIC ZZ9.                                   00001260
             03 MSLASHO      PIC X.                                     00001270
             03 MNBPAGEO     PIC ZZ9.                                   00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNSOCENTA      PIC X.                                     00001300
           02 MNSOCENTC PIC X.                                          00001310
           02 MNSOCENTH PIC X.                                          00001320
           02 MNSOCENTO      PIC X(3).                                  00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNDEPOTA  PIC X.                                          00001350
           02 MNDEPOTC  PIC X.                                          00001360
           02 MNDEPOTH  PIC X.                                          00001370
           02 MNDEPOTO  PIC X(3).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MNMUTATIA      PIC X.                                     00001400
           02 MNMUTATIC PIC X.                                          00001410
           02 MNMUTATIH PIC X.                                          00001420
           02 MNMUTATIO      PIC X(7).                                  00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNSOCIETA      PIC X.                                     00001450
           02 MNSOCIETC PIC X.                                          00001460
           02 MNSOCIETH PIC X.                                          00001470
           02 MNSOCIETO      PIC X(3).                                  00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNLIEUA   PIC X.                                          00001500
           02 MNLIEUC   PIC X.                                          00001510
           02 MNLIEUH   PIC X.                                          00001520
           02 MNLIEUO   PIC X(3).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MLLIEUA   PIC X.                                          00001550
           02 MLLIEUC   PIC X.                                          00001560
           02 MLLIEUH   PIC X.                                          00001570
           02 MLLIEUO   PIC X(20).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNCODICA  PIC X.                                          00001600
           02 MNCODICC  PIC X.                                          00001610
           02 MNCODICH  PIC X.                                          00001620
           02 MNCODICO  PIC X(7).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MCFAMA    PIC X.                                          00001650
           02 MCFAMC    PIC X.                                          00001660
           02 MCFAMH    PIC X.                                          00001670
           02 MCFAMO    PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCMARQA   PIC X.                                          00001700
           02 MCMARQC   PIC X.                                          00001710
           02 MCMARQH   PIC X.                                          00001720
           02 MCMARQO   PIC X(5).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MLREFFOUA      PIC X.                                     00001750
           02 MLREFFOUC PIC X.                                          00001760
           02 MLREFFOUH PIC X.                                          00001770
           02 MLREFFOUO      PIC X(20).                                 00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MQTEDEMA  PIC X.                                          00001800
           02 MQTEDEMC  PIC X.                                          00001810
           02 MQTEDEMH  PIC X.                                          00001820
           02 MQTEDEMO  PIC ----9.                                      00001830
           02 FILLER  OCCURS   12 TIMES .                               00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MSELECTA     PIC X.                                     00001860
             03 MSELECTC     PIC X.                                     00001870
             03 MSELECTH     PIC X.                                     00001880
             03 MSELECTO     PIC X.                                     00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MNLIEUENTA   PIC X.                                     00001910
             03 MNLIEUENTC   PIC X.                                     00001920
             03 MNLIEUENTH   PIC X.                                     00001930
             03 MNLIEUENTO   PIC X(3).                                  00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MNHSA   PIC X.                                          00001960
             03 MNHSC   PIC X.                                          00001970
             03 MNHSH   PIC X.                                          00001980
             03 MNHSO   PIC X(7).                                       00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MQHSA   PIC X.                                          00002010
             03 MQHSC   PIC X.                                          00002020
             03 MQHSH   PIC X.                                          00002030
             03 MQHSO   PIC ----9.                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MLIBERRA  PIC X.                                          00002060
           02 MLIBERRC  PIC X.                                          00002070
           02 MLIBERRH  PIC X.                                          00002080
           02 MLIBERRO  PIC X(78).                                      00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MCODTRAA  PIC X.                                          00002110
           02 MCODTRAC  PIC X.                                          00002120
           02 MCODTRAH  PIC X.                                          00002130
           02 MCODTRAO  PIC X(4).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MZONCMDA  PIC X.                                          00002160
           02 MZONCMDC  PIC X.                                          00002170
           02 MZONCMDH  PIC X.                                          00002180
           02 MZONCMDO  PIC X(15).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCICSA    PIC X.                                          00002210
           02 MCICSC    PIC X.                                          00002220
           02 MCICSH    PIC X.                                          00002230
           02 MCICSO    PIC X(5).                                       00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MNETNAMA  PIC X.                                          00002260
           02 MNETNAMC  PIC X.                                          00002270
           02 MNETNAMH  PIC X.                                          00002280
           02 MNETNAMO  PIC X(8).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MSCREENA  PIC X.                                          00002310
           02 MSCREENC  PIC X.                                          00002320
           02 MSCREENH  PIC X.                                          00002330
           02 MSCREENO  PIC X(4).                                       00002340
                                                                                
