      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *01  COMM-GL00-LONG-COMMAREA       PIC S9(4) COMP VALUE +8192.            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-GL00-LONG-COMMAREA       PIC S9(4) COMP VALUE +9304.            
      *--                                                                       
       01  COMM-GL00-LONG-COMMAREA       PIC S9(4) COMP-5 VALUE +9304.          
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA            PIC X(100).                             
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02    COMM-CICS-APPLID        PIC X(8).                               
          02    COMM-CICS-NETNAM        PIC X(8).                               
          02    COMM-CICS-TRANSA        PIC X(4).                               
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02    COMM-DATE-SIECLE        PIC XX.                                 
          02    COMM-DATE-ANNEE         PIC XX.                                 
          02    COMM-DATE-MOIS          PIC XX.                                 
          02    COMM-DATE-JOUR          PIC XX.                                 
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02    COMM-DATE-QNTA          PIC 999.                                
          02    COMM-DATE-QNT0          PIC 99999.                              
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02    COMM-DATE-BISX            PIC 9.                                
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02    COMM-DATE-JSM             PIC 9.                                
      *   LIBELLES DU JOUR COURT - LONG                                         
          02    COMM-DATE-JSM-LC          PIC XXX.                              
          02    COMM-DATE-JSM-LL          PIC XXXXXXXX.                         
      *   LIBELLES DU MOIS COURT - LONG                                         
          02    COMM-DATE-MOIS-LC         PIC XXX.                              
          02    COMM-DATE-MOIS-LL         PIC XXXXXXXX.                         
      *   DIFFERENTES FORMES DE DATE                                            
          02    COMM-DATE-SSAAMMJJ        PIC X(8).                             
          02    COMM-DATE-AAMMJJ          PIC X(6).                             
          02    COMM-DATE-JJMMSSAA        PIC X(8).                             
          02    COMM-DATE-JJMMAA          PIC X(6).                             
          02    COMM-DATE-JJ-MM-AA        PIC X(8).                             
          02    COMM-DATE-JJ-MM-SSAA      PIC X(10).                            
      *   DIFFERENTES FORMES DE DATE                                            
          02    COMM-DATE-SEMSS           PIC X(02).                            
          02    COMM-DATE-SEMAA           PIC X(02).                            
          02    COMM-DATE-SEMNU           PIC X(02).                            
          02    COMM-DATE-FILLER          PIC X(08).                            
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02    COMM-SWAP-CURS          PIC S9(4) COMP VALUE -1.                
      *--                                                                       
          02    COMM-SWAP-CURS          PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
          02    COMM-SWAP-ATTR OCCURS 150       PIC X(1).                       
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                                         
          02    COMM-GL00-NSOC          PIC X(3).                               
          02    COMM-GL00-NDEPOT        PIC X(3).                               
          02    COMM-GL00-TAB-DEPOT.                                            
      *         03 COMM-GL00-TAB OCCURS 15.                                     
                03 COMM-GL00-TAB OCCURS 200.                                    
                   05 COMM-GL00-AUTOR-NSOCDEPOT PIC X(3).                       
                   05 COMM-GL00-AUTOR-NDEPOT    PIC X(3).                       
          02    COMM-GL00-NB-NDEPOT     PIC S9(3) COMP-3.                       
          02    COMM-GL00-CACID         PIC X(4).                               
      *         -- TYPE DE DETAIL N=NCDE ET O=CODIC ------------------*         
          02    COMM-GL00-DETCODIC      PIC X.                                  
      *         -- COMMANDE MUTLI-ENTREPOT                                      
          02    COMM-GL00-NSURCDE       PIC X(7).                               
          02    COMM-GL00-LIBRE         PIC X(5).                               
          02    COMM-GL00-NSOCCICS      PIC X(3).                               
          02    COMM-GL00-CODLANG       PIC X(2).                               
          02    COMM-GL00-CODPIC        PIC X(2).                               
      *   02    COMM-GL00-FILLER        PIC X(150).                             
          02    COMM-GL00-MESSAGE       PIC X(78).                              
          02    COMM-GL00-PGR-PRC       PIC X(05).                              
      *   02    COMM-GL00-FILLER        PIC X(67).                              
          02    COMM-GL00-FILLER        PIC X(69).                              
          02    COMM-GL00-APPLI.                                                
      *         -- ZONE COMMUNE                                                 
             03 COMM-GL00-APPLI-FILLER  PIC X(3423).                            
      *      03 FILLER                  PIC X(3809).                            
             03 FILLER                  PIC X(3789).                            
                COPY WORKGL06.                                                  
                COPY WORKGF55.                                                  
      *         -- FILLER A LAISSER EN FIN ET NE PAS MODIFIER                   
             03 FILLER                  PIC X(16).                              
      *****************************************************************         
                                                                                
