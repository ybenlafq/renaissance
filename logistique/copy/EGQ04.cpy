      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGQ04   EGQ04                                              00000020
      ***************************************************************** 00000030
       01   EGQ04I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPTL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MDEPTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEPTF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDEPTI    PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMMUNE1L     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCOMMUNE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCOMMUNE1F     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCOMMUNE1I     PIC X(32).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCODEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCODEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCODEI    PIC X.                                          00000290
           02 MCGRPD OCCURS   3 TIMES .                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGRPL  COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MCGRPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCGRPF  PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCGRPI  PIC X(5).                                       00000340
           02 MTABLEI OCCURS   14 TIMES .                               00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINSEEL      COMP PIC S9(4).                            00000360
      *--                                                                       
             03 MINSEEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MINSEEF      PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 MINSEEI      PIC X(5).                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPARTBL      COMP PIC S9(4).                            00000400
      *--                                                                       
             03 MPARTBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPARTBF      PIC X.                                     00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MPARTBI      PIC X.                                     00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMUNE2L   COMP PIC S9(4).                            00000440
      *--                                                                       
             03 MCOMMUNE2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCOMMUNE2F   PIC X.                                     00000450
             03 FILLER  PIC X(4).                                       00000460
             03 MCOMMUNE2I   PIC X(32).                                 00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDPOSTL     COMP PIC S9(4).                            00000480
      *--                                                                       
             03 MCDPOSTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCDPOSTF     PIC X.                                     00000490
             03 FILLER  PIC X(4).                                       00000500
             03 MCDPOSTI     PIC X(5).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRESPRINL    COMP PIC S9(4).                            00000520
      *--                                                                       
             03 MRESPRINL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MRESPRINF    PIC X.                                     00000530
             03 FILLER  PIC X(4).                                       00000540
             03 MRESPRINI    PIC X(9).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000560
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000570
             03 FILLER  PIC X(4).                                       00000580
             03 MNSOCI  PIC X(3).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPERIML      COMP PIC S9(4).                            00000600
      *--                                                                       
             03 MPERIML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPERIMF      PIC X.                                     00000610
             03 FILLER  PIC X(4).                                       00000620
             03 MPERIMI      PIC X.                                     00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONELEL     COMP PIC S9(4).                            00000640
      *--                                                                       
             03 MZONELEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MZONELEF     PIC X.                                     00000650
             03 FILLER  PIC X(4).                                       00000660
             03 MZONELEI     PIC X(5).                                  00000670
             03 MWGRPD OCCURS   3 TIMES .                               00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MWGRPL     COMP PIC S9(4).                            00000690
      *--                                                                       
               04 MWGRPL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MWGRPF     PIC X.                                     00000700
               04 FILLER     PIC X(4).                                  00000710
               04 MWGRPI     PIC X.                                     00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MZONCMDI  PIC X(15).                                      00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MLIBERRI  PIC X(58).                                      00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MCODTRAI  PIC X(4).                                       00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MCICSI    PIC X(5).                                       00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MNETNAMI  PIC X(8).                                       00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MSCREENI  PIC X(4).                                       00000960
      ***************************************************************** 00000970
      * SDF: EGQ04   EGQ04                                              00000980
      ***************************************************************** 00000990
       01   EGQ04O REDEFINES EGQ04I.                                    00001000
           02 FILLER    PIC X(12).                                      00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MDATJOUA  PIC X.                                          00001030
           02 MDATJOUC  PIC X.                                          00001040
           02 MDATJOUP  PIC X.                                          00001050
           02 MDATJOUH  PIC X.                                          00001060
           02 MDATJOUV  PIC X.                                          00001070
           02 MDATJOUO  PIC X(10).                                      00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MTIMJOUA  PIC X.                                          00001100
           02 MTIMJOUC  PIC X.                                          00001110
           02 MTIMJOUP  PIC X.                                          00001120
           02 MTIMJOUH  PIC X.                                          00001130
           02 MTIMJOUV  PIC X.                                          00001140
           02 MTIMJOUO  PIC X(5).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MWPAGEA   PIC X.                                          00001170
           02 MWPAGEC   PIC X.                                          00001180
           02 MWPAGEP   PIC X.                                          00001190
           02 MWPAGEH   PIC X.                                          00001200
           02 MWPAGEV   PIC X.                                          00001210
           02 MWPAGEO   PIC ZZ9.                                        00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MDEPTA    PIC X.                                          00001240
           02 MDEPTC    PIC X.                                          00001250
           02 MDEPTP    PIC X.                                          00001260
           02 MDEPTH    PIC X.                                          00001270
           02 MDEPTV    PIC X.                                          00001280
           02 MDEPTO    PIC X(2).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MCOMMUNE1A     PIC X.                                     00001310
           02 MCOMMUNE1C     PIC X.                                     00001320
           02 MCOMMUNE1P     PIC X.                                     00001330
           02 MCOMMUNE1H     PIC X.                                     00001340
           02 MCOMMUNE1V     PIC X.                                     00001350
           02 MCOMMUNE1O     PIC X(32).                                 00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MCODEA    PIC X.                                          00001380
           02 MCODEC    PIC X.                                          00001390
           02 MCODEP    PIC X.                                          00001400
           02 MCODEH    PIC X.                                          00001410
           02 MCODEV    PIC X.                                          00001420
           02 MCODEO    PIC X.                                          00001430
           02 DFHMS1 OCCURS   3 TIMES .                                 00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MCGRPA  PIC X.                                          00001460
             03 MCGRPC  PIC X.                                          00001470
             03 MCGRPP  PIC X.                                          00001480
             03 MCGRPH  PIC X.                                          00001490
             03 MCGRPV  PIC X.                                          00001500
             03 MCGRPO  PIC X(5).                                       00001510
           02 MTABLEO OCCURS   14 TIMES .                               00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MINSEEA      PIC X.                                     00001540
             03 MINSEEC PIC X.                                          00001550
             03 MINSEEP PIC X.                                          00001560
             03 MINSEEH PIC X.                                          00001570
             03 MINSEEV PIC X.                                          00001580
             03 MINSEEO      PIC X(5).                                  00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MPARTBA      PIC X.                                     00001610
             03 MPARTBC PIC X.                                          00001620
             03 MPARTBP PIC X.                                          00001630
             03 MPARTBH PIC X.                                          00001640
             03 MPARTBV PIC X.                                          00001650
             03 MPARTBO      PIC X.                                     00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MCOMMUNE2A   PIC X.                                     00001680
             03 MCOMMUNE2C   PIC X.                                     00001690
             03 MCOMMUNE2P   PIC X.                                     00001700
             03 MCOMMUNE2H   PIC X.                                     00001710
             03 MCOMMUNE2V   PIC X.                                     00001720
             03 MCOMMUNE2O   PIC X(32).                                 00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MCDPOSTA     PIC X.                                     00001750
             03 MCDPOSTC     PIC X.                                     00001760
             03 MCDPOSTP     PIC X.                                     00001770
             03 MCDPOSTH     PIC X.                                     00001780
             03 MCDPOSTV     PIC X.                                     00001790
             03 MCDPOSTO     PIC X(5).                                  00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MRESPRINA    PIC X.                                     00001820
             03 MRESPRINC    PIC X.                                     00001830
             03 MRESPRINP    PIC X.                                     00001840
             03 MRESPRINH    PIC X.                                     00001850
             03 MRESPRINV    PIC X.                                     00001860
             03 MRESPRINO    PIC ZZZZZZZZ9.                             00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MNSOCA  PIC X.                                          00001890
             03 MNSOCC  PIC X.                                          00001900
             03 MNSOCP  PIC X.                                          00001910
             03 MNSOCH  PIC X.                                          00001920
             03 MNSOCV  PIC X.                                          00001930
             03 MNSOCO  PIC X(3).                                       00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MPERIMA      PIC X.                                     00001960
             03 MPERIMC PIC X.                                          00001970
             03 MPERIMP PIC X.                                          00001980
             03 MPERIMH PIC X.                                          00001990
             03 MPERIMV PIC X.                                          00002000
             03 MPERIMO      PIC X.                                     00002010
             03 FILLER       PIC X(2).                                  00002020
             03 MZONELEA     PIC X.                                     00002030
             03 MZONELEC     PIC X.                                     00002040
             03 MZONELEP     PIC X.                                     00002050
             03 MZONELEH     PIC X.                                     00002060
             03 MZONELEV     PIC X.                                     00002070
             03 MZONELEO     PIC X(5).                                  00002080
             03 DFHMS2 OCCURS   3 TIMES .                               00002090
               04 FILLER     PIC X(2).                                  00002100
               04 MWGRPA     PIC X.                                     00002110
               04 MWGRPC     PIC X.                                     00002120
               04 MWGRPP     PIC X.                                     00002130
               04 MWGRPH     PIC X.                                     00002140
               04 MWGRPV     PIC X.                                     00002150
               04 MWGRPO     PIC X.                                     00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MZONCMDA  PIC X.                                          00002180
           02 MZONCMDC  PIC X.                                          00002190
           02 MZONCMDP  PIC X.                                          00002200
           02 MZONCMDH  PIC X.                                          00002210
           02 MZONCMDV  PIC X.                                          00002220
           02 MZONCMDO  PIC X(15).                                      00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MLIBERRA  PIC X.                                          00002250
           02 MLIBERRC  PIC X.                                          00002260
           02 MLIBERRP  PIC X.                                          00002270
           02 MLIBERRH  PIC X.                                          00002280
           02 MLIBERRV  PIC X.                                          00002290
           02 MLIBERRO  PIC X(58).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MCODTRAA  PIC X.                                          00002320
           02 MCODTRAC  PIC X.                                          00002330
           02 MCODTRAP  PIC X.                                          00002340
           02 MCODTRAH  PIC X.                                          00002350
           02 MCODTRAV  PIC X.                                          00002360
           02 MCODTRAO  PIC X(4).                                       00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCICSA    PIC X.                                          00002390
           02 MCICSC    PIC X.                                          00002400
           02 MCICSP    PIC X.                                          00002410
           02 MCICSH    PIC X.                                          00002420
           02 MCICSV    PIC X.                                          00002430
           02 MCICSO    PIC X(5).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MNETNAMA  PIC X.                                          00002460
           02 MNETNAMC  PIC X.                                          00002470
           02 MNETNAMP  PIC X.                                          00002480
           02 MNETNAMH  PIC X.                                          00002490
           02 MNETNAMV  PIC X.                                          00002500
           02 MNETNAMO  PIC X(8).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MSCREENA  PIC X.                                          00002530
           02 MSCREENC  PIC X.                                          00002540
           02 MSCREENP  PIC X.                                          00002550
           02 MSCREENH  PIC X.                                          00002560
           02 MSCREENV  PIC X.                                          00002570
           02 MSCREENO  PIC X(4).                                       00002580
                                                                                
