      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM51   ERM51                                              00000020
      ***************************************************************** 00000030
       01   ERM51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCGROUPI  PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTSIMUL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWTSIMUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWTSIMUF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWTSIMUI  PIC X.                                          00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDEFFETI  PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINEFFL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MDFINEFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDFINEFFF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDFINEFFI      PIC X(10).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDEPL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSOCDEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCDEPF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSOCDEPI  PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDEPOTI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCLERPAL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MWCLERPAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWCLERPAF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MWCLERPAI      PIC X.                                     00000410
           02 LIGNEI OCCURS   12 TIMES .                                00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNCODICI     PIC X(7).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCFAMI  PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLREFI  PIC X(20).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCLERL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MWCLERL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWCLERF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MWCLERI      PIC X.                                     00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICML    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNCODICML COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNCODICMF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNCODICMI    PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSPL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MQSPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQSPF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQSPI   PIC X(2).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE1L  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MQTE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE1F  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQTE1I  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSA1L  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MQSA1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSA1F  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQSA1I  PIC X(3).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE2L  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MQTE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE2F  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQTE2I  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSA2L  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MQSA2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSA2F  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQSA2I  PIC X(3).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE3L  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MQTE3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE3F  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQTE3I  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSA3L  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MQSA3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSA3F  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQSA3I  PIC X(3).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRIL     COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MTRIL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTRIF     PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MTRII     PIC X.                                          00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MZONCMDI  PIC X(12).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(61).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: ERM51   ERM51                                              00001200
      ***************************************************************** 00001210
       01   ERM51O REDEFINES ERM51I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MCGROUPA  PIC X.                                          00001390
           02 MCGROUPC  PIC X.                                          00001400
           02 MCGROUPP  PIC X.                                          00001410
           02 MCGROUPH  PIC X.                                          00001420
           02 MCGROUPV  PIC X.                                          00001430
           02 MCGROUPO  PIC X(5).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MWTSIMUA  PIC X.                                          00001460
           02 MWTSIMUC  PIC X.                                          00001470
           02 MWTSIMUP  PIC X.                                          00001480
           02 MWTSIMUH  PIC X.                                          00001490
           02 MWTSIMUV  PIC X.                                          00001500
           02 MWTSIMUO  PIC X.                                          00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MDEFFETA  PIC X.                                          00001530
           02 MDEFFETC  PIC X.                                          00001540
           02 MDEFFETP  PIC X.                                          00001550
           02 MDEFFETH  PIC X.                                          00001560
           02 MDEFFETV  PIC X.                                          00001570
           02 MDEFFETO  PIC X(10).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MDFINEFFA      PIC X.                                     00001600
           02 MDFINEFFC PIC X.                                          00001610
           02 MDFINEFFP PIC X.                                          00001620
           02 MDFINEFFH PIC X.                                          00001630
           02 MDFINEFFV PIC X.                                          00001640
           02 MDFINEFFO      PIC X(10).                                 00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MSOCDEPA  PIC X.                                          00001670
           02 MSOCDEPC  PIC X.                                          00001680
           02 MSOCDEPP  PIC X.                                          00001690
           02 MSOCDEPH  PIC X.                                          00001700
           02 MSOCDEPV  PIC X.                                          00001710
           02 MSOCDEPO  PIC X(3).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MDEPOTA   PIC X.                                          00001740
           02 MDEPOTC   PIC X.                                          00001750
           02 MDEPOTP   PIC X.                                          00001760
           02 MDEPOTH   PIC X.                                          00001770
           02 MDEPOTV   PIC X.                                          00001780
           02 MDEPOTO   PIC X(3).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MWCLERPAA      PIC X.                                     00001810
           02 MWCLERPAC PIC X.                                          00001820
           02 MWCLERPAP PIC X.                                          00001830
           02 MWCLERPAH PIC X.                                          00001840
           02 MWCLERPAV PIC X.                                          00001850
           02 MWCLERPAO      PIC X.                                     00001860
           02 LIGNEO OCCURS   12 TIMES .                                00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MNCODICA     PIC X.                                     00001890
             03 MNCODICC     PIC X.                                     00001900
             03 MNCODICP     PIC X.                                     00001910
             03 MNCODICH     PIC X.                                     00001920
             03 MNCODICV     PIC X.                                     00001930
             03 MNCODICO     PIC X(7).                                  00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MCFAMA  PIC X.                                          00001960
             03 MCFAMC  PIC X.                                          00001970
             03 MCFAMP  PIC X.                                          00001980
             03 MCFAMH  PIC X.                                          00001990
             03 MCFAMV  PIC X.                                          00002000
             03 MCFAMO  PIC X(5).                                       00002010
             03 FILLER       PIC X(2).                                  00002020
             03 MLREFA  PIC X.                                          00002030
             03 MLREFC  PIC X.                                          00002040
             03 MLREFP  PIC X.                                          00002050
             03 MLREFH  PIC X.                                          00002060
             03 MLREFV  PIC X.                                          00002070
             03 MLREFO  PIC X(20).                                      00002080
             03 FILLER       PIC X(2).                                  00002090
             03 MWCLERA      PIC X.                                     00002100
             03 MWCLERC PIC X.                                          00002110
             03 MWCLERP PIC X.                                          00002120
             03 MWCLERH PIC X.                                          00002130
             03 MWCLERV PIC X.                                          00002140
             03 MWCLERO      PIC X.                                     00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MNCODICMA    PIC X.                                     00002170
             03 MNCODICMC    PIC X.                                     00002180
             03 MNCODICMP    PIC X.                                     00002190
             03 MNCODICMH    PIC X.                                     00002200
             03 MNCODICMV    PIC X.                                     00002210
             03 MNCODICMO    PIC X(7).                                  00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MQSPA   PIC X.                                          00002240
             03 MQSPC   PIC X.                                          00002250
             03 MQSPP   PIC X.                                          00002260
             03 MQSPH   PIC X.                                          00002270
             03 MQSPV   PIC X.                                          00002280
             03 MQSPO   PIC X(2).                                       00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MQTE1A  PIC X.                                          00002310
             03 MQTE1C  PIC X.                                          00002320
             03 MQTE1P  PIC X.                                          00002330
             03 MQTE1H  PIC X.                                          00002340
             03 MQTE1V  PIC X.                                          00002350
             03 MQTE1O  PIC X(4).                                       00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MQSA1A  PIC X.                                          00002380
             03 MQSA1C  PIC X.                                          00002390
             03 MQSA1P  PIC X.                                          00002400
             03 MQSA1H  PIC X.                                          00002410
             03 MQSA1V  PIC X.                                          00002420
             03 MQSA1O  PIC X(3).                                       00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MQTE2A  PIC X.                                          00002450
             03 MQTE2C  PIC X.                                          00002460
             03 MQTE2P  PIC X.                                          00002470
             03 MQTE2H  PIC X.                                          00002480
             03 MQTE2V  PIC X.                                          00002490
             03 MQTE2O  PIC X(4).                                       00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MQSA2A  PIC X.                                          00002520
             03 MQSA2C  PIC X.                                          00002530
             03 MQSA2P  PIC X.                                          00002540
             03 MQSA2H  PIC X.                                          00002550
             03 MQSA2V  PIC X.                                          00002560
             03 MQSA2O  PIC X(3).                                       00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MQTE3A  PIC X.                                          00002590
             03 MQTE3C  PIC X.                                          00002600
             03 MQTE3P  PIC X.                                          00002610
             03 MQTE3H  PIC X.                                          00002620
             03 MQTE3V  PIC X.                                          00002630
             03 MQTE3O  PIC X(4).                                       00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MQSA3A  PIC X.                                          00002660
             03 MQSA3C  PIC X.                                          00002670
             03 MQSA3P  PIC X.                                          00002680
             03 MQSA3H  PIC X.                                          00002690
             03 MQSA3V  PIC X.                                          00002700
             03 MQSA3O  PIC X(3).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MTRIA     PIC X.                                          00002730
           02 MTRIC     PIC X.                                          00002740
           02 MTRIP     PIC X.                                          00002750
           02 MTRIH     PIC X.                                          00002760
           02 MTRIV     PIC X.                                          00002770
           02 MTRIO     PIC X.                                          00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MZONCMDA  PIC X.                                          00002800
           02 MZONCMDC  PIC X.                                          00002810
           02 MZONCMDP  PIC X.                                          00002820
           02 MZONCMDH  PIC X.                                          00002830
           02 MZONCMDV  PIC X.                                          00002840
           02 MZONCMDO  PIC X(12).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(61).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
