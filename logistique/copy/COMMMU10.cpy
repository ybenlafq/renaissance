      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************           
      *    COMMAREA SPECIFIQUE PRG TMU10 (MENU)          TR: MU10  *            
      *                           TMU11 TMU12                      *            
      *                           TMU13 TMU14                      *            
      *                           TMU34 TMU37 TMU38                *            
      *                                                            *            
      *                                                            *            
      *                                                            *            
      *                                                            *            
      *           POUR L'ADMINISTATION DES DONNEES                 *            
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-------------------------------------------------------------            
      *                                                                         
       01 COM-MU10-LONGUEUR-COMAREA.                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  COM-MU10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                
      *--                                                                       
          02  COM-MU10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  COM-GS50-LONG-COMMAREA REDEFINES COM-MU10-LONG-COMMAREA           
      *                              PIC S9(4) COMP.                            
      *--                                                                       
          02  COM-GS50-LONG-COMMAREA REDEFINES COM-MU10-LONG-COMMAREA           
                                     PIC S9(4) COMP-5.                          
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA      PIC X(100).                                   
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02 COMM-CICS-APPLID     PIC X(8).                                     
          02 COMM-CICS-NETNAM     PIC X(8).                                     
          02 COMM-CICS-TRANSA     PIC X(4).                                     
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE     PIC XX.                                       
          02 COMM-DATE-ANNEE      PIC XX.                                       
          02 COMM-DATE-MOIS       PIC XX.                                       
          02 COMM-DATE-JOUR       PIC XX.                                       
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC 999.                                      
          02 COMM-DATE-QNT0       PIC 99999.                                    
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC 9.                                        
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC 9.                                        
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC XXX.                                      
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                 
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC XXX.                                      
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                 
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                                     
          02 COMM-DATE-AAMMJJ     PIC X(6).                                     
          02 COMM-DATE-JJMMSSAA   PIC X(8).                                     
          02 COMM-DATE-JJMMAA     PIC X(6).                                     
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                                     
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                    
      * DIFFERENTES FORMES DE DATE                                              
          02 COMM-DATE-SEMSS      PIC 9(02).                                    
          02 COMM-DATE-SEMAA      PIC 9(02).                                    
          02 COMM-DATE-SEMNU      PIC 9(02).                                    
          02 COMM-DATE-FILLER     PIC X(08).                                    
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                                
      *                                                                         
      * ZONES DE COMMAREA CHARGEES PAR LE PROG TMU10 ------------- 205          
      *                                                                         
          02 COMM-MU10-APPLI.                                                   
      *                                                                         
              03 COMM-MU10-NMUTATION         PIC X(7).                          
              03 COMM-MU10-NSOCIETE          PIC X(3).                          
              03 COMM-MU10-NLIEU             PIC X(3).                          
              03 COMM-MU10-LLIEU             PIC X(20).                         
              03 COMM-MU10-NSOCENTR          PIC X(3).                          
              03 COMM-MU10-NDEPOT            PIC X(3).                          
              03 COMM-MU10-DMUTATION         PIC X(8).                          
              03 COMM-MU10-CSELART           PIC X(5).                          
              03 COMM-MU10-QNBPIECES         PIC S9(5) COMP-3.                  
              03 COMM-MU10-QVOLUME           PIC S9(11) COMP-3.                 
              03 COMM-MU10-QNBPQUOTA         PIC S9(5) COMP-3.                  
              03 COMM-MU10-QNBM3QUOTA        PIC S9(5)V9 COMP-3.                
              03 COMM-MU10-QNBLIGNES         PIC S9(5) COMP-3.                  
              03 COMM-MU10-CTYPSOC           PIC X(3).                          
              03 COMM-MU10-WVAL              PIC X(1).                          
              03 COMM-MU10-ORIGINE-MUT       PIC X(1).                          
LB            03 COMM-MU10-OPTION5           PIC X(1).                          
                 88 COMM-MU10-PAS-OPT5       VALUE ' '.                         
                 88 COMM-MU10-OPT5           VALUE '1'.                         
              03 COMM-MU10-OPTION6           PIC X(1).                          
                 88 COMM-MU10-PAS-OPT6       VALUE ' '.                         
                 88 COMM-MU10-OPT6           VALUE '1'.                         
              03 COMM-MU10-OPTION7           PIC X(1).                          
                 88 COMM-MU10-PAS-OPT7       VALUE ' '.                         
                 88 COMM-MU10-OPT7           VALUE '1'.                         
              03 COMM-MU10-EDITION-MUT       PIC X(1).                          
                 88 COMM-MU10-PAS-MUT        VALUE ' '.                         
                 88 COMM-MU10-EDIT-MUT       VALUE '1'.                         
              03 COMM-MU10-EDITION-DEM       PIC X(1).                          
                 88 COMM-MU10-PAS-DEM        VALUE ' '.                         
                 88 COMM-MU10-EDIT-DEM       VALUE '1'.                         
              03 COMM-MU10-EDITION-ELA       PIC X(1).                          
                 88 COMM-MU10-PAS-ELA        VALUE ' '.                         
                 88 COMM-MU10-EDIT-ELA       VALUE '1'.                         
              03 COMM-MU10-DDESTOCK          PIC X(8).                          
              03 COMM-MU10-MESS              PIC X(80).                         
              03 COMM-MU10-DJOUR             PIC X(8).                          
              03 COMM-MU10-CODEIMPRI         PIC X(4).                          
              03 COMM-MU10-PROG              PIC X(05).                         
              03 COMM-MU10-ITEM              PIC S9(5) COMP-3.                  
              03 COMM-MU10-CODRET            PIC S9(4).                         
              03 COMM-MU10-MAX-PAGE          PIC S9(5) COMP-3.                  
              03 COMM-MU10-OPTIONS           PIC X.                             
              03 COMM-MU10-LECT-TS           PIC 9.                             
                 88  PAS-LIRE-TS-MU37           VALUE 0.                        
                 88  LIRE-TS-MU37               VALUE 1.                        
              03 COMM-MU10-SOCMUT            PIC X(3).                          
              03 COMM-MU10-LIEMUT            PIC X(3).                          
      *                                                                         
          02 COMM-GS50-APPLI REDEFINES COMM-MU10-APPLI.                         
      *                                                           0205          
              03 COMM-GS50-NMUTATION         PIC X(7).                          
              03 COMM-GS50-NSOCLIEU          PIC X(3).                          
              03 COMM-GS50-NLIEU             PIC X(3).                          
              03 FILLER                      PIC X(20).                         
              03 COMM-GS50-NSOCENTRE         PIC X(3).                          
              03 COMM-GS50-NDEPOT            PIC X(3).                          
              03 COMM-GS50-DMUTATION         PIC X(8).                          
              03 COMM-GS50-SELART            PIC X(5).                          
              03 FILLER                      PIC X(22).                         
              03 COMM-GS50-WVAL              PIC X(1).                          
              03 COMM-GS50-ORIGINE-MUT       PIC X(1).                          
LB            03 COMM-GS50-OPTION5           PIC X(1).                          
                 88 COMM-GS50-PAS-OPT5       VALUE ' '.                         
                 88 COMM-GS50-OPT5           VALUE '1'.                         
              03 COMM-GS50-OPTION6           PIC X(1).                          
                 88 COMM-GS50-PAS-OPT6       VALUE ' '.                         
                 88 COMM-GS50-OPT6           VALUE '1'.                         
              03 COMM-GS50-OPTION7           PIC X(1).                          
                 88 COMM-GS50-PAS-OPT7       VALUE ' '.                         
                 88 COMM-GS50-OPT7           VALUE '1'.                         
              03 COMM-GS50-EDITION-MUT       PIC X(1).                          
                 88 COMM-GS50-PAS-MUT        VALUE ' '.                         
                 88 COMM-GS50-EDIT-MUT       VALUE '1'.                         
              03 COMM-GS50-EDITION-DEM       PIC X(1).                          
                 88 COMM-GS50-PAS-DEM        VALUE ' '.                         
                 88 COMM-GS50-EDIT-DEM       VALUE '1'.                         
              03 COMM-GS50-EDITION-ELA       PIC X(1).                          
                 88 COMM-GS50-PAS-ELA        VALUE ' '.                         
                 88 COMM-GS50-EDIT-ELA       VALUE '1'.                         
              03 COMM-GS50-DDESTOCK          PIC X(8).                          
              03 COMM-GS50-MESS              PIC X(80).                         
              03 COMM-GS50-DJOUR             PIC X(8).                          
              03 COMM-GS50-CODEIMPRI         PIC X(4).                          
              03 COMM-GS50-PROG              PIC X(05).                         
              03 COMM-GS50-ITEM              PIC S9(5) COMP-3.                  
              03 COMM-GS50-CODRET            PIC S9(4).                         
              03 COMM-GS50-MAX-PAGE          PIC S9(5) COMP-3.                  
              03 COMM-GS50-OPTIONS           PIC X.                             
              03 COMM-GS50-LECT-TS           PIC 9.                             
                 88  PAS-LIRE-TS-GS37           VALUE 0.                        
                 88  LIRE-TS-GS37               VALUE 1.                        
              03 COMM-GS50-SOCMUT            PIC X(3).                          
              03 COMM-GS50-LIEMUT            PIC X(3).                          
      *                                                                         
      *-------                                                                  
      * ZONES RESERVEES APPLICATIVES SOUS MU10 ET GS50  -------   3519          
      *                                                                         
      *****************************************************************         
           02 COMM-MU10-FILLER               PIC X(3519).                       
      *                                                                         
                                                                                
