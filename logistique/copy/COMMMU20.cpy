      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      *                                                                 00050000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-MU20-LONG-COMMAREA PIC S9(4) COMP VALUE +8192.           00240001
      *--                                                                       
       01  COM-MU20-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +8192.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 153  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * REST DISPO ---------------------------------------------- 7819          
      *                                                                         
      * ZONES DE COMMAREA CHARGEES PAR LE PROG TMU10 ------------- 204          
      *                                                                         
          02 COMM-MU10-APPLI.                                                   
      *                                                                         
              03 COMM-MU10-NMUTATION         PIC X(7).                          
              03 COMM-MU10-NSOCIETE          PIC X(3).                          
              03 COMM-MU10-NLIEU             PIC X(3).                          
              03 COMM-MU10-LLIEU             PIC X(20).                         
              03 COMM-MU10-NSOCENTR          PIC X(3).                          
              03 COMM-MU10-NDEPOT            PIC X(3).                          
              03 COMM-MU10-DMUTATION         PIC X(8).                          
              03 COMM-MU10-CSELART           PIC X(5).                          
              03 COMM-MU10-QNBPIECES         PIC S9(5) COMP-3.                  
              03 COMM-MU10-QVOLUME           PIC S9(11) COMP-3.                 
              03 COMM-MU10-QNBPQUOTA         PIC S9(5) COMP-3.                  
              03 COMM-MU10-QNBM3QUOTA        PIC S9(5)V9 COMP-3.                
              03 COMM-MU10-QNBLIGNES         PIC S9(5) COMP-3.                  
              03 COMM-MU10-CTYPSOC           PIC X(3).                          
              03 COMM-MU10-WVAL              PIC X(1).                          
              03 COMM-MU10-ORIGINE-MUT       PIC X(1).                          
LB            03 COMM-MU10-OPTION5           PIC X(1).                          
                 88 COMM-MU10-PAS-OPT5       VALUE ' '.                         
                 88 COMM-MU10-OPT5           VALUE '1'.                         
              03 COMM-MU10-OPTION6           PIC X(1).                          
                 88 COMM-MU10-PAS-OPT6       VALUE ' '.                         
                 88 COMM-MU10-OPT6           VALUE '1'.                         
              03 COMM-MU10-OPTION7           PIC X(1).                          
                 88 COMM-MU10-PAS-OPT7       VALUE ' '.                         
                 88 COMM-MU10-OPT7           VALUE '1'.                         
              03 COMM-MU10-EDITION-MUT       PIC X(1).                          
                 88 COMM-MU10-PAS-MUT        VALUE ' '.                         
                 88 COMM-MU10-EDIT-MUT       VALUE '1'.                         
              03 COMM-MU10-EDITION-DEM       PIC X(1).                          
                 88 COMM-MU10-PAS-DEM        VALUE ' '.                         
                 88 COMM-MU10-EDIT-DEM       VALUE '1'.                         
              03 COMM-MU10-EDITION-ELA       PIC X(1).                          
                 88 COMM-MU10-PAS-ELA        VALUE ' '.                         
                 88 COMM-MU10-EDIT-ELA       VALUE '1'.                         
              03 COMM-MU10-DDESTOCK          PIC X(8).                          
              03 COMM-MU10-MESS              PIC X(80).                         
              03 COMM-MU10-DJOUR             PIC X(8).                          
              03 COMM-MU10-CODEIMPRI         PIC X(4).                          
              03 COMM-MU10-PROG              PIC X(05).                         
              03 COMM-MU10-ITEM              PIC S9(5) COMP-3.                  
              03 COMM-MU10-CODRET            PIC S9(4).                         
              03 COMM-MU10-MAX-PAGE          PIC S9(5) COMP-3.                  
              03 COMM-MU10-OPTIONS           PIC X.                             
              03 COMM-MU10-LECT-TS           PIC 9.                             
                 88  PAS-LIRE-TS-MU37           VALUE 0.                        
                 88  LIRE-TS-MU37               VALUE 1.                        
              03 COMM-MU10-SOCMUT            PIC X(3).                          
              03 COMM-MU10-LIEMUT            PIC X(3).                          
              03 COMM-MU10-FILLER         PIC X(7615).                  00970003
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7819  00730000
           02 COMM-MU20-APPLI REDEFINES COMM-MU10-APPLI.                00740000
              03 COMM-MU20-NMUTATION      PIC X(7).                     00741002
              03 COMM-MU20-NSOCIETE       PIC X(3).                     00742002
              03 COMM-MU20-NLIEU          PIC X(3).                     00743002
              03 COMM-MU20-LLIEU          PIC X(20).                    00744002
              03 COMM-MU20-NSOCENTR       PIC X(3).                     00745002
              03 COMM-MU20-NDEPOT         PIC X(3).                     00748002
              03 COMM-MU20-DMUTATION      PIC X(8).                     00748002
              03 COMM-MU20-CSELART        PIC X(5).                     00748002
              03 COMM-MU20-QVOLUME        PIC S9(11) COMP-3.            00748002
              03 COMM-MU20-QNBLIGNES      PIC S9(5) COMP-3.             00748002
              03 COMM-MU20-QNBPIECES      PIC S9(5) COMP-3.             00748002
              03 COMM-MU20-QNBPQUOTA      PIC S9(5) COMP-3.             00748002
              03 COMM-MU20-QNBM3QUOTA     PIC S9(4)V9 COMP-3.           00748002
              03 COMM-MU20-NCODIC         PIC X(7).                     00748002
              03 COMM-MU20-CFAM           PIC X(5).                     00748002
              03 COMM-MU20-CMARQ          PIC X(5).                     00748002
              03 COMM-MU20-CTYPCONDT      PIC X(5).                     00748002
              03 COMM-MU20-QCONDT         PIC S9(5) COMP-3.             00748002
              03 COMM-MU20-QCOLIDSTOCK    PIC S9(5) COMP-3.             00748002
              03 COMM-MU20-QPOIDS         PIC S9(7) COMP-3.             00748002
              03 COMM-MU20-QLARGEUR       PIC S9(3) COMP-3.             00748002
              03 COMM-MU20-QPROFONDEUR    PIC S9(3) COMP-3.             00748002
              03 COMM-MU20-QHAUTEUR       PIC S9(3) COMP-3.             00748002
              03 COMM-MU20-CMODSTOCK      PIC X(5).                     00748002
              03 COMM-MU20-LFAM           PIC X(20).                    00748002
              03 COMM-MU20-WSEQFAM        PIC S9(5) COMP-3.             00748002
              03 COMM-MU20-LREFFOURN      PIC X(20).                    00748002
              03 COMM-MU20-CTYPSOC        PIC X(3).                     00748002
              03 COMM-MU20-CASSORT        PIC X(5).                     00748002
              03 COMM-MU20-WVAL           PIC X(1).                     00748002
              03 COMM-MU20-ORIGINE-MUT    PIC X(1).                     00748002
              03 COMM-MU20-TAB1-DEPOT.                                          
  CJ01*          05 COMM-MU20-TAB1 OCCURS 5.                                    
  CJ01           05 COMM-MU20-TAB1 OCCURS 100.                                  
                    10 COMM-MU20-AUTOR-NSOCDEPOT PIC X(3).                      
                    10 COMM-MU20-AUTOR-NDEPOT    PIC X(3).                      
              03 COMM-MU20-TAB2-NLIEU.                                          
                 05 COMM-MU20-TAB2 OCCURS 600.                                  
                    10 COMM-MU20-AUTOR-NSOCLIEU  PIC X(3).                      
                    10 COMM-MU20-AUTOR-NLIEU     PIC X(3).                      
              03 COMM-MU20-AUTOR-NSOC            PIC X(3).                      
              03 COMM-MU20-NB-NDEPOT             PIC S9(3) COMP-3.              
              03 COMM-MU20-NB-NLIEU              PIC S9(3) COMP-3.              
  CJ01*       03 COMM-MU20-FILLER         PIC X(4016).                  00970003
  CJ01        03 COMM-MU20-FILLER         PIC X(3446).                  00970003
              03 COMM-MU20-RAJOUTS-SM     REDEFINES COMM-MU20-FILLER.           
                 04 COMM-MU20-DEBGRP         PIC X(5).                          
                 04 COMM-MU20-DEBSEM.                                           
                    05 COMM-MU20-DEBSEM-SS   PIC XX.                            
                    05 COMM-MU20-DEBSEM-AA   PIC XX.                            
                    05 COMM-MU20-DEBSEM-WW   PIC XX.                            
              03 COMM-MU66-APPLI    REDEFINES COMM-MU20-FILLER.                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-MU66-NB-CODIC          PIC S9(04) COMP.                 
      *--                                                                       
                05 COMM-MU66-NB-CODIC          PIC S9(04) COMP-5.               
      *}                                                                        
                05 COMM-MU66-NB-PAGE           PIC 9(03).                       
                05 COMM-MU66-NO-PAGE           PIC 9(03).                       
                05 COMM-MU66-CODIC             PIC 9(07).                       
                05 COMM-MU66-LDEPOT            PIC X(20).                       
                05 COMM-MU66-TRAIT             PIC X(01).                       
                05 COMM-MU66-TABLE.                                             
                   07 COMM-MU66-LIGNE          OCCURS 13.                       
                      09 COMM-MU66-CFAM        PIC X(05).                       
                      09 COMM-MU66-CMARQ       PIC X(05).                       
                      09 COMM-MU66-REF         PIC X(20).                       
                      09 COMM-MU66-NCODIC      PIC X(07).                       
                      09 COMM-MU66-NDOC        PIC X(07).                       
                      09 COMM-MU66-NMUT        PIC X(07).                       
                      09 COMM-MU66-DMUT        PIC X(08).                       
                      09 COMM-MU66-QTE         PIC 9(04).                       
                      09 COMM-MU66-STAT        PIC X(01).                       
                05 COMM-MU66-T-RNG.                                             
                   07 COMM-MU66-TRNG           OCCURS 200.                      
                      09 COMM-MU66-WCODIC      PIC  X(7).                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               09 COMM-MU66-RNG         PIC S9(4) COMP.                  
      *--                                                                       
                      09 COMM-MU66-RNG         PIC S9(4) COMP-5.                
      *}                                                                        
AD1           03 COMM-MU47-MU48-APPLI    REDEFINES COMM-MU20-FILLER.            
                 05 COMM-MU4-COPCO               PIC X(03).                     
                 05 COMM-MU4-CGROUP              PIC X(05).                     
                 05 COMM-MU4-MUT-LOINTAINE.                                     
                    07 COMM-MU4-ML-NB            PIC S9(05) COMP-3.             
                    07 COMM-MU4-ML-NMUTATION     PIC X(07).                     
                    07 COMM-MU4-ML-NSOCDEPOT     PIC X(03).                     
                    07 COMM-MU4-ML-NDEPOT        PIC X(03).                     
                    07 COMM-MU4-ML-CTRL          PIC X(01).                     
                 05 COMM-MU47-MUT.                                              
                    07 COMM-MU47-LDEPOT        PIC X(20).                       
                    07 COMM-MU47-QVOLUME       PIC S9(11) COMP-3.               
                    07 COMM-MU47-QNBPIECES     PIC S9(05) COMP-3.               
                    07 COMM-MU47-QNBM3QUOTA    PIC  9(5)V9.                     
                    07 COMM-MU47-QNBPQUOTA     PIC  9(5).                       
                 05 COMM-ML47-NB               PIC S9(05)   COMP-3.             
                 05 COMM-MU47-LIGNES           OCCURS 12.                       
      *             LONGUEUR DU TABLEAU 61 * 12 = 732                           
                    07 COMM-MU47-CFAM          PIC X(05).                       
                    07 COMM-MU47-CMARQ         PIC X(05).                       
                    07 COMM-MU47-REFFRS        PIC X(20).                       
                    07 COMM-MU47-QDISPO        PIC S9(05) COMP-3.               
                    07 COMM-MU47-QMUT          PIC S9(05) COMP-3.               
                    07 COMM-MU47-GP            PIC X(01).                       
                    07 COMM-MU47-NCODIC        PIC X(07).                       
                    07 COMM-MU47-QSAISIE       PIC S9(05) COMP-3.               
                    07 COMM-MU47-NCDE          PIC X(07).                       
                    07 COMM-MU47-NSURCDE       PIC X(07).                       
                 05 FILLER                     PIC X(0499).                     
                 05 COMM-MU48-MUT.                                              
                    07 COMM-MU48-LDEPOT        PIC X(20).                       
                    07 COMM-MU48-QVOLUME       PIC S9(11) COMP-3.               
                    07 COMM-MU48-QNBPIECES     PIC S9(05) COMP-3.               
                    07 COMM-MU48-QNBM3QUOTA    PIC  9(5)V9.                     
                    07 COMM-MU48-QNBPQUOTA     PIC  9(5).                       
                 05 COMM-ML48-NB               PIC S9(05)   COMP-3.             
                 05 COMM-MU48-LIGNES           OCCURS 12.                       
      *             LONGUEUR DU TABLEAU 61 * 12 = 732                           
                    07 COMM-MU48-CFAM          PIC X(05).                       
                    07 COMM-MU48-WSEQFAM       PIC S9(5) COMP-3.                
                    07 COMM-MU48-CMARQ         PIC X(05).                       
                    07 COMM-MU48-REFFRS        PIC X(20).                       
                    07 COMM-MU48-QPOIDS        PIC S9(7) COMP-3.                
                    07 COMM-MU48-VOL           PIC S9(07) COMP-3.               
                    07 COMM-MU48-CMODSTOCK     PIC X(05).                       
                    07 COMM-MU48-QDISPO        PIC S9(05) COMP-3.               
                    07 COMM-MU48-QMUT          PIC S9(05) COMP-3.               
                    07 COMM-MU48-GP            PIC X(01).                       
                    07 COMM-MU48-NCODIC        PIC X(07).                       
                    07 COMM-MU48-QSAISIE       PIC S9(05) COMP-3.               
                    07 COMM-MU48-NCDE          PIC X(07).                       
                    07 COMM-MU48-PRESENT-ML    PIC X(01).                       
                 05 FILLER                     PIC X(1000).                     
      *          05 FILLER                     PIC X(3446).                     
                                                                                
