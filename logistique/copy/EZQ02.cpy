      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EZQ02   EZQ02                                              00000020
      ***************************************************************** 00000030
       01   EZQ02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCTIONL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MFONCTIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MFONCTIONF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MFONCTIONI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPROFILL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MPROFILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPROFILF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MPROFILI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MJOURL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJOURF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MJOURI    PIC X(6).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOURNEEL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MTOURNEEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOURNEEF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MTOURNEEI      PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNMAGI    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNVENTEI  PIC X(7).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSPROFILL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MSPROFILL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSPROFILF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSPROFILI      PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLPROFILL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MSLPROFILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSLPROFILF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSLPROFILI     PIC X(19).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSJOURL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSJOURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSJOURF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSJOURI   PIC X(6).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTOURNEEL     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MSTOURNEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSTOURNEEF     PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSTOURNEEI     PIC X(3).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEPROFILL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MEPROFILL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MEPROFILF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MEPROFILI      PIC X(5).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MELPROFILL     COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MELPROFILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MELPROFILF     PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MELPROFILI     PIC X(19).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEJOURL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MEJOURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEJOURF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MEJOURI   PIC X(6).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 METOURNEEL     COMP PIC S9(4).                            00000740
      *--                                                                       
           02 METOURNEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 METOURNEEF     PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 METOURNEEI     PIC X(3).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPRIMANTEL   COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MIMPRIMANTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MIMPRIMANTEF   PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MIMPRIMANTEI   PIC X(4).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURCTL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MJOURCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJOURCTF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MJOURCTI  PIC X(6).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPRCTL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MIMPRCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MIMPRCTF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MIMPRCTI  PIC X(4).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR1L   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MJOUR1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR1F   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MJOUR1I   PIC X(6).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAG6L    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MMAG6L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMAG6F    PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MMAG6I    PIC X(3).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTE6L      COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MNVENTE6L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNVENTE6F      PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MNVENTE6I      PIC X(7).                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR6L   COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MJOUR6L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR6F   PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MJOUR6I   PIC X(6).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOUR7L   COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MJOUR7L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJOUR7F   PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MJOUR7I   PIC X(6).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPRIML  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MIMPRIML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MIMPRIMF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MIMPRIMI  PIC X(4).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLIBERRI  PIC X(78).                                      00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCODTRAI  PIC X(4).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MCICSI    PIC X(5).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MNETNAMI  PIC X(8).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MSCREENI  PIC X(4).                                       00001330
      ***************************************************************** 00001340
      * SDF: EZQ02   EZQ02                                              00001350
      ***************************************************************** 00001360
       01   EZQ02O REDEFINES EZQ02I.                                    00001370
           02 FILLER    PIC X(12).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MDATJOUA  PIC X.                                          00001400
           02 MDATJOUC  PIC X.                                          00001410
           02 MDATJOUP  PIC X.                                          00001420
           02 MDATJOUH  PIC X.                                          00001430
           02 MDATJOUV  PIC X.                                          00001440
           02 MDATJOUO  PIC X(10).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MTIMJOUA  PIC X.                                          00001470
           02 MTIMJOUC  PIC X.                                          00001480
           02 MTIMJOUP  PIC X.                                          00001490
           02 MTIMJOUH  PIC X.                                          00001500
           02 MTIMJOUV  PIC X.                                          00001510
           02 MTIMJOUO  PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MZONCMDA  PIC X.                                          00001540
           02 MZONCMDC  PIC X.                                          00001550
           02 MZONCMDP  PIC X.                                          00001560
           02 MZONCMDH  PIC X.                                          00001570
           02 MZONCMDV  PIC X.                                          00001580
           02 MZONCMDO  PIC X(15).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNSOCA    PIC X.                                          00001610
           02 MNSOCC    PIC X.                                          00001620
           02 MNSOCP    PIC X.                                          00001630
           02 MNSOCH    PIC X.                                          00001640
           02 MNSOCV    PIC X.                                          00001650
           02 MNSOCO    PIC X(3).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MFONCTIONA     PIC X.                                     00001680
           02 MFONCTIONC     PIC X.                                     00001690
           02 MFONCTIONP     PIC X.                                     00001700
           02 MFONCTIONH     PIC X.                                     00001710
           02 MFONCTIONV     PIC X.                                     00001720
           02 MFONCTIONO     PIC X(3).                                  00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MPROFILA  PIC X.                                          00001750
           02 MPROFILC  PIC X.                                          00001760
           02 MPROFILP  PIC X.                                          00001770
           02 MPROFILH  PIC X.                                          00001780
           02 MPROFILV  PIC X.                                          00001790
           02 MPROFILO  PIC X(5).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MJOURA    PIC X.                                          00001820
           02 MJOURC    PIC X.                                          00001830
           02 MJOURP    PIC X.                                          00001840
           02 MJOURH    PIC X.                                          00001850
           02 MJOURV    PIC X.                                          00001860
           02 MJOURO    PIC X(6).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MTOURNEEA      PIC X.                                     00001890
           02 MTOURNEEC PIC X.                                          00001900
           02 MTOURNEEP PIC X.                                          00001910
           02 MTOURNEEH PIC X.                                          00001920
           02 MTOURNEEV PIC X.                                          00001930
           02 MTOURNEEO      PIC X(3).                                  00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MNMAGA    PIC X.                                          00001960
           02 MNMAGC    PIC X.                                          00001970
           02 MNMAGP    PIC X.                                          00001980
           02 MNMAGH    PIC X.                                          00001990
           02 MNMAGV    PIC X.                                          00002000
           02 MNMAGO    PIC X(3).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MNVENTEA  PIC X.                                          00002030
           02 MNVENTEC  PIC X.                                          00002040
           02 MNVENTEP  PIC X.                                          00002050
           02 MNVENTEH  PIC X.                                          00002060
           02 MNVENTEV  PIC X.                                          00002070
           02 MNVENTEO  PIC X(7).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MSPROFILA      PIC X.                                     00002100
           02 MSPROFILC PIC X.                                          00002110
           02 MSPROFILP PIC X.                                          00002120
           02 MSPROFILH PIC X.                                          00002130
           02 MSPROFILV PIC X.                                          00002140
           02 MSPROFILO      PIC X(5).                                  00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MSLPROFILA     PIC X.                                     00002170
           02 MSLPROFILC     PIC X.                                     00002180
           02 MSLPROFILP     PIC X.                                     00002190
           02 MSLPROFILH     PIC X.                                     00002200
           02 MSLPROFILV     PIC X.                                     00002210
           02 MSLPROFILO     PIC X(19).                                 00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MSJOURA   PIC X.                                          00002240
           02 MSJOURC   PIC X.                                          00002250
           02 MSJOURP   PIC X.                                          00002260
           02 MSJOURH   PIC X.                                          00002270
           02 MSJOURV   PIC X.                                          00002280
           02 MSJOURO   PIC X(6).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MSTOURNEEA     PIC X.                                     00002310
           02 MSTOURNEEC     PIC X.                                     00002320
           02 MSTOURNEEP     PIC X.                                     00002330
           02 MSTOURNEEH     PIC X.                                     00002340
           02 MSTOURNEEV     PIC X.                                     00002350
           02 MSTOURNEEO     PIC X(3).                                  00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MEPROFILA      PIC X.                                     00002380
           02 MEPROFILC PIC X.                                          00002390
           02 MEPROFILP PIC X.                                          00002400
           02 MEPROFILH PIC X.                                          00002410
           02 MEPROFILV PIC X.                                          00002420
           02 MEPROFILO      PIC X(5).                                  00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MELPROFILA     PIC X.                                     00002450
           02 MELPROFILC     PIC X.                                     00002460
           02 MELPROFILP     PIC X.                                     00002470
           02 MELPROFILH     PIC X.                                     00002480
           02 MELPROFILV     PIC X.                                     00002490
           02 MELPROFILO     PIC X(19).                                 00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MEJOURA   PIC X.                                          00002520
           02 MEJOURC   PIC X.                                          00002530
           02 MEJOURP   PIC X.                                          00002540
           02 MEJOURH   PIC X.                                          00002550
           02 MEJOURV   PIC X.                                          00002560
           02 MEJOURO   PIC X(6).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 METOURNEEA     PIC X.                                     00002590
           02 METOURNEEC     PIC X.                                     00002600
           02 METOURNEEP     PIC X.                                     00002610
           02 METOURNEEH     PIC X.                                     00002620
           02 METOURNEEV     PIC X.                                     00002630
           02 METOURNEEO     PIC X(3).                                  00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MIMPRIMANTEA   PIC X.                                     00002660
           02 MIMPRIMANTEC   PIC X.                                     00002670
           02 MIMPRIMANTEP   PIC X.                                     00002680
           02 MIMPRIMANTEH   PIC X.                                     00002690
           02 MIMPRIMANTEV   PIC X.                                     00002700
           02 MIMPRIMANTEO   PIC X(4).                                  00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MJOURCTA  PIC X.                                          00002730
           02 MJOURCTC  PIC X.                                          00002740
           02 MJOURCTP  PIC X.                                          00002750
           02 MJOURCTH  PIC X.                                          00002760
           02 MJOURCTV  PIC X.                                          00002770
           02 MJOURCTO  PIC X(6).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MIMPRCTA  PIC X.                                          00002800
           02 MIMPRCTC  PIC X.                                          00002810
           02 MIMPRCTP  PIC X.                                          00002820
           02 MIMPRCTH  PIC X.                                          00002830
           02 MIMPRCTV  PIC X.                                          00002840
           02 MIMPRCTO  PIC X(4).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MJOUR1A   PIC X.                                          00002870
           02 MJOUR1C   PIC X.                                          00002880
           02 MJOUR1P   PIC X.                                          00002890
           02 MJOUR1H   PIC X.                                          00002900
           02 MJOUR1V   PIC X.                                          00002910
           02 MJOUR1O   PIC X(6).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MMAG6A    PIC X.                                          00002940
           02 MMAG6C    PIC X.                                          00002950
           02 MMAG6P    PIC X.                                          00002960
           02 MMAG6H    PIC X.                                          00002970
           02 MMAG6V    PIC X.                                          00002980
           02 MMAG6O    PIC X(3).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MNVENTE6A      PIC X.                                     00003010
           02 MNVENTE6C PIC X.                                          00003020
           02 MNVENTE6P PIC X.                                          00003030
           02 MNVENTE6H PIC X.                                          00003040
           02 MNVENTE6V PIC X.                                          00003050
           02 MNVENTE6O      PIC X(7).                                  00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MJOUR6A   PIC X.                                          00003080
           02 MJOUR6C   PIC X.                                          00003090
           02 MJOUR6P   PIC X.                                          00003100
           02 MJOUR6H   PIC X.                                          00003110
           02 MJOUR6V   PIC X.                                          00003120
           02 MJOUR6O   PIC X(6).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MJOUR7A   PIC X.                                          00003150
           02 MJOUR7C   PIC X.                                          00003160
           02 MJOUR7P   PIC X.                                          00003170
           02 MJOUR7H   PIC X.                                          00003180
           02 MJOUR7V   PIC X.                                          00003190
           02 MJOUR7O   PIC X(6).                                       00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MIMPRIMA  PIC X.                                          00003220
           02 MIMPRIMC  PIC X.                                          00003230
           02 MIMPRIMP  PIC X.                                          00003240
           02 MIMPRIMH  PIC X.                                          00003250
           02 MIMPRIMV  PIC X.                                          00003260
           02 MIMPRIMO  PIC X(4).                                       00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MLIBERRA  PIC X.                                          00003290
           02 MLIBERRC  PIC X.                                          00003300
           02 MLIBERRP  PIC X.                                          00003310
           02 MLIBERRH  PIC X.                                          00003320
           02 MLIBERRV  PIC X.                                          00003330
           02 MLIBERRO  PIC X(78).                                      00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MCODTRAA  PIC X.                                          00003360
           02 MCODTRAC  PIC X.                                          00003370
           02 MCODTRAP  PIC X.                                          00003380
           02 MCODTRAH  PIC X.                                          00003390
           02 MCODTRAV  PIC X.                                          00003400
           02 MCODTRAO  PIC X(4).                                       00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MCICSA    PIC X.                                          00003430
           02 MCICSC    PIC X.                                          00003440
           02 MCICSP    PIC X.                                          00003450
           02 MCICSH    PIC X.                                          00003460
           02 MCICSV    PIC X.                                          00003470
           02 MCICSO    PIC X(5).                                       00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MNETNAMA  PIC X.                                          00003500
           02 MNETNAMC  PIC X.                                          00003510
           02 MNETNAMP  PIC X.                                          00003520
           02 MNETNAMH  PIC X.                                          00003530
           02 MNETNAMV  PIC X.                                          00003540
           02 MNETNAMO  PIC X(8).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MSCREENA  PIC X.                                          00003570
           02 MSCREENC  PIC X.                                          00003580
           02 MSCREENP  PIC X.                                          00003590
           02 MSCREENH  PIC X.                                          00003600
           02 MSCREENV  PIC X.                                          00003610
           02 MSCREENO  PIC X(4).                                       00003620
                                                                                
