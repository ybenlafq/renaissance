      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      * ZONE DE TS                                                              
       01  :X:-EF10-NOM.                                                        
           02 :X:-EF10-TRAN             PIC X(4) VALUE 'EF10'.                  
           02 :X:-EF10-TERM             PIC X(4) VALUE SPACES.                  
       01  :X:-NOM REDEFINES :X:-EF10-NOM                                       
                                        PIC X(8).                               
      *                                                                         
       01  :X:-EF10-DATA.                                                       
           02 :X:-EF10-LIGNE-DATA OCCURS 10.                                    
              03 :X:-EF10-SELECTION     PIC X(1).                               
              03 :X:-EF10-NACCORD       PIC X(12).                              
              03 :X:-EF10-DACCORD       PIC X(8).                               
              03 :X:-EF10-LNOMACCORD    PIC X(10).                              
              03 :X:-EF10-NCODIC        PIC X(7).                               
              03 :X:-EF10-NSOCORIG      PIC X(3).                               
              03 :X:-EF10-NLIEUORIG     PIC X(3).                               
              03 :X:-EF10-NORIGINE      PIC X(7).                               
              03 :X:-EF10-QTE-X         PIC X(3).                               
              03 :X:-EF10-QTE  REDEFINES :X:-EF10-QTE-X PIC 9(3).               
              03 :X:-EF10-NSERIE        PIC X(16).                              
              03 :X:-EF10-CGARANTI      PIC X(5).                               
              03 :X:-EF10-CRENDU        PIC X(5).                               
              03 :X:-EF10-CMOTIF        PIC X(10).                              
              03 :X:-EF10-ORIG-NSOC     PIC X(3).                               
              03 :X:-EF10-ORIG-NLIEU    PIC X(3).                               
              03 :X:-EF10-ORIG-NSLIEU   PIC X(3).                               
              03 :X:-EF10-ORIG-LITRT    PIC X(5).                               
              03 :X:-EF10-ORIG-CTYPLIEU PIC X(1).                               
              03 :X:-EF10-DEST-NSOC     PIC X(3).                               
              03 :X:-EF10-DEST-NLIEU    PIC X(3).                               
              03 :X:-EF10-DEST-NSLIEU   PIC X(3).                               
              03 :X:-EF10-DEST-LITRT    PIC X(5).                               
              03 :X:-EF10-DEST-CTYPLIEU PIC X(1).                               
              03 :X:-EF10-NENTCDE       PIC X(5).                               
              03 :X:-EF10-CTAUXTVA      PIC X(5).                               
              03 :X:-EF10-LNOMCLI       PIC X(25).                              
              03 :X:-EF10-CEMPL         PIC X(8).                               
              03 :X:-EF10-LPANNE        PIC X(30).                              
              03 :X:-EF10-CFAM          PIC X(05).                              
              03 :X:-EF10-WSEQFAM       PIC S9(5).                              
              03 :X:-EF10-CMARQ         PIC X(5).                               
              03 :X:-EF10-LREF          PIC X(20).                              
       01  :X:-DATA REDEFINES :X:-EF10-DATA.                                    
           03 :X:-LIGNE OCCURS 10.                                              
              05 FILLER                 PIC X(228).                             
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-EF10-LONG                PIC S9(4) COMP VALUE 2280.               
      *--                                                                       
       01  :X:-EF10-LONG                PIC S9(4) COMP-5 VALUE 2280.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-LONG REDEFINES TS-EF10-LONG                                       
      *                                 PIC S9(4) COMP.                         
      *--                                                                       
       01  :X:-LONG REDEFINES :X:-EF10-LONG                                     
                                        PIC S9(4) COMP-5.                       
      *}                                                                        
      *                                                               * 00000720
                                                                                
