      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *                COMMAREA PROGRAMME TFL32                    *            
      **************************************************************            
      *                                                                         
      * COM-RM40-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      *                                                                         
           03  COMM-FL32-APPLI REDEFINES COMM-FL30-FILLER.                      
               05 COMM-FL32-CREATE          PIC X.                              
               05 COMM-FL32-SUPP            PIC X.                              
               05 COMM-FL32-PAGE            PIC S9(4) COMP-3.                   
               05 COMM-FL32-PAGE-MAX        PIC S9(4) COMP-3.                   
               05 COMM-FL32-ITEMAX          PIC S9(4) COMP-3.                   
               05 COMM-FL32-CPROAFF         PIC X(05).                          
               05 COMM-FL32-TYPTRAIT        PIC X(05).                          
               05 COMM-FL32-SOC             PIC X(03).                          
               05 COMM-FL32-LIEU            PIC X(03).                          
      *        05 COMM-FL32-FILLER          PIC X(3695).                        
                                                                                
