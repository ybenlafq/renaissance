      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:32 >
      
       01  COMMGD91.
           05  COMM-GD91-DATE.
               10  COMM-GD91-DAA                 PIC  X(2).
               10  COMM-GD91-DMM                 PIC  X(2).
               10  COMM-GD91-DJJ                 PIC  X(2).
           05  COMM-GD91-DOC.
               10  FILLER                        PIC  X(6).
               10  COMM-GD91-NRAFALE             PIC  X(3).
               10  COMM-GD91-NMAG-NRUPT          PIC  9(3).
      * PASSAGE DU RAF SUR 3 --> SUPPRESSION DU '.' AVANT CNPAGE
      *        10  FILLER                        PIC  X(1).
               10  COMM-GD91-CNPAGE              PIC  9(3).
           05  COMM-GD91-GAUCHE                  PIC  X(1).
           05  COMM-GD91-PSB                     PIC  X(1).
           05  COMM-GD91-OPEN                    PIC  X(1).
           05  COMM-GD91-NBE                     PIC  S9(5)  COMP-3.
           05  COMM-GD91-LIGNE.
               10  COMM-GD91-LLIEU               PIC  X(20).
               10  COMM-GD91-CALLEE              PIC  X(2).
               10  COMM-GD91-CNIVEAU             PIC  X(2).
               10  COMM-GD91-NPOSITION           PIC  X(3).
               10  COMM-GD91-NCODIC              PIC  X(7).
               10  COMM-GD91-QTE                 PIC  S9(5)  COMP-3.
               10  COMM-GD91-QCONDT              PIC  S9(5)  COMP-3.
               10  COMM-GD91-CFAM                PIC  X(5).
               10  COMM-GD91-CMARQ               PIC  X(5).
               10  COMM-GD91-LREFFOURN           PIC  X(20).
               10  COMM-GD91-LEMBALLAGE          PIC  X(42).
               10  COMM-GD91-NSATELLITE          PIC  X(2).
           05  COMM-GD91-RUPTURE.
               10  COMM-GD91-CALLEE-R            PIC  X(2).
               10  COMM-GD91-CNIVEAU-R           PIC  X(2).
               10  COMM-GD91-NPOSITION-R         PIC  X(3).
               10  COMM-GD91-NCODIC-R            PIC  X(7).
               10  COMM-GD91-CFAM-R              PIC  X(5).
               10  COMM-GD91-CMARQ-R             PIC  X(5).
               10  COMM-GD91-LREFFOURN-R         PIC  X(20).
               10  COMM-GD91-LEMBALLAGE-R        PIC  X(42).
               10  COMM-GD91-NSATELLITE-R        PIC  X(2).
               10  COMM-GD91-NSOC-R              PIC  X(3).
               10  COMM-GD91-NMAG-R              PIC  X(3).
               10  COMM-GD91-NRUPT-R             PIC  9(3).
               10  COMM-GD91-NPAGE-R             PIC  9(3).
           05  COMM-GD91-REPARTITION.
               10  COMM-GD91-TABLE-REP                OCCURS 100.
                   15  COMM-GD91-NSOC-REP   PIC X(3).
                   15  COMM-GD91-NMAG-REP   PIC X(3).
                   15  COMM-GD91-CSEL-REP   PIC X(5).
                   15  COMM-GD91-NSAT-REP   PIC X(2)         OCCURS 15.
                   15  COMM-GD91-QTE-REP    PIC S9(5) COMP-3 OCCURS 15.
           05  COMM-GD91-NSOC               PIC X(3).
           05  COMM-GD91-NMAG               PIC X(3).
           05  COMM-GD91-NRUPT              PIC 9(3).
           05  COMM-GD91-NPAGE              PIC 9(3).
           05  COMM-GD91-CSELART            PIC X(5).
           05  COMM-GD91-WVENTMUT           PIC X(1).
           05  COMM-GD91-WLISTESAT          PIC X(1).
           05  COMM-GD91-WCOMMUT            PIC X(1).
           05  COMM-GD91-ETAT-COLIS         PIC X(1) VALUE 'N'.
               88  COMM-GD91-COLIS-OK                VALUE 'O'.
               88  COMM-GD91-COLIS-NOK               VALUE 'N'.
           05  COMM-GD91-ETAT-UNITEE        PIC X(1) VALUE 'N'.
               88  COMM-GD91-UNITEE-OK               VALUE 'O'.
               88  COMM-GD91-UNITEE-NOK              VALUE 'N'.
           05  COMM-GD91-ETAT-RUPTURE       PIC X(1) VALUE 'O'.
               88  COMM-GD91-RUPT-NOK                VALUE 'O'.
               88  COMM-GD91-RUPT-OK                 VALUE 'N'.
           05  COMM-GD91-ETAT-RAFALE        PIC X(1) VALUE 'N'.
               88  COMM-GD91-NEW-RAF                 VALUE 'N'.
               88  COMM-GD91-OLD-RAF                 VALUE 'O'.
           05  COMM-GD91-PROG               PIC X(4).
           05  COMM-GD91-NSOCDEPOT          PIC X(3).
           05  COMM-GD91-NDEPOT             PIC X(3).
      
