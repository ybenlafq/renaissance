      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS80   EGS80                                              00000020
      ***************************************************************** 00000030
       01   EGS80I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGESI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCODICI   PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLCODICI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MQTEL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MQTEF     PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MQTEI     PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAML     COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MFAML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFAMF     PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MFAMI     PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIG1L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MORIG1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MORIG1F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MORIG1I   PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIG2L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MORIG2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MORIG2F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MORIG2I   PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIG3L   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MORIG3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MORIG3F   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MORIG3I   PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMQEL     COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MMQEL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMQEF     PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MMQEI     PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMQEL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLMQEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMQEF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLMQEI    PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMENTL     COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLCOMMENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMENTF     PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLCOMMENTI     PIC X(20).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUENTL     COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNLIEUENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNLIEUENTF     PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNLIEUENTI     PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNHSL     COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNHSL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNHSF     PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNHSI     PIC X(7).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMOTIFL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MMOTIFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MMOTIFF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MMOTIFI   PIC X(10).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNSOCIETEI     PIC X(3).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNLIEUI   PIC X(3).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSSLIEUL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MNSSLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSSLIEUF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MNSSLIEUI      PIC X(3).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMOUVMTL      COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MDMOUVMTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDMOUVMTF      PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MDMOUVMTI      PIC X(8).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEMPLACL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MEMPLACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MEMPLACF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MEMPLACI  PIC X(5).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLITRTL   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLITRTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLITRTF   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLITRTI   PIC X(5).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSTATUTI  PIC X(5).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMSTOCL     COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MLCOMSTOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMSTOCF     PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLCOMSTOCI     PIC X(20).                                 00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MZONCMDI  PIC X(12).                                      00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLIBERRI  PIC X(61).                                      00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCODTRAI  PIC X(4).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MCICSI    PIC X(5).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MNETNAMI  PIC X(8).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MSCREENI  PIC X(4).                                       00001330
      ***************************************************************** 00001340
      * SDF: EGS80   EGS80                                              00001350
      ***************************************************************** 00001360
       01   EGS80O REDEFINES EGS80I.                                    00001370
           02 FILLER    PIC X(12).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MDATJOUA  PIC X.                                          00001400
           02 MDATJOUC  PIC X.                                          00001410
           02 MDATJOUP  PIC X.                                          00001420
           02 MDATJOUH  PIC X.                                          00001430
           02 MDATJOUV  PIC X.                                          00001440
           02 MDATJOUO  PIC X(10).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MTIMJOUA  PIC X.                                          00001470
           02 MTIMJOUC  PIC X.                                          00001480
           02 MTIMJOUP  PIC X.                                          00001490
           02 MTIMJOUH  PIC X.                                          00001500
           02 MTIMJOUV  PIC X.                                          00001510
           02 MTIMJOUO  PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MPAGEA    PIC X.                                          00001540
           02 MPAGEC    PIC X.                                          00001550
           02 MPAGEP    PIC X.                                          00001560
           02 MPAGEH    PIC X.                                          00001570
           02 MPAGEV    PIC X.                                          00001580
           02 MPAGEO    PIC X(3).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNBPAGESA      PIC X.                                     00001610
           02 MNBPAGESC PIC X.                                          00001620
           02 MNBPAGESP PIC X.                                          00001630
           02 MNBPAGESH PIC X.                                          00001640
           02 MNBPAGESV PIC X.                                          00001650
           02 MNBPAGESO      PIC X(3).                                  00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MCODICA   PIC X.                                          00001680
           02 MCODICC   PIC X.                                          00001690
           02 MCODICP   PIC X.                                          00001700
           02 MCODICH   PIC X.                                          00001710
           02 MCODICV   PIC X.                                          00001720
           02 MCODICO   PIC X(7).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MLCODICA  PIC X.                                          00001750
           02 MLCODICC  PIC X.                                          00001760
           02 MLCODICP  PIC X.                                          00001770
           02 MLCODICH  PIC X.                                          00001780
           02 MLCODICV  PIC X.                                          00001790
           02 MLCODICO  PIC X(20).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MQTEA     PIC X.                                          00001820
           02 MQTEC     PIC X.                                          00001830
           02 MQTEP     PIC X.                                          00001840
           02 MQTEH     PIC X.                                          00001850
           02 MQTEV     PIC X.                                          00001860
           02 MQTEO     PIC X(5).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MFAMA     PIC X.                                          00001890
           02 MFAMC     PIC X.                                          00001900
           02 MFAMP     PIC X.                                          00001910
           02 MFAMH     PIC X.                                          00001920
           02 MFAMV     PIC X.                                          00001930
           02 MFAMO     PIC X(5).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MLFAMA    PIC X.                                          00001960
           02 MLFAMC    PIC X.                                          00001970
           02 MLFAMP    PIC X.                                          00001980
           02 MLFAMH    PIC X.                                          00001990
           02 MLFAMV    PIC X.                                          00002000
           02 MLFAMO    PIC X(20).                                      00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MORIG1A   PIC X.                                          00002030
           02 MORIG1C   PIC X.                                          00002040
           02 MORIG1P   PIC X.                                          00002050
           02 MORIG1H   PIC X.                                          00002060
           02 MORIG1V   PIC X.                                          00002070
           02 MORIG1O   PIC X(3).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MORIG2A   PIC X.                                          00002100
           02 MORIG2C   PIC X.                                          00002110
           02 MORIG2P   PIC X.                                          00002120
           02 MORIG2H   PIC X.                                          00002130
           02 MORIG2V   PIC X.                                          00002140
           02 MORIG2O   PIC X(3).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MORIG3A   PIC X.                                          00002170
           02 MORIG3C   PIC X.                                          00002180
           02 MORIG3P   PIC X.                                          00002190
           02 MORIG3H   PIC X.                                          00002200
           02 MORIG3V   PIC X.                                          00002210
           02 MORIG3O   PIC X(3).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MMQEA     PIC X.                                          00002240
           02 MMQEC     PIC X.                                          00002250
           02 MMQEP     PIC X.                                          00002260
           02 MMQEH     PIC X.                                          00002270
           02 MMQEV     PIC X.                                          00002280
           02 MMQEO     PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLMQEA    PIC X.                                          00002310
           02 MLMQEC    PIC X.                                          00002320
           02 MLMQEP    PIC X.                                          00002330
           02 MLMQEH    PIC X.                                          00002340
           02 MLMQEV    PIC X.                                          00002350
           02 MLMQEO    PIC X(20).                                      00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MLCOMMENTA     PIC X.                                     00002380
           02 MLCOMMENTC     PIC X.                                     00002390
           02 MLCOMMENTP     PIC X.                                     00002400
           02 MLCOMMENTH     PIC X.                                     00002410
           02 MLCOMMENTV     PIC X.                                     00002420
           02 MLCOMMENTO     PIC X(20).                                 00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MNLIEUENTA     PIC X.                                     00002450
           02 MNLIEUENTC     PIC X.                                     00002460
           02 MNLIEUENTP     PIC X.                                     00002470
           02 MNLIEUENTH     PIC X.                                     00002480
           02 MNLIEUENTV     PIC X.                                     00002490
           02 MNLIEUENTO     PIC X(3).                                  00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MNHSA     PIC X.                                          00002520
           02 MNHSC     PIC X.                                          00002530
           02 MNHSP     PIC X.                                          00002540
           02 MNHSH     PIC X.                                          00002550
           02 MNHSV     PIC X.                                          00002560
           02 MNHSO     PIC X(7).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MMOTIFA   PIC X.                                          00002590
           02 MMOTIFC   PIC X.                                          00002600
           02 MMOTIFP   PIC X.                                          00002610
           02 MMOTIFH   PIC X.                                          00002620
           02 MMOTIFV   PIC X.                                          00002630
           02 MMOTIFO   PIC X(10).                                      00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MNSOCIETEA     PIC X.                                     00002660
           02 MNSOCIETEC     PIC X.                                     00002670
           02 MNSOCIETEP     PIC X.                                     00002680
           02 MNSOCIETEH     PIC X.                                     00002690
           02 MNSOCIETEV     PIC X.                                     00002700
           02 MNSOCIETEO     PIC X(3).                                  00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MNLIEUA   PIC X.                                          00002730
           02 MNLIEUC   PIC X.                                          00002740
           02 MNLIEUP   PIC X.                                          00002750
           02 MNLIEUH   PIC X.                                          00002760
           02 MNLIEUV   PIC X.                                          00002770
           02 MNLIEUO   PIC X(3).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MNSSLIEUA      PIC X.                                     00002800
           02 MNSSLIEUC PIC X.                                          00002810
           02 MNSSLIEUP PIC X.                                          00002820
           02 MNSSLIEUH PIC X.                                          00002830
           02 MNSSLIEUV PIC X.                                          00002840
           02 MNSSLIEUO      PIC X(3).                                  00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MDMOUVMTA      PIC X.                                     00002870
           02 MDMOUVMTC PIC X.                                          00002880
           02 MDMOUVMTP PIC X.                                          00002890
           02 MDMOUVMTH PIC X.                                          00002900
           02 MDMOUVMTV PIC X.                                          00002910
           02 MDMOUVMTO      PIC X(8).                                  00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MEMPLACA  PIC X.                                          00002940
           02 MEMPLACC  PIC X.                                          00002950
           02 MEMPLACP  PIC X.                                          00002960
           02 MEMPLACH  PIC X.                                          00002970
           02 MEMPLACV  PIC X.                                          00002980
           02 MEMPLACO  PIC X(5).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MLITRTA   PIC X.                                          00003010
           02 MLITRTC   PIC X.                                          00003020
           02 MLITRTP   PIC X.                                          00003030
           02 MLITRTH   PIC X.                                          00003040
           02 MLITRTV   PIC X.                                          00003050
           02 MLITRTO   PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MSTATUTA  PIC X.                                          00003080
           02 MSTATUTC  PIC X.                                          00003090
           02 MSTATUTP  PIC X.                                          00003100
           02 MSTATUTH  PIC X.                                          00003110
           02 MSTATUTV  PIC X.                                          00003120
           02 MSTATUTO  PIC X(5).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MLCOMSTOCA     PIC X.                                     00003150
           02 MLCOMSTOCC     PIC X.                                     00003160
           02 MLCOMSTOCP     PIC X.                                     00003170
           02 MLCOMSTOCH     PIC X.                                     00003180
           02 MLCOMSTOCV     PIC X.                                     00003190
           02 MLCOMSTOCO     PIC X(20).                                 00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MZONCMDA  PIC X.                                          00003220
           02 MZONCMDC  PIC X.                                          00003230
           02 MZONCMDP  PIC X.                                          00003240
           02 MZONCMDH  PIC X.                                          00003250
           02 MZONCMDV  PIC X.                                          00003260
           02 MZONCMDO  PIC X(12).                                      00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MLIBERRA  PIC X.                                          00003290
           02 MLIBERRC  PIC X.                                          00003300
           02 MLIBERRP  PIC X.                                          00003310
           02 MLIBERRH  PIC X.                                          00003320
           02 MLIBERRV  PIC X.                                          00003330
           02 MLIBERRO  PIC X(61).                                      00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MCODTRAA  PIC X.                                          00003360
           02 MCODTRAC  PIC X.                                          00003370
           02 MCODTRAP  PIC X.                                          00003380
           02 MCODTRAH  PIC X.                                          00003390
           02 MCODTRAV  PIC X.                                          00003400
           02 MCODTRAO  PIC X(4).                                       00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MCICSA    PIC X.                                          00003430
           02 MCICSC    PIC X.                                          00003440
           02 MCICSP    PIC X.                                          00003450
           02 MCICSH    PIC X.                                          00003460
           02 MCICSV    PIC X.                                          00003470
           02 MCICSO    PIC X(5).                                       00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MNETNAMA  PIC X.                                          00003500
           02 MNETNAMC  PIC X.                                          00003510
           02 MNETNAMP  PIC X.                                          00003520
           02 MNETNAMH  PIC X.                                          00003530
           02 MNETNAMV  PIC X.                                          00003540
           02 MNETNAMO  PIC X(8).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MSCREENA  PIC X.                                          00003570
           02 MSCREENC  PIC X.                                          00003580
           02 MSCREENP  PIC X.                                          00003590
           02 MSCREENH  PIC X.                                          00003600
           02 MSCREENV  PIC X.                                          00003610
           02 MSCREENO  PIC X(4).                                       00003620
                                                                                
