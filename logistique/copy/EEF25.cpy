      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EEF25   EEF25                                              00000020
      ***************************************************************** 00000030
       01   EEF25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCTRAITI  PIC X(5).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MLTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRAITF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLTRAITI  PIC X(20).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLTIERSI  PIC X(15).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFILLER1L      COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MFILLER1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFILLER1F      PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MFILLER1I      PIC X(5).                                  00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTIERSI  PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDEBL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MDATEDEBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEDEBF      PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MDATEDEBI      PIC X(7).                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEFINL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MDATEFINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEFINF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MDATEFINI      PIC X(7).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFILLER2L      COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MFILLER2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFILLER2F      PIC X.                                     00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MFILLER2I      PIC X.                                     00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMRELL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MNUMRELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMRELF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MNUMRELI  PIC X.                                          00000510
      * MESSAGE ERREUR                                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MLIBERRI  PIC X(78).                                      00000560
      * CODE TRANSACTION                                                00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCODTRAI  PIC X(4).                                       00000610
      * CICS DE TRAVAIL                                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      * NETNAME                                                         00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MNETNAMI  PIC X(8).                                       00000710
      * CODE TERMINAL                                                   00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MSCREENI  PIC X(5).                                       00000760
      ***************************************************************** 00000770
      * SDF: EEF25   EEF25                                              00000780
      ***************************************************************** 00000790
       01   EEF25O REDEFINES EEF25I.                                    00000800
           02 FILLER    PIC X(12).                                      00000810
      * DATE DU JOUR                                                    00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MDATJOUA  PIC X.                                          00000840
           02 MDATJOUC  PIC X.                                          00000850
           02 MDATJOUP  PIC X.                                          00000860
           02 MDATJOUH  PIC X.                                          00000870
           02 MDATJOUV  PIC X.                                          00000880
           02 MDATJOUO  PIC X(10).                                      00000890
      * HEURE                                                           00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MCTRAITA  PIC X.                                          00000990
           02 MCTRAITC  PIC X.                                          00001000
           02 MCTRAITP  PIC X.                                          00001010
           02 MCTRAITH  PIC X.                                          00001020
           02 MCTRAITV  PIC X.                                          00001030
           02 MCTRAITO  PIC X(5).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MLTRAITA  PIC X.                                          00001060
           02 MLTRAITC  PIC X.                                          00001070
           02 MLTRAITP  PIC X.                                          00001080
           02 MLTRAITH  PIC X.                                          00001090
           02 MLTRAITV  PIC X.                                          00001100
           02 MLTRAITO  PIC X(20).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MLTIERSA  PIC X.                                          00001130
           02 MLTIERSC  PIC X.                                          00001140
           02 MLTIERSP  PIC X.                                          00001150
           02 MLTIERSH  PIC X.                                          00001160
           02 MLTIERSV  PIC X.                                          00001170
           02 MLTIERSO  PIC X(15).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MFILLER1A      PIC X.                                     00001200
           02 MFILLER1C PIC X.                                          00001210
           02 MFILLER1P PIC X.                                          00001220
           02 MFILLER1H PIC X.                                          00001230
           02 MFILLER1V PIC X.                                          00001240
           02 MFILLER1O      PIC X(5).                                  00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MCTIERSA  PIC X.                                          00001270
           02 MCTIERSC  PIC X.                                          00001280
           02 MCTIERSP  PIC X.                                          00001290
           02 MCTIERSH  PIC X.                                          00001300
           02 MCTIERSV  PIC X.                                          00001310
           02 MCTIERSO  PIC X(5).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MDATEDEBA      PIC X.                                     00001340
           02 MDATEDEBC PIC X.                                          00001350
           02 MDATEDEBP PIC X.                                          00001360
           02 MDATEDEBH PIC X.                                          00001370
           02 MDATEDEBV PIC X.                                          00001380
           02 MDATEDEBO      PIC X(7).                                  00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDATEFINA      PIC X.                                     00001410
           02 MDATEFINC PIC X.                                          00001420
           02 MDATEFINP PIC X.                                          00001430
           02 MDATEFINH PIC X.                                          00001440
           02 MDATEFINV PIC X.                                          00001450
           02 MDATEFINO      PIC X(7).                                  00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MFILLER2A      PIC X.                                     00001480
           02 MFILLER2C PIC X.                                          00001490
           02 MFILLER2P PIC X.                                          00001500
           02 MFILLER2H PIC X.                                          00001510
           02 MFILLER2V PIC X.                                          00001520
           02 MFILLER2O      PIC X.                                     00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNUMRELA  PIC X.                                          00001550
           02 MNUMRELC  PIC X.                                          00001560
           02 MNUMRELP  PIC X.                                          00001570
           02 MNUMRELH  PIC X.                                          00001580
           02 MNUMRELV  PIC X.                                          00001590
           02 MNUMRELO  PIC X.                                          00001600
      * MESSAGE ERREUR                                                  00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLIBERRA  PIC X.                                          00001630
           02 MLIBERRC  PIC X.                                          00001640
           02 MLIBERRP  PIC X.                                          00001650
           02 MLIBERRH  PIC X.                                          00001660
           02 MLIBERRV  PIC X.                                          00001670
           02 MLIBERRO  PIC X(78).                                      00001680
      * CODE TRANSACTION                                                00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCODTRAA  PIC X.                                          00001710
           02 MCODTRAC  PIC X.                                          00001720
           02 MCODTRAP  PIC X.                                          00001730
           02 MCODTRAH  PIC X.                                          00001740
           02 MCODTRAV  PIC X.                                          00001750
           02 MCODTRAO  PIC X(4).                                       00001760
      * CICS DE TRAVAIL                                                 00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCICSA    PIC X.                                          00001790
           02 MCICSC    PIC X.                                          00001800
           02 MCICSP    PIC X.                                          00001810
           02 MCICSH    PIC X.                                          00001820
           02 MCICSV    PIC X.                                          00001830
           02 MCICSO    PIC X(5).                                       00001840
      * NETNAME                                                         00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNETNAMA  PIC X.                                          00001870
           02 MNETNAMC  PIC X.                                          00001880
           02 MNETNAMP  PIC X.                                          00001890
           02 MNETNAMH  PIC X.                                          00001900
           02 MNETNAMV  PIC X.                                          00001910
           02 MNETNAMO  PIC X(8).                                       00001920
      * CODE TERMINAL                                                   00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MSCREENA  PIC X.                                          00001950
           02 MSCREENC  PIC X.                                          00001960
           02 MSCREENP  PIC X.                                          00001970
           02 MSCREENH  PIC X.                                          00001980
           02 MSCREENV  PIC X.                                          00001990
           02 MSCREENO  PIC X(5).                                       00002000
                                                                                
