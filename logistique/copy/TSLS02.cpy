      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TLS02                                          *  00020001
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-LS02-LONG            PIC S9(3) COMP-3 VALUE 8.            00060001
       01  TS-LS02-DONNEES.                                             00070001
              10 TS-LS02-WSEL      PIC X(01).                           00080001
              10 TS-LS02-CFAM      PIC X(05).                           00090001
              10 TS-LS02-EXIST     PIC X(01).                           00100001
              10 TS-LS02-MAJ       PIC X(01).                           00110001
                                                                                
