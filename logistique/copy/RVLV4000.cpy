      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTLV40                      *        
      ******************************************************************        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLV4000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLV4000.                                                            
           10 LV40-NSOCDEPOT       PIC X(3).                                    
           10 LV40-NDEPOT          PIC X(3).                                    
           10 LV40-NMVT            PIC X(9).                                    
           10 LV40-CMVT            PIC X(2).                                    
           10 LV40-BLOCAGEI        PIC X(3).                                    
           10 LV40-BLOCAGEF        PIC X(3).                                    
           10 LV40-DMVT            PIC X(8).                                    
           10 LV40-HMVT            PIC X(6).                                    
           10 LV40-NRECQUAI        PIC X(7).                                    
           10 LV40-DJRECQUAI       PIC X(8).                                    
           10 LV40-NCODIC          PIC X(7).                                    
           10 LV40-QMVT            PIC S9(9)V USAGE COMP-3.                     
           10 LV40-NENTCDE         PIC X(5).                                    
           10 LV40-NDOSLM6         PIC X(15).                                   
           10 LV40-NINV            PIC X(15).                                   
           10 LV40-DTRANSFERT      PIC X(8).                                    
           10 LV40-CSTATUT         PIC X(2).                                    
           10 LV40-LCOMMENT        PIC X(20).                                   
           10 LV40-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLV4000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLV4000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-NSOCDEPOT-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-NSOCDEPOT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-NDEPOT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-NDEPOT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-NMVT-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-NMVT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-CMVT-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-CMVT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-BLOCAGEI-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-BLOCAGEI-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-BLOCAGEF-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-BLOCAGEF-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-DMVT-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-DMVT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-HMVT-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-HMVT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-NRECQUAI-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-NRECQUAI-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-DJRECQUAI-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-DJRECQUAI-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-QMVT-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-QMVT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-NENTCDE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-NENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-NDOSLM6-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-NDOSLM6-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-NINV-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-NINV-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-DTRANSFERT-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-DTRANSFERT-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-CSTATUT-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-CSTATUT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-LCOMMENT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 LV40-LCOMMENT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LV40-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 LV40-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
