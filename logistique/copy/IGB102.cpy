      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGB102 AU 08/09/2005  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,24,BI,A,                          *        
      *                           37,05,BI,A,                          *        
      *                           42,03,BI,A,                          *        
      *                           45,03,BI,A,                          *        
      *                           48,03,BI,A,                          *        
      *                           51,07,BI,A,                          *        
      *                           58,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGB102.                                                        
            05 NOMETAT-IGB102           PIC X(6) VALUE 'IGB102'.                
            05 RUPTURES-IGB102.                                                 
           10 IGB102-NSOCENTR           PIC X(03).                      007  003
           10 IGB102-NDEPOT             PIC X(03).                      010  003
           10 IGB102-DDESTOCK           PIC X(24).                      013  024
           10 IGB102-CSELART            PIC X(05).                      037  005
           10 IGB102-NSOCGRP            PIC X(03).                      042  003
           10 IGB102-NSOCIETE           PIC X(03).                      045  003
           10 IGB102-NLIEU              PIC X(03).                      048  003
           10 IGB102-NMUTATION          PIC X(07).                      051  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGB102-SEQUENCE           PIC S9(04) COMP.                058  002
      *--                                                                       
           10 IGB102-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGB102.                                                   
           10 IGB102-LFILIALE           PIC X(13).                      060  013
           10 IGB102-LLIEU              PIC X(20).                      073  020
           10 IGB102-QDEMANDEE          PIC S9(07)      COMP-3.         093  004
           10 IGB102-QVOLUME            PIC S9(04)V9(3) COMP-3.         097  004
            05 FILLER                      PIC X(412).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGB102-LONG           PIC S9(4)   COMP  VALUE +100.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGB102-LONG           PIC S9(4) COMP-5  VALUE +100.           
                                                                                
      *}                                                                        
