      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      *         DEFINITION DU FICHIER POUR ETATS IEF010, 011, 012               
      *                                                                         
      *                                                                         
       01       FEF010-DSECT.                                                   
           05   FEF010-CTRAIT         PIC X(05).                                
           05   FEF010-LTRAIT         PIC X(20).                                
           05   FEF010-CTIERS-AFF     PIC X(05).                                
           05   FEF010-LTIERS         PIC X(20).                                
           05   FEF010-CTIERS         PIC X(05).                                
           05   FEF010-NENTCDE        PIC X(05).                                
           05   FEF010-CTIERSELA      PIC X(04).                                
           05   FEF010-NENVOI         PIC X(07).                                
           05   FEF010-DENVOI         PIC X(08).                                
           05   FEF010-CRENDU         PIC X(05).                                
           05   FEF010-QTENV          PIC S9(5)    COMP-3.                      
           05   FEF010-MTPROV         PIC S9(7)V99 COMP-3.                      
           05   FEF010-PRMP           PIC S9(7)V99 COMP-3.                      
           05   FEF010-PSTDTTC        PIC S9(7)V99 COMP-3.                      
           05   FEF010-CTYPT          PIC X(01).                                
           05   FEF010-DEVREF         PIC X(03).                                
           05   FEF010-DEVEQU         PIC X(03).                                
           05   FEF010-PTAUX          PIC S9V9(6)   COMP-3.                     
           05   FILLER                PIC X(07).                                
      *                                   ----                                  
      *                                    120                                  
                                                                                
