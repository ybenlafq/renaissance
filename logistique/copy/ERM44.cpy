      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM44   D4497  Maquette                                    00000020
      ***************************************************************** 00000030
       01   ERM44I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCGROUPI  PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPAGEMAXI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLFAMI    PIC X(20).                                      00000330
           02 MLIGNEI OCCURS   14 TIMES .                               00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLAGREGL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLAGREGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLAGREGF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLAGREGI     PIC X(20).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXPOAL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCEXPOAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCEXPOAF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCEXPOAI     PIC X(10).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MLIBERRI  PIC X(78).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCODTRAI  PIC X(4).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCICSI    PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MNETNAMI  PIC X(8).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MSCREENI  PIC X(4).                                       00000620
      ***************************************************************** 00000630
      * SDF: ERM44   D4497  Maquette                                    00000640
      ***************************************************************** 00000650
       01   ERM44O REDEFINES ERM44I.                                    00000660
           02 FILLER    PIC X(12).                                      00000670
           02 FILLER    PIC X(2).                                       00000680
           02 MDATJOUA  PIC X.                                          00000690
           02 MDATJOUC  PIC X.                                          00000700
           02 MDATJOUP  PIC X.                                          00000710
           02 MDATJOUH  PIC X.                                          00000720
           02 MDATJOUV  PIC X.                                          00000730
           02 MDATJOUO  PIC X(10).                                      00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MTIMJOUA  PIC X.                                          00000760
           02 MTIMJOUC  PIC X.                                          00000770
           02 MTIMJOUP  PIC X.                                          00000780
           02 MTIMJOUH  PIC X.                                          00000790
           02 MTIMJOUV  PIC X.                                          00000800
           02 MTIMJOUO  PIC X(5).                                       00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MCGROUPA  PIC X.                                          00000830
           02 MCGROUPC  PIC X.                                          00000840
           02 MCGROUPP  PIC X.                                          00000850
           02 MCGROUPH  PIC X.                                          00000860
           02 MCGROUPV  PIC X.                                          00000870
           02 MCGROUPO  PIC X(5).                                       00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MPAGEA    PIC X.                                          00000900
           02 MPAGEC    PIC X.                                          00000910
           02 MPAGEP    PIC X.                                          00000920
           02 MPAGEH    PIC X.                                          00000930
           02 MPAGEV    PIC X.                                          00000940
           02 MPAGEO    PIC X(3).                                       00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MPAGEMAXA      PIC X.                                     00000970
           02 MPAGEMAXC PIC X.                                          00000980
           02 MPAGEMAXP PIC X.                                          00000990
           02 MPAGEMAXH PIC X.                                          00001000
           02 MPAGEMAXV PIC X.                                          00001010
           02 MPAGEMAXO      PIC X(3).                                  00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MCFAMA    PIC X.                                          00001040
           02 MCFAMC    PIC X.                                          00001050
           02 MCFAMP    PIC X.                                          00001060
           02 MCFAMH    PIC X.                                          00001070
           02 MCFAMV    PIC X.                                          00001080
           02 MCFAMO    PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MLFAMA    PIC X.                                          00001110
           02 MLFAMC    PIC X.                                          00001120
           02 MLFAMP    PIC X.                                          00001130
           02 MLFAMH    PIC X.                                          00001140
           02 MLFAMV    PIC X.                                          00001150
           02 MLFAMO    PIC X(20).                                      00001160
           02 MLIGNEO OCCURS   14 TIMES .                               00001170
             03 FILLER       PIC X(2).                                  00001180
             03 MLAGREGA     PIC X.                                     00001190
             03 MLAGREGC     PIC X.                                     00001200
             03 MLAGREGP     PIC X.                                     00001210
             03 MLAGREGH     PIC X.                                     00001220
             03 MLAGREGV     PIC X.                                     00001230
             03 MLAGREGO     PIC X(20).                                 00001240
             03 FILLER       PIC X(2).                                  00001250
             03 MCEXPOAA     PIC X.                                     00001260
             03 MCEXPOAC     PIC X.                                     00001270
             03 MCEXPOAP     PIC X.                                     00001280
             03 MCEXPOAH     PIC X.                                     00001290
             03 MCEXPOAV     PIC X.                                     00001300
             03 MCEXPOAO     PIC X(10).                                 00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLIBERRA  PIC X.                                          00001330
           02 MLIBERRC  PIC X.                                          00001340
           02 MLIBERRP  PIC X.                                          00001350
           02 MLIBERRH  PIC X.                                          00001360
           02 MLIBERRV  PIC X.                                          00001370
           02 MLIBERRO  PIC X(78).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCODTRAA  PIC X.                                          00001400
           02 MCODTRAC  PIC X.                                          00001410
           02 MCODTRAP  PIC X.                                          00001420
           02 MCODTRAH  PIC X.                                          00001430
           02 MCODTRAV  PIC X.                                          00001440
           02 MCODTRAO  PIC X(4).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCICSA    PIC X.                                          00001470
           02 MCICSC    PIC X.                                          00001480
           02 MCICSP    PIC X.                                          00001490
           02 MCICSH    PIC X.                                          00001500
           02 MCICSV    PIC X.                                          00001510
           02 MCICSO    PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNETNAMA  PIC X.                                          00001540
           02 MNETNAMC  PIC X.                                          00001550
           02 MNETNAMP  PIC X.                                          00001560
           02 MNETNAMH  PIC X.                                          00001570
           02 MNETNAMV  PIC X.                                          00001580
           02 MNETNAMO  PIC X(8).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MSCREENA  PIC X.                                          00001610
           02 MSCREENC  PIC X.                                          00001620
           02 MSCREENP  PIC X.                                          00001630
           02 MSCREENH  PIC X.                                          00001640
           02 MSCREENV  PIC X.                                          00001650
           02 MSCREENO  PIC X(4).                                       00001660
                                                                                
