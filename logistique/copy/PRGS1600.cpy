      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE GS1600                       
      ******************************************************************        
      *                                                                         
       CLEF-GS1600             SECTION.                                         
      *                                                                         
           MOVE 'RVGS1600          '       TO   TABLE-NAME.                     
           MOVE 'GS1600'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-GS1600. EXIT.                                                   
                EJECT                                                           
                                                                                
