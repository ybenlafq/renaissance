      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TFL42 (TFL30 -> MENU)    TR: FL30  *    00002201
      *          GESTION DES PROFILS D'AFFILIATION                 *    00002301
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *  TRANSACTION FL42 : EXCEPTION DES MARQUES                  *    00411001
      *                                                                 00412000
          02 COMM-FL42-APPLI REDEFINES COMM-FL30-APPLI.                 00420001
      *------------------------------ ZONE DONNEES TFL42                00510001
             03 COMM-FL42-DONNEES-1-TFL42.                              00520001
      *------------------------------ CODE FONCTION                     00550001
                04 COMM-FL42-FONCT             PIC X(3).                00560001
      *------------------------------ CODE PROFIL                       00561001
                04 COMM-FL42-CPROAFF           PIC X(5).                00562001
      *------------------------------ LIBELLE PROFIL                    00570001
                04 COMM-FL42-LPROAFF           PIC X(20).               00580001
      *------------------------------ CODE FAMILLE                      00590001
                04 COMM-FL42-CEFAMIL           PIC X(5).                00600001
      *------------------------------ LIBELLE FAMILLE                   00610001
                04 COMM-FL42-LEFAMIL           PIC X(20).               00620001
      *------------------------------ DERNIER IND-TS                    00742401
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL42-PAGSUI            PIC S9(4) COMP.          00742501
      *--                                                                       
                04 COMM-FL42-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN IND-TS                     00742601
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL42-PAGENC            PIC S9(4) COMP.          00742701
      *--                                                                       
                04 COMM-FL42-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00742801
                04 COMM-FL42-NUMPAG            PIC 9(3).                00742901
      *------------------------------ NOMBRE DE PAGE                    00743001
                04 COMM-FL42-NBRPAG            PIC 9(3).                00743101
      *------------------------------ INDICATEUR FIN DE TABLE           00743201
                04 COMM-FL42-INDPAG            PIC 9.                   00743301
      *------------------------------ ZONE MODIFICATION TS              00743401
                04 COMM-FL42-MODIF             PIC 9(1).                00743501
      *------------------------------ ZONE CONFIRMATION PF4             00743601
                04 COMM-FL42-CONF-PF4          PIC 9(1).                00743701
      *------------------------------ OK MAJ SUR RTFL06 => VALID = '1'  00743801
                04 COMM-FL42-VALID-RTFL06   PIC  9.                     00743901
      *------------------------------ TABLE N� TS PR PAGINATION         00744201
                04 COMM-FL42-TABTS          OCCURS 100.                 00744301
      *------------------------------ 1ERS IND TS DES PAGES <=> FAMILLE 00744401
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               06 COMM-FL42-NUMTS          PIC S9(4) COMP.       00744501
      *--                                                                       
                      06 COMM-FL42-NUMTS          PIC S9(4) COMP-5.             
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS               00744601
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL42-IND-RECH-TS    PIC S9(4) COMP.             00745001
      *--                                                                       
                04 COMM-FL42-IND-RECH-TS    PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND TS MAX                        00746001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FL42-IND-TS-MAX     PIC S9(4) COMP.             00747001
      *--                                                                       
                04 COMM-FL42-IND-TS-MAX     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ ZONE DONNEES 2 TFL42              00760001
             03 COMM-FL42-DONNEES-2-TFL42.                              00761001
      *------------------------------ CODES D'AFFILIATION PAR DEFAUT    00762001
                04 COMM-FL42-ENTAFF-DF      OCCURS 5.                   00763001
      *------------------------------ CODE SOCIETE D'AFFILIATION DEFAUT 00764001
                   05 COMM-FL42-CSOCAFF-DF  PIC X(03).                  00765001
      *------------------------------ CODE ENTREPOT D'AFFILIATION DEFAUT00766001
                   05 COMM-FL42-CDEPAFF-DF  PIC X(03).                  00767001
      *------------------------------ ZONE FAMILLES PAGE                00770001
                04 COMM-FL42-TABFAM.                                    00780001
      *------------------------------ ZONE LIGNES FAMILLES              00790001
                   05 COMM-FL42-LIGFAM  OCCURS 15.                      00800001
      *------------------------------ CODIC                             00801001
                      06 COMM-FL42-CDAC           PIC X(1).             00802001
      *------------------------------ CODIC                             00810001
                      06 COMM-FL42-CMAR           PIC X(5).             00820001
      *------------------------------ LIBELLE CODIC                     00830001
                      06 COMM-FL42-LMAR           PIC X(20).            00840001
      *------------------------------ CODES D'AFFILIATION               00850001
                      06 COMM-FL42-ENTAFF         OCCURS 5.             00860001
      *------------------------------ CODE SOCIETE D'AFFILIATION        00870001
                         07 COMM-FL42-CSOCAFF     PIC X(03).            00880001
      *------------------------------ CODE ENTREPOT D'AFFILIATION       00890001
                         07 COMM-FL42-CDEPAFF     PIC X(03).            00900001
      *------------------------------ ZONE DONNEES 3 TFL42              01650001
             03 COMM-FL42-DONNEES-3-TFL42.                              01651001
      *------------------------------ ZONES SWAP                        01670001
                   05 COMM-FL42-ATTR           PIC X OCCURS 500.        01680001
      *------------------------------ ZONE LIBRE                        01710001
             03 COMM-FL42-LIBRE          PIC X(2067).                   01720001
      ***************************************************************** 02170001
                                                                                
