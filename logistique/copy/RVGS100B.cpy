      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGS100B                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS100B                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS1000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS1000.                                                            
      *}                                                                        
           02  GS10B-NSOCDEPOT                                                  
               PIC X(0003).                                                     
           02  GS10B-NDEPOT                                                     
               PIC X(0003).                                                     
           02  GS10B-NCODIC                                                     
               PIC X(0007).                                                     
           02  GS10B-WSTOCKMASQ                                                 
               PIC X(0001).                                                     
           02  GS10B-NSSLIEU                                                    
               PIC X(0003).                                                     
           02  GS10B-QSTOCK                                                     
               PIC S9(5) COMP-3.                                                
           02  GS10B-QSTOCKRES                                                  
               PIC S9(5) COMP-3.                                                
           02  GS10B-DMAJSTOCK                                                  
               PIC X(0008).                                                     
           02  GS10B-WARTINC                                                    
               PIC X(0001).                                                     
           02  GS10B-DSYST                                                      
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGS1000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS1000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS1000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS10B-NSOCDEPOT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS10B-NSOCDEPOT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS10B-NDEPOT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS10B-NDEPOT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS10B-NCODIC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS10B-NCODIC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS10B-WSTOCKMASQ-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS10B-WSTOCKMASQ-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS10B-NSSLIEU-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS10B-NSSLIEU-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS10B-QSTOCK-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS10B-QSTOCK-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS10B-QSTOCKRES-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS10B-QSTOCKRES-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS10B-DMAJSTOCK-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS10B-DMAJSTOCK-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS10B-WARTINC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS10B-WARTINC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS10B-DSYST-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GS10B-DSYST-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
