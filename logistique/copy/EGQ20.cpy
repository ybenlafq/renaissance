      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGQ20   EGQ20                                              00000020
      ***************************************************************** 00000030
       01   EGQ20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDELVL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MDELVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDELVF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDELVI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEQUIL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCEQUIL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEQUIF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCEQUII   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELQUIL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLIBELQUIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELQUIF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIBELQUII     PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBJOURL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLIBJOURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBJOURF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLIBJOURI      PIC X(2).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDATEI    PIC X(8).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROFL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCPROFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPROFF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCPROFI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELPROFL    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLIBELPROFL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLIBELPROFF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLIBELPROFI    PIC X(20).                                 00000450
           02 M106I OCCURS   14 TIMES .                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCZONEL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCZONEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCZONEF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCZONEI      PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELZONEL  COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLIBELZONEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLIBELZONEF  PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLIBELZONEI  PIC X(20).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MACTI   PIC X.                                          00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPLAGEL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MPLAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPLAGEF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MPLAGEI      PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHHL    COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MHHL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MHHF    PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MHHI    PIC X(2).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMML    COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MMML COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MMMF    PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MMMI    PIC X(2).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPOIDSL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MPOIDSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPOIDSF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MPOIDSI      PIC X(6).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(15).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(58).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: EGQ20   EGQ20                                              00001000
      ***************************************************************** 00001010
       01   EGQ20O REDEFINES EGQ20I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MWFONCA   PIC X.                                          00001190
           02 MWFONCC   PIC X.                                          00001200
           02 MWFONCP   PIC X.                                          00001210
           02 MWFONCH   PIC X.                                          00001220
           02 MWFONCV   PIC X.                                          00001230
           02 MWFONCO   PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MDELVA    PIC X.                                          00001260
           02 MDELVC    PIC X.                                          00001270
           02 MDELVP    PIC X.                                          00001280
           02 MDELVH    PIC X.                                          00001290
           02 MDELVV    PIC X.                                          00001300
           02 MDELVO    PIC X(3).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MCEQUIA   PIC X.                                          00001330
           02 MCEQUIC   PIC X.                                          00001340
           02 MCEQUIP   PIC X.                                          00001350
           02 MCEQUIH   PIC X.                                          00001360
           02 MCEQUIV   PIC X.                                          00001370
           02 MCEQUIO   PIC X(5).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MLIBELQUIA     PIC X.                                     00001400
           02 MLIBELQUIC     PIC X.                                     00001410
           02 MLIBELQUIP     PIC X.                                     00001420
           02 MLIBELQUIH     PIC X.                                     00001430
           02 MLIBELQUIV     PIC X.                                     00001440
           02 MLIBELQUIO     PIC X(20).                                 00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MLIBJOURA      PIC X.                                     00001470
           02 MLIBJOURC PIC X.                                          00001480
           02 MLIBJOURP PIC X.                                          00001490
           02 MLIBJOURH PIC X.                                          00001500
           02 MLIBJOURV PIC X.                                          00001510
           02 MLIBJOURO      PIC X(2).                                  00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MDATEA    PIC X.                                          00001540
           02 MDATEC    PIC X.                                          00001550
           02 MDATEP    PIC X.                                          00001560
           02 MDATEH    PIC X.                                          00001570
           02 MDATEV    PIC X.                                          00001580
           02 MDATEO    PIC X(8).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MCPROFA   PIC X.                                          00001610
           02 MCPROFC   PIC X.                                          00001620
           02 MCPROFP   PIC X.                                          00001630
           02 MCPROFH   PIC X.                                          00001640
           02 MCPROFV   PIC X.                                          00001650
           02 MCPROFO   PIC X(5).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLIBELPROFA    PIC X.                                     00001680
           02 MLIBELPROFC    PIC X.                                     00001690
           02 MLIBELPROFP    PIC X.                                     00001700
           02 MLIBELPROFH    PIC X.                                     00001710
           02 MLIBELPROFV    PIC X.                                     00001720
           02 MLIBELPROFO    PIC X(20).                                 00001730
           02 M106O OCCURS   14 TIMES .                                 00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MCZONEA      PIC X.                                     00001760
             03 MCZONEC PIC X.                                          00001770
             03 MCZONEP PIC X.                                          00001780
             03 MCZONEH PIC X.                                          00001790
             03 MCZONEV PIC X.                                          00001800
             03 MCZONEO      PIC X(5).                                  00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MLIBELZONEA  PIC X.                                     00001830
             03 MLIBELZONEC  PIC X.                                     00001840
             03 MLIBELZONEP  PIC X.                                     00001850
             03 MLIBELZONEH  PIC X.                                     00001860
             03 MLIBELZONEV  PIC X.                                     00001870
             03 MLIBELZONEO  PIC X(20).                                 00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MACTA   PIC X.                                          00001900
             03 MACTC   PIC X.                                          00001910
             03 MACTP   PIC X.                                          00001920
             03 MACTH   PIC X.                                          00001930
             03 MACTV   PIC X.                                          00001940
             03 MACTO   PIC X.                                          00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MPLAGEA      PIC X.                                     00001970
             03 MPLAGEC PIC X.                                          00001980
             03 MPLAGEP PIC X.                                          00001990
             03 MPLAGEH PIC X.                                          00002000
             03 MPLAGEV PIC X.                                          00002010
             03 MPLAGEO      PIC X(8).                                  00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MHHA    PIC X.                                          00002040
             03 MHHC    PIC X.                                          00002050
             03 MHHP    PIC X.                                          00002060
             03 MHHH    PIC X.                                          00002070
             03 MHHV    PIC X.                                          00002080
             03 MHHO    PIC X(2).                                       00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MMMA    PIC X.                                          00002110
             03 MMMC    PIC X.                                          00002120
             03 MMMP    PIC X.                                          00002130
             03 MMMH    PIC X.                                          00002140
             03 MMMV    PIC X.                                          00002150
             03 MMMO    PIC X(2).                                       00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MPOIDSA      PIC X.                                     00002180
             03 MPOIDSC PIC X.                                          00002190
             03 MPOIDSP PIC X.                                          00002200
             03 MPOIDSH PIC X.                                          00002210
             03 MPOIDSV PIC X.                                          00002220
             03 MPOIDSO      PIC X(6).                                  00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MZONCMDA  PIC X.                                          00002250
           02 MZONCMDC  PIC X.                                          00002260
           02 MZONCMDP  PIC X.                                          00002270
           02 MZONCMDH  PIC X.                                          00002280
           02 MZONCMDV  PIC X.                                          00002290
           02 MZONCMDO  PIC X(15).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(58).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
