      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE DELIV DELAI APPROVISIONNEMENT          *        
      *----------------------------------------------------------------*        
       01  RVDELIV .                                                            
           05  DELIV-CTABLEG2    PIC X(15).                                     
           05  DELIV-CTABLEG2-REDEF REDEFINES DELIV-CTABLEG2.                   
               10  DELIV-ORIGDEST        PIC X(06).                             
               10  DELIV-CMODDEL         PIC X(03).                             
               10  DELIV-NSEQ            PIC X(02).                             
           05  DELIV-WTABLEG     PIC X(80).                                     
           05  DELIV-WTABLEG-REDEF  REDEFINES DELIV-WTABLEG.                    
               10  DELIV-WFLAG           PIC X(01).                             
               10  DELIV-JJMM            PIC X(04).                             
               10  DELIV-DELAI           PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVDELIV-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DELIV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  DELIV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DELIV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  DELIV-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
