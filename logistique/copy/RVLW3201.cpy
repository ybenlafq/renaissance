      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      *   COPY DE LA TABLE RVLW3201                                             
      ******************************************************************        
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLW3201                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW3201.                                                            
           02 LW32-NSOCDEPOT       PIC X(3).                                    
           02 LW32-NDEPOT          PIC X(3).                                    
           02 LW32-NCDE            PIC X(7).                                    
           02 LW32-NCODIC          PIC X(7).                                    
           02 LW32-DLIVRAISON      PIC X(8).                                    
           02 LW32-NRECQUAI        PIC X(7).                                    
           02 LW32-DJRECQUAI       PIC X(8).                                    
           02 LW32-QRECQUAI        PIC S9(9)V USAGE COMP-3.                     
           02 LW32-DSYST           PIC S9(13)V USAGE COMP-3.                    
           02 LW32-NDOSLM7         PIC X(15).                                   
           02 LW32-NBL             PIC X(10).                                   
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLW3201                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW3201-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LW32-NSOCDEPOT-F                                                  
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 LW32-NSOCDEPOT-F                                                  
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LW32-NDEPOT-F                                                     
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 LW32-NDEPOT-F                                                     
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LW32-NCDE-F                                                       
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 LW32-NCDE-F                                                       
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LW32-NCODIC-F                                                     
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 LW32-NCODIC-F                                                     
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LW32-DLIVRAISON-F                                                 
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 LW32-DLIVRAISON-F                                                 
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LW32-NRECQUAI-F                                                   
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 LW32-NRECQUAI-F                                                   
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LW32-DJRECQUAI-F                                                  
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 LW32-DJRECQUAI-F                                                  
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LW32-QRECQUAI-F                                                   
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 LW32-QRECQUAI-F                                                   
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LW32-DSYST-F                                                      
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 LW32-DSYST-F                                                      
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LW32-NDOSLM7-F                                                    
      *       PIC S9(4) COMP.                                                   
      *--                                                                       
           02 LW32-NDOSLM7-F                                                    
              PIC S9(4) COMP-5.                                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LW32-NBL-F                                                        
      *       PIC S9(4) COMP.                                                   
      *                                                                         
      *--                                                                       
           02 LW32-NBL-F                                                        
              PIC S9(4) COMP-5.                                                 
                                                                                
      *}                                                                        
