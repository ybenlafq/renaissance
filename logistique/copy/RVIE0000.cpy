      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVIE0000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIE0000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIE0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIE0000.                                                            
      *}                                                                        
           02  IE00-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  IE00-NDEPOT                                                      
               PIC X(0003).                                                     
           02  IE00-NFICHE                                                      
               PIC X(0005).                                                     
           02  IE00-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  IE00-NCODIC1                                                     
               PIC X(0007).                                                     
           02  IE00-NCODIC3                                                     
               PIC X(0007).                                                     
           02  IE00-CEMP11                                                      
               PIC X(0002).                                                     
           02  IE00-CEMP21                                                      
               PIC X(0002).                                                     
           02  IE00-CEMP31                                                      
               PIC X(0003).                                                     
           02  IE00-CTYPEMPL1                                                   
               PIC X(0001).                                                     
           02  IE00-CEMP12                                                      
               PIC X(0002).                                                     
           02  IE00-CEMP22                                                      
               PIC X(0002).                                                     
           02  IE00-CEMP32                                                      
               PIC X(0003).                                                     
           02  IE00-CTYPEMPL2                                                   
               PIC X(0001).                                                     
           02  IE00-CEMP13                                                      
               PIC X(0002).                                                     
           02  IE00-CEMP23                                                      
               PIC X(0002).                                                     
           02  IE00-CEMP33                                                      
               PIC X(0003).                                                     
           02  IE00-CTYPEMPL3                                                   
               PIC X(0001).                                                     
           02  IE00-QSTOCKU1                                                    
               PIC S9(5) COMP-3.                                                
           02  IE00-QNBCOL11                                                    
               PIC S9(3) COMP-3.                                                
           02  IE00-QSTOCKC11                                                   
               PIC S9(3) COMP-3.                                                
           02  IE00-QNBCOL21                                                    
               PIC S9(3) COMP-3.                                                
           02  IE00-QSTOCKC21                                                   
               PIC S9(3) COMP-3.                                                
           02  IE00-QNBCOL31                                                    
               PIC S9(3) COMP-3.                                                
           02  IE00-QSTOCKC31                                                   
               PIC S9(3) COMP-3.                                                
           02  IE00-QSTOCKU2                                                    
               PIC S9(5) COMP-3.                                                
           02  IE00-QNBCOL12                                                    
               PIC S9(3) COMP-3.                                                
           02  IE00-QSTOCKC12                                                   
               PIC S9(3) COMP-3.                                                
           02  IE00-QNBCOL22                                                    
               PIC S9(3) COMP-3.                                                
           02  IE00-QSTOCKC22                                                   
               PIC S9(3) COMP-3.                                                
           02  IE00-QNBCOL32                                                    
               PIC S9(3) COMP-3.                                                
           02  IE00-QSTOCKC32                                                   
               PIC S9(3) COMP-3.                                                
           02  IE00-QSTOCKU3                                                    
               PIC S9(5) COMP-3.                                                
           02  IE00-QNBCOL13                                                    
               PIC S9(3) COMP-3.                                                
           02  IE00-QSTOCKC13                                                   
               PIC S9(3) COMP-3.                                                
           02  IE00-QNBCOL23                                                    
               PIC S9(3) COMP-3.                                                
           02  IE00-QSTOCKC23                                                   
               PIC S9(3) COMP-3.                                                
           02  IE00-QNBCOL33                                                    
               PIC S9(3) COMP-3.                                                
           02  IE00-QSTOCKC33                                                   
               PIC S9(3) COMP-3.                                                
           02  IE00-CFAM                                                        
               PIC X(0005).                                                     
           02  IE00-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  IE00-CMARQ                                                       
               PIC X(0005).                                                     
           02  IE00-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  IE00-WARTOK                                                      
               PIC X(0001).                                                     
           02  IE00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVIE0000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIE0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIE0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-NFICHE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-NFICHE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-NCODIC1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-NCODIC1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-NCODIC3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-NCODIC3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CEMP11-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CEMP11-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CEMP21-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CEMP21-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CEMP31-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CEMP31-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CTYPEMPL1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CTYPEMPL1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CEMP12-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CEMP12-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CEMP22-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CEMP22-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CEMP32-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CEMP32-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CTYPEMPL2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CTYPEMPL2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CEMP13-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CEMP13-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CEMP23-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CEMP23-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CEMP33-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CEMP33-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CTYPEMPL3-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CTYPEMPL3-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKU1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKU1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QNBCOL11-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QNBCOL11-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKC11-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKC11-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QNBCOL21-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QNBCOL21-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKC21-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKC21-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QNBCOL31-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QNBCOL31-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKC31-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKC31-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKU2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKU2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QNBCOL12-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QNBCOL12-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKC12-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKC12-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QNBCOL22-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QNBCOL22-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKC22-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKC22-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QNBCOL32-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QNBCOL32-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKC32-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKC32-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKU3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKU3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QNBCOL13-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QNBCOL13-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKC13-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKC13-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QNBCOL23-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QNBCOL23-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKC23-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKC23-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QNBCOL33-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QNBCOL33-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-QSTOCKC33-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-QSTOCKC33-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-WARTOK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE00-WARTOK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  IE00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
