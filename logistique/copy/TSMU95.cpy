      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *       TS SPECIFIQUE  OPTIMISATION TRANSPORT TMU95            *          
      ****************************************************************          
       01  TS-MU95-LONG             PIC S9(4) COMP-3 VALUE +75.                 
       01  TS-MU95-DATA.                                                        
           05 TS-MU95-LIGNE.                                                    
              10 TS-MU95-NMAG       PIC X(3).                                   
              10 TS-MU95-NVENTE     PIC X(7).                                   
              10 TS-MU95-DDELIV     PIC X(8).                                   
              10 TS-MU95-MUTORIG    PIC X(8).                                   
              10 TS-MU95-INDICE     PIC X(7).                                   
              10 TS-MU95-NCODIC     PIC X(7).                                   
              10 TS-MU95-QLIGNES    PIC 9(5).                                   
              10 TS-MU95-QPIECES    PIC 9(5).                                   
              10 TS-MU95-QVOLUME    PIC S9(3)V999    COMP-3.                    
              10 TS-MU95-MESSAGE    PIC X(20).                                  
              10 TS-MU95-ACTION     PIC X(1).                                   
                                                                                
