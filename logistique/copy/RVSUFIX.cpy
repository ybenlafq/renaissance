      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SUFIX CODE SOCIETE -> SUFFIXE          *        
      *----------------------------------------------------------------*        
       01  RVSUFIX.                                                             
           05  SUFIX-CTABLEG2    PIC X(15).                                     
           05  SUFIX-CTABLEG2-REDEF REDEFINES SUFIX-CTABLEG2.                   
               10  SUFIX-NSOC            PIC X(03).                             
           05  SUFIX-WTABLEG     PIC X(80).                                     
           05  SUFIX-WTABLEG-REDEF  REDEFINES SUFIX-WTABLEG.                    
               10  SUFIX-SUFIX           PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVSUFIX-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SUFIX-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SUFIX-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SUFIX-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SUFIX-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
