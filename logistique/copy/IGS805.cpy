      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGS805 AU 07/02/2005  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,03,PD,A,                          *        
      *                           21,05,BI,A,                          *        
      *                           26,20,BI,A,                          *        
      *                           46,07,BI,A,                          *        
      *                           53,03,BI,A,                          *        
      *                           56,08,BI,A,                          *        
      *                           64,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGS805.                                                        
            05 NOMETAT-IGS805           PIC X(6) VALUE 'IGS805'.                
            05 RUPTURES-IGS805.                                                 
           10 IGS805-NSOCIETE           PIC X(03).                      007  003
           10 IGS805-NDEPOT             PIC X(03).                      010  003
           10 IGS805-CREC               PIC X(05).                      013  005
           10 IGS805-WSEQFAM            PIC S9(05)      COMP-3.         018  003
           10 IGS805-CMARQ              PIC X(05).                      021  005
           10 IGS805-LREFFOURN          PIC X(20).                      026  020
           10 IGS805-NCODIC             PIC X(07).                      046  007
           10 IGS805-NDEPOTL            PIC X(03).                      053  003
           10 IGS805-DLIVRAISON         PIC X(08).                      056  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGS805-SEQUENCE           PIC S9(04) COMP.                064  002
      *--                                                                       
           10 IGS805-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGS805.                                                   
           10 IGS805-CAPPRO             PIC X(05).                      066  005
           10 IGS805-CFAM               PIC X(05).                      071  005
           10 IGS805-NSOCDEPOT          PIC X(03).                      076  003
           10 IGS805-WNOUVEAU           PIC X(01).                      079  001
           10 IGS805-WQNBJOURS          PIC X(01).                      080  001
           10 IGS805-QCDE               PIC S9(07)      COMP-3.         081  004
           10 IGS805-QCDEPREL           PIC S9(07)      COMP-3.         085  004
           10 IGS805-QNBJOURS           PIC S9(05)V9(2) COMP-3.         089  004
           10 IGS805-QSTOCK             PIC S9(07)      COMP-3.         093  004
           10 IGS805-QSTOCKDEP          PIC S9(07)      COMP-3.         097  004
           10 IGS805-QSTOCKGEN          PIC S9(07)      COMP-3.         101  004
           10 IGS805-TOTALGENE          PIC S9(07)      COMP-3.         105  004
            05 FILLER                      PIC X(404).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGS805-LONG           PIC S9(4)   COMP  VALUE +108.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGS805-LONG           PIC S9(4) COMP-5  VALUE +108.           
                                                                                
      *}                                                                        
