      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010003
      * COMMAREA SPECIFIQUE PRG TIE32 (TIE30 -> MENU)    TR: IE00  *    00020003
      *               INVENTAIRE ENTREPOT                          *    00030003
      *                                                                 00040003
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3598  00050003
      *                                                                 00060003
      *    TRANSACTION IE32 : REGULARISATION POST-INVENTAIRE       *    00070003
      *                                                                 00080003
          02  FILLER         REDEFINES COMM-IS00-APPLI.                 00080004
          03 COMM-IS33-APPLI.                                           00090003
      *------------------------------ ZONE INDICATEUR PHASE TIS33       00100003
             04 COMM-IS33-PHASE               PIC  9(01).               00140015
           88 SAISIE-ENTETE                   VALUE 1.                  00141006
           88 SAISIE-CORPS                    VALUE 2.                  00142006
           88 CONFIRMATION                    VALUE 3.                  00143006
      *------------------------------ ZONE ECRAN TIE32                  00150003
             04 COMM-IS33-ECRAN.                                        00160003
                05 COMM-IS33-CMODSTOCK        PIC  X(05).               00170003
                05 COMM-IS33-CEMP1            PIC  X(02).               00180003
                05 COMM-IS33-CEMP2            PIC  X(02).               00190001
                05 COMM-IS33-NSSLIEU          PIC  X(01).               00200001
                05 COMM-IS33-CLIEUTRT         PIC  X(05).               00200001
                05 COMM-IS33-NCODIC           PIC  X(07).               00210001
                05 COMM-IS33-LREFFOURN        PIC  X(20).               00220001
                05 COMM-IS33-CFAM             PIC  X(05).               00230001
                05 COMM-IS33-LFAM             PIC  X(20).               00240001
                05 COMM-IS33-CMARQ            PIC  X(05).               00250001
                05 COMM-IS33-LMARQ            PIC  X(20).               00260001
                05 COMM-IS33-LEMBALLAGE       PIC  X(50).               00270001
                05 COMM-IS33-QTEINIT.                                   00280015
                   10 COMM-IS33-QTEINIT-NUM   PIC  9(05).               00281013
                05 COMM-IS33-QTECORR.                                   00290015
                   10 COMM-IS33-QTECORR-NUM   PIC  9(05).               00291014
                05 COMM-IS33-QTECODIC.                                  00300015
                   10 COMM-IS33-QTECODIC-NUM  PIC  9(05).               00301013
      *------------------------------ ZONE DONNEES TIE32                00310001
             04 COMM-IS33-DONNEES.                                      00320001
                05 COMM-IS33-TYPE-EMPLACEMENT PIC  9(01).               00350305
           88 EMPLACEMENT-REEL                VALUE 0.                  00350405
           88 EMPLACEMENT-FICTIF              VALUE 1 THRU 3.           00350505
032006     88 EMPLACEMENT-PRET                VALUE 4 THRU 7.           00350706
           88 EMPLACEMENT-FICTIF-ENTREPOT     VALUE 1.                  00350605
           88 EMPLACEMENT-FICTIF-FOURNISSEUR  VALUE 2.                  00350705
           88 EMPLACEMENT-FICTIF-DIVERS       VALUE 3.                  00350706
032006     88 EMPLACEMENT-PRET-F              VALUE 4.                  00350706
032006     88 EMPLACEMENT-PRET-P              VALUE 5.                  00350706
032006     88 EMPLACEMENT-PRET-T              VALUE 6.                  00350706
                05 COMM-IS33-TABLE            PIC  9(01).               00350801
           88 TRAITEMENT-SOL                  VALUE 1.                  00350911
           88 TRAITEMENT-RACK                 VALUE 2 THRU 3.           00351012
           88 TRAITEMENT-RACK-STOCKAGE        VALUE 2.                  00352012
           88 TRAITEMENT-RACK-PICKING         VALUE 3.                  00360011
           88 TRAITEMENT-VRAC                 VALUE 4.                  00370011
                05 COMM-IS33-INSERT-RTIE05    PIC  9(01).               00390001
                05 COMM-IS33-INSERT-RTGS10    PIC  9(01).               00400002
                05 COMM-IS33-WSEQFAM          PIC S9(05) COMP-3.        00490001
                05 COMM-IS33-QLARGEUR         PIC S9(03) COMP-3.        00500001
                05 COMM-IS33-QPROFONDEUR      PIC S9(03) COMP-3.        00510001
                05 COMM-IS33-QHAUTEUR         PIC S9(03) COMP-3.        00520001
                05 COMM-IS33-CCONTENEUR       PIC  X(01).               00521010
      *------------------------------ ZONE TABLE INTERNE                00560017
             04 COMM-IS33-TANLE-INTERNE.                                00560117
                05 COMM-IS33-TABLE-HS-FICTIF.                           00561017
                   10 COMM-IS33-RTIE30        OCCURS 30.                00562017
                      15 COMM-IS33-ZONE       PIC  X(02).               00563017
                      15 COMM-IS33-SECTEUR    PIC  X(02).               00564017
                      15 COMM-IS33-TYPE       PIC  X(05).               00565018
      ***************************************************************** 00580000
                                                                                
