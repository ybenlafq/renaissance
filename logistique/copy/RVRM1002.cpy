      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM1001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM1001                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1001.                                                            
           02  RM10-CGROUP                                                      
               PIC X(0005).                                                     
           02  RM10-CFAM                                                        
               PIC X(0005).                                                     
           02  RM10-W20                                                         
               PIC X(0001).                                                     
           02  RM10-W80                                                         
               PIC X(0001).                                                     
           02  RM10-WTXEMP                                                      
               PIC X(0001).                                                     
           02  RM10-QSEUILEXC                                                   
               PIC S9(7) COMP-3.                                                
           02  RM10-DEFFETD                                                     
               PIC X(0008).                                                     
           02  RM10-DFINEFFETD                                                  
               PIC X(0008).                                                     
           02  RM10-QINDISPO                                                    
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM10-QPONDERATION                                                
               PIC S9(1)V9(0002) COMP-3.                                        
           02  RM10-NINDICE                                                     
               PIC X(0001).                                                     
           02  RM10-QSEUIL8S                                                    
               PIC S9(7) COMP-3.                                                
           02  RM10-QPVM                                                        
               PIC S9(1)V9(0002) COMP-3.                                        
           02  RM10-QFREQUENCE                                                  
               PIC S9(1)V9(0002) COMP-3.                                        
           02  RM10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  RM10-NTABLE                                                      
               PIC X(0002).                                                     
           02  RM10-NTABLESA                                                    
               PIC X(0002).                                                     
           02  RM10-QLSMAX                                                      
               PIC S9(5) COMP-3.                                                
           02  RM10-QEXPOMAX                                                    
               PIC S9(5) COMP-3.                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM1001                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-CGROUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-CGROUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-W20-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-W20-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-W80-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-W80-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-WTXEMP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-WTXEMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-QSEUILEXC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-QSEUILEXC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-DEFFETD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-DEFFETD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-DFINEFFETD-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-DFINEFFETD-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-QINDISPO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-QINDISPO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-QPONDERATION-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-QPONDERATION-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-NINDICE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-NINDICE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-QSEUIL8S-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-QSEUIL8S-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-QPVM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-QPVM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-QFREQUENCE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-QFREQUENCE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-NTABLE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-NTABLE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-NTABLESA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-NTABLESA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-QLSMAX-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-QLSMAX-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM10-QEXPOMAX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM10-QEXPOMAX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
