      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FGP869  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : REAPPROVISIONNEMENT FOURNISSEUR             *         
      *                                                               *         
      *    FGP869       : FICHIER DE TRAVAIL                          *         
      *                                                               *         
      *    STRUCTURE    :   SAM                                       *         
      *    LONGUEUR ENR.:  676 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  :FGPXXX:-ENREG.                                                      
      *                                                         1    5          
        02 :FGPXXX:-DONNEE-ENTETE.                                              
           05  :FGPXXX:-CHEFPROD              PIC  X(05).                       
      *                                                         6   20          
           05  :FGPXXX:-LCHEFPROD             PIC  X(20).                       
      *                                                        26   15          
           05  :FGPXXX:-LIBELLE               PIC  X(15).                       
      *                                                        41    5          
           05  :FGPXXX:-WSEQFAM               PIC  X(05).                       
      *                                                        46    5          
           05  :FGPXXX:-CFAM                  PIC  X(05).                       
      *                                                        51   20          
           05  :FGPXXX:-LFAM                  PIC  X(20).                       
      *                                                        71    2          
           05  :FGPXXX:-NAGREGATED            PIC  X(02).                       
      *                                                        73   20          
           05  :FGPXXX:-LAGREGATED            PIC  X(20).                       
      *                                                                         
        02 :FGPXXX:-DONNEE-CODIC.                                               
      *                                                        93    5          
           05  :FGPXXX:-CMARQ                 PIC  X(05).                       
      *                                                        98   20          
           05  :FGPXXX:-LMARQ                 PIC  X(20).                       
      *                                                       118   20          
           05  :FGPXXX:-LREFFOURN             PIC  X(20).                       
      *                                                       138    1          
           05  :FGPXXX:-WOA                   PIC  X(01).                       
      *                                                                         
      *                                                                         
        02 :FGPXXX:-DONNEE-DEPOT.                                               
      *                                                       139    4          
           05  :FGPXXX:-QSTOCKDIS             PIC S9(07) COMP-3.                
      *                                                       143    4          
           05  :FGPXXX:-QSTOCKRES             PIC S9(06) COMP-3.                
      *                                                       147    3          
           05  :FGPXXX:-NSOCDEPOT             PIC  X(03).                       
      *                                                       150    3          
           05  :FGPXXX:-NDEPOT                PIC  X(03).                       
      *                                                       153    4          
           05  :FGPXXX:-DCDE4                 PIC  X(04).                       
      *                                                       157    3          
           05  :FGPXXX:-QCDE                  PIC S9(05) COMP-3.                
      *                                                       160    3          
           05  :FGPXXX:-QCDERES               PIC S9(05) COMP-3.                
      *                                                       163    3          
           05  :FGPXXX:-QCOLIRECEPT           PIC S9(05) COMP-3.                
      *                                                       166    3          
           05  :FGPXXX:-QNBPRACK              PIC S9(05) COMP-3.                
      *                                                       169    1          
           05  :FGPXXX:-WSOL                  PIC  X(01).                       
      *                                                                         
      *                                                                         
        02 :FGPXXX:-DONNEE-TRI.                                                 
      *                                                       170    7          
           05  :FGPXXX:-NCODIC                PIC  X(07).                       
      *                                                       177    7          
           05  :FGPXXX:-NCODIC2               PIC  X(07).                       
      *                                                       184    1          
           05  :FGPXXX:-WDEB                  PIC  X(01).                       
      *                                                       185    1          
           05  :FGPXXX:-WGROUPE               PIC  X(01).                       
      *                                                       186    5          
           05  :FGPXXX:-WCMARQ                PIC  X(05).                       
      *                                                       191    6          
           05  :FGPXXX:-WENTNAT.                                                
               10  :FGPXXX:-WSOCNAT               PIC  X(03).                   
               10  :FGPXXX:-WDEPNAT               PIC  X(03).                   
      *                                                       197    4          
           05  :FGPXXX:-WVOLV4S               PIC S9(07) COMP-3.                
      *                                                       201    1          
           05  :FGPXXX:-WTYPENT               PIC  X(01).                       
      *                                                       202    1          
           05  :FGPXXX:-WSENSAPPRO            PIC  X(01).                       
      *                                                       203    8          
           05  :FGPXXX:-DCDE8                 PIC  X(08).                       
      *                                                       211    5          
           05  :FGPXXX:-CAPPRO                PIC  X(05).                       
      *                                                       216    1          
           05  :FGPXXX:-WDACEM                PIC  X(01).                       
      *                                                       217    5          
           05  :FGPXXX:-CAPPRO2               PIC  X(05).                       
      *                                                       222    2          
           05  :FGPXXX:-QTYPLIEN              PIC  S9(3) COMP-3.                
      *                                                       224    8          
           05  :FGPXXX:-DEFFET                PIC  X(08).                       
      *                                                       232    5          
           05  :FGPXXX:-CHEFPRM-P             PIC  X(05).                       
      *                                                                         
      *                                                237      (8*54)          
        02 :FGPXXX:-DONNEE-FILIALE    OCCURS 8.                                 
      *                                                                         
           05  :FGPXXX:-NFILIALE.                                               
                10  FILLER                    PIC X(01).                        
                10  :FGPXXX:-CODFILIALE       PIC X(02).                        
           05  :FGPXXX:-ENTAFF.                                                 
               10  :FGPXXX:-SOCAFF            PIC X(03).                        
               10  :FGPXXX:-DEPAFF            PIC X(03).                        
           05  :FGPXXX:-SOCCHEF               PIC X(05).                        
           05  :FGPXXX:-STKNR                 PIC S9(04) COMP-3.                
           05  :FGPXXX:-PSTDTTC               PIC S9(07)V9(02) COMP-3.          
           05  :FGPXXX:-QS-0                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-QS-1                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-QS-2                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-QS-3                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-QS-4                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-NBMAG                 PIC S9(03) COMP-3.                
           05  :FGPXXX:-QSTOCKMAG             PIC S9(05) COMP-3.                
           05  :FGPXXX:-QBESOINMAG            PIC S9(05) COMP-3.                
           05  :FGPXXX:-WGP                   PIC  X(01).                       
           05  :FGPXXX:-LSTATCOMP             PIC  X(03).                       
      *                                                       669   08          
         02  FILLER                           PIC  X(08).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
