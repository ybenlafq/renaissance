      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGQ0300                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGQ0300                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGQ0300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGQ0300.                                                            
      *}                                                                        
           02  GQ03-CMODDEL                                                     
               PIC X(0003).                                                     
           02  GQ03-CEQUIP                                                      
               PIC X(0005).                                                     
           02  GQ03-DJOUR                                                       
               PIC X(0008).                                                     
           02  GQ03-CZONLIV                                                     
               PIC X(0005).                                                     
           02  GQ03-WZONACT                                                     
               PIC X(0001).                                                     
           02  GQ03-QPSR                                                        
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GQ03-CPLAGE                                                      
               PIC X(0002).                                                     
           02  GQ03-QQUOTA                                                      
               PIC S9(5) COMP-3.                                                
           02  GQ03-QPRIS                                                       
               PIC S9(5) COMP-3.                                                
           02  GQ03-WILLIM                                                      
               PIC X(0001).                                                     
           02  GQ03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGQ0300                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGQ0300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGQ0300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ03-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ03-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ03-CEQUIP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ03-CEQUIP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ03-DJOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ03-DJOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ03-CZONLIV-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ03-CZONLIV-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ03-WZONACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ03-WZONACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ03-QPSR-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ03-QPSR-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ03-CPLAGE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ03-CPLAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ03-QQUOTA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ03-QQUOTA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ03-QPRIS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ03-QPRIS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ03-WILLIM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ03-WILLIM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GQ03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
