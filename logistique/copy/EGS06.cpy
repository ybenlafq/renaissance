      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGS06   EGS06                                              00000020
      ***************************************************************** 00000030
       01   EGS06I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNDEPOTI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRAYONL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MRAYONL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRAYONF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MRAYONI   PIC X(5).                                       00000290
           02 M5I OCCURS   12 TIMES .                                   00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MSELECTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELECTF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MSELECTI     PIC X.                                     00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNCODICI     PIC X(7).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCMARQI      PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCFAMI  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOURNL  COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLREFFOURNL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLREFFOURNF  PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLREFFOURNI  PIC X(20).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCROSSDOCKL  COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCROSSDOCKL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCROSSDOCKF  PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCROSSDOCKI  PIC X.                                     00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MQSTOCKL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQSTOCKF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MQSTOCKI     PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM1L   COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCOMM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM1F   PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCOMM1I   PIC X(20).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM2L   COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCOMM2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM2F   PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCOMM2I   PIC X(20).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM3L   COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCOMM3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM3F   PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCOMM3I   PIC X(20).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM4L   COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCOMM4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM4F   PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCOMM4I   PIC X(20).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM5L   COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCOMM5L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM5F   PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCOMM5I   PIC X(20).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM6L   COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCOMM6L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM6F   PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCOMM6I   PIC X(20).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM7L   COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCOMM7L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM7F   PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCOMM7I   PIC X(20).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMM8L   COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCOMM8L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOMM8F   PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCOMM8I   PIC X(20).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(12).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(61).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: EGS06   EGS06                                              00001160
      ***************************************************************** 00001170
       01   EGS06O REDEFINES EGS06I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEA    PIC X.                                          00001350
           02 MPAGEC    PIC X.                                          00001360
           02 MPAGEP    PIC X.                                          00001370
           02 MPAGEH    PIC X.                                          00001380
           02 MPAGEV    PIC X.                                          00001390
           02 MPAGEO    PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNSOCA    PIC X.                                          00001420
           02 MNSOCC    PIC X.                                          00001430
           02 MNSOCP    PIC X.                                          00001440
           02 MNSOCH    PIC X.                                          00001450
           02 MNSOCV    PIC X.                                          00001460
           02 MNSOCO    PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNDEPOTA  PIC X.                                          00001490
           02 MNDEPOTC  PIC X.                                          00001500
           02 MNDEPOTP  PIC X.                                          00001510
           02 MNDEPOTH  PIC X.                                          00001520
           02 MNDEPOTV  PIC X.                                          00001530
           02 MNDEPOTO  PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MRAYONA   PIC X.                                          00001560
           02 MRAYONC   PIC X.                                          00001570
           02 MRAYONP   PIC X.                                          00001580
           02 MRAYONH   PIC X.                                          00001590
           02 MRAYONV   PIC X.                                          00001600
           02 MRAYONO   PIC X(5).                                       00001610
           02 M5O OCCURS   12 TIMES .                                   00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MSELECTA     PIC X.                                     00001640
             03 MSELECTC     PIC X.                                     00001650
             03 MSELECTP     PIC X.                                     00001660
             03 MSELECTH     PIC X.                                     00001670
             03 MSELECTV     PIC X.                                     00001680
             03 MSELECTO     PIC X.                                     00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MNCODICA     PIC X.                                     00001710
             03 MNCODICC     PIC X.                                     00001720
             03 MNCODICP     PIC X.                                     00001730
             03 MNCODICH     PIC X.                                     00001740
             03 MNCODICV     PIC X.                                     00001750
             03 MNCODICO     PIC X(7).                                  00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MCMARQA      PIC X.                                     00001780
             03 MCMARQC PIC X.                                          00001790
             03 MCMARQP PIC X.                                          00001800
             03 MCMARQH PIC X.                                          00001810
             03 MCMARQV PIC X.                                          00001820
             03 MCMARQO      PIC X(5).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MCFAMA  PIC X.                                          00001850
             03 MCFAMC  PIC X.                                          00001860
             03 MCFAMP  PIC X.                                          00001870
             03 MCFAMH  PIC X.                                          00001880
             03 MCFAMV  PIC X.                                          00001890
             03 MCFAMO  PIC X(5).                                       00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MLREFFOURNA  PIC X.                                     00001920
             03 MLREFFOURNC  PIC X.                                     00001930
             03 MLREFFOURNP  PIC X.                                     00001940
             03 MLREFFOURNH  PIC X.                                     00001950
             03 MLREFFOURNV  PIC X.                                     00001960
             03 MLREFFOURNO  PIC X(20).                                 00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MCROSSDOCKA  PIC X.                                     00001990
             03 MCROSSDOCKC  PIC X.                                     00002000
             03 MCROSSDOCKP  PIC X.                                     00002010
             03 MCROSSDOCKH  PIC X.                                     00002020
             03 MCROSSDOCKV  PIC X.                                     00002030
             03 MCROSSDOCKO  PIC X.                                     00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MQSTOCKA     PIC X.                                     00002060
             03 MQSTOCKC     PIC X.                                     00002070
             03 MQSTOCKP     PIC X.                                     00002080
             03 MQSTOCKH     PIC X.                                     00002090
             03 MQSTOCKV     PIC X.                                     00002100
             03 MQSTOCKO     PIC X(5).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCOMM1A   PIC X.                                          00002130
           02 MCOMM1C   PIC X.                                          00002140
           02 MCOMM1P   PIC X.                                          00002150
           02 MCOMM1H   PIC X.                                          00002160
           02 MCOMM1V   PIC X.                                          00002170
           02 MCOMM1O   PIC X(20).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCOMM2A   PIC X.                                          00002200
           02 MCOMM2C   PIC X.                                          00002210
           02 MCOMM2P   PIC X.                                          00002220
           02 MCOMM2H   PIC X.                                          00002230
           02 MCOMM2V   PIC X.                                          00002240
           02 MCOMM2O   PIC X(20).                                      00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MCOMM3A   PIC X.                                          00002270
           02 MCOMM3C   PIC X.                                          00002280
           02 MCOMM3P   PIC X.                                          00002290
           02 MCOMM3H   PIC X.                                          00002300
           02 MCOMM3V   PIC X.                                          00002310
           02 MCOMM3O   PIC X(20).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCOMM4A   PIC X.                                          00002340
           02 MCOMM4C   PIC X.                                          00002350
           02 MCOMM4P   PIC X.                                          00002360
           02 MCOMM4H   PIC X.                                          00002370
           02 MCOMM4V   PIC X.                                          00002380
           02 MCOMM4O   PIC X(20).                                      00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MCOMM5A   PIC X.                                          00002410
           02 MCOMM5C   PIC X.                                          00002420
           02 MCOMM5P   PIC X.                                          00002430
           02 MCOMM5H   PIC X.                                          00002440
           02 MCOMM5V   PIC X.                                          00002450
           02 MCOMM5O   PIC X(20).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MCOMM6A   PIC X.                                          00002480
           02 MCOMM6C   PIC X.                                          00002490
           02 MCOMM6P   PIC X.                                          00002500
           02 MCOMM6H   PIC X.                                          00002510
           02 MCOMM6V   PIC X.                                          00002520
           02 MCOMM6O   PIC X(20).                                      00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MCOMM7A   PIC X.                                          00002550
           02 MCOMM7C   PIC X.                                          00002560
           02 MCOMM7P   PIC X.                                          00002570
           02 MCOMM7H   PIC X.                                          00002580
           02 MCOMM7V   PIC X.                                          00002590
           02 MCOMM7O   PIC X(20).                                      00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MCOMM8A   PIC X.                                          00002620
           02 MCOMM8C   PIC X.                                          00002630
           02 MCOMM8P   PIC X.                                          00002640
           02 MCOMM8H   PIC X.                                          00002650
           02 MCOMM8V   PIC X.                                          00002660
           02 MCOMM8O   PIC X(20).                                      00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(12).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(61).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
