      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES LIGNES DE MUTATIONS                  *        
      *  PROGRAMME  : MMU96                                            *        
      *  TITRE      : COMMAREA DU MODULE MMU96                         *        
      *               TRANSFERT D'UNE MUTATION PRINCIPALE VERS UNE     *        
      *               PETITE MUTATION - PAS DE LIGNE DE VENTE          *        
      *  LONGUEUR   : 150 C                                            *        
      *                                                                *        
      ******************************************************************        
       01  COMM-MMU96-APPLI.                                                    
      *--  DONNEES EN ENTREE DU MODULE ------------------------------ 21        
           02 COMM-MMU96-DONNEES-ENTREE.                                        
              03 COMM-MMU96-NMUTATION-ORIG    PIC X(07).                00890001
              03 COMM-MMU96-NMUTATION-DEST    PIC X(07).                00890001
              03 COMM-MMU96-NCODIC            PIC X(07).                00890001
      *--- DONNEES EN SORTIE DU MODULE ------------------------------ 59        
           02 COMM-MMU96-DONNEES-SORTIE.                                        
              03 COMM-MMU96-MESSAGE.                                            
                 05 COMM-MMU96-CODRET         PIC X(01).                        
                    88 COMM-MMU96-CODRET-OK              VALUE ' '.             
                    88 COMM-MMU96-CODRET-KO              VALUE '1'.             
                 05 COMM-MMU96-LIBERR.                                          
                    20 COMM-MMU96-NSEQERR     PIC X(04).                        
                    20 COMM-MMU96-ERRFIL      PIC X(01).                        
                    20 COMM-MMU96-LMESSAGE    PIC X(53).                        
      *--- RESERVE -------------------------------------------------- 70        
           02 COMM-MMU96-FILLER               PIC X(70).                        
                                                                                
