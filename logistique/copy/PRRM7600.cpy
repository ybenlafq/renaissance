      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE RM7600                       
      ******************************************************************        
      *                                                                         
       CLEF-RM7600             SECTION.                                         
      *                                                                         
           MOVE 'RVRM7600          '       TO   TABLE-NAME.                     
           MOVE 'RM7600'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-RM7600. EXIT.                                                   
                                                                                
