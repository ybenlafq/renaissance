      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVLV0500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLV0500                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLV0500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLV0500.                                                            
      *}                                                                        
           02  LV05-NSOCDEPOT      PIC X(0003).                                 
           02  LV05-NDEPOT         PIC X(0003).                                 
           02  LV05-NSOCIETE       PIC X(0003).                                 
           02  LV05-NLIEU          PIC X(0003).                                 
           02  LV05-NMUTATION      PIC X(0007).                                 
           02  LV05-TYPOP          PIC X(0002).                                 
           02  LV05-CSELART        PIC X(0005).                                 
           02  LV05-DFINSAIS       PIC X(0008).                                 
           02  LV05-DDESTOCK       PIC X(0008).                                 
           02  LV05-HCHARG         PIC X(0005).                                 
           02  LV05-DCHARG         PIC X(0008).                                 
           02  LV05-DMUTATION      PIC X(0008).                                 
           02  LV05-CTRANSPORTEUR  PIC X(0013).                                 
           02  LV05-WTYPLIVR       PIC X(0001).                                 
           02  LV05-WCPTCLIENT     PIC X(0001).                                 
           02  LV05-WFLAG          PIC X(0001).                                 
           02  LV05-NBL            PIC X(0007).                                 
           02  LV05-DTRANSFERT     PIC X(0008).                                 
           02  LV05-CTRANSFERT     PIC X(0008).                                 
           02  LV05-DEXPED         PIC X(0008).                                 
           02  LV05-DSYST          PIC S9(13) COMP-3.                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLV0500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLV0500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLV0500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-TYPOP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-TYPOP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-CSELART-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-CSELART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-DFINSAIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-DFINSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-DDESTOCK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-DDESTOCK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-HCHARG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-HCHARG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-DCHARG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-DCHARG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-DMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-DMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-CTRANSPORTEUR-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-CTRANSPORTEUR-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-WTYPLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-WTYPLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-WCPTCLIENT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-WCPTCLIENT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-WFLAG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-WFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-NBL-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-NBL-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-DTRANSFERT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-DTRANSFERT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-CTRANSFERT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-CTRANSFERT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-DEXPED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-DEXPED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LV05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LV05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
