      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGQ0100                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGQ0100                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGQ0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGQ0100.                                                            
      *}                                                                        
           02  GQ01-CINSEE                                                      
               PIC X(0005).                                                     
           02  GQ01-NSOC                                                        
               PIC X(0003).                                                     
           02  GQ01-CURBAIN                                                     
               PIC X(0003).                                                     
           02  GQ01-CDOUBLE                                                     
               PIC S9(9) COMP-3.                                                
           02  GQ01-QPRINCI                                                     
               PIC S9(9) COMP-3.                                                
           02  GQ01-QSECOND                                                     
               PIC S9(9) COMP-3.                                                
           02  GQ01-QVACANT                                                     
               PIC S9(9) COMP-3.                                                
           02  GQ01-QEVOLUT                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GQ01-QRESACT                                                     
               PIC S9(7) COMP-3.                                                
           02  GQ01-QCOEFF                                                      
               PIC S9(3) COMP-3.                                                
           02  GQ01-WCONTRA                                                     
               PIC X(0001).                                                     
           02  GQ01-CELEMEN                                                     
               PIC X(0005).                                                     
           02  GQ01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGQ0100                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGQ0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGQ0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-CURBAIN-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-CURBAIN-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-CDOUBLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-CDOUBLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-QPRINCI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-QPRINCI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-QSECOND-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-QSECOND-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-QVACANT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-QVACANT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-QEVOLUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-QEVOLUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-QRESACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-QRESACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-QCOEFF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-QCOEFF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-WCONTRA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-WCONTRA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-CELEMEN-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ01-CELEMEN-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GQ01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
