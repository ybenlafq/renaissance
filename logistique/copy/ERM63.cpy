      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM23   ERM23                                              00000020
      ***************************************************************** 00000030
       01   ERM63I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CRMGROUPL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 CRMGROUPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 CRMGROUPF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 CRMGROUPI      PIC X(5).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPAGEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MTOTPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTPAGEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MTOTPAGEI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFGL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCHEFGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFGF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCHEFGI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFGL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLCHEFGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCHEFGF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCHEFGI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAGREGL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLAGREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAGREGF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLAGREGI  PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPVTS1L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MQPVTS1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPVTS1F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MQPVTS1I  PIC X(6).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPVTS2L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MQPVTS2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPVTS2F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MQPVTS2I  PIC X(6).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPVTS3L  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MQPVTS3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPVTS3F  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MQPVTS3I  PIC X(6).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPVTP1L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MQPVTP1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPVTP1F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQPVTP1I  PIC X(6).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPVTP2L  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MQPVTP2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPVTP2F  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQPVTP2I  PIC X(6).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPVTP3L  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MQPVTP3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPVTP3F  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MQPVTP3I  PIC X(6).                                       00000690
           02 MQRANGD OCCURS   6 TIMES .                                00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRANGL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MQRANGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQRANGF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQRANGI      PIC X(3).                                  00000740
           02 MNCODICD OCCURS   6 TIMES .                               00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000760
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000770
             03 FILLER  PIC X(4).                                       00000780
             03 MNCODICI     PIC X(7).                                  00000790
           02 MCMARQD OCCURS   6 TIMES .                                00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MCMARQI      PIC X(5).                                  00000840
           02 MQV4SD OCCURS   6 TIMES .                                 00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQV4SL  COMP PIC S9(4).                                 00000860
      *--                                                                       
             03 MQV4SL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQV4SF  PIC X.                                          00000870
             03 FILLER  PIC X(4).                                       00000880
             03 MQV4SI  PIC X(5).                                       00000890
           02 MQV3SD OCCURS   6 TIMES .                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQV3SL  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MQV3SL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQV3SF  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQV3SI  PIC X(5).                                       00000940
           02 MQV2SD OCCURS   6 TIMES .                                 00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQV2SL  COMP PIC S9(4).                                 00000960
      *--                                                                       
             03 MQV2SL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQV2SF  PIC X.                                          00000970
             03 FILLER  PIC X(4).                                       00000980
             03 MQV2SI  PIC X(5).                                       00000990
           02 MQV1SD OCCURS   6 TIMES .                                 00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQV1SL  COMP PIC S9(4).                                 00001010
      *--                                                                       
             03 MQV1SL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQV1SF  PIC X.                                          00001020
             03 FILLER  PIC X(4).                                       00001030
             03 MQV1SI  PIC X(5).                                       00001040
           02 MQR1SD OCCURS   6 TIMES .                                 00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQR1SL  COMP PIC S9(4).                                 00001060
      *--                                                                       
             03 MQR1SL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQR1SF  PIC X.                                          00001070
             03 FILLER  PIC X(4).                                       00001080
             03 MQR1SI  PIC X(5).                                       00001090
           02 MQR2SD OCCURS   6 TIMES .                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQR2SL  COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MQR2SL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQR2SF  PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MQR2SI  PIC X(6).                                       00001140
           02 MQR3SD OCCURS   6 TIMES .                                 00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQR3SL  COMP PIC S9(4).                                 00001160
      *--                                                                       
             03 MQR3SL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQR3SF  PIC X.                                          00001170
             03 FILLER  PIC X(4).                                       00001180
             03 MQR3SI  PIC X(6).                                       00001190
           02 MLREFD OCCURS   6 TIMES .                                 00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00001210
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00001220
             03 FILLER  PIC X(4).                                       00001230
             03 MLREFI  PIC X(20).                                      00001240
           02 MQSV4D OCCURS   6 TIMES .                                 00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSV4L  COMP PIC S9(4).                                 00001260
      *--                                                                       
             03 MQSV4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSV4F  PIC X.                                          00001270
             03 FILLER  PIC X(4).                                       00001280
             03 MQSV4I  PIC X(5).                                       00001290
           02 MQSV3D OCCURS   6 TIMES .                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSV3L  COMP PIC S9(4).                                 00001310
      *--                                                                       
             03 MQSV3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSV3F  PIC X.                                          00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MQSV3I  PIC X(5).                                       00001340
           02 MQSV2D OCCURS   6 TIMES .                                 00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSV2L  COMP PIC S9(4).                                 00001360
      *--                                                                       
             03 MQSV2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSV2F  PIC X.                                          00001370
             03 FILLER  PIC X(4).                                       00001380
             03 MQSV2I  PIC X(5).                                       00001390
           02 MQSV1D OCCURS   6 TIMES .                                 00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSV1L  COMP PIC S9(4).                                 00001410
      *--                                                                       
             03 MQSV1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSV1F  PIC X.                                          00001420
             03 FILLER  PIC X(4).                                       00001430
             03 MQSV1I  PIC X(5).                                       00001440
           02 MQPV0D OCCURS   6 TIMES .                                 00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPV0L  COMP PIC S9(4).                                 00001460
      *--                                                                       
             03 MQPV0L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQPV0F  PIC X.                                          00001470
             03 FILLER  PIC X(4).                                       00001480
             03 MQPV0I  PIC X(6).                                       00001490
           02 MQPV1FD OCCURS   6 TIMES .                                00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPV1FL      COMP PIC S9(4).                            00001510
      *--                                                                       
             03 MQPV1FL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPV1FF      PIC X.                                     00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MQPV1FI      PIC X(5).                                  00001540
           02 MQPV2FD OCCURS   6 TIMES .                                00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPV2FL      COMP PIC S9(4).                            00001560
      *--                                                                       
             03 MQPV2FL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPV2FF      PIC X.                                     00001570
             03 FILLER  PIC X(4).                                       00001580
             03 MQPV2FI      PIC X(5).                                  00001590
           02 MQPV3FD OCCURS   6 TIMES .                                00001600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPV3FL      COMP PIC S9(4).                            00001610
      *--                                                                       
             03 MQPV3FL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPV3FF      PIC X.                                     00001620
             03 FILLER  PIC X(4).                                       00001630
             03 MQPV3FI      PIC X(5).                                  00001640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001650
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001660
           02 FILLER    PIC X(4).                                       00001670
           02 MLIBERRI  PIC X(78).                                      00001680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001690
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001700
           02 FILLER    PIC X(4).                                       00001710
           02 MCODTRAI  PIC X(4).                                       00001720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001730
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001740
           02 FILLER    PIC X(4).                                       00001750
           02 MCICSI    PIC X(5).                                       00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001770
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001780
           02 FILLER    PIC X(4).                                       00001790
           02 MNETNAMI  PIC X(8).                                       00001800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001810
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001820
           02 FILLER    PIC X(4).                                       00001830
           02 MSCREENI  PIC X(4).                                       00001840
      ***************************************************************** 00001850
      * SDF: ERM23   ERM23                                              00001860
      ***************************************************************** 00001870
       01   ERM63O REDEFINES ERM63I.                                    00001880
           02 FILLER    PIC X(12).                                      00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MDATJOUA  PIC X.                                          00001910
           02 MDATJOUC  PIC X.                                          00001920
           02 MDATJOUP  PIC X.                                          00001930
           02 MDATJOUH  PIC X.                                          00001940
           02 MDATJOUV  PIC X.                                          00001950
           02 MDATJOUO  PIC X(10).                                      00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MTIMJOUA  PIC X.                                          00001980
           02 MTIMJOUC  PIC X.                                          00001990
           02 MTIMJOUP  PIC X.                                          00002000
           02 MTIMJOUH  PIC X.                                          00002010
           02 MTIMJOUV  PIC X.                                          00002020
           02 MTIMJOUO  PIC X(5).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 CRMGROUPA      PIC X.                                     00002050
           02 CRMGROUPC PIC X.                                          00002060
           02 CRMGROUPP PIC X.                                          00002070
           02 CRMGROUPH PIC X.                                          00002080
           02 CRMGROUPV PIC X.                                          00002090
           02 CRMGROUPO      PIC X(5).                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MNPAGEA   PIC X.                                          00002120
           02 MNPAGEC   PIC X.                                          00002130
           02 MNPAGEP   PIC X.                                          00002140
           02 MNPAGEH   PIC X.                                          00002150
           02 MNPAGEV   PIC X.                                          00002160
           02 MNPAGEO   PIC X(3).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MTOTPAGEA      PIC X.                                     00002190
           02 MTOTPAGEC PIC X.                                          00002200
           02 MTOTPAGEP PIC X.                                          00002210
           02 MTOTPAGEH PIC X.                                          00002220
           02 MTOTPAGEV PIC X.                                          00002230
           02 MTOTPAGEO      PIC X(3).                                  00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCHEFGA   PIC X.                                          00002260
           02 MCHEFGC   PIC X.                                          00002270
           02 MCHEFGP   PIC X.                                          00002280
           02 MCHEFGH   PIC X.                                          00002290
           02 MCHEFGV   PIC X.                                          00002300
           02 MCHEFGO   PIC X(5).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLCHEFGA  PIC X.                                          00002330
           02 MLCHEFGC  PIC X.                                          00002340
           02 MLCHEFGP  PIC X.                                          00002350
           02 MLCHEFGH  PIC X.                                          00002360
           02 MLCHEFGV  PIC X.                                          00002370
           02 MLCHEFGO  PIC X(20).                                      00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MCFAMA    PIC X.                                          00002400
           02 MCFAMC    PIC X.                                          00002410
           02 MCFAMP    PIC X.                                          00002420
           02 MCFAMH    PIC X.                                          00002430
           02 MCFAMV    PIC X.                                          00002440
           02 MCFAMO    PIC X(5).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MLFAMA    PIC X.                                          00002470
           02 MLFAMC    PIC X.                                          00002480
           02 MLFAMP    PIC X.                                          00002490
           02 MLFAMH    PIC X.                                          00002500
           02 MLFAMV    PIC X.                                          00002510
           02 MLFAMO    PIC X(20).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLAGREGA  PIC X.                                          00002540
           02 MLAGREGC  PIC X.                                          00002550
           02 MLAGREGP  PIC X.                                          00002560
           02 MLAGREGH  PIC X.                                          00002570
           02 MLAGREGV  PIC X.                                          00002580
           02 MLAGREGO  PIC X(20).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MQPVTS1A  PIC X.                                          00002610
           02 MQPVTS1C  PIC X.                                          00002620
           02 MQPVTS1P  PIC X.                                          00002630
           02 MQPVTS1H  PIC X.                                          00002640
           02 MQPVTS1V  PIC X.                                          00002650
           02 MQPVTS1O  PIC X(6).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MQPVTS2A  PIC X.                                          00002680
           02 MQPVTS2C  PIC X.                                          00002690
           02 MQPVTS2P  PIC X.                                          00002700
           02 MQPVTS2H  PIC X.                                          00002710
           02 MQPVTS2V  PIC X.                                          00002720
           02 MQPVTS2O  PIC X(6).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MQPVTS3A  PIC X.                                          00002750
           02 MQPVTS3C  PIC X.                                          00002760
           02 MQPVTS3P  PIC X.                                          00002770
           02 MQPVTS3H  PIC X.                                          00002780
           02 MQPVTS3V  PIC X.                                          00002790
           02 MQPVTS3O  PIC X(6).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MQPVTP1A  PIC X.                                          00002820
           02 MQPVTP1C  PIC X.                                          00002830
           02 MQPVTP1P  PIC X.                                          00002840
           02 MQPVTP1H  PIC X.                                          00002850
           02 MQPVTP1V  PIC X.                                          00002860
           02 MQPVTP1O  PIC ZZZZZZ.                                     00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MQPVTP2A  PIC X.                                          00002890
           02 MQPVTP2C  PIC X.                                          00002900
           02 MQPVTP2P  PIC X.                                          00002910
           02 MQPVTP2H  PIC X.                                          00002920
           02 MQPVTP2V  PIC X.                                          00002930
           02 MQPVTP2O  PIC ZZZZZZ.                                     00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MQPVTP3A  PIC X.                                          00002960
           02 MQPVTP3C  PIC X.                                          00002970
           02 MQPVTP3P  PIC X.                                          00002980
           02 MQPVTP3H  PIC X.                                          00002990
           02 MQPVTP3V  PIC X.                                          00003000
           02 MQPVTP3O  PIC ZZZZZZ.                                     00003010
           02 DFHMS1 OCCURS   6 TIMES .                                 00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MQRANGA      PIC X.                                     00003040
             03 MQRANGC PIC X.                                          00003050
             03 MQRANGP PIC X.                                          00003060
             03 MQRANGH PIC X.                                          00003070
             03 MQRANGV PIC X.                                          00003080
             03 MQRANGO      PIC X(3).                                  00003090
           02 DFHMS2 OCCURS   6 TIMES .                                 00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MNCODICA     PIC X.                                     00003120
             03 MNCODICC     PIC X.                                     00003130
             03 MNCODICP     PIC X.                                     00003140
             03 MNCODICH     PIC X.                                     00003150
             03 MNCODICV     PIC X.                                     00003160
             03 MNCODICO     PIC X(7).                                  00003170
           02 DFHMS3 OCCURS   6 TIMES .                                 00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MCMARQA      PIC X.                                     00003200
             03 MCMARQC PIC X.                                          00003210
             03 MCMARQP PIC X.                                          00003220
             03 MCMARQH PIC X.                                          00003230
             03 MCMARQV PIC X.                                          00003240
             03 MCMARQO      PIC X(5).                                  00003250
           02 DFHMS4 OCCURS   6 TIMES .                                 00003260
             03 FILLER       PIC X(2).                                  00003270
             03 MQV4SA  PIC X.                                          00003280
             03 MQV4SC  PIC X.                                          00003290
             03 MQV4SP  PIC X.                                          00003300
             03 MQV4SH  PIC X.                                          00003310
             03 MQV4SV  PIC X.                                          00003320
             03 MQV4SO  PIC ZZZZ9.                                      00003330
           02 DFHMS5 OCCURS   6 TIMES .                                 00003340
             03 FILLER       PIC X(2).                                  00003350
             03 MQV3SA  PIC X.                                          00003360
             03 MQV3SC  PIC X.                                          00003370
             03 MQV3SP  PIC X.                                          00003380
             03 MQV3SH  PIC X.                                          00003390
             03 MQV3SV  PIC X.                                          00003400
             03 MQV3SO  PIC ZZZZ9.                                      00003410
           02 DFHMS6 OCCURS   6 TIMES .                                 00003420
             03 FILLER       PIC X(2).                                  00003430
             03 MQV2SA  PIC X.                                          00003440
             03 MQV2SC  PIC X.                                          00003450
             03 MQV2SP  PIC X.                                          00003460
             03 MQV2SH  PIC X.                                          00003470
             03 MQV2SV  PIC X.                                          00003480
             03 MQV2SO  PIC ZZZZ9.                                      00003490
           02 DFHMS7 OCCURS   6 TIMES .                                 00003500
             03 FILLER       PIC X(2).                                  00003510
             03 MQV1SA  PIC X.                                          00003520
             03 MQV1SC  PIC X.                                          00003530
             03 MQV1SP  PIC X.                                          00003540
             03 MQV1SH  PIC X.                                          00003550
             03 MQV1SV  PIC X.                                          00003560
             03 MQV1SO  PIC ZZZZ9.                                      00003570
           02 DFHMS8 OCCURS   6 TIMES .                                 00003580
             03 FILLER       PIC X(2).                                  00003590
             03 MQR1SA  PIC X.                                          00003600
             03 MQR1SC  PIC X.                                          00003610
             03 MQR1SP  PIC X.                                          00003620
             03 MQR1SH  PIC X.                                          00003630
             03 MQR1SV  PIC X.                                          00003640
             03 MQR1SO  PIC X(5).                                       00003650
           02 DFHMS9 OCCURS   6 TIMES .                                 00003660
             03 FILLER       PIC X(2).                                  00003670
             03 MQR2SA  PIC X.                                          00003680
             03 MQR2SC  PIC X.                                          00003690
             03 MQR2SP  PIC X.                                          00003700
             03 MQR2SH  PIC X.                                          00003710
             03 MQR2SV  PIC X.                                          00003720
             03 MQR2SO  PIC -----9.                                     00003730
           02 DFHMS10 OCCURS   6 TIMES .                                00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MQR3SA  PIC X.                                          00003760
             03 MQR3SC  PIC X.                                          00003770
             03 MQR3SP  PIC X.                                          00003780
             03 MQR3SH  PIC X.                                          00003790
             03 MQR3SV  PIC X.                                          00003800
             03 MQR3SO  PIC -----9.                                     00003810
           02 DFHMS11 OCCURS   6 TIMES .                                00003820
             03 FILLER       PIC X(2).                                  00003830
             03 MLREFA  PIC X.                                          00003840
             03 MLREFC  PIC X.                                          00003850
             03 MLREFP  PIC X.                                          00003860
             03 MLREFH  PIC X.                                          00003870
             03 MLREFV  PIC X.                                          00003880
             03 MLREFO  PIC X(20).                                      00003890
           02 DFHMS12 OCCURS   6 TIMES .                                00003900
             03 FILLER       PIC X(2).                                  00003910
             03 MQSV4A  PIC X.                                          00003920
             03 MQSV4C  PIC X.                                          00003930
             03 MQSV4P  PIC X.                                          00003940
             03 MQSV4H  PIC X.                                          00003950
             03 MQSV4V  PIC X.                                          00003960
             03 MQSV4O  PIC ZZZZ9.                                      00003970
           02 DFHMS13 OCCURS   6 TIMES .                                00003980
             03 FILLER       PIC X(2).                                  00003990
             03 MQSV3A  PIC X.                                          00004000
             03 MQSV3C  PIC X.                                          00004010
             03 MQSV3P  PIC X.                                          00004020
             03 MQSV3H  PIC X.                                          00004030
             03 MQSV3V  PIC X.                                          00004040
             03 MQSV3O  PIC ZZZZ9.                                      00004050
           02 DFHMS14 OCCURS   6 TIMES .                                00004060
             03 FILLER       PIC X(2).                                  00004070
             03 MQSV2A  PIC X.                                          00004080
             03 MQSV2C  PIC X.                                          00004090
             03 MQSV2P  PIC X.                                          00004100
             03 MQSV2H  PIC X.                                          00004110
             03 MQSV2V  PIC X.                                          00004120
             03 MQSV2O  PIC ZZZZ9.                                      00004130
           02 DFHMS15 OCCURS   6 TIMES .                                00004140
             03 FILLER       PIC X(2).                                  00004150
             03 MQSV1A  PIC X.                                          00004160
             03 MQSV1C  PIC X.                                          00004170
             03 MQSV1P  PIC X.                                          00004180
             03 MQSV1H  PIC X.                                          00004190
             03 MQSV1V  PIC X.                                          00004200
             03 MQSV1O  PIC ZZZZ9.                                      00004210
           02 DFHMS16 OCCURS   6 TIMES .                                00004220
             03 FILLER       PIC X(2).                                  00004230
             03 MQPV0A  PIC X.                                          00004240
             03 MQPV0C  PIC X.                                          00004250
             03 MQPV0P  PIC X.                                          00004260
             03 MQPV0H  PIC X.                                          00004270
             03 MQPV0V  PIC X.                                          00004280
             03 MQPV0O  PIC -----9.                                     00004290
           02 DFHMS17 OCCURS   6 TIMES .                                00004300
             03 FILLER       PIC X(2).                                  00004310
             03 MQPV1FA      PIC X.                                     00004320
             03 MQPV1FC PIC X.                                          00004330
             03 MQPV1FP PIC X.                                          00004340
             03 MQPV1FH PIC X.                                          00004350
             03 MQPV1FV PIC X.                                          00004360
             03 MQPV1FO      PIC X(5).                                  00004370
           02 DFHMS18 OCCURS   6 TIMES .                                00004380
             03 FILLER       PIC X(2).                                  00004390
             03 MQPV2FA      PIC X.                                     00004400
             03 MQPV2FC PIC X.                                          00004410
             03 MQPV2FP PIC X.                                          00004420
             03 MQPV2FH PIC X.                                          00004430
             03 MQPV2FV PIC X.                                          00004440
             03 MQPV2FO      PIC X(5).                                  00004450
           02 DFHMS19 OCCURS   6 TIMES .                                00004460
             03 FILLER       PIC X(2).                                  00004470
             03 MQPV3FA      PIC X.                                     00004480
             03 MQPV3FC PIC X.                                          00004490
             03 MQPV3FP PIC X.                                          00004500
             03 MQPV3FH PIC X.                                          00004510
             03 MQPV3FV PIC X.                                          00004520
             03 MQPV3FO      PIC X(5).                                  00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MLIBERRA  PIC X.                                          00004550
           02 MLIBERRC  PIC X.                                          00004560
           02 MLIBERRP  PIC X.                                          00004570
           02 MLIBERRH  PIC X.                                          00004580
           02 MLIBERRV  PIC X.                                          00004590
           02 MLIBERRO  PIC X(78).                                      00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MCODTRAA  PIC X.                                          00004620
           02 MCODTRAC  PIC X.                                          00004630
           02 MCODTRAP  PIC X.                                          00004640
           02 MCODTRAH  PIC X.                                          00004650
           02 MCODTRAV  PIC X.                                          00004660
           02 MCODTRAO  PIC X(4).                                       00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MCICSA    PIC X.                                          00004690
           02 MCICSC    PIC X.                                          00004700
           02 MCICSP    PIC X.                                          00004710
           02 MCICSH    PIC X.                                          00004720
           02 MCICSV    PIC X.                                          00004730
           02 MCICSO    PIC X(5).                                       00004740
           02 FILLER    PIC X(2).                                       00004750
           02 MNETNAMA  PIC X.                                          00004760
           02 MNETNAMC  PIC X.                                          00004770
           02 MNETNAMP  PIC X.                                          00004780
           02 MNETNAMH  PIC X.                                          00004790
           02 MNETNAMV  PIC X.                                          00004800
           02 MNETNAMO  PIC X(8).                                       00004810
           02 FILLER    PIC X(2).                                       00004820
           02 MSCREENA  PIC X.                                          00004830
           02 MSCREENC  PIC X.                                          00004840
           02 MSCREENP  PIC X.                                          00004850
           02 MSCREENH  PIC X.                                          00004860
           02 MSCREENV  PIC X.                                          00004870
           02 MSCREENO  PIC X(4).                                       00004880
                                                                                
