      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EEF30   EEF30                                              00000020
      ***************************************************************** 00000030
       01   EEF30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MPAGEMAXI      PIC X(3).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCTRAITI  PIC X(5).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MLTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRAITF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLTRAITI  PIC X(30).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCTIERSI  PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTIERSL     COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MLIBTIERSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBTIERSF     PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLIBTIERSI     PIC X(20).                                 00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLTIERSI  PIC X(15).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNENVOII  PIC X(7).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MDENVOII  PIC X(10).                                      00000510
           02 MAFFICHEI OCCURS   5 TIMES .                              00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTL     COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MSELECTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELECTF     PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MSELECTI     PIC X.                                     00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUHSL    COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MNLIEUHSL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNLIEUHSF    PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MNLIEUHSI    PIC X(3).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLOTHSL     COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MNLOTHSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLOTHSF     PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MNLOTHSI     PIC X(7).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MNCODICI     PIC X(7).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000690
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MCFAMI  PIC X(5).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000730
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MREFI   PIC X(20).                                      00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGARL  COMP PIC S9(4).                                 00000770
      *--                                                                       
             03 MCGARL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCGARF  PIC X.                                          00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MCGARI  PIC X(5).                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNACCORDL    COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MNACCORDL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNACCORDF    PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MNACCORDI    PIC X(12).                                 00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDACCORDL    COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MDACCORDL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDACCORDF    PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MDACCORDI    PIC X(5).                                  00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000890
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MQTEI   PIC X(3).                                       00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDANNULL     COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MDANNULL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDANNULF     PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MDANNULI     PIC X(5).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRENDUL     COMP PIC S9(4).                            00000970
      *--                                                                       
             03 MDRENDUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDRENDUF     PIC X.                                     00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MDRENDUI     PIC X(5).                                  00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRENDUL     COMP PIC S9(4).                            00001010
      *--                                                                       
             03 MNRENDUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNRENDUF     PIC X.                                     00001020
             03 FILLER  PIC X(4).                                       00001030
             03 MNRENDUI     PIC X(20).                                 00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRENDUL     COMP PIC S9(4).                            00001050
      *--                                                                       
             03 MCRENDUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCRENDUF     PIC X.                                     00001060
             03 FILLER  PIC X(4).                                       00001070
             03 MCRENDUI     PIC X(5).                                  00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSERIEL     COMP PIC S9(4).                            00001090
      *--                                                                       
             03 MNSERIEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNSERIEF     PIC X.                                     00001100
             03 FILLER  PIC X(4).                                       00001110
             03 MNSERIEI     PIC X(16).                                 00001120
      * MESSAGE ERREUR                                                  00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLIBERRI  PIC X(78).                                      00001170
      * CODE TRANSACTION                                                00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      * CICS DE TRAVAIL                                                 00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MCICSI    PIC X(5).                                       00001270
      * NETNAME                                                         00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MNETNAMI  PIC X(8).                                       00001320
      * CODE TERMINAL                                                   00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MSCREENI  PIC X(5).                                       00001370
      ***************************************************************** 00001380
      * SDF: EEF30   EEF30                                              00001390
      ***************************************************************** 00001400
       01   EEF30O REDEFINES EEF30I.                                    00001410
           02 FILLER    PIC X(12).                                      00001420
      * DATE DU JOUR                                                    00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MDATJOUA  PIC X.                                          00001450
           02 MDATJOUC  PIC X.                                          00001460
           02 MDATJOUP  PIC X.                                          00001470
           02 MDATJOUH  PIC X.                                          00001480
           02 MDATJOUV  PIC X.                                          00001490
           02 MDATJOUO  PIC X(10).                                      00001500
      * HEURE                                                           00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MTIMJOUA  PIC X.                                          00001530
           02 MTIMJOUC  PIC X.                                          00001540
           02 MTIMJOUP  PIC X.                                          00001550
           02 MTIMJOUH  PIC X.                                          00001560
           02 MTIMJOUV  PIC X.                                          00001570
           02 MTIMJOUO  PIC X(5).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MPAGEA    PIC X.                                          00001600
           02 MPAGEC    PIC X.                                          00001610
           02 MPAGEP    PIC X.                                          00001620
           02 MPAGEH    PIC X.                                          00001630
           02 MPAGEV    PIC X.                                          00001640
           02 MPAGEO    PIC 999.                                        00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MPAGEMAXA      PIC X.                                     00001670
           02 MPAGEMAXC PIC X.                                          00001680
           02 MPAGEMAXP PIC X.                                          00001690
           02 MPAGEMAXH PIC X.                                          00001700
           02 MPAGEMAXV PIC X.                                          00001710
           02 MPAGEMAXO      PIC 999.                                   00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MCTRAITA  PIC X.                                          00001740
           02 MCTRAITC  PIC X.                                          00001750
           02 MCTRAITP  PIC X.                                          00001760
           02 MCTRAITH  PIC X.                                          00001770
           02 MCTRAITV  PIC X.                                          00001780
           02 MCTRAITO  PIC X(5).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLTRAITA  PIC X.                                          00001810
           02 MLTRAITC  PIC X.                                          00001820
           02 MLTRAITP  PIC X.                                          00001830
           02 MLTRAITH  PIC X.                                          00001840
           02 MLTRAITV  PIC X.                                          00001850
           02 MLTRAITO  PIC X(30).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCTIERSA  PIC X.                                          00001880
           02 MCTIERSC  PIC X.                                          00001890
           02 MCTIERSP  PIC X.                                          00001900
           02 MCTIERSH  PIC X.                                          00001910
           02 MCTIERSV  PIC X.                                          00001920
           02 MCTIERSO  PIC X(5).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLIBTIERSA     PIC X.                                     00001950
           02 MLIBTIERSC     PIC X.                                     00001960
           02 MLIBTIERSP     PIC X.                                     00001970
           02 MLIBTIERSH     PIC X.                                     00001980
           02 MLIBTIERSV     PIC X.                                     00001990
           02 MLIBTIERSO     PIC X(20).                                 00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLTIERSA  PIC X.                                          00002020
           02 MLTIERSC  PIC X.                                          00002030
           02 MLTIERSP  PIC X.                                          00002040
           02 MLTIERSH  PIC X.                                          00002050
           02 MLTIERSV  PIC X.                                          00002060
           02 MLTIERSO  PIC X(15).                                      00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNENVOIA  PIC X.                                          00002090
           02 MNENVOIC  PIC X.                                          00002100
           02 MNENVOIP  PIC X.                                          00002110
           02 MNENVOIH  PIC X.                                          00002120
           02 MNENVOIV  PIC X.                                          00002130
           02 MNENVOIO  PIC X(7).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MDENVOIA  PIC X.                                          00002160
           02 MDENVOIC  PIC X.                                          00002170
           02 MDENVOIP  PIC X.                                          00002180
           02 MDENVOIH  PIC X.                                          00002190
           02 MDENVOIV  PIC X.                                          00002200
           02 MDENVOIO  PIC X(10).                                      00002210
           02 MAFFICHEO OCCURS   5 TIMES .                              00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MSELECTA     PIC X.                                     00002240
             03 MSELECTC     PIC X.                                     00002250
             03 MSELECTP     PIC X.                                     00002260
             03 MSELECTH     PIC X.                                     00002270
             03 MSELECTV     PIC X.                                     00002280
             03 MSELECTO     PIC X.                                     00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MNLIEUHSA    PIC X.                                     00002310
             03 MNLIEUHSC    PIC X.                                     00002320
             03 MNLIEUHSP    PIC X.                                     00002330
             03 MNLIEUHSH    PIC X.                                     00002340
             03 MNLIEUHSV    PIC X.                                     00002350
             03 MNLIEUHSO    PIC 999.                                   00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MNLOTHSA     PIC X.                                     00002380
             03 MNLOTHSC     PIC X.                                     00002390
             03 MNLOTHSP     PIC X.                                     00002400
             03 MNLOTHSH     PIC X.                                     00002410
             03 MNLOTHSV     PIC X.                                     00002420
             03 MNLOTHSO     PIC 9(7).                                  00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MNCODICA     PIC X.                                     00002450
             03 MNCODICC     PIC X.                                     00002460
             03 MNCODICP     PIC X.                                     00002470
             03 MNCODICH     PIC X.                                     00002480
             03 MNCODICV     PIC X.                                     00002490
             03 MNCODICO     PIC 9999999.                               00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MCFAMA  PIC X.                                          00002520
             03 MCFAMC  PIC X.                                          00002530
             03 MCFAMP  PIC X.                                          00002540
             03 MCFAMH  PIC X.                                          00002550
             03 MCFAMV  PIC X.                                          00002560
             03 MCFAMO  PIC X(5).                                       00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MREFA   PIC X.                                          00002590
             03 MREFC   PIC X.                                          00002600
             03 MREFP   PIC X.                                          00002610
             03 MREFH   PIC X.                                          00002620
             03 MREFV   PIC X.                                          00002630
             03 MREFO   PIC X(20).                                      00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MCGARA  PIC X.                                          00002660
             03 MCGARC  PIC X.                                          00002670
             03 MCGARP  PIC X.                                          00002680
             03 MCGARH  PIC X.                                          00002690
             03 MCGARV  PIC X.                                          00002700
             03 MCGARO  PIC X(5).                                       00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MNACCORDA    PIC X.                                     00002730
             03 MNACCORDC    PIC X.                                     00002740
             03 MNACCORDP    PIC X.                                     00002750
             03 MNACCORDH    PIC X.                                     00002760
             03 MNACCORDV    PIC X.                                     00002770
             03 MNACCORDO    PIC 9(12).                                 00002780
             03 FILLER       PIC X(2).                                  00002790
             03 MDACCORDA    PIC X.                                     00002800
             03 MDACCORDC    PIC X.                                     00002810
             03 MDACCORDP    PIC X.                                     00002820
             03 MDACCORDH    PIC X.                                     00002830
             03 MDACCORDV    PIC X.                                     00002840
             03 MDACCORDO    PIC X(5).                                  00002850
             03 FILLER       PIC X(2).                                  00002860
             03 MQTEA   PIC X.                                          00002870
             03 MQTEC   PIC X.                                          00002880
             03 MQTEP   PIC X.                                          00002890
             03 MQTEH   PIC X.                                          00002900
             03 MQTEV   PIC X.                                          00002910
             03 MQTEO   PIC X(3).                                       00002920
             03 FILLER       PIC X(2).                                  00002930
             03 MDANNULA     PIC X.                                     00002940
             03 MDANNULC     PIC X.                                     00002950
             03 MDANNULP     PIC X.                                     00002960
             03 MDANNULH     PIC X.                                     00002970
             03 MDANNULV     PIC X.                                     00002980
             03 MDANNULO     PIC X(5).                                  00002990
             03 FILLER       PIC X(2).                                  00003000
             03 MDRENDUA     PIC X.                                     00003010
             03 MDRENDUC     PIC X.                                     00003020
             03 MDRENDUP     PIC X.                                     00003030
             03 MDRENDUH     PIC X.                                     00003040
             03 MDRENDUV     PIC X.                                     00003050
             03 MDRENDUO     PIC X(5).                                  00003060
             03 FILLER       PIC X(2).                                  00003070
             03 MNRENDUA     PIC X.                                     00003080
             03 MNRENDUC     PIC X.                                     00003090
             03 MNRENDUP     PIC X.                                     00003100
             03 MNRENDUH     PIC X.                                     00003110
             03 MNRENDUV     PIC X.                                     00003120
             03 MNRENDUO     PIC X(20).                                 00003130
             03 FILLER       PIC X(2).                                  00003140
             03 MCRENDUA     PIC X.                                     00003150
             03 MCRENDUC     PIC X.                                     00003160
             03 MCRENDUP     PIC X.                                     00003170
             03 MCRENDUH     PIC X.                                     00003180
             03 MCRENDUV     PIC X.                                     00003190
             03 MCRENDUO     PIC X(5).                                  00003200
             03 FILLER       PIC X(2).                                  00003210
             03 MNSERIEA     PIC X.                                     00003220
             03 MNSERIEC     PIC X.                                     00003230
             03 MNSERIEP     PIC X.                                     00003240
             03 MNSERIEH     PIC X.                                     00003250
             03 MNSERIEV     PIC X.                                     00003260
             03 MNSERIEO     PIC X(16).                                 00003270
      * MESSAGE ERREUR                                                  00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MLIBERRA  PIC X.                                          00003300
           02 MLIBERRC  PIC X.                                          00003310
           02 MLIBERRP  PIC X.                                          00003320
           02 MLIBERRH  PIC X.                                          00003330
           02 MLIBERRV  PIC X.                                          00003340
           02 MLIBERRO  PIC X(78).                                      00003350
      * CODE TRANSACTION                                                00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCODTRAA  PIC X.                                          00003380
           02 MCODTRAC  PIC X.                                          00003390
           02 MCODTRAP  PIC X.                                          00003400
           02 MCODTRAH  PIC X.                                          00003410
           02 MCODTRAV  PIC X.                                          00003420
           02 MCODTRAO  PIC X(4).                                       00003430
      * CICS DE TRAVAIL                                                 00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MCICSA    PIC X.                                          00003460
           02 MCICSC    PIC X.                                          00003470
           02 MCICSP    PIC X.                                          00003480
           02 MCICSH    PIC X.                                          00003490
           02 MCICSV    PIC X.                                          00003500
           02 MCICSO    PIC X(5).                                       00003510
      * NETNAME                                                         00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MNETNAMA  PIC X.                                          00003540
           02 MNETNAMC  PIC X.                                          00003550
           02 MNETNAMP  PIC X.                                          00003560
           02 MNETNAMH  PIC X.                                          00003570
           02 MNETNAMV  PIC X.                                          00003580
           02 MNETNAMO  PIC X(8).                                       00003590
      * CODE TERMINAL                                                   00003600
           02 FILLER    PIC X(2).                                       00003610
           02 MSCREENA  PIC X.                                          00003620
           02 MSCREENC  PIC X.                                          00003630
           02 MSCREENP  PIC X.                                          00003640
           02 MSCREENH  PIC X.                                          00003650
           02 MSCREENV  PIC X.                                          00003660
           02 MSCREENO  PIC X(5).                                       00003670
                                                                                
