      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00000100
      * COMMAREA SPECIFIQUE PRG TGD20 (TGD00 -> MENU)    TR: GD20  *    00000200
      *               SELECTION DES PROFILS POUR DESTOCKAGE        *    00000300
      *                                                                 00000400
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3580  00000500
      *                                                                 00000600
      *        TRANSACTION GD20 : SAISIE DES PROFILS (DESTOCKAGE)  *    00000700
      *                                                                 00000710
          03 COMM-GD20-APPLI REDEFINES COMM-GD00-FILLER.                00000720
      *------------------------------ ZONE DONNEES TGD20                00000730
             04 COMM-GD20-DONNEES-TGD20.                                00000740
                05  COMM-GD20-GD10-FILLER        PIC X(300).            00000741
                05  COMM-GD20-NB-LIGNES          PIC 99.                00000742
                05  COMM-GD20-ATTR               PIC X(01) OCCURS 400.  00000743
                05  COMM-GD20-TOPLISTE           PIC X.                 00000744
                05  COMM-GD20-PAGE-SUP           PIC 9(3).              00000745
                05  COMM-GD20-PAGE               PIC 9(3).              00000746
                05  COMM-GD20-FIN-RTTL05         PIC X.                 00000747
                    88  FIN-TABLE-RTTL05-NON-ATTEINTE  VALUE '0'.       00000748
                    88  FIN-TABLE-RTTL05-ATTEINTE  VALUE '1'.           00000749
                05  COMM-GD20-RTTL05-VIDE        PIC X.                 00000750
                    88  TABLE-RTTL05-NON-VIDE     VALUE '0'.            00000751
                    88  TABLE-RTTL05-VIDE         VALUE '1'.            00000752
                05  COMM-GD20-PROFIL             PIC X(5).              00000753
                05  COMM-GD20-DDELIV             PIC X(8).              00000754
                05  COMM-GD20-TOTLCOM            PIC 9(4).              00000755
                05  COMM-GD20-TOTPCOM            PIC 9(5).              00000756
                05  COMM-GD20-DJOUR              PIC X(8).              00000757
                05  COMM-GD20-DONNEES-GD35   OCCURS 11.                 00000758
                    10  COMM-GD20-GD35-PROFIL       PIC X(5).           00000759
                    10  COMM-GD20-GD35-DDELIV       PIC X(8).           00000760
                05  COMM-GD20-FILLER             PIC X(500).            00000770
                                                                                
