      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVIN0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIN0000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIN0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIN0000.                                                            
      *}                                                                        
           02  IN00-NSOCMAG                                                     
               PIC X(0003).                                                     
           02  IN00-NMAG                                                        
               PIC X(0003).                                                     
           02  IN00-DINVENTAIRE                                                 
               PIC X(0008).                                                     
           02  IN00-CCARTON                                                     
               PIC X(0001).                                                     
           02  IN00-NCODIC                                                      
               PIC X(0007).                                                     
           02  IN00-CMARQ                                                       
               PIC X(0005).                                                     
           02  IN00-CFAM                                                        
               PIC X(0005).                                                     
           02  IN00-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  IN00-WSTOCKMASQ                                                  
               PIC X(0001).                                                     
           02  IN00-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  IN00-QSTOCK                                                      
               PIC S9(5) COMP-3.                                                
           02  IN00-PRMP                                                        
               PIC S9(7)V9(0006) COMP-3.                                        
           02  IN00-QSTOCKEC1                                                   
               PIC S9(5) COMP-3.                                                
           02  IN00-WECART                                                      
               PIC X(0001).                                                     
           02  IN00-QSTOCKEC2                                                   
               PIC S9(5) COMP-3.                                                
           02  IN00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  IN00-CSTATUT                                                     
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVIN0000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIN0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIN0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-NSOCMAG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-NSOCMAG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-DINVENTAIRE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-DINVENTAIRE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-CCARTON-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-CCARTON-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-WSTOCKMASQ-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-WSTOCKMASQ-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-NSSLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-QSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-QSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-PRMP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-QSTOCKEC1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-QSTOCKEC1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-WECART-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-WECART-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-QSTOCKEC2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-QSTOCKEC2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN00-CSTATUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN00-CSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
