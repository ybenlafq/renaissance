      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGE0000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGE0000                         
      **********************************************************                
       01  RVGE0000.                                                            
           02  GE00-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GE00-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GE00-DMVT                                                        
               PIC X(0008).                                                     
           02  GE00-NCODIC                                                      
               PIC X(0007).                                                     
           02  GE00-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  GE00-CEMP1                                                       
               PIC X(0002).                                                     
           02  GE00-CEMP2                                                       
               PIC X(0002).                                                     
           02  GE00-CEMP3                                                       
               PIC X(0003).                                                     
           02  GE00-QSTOCKINIT                                                  
               PIC S9(5) COMP-3.                                                
           02  GE00-QSTOCKMAJ                                                   
               PIC S9(5) COMP-3.                                                
           02  GE00-LCOMMENT                                                    
               PIC X(0020).                                                     
           02  GE00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGE0000                                  
      **********************************************************                
       01  RVGE0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE00-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE00-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-DMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE00-DMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE00-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-CEMP1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE00-CEMP1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-CEMP2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE00-CEMP2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-CEMP3-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE00-CEMP3-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-QSTOCKINIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE00-QSTOCKINIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-QSTOCKMAJ-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE00-QSTOCKMAJ-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GE00-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GE00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GE00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
