      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA610      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,04,PD,A,                          *        
      *                           14,03,BI,A,                          *        
      *                           17,03,BI,A,                          *        
      *                           20,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA610.                                                        
            05 NOMETAT-IVA610           PIC X(6) VALUE 'IVA610'.                
            05 RUPTURES-IVA610.                                                 
           10 IVA610-NSOCIETE           PIC X(03).                      007  003
           10 IVA610-NSEQPRO            PIC S9(07)      COMP-3.         010  004
           10 IVA610-NSOCMVT            PIC X(03).                      014  003
           10 IVA610-NLIEUMVT           PIC X(03).                      017  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA610-SEQUENCE           PIC S9(04) COMP.                020  002
      *--                                                                       
           10 IVA610-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA610.                                                   
           10 IVA610-LLIEU              PIC X(20).                      022  020
           10 IVA610-LSEQPRO            PIC X(26).                      042  026
           10 IVA610-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         068  008
           10 IVA610-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         076  008
           10 IVA610-QSTOCKENTREE       PIC S9(11)      COMP-3.         084  006
           10 IVA610-QSTOCKSORTIE       PIC S9(11)      COMP-3.         090  006
            05 FILLER                      PIC X(417).                          
                                                                                
