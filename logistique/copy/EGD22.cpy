      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD22   EGD22                                              00000020
      ***************************************************************** 00000030
       01   EGD22I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MENTL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MENTF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MENTI     PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPL     COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MDEPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MDEPF     PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MDEPI     PIC X(3).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJDESTL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MJDESTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJDESTF   PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MJDESTI   PIC X(10).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELGLOBL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MSELGLOBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSELGLOBF      PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MSELGLOBI      PIC X.                                     00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORAFL   COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MNORAFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNORAFF   PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MNORAFI   PIC X(3).                                       00000390
           02 MLIGNEI OCCURS   13 TIMES .                               00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOMUTL      COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MNOMUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNOMUTF      PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MNOMUTI      PIC X(7).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCL   COMP PIC S9(4).                                 00000450
      *--                                                                       
             03 MSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSOCF   PIC X.                                          00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MSOCI   PIC X(3).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUL  COMP PIC S9(4).                                 00000490
      *--                                                                       
             03 MLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIEUF  PIC X.                                          00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MLIEUI  PIC X(3).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MLIBELLEI    PIC X(20).                                 00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELARTL     COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MSELARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELARTF     PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MSELARTI     PIC X(3).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRAFALEL     COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MRAFALEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MRAFALEF     PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MRAFALEI     PIC X(3).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTL     COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MSELECTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELECTF     PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MSELECTI     PIC X.                                     00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MZONCMDI  PIC X(15).                                      00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MLIBERRI  PIC X(58).                                      00000760
      * CODE TRANSACTION                                                00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCODTRAI  PIC X(4).                                       00000810
      * NOM DU CICS                                                     00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      * NETNAME                                                         00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MNETNAMI  PIC X(8).                                       00000910
      * CODE TERMINAL                                                   00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MSCREENI  PIC X(5).                                       00000960
      ***************************************************************** 00000970
      * SDF: EGD22   EGD22                                              00000980
      ***************************************************************** 00000990
       01   EGD22O REDEFINES EGD22I.                                    00001000
           02 FILLER    PIC X(12).                                      00001010
      * DATE DU JOUR                                                    00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MDATJOUA  PIC X.                                          00001040
           02 MDATJOUC  PIC X.                                          00001050
           02 MDATJOUP  PIC X.                                          00001060
           02 MDATJOUH  PIC X.                                          00001070
           02 MDATJOUV  PIC X.                                          00001080
           02 MDATJOUO  PIC X(10).                                      00001090
      * HEURE                                                           00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MPAGEA    PIC X.                                          00001190
           02 MPAGEC    PIC X.                                          00001200
           02 MPAGEP    PIC X.                                          00001210
           02 MPAGEH    PIC X.                                          00001220
           02 MPAGEV    PIC X.                                          00001230
           02 MPAGEO    PIC Z9.                                         00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MENTA     PIC X.                                          00001260
           02 MENTC     PIC X.                                          00001270
           02 MENTP     PIC X.                                          00001280
           02 MENTH     PIC X.                                          00001290
           02 MENTV     PIC X.                                          00001300
           02 MENTO     PIC X(3).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDEPA     PIC X.                                          00001330
           02 MDEPC     PIC X.                                          00001340
           02 MDEPP     PIC X.                                          00001350
           02 MDEPH     PIC X.                                          00001360
           02 MDEPV     PIC X.                                          00001370
           02 MDEPO     PIC X(3).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MJDESTA   PIC X.                                          00001400
           02 MJDESTC   PIC X.                                          00001410
           02 MJDESTP   PIC X.                                          00001420
           02 MJDESTH   PIC X.                                          00001430
           02 MJDESTV   PIC X.                                          00001440
           02 MJDESTO   PIC X(10).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MSELGLOBA      PIC X.                                     00001470
           02 MSELGLOBC PIC X.                                          00001480
           02 MSELGLOBP PIC X.                                          00001490
           02 MSELGLOBH PIC X.                                          00001500
           02 MSELGLOBV PIC X.                                          00001510
           02 MSELGLOBO      PIC X.                                     00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNORAFA   PIC X.                                          00001540
           02 MNORAFC   PIC X.                                          00001550
           02 MNORAFP   PIC X.                                          00001560
           02 MNORAFH   PIC X.                                          00001570
           02 MNORAFV   PIC X.                                          00001580
           02 MNORAFO   PIC X(3).                                       00001590
           02 MLIGNEO OCCURS   13 TIMES .                               00001600
             03 FILLER       PIC X(2).                                  00001610
             03 MNOMUTA      PIC X.                                     00001620
             03 MNOMUTC PIC X.                                          00001630
             03 MNOMUTP PIC X.                                          00001640
             03 MNOMUTH PIC X.                                          00001650
             03 MNOMUTV PIC X.                                          00001660
             03 MNOMUTO      PIC X(7).                                  00001670
             03 FILLER       PIC X(2).                                  00001680
             03 MSOCA   PIC X.                                          00001690
             03 MSOCC   PIC X.                                          00001700
             03 MSOCP   PIC X.                                          00001710
             03 MSOCH   PIC X.                                          00001720
             03 MSOCV   PIC X.                                          00001730
             03 MSOCO   PIC X(3).                                       00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MLIEUA  PIC X.                                          00001760
             03 MLIEUC  PIC X.                                          00001770
             03 MLIEUP  PIC X.                                          00001780
             03 MLIEUH  PIC X.                                          00001790
             03 MLIEUV  PIC X.                                          00001800
             03 MLIEUO  PIC X(3).                                       00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MLIBELLEA    PIC X.                                     00001830
             03 MLIBELLEC    PIC X.                                     00001840
             03 MLIBELLEP    PIC X.                                     00001850
             03 MLIBELLEH    PIC X.                                     00001860
             03 MLIBELLEV    PIC X.                                     00001870
             03 MLIBELLEO    PIC X(20).                                 00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MSELARTA     PIC X.                                     00001900
             03 MSELARTC     PIC X.                                     00001910
             03 MSELARTP     PIC X.                                     00001920
             03 MSELARTH     PIC X.                                     00001930
             03 MSELARTV     PIC X.                                     00001940
             03 MSELARTO     PIC X(3).                                  00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MRAFALEA     PIC X.                                     00001970
             03 MRAFALEC     PIC X.                                     00001980
             03 MRAFALEP     PIC X.                                     00001990
             03 MRAFALEH     PIC X.                                     00002000
             03 MRAFALEV     PIC X.                                     00002010
             03 MRAFALEO     PIC X(3).                                  00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MSELECTA     PIC X.                                     00002040
             03 MSELECTC     PIC X.                                     00002050
             03 MSELECTP     PIC X.                                     00002060
             03 MSELECTH     PIC X.                                     00002070
             03 MSELECTV     PIC X.                                     00002080
             03 MSELECTO     PIC X.                                     00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MZONCMDA  PIC X.                                          00002110
           02 MZONCMDC  PIC X.                                          00002120
           02 MZONCMDP  PIC X.                                          00002130
           02 MZONCMDH  PIC X.                                          00002140
           02 MZONCMDV  PIC X.                                          00002150
           02 MZONCMDO  PIC X(15).                                      00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MLIBERRA  PIC X.                                          00002180
           02 MLIBERRC  PIC X.                                          00002190
           02 MLIBERRP  PIC X.                                          00002200
           02 MLIBERRH  PIC X.                                          00002210
           02 MLIBERRV  PIC X.                                          00002220
           02 MLIBERRO  PIC X(58).                                      00002230
      * CODE TRANSACTION                                                00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCODTRAA  PIC X.                                          00002260
           02 MCODTRAC  PIC X.                                          00002270
           02 MCODTRAP  PIC X.                                          00002280
           02 MCODTRAH  PIC X.                                          00002290
           02 MCODTRAV  PIC X.                                          00002300
           02 MCODTRAO  PIC X(4).                                       00002310
      * NOM DU CICS                                                     00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCICSA    PIC X.                                          00002340
           02 MCICSC    PIC X.                                          00002350
           02 MCICSP    PIC X.                                          00002360
           02 MCICSH    PIC X.                                          00002370
           02 MCICSV    PIC X.                                          00002380
           02 MCICSO    PIC X(5).                                       00002390
      * NETNAME                                                         00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
      * CODE TERMINAL                                                   00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MSCREENA  PIC X.                                          00002500
           02 MSCREENC  PIC X.                                          00002510
           02 MSCREENP  PIC X.                                          00002520
           02 MSCREENH  PIC X.                                          00002530
           02 MSCREENV  PIC X.                                          00002540
           02 MSCREENO  PIC X(5).                                       00002550
                                                                                
