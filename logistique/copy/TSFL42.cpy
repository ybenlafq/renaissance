      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : DEFINITION DES PROFILS D'AFFILIATION (FAM / ENTREPOT ) *         
      *        POUR MISE A JOUR VUE RVGA01WN     (PGR : TFL42)        *         
      *****************************************************************         
      *                                                               *         
       01  TS-FL42.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-FL42-LONG                 PIC S9(3) COMP-3                     
                                           VALUE +57.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-FL42-DONNEES.                                                  
      *----------------------------------  MODE DE MAJ                          
      *                                         ' ' : PAS DE MAJ                
      *                                         'C' : CREATION                  
      *                                         'M' : MODIFICATION              
              03 TS-FL42-CMAJ              PIC X(1).                            
      *----------------------------------  CODIC                                
              03 TS-FL42-CDAC              PIC X(1).                            
      *----------------------------------  CODIC                                
              03 TS-FL42-CMAR              PIC X(5).                            
      *----------------------------------  LIBELLE CODIC                        
              03 TS-FL42-LMAR              PIC X(20).                           
      *----------------------------------  ZONE ENTREPOTS D'AFFILIATION         
              03 TS-FL42-ENTAFF            OCCURS 5.                            
      *----------------------------------  CODE SOCIETE                         
                 04 TS-FL42-CSOCAFF           PIC X(03).                        
      *----------------------------------  CODE ENTREPOT                        
                 04 TS-FL42-CDEPAFF           PIC X(03).                        
                                                                                
