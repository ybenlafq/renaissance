      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVIN9000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIN9000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVIN9000.                                                            
           02  IN90-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  IN90-NDEPOT                                                      
               PIC X(0003).                                                     
           02  IN90-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  IN90-LIEUTRT                                                     
               PIC X(0005).                                                     
           02  IN90-DINVENTAIRE                                                 
               PIC X(0008).                                                     
           02  IN90-NCODIC                                                      
               PIC X(0007).                                                     
           02  IN90-CFAM                                                        
               PIC X(0005).                                                     
           02  IN90-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  IN90-CMARQ                                                       
               PIC X(0005).                                                     
           02  IN90-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  IN90-QSTOCKAV                                                    
               PIC S9(5) COMP-3.                                                
           02  IN90-QREGUL                                                      
               PIC S9(5) COMP-3.                                                
           02  IN90-QSTOCKAP                                                    
               PIC S9(5) COMP-3.                                                
           02  IN90-LCOMMENT                                                    
               PIC X(0020).                                                     
           02  IN90-CACID                                                       
               PIC X(0008).                                                     
           02  IN90-DJOUR                                                       
               PIC X(0008).                                                     
           02  IN90-DHEURE                                                      
               PIC X(0004).                                                     
           02  IN90-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVIN9000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVIN9000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-NSSLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-LIEUTRT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-LIEUTRT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-DINVENTAIRE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-DINVENTAIRE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-QSTOCKAV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-QSTOCKAV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-QREGUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-QREGUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-QSTOCKAP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-QSTOCKAP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-DJOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-DJOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-DHEURE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-DHEURE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN90-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IN90-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
