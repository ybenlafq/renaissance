      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * PARAMETRES � RENSEIGNER POUR PASSAGE AU MODULE MFL016        *  00020001
      ****************************************************************  00050000
      *                                                                 00060000
      *COPY COMMCD16. ATTENTION A CHANGER EN MEME TEMPS QUE COMMCD16            
       01  Z-COMMAREA-FL16.                                                     
           05  COMM-CD16-SSAAMMJJ   PIC X(8).                                   
           05  COMM-CD16-NSOCORIG   PIC X(3).                                   
           05  COMM-CD16-NSOCDEST   PIC X(3).                                   
           05  COMM-CD16-NLIEUDEST  PIC X(3).                                   
           05  COMM-CD16-NOMTS      PIC X(8).                                   
           05  COMM-CD16-NORIGINE   PIC X(7).                                   
           05  COMM-CD16-ERR        PIC X(60).                                  
           05  COMM-CD16-CDRET      PIC X(4).                                   
           05  COMM-CD16-MAX-DATE   PIC X(8).                                   
           05  COMM-CD16-MIN-DATE-M PIC X(8).                                   
           05  COMM-CD16-MIN-DATE-S PIC X(8).                                   
           05  COMM-CD16-NB-POSTE   PIC S9(5) COMP-3.                           
           05  COMM-CD16-MAX-POSTE  PIC S9(5) COMP-3.                           
           05  COMM-CD16-TABLEAU.                                               
      *        10  COMM-CD16-POSTE OCCURS  500.                                 
               10  COMM-CD16-POSTE OCCURS  1000.                                
                   15 COMM-CD16-NCODIC      PIC X(07).                  00120001
                   15 COMM-CD16-QLIVR       PIC S9(7) COMP-3.                   
                   15 COMM-CD16-PRA         PIC S9(7)V99 COMP-3.        00122001
                   15 COMM-CD16-SRP         PIC S9(7)V9(6) COMP-3.      00122001
           05  COMM-CD16-DMUTATION  PIC X(8).                                   
                                                                                
