      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGD2500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGD2500                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGD2500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGD2500.                                                            
      *}                                                                        
           02  GD25-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GD25-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GD25-DRAFALE                                                     
               PIC X(0008).                                                     
           02  GD25-NRAFALE                                                     
               PIC X(0003).                                                     
           02  GD25-NSOC                                                        
               PIC X(0003).                                                     
           02  GD25-NLIEU                                                       
               PIC X(0003).                                                     
           02  GD25-CSELART                                                     
               PIC X(0005).                                                     
           02  GD25-LHEURLIMIT                                                  
               PIC X(0010).                                                     
           02  GD25-NSATELLITE                                                  
               PIC X(0002).                                                     
           02  GD25-NCASE                                                       
               PIC X(0003).                                                     
           02  GD25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGD2500                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGD2500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGD2500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD25-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD25-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD25-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD25-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD25-DRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD25-DRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD25-NRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD25-NRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD25-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD25-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD25-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD25-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD25-CSELART-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD25-CSELART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD25-LHEURLIMIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD25-LHEURLIMIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD25-NSATELLITE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD25-NSATELLITE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD25-NCASE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD25-NCASE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GD25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
