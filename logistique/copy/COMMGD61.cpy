      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010016
      * COMMAREA SPECIFIQUE PRG TGD61 (TGD00 -> MENU)    TR: GD00  *    00020016
      *               VALIDATION DESTOCKAGE                        *    00030016
      *                                                                 00040016
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00050016
      *                                                                 00060016
      *        TRANSACTION GD61 : CREATION RECEPTION FOURNISSEUR   *    00070016
      *                                                                 00080016
          02 COMM-GD61-APPLI REDEFINES COMM-GD00-APPLI.                 00090016
      *------------------------------ ZONE DONNEES TGD61                00100016
             03 COMM-GD61-DONNEES-TGD61.                                00110016
      *------------------------------                                   00120016
                 04  FILLER                 PIC X(06).                  00130016
                 04  COMM-GD61-NPAGE        PIC 9(03).                  00170016
                 04  COMM-GD61-NPAGMAX      PIC 9(03).                  00190016
                 04  COMM-GD61-WTYPEMPL     PIC X(01) OCCURS 14.        00190016
                 04  COMM-GD61-WSEQ         PIC S9(03) COMP-3 OCCURS 14.00190016
                 04  COMM-GD61-QDEM         PIC X(05).                  00190016
                 04  COMM-GD61-SWAP-ATTR    PIC X(01) OCCURS 300.       00190016
      *                                                                 00640016
      *------------------------------ ZONE LIBRE                        00650018
      *                                                                 00660016
             03 COMM-GD61-LIBRE             PIC X(2841).                00670030
      *                                                                 00680016
                                                                                
