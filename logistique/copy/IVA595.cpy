      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA595 AU 30/01/2007  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA595.                                                        
            05 NOMETAT-IVA595           PIC X(6) VALUE 'IVA595'.                
            05 RUPTURES-IVA595.                                                 
           10 IVA595-NSOCVALO           PIC X(03).                      007  003
           10 IVA595-NLIEUVALO          PIC X(03).                      010  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA595-SEQUENCE           PIC S9(04) COMP.                013  002
      *--                                                                       
           10 IVA595-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA595.                                                   
           10 IVA595-DEVISE             PIC X(03).                      015  003
           10 IVA595-LIB1               PIC X(01).                      018  001
           10 IVA595-LIB11              PIC X(01).                      019  001
           10 IVA595-LIB2               PIC X(01).                      020  001
           10 IVA595-LIB21              PIC X(01).                      021  001
           10 IVA595-LIB3               PIC X(01).                      022  001
           10 IVA595-LIB31              PIC X(01).                      023  001
           10 IVA595-LLIEU              PIC X(20).                      024  020
           10 IVA595-PRISTT-ENTEUR      PIC S9(07)V9(2) COMP-3.         044  005
           10 IVA595-PRISTT-ENTREE      PIC S9(07)V9(2) COMP-3.         049  005
           10 IVA595-PRISTT-SOREUR      PIC S9(07)V9(2) COMP-3.         054  005
           10 IVA595-PRISTT-SORTIE      PIC S9(07)V9(2) COMP-3.         059  005
           10 IVA595-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         064  008
           10 IVA595-PSTOCKFINAL        PIC S9(09)V9(6) COMP-3.         072  008
           10 IVA595-PSTOCKINIT         PIC S9(09)V9(6) COMP-3.         080  008
           10 IVA595-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         088  008
           10 IVA595-QSTOCKENTREE       PIC S9(11)      COMP-3.         096  006
           10 IVA595-QSTOCKFINAL        PIC S9(13)      COMP-3.         102  007
           10 IVA595-QSTOCKINIT         PIC S9(13)      COMP-3.         109  007
           10 IVA595-QSTOCKSORTIE       PIC S9(11)      COMP-3.         116  006
           10 IVA595-TXEURO             PIC S9(01)V9(5) COMP-3.         122  004
            05 FILLER                      PIC X(387).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IVA595-LONG           PIC S9(4)   COMP  VALUE +125.           
      *                                                                         
      *--                                                                       
        01  DSECT-IVA595-LONG           PIC S9(4) COMP-5  VALUE +125.           
                                                                                
      *}                                                                        
