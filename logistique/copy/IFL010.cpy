      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IFL010 AU 26/06/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,07,BI,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,05,BI,A,                          *        
      *                           30,07,BI,A,                          *        
      *                           37,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IFL010.                                                        
            05 NOMETAT-IFL010           PIC X(6) VALUE 'IFL010'.                
            05 RUPTURES-IFL010.                                                 
           10 IFL010-NSOCIETE           PIC X(03).                      007  003
           10 IFL010-NLIEU              PIC X(03).                      010  003
           10 IFL010-NMUTATION          PIC X(07).                      013  007
           10 IFL010-WSEQFAM            PIC X(05).                      020  005
           10 IFL010-CMARQ              PIC X(05).                      025  005
           10 IFL010-NCODIC             PIC X(07).                      030  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IFL010-SEQUENCE           PIC S9(04) COMP.                037  002
      *--                                                                       
           10 IFL010-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IFL010.                                                   
           10 IFL010-CFAM               PIC X(05).                      039  005
           10 IFL010-CSELART            PIC X(05).                      044  005
           10 IFL010-LFAM               PIC X(20).                      049  020
           10 IFL010-LIBSOC             PIC X(20).                      069  020
           10 IFL010-LLIEU              PIC X(20).                      089  020
           10 IFL010-LREFFOURN          PIC X(20).                      109  020
           10 IFL010-NDEPOT             PIC X(03).                      129  003
           10 IFL010-NSOC               PIC X(03).                      132  003
           10 IFL010-NSOCDEPOT          PIC X(03).                      135  003
           10 IFL010-NSOCGRP            PIC X(03).                      138  003
           10 IFL010-QMUTEE             PIC S9(07)      COMP-3.         141  004
           10 IFL010-DMUTATION          PIC X(08).                      145  008
            05 FILLER                      PIC X(360).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IFL010-LONG           PIC S9(4)   COMP  VALUE +152.           
      *                                                                         
      *--                                                                       
        01  DSECT-IFL010-LONG           PIC S9(4) COMP-5  VALUE +152.           
                                                                                
      *}                                                                        
