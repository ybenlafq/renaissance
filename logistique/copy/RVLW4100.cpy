      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RTLW41                             *        
      ******************************************************************        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLW4100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW4100.                                                            
           10 LW41-NSOCDEPOT       PIC X(3).                                    
           10 LW41-NDEPOT          PIC X(3).                                    
           10 LW41-NCODIC          PIC X(7).                                    
           10 LW41-BLOCAGE         PIC X(3).                                    
           10 LW41-QPILE           PIC S9(9)V USAGE COMP-3.                     
           10 LW41-DEFFET          PIC X(8).                                    
           10 LW41-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLW4100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW4100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW41-NSOCDEPOT-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 LW41-NSOCDEPOT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW41-NDEPOT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 LW41-NDEPOT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW41-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 LW41-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW41-BLOCAGE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 LW41-BLOCAGE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW41-QPILE-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 LW41-QPILE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW41-DEFFET-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 LW41-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 LW41-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 LW41-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
