      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE INVLM DATE INVENTAIRE LM7              *        
      *----------------------------------------------------------------*        
       01  RVINVLM .                                                            
           05  INVLM-CTABLEG2    PIC X(15).                                     
           05  INVLM-CTABLEG2-REDEF REDEFINES INVLM-CTABLEG2.                   
               10  INVLM-NSOCIETE        PIC X(03).                             
               10  INVLM-NSOCIETE-N     REDEFINES INVLM-NSOCIETE                
                                         PIC 9(03).                             
               10  INVLM-NDEPOT          PIC X(03).                             
               10  INVLM-NDEPOT-N       REDEFINES INVLM-NDEPOT                  
                                         PIC 9(03).                             
               10  INVLM-DINVENV         PIC X(08).                             
               10  INVLM-DINVENV-N      REDEFINES INVLM-DINVENV                 
                                         PIC 9(08).                             
           05  INVLM-WTABLEG     PIC X(80).                                     
           05  INVLM-WTABLEG-REDEF  REDEFINES INVLM-WTABLEG.                    
               10  INVLM-DDEBUT          PIC X(08).                             
               10  INVLM-DDEBUT-N       REDEFINES INVLM-DDEBUT                  
                                         PIC 9(08).                             
               10  INVLM-DFIN            PIC X(08).                             
               10  INVLM-DFIN-N         REDEFINES INVLM-DFIN                    
                                         PIC 9(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVINVLM-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  INVLM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  INVLM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  INVLM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  INVLM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
