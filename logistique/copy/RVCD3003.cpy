      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVCD3003                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCD3003                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVCD3003.                                                            
           02  CD30-DCDE                                                        
               PIC X(0008).                                                     
           02  CD30-NMAG                                                        
               PIC X(0003).                                                     
           02  CD30-NCDE                                                        
               PIC X(0007).                                                     
           02  CD30-NCODIC                                                      
               PIC X(0007).                                                     
           02  CD30-QCDE                                                        
               PIC S9(5) COMP-3.                                                
           02  CD30-NREC                                                        
               PIC X(0007).                                                     
           02  CD30-DREC                                                        
               PIC X(0008).                                                     
           02  CD30-QIMPUTEE                                                    
               PIC S9(5) COMP-3.                                                
           02  CD30-QREC                                                        
               PIC S9(5) COMP-3.                                                
           02  CD30-DVALIDATION                                                 
               PIC X(0008).                                                     
           02  CD30-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  CD30-NCBPCS                                                      
               PIC X(0006).                                                     
           02  CD30-NLBPCS                                                      
               PIC X(0005).                                                     
           02  CD30-WNATURE                                                     
               PIC X(0001).                                                     
           02  CD30-PRA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  CD30-SRP                                                         
               PIC S9(7)V9(0006) COMP-3.                                        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVCD3003                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVCD3003-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-DCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-DCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-QCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-QCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-DREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-DREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-QIMPUTEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-QIMPUTEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-QREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-QREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-DVALIDATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-DVALIDATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-NCBPCS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-NCBPCS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-NLBPCS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-NLBPCS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-WNATURE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-WNATURE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-PRA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-PRA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CD30-SRP-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  CD30-SRP-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
