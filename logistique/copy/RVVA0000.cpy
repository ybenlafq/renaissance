      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVVA0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVA0000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVA0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVA0000.                                                            
      *}                                                                        
           02  VA00-NCODIC                                                      
               PIC X(0007).                                                     
           02  VA00-NSOCVALO                                                    
               PIC X(0003).                                                     
           02  VA00-NLIEUVALO                                                   
               PIC X(0003).                                                     
           02  VA00-CTYPLIEUVALO                                                
               PIC X(0001).                                                     
           02  VA00-LLIEUVALO                                                   
               PIC X(0020).                                                     
           02  VA00-CTYPMVTVALO                                                 
               PIC X(0001).                                                     
           02  VA00-QSTOCKENTREE                                                
               PIC S9(11) COMP-3.                                               
           02  VA00-QSTOCKSORTIE                                                
               PIC S9(11) COMP-3.                                               
           02  VA00-DOPER                                                       
               PIC X(0008).                                                     
           02  VA00-PRMP                                                        
               PIC S9(7)V9(0006) COMP-3.                                        
           02  VA00-PRA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VA00-PSTOCKENTREE                                                
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA00-PSTOCKSORTIE                                                
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA00-NORIGINE                                                    
               PIC X(0007).                                                     
           02  VA00-NSOCMVT                                                     
               PIC X(0003).                                                     
           02  VA00-NLIEUMVT                                                    
               PIC X(0003).                                                     
           02  VA00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVVA0000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVA0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVA0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-NSOCVALO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-NSOCVALO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-NLIEUVALO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-NLIEUVALO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-CTYPLIEUVALO-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-CTYPLIEUVALO-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-LLIEUVALO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-LLIEUVALO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-CTYPMVTVALO-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-CTYPMVTVALO-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-QSTOCKENTREE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-QSTOCKENTREE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-QSTOCKSORTIE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-QSTOCKSORTIE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-DOPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-DOPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-PRMP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-PRA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-PRA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-PSTOCKENTREE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-PSTOCKENTREE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-PSTOCKSORTIE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-PSTOCKSORTIE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-NORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-NORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-NSOCMVT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-NSOCMVT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-NLIEUMVT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-NLIEUMVT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
