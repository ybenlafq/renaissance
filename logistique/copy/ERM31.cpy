      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM31   ERM31                                              00000020
      ***************************************************************** 00000030
       01   ERM31I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNMAGI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLMAGI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAGREGL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLAGREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAGREGF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLAGREGI  PIC X(20).                                      00000410
           02 LIGNEI OCCURS   15 TIMES .                                00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNCODICI     PIC X(8).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCMARQI      PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLREFI  PIC X(20).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPSTDTTCL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MPSTDTTCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPSTDTTCF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MPSTDTTCI    PIC X(8).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXPOL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCEXPOF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCEXPOI      PIC X.                                     00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSTATCL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLSTATCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLSTATCF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLSTATCI     PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOLLECTL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCOLLECTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOLLECTF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCOLLECTI    PIC X(2).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSENSAPPL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MSENSAPPL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSENSAPPF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSENSAPPI    PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWASSORTL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MWASSORTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWASSORTF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MWASSORTI    PIC X.                                     00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPOL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQEXPOF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQEXPOI      PIC X(3).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLSL   COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MQLSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQLSF   PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQLSI   PIC X(3).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQSTOCKL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQSTOCKF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQSTOCKI     PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTODSPL    COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQSTODSPL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQSTODSPF    PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQSTODSPI    PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MZONCMDI  PIC X(12).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(61).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: ERM31   ERM31                                              00001200
      ***************************************************************** 00001210
       01   ERM31O REDEFINES ERM31I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNPAGEA   PIC X.                                          00001390
           02 MNPAGEC   PIC X.                                          00001400
           02 MNPAGEP   PIC X.                                          00001410
           02 MNPAGEH   PIC X.                                          00001420
           02 MNPAGEV   PIC X.                                          00001430
           02 MNPAGEO   PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNSOCA    PIC X.                                          00001460
           02 MNSOCC    PIC X.                                          00001470
           02 MNSOCP    PIC X.                                          00001480
           02 MNSOCH    PIC X.                                          00001490
           02 MNSOCV    PIC X.                                          00001500
           02 MNSOCO    PIC X(3).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MNMAGA    PIC X.                                          00001530
           02 MNMAGC    PIC X.                                          00001540
           02 MNMAGP    PIC X.                                          00001550
           02 MNMAGH    PIC X.                                          00001560
           02 MNMAGV    PIC X.                                          00001570
           02 MNMAGO    PIC X(3).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MLMAGA    PIC X.                                          00001600
           02 MLMAGC    PIC X.                                          00001610
           02 MLMAGP    PIC X.                                          00001620
           02 MLMAGH    PIC X.                                          00001630
           02 MLMAGV    PIC X.                                          00001640
           02 MLMAGO    PIC X(20).                                      00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCFAMA    PIC X.                                          00001670
           02 MCFAMC    PIC X.                                          00001680
           02 MCFAMP    PIC X.                                          00001690
           02 MCFAMH    PIC X.                                          00001700
           02 MCFAMV    PIC X.                                          00001710
           02 MCFAMO    PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLFAMA    PIC X.                                          00001740
           02 MLFAMC    PIC X.                                          00001750
           02 MLFAMP    PIC X.                                          00001760
           02 MLFAMH    PIC X.                                          00001770
           02 MLFAMV    PIC X.                                          00001780
           02 MLFAMO    PIC X(20).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLAGREGA  PIC X.                                          00001810
           02 MLAGREGC  PIC X.                                          00001820
           02 MLAGREGP  PIC X.                                          00001830
           02 MLAGREGH  PIC X.                                          00001840
           02 MLAGREGV  PIC X.                                          00001850
           02 MLAGREGO  PIC X(20).                                      00001860
           02 LIGNEO OCCURS   15 TIMES .                                00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MNCODICA     PIC X.                                     00001890
             03 MNCODICC     PIC X.                                     00001900
             03 MNCODICP     PIC X.                                     00001910
             03 MNCODICH     PIC X.                                     00001920
             03 MNCODICV     PIC X.                                     00001930
             03 MNCODICO     PIC X(8).                                  00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MCMARQA      PIC X.                                     00001960
             03 MCMARQC PIC X.                                          00001970
             03 MCMARQP PIC X.                                          00001980
             03 MCMARQH PIC X.                                          00001990
             03 MCMARQV PIC X.                                          00002000
             03 MCMARQO      PIC X(5).                                  00002010
             03 FILLER       PIC X(2).                                  00002020
             03 MLREFA  PIC X.                                          00002030
             03 MLREFC  PIC X.                                          00002040
             03 MLREFP  PIC X.                                          00002050
             03 MLREFH  PIC X.                                          00002060
             03 MLREFV  PIC X.                                          00002070
             03 MLREFO  PIC X(20).                                      00002080
             03 FILLER       PIC X(2).                                  00002090
             03 MPSTDTTCA    PIC X.                                     00002100
             03 MPSTDTTCC    PIC X.                                     00002110
             03 MPSTDTTCP    PIC X.                                     00002120
             03 MPSTDTTCH    PIC X.                                     00002130
             03 MPSTDTTCV    PIC X.                                     00002140
             03 MPSTDTTCO    PIC ZZZZ9,99.                              00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MCEXPOA      PIC X.                                     00002170
             03 MCEXPOC PIC X.                                          00002180
             03 MCEXPOP PIC X.                                          00002190
             03 MCEXPOH PIC X.                                          00002200
             03 MCEXPOV PIC X.                                          00002210
             03 MCEXPOO      PIC X.                                     00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MLSTATCA     PIC X.                                     00002240
             03 MLSTATCC     PIC X.                                     00002250
             03 MLSTATCP     PIC X.                                     00002260
             03 MLSTATCH     PIC X.                                     00002270
             03 MLSTATCV     PIC X.                                     00002280
             03 MLSTATCO     PIC X(3).                                  00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MCOLLECTA    PIC X.                                     00002310
             03 MCOLLECTC    PIC X.                                     00002320
             03 MCOLLECTP    PIC X.                                     00002330
             03 MCOLLECTH    PIC X.                                     00002340
             03 MCOLLECTV    PIC X.                                     00002350
             03 MCOLLECTO    PIC X(2).                                  00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MSENSAPPA    PIC X.                                     00002380
             03 MSENSAPPC    PIC X.                                     00002390
             03 MSENSAPPP    PIC X.                                     00002400
             03 MSENSAPPH    PIC X.                                     00002410
             03 MSENSAPPV    PIC X.                                     00002420
             03 MSENSAPPO    PIC X.                                     00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MWASSORTA    PIC X.                                     00002450
             03 MWASSORTC    PIC X.                                     00002460
             03 MWASSORTP    PIC X.                                     00002470
             03 MWASSORTH    PIC X.                                     00002480
             03 MWASSORTV    PIC X.                                     00002490
             03 MWASSORTO    PIC X.                                     00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MQEXPOA      PIC X.                                     00002520
             03 MQEXPOC PIC X.                                          00002530
             03 MQEXPOP PIC X.                                          00002540
             03 MQEXPOH PIC X.                                          00002550
             03 MQEXPOV PIC X.                                          00002560
             03 MQEXPOO      PIC X(3).                                  00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MQLSA   PIC X.                                          00002590
             03 MQLSC   PIC X.                                          00002600
             03 MQLSP   PIC X.                                          00002610
             03 MQLSH   PIC X.                                          00002620
             03 MQLSV   PIC X.                                          00002630
             03 MQLSO   PIC X(3).                                       00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MQSTOCKA     PIC X.                                     00002660
             03 MQSTOCKC     PIC X.                                     00002670
             03 MQSTOCKP     PIC X.                                     00002680
             03 MQSTOCKH     PIC X.                                     00002690
             03 MQSTOCKV     PIC X.                                     00002700
             03 MQSTOCKO     PIC X(5).                                  00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MQSTODSPA    PIC X.                                     00002730
             03 MQSTODSPC    PIC X.                                     00002740
             03 MQSTODSPP    PIC X.                                     00002750
             03 MQSTODSPH    PIC X.                                     00002760
             03 MQSTODSPV    PIC X.                                     00002770
             03 MQSTODSPO    PIC X(5).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MZONCMDA  PIC X.                                          00002800
           02 MZONCMDC  PIC X.                                          00002810
           02 MZONCMDP  PIC X.                                          00002820
           02 MZONCMDH  PIC X.                                          00002830
           02 MZONCMDV  PIC X.                                          00002840
           02 MZONCMDO  PIC X(12).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(61).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
