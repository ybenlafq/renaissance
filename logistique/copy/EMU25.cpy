      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU25   EMU25                                              00000020
      ***************************************************************** 00000030
       01   EMU25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSOCIETEI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCONTRIL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MPCONTRIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPCONTRIF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPCONTRII      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLISSAGEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLISSAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLISSAGEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLISSAGEI      PIC X(4).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBARTL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNBARTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBARTF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNBARTI   PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRUPTUREL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MRUPTUREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MRUPTUREF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MRUPTUREI      PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MROT1L    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MROT1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MROT1F    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MROT1I    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MROT2L    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MROT2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MROT2F    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MROT2I    PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MROT3L    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MROT3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MROT3F    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MROT3I    PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MROT4L    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MROT4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MROT4F    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MROT4I    PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MROT5L    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MROT5L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MROT5F    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MROT5I    PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MROTFL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MROTFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MROTFF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MROTFI    PIC X(3).                                       00000610
           02 M6I OCCURS   11 TIMES .                                   00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNLIEUI      PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOLMVL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCPOLMVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCPOLMVF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCPOLMVI     PIC X.                                     00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVPOLMVL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MVPOLMVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVPOLMVF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MVPOLMVI     PIC X(3).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOLR1L     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCPOLR1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCPOLR1F     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCPOLR1I     PIC X.                                     00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVPOLR1L     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MVPOLR1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVPOLR1F     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MVPOLR1I     PIC X(3).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOLR2L     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MCPOLR2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCPOLR2F     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCPOLR2I     PIC X.                                     00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVPOLR2L     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MVPOLR2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVPOLR2F     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MVPOLR2I     PIC X(3).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOLR3L     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MCPOLR3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCPOLR3F     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCPOLR3I     PIC X.                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVPOLR3L     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MVPOLR3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVPOLR3F     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MVPOLR3I     PIC X(3).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOLR4L     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MCPOLR4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCPOLR4F     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCPOLR4I     PIC X.                                     00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVPOLR4L     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MVPOLR4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVPOLR4F     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MVPOLR4I     PIC X(3).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOLR5L     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MCPOLR5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCPOLR5F     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MCPOLR5I     PIC X.                                     00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVPOLR5L     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MVPOLR5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVPOLR5F     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MVPOLR5I     PIC X(3).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOLRFL     COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MCPOLRFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCPOLRFF     PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MCPOLRFI     PIC X.                                     00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVPOLRFL     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MVPOLRFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVPOLRFF     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MVPOLRFI     PIC X(3).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MZONCMDI  PIC X(12).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLIBERRI  PIC X(61).                                      00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCODTRAI  PIC X(4).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCICSI    PIC X(5).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MNETNAMI  PIC X(8).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MSCREENI  PIC X(4).                                       00001460
      ***************************************************************** 00001470
      * SDF: EMU25   EMU25                                              00001480
      ***************************************************************** 00001490
       01   EMU25O REDEFINES EMU25I.                                    00001500
           02 FILLER    PIC X(12).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MDATJOUA  PIC X.                                          00001530
           02 MDATJOUC  PIC X.                                          00001540
           02 MDATJOUP  PIC X.                                          00001550
           02 MDATJOUH  PIC X.                                          00001560
           02 MDATJOUV  PIC X.                                          00001570
           02 MDATJOUO  PIC X(10).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MTIMJOUA  PIC X.                                          00001600
           02 MTIMJOUC  PIC X.                                          00001610
           02 MTIMJOUP  PIC X.                                          00001620
           02 MTIMJOUH  PIC X.                                          00001630
           02 MTIMJOUV  PIC X.                                          00001640
           02 MTIMJOUO  PIC X(5).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MPAGEA    PIC X.                                          00001670
           02 MPAGEC    PIC X.                                          00001680
           02 MPAGEP    PIC X.                                          00001690
           02 MPAGEH    PIC X.                                          00001700
           02 MPAGEV    PIC X.                                          00001710
           02 MPAGEO    PIC X(3).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MSOCIETEA      PIC X.                                     00001740
           02 MSOCIETEC PIC X.                                          00001750
           02 MSOCIETEP PIC X.                                          00001760
           02 MSOCIETEH PIC X.                                          00001770
           02 MSOCIETEV PIC X.                                          00001780
           02 MSOCIETEO      PIC X(3).                                  00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MPCONTRIA      PIC X.                                     00001810
           02 MPCONTRIC PIC X.                                          00001820
           02 MPCONTRIP PIC X.                                          00001830
           02 MPCONTRIH PIC X.                                          00001840
           02 MPCONTRIV PIC X.                                          00001850
           02 MPCONTRIO      PIC X(3).                                  00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLISSAGEA      PIC X.                                     00001880
           02 MLISSAGEC PIC X.                                          00001890
           02 MLISSAGEP PIC X.                                          00001900
           02 MLISSAGEH PIC X.                                          00001910
           02 MLISSAGEV PIC X.                                          00001920
           02 MLISSAGEO      PIC X(4).                                  00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MNBARTA   PIC X.                                          00001950
           02 MNBARTC   PIC X.                                          00001960
           02 MNBARTP   PIC X.                                          00001970
           02 MNBARTH   PIC X.                                          00001980
           02 MNBARTV   PIC X.                                          00001990
           02 MNBARTO   PIC X(3).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MRUPTUREA      PIC X.                                     00002020
           02 MRUPTUREC PIC X.                                          00002030
           02 MRUPTUREP PIC X.                                          00002040
           02 MRUPTUREH PIC X.                                          00002050
           02 MRUPTUREV PIC X.                                          00002060
           02 MRUPTUREO      PIC X(3).                                  00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MROT1A    PIC X.                                          00002090
           02 MROT1C    PIC X.                                          00002100
           02 MROT1P    PIC X.                                          00002110
           02 MROT1H    PIC X.                                          00002120
           02 MROT1V    PIC X.                                          00002130
           02 MROT1O    PIC X(3).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MROT2A    PIC X.                                          00002160
           02 MROT2C    PIC X.                                          00002170
           02 MROT2P    PIC X.                                          00002180
           02 MROT2H    PIC X.                                          00002190
           02 MROT2V    PIC X.                                          00002200
           02 MROT2O    PIC X(3).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MROT3A    PIC X.                                          00002230
           02 MROT3C    PIC X.                                          00002240
           02 MROT3P    PIC X.                                          00002250
           02 MROT3H    PIC X.                                          00002260
           02 MROT3V    PIC X.                                          00002270
           02 MROT3O    PIC X(3).                                       00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MROT4A    PIC X.                                          00002300
           02 MROT4C    PIC X.                                          00002310
           02 MROT4P    PIC X.                                          00002320
           02 MROT4H    PIC X.                                          00002330
           02 MROT4V    PIC X.                                          00002340
           02 MROT4O    PIC X(3).                                       00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MROT5A    PIC X.                                          00002370
           02 MROT5C    PIC X.                                          00002380
           02 MROT5P    PIC X.                                          00002390
           02 MROT5H    PIC X.                                          00002400
           02 MROT5V    PIC X.                                          00002410
           02 MROT5O    PIC X(3).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MROTFA    PIC X.                                          00002440
           02 MROTFC    PIC X.                                          00002450
           02 MROTFP    PIC X.                                          00002460
           02 MROTFH    PIC X.                                          00002470
           02 MROTFV    PIC X.                                          00002480
           02 MROTFO    PIC X(3).                                       00002490
           02 M6O OCCURS   11 TIMES .                                   00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MNLIEUA      PIC X.                                     00002520
             03 MNLIEUC PIC X.                                          00002530
             03 MNLIEUP PIC X.                                          00002540
             03 MNLIEUH PIC X.                                          00002550
             03 MNLIEUV PIC X.                                          00002560
             03 MNLIEUO      PIC X(3).                                  00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MCPOLMVA     PIC X.                                     00002590
             03 MCPOLMVC     PIC X.                                     00002600
             03 MCPOLMVP     PIC X.                                     00002610
             03 MCPOLMVH     PIC X.                                     00002620
             03 MCPOLMVV     PIC X.                                     00002630
             03 MCPOLMVO     PIC X.                                     00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MVPOLMVA     PIC X.                                     00002660
             03 MVPOLMVC     PIC X.                                     00002670
             03 MVPOLMVP     PIC X.                                     00002680
             03 MVPOLMVH     PIC X.                                     00002690
             03 MVPOLMVV     PIC X.                                     00002700
             03 MVPOLMVO     PIC X(3).                                  00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MCPOLR1A     PIC X.                                     00002730
             03 MCPOLR1C     PIC X.                                     00002740
             03 MCPOLR1P     PIC X.                                     00002750
             03 MCPOLR1H     PIC X.                                     00002760
             03 MCPOLR1V     PIC X.                                     00002770
             03 MCPOLR1O     PIC X.                                     00002780
             03 FILLER       PIC X(2).                                  00002790
             03 MVPOLR1A     PIC X.                                     00002800
             03 MVPOLR1C     PIC X.                                     00002810
             03 MVPOLR1P     PIC X.                                     00002820
             03 MVPOLR1H     PIC X.                                     00002830
             03 MVPOLR1V     PIC X.                                     00002840
             03 MVPOLR1O     PIC X(3).                                  00002850
             03 FILLER       PIC X(2).                                  00002860
             03 MCPOLR2A     PIC X.                                     00002870
             03 MCPOLR2C     PIC X.                                     00002880
             03 MCPOLR2P     PIC X.                                     00002890
             03 MCPOLR2H     PIC X.                                     00002900
             03 MCPOLR2V     PIC X.                                     00002910
             03 MCPOLR2O     PIC X.                                     00002920
             03 FILLER       PIC X(2).                                  00002930
             03 MVPOLR2A     PIC X.                                     00002940
             03 MVPOLR2C     PIC X.                                     00002950
             03 MVPOLR2P     PIC X.                                     00002960
             03 MVPOLR2H     PIC X.                                     00002970
             03 MVPOLR2V     PIC X.                                     00002980
             03 MVPOLR2O     PIC X(3).                                  00002990
             03 FILLER       PIC X(2).                                  00003000
             03 MCPOLR3A     PIC X.                                     00003010
             03 MCPOLR3C     PIC X.                                     00003020
             03 MCPOLR3P     PIC X.                                     00003030
             03 MCPOLR3H     PIC X.                                     00003040
             03 MCPOLR3V     PIC X.                                     00003050
             03 MCPOLR3O     PIC X.                                     00003060
             03 FILLER       PIC X(2).                                  00003070
             03 MVPOLR3A     PIC X.                                     00003080
             03 MVPOLR3C     PIC X.                                     00003090
             03 MVPOLR3P     PIC X.                                     00003100
             03 MVPOLR3H     PIC X.                                     00003110
             03 MVPOLR3V     PIC X.                                     00003120
             03 MVPOLR3O     PIC X(3).                                  00003130
             03 FILLER       PIC X(2).                                  00003140
             03 MCPOLR4A     PIC X.                                     00003150
             03 MCPOLR4C     PIC X.                                     00003160
             03 MCPOLR4P     PIC X.                                     00003170
             03 MCPOLR4H     PIC X.                                     00003180
             03 MCPOLR4V     PIC X.                                     00003190
             03 MCPOLR4O     PIC X.                                     00003200
             03 FILLER       PIC X(2).                                  00003210
             03 MVPOLR4A     PIC X.                                     00003220
             03 MVPOLR4C     PIC X.                                     00003230
             03 MVPOLR4P     PIC X.                                     00003240
             03 MVPOLR4H     PIC X.                                     00003250
             03 MVPOLR4V     PIC X.                                     00003260
             03 MVPOLR4O     PIC X(3).                                  00003270
             03 FILLER       PIC X(2).                                  00003280
             03 MCPOLR5A     PIC X.                                     00003290
             03 MCPOLR5C     PIC X.                                     00003300
             03 MCPOLR5P     PIC X.                                     00003310
             03 MCPOLR5H     PIC X.                                     00003320
             03 MCPOLR5V     PIC X.                                     00003330
             03 MCPOLR5O     PIC X.                                     00003340
             03 FILLER       PIC X(2).                                  00003350
             03 MVPOLR5A     PIC X.                                     00003360
             03 MVPOLR5C     PIC X.                                     00003370
             03 MVPOLR5P     PIC X.                                     00003380
             03 MVPOLR5H     PIC X.                                     00003390
             03 MVPOLR5V     PIC X.                                     00003400
             03 MVPOLR5O     PIC X(3).                                  00003410
             03 FILLER       PIC X(2).                                  00003420
             03 MCPOLRFA     PIC X.                                     00003430
             03 MCPOLRFC     PIC X.                                     00003440
             03 MCPOLRFP     PIC X.                                     00003450
             03 MCPOLRFH     PIC X.                                     00003460
             03 MCPOLRFV     PIC X.                                     00003470
             03 MCPOLRFO     PIC X.                                     00003480
             03 FILLER       PIC X(2).                                  00003490
             03 MVPOLRFA     PIC X.                                     00003500
             03 MVPOLRFC     PIC X.                                     00003510
             03 MVPOLRFP     PIC X.                                     00003520
             03 MVPOLRFH     PIC X.                                     00003530
             03 MVPOLRFV     PIC X.                                     00003540
             03 MVPOLRFO     PIC X(3).                                  00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MZONCMDA  PIC X.                                          00003570
           02 MZONCMDC  PIC X.                                          00003580
           02 MZONCMDP  PIC X.                                          00003590
           02 MZONCMDH  PIC X.                                          00003600
           02 MZONCMDV  PIC X.                                          00003610
           02 MZONCMDO  PIC X(12).                                      00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MLIBERRA  PIC X.                                          00003640
           02 MLIBERRC  PIC X.                                          00003650
           02 MLIBERRP  PIC X.                                          00003660
           02 MLIBERRH  PIC X.                                          00003670
           02 MLIBERRV  PIC X.                                          00003680
           02 MLIBERRO  PIC X(61).                                      00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MCODTRAA  PIC X.                                          00003710
           02 MCODTRAC  PIC X.                                          00003720
           02 MCODTRAP  PIC X.                                          00003730
           02 MCODTRAH  PIC X.                                          00003740
           02 MCODTRAV  PIC X.                                          00003750
           02 MCODTRAO  PIC X(4).                                       00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MCICSA    PIC X.                                          00003780
           02 MCICSC    PIC X.                                          00003790
           02 MCICSP    PIC X.                                          00003800
           02 MCICSH    PIC X.                                          00003810
           02 MCICSV    PIC X.                                          00003820
           02 MCICSO    PIC X(5).                                       00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MNETNAMA  PIC X.                                          00003850
           02 MNETNAMC  PIC X.                                          00003860
           02 MNETNAMP  PIC X.                                          00003870
           02 MNETNAMH  PIC X.                                          00003880
           02 MNETNAMV  PIC X.                                          00003890
           02 MNETNAMO  PIC X(8).                                       00003900
           02 FILLER    PIC X(2).                                       00003910
           02 MSCREENA  PIC X.                                          00003920
           02 MSCREENC  PIC X.                                          00003930
           02 MSCREENP  PIC X.                                          00003940
           02 MSCREENH  PIC X.                                          00003950
           02 MSCREENV  PIC X.                                          00003960
           02 MSCREENO  PIC X(4).                                       00003970
                                                                                
