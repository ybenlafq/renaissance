      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===============================================================* 00000010
      *                                                               * 00000020
      *      TS ASSOCIE A LA TRANSACTION TL05                         * 00000030
      *      CONTIENT LES RENSEIGNEMENTS POUR L'EDITION               * 00000040
      *      DES FACTURES HORS-TAXES                                  * 00000050
      *                                                               * 00000060
      *      NOM: 'TL03' + EIBTRMID                                   * 00000070
      *                                                               * 00000080
      *===============================================================* 00000090
                                                                        00000091
       01  TS-TL03.                                                     00000092
           02 TS-TL03-LONG              PIC S9(5) COMP-3     VALUE +15. 00000093
           02 TS-TL03-DONNEES.                                          00000094
              03  TS-TL03-CTOURNEE      PIC X(03).                      00000095
              03  TS-TL03-NORDRE        PIC X(02).                      00000096
              03  TS-TL03-NMAG          PIC X(03).                      00000097
              03  TS-TL03-NVENTE        PIC X(07).                      00000098
                                                                        00000100
      *===============================================================* 00000110
                                                                                
