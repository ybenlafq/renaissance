      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGD1500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGD1500                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGD1500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGD1500.                                                            
      *}                                                                        
           02  GD15-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GD15-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GD15-DATEDEST                                                    
               PIC X(0008).                                                     
           02  GD15-WTYPCUM                                                     
               PIC X(0001).                                                     
           02  GD15-QNBCOMMUT                                                   
               PIC S9(5) COMP-3.                                                
           02  GD15-QNBLTOT                                                     
               PIC S9(5) COMP-3.                                                
           02  GD15-QNBPTOT                                                     
               PIC S9(7) COMP-3.                                                
           02  GD15-QNBLSOL                                                     
               PIC S9(5) COMP-3.                                                
           02  GD15-QNBPSOL                                                     
               PIC S9(7) COMP-3.                                                
           02  GD15-QNBLRACK                                                    
               PIC S9(5) COMP-3.                                                
           02  GD15-QNBPRACK                                                    
               PIC S9(7) COMP-3.                                                
           02  GD15-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGD1500                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGD1500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGD1500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD15-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD15-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-DATEDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD15-DATEDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-WTYPCUM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD15-WTYPCUM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-QNBCOMMUT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD15-QNBCOMMUT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-QNBLTOT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD15-QNBLTOT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-QNBPTOT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD15-QNBPTOT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-QNBLSOL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD15-QNBLSOL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-QNBPSOL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD15-QNBPSOL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-QNBLRACK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD15-QNBLRACK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-QNBPRACK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD15-QNBPRACK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD15-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GD15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
