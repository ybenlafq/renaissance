      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *  PROJET     : QUOTAS DECENTRALISES                             *        
      *  TITRE      : TS D'AFFICHAGE DE L'ECRAN EZQ20 (SAISIE QUOTA)   *        
      *  APPELER PAR: TZQ20 - SAISIE DES QUOTAS - DETAIL PAR PLAGE     *        
      *                                                                *        
      *  CONTENU    : 1 ITEM PAR PAGE                                  *00000050
      *               NIVEAU 1 : 16 ZONES DE LIVRAISON                 *        
      *               NIVEAU 2 :  5 PLAGES HORAIRES                    *        
      ******************************************************************        
      *                                                                         
      **** LONGUEUR TS ********************************** 16LG*261C=4176        
       01  TS-DONNEES.                                                          
           05 TS-ZONE                OCCURS 16.                                 
      ******* ZONE DE LIVRAISON ************************** 11C+250C= 261        
              10 TS-CZONLIV             PIC X(05).                              
              10 TS-QQUOTA-ZL           PIC S9(05) COMP-3.                      
              10 TS-QPRIS-ZL            PIC S9(05) COMP-3.                      
      ********** PLAGES DE DELIVRANCE ******************** 50LG*5C = 250        
              10 TS-PLAGE               OCCURS 50.                              
                 15 TS-EXISTE              PIC X(01).                           
                    88 TS-INEXISTENCE                      VALUE ' '.           
                    88 TS-EXISTENCE                        VALUE 'E'.           
      *......... NUMERO DU POSTE DE L'ENR RTGQ03 DANS LA TSZQ21                 
                 15 TS-IT2                 PIC S9(03) COMP-3.                   
      *......... FLAG DU TAUX DE REMPLISSAGE POUR AFFICHAGE COULEUR             
                 15 TS-WREMPLISSAGE        PIC X.                               
                    88 TS-REMPLISSAGE-100                  VALUE '1'.           
                    88 TS-REMPLISSAGE-SUP-BORNE            VALUE 'S'.           
                    88 TS-REMPLISSAGE-INF-BORNE            VALUE ' '.           
                 15 TS-FILLER1             PIC X(01).                           
                                                                                
