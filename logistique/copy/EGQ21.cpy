      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGQ21   EGQ21                                              00000020
      ***************************************************************** 00000030
       01   EGQ21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MFONCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDATEI    PIC X(8).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDELL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCDELL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCDELF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCDELI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEQUIPL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MEQUIPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEQUIPF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MEQUIPI   PIC X(5).                                       00000330
           02 M131I OCCURS   14 TIMES .                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEGL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MDATEGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEGF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MDATEGI      PIC X(8).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTGL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MQUOTGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQUOTGF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MQUOTGI      PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRISGL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MPRISGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRISGF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MPRISGI      PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEDL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MDATEDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEDF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MDATEDI      PIC X(8).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTDL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MQUOTDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQUOTDF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MQUOTDI      PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRISDL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MPRISDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRISDF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MPRISDI      PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPAGE1L     COMP PIC S9(4).                            00000590
      *--                                                                       
           02 MTOTPAGE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTOTPAGE1F     PIC X.                                     00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MTOTPAGE1I     PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPAGE2L     COMP PIC S9(4).                            00000630
      *--                                                                       
           02 MTOTPAGE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTOTPAGE2F     PIC X.                                     00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MTOTPAGE2I     PIC X(8).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTGEN1L      COMP PIC S9(4).                            00000670
      *--                                                                       
           02 MTOTGEN1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTGEN1F      PIC X.                                     00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MTOTGEN1I      PIC X(8).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTGEN2L      COMP PIC S9(4).                            00000710
      *--                                                                       
           02 MTOTGEN2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTOTGEN2F      PIC X.                                     00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MTOTGEN2I      PIC X(8).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(15).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(58).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: EGQ21   EGQ21                                              00001000
      ***************************************************************** 00001010
       01   EGQ21O REDEFINES EGQ21I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MPAGEA    PIC X.                                          00001190
           02 MPAGEC    PIC X.                                          00001200
           02 MPAGEP    PIC X.                                          00001210
           02 MPAGEH    PIC X.                                          00001220
           02 MPAGEV    PIC X.                                          00001230
           02 MPAGEO    PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MFONCA    PIC X.                                          00001260
           02 MFONCC    PIC X.                                          00001270
           02 MFONCP    PIC X.                                          00001280
           02 MFONCH    PIC X.                                          00001290
           02 MFONCV    PIC X.                                          00001300
           02 MFONCO    PIC X(3).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATEA    PIC X.                                          00001330
           02 MDATEC    PIC X.                                          00001340
           02 MDATEP    PIC X.                                          00001350
           02 MDATEH    PIC X.                                          00001360
           02 MDATEV    PIC X.                                          00001370
           02 MDATEO    PIC X(8).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCDELA    PIC X.                                          00001400
           02 MCDELC    PIC X.                                          00001410
           02 MCDELP    PIC X.                                          00001420
           02 MCDELH    PIC X.                                          00001430
           02 MCDELV    PIC X.                                          00001440
           02 MCDELO    PIC X(3).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MEQUIPA   PIC X.                                          00001470
           02 MEQUIPC   PIC X.                                          00001480
           02 MEQUIPP   PIC X.                                          00001490
           02 MEQUIPH   PIC X.                                          00001500
           02 MEQUIPV   PIC X.                                          00001510
           02 MEQUIPO   PIC X(5).                                       00001520
           02 M131O OCCURS   14 TIMES .                                 00001530
             03 FILLER       PIC X(2).                                  00001540
             03 MDATEGA      PIC X.                                     00001550
             03 MDATEGC PIC X.                                          00001560
             03 MDATEGP PIC X.                                          00001570
             03 MDATEGH PIC X.                                          00001580
             03 MDATEGV PIC X.                                          00001590
             03 MDATEGO      PIC X(8).                                  00001600
             03 FILLER       PIC X(2).                                  00001610
             03 MQUOTGA      PIC X.                                     00001620
             03 MQUOTGC PIC X.                                          00001630
             03 MQUOTGP PIC X.                                          00001640
             03 MQUOTGH PIC X.                                          00001650
             03 MQUOTGV PIC X.                                          00001660
             03 MQUOTGO      PIC X(5).                                  00001670
             03 FILLER       PIC X(2).                                  00001680
             03 MPRISGA      PIC X.                                     00001690
             03 MPRISGC PIC X.                                          00001700
             03 MPRISGP PIC X.                                          00001710
             03 MPRISGH PIC X.                                          00001720
             03 MPRISGV PIC X.                                          00001730
             03 MPRISGO      PIC X(5).                                  00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MDATEDA      PIC X.                                     00001760
             03 MDATEDC PIC X.                                          00001770
             03 MDATEDP PIC X.                                          00001780
             03 MDATEDH PIC X.                                          00001790
             03 MDATEDV PIC X.                                          00001800
             03 MDATEDO      PIC X(8).                                  00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MQUOTDA      PIC X.                                     00001830
             03 MQUOTDC PIC X.                                          00001840
             03 MQUOTDP PIC X.                                          00001850
             03 MQUOTDH PIC X.                                          00001860
             03 MQUOTDV PIC X.                                          00001870
             03 MQUOTDO      PIC X(5).                                  00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MPRISDA      PIC X.                                     00001900
             03 MPRISDC PIC X.                                          00001910
             03 MPRISDP PIC X.                                          00001920
             03 MPRISDH PIC X.                                          00001930
             03 MPRISDV PIC X.                                          00001940
             03 MPRISDO      PIC X(5).                                  00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MTOTPAGE1A     PIC X.                                     00001970
           02 MTOTPAGE1C     PIC X.                                     00001980
           02 MTOTPAGE1P     PIC X.                                     00001990
           02 MTOTPAGE1H     PIC X.                                     00002000
           02 MTOTPAGE1V     PIC X.                                     00002010
           02 MTOTPAGE1O     PIC X(8).                                  00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MTOTPAGE2A     PIC X.                                     00002040
           02 MTOTPAGE2C     PIC X.                                     00002050
           02 MTOTPAGE2P     PIC X.                                     00002060
           02 MTOTPAGE2H     PIC X.                                     00002070
           02 MTOTPAGE2V     PIC X.                                     00002080
           02 MTOTPAGE2O     PIC X(8).                                  00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MTOTGEN1A      PIC X.                                     00002110
           02 MTOTGEN1C PIC X.                                          00002120
           02 MTOTGEN1P PIC X.                                          00002130
           02 MTOTGEN1H PIC X.                                          00002140
           02 MTOTGEN1V PIC X.                                          00002150
           02 MTOTGEN1O      PIC X(8).                                  00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MTOTGEN2A      PIC X.                                     00002180
           02 MTOTGEN2C PIC X.                                          00002190
           02 MTOTGEN2P PIC X.                                          00002200
           02 MTOTGEN2H PIC X.                                          00002210
           02 MTOTGEN2V PIC X.                                          00002220
           02 MTOTGEN2O      PIC X(8).                                  00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MZONCMDA  PIC X.                                          00002250
           02 MZONCMDC  PIC X.                                          00002260
           02 MZONCMDP  PIC X.                                          00002270
           02 MZONCMDH  PIC X.                                          00002280
           02 MZONCMDV  PIC X.                                          00002290
           02 MZONCMDO  PIC X(15).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(58).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
