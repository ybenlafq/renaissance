      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE LW1000                       
      ******************************************************************        
      *                                                                         
       CLEF-LW1000             SECTION.                                         
      *                                                                         
           MOVE 'RVLW1000          '       TO   TABLE-NAME.                     
           MOVE 'LW1000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-LW1000. EXIT.                                                   
                EJECT                                                           
                                                                                
